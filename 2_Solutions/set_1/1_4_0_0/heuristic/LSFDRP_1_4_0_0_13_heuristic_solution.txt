	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_0_fr.p
Instance LSFDP: LSFDRP_1_4_0_0_fd_13.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_4_0_0_fd_13.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.054123 sec.
***  |   |- Create additional vessel information,				took 0.000309 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.000245 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took  0.00022 sec.
***  |   |- Calculation finished,					took    43.03 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000554 sec.
***  |   |- Calculation finished,					took    42.87 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000614 sec.
***  |   |- Calculation finished,					took    43.14 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000209 sec.
***  |   |- Calculation finished,					took    41.98 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000236 sec.
***  |   |- Calculation finished,					took     44.2 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000548 sec.
***  |   |- Calculation finished,					took    42.01 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 217, 10, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 217 (TRME
###  |  R In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 216, 11, 0]
###  |  179 (NLRTM Out@596) - 216 (TRMER In@747/Out@791) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 130 (GRPIR In@696/Out@721) - 81 (EGPSD
###  |   In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 217, 10, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 217 (TRME
###  |  R In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 216, 11, 0]
###  |  179 (NLRTM Out@596) - 216 (TRMER In@747/Out@791) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 130 (GRPIR In@696/Out@721) - 81 (EGPSD
###  |   In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 217, 10, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 217 (TRME
###  |  R In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 216, 11, 0]
###  |  179 (NLRTM Out@596) - 216 (TRMER In@747/Out@791) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 130 (GRPIR In@696/Out@721) - 81 (EGPSD
###  |   In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 217, 10, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 217 (TRME
###  |  R In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 216, 11, 0]
###  |  179 (NLRTM Out@596) - 216 (TRMER In@747/Out@791) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 130 (GRPIR In@696/Out@721) - 81 (EGPSD
###  |   In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 217, 10, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 217 (TRME
###  |  R In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 216, 11, 0]
###  |  179 (NLRTM Out@596) - 216 (TRMER In@747/Out@791) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 130 (GRPIR In@696/Out@721) - 81 (EGPSD
###  |   In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 217, 10, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 217 (TRME
###  |  R In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 216, 11, 0]
###  |  179 (NLRTM Out@596) - 216 (TRMER In@747/Out@791) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 130 (GRPIR In@696/Out@721) - 81 (EGPSD
###  |   In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -1,075,516.5927 (-1075516.5927)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	4m20.488s
user	10m4.434s
sys	0m48.022s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
