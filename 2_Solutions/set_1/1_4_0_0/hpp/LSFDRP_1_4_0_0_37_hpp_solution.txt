	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_0_fr.p
Instance LSFDP: LSFDRP_1_4_0_0_fd_37.p

This object contains the information regarding the instance 1_4_0_0 for a duration of 37 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.043111 sec.
***  |   |- Create additional vessel information,				took 0.000257 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 8056 rows, 25692 columns and 168994 nonzeros
Variable types: 6810 continuous, 18882 integer (18882 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [4e+01, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Found heuristic solution: objective -7518428.133
Presolve removed 7055 rows and 18411 columns
Presolve time: 0.38s
Presolved: 1001 rows, 7281 columns, 29539 nonzeros
Found heuristic solution: objective -7463681.837
Variable types: 0 continuous, 7281 integer (7225 binary)

Root relaxation: objective -5.677578e+05, 995 iterations, 0.06 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -567757.80    0  284 -7463681.8 -567757.80  92.4%     -    0s
H    0     0                    -1609842.772 -567757.80  64.7%     -    0s
H    0     0                    -1235027.596 -567757.80  54.0%     -    0s
     0     0 -796333.00    0  210 -1235027.6 -796333.00  35.5%     -    0s
H    0     0                    -1156379.409 -796333.00  31.1%     -    0s
H    0     0                    -1152780.774 -796333.00  30.9%     -    0s
     0     0 -801002.33    0  121 -1152780.8 -801002.33  30.5%     -    0s
     0     0 -802642.90    0  118 -1152780.8 -802642.90  30.4%     -    0s
     0     0 -848697.84    0  133 -1152780.8 -848697.84  26.4%     -    1s
     0     0 -848697.84    0  229 -1152780.8 -848697.84  26.4%     -    1s
H    0     0                    -1076948.409 -848697.84  21.2%     -    1s
     0     0 -848697.84    0  160 -1076948.4 -848697.84  21.2%     -    1s
     0     0 -848697.84    0  212 -1076948.4 -848697.84  21.2%     -    1s
     0     0 -848697.84    0  150 -1076948.4 -848697.84  21.2%     -    1s
     0     0 -884514.00    0  146 -1076948.4 -884514.00  17.9%     -    1s
     0     0 -886153.58    0  135 -1076948.4 -886153.58  17.7%     -    1s
     0     0 -943531.67    0  188 -1076948.4 -943531.67  12.4%     -    1s
     0     0 -946839.40    0  181 -1076948.4 -946839.40  12.1%     -    1s
     0     0 -946934.51    0  181 -1076948.4 -946934.51  12.1%     -    1s
     0     0 -953904.96    0  175 -1076948.4 -953904.96  11.4%     -    1s
     0     0 -953904.96    0  150 -1076948.4 -953904.96  11.4%     -    2s
     0     0 -953904.96    0  148 -1076948.4 -953904.96  11.4%     -    2s
     0     0 -980113.50    0  130 -1076948.4 -980113.50  8.99%     -    2s
     0     0 -985169.28    0  135 -1076948.4 -985169.28  8.52%     -    2s
     0     0 -985426.67    0  133 -1076948.4 -985426.67  8.50%     -    2s
     0     0 -1010394.3    0  141 -1076948.4 -1010394.3  6.18%     -    2s
     0     0 -1010394.3    0   70 -1076948.4 -1010394.3  6.18%     -    2s
     0     0 -1046187.7    0   68 -1076948.4 -1046187.7  2.86%     -    2s
H    0     0                    -1075516.593 -1046187.7  2.73%     -    2s
     0     0 -1061052.7    0   23 -1075516.6 -1061052.7  1.34%     -    2s
     0     0 -1069419.7    0   56 -1075516.6 -1069419.7  0.57%     -    2s
     0     0     cutoff    0      -1075516.6 -1075516.6  0.00%     -    2s

Explored 1 nodes (5448 simplex iterations) in 2.26 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: -1.07552e+06 -1.07552e+06 -1.07695e+06 ... -7.46368e+06
No other solutions better than -1.07552e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.075516592702e+06, best bound -1.075516592702e+06, gap 0.0000%
Took  3.052389621734619
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 3468 rows, 3143 columns and 13265 nonzeros
Variable types: 2546 continuous, 597 integer (597 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 3468 rows and 3143 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.01 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -1.07552e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -1.075516592702e+06, best bound -1.075516592702e+06, gap 0.0000%

### Instance
### LSFDRP_1_4_0_0_fd_37

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 33
### |A|: 32
### |A^i|: 32
### |A^f|: 0
### |M|: 75
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92) - 105 (FRLEH In@111/Out@127) - 159 (MAPTM In@207/Out@228) - 86 (ESALG In@253/Out@265) - 214 (TRMER
###  |   In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to 86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to 86
###  |
###  |- Vessel  6 has the path [175, 107, 161, 88, 130, 81, 82, 9, 0]
###  |  175 (NLRTM Out@260) - 107 (FRLEH In@279/Out@295) - 161 (MAPTM In@375/Out@396) - 88 (ESALG In@421/Out@433) - 130 (GRPI
###  |  R In@696/Out@721) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In@1194/Out@1227) - 0 (DUMMY_END
###  |   Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to 88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to 88
###  |   |- and carried Demand 16,  18.0 containers from 161 to 88
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428) - 215 (TRMER In@579/Out@623) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  8 has the path [179, 217, 10, 0]
###  |  179 (NLRTM Out@596) - 217 (TRMER In@915/Out@959) - 10 (AEJEA In@1362/Out@1395) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158) - 160 (MAPTM In@238/Out@259) - 87 (ESALG In@284/Out@296) - 200 (TRALI In@668/Out@686) - 187 (OMSL
###  |  L In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to 87
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 16 has the path [108, 162, 89, 216, 11, 0]
###  |  108 (FRLEH Out@326) - 162 (MAPTM In@406/Out@427) - 89 (ESALG In@452/Out@464) - 216 (TRMER In@747/Out@791) - 11 (AEJEA
###  |   In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to 89
###  |   |- and carried Demand 45,  18.0 containers from 162 to 89
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m4.025s
user	0m7.685s
sys	0m0.630s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
