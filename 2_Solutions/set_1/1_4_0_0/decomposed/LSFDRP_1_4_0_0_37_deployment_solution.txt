	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_0_fr.p
Instance LSFDP: LSFDRP_1_4_0_0_fd_37.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 42 rows, 39 columns and 144 nonzeros
Variable types: 0 continuous, 39 integer (39 binary)
Coefficient statistics:
  Matrix range     [1e+00, 6e+00]
  Objective range  [7e+05, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Found heuristic solution: objective -3.52583e+07
Presolve removed 28 rows and 27 columns
Presolve time: 0.00s
Presolved: 14 rows, 12 columns, 50 nonzeros
Variable types: 0 continuous, 12 integer (12 binary)

Root relaxation: objective 1.862645e-09, 12 iterations, 0.00 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0    0.00000    0    4 -3.526e+07    0.00000   100%     -    0s
H    0     0                      -0.0000000    0.00000      -     -    0s

Explored 1 nodes (12 simplex iterations) in 0.00 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -0 -3.52583e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective -0.000000000000e+00, best bound -0.000000000000e+00, gap 0.0000%

### Objective value in 1k: -0.00
### Objective value: -0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 7(ME3): 19, (PMax28), 6
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[7,19] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[5,7] = 1
y[6,7] = 1
y[7,7] = 1
y[8,7] = 1
y[15,7] = 1
y[16,7] = 1

### All variables with value zero:
rho[7,18] = 0
rho[7,30] = 0
eta[7,18,1] = -0
eta[7,18,2] = -0
eta[7,18,3] = -0
eta[7,18,4] = 0
eta[7,18,5] = 0
eta[7,18,6] = 0
eta[7,30,1] = 0
eta[7,30,2] = 0
eta[7,30,3] = 0
eta[7,30,4] = 0
eta[7,30,5] = 0
eta[7,30,6] = -0
y[1,7] = 0
y[2,7] = 0
y[3,7] = 0
y[4,7] = 0
y[9,7] = 0
y[10,7] = 0
y[11,7] = 0
y[12,7] = 0
y[13,7] = 0
y[14,7] = 0
y[17,7] = 0
y[18,7] = 0

### End

real	0m0.584s
user	0m0.165s
sys	0m0.077s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
