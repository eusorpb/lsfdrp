	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_0_fr.p
Instance LSFDP: LSFDRP_1_4_0_0_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_4_0_0_fd_19 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 221
*** |A|: 4463
*** |A^i|: 4463
*** |A^f|: 0
*** |M|: 75
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 30424 rows, 424452 columns and 1727894 nonzeros
Variable types: 344097 continuous, 80355 integer (80355 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [4e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Presolve removed 26588 rows and 347571 columns (presolve time = 28s) ...
Presolve removed 26632 rows and 347671 columns
Presolve time: 29.34s
Presolved: 3792 rows, 76781 columns, 239415 nonzeros
Variable types: 416 continuous, 76365 integer (76330 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9661668e+08   4.577502e+04   0.000000e+00     31s
   10189    4.9440677e+05   0.000000e+00   0.000000e+00     35s

Root relaxation: objective 4.944068e+05, 10189 iterations, 4.17 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 423348.073    0   74          - 423348.073      -     -   36s
H    0     0                    -1.94571e+07 423348.073   102%     -   38s
H    0     0                    -1.81631e+07 423348.073   102%     -   38s
H    0     0                    -1189049.485 423348.073   136%     -   41s
     0     0 261024.831    0  120 -1189049.5 261024.831   122%     -   44s
H    0     0                    -1075516.593 261024.831   124%     -   47s
     0     0 -200978.10    0  129 -1075516.6 -200978.10  81.3%     -   49s
H    0     0                    -1075516.565 -200978.10  81.3%     -   54s
     0     0 -208185.38    0  133 -1075516.6 -208185.38  80.6%     -   57s
     0     0 -238467.79    0  138 -1075516.6 -238467.79  77.8%     -   58s
     0     0 -412015.91    0  133 -1075516.6 -412015.91  61.7%     -   62s
     0     0 -469736.18    0  125 -1075516.6 -469736.18  56.3%     -   66s
     0     0 -471374.20    0  131 -1075516.6 -471374.20  56.2%     -   66s
     0     0 -471439.94    0  130 -1075516.6 -471439.94  56.2%     -   66s
     0     0 -493581.33    0  133 -1075516.6 -493581.33  54.1%     -   66s
     0     0 -499476.28    0  141 -1075516.6 -499476.28  53.6%     -   66s
     0     0 -499742.59    0  137 -1075516.6 -499742.59  53.5%     -   67s
     0     0 -508764.62    0  146 -1075516.6 -508764.62  52.7%     -   67s
     0     0 -508764.62    0  133 -1075516.6 -508764.62  52.7%     -   69s
     0     0 -508764.62    0  127 -1075516.6 -508764.62  52.7%     -   71s
     0     0 -508764.62    0  140 -1075516.6 -508764.62  52.7%     -   72s
     0     0 -508764.62    0  144 -1075516.6 -508764.62  52.7%     -   72s
     0     0 -508896.16    0  144 -1075516.6 -508896.16  52.7%     -   72s
     0     0 -525599.24    0  140 -1075516.6 -525599.24  51.1%     -   72s
     0     0 -526264.83    0  156 -1075516.6 -526264.83  51.1%     -   72s
     0     0 -526273.78    0  156 -1075516.6 -526273.78  51.1%     -   72s
     0     0 -534390.43    0  157 -1075516.6 -534390.43  50.3%     -   73s
     0     0 -536374.40    0  158 -1075516.6 -536374.40  50.1%     -   73s
     0     0 -536398.46    0  158 -1075516.6 -536398.46  50.1%     -   73s
     0     0 -539635.93    0  167 -1075516.6 -539635.93  49.8%     -   73s
     0     0 -539758.79    0  168 -1075516.6 -539758.79  49.8%     -   73s
     0     0 -542176.32    0  159 -1075516.6 -542176.32  49.6%     -   73s
     0     0 -542176.32    0  160 -1075516.6 -542176.32  49.6%     -   73s
     0     0 -542176.32    0  160 -1075516.6 -542176.32  49.6%     -   75s
     0     0 -542176.32    0  124 -1075516.6 -542176.32  49.6%     -   78s
     0     0 -542176.32    0  125 -1075516.6 -542176.32  49.6%     -   79s
     0     0 -542176.32    0  139 -1075516.6 -542176.32  49.6%     -   80s
     0     0 -542176.32    0  142 -1075516.6 -542176.32  49.6%     -   80s
     0     0 -542176.32    0  149 -1075516.6 -542176.32  49.6%     -   80s
     0     0 -542176.32    0  157 -1075516.6 -542176.32  49.6%     -   80s
     0     0 -542176.32    0  165 -1075516.6 -542176.32  49.6%     -   80s
     0     0 -542176.32    0  161 -1075516.6 -542176.32  49.6%     -   80s
     0     0 -542176.32    0  161 -1075516.6 -542176.32  49.6%     -   82s
     0     2 -542176.32    0  160 -1075516.6 -542176.32  49.6%     -   83s
     3     8 -694685.89    2  144 -1075516.6 -576075.54  46.4%   402   85s
   528   235 -957894.13   26   41 -1075516.6 -704339.88  34.5%  26.7   90s
  1063   551 -992187.63   25  138 -1075516.6 -735012.01  31.7%  23.8  105s
  1073   559 -929627.12   38  138 -1075516.6 -747904.97  30.5%  31.7  110s
H 1115   561                    -1075516.520 -995173.46  7.47%  42.0  114s
H 1117   535                    -1075516.517 -995173.46  7.47%  41.9  114s

Cutting planes:
  Gomory: 2
  Projected implied bound: 1
  MIR: 1

Explored 1166 nodes (92966 simplex iterations) in 115.67 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.07552e+06 -1.07552e+06 -1.07552e+06 ... -1.81631e+07

Optimal solution found (tolerance 1.00e-04)
Best objective -1.075516592702e+06, best bound -1.075516508936e+06, gap 0.0000%

***
***  |- Calculation finished, took 115.68 (115.68) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5927 (-1075516.5927) and is feasible with a Gap of 0.0
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92.0) - 105 (FRLEH In@111.0/Out@127.0) - 159 (MAPTM In@207.0/Out@228.0) - 86 (ESALG In@253.0/Out@265.0
###  |  ) - 214 (TRMER In@411.0/Out@455.0) - 5 (AEJEA In@690.0/Out@723.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to  86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to  86
###  |- Vessel  6 has the path [175, 107, 161, 88, 218, 11, 0]
###  |  175 (NLRTM Out@260.0) - 107 (FRLEH In@279.0/Out@295.0) - 161 (MAPTM In@375.0/Out@396.0) - 88 (ESALG In@421.0/Out@433.
###  |  0) - 218 (TRMER In@1083.0/Out@1127.0) - 11 (AEJEA In@1530.0/Out@1563.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to  88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to  88
###  |   |- and carried Demand 16,  18.0 containers from 161 to  88
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428.0) - 215 (TRMER In@579.0/Out@623.0) - 7 (AEJEA In@858.0/Out@891.0) - 0 (DUMMY_END Ziel-Service 7)
###  |- Vessel  8 has the path [179, 217, 10, 0]
###  |  179 (NLRTM Out@596.0) - 217 (TRMER In@915.0/Out@959.0) - 10 (AEJEA In@1362.0/Out@1395.0) - 0 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158.0) - 160 (MAPTM In@238.0/Out@259.0) - 87 (ESALG In@284.0/Out@296.0) - 200 (TRALI In@668.0/Out@686.
###  |  0) - 187 (OMSLL In@878.0/Out@890.0) - 188 (OMSLL In@889.0/Out@904.0) - 8 (AEJEA In@1026.0/Out@1059.0) - 0 (DUMMY_END 
###  |  Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to  87
###  |   |- and carried Demand 55, 111.0 containers from 187 to   8
###  |   |- and carried Demand 56,  35.0 containers from 187 to   8
###  |- Vessel 16 has the path [108, 162, 89, 130, 81, 82, 9, 0]
###  |  108 (FRLEH Out@326.0) - 162 (MAPTM In@406.0/Out@427.0) - 89 (ESALG In@452.0/Out@464.0) - 130 (GRPIR In@696.0/Out@721.
###  |  0) - 81 (EGPSD In@867.0/Out@886.0) - 82 (EGPSD In@957.0/Out@973.0) - 9 (AEJEA In@1194.0/Out@1227.0) - 0 (DUMMY_END Zi
###  |  el-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to  89
###  |   |- and carried Demand 45,  18.0 containers from 162 to  89
###  |   |- and carried Demand 60, 467.0 containers from  81 to   9
###  |   |- and carried Demand 64, 467.0 containers from  82 to   9
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,19] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[5,105,159] = 1
y[5,214,5] = 1
y[5,5,0] = 1
y[5,86,214] = 1
y[5,174,105] = 1
y[5,159,86] = 1
y[6,218,11] = 1
y[6,107,161] = 1
y[6,88,218] = 1
y[6,175,107] = 1
y[6,11,0] = 1
y[6,161,88] = 1
y[7,177,215] = 1
y[7,7,0] = 1
y[7,215,7] = 1
y[8,179,217] = 1
y[8,10,0] = 1
y[8,217,10] = 1
y[15,200,187] = 1
y[15,160,87] = 1
y[15,106,160] = 1
y[15,187,188] = 1
y[15,8,0] = 1
y[15,87,200] = 1
y[15,188,8] = 1
y[16,9,0] = 1
y[16,162,89] = 1
y[16,108,162] = 1
y[16,82,9] = 1
y[16,89,130] = 1
y[16,81,82] = 1
y[16,130,81] = 1
xD[5,105,159] = 42
xD[5,174,105] = 42
xD[5,159,86] = 42
xD[6,174,105] = 17
xD[8,159,86] = 580
xD[11,107,161] = 74
xD[11,175,107] = 74
xD[11,161,88] = 74
xD[12,175,107] = 5
xD[15,161,88] = 484
xD[16,161,88] = 18
xD[40,160,87] = 580
xD[44,162,89] = 484
xD[45,162,89] = 18
xD[55,187,188] = 111
xD[55,188,8] = 111
xD[56,187,188] = 35
xD[56,188,8] = 35
xD[60,82,9] = 467
xD[60,81,82] = 467
xD[64,82,9] = 467
zE[5] = 690
zE[7] = 858
zE[8] = 1026
zE[9] = 1194
zE[10] = 1362
zE[11] = 1530
zE[81] = 867
zE[82] = 957
zE[86] = 253
zE[87] = 284
zE[88] = 421
zE[89] = 452
zE[105] = 111
zE[107] = 279
zE[130] = 696
zE[159] = 207
zE[160] = 238
zE[161] = 375
zE[162] = 406
zE[187] = 878
zE[188] = 889
zE[200] = 668
zE[214] = 411
zE[215] = 579
zE[217] = 915
zE[218] = 1083
zX[5] = 723
zX[7] = 891
zX[8] = 1059
zX[9] = 1227
zX[10] = 1395
zX[11] = 1563
zX[81] = 886
zX[82] = 973
zX[86] = 265
zX[87] = 296
zX[88] = 433
zX[89] = 464
zX[105] = 127
zX[106] = 158
zX[107] = 295
zX[108] = 326
zX[130] = 721
zX[159] = 228
zX[160] = 259
zX[161] = 396
zX[162] = 427
zX[174] = 92
zX[175] = 260
zX[177] = 428
zX[179] = 596
zX[187] = 890
zX[188] = 904
zX[200] = 686
zX[214] = 455
zX[215] = 623
zX[217] = 959
zX[218] = 1127
phi[5] = 631
phi[6] = 1303
phi[7] = 463
phi[8] = 799
phi[15] = 901
phi[16] = 901

### All variables with value smaller zero:

### End

real	2m19.519s
user	5m16.352s
sys	0m21.509s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
