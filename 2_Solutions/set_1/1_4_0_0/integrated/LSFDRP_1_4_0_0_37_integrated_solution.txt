	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_0_fr.p
Instance LSFDP: LSFDRP_1_4_0_0_fd_37.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_4_0_0_fd_37 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 221
*** |A|: 4463
*** |A^i|: 4463
*** |A^f|: 0
*** |M|: 75
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 30424 rows, 424452 columns and 1727894 nonzeros
Variable types: 344097 continuous, 80355 integer (80355 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [4e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Presolve removed 26588 rows and 347571 columns (presolve time = 22s) ...
Presolve removed 26632 rows and 347671 columns
Presolve time: 22.62s
Presolved: 3792 rows, 76781 columns, 239415 nonzeros
Variable types: 416 continuous, 76365 integer (76330 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.3725820e+08   4.577502e+04   0.000000e+00     24s
    6230    1.0490797e+06   5.397502e+03   0.000000e+00     25s
   10122    4.9440677e+05   0.000000e+00   0.000000e+00     28s

Root relaxation: objective 4.944068e+05, 10122 iterations, 3.71 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 423348.073    0   75          - 423348.073      -     -   29s
H    0     0                    -3.58012e+07 423348.073   101%     -   30s
H    0     0                    -2.23117e+07 423348.073   102%     -   30s
H    0     0                    -1186907.579 423348.073   136%     -   33s
     0     0 261024.831    0  120 -1186907.6 261024.831   122%     -   35s
     0     0 -198254.58    0  117 -1186907.6 -198254.58  83.3%     -   36s
H    0     0                    -1099011.901 -198254.58  82.0%     -   37s
H    0     0                    -1075516.593 -198254.58  81.6%     -   37s
     0     0 -201039.71    0  143 -1075516.6 -201039.71  81.3%     -   37s
     0     0 -203842.89    0  133 -1075516.6 -203842.89  81.0%     -   37s
     0     0 -252528.73    0  132 -1075516.6 -252528.73  76.5%     -   38s
     0     0 -271931.72    0  133 -1075516.6 -271931.72  74.7%     -   38s
     0     0 -276424.53    0  135 -1075516.6 -276424.53  74.3%     -   38s
     0     0 -276707.55    0  138 -1075516.6 -276707.55  74.3%     -   38s
     0     0 -276858.08    0  137 -1075516.6 -276858.08  74.3%     -   38s
     0     0 -276858.08    0  140 -1075516.6 -276858.08  74.3%     -   38s
     0     0 -293165.18    0  137 -1075516.6 -293165.18  72.7%     -   38s
     0     0 -309570.38    0  138 -1075516.6 -309570.38  71.2%     -   38s
     0     0 -317717.37    0  143 -1075516.6 -317717.37  70.5%     -   38s
     0     0 -317717.37    0  113 -1075516.6 -317717.37  70.5%     -   39s
     0     0 -317717.37    0  120 -1075516.6 -317717.37  70.5%     -   40s
     0     0 -317780.48    0  122 -1075516.6 -317780.48  70.5%     -   40s
     0     0 -317976.63    0  131 -1075516.6 -317976.63  70.4%     -   40s
     0     0 -321493.84    0  133 -1075516.6 -321493.84  70.1%     -   40s
     0     0 -324900.36    0  130 -1075516.6 -324900.36  69.8%     -   40s
     0     0 -324900.36    0  132 -1075516.6 -324900.36  69.8%     -   40s
     0     0 -344303.94    0  143 -1075516.6 -344303.94  68.0%     -   40s
     0     0 -349018.87    0  129 -1075516.6 -349018.87  67.5%     -   40s
     0     0 -349344.23    0  128 -1075516.6 -349344.23  67.5%     -   40s
     0     0 -349344.23    0  128 -1075516.6 -349344.23  67.5%     -   41s
     0     2 -349344.23    0  128 -1075516.6 -349344.23  67.5%     -   41s
H  334   208                    -1075516.593 -564393.85  47.5%  33.9   44s
H  335   208                    -1075516.545 -564393.85  47.5%  34.1   44s
   396   256 -850441.27   61   76 -1075516.5 -564393.85  47.5%  32.6   45s
  1078   667 -744111.34   17  120 -1075516.5 -728076.26  32.3%  28.8   54s
H 1079   634                    -1075516.545 -728076.26  32.3%  28.8   55s
H 1096   615                    -1075516.544 -940220.03  12.6%  34.2   56s

Cutting planes:
  Gomory: 12
  Projected implied bound: 3
  MIR: 2
  Flow cover: 8

Explored 1135 nodes (64429 simplex iterations) in 57.44 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: -1.07552e+06 -1.07552e+06 -1.07552e+06 ... -3.58012e+07
No other solutions better than -1.07552e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (1.9867e-06) exceeds tolerance
Best objective -1.075516544183e+06, best bound -1.075516544183e+06, gap 0.0000%

***
***  |- Calculation finished, took 57.45 (57.45) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,075,516.5442 (-1075516.5442) and is feasible with a Gap of 0.0
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [174, 105, 159, 86, 214, 5, 0]
###  |  174 (NLRTM Out@92.0) - 105 (FRLEH In@111.0/Out@127.0) - 159 (MAPTM In@207.0/Out@228.0) - 86 (ESALG In@253.0/Out@265.0
###  |  ) - 214 (TRMER In@411.0/Out@455.0) - 5 (AEJEA In@690.0/Out@723.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  5,  42.0 containers from 174 to  86
###  |   |- and carried Demand  6,  17.0 containers from 174 to 105
###  |   |- and carried Demand  8, 580.0 containers from 159 to  86
###  |- Vessel  6 has the path [175, 107, 161, 88, 130, 81, 82, 9, 0]
###  |  175 (NLRTM Out@260.0) - 107 (FRLEH In@279.0/Out@295.0) - 161 (MAPTM In@375.0/Out@396.0) - 88 (ESALG In@421.0/Out@433.
###  |  0) - 130 (GRPIR In@696.0/Out@721.0) - 81 (EGPSD In@867.0/Out@886.0) - 82 (EGPSD In@957.0/Out@973.0) - 9 (AEJEA In@119
###  |  4.0/Out@1227.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 11,  74.0 containers from 175 to  88
###  |   |- and carried Demand 12,   5.0 containers from 175 to 107
###  |   |- and carried Demand 15, 484.0 containers from 161 to  88
###  |   |- and carried Demand 16,  18.0 containers from 161 to  88
###  |   |- and carried Demand 60, 467.0 containers from  81 to   9
###  |   |- and carried Demand 64, 467.0 containers from  82 to   9
###  |- Vessel  7 has the path [177, 215, 7, 0]
###  |  177 (NLRTM Out@428.0) - 215 (TRMER In@579.0/Out@623.0) - 7 (AEJEA In@858.0/Out@891.0) - 0 (DUMMY_END Ziel-Service 7)
###  |- Vessel  8 has the path [179, 217, 11, 0]
###  |  179 (NLRTM Out@596.0) - 217 (TRMER In@915.0/Out@959.0) - 11 (AEJEA In@1530.0/Out@1563.0) - 0 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 15 has the path [106, 160, 87, 200, 187, 188, 8, 0]
###  |  106 (FRLEH Out@158.0) - 160 (MAPTM In@238.0/Out@259.0) - 87 (ESALG In@284.0/Out@296.0) - 200 (TRALI In@668.0/Out@686.
###  |  0) - 187 (OMSLL In@878.0/Out@890.0) - 188 (OMSLL In@889.0/Out@904.0) - 8 (AEJEA In@1026.0/Out@1059.0) - 0 (DUMMY_END 
###  |  Ziel-Service 7)
###  |   |- and carried Demand 40, 580.0 containers from 160 to  87
###  |   |- and carried Demand 55, 111.0 containers from 187 to   8
###  |   |- and carried Demand 56,  35.0 containers from 187 to   8
###  |- Vessel 16 has the path [108, 162, 89, 216, 10, 0]
###  |  108 (FRLEH Out@326.0) - 162 (MAPTM In@406.0/Out@427.0) - 89 (ESALG In@452.0/Out@464.0) - 216 (TRMER In@747.0/Out@791.
###  |  0) - 10 (AEJEA In@1362.0/Out@1395.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 44, 484.0 containers from 162 to  89
###  |   |- and carried Demand 45,  18.0 containers from 162 to  89
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,19] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[5,105,159] = 1
y[5,214,5] = 1
y[5,5,0] = 1
y[5,86,214] = 1
y[5,174,105] = 1
y[5,159,86] = 1
y[6,9,0] = 1
y[6,107,161] = 1
y[6,82,9] = 0.999997
y[6,88,130] = 1
y[6,81,82] = 0.999997
y[6,175,107] = 1
y[6,130,81] = 1
y[6,161,88] = 1
y[7,177,215] = 1
y[7,7,0] = 1
y[7,215,7] = 1
y[8,217,11] = 0.999997
y[8,179,217] = 0.999997
y[8,11,0] = 1
y[15,200,187] = 1
y[15,160,87] = 1
y[15,106,160] = 1
y[15,187,188] = 1
y[15,8,0] = 1
y[15,87,200] = 1
y[15,188,8] = 1
y[16,162,89] = 1
y[16,108,162] = 1
y[16,10,0] = 1
y[16,89,216] = 0.999997
y[16,216,10] = 0.999997
xD[5,105,159] = 42
xD[5,174,105] = 42
xD[5,159,86] = 42
xD[6,174,105] = 17
xD[8,159,86] = 580
xD[11,107,161] = 74
xD[11,175,107] = 74
xD[11,161,88] = 74
xD[12,175,107] = 5
xD[15,161,88] = 484
xD[16,161,88] = 18
xD[40,160,87] = 580
xD[44,162,89] = 484
xD[45,162,89] = 18
xD[55,187,188] = 111
xD[55,188,8] = 111
xD[56,187,188] = 35
xD[56,188,8] = 35
xD[60,82,9] = 466.999
xD[60,81,82] = 466.999
xD[64,82,9] = 467
zE[5] = 690
zE[7] = 858
zE[8] = 1026
zE[9] = 1194
zE[10] = 1362
zE[11] = 1530
zE[81] = 867
zE[82] = 957
zE[86] = 253
zE[87] = 284
zE[88] = 421
zE[89] = 452
zE[105] = 111
zE[107] = 279
zE[130] = 696
zE[159] = 207
zE[160] = 238
zE[161] = 375
zE[162] = 406
zE[187] = 878
zE[188] = 889
zE[200] = 668
zE[214] = 411
zE[215] = 579
zE[216] = 747
zE[217] = 915
zX[5] = 723
zX[7] = 891
zX[8] = 1059
zX[9] = 1227
zX[10] = 1395
zX[11] = 1563
zX[81] = 886
zX[82] = 973
zX[86] = 265
zX[87] = 296
zX[88] = 433
zX[89] = 464
zX[105] = 127
zX[106] = 158
zX[107] = 295
zX[108] = 326
zX[130] = 721
zX[159] = 228
zX[160] = 259
zX[161] = 396
zX[162] = 427
zX[174] = 92
zX[175] = 260
zX[177] = 428
zX[179] = 596
zX[187] = 890
zX[188] = 904
zX[200] = 686
zX[214] = 455
zX[215] = 623
zX[216] = 791
zX[217] = 959
phi[5] = 631
phi[6] = 967
phi[7] = 463
phi[8] = 967
phi[15] = 901
phi[16] = 1069

### All variables with value smaller zero:

### End

real	1m19.423s
user	2m25.314s
sys	0m11.094s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
