	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_2_0_0_fr.p
Instance LSFDP: LSFDRP_1_2_0_0_fd_40.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 36 rows, 33 columns and 108 nonzeros
Variable types: 0 continuous, 33 integer (33 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+00]
  Objective range  [8e+05, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Found heuristic solution: objective -8907579.632
Presolve removed 30 rows and 27 columns
Presolve time: 0.00s
Presolved: 6 rows, 6 columns, 19 nonzeros
Variable types: 0 continuous, 6 integer (6 binary)

Root relaxation: objective -0.000000e+00, 1 iterations, 0.00 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0      -0.0000000   -0.00000  0.00%     -    0s

Explored 0 nodes (1 simplex iterations) in 0.00 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -0 -8.90758e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -0.000000000000e+00, best bound -0.000000000000e+00, gap 0.0000%

### Objective value in 1k: -0.00
### Objective value: -0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 24(WAF7): 18, (PMax25), 3
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[4,24] = 1
y[13,24] = 1
y[14,24] = 1

### All variables with value zero:
rho[24,19] = -0
rho[24,30] = -0
eta[24,18,4] = 0
eta[24,19,1] = 0
eta[24,19,2] = 0
eta[24,19,3] = 0
eta[24,19,4] = 0
eta[24,30,1] = 0
eta[24,30,2] = 0
eta[24,30,3] = 0
eta[24,30,4] = 0
y[1,24] = 0
y[2,24] = 0
y[3,24] = 0
y[5,24] = 0
y[6,24] = 0
y[7,24] = 0
y[8,24] = 0
y[9,24] = 0
y[10,24] = 0
y[11,24] = 0
y[12,24] = 0
y[15,24] = 0
y[16,24] = 0
y[17,24] = 0
y[18,24] = 0

### End

real	0m0.306s
user	0m0.073s
sys	0m0.030s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
