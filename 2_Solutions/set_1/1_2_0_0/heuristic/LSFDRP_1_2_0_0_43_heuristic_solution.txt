	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_2_0_0_fr.p
Instance LSFDP: LSFDRP_1_2_0_0_fd_43.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_2_0_0_fd_43.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.029967 sec.
***  |   |- Create additional vessel information,				took  0.00013 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.000262 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took 0.000244 sec.
***  |   |- Calculation finished,					took    10.47 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000162 sec.
***  |   |- Calculation finished,					took      9.9 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000158 sec.
***  |   |- Calculation finished,					took     9.97 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took  0.00017 sec.
***  |   |- Calculation finished,					took     9.99 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000168 sec.
***  |   |- Calculation finished,					took    10.26 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000198 sec.
***  |   |- Calculation finished,					took    10.09 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is 1,764,567.4262 (1764567.4262)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	1m4.554s
user	2m5.821s
sys	0m10.492s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
