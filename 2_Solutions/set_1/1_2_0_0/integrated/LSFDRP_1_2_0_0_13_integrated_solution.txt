	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_2_0_0_fr.p
Instance LSFDP: LSFDRP_1_2_0_0_fd_13.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_2_0_0_fd_13 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 112
*** |A|: 849
*** |A^i|: 849
*** |A^f|: 0
*** |M|: 45
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 9143 rows, 55432 columns and 242008 nonzeros
Variable types: 40135 continuous, 15297 integer (15297 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Presolve removed 7536 rows and 46953 columns
Presolve time: 4.95s
Presolved: 1607 rows, 8479 columns, 36766 nonzeros
Variable types: 805 continuous, 7674 integer (7650 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    1.7180246e+09   1.669707e+05   0.000000e+00      5s
    4091    4.3035833e+06   0.000000e+00   0.000000e+00      5s

Root relaxation: objective 4.303583e+06, 4091 iterations, 0.33 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 4303583.34    0   57          - 4303583.34      -     -    5s
H    0     0                    -6056473.679 4303583.34   171%     -    5s
H    0     0                    -2695669.022 4303583.34   260%     -    5s
H    0     0                    1092979.0148 4303583.34   294%     -    5s
     0     0 3202764.08    0   81 1092979.01 3202764.08   193%     -    5s
     0     0 3202178.06    0   81 1092979.01 3202178.06   193%     -    5s
     0     0 2740260.37    0  105 1092979.01 2740260.37   151%     -    6s
     0     0 2740260.37    0   60 1092979.01 2740260.37   151%     -    6s
H    0     0                    1579463.1535 2740260.37  73.5%     -    6s
     0     0 2420823.16    0   65 1579463.15 2420823.16  53.3%     -    7s
H    0     0                    1764567.4262 2420823.16  37.2%     -    7s
     0     0 1796733.20    0   66 1764567.43 1796733.20  1.82%     -    7s
     0     0 1796733.20    0   70 1764567.43 1796733.20  1.82%     -    7s
     0     0 1775321.04    0  108 1764567.43 1775321.04  0.61%     -    7s
     0     0 1775321.04    0   52 1764567.43 1775321.04  0.61%     -    7s
     0     0 1773823.23    0   80 1764567.43 1773823.23  0.52%     -    7s
     0     0 1773823.23    0   76 1764567.43 1773823.23  0.52%     -    7s
     0     0 1773823.23    0   78 1764567.43 1773823.23  0.52%     -    7s
     0     0 1773823.23    0   79 1764567.43 1773823.23  0.52%     -    7s

Explored 1 nodes (12735 simplex iterations) in 7.75 seconds
Thread count was 16 (of 16 available processors)

Solution count 5: 1.76457e+06 1.57946e+06 1.09298e+06 ... -6.05647e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 1.764567426168e+06, best bound 1.764567426171e+06, gap 0.0000%

***
***  |- Calculation finished, took 7.75 (7.75) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262) and is feasible with a Gap of 0.0
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288.0) - 37 (ESALG In@496.0/Out@515.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456.0) - 39 (ESALG In@664.0/Out@683.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220.0) - 34 (ESALG In@328.0/Out@347.0) - 83 (MAPTM In@375.0/Out@396.0) - 84 (MAPTM In@379.0/Out@398.0) 
###  |  - 85 (MAPTM In@406.0/Out@427.0) - 36 (ESALG In@452.0/Out@464.0) - 41 (ESALG In@832.0/Out@851.0) - 0 (DUMMY_END Ziel-S
###  |  ervice 24)
###  |   |- and carried Demand 11, 214.0 containers from  83 to  41
###  |   |- and carried Demand 12,  10.0 containers from  83 to  41
###  |   |- and carried Demand 13, 484.0 containers from  83 to  36
###  |   |- and carried Demand 14,  18.0 containers from  83 to  36
###  |   |- and carried Demand 28, 214.0 containers from  85 to  41
###  |   |- and carried Demand 29,  10.0 containers from  85 to  41
###  |   |- and carried Demand 30, 484.0 containers from  85 to  36
###  |   |- and carried Demand 31,  18.0 containers from  85 to  36
###  |   |- and carried Demand 37, 214.0 containers from  84 to  41
###  |   |- and carried Demand 38,  10.0 containers from  84 to  41
###  |   |- and carried Demand 39, 484.0 containers from  84 to  36
###  |   |- and carried Demand 40,  18.0 containers from  84 to  36
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[1,25,37] = 1
y[1,37,0] = 1
y[2,39,0] = 1
y[2,26,39] = 1
y[13,36,41] = 1
y[13,34,83] = 1
y[13,84,85] = 1
y[13,83,84] = 1
y[13,41,0] = 1
y[13,29,34] = 1
y[13,85,36] = 1
xD[11,36,41] = 214
xD[11,84,85] = 214
xD[11,83,84] = 214
xD[11,85,36] = 214
xD[12,36,41] = 10
xD[12,84,85] = 10
xD[12,83,84] = 10
xD[12,85,36] = 10
xD[13,84,85] = 484
xD[13,83,84] = 484
xD[13,85,36] = 484
xD[14,84,85] = 18
xD[14,83,84] = 18
xD[14,85,36] = 18
xD[28,36,41] = 214
xD[28,85,36] = 214
xD[29,36,41] = 10
xD[29,85,36] = 10
xD[30,85,36] = 484
xD[31,85,36] = 18
xD[37,36,41] = 214
xD[37,84,85] = 214
xD[37,85,36] = 214
xD[38,36,41] = 10
xD[38,84,85] = 10
xD[38,85,36] = 10
xD[39,84,85] = 484
xD[39,85,36] = 484
xD[40,84,85] = 18
xD[40,85,36] = 18
zE[34] = 328
zE[36] = 452
zE[37] = 496
zE[39] = 664
zE[41] = 832
zE[83] = 375
zE[84] = 379
zE[85] = 406
zX[25] = 288
zX[26] = 456
zX[29] = 220
zX[34] = 347
zX[36] = 464
zX[37] = 515
zX[39] = 683
zX[41] = 851
zX[83] = 396
zX[84] = 398
zX[85] = 427
phi[1] = 227
phi[2] = 227
phi[13] = 631

### All variables with value smaller zero:

### End

real	0m11.059s
user	0m29.586s
sys	0m5.040s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
