	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_3_0_0_fr.p
Instance LSFDP: LSFDRP_1_3_0_0_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_3_0_0_fd_19 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 117
*** |A|: 1359
*** |A^i|: 1359
*** |A^f|: 0
*** |M|: 77
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 14240 rows, 132078 columns and 524596 nonzeros
Variable types: 107607 continuous, 24471 integer (24471 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 7e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 2e+00]
Presolve removed 12475 rows and 120107 columns (presolve time = 10s) ...
Presolve removed 12668 rows and 120482 columns
Presolve time: 9.98s
Presolved: 1572 rows, 11596 columns, 27466 nonzeros
Variable types: 39 continuous, 11557 integer (11530 binary)
Found heuristic solution: objective -3627186.670

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.8414875e+07   1.649183e+03   0.000000e+00     10s
    1100   -7.4061432e+05   0.000000e+00   0.000000e+00     10s

Root relaxation: objective -7.406143e+05, 1100 iterations, 0.06 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0    -740614.3223 -740614.32  0.00%     -   10s

Explored 0 nodes (1100 simplex iterations) in 10.23 seconds
Thread count was 16 (of 16 available processors)

Solution count 2: -740614 -3.62719e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -7.406143222740e+05, best bound -7.406143222740e+05, gap 0.0000%

***
***  |- Calculation finished, took 10.23 (10.23) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223) and is feasible with a Gap of 0.0
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570.0) - 68 (HNPCR In@1166.0/Out@1179.0) - 0 (DUMMY_END Ziel-Service 121)
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470.0) - 112 (USORF In@873.0/Out@881.0) - 108 (USMIA In@935.0/Out@943.0) - 67 (HNPCR In@998.0/Out@1011.
###  |  0) - 0 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to  67
###  |   |- and carried Demand 55,  75.0 containers from 112 to  67
###  |   |- and carried Demand 56, 114.0 containers from 112 to  67
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[121,30] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[12,68,0] = 1
y[12,8,68] = 1
y[18,112,108] = 1
y[18,67,0] = 1
y[18,108,67] = 1
y[18,54,112] = 1
xD[54,108,67] = 10
xD[55,112,108] = 75
xD[55,108,67] = 75
xD[56,112,108] = 114
xD[56,108,67] = 114
zE[67] = 998
zE[68] = 1166
zE[108] = 935
zE[112] = 873
zX[8] = 570
zX[54] = 470
zX[67] = 1011
zX[68] = 1179
zX[108] = 943
zX[112] = 881
phi[12] = 609
phi[18] = 541

### All variables with value smaller zero:

### End

real	0m16.557s
user	0m16.053s
sys	0m0.271s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
