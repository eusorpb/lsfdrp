	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_3_0_0_fr.p
Instance LSFDP: LSFDRP_1_3_0_0_fd_52.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_3_0_0_fd_52.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.027393 sec.
***  |   |- Create additional vessel information,				took 0.000135 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took  0.00016 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took 0.000145 sec.
***  |   |- Calculation finished,					took     2.52 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000155 sec.
***  |   |- Calculation finished,					took     2.39 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000151 sec.
***  |   |- Calculation finished,					took     2.57 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000115 sec.
***  |   |- Calculation finished,					took     2.64 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took  0.00014 sec.
***  |   |- Calculation finished,					took     2.78 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000134 sec.
***  |   |- Calculation finished,					took     2.71 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -740,614.3223 (-740614.3223)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	0m17.607s
user	0m19.582s
sys	0m0.588s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
