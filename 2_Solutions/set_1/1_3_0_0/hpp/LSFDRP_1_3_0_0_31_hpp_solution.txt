	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_3_0_0_fr.p
Instance LSFDP: LSFDRP_1_3_0_0_fd_31.p

This object contains the information regarding the instance 1_3_0_0 for a duration of 31 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.016467 sec.
***  |   |- Create additional vessel information,				took  9.4e-05 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 399 rows, 456 columns and 2223 nonzeros
Variable types: 256 continuous, 200 integer (200 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 9e+05]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -1626441.383
Presolve removed 328 rows and 399 columns
Presolve time: 0.01s
Presolved: 71 rows, 57 columns, 195 nonzeros
Found heuristic solution: objective -896678.0489
Variable types: 0 continuous, 57 integer (57 binary)

Root relaxation: objective -7.406143e+05, 5 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0    -740614.3223 -740614.32  0.00%     -    0s

Explored 0 nodes (5 simplex iterations) in 0.01 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -740614 -896678 

Optimal solution found (tolerance 1.00e-04)
Best objective -7.406143222740e+05, best bound -7.406143222740e+05, gap 0.0000%
Took  0.06976532936096191
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 972 rows, 621 columns and 2497 nonzeros
Variable types: 504 continuous, 117 integer (117 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 972 rows and 621 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.00 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -740614 

Optimal solution found (tolerance 1.00e-04)
Best objective -7.406143222740e+05, best bound -7.406143222740e+05, gap 0.0000%

### Instance
### LSFDRP_1_3_0_0_fd_31

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 7
### |A|: 6
### |A^i|: 6
### |A^f|: 0
### |M|: 77
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is -740,614.3223 (-740614.3223)
###
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 68, 0]
###  |  8 (BEANR Out@570) - 68 (HNPCR In@1166/Out@1179) - 0 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [54, 112, 108, 67, 0]
###  |  54 (GBFXT Out@470) - 112 (USORF In@873/Out@881) - 108 (USMIA In@935/Out@943) - 67 (HNPCR In@998/Out@1011) - 0 (DUMMY_
###  |  END Ziel-Service 121)
###  |   |- and carried Demand 54,  10.0 containers from 108 to 67
###  |   |- and carried Demand 55,  75.0 containers from 112 to 67
###  |   |- and carried Demand 56, 114.0 containers from 112 to 67
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m0.624s
user	0m0.220s
sys	0m0.033s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
