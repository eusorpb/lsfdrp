	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_1_fr.p
Instance LSFDP: LSFDRP_1_4_0_1_fd_49.p

This object contains the information regarding the instance 1_4_0_1 for a duration of 49 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.045206 sec.
***  |   |- Create additional vessel information,				took  0.00029 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 5483 rows, 18170 columns and 114272 nonzeros
Variable types: 4754 continuous, 13416 integer (13416 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [4e+01, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -4454723.096
Presolve removed 4620 rows and 12440 columns
Presolve time: 0.34s
Presolved: 863 rows, 5730 columns, 22759 nonzeros
Found heuristic solution: objective -3421907.317
Variable types: 0 continuous, 5730 integer (5684 binary)

Root relaxation: objective -6.439558e+05, 876 iterations, 0.04 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -643955.77    0  162 -3421907.3 -643955.77  81.2%     -    0s
H    0     0                    -1588212.197 -643955.77  59.5%     -    0s
H    0     0                    -1519986.386 -643955.77  57.6%     -    0s
H    0     0                    -1244931.015 -643955.77  48.3%     -    0s
H    0     0                    -1181266.436 -644107.20  45.5%     -    0s
H    0     0                    -1151911.337 -644107.20  44.1%     -    0s
     0     0 -855902.60    0  174 -1151911.3 -855902.60  25.7%     -    0s
H    0     0                    -1046809.963 -855902.60  18.2%     -    0s
     0     0 -857498.49    0  183 -1046810.0 -857498.49  18.1%     -    0s
     0     0 -883999.42    0  134 -1046810.0 -883999.42  15.6%     -    0s
     0     0 -883999.42    0  180 -1046810.0 -883999.42  15.6%     -    1s
     0     0 -978694.36    0   81 -1046810.0 -978694.36  6.51%     -    1s
     0     0 -985753.61    0  123 -1046810.0 -985753.61  5.83%     -    1s
     0     0 -989191.99    0  119 -1046810.0 -989191.99  5.50%     -    1s
     0     0 -1015031.2    0  112 -1046810.0 -1015031.2  3.04%     -    1s
     0     0 infeasible    0      -1046810.0 -1046810.0  0.00%     -    1s

Explored 1 nodes (2162 simplex iterations) in 1.16 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -1.04681e+06 -1.15191e+06 -1.18127e+06 ... -3.42191e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.046809963211e+06, best bound -1.046809963211e+06, gap 0.0000%
Took  1.819197416305542
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 2994 rows, 2658 columns and 11061 nonzeros
Variable types: 2151 continuous, 507 integer (507 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 2994 rows and 2658 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.01 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -1.04681e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -1.046809963211e+06, best bound -1.046809963211e+06, gap 0.0000%

### Instance
### LSFDRP_1_4_0_1_fd_49

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 28
### |A|: 27
### |A^i|: 27
### |A^f|: 0
### |M|: 75
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 215, 187, 188, 8, 0]
###  |  16 (BEANR Out@402) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA 
###  |  In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 217, 11, 0]
###  |  115 (GBFXT Out@302) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m4.223s
user	0m3.832s
sys	0m0.292s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
