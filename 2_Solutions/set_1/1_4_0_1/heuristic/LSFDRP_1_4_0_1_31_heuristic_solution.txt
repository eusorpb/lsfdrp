	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_1_fr.p
Instance LSFDP: LSFDRP_1_4_0_1_fd_31.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_4_0_1_fd_31.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.054795 sec.
***  |   |- Create additional vessel information,				took  0.00031 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.000236 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took 0.000217 sec.
***  |   |- Calculation finished,					took    45.75 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000362 sec.
***  |   |- Calculation finished,					took    43.91 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000536 sec.
***  |   |- Calculation finished,					took    45.53 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000251 sec.
***  |   |- Calculation finished,					took     44.8 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000535 sec.
***  |   |- Calculation finished,					took    46.87 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000536 sec.
***  |   |- Calculation finished,					took    45.38 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 217, 11, 0]
###  |  16 (BEANR Out@402) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA
###  |   In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 217, 11, 0]
###  |  16 (BEANR Out@402) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA
###  |   In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 217, 11, 0]
###  |  16 (BEANR Out@402) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA
###  |   In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 217, 11, 0]
###  |  16 (BEANR Out@402) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA
###  |   In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 217, 11, 0]
###  |  16 (BEANR Out@402) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA
###  |   In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66) - 213 (TRMER In@243/Out@287) - 135 (INNSA In@522/Out@546) - 4 (AEJEA In@617/Out@642) - 185 (OMSLL I
###  |  n@710/Out@722) - 186 (OMSLL In@721/Out@736) - 7 (AEJEA In@858/Out@891) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to  7
###  |   |- and carried Demand 62,  14.0 containers from 185 to  7
###  |   |- and carried Demand 63, 280.0 containers from 135 to  4
###  |
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234) - 214 (TRMER In@411/Out@455) - 5 (AEJEA In@690/Out@723) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [16, 217, 11, 0]
###  |  16 (BEANR Out@402) - 217 (TRMER In@915/Out@959) - 11 (AEJEA In@1530/Out@1563) - 0 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570) - 216 (TRMER In@747/Out@791) - 81 (EGPSD In@867/Out@886) - 82 (EGPSD In@957/Out@973) - 9 (AEJEA In
###  |  @1194/Out@1227) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from 81 to  9
###  |   |- and carried Demand 64, 467.0 containers from 82 to  9
###  |
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302) - 215 (TRMER In@579/Out@623) - 187 (OMSLL In@878/Out@890) - 188 (OMSLL In@889/Out@904) - 8 (AEJEA
###  |   In@1026/Out@1059) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to  8
###  |   |- and carried Demand 56,  35.0 containers from 187 to  8
###  |
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470) - 201 (TRALI In@836/Out@854) - 83 (EGPSD In@1035/Out@1054) - 10 (AEJEA In@1362/Out@1395) - 0 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from 83 to 10
###  |   |- and carried Demand 54,   7.0 containers from 83 to 10
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -1,046,809.9632 (-1046809.9632)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	4m36.019s
user	10m35.580s
sys	0m49.749s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
