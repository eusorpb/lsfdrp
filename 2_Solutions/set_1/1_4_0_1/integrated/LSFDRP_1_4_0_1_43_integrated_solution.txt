	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_1_fr.p
Instance LSFDP: LSFDRP_1_4_0_1_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_4_0_1_fd_43 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 221
*** |A|: 4463
*** |A^i|: 4463
*** |A^f|: 0
*** |M|: 75
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 30424 rows, 424452 columns and 1727894 nonzeros
Variable types: 344097 continuous, 80355 integer (80355 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [4e+01, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Presolve removed 26588 rows and 347571 columns (presolve time = 32s) ...
Presolve removed 26632 rows and 347671 columns
Presolve time: 33.06s
Presolved: 3792 rows, 76781 columns, 239415 nonzeros
Variable types: 416 continuous, 76365 integer (76330 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.5080538e+08   4.577502e+04   0.000000e+00     35s
    4496    1.8049816e+06   2.541029e+04   0.000000e+00     35s
   11857    3.3920096e+05   0.000000e+00   0.000000e+00     40s

Root relaxation: objective 3.392010e+05, 11857 iterations, 5.59 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 250076.745    0  171          - 250076.745      -     -   41s
H    0     0                    -1.89902e+08 250076.745   100%     -   42s
H    0     0                    -4.60437e+07 250076.745   101%     -   42s
H    0     0                    -1306952.758 250076.745   119%     -   44s
H    0     0                    -1046809.963 250076.745   124%     -   45s
     0     0 -661444.27    0   97 -1046810.0 -661444.27  36.8%     -   47s
     0     0 -661444.27    0   95 -1046810.0 -661444.27  36.8%     -   48s
     0     0 -845027.52    0   75 -1046810.0 -845027.52  19.3%     -   48s
     0     0 -861925.99    0   65 -1046810.0 -861925.99  17.7%     -   48s
     0     0 -879972.25    0   75 -1046810.0 -879972.25  15.9%     -   48s
     0     0 -908582.45    0   89 -1046810.0 -908582.45  13.2%     -   48s
     0     0 -908582.45    0   66 -1046810.0 -908582.45  13.2%     -   48s
H    0     0                    -1046809.961 -908582.45  13.2%     -   48s
H    0     0                    -1046809.842 -908582.45  13.2%     -   48s
     0     0 -908582.45    0   85 -1046809.8 -908582.45  13.2%     -   48s
     0     0 -926639.42    0   97 -1046809.8 -926639.42  11.5%     -   48s
     0     0 -950037.91    0   92 -1046809.8 -950037.91  9.24%     -   48s
     0     0 -951878.06    0   89 -1046809.8 -951878.06  9.07%     -   48s
     0     0 -952251.15    0   89 -1046809.8 -952251.15  9.03%     -   48s
     0     0 -955068.57    0   96 -1046809.8 -955068.57  8.76%     -   48s
     0     0 -955068.57    0   77 -1046809.8 -955068.57  8.76%     -   48s
     0     0 -955068.57    0   74 -1046809.8 -955068.57  8.76%     -   48s
     0     0 -960372.11    0   64 -1046809.8 -960372.11  8.26%     -   48s
     0     0 -992117.08    0   78 -1046809.8 -992117.08  5.22%     -   48s
     0     0 -1004186.9    0   42 -1046809.8 -1004186.9  4.07%     -   48s
     0     0 -1015621.8    0   62 -1046809.8 -1015621.8  2.98%     -   48s
     0     0 -1015621.8    0   45 -1046809.8 -1015621.8  2.98%     -   48s
     0     0 -1035184.4    0   34 -1046809.8 -1035184.4  1.11%     -   48s
     0     0 -1040403.0    0   15 -1046809.8 -1040403.0  0.61%     -   48s
     0     0 -1045984.1    0   22 -1046809.8 -1045984.1  0.08%     -   48s

Cutting planes:
  Gomory: 5
  Implied bound: 2
  MIR: 1
  Flow cover: 1

Explored 1 nodes (20029 simplex iterations) in 49.54 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -1.04681e+06 -1.04681e+06 -1.04681e+06 ... -1.89902e+08

Optimal solution found (tolerance 1.00e-04)
Best objective -1.046809963211e+06, best bound -1.046809841810e+06, gap 0.0000%

***
***  |- Calculation finished, took 49.55 (49.55) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632) and is feasible with a Gap of 0.0
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66.0) - 213 (TRMER In@243.0/Out@287.0) - 135 (INNSA In@522.0/Out@546.0) - 4 (AEJEA In@617.0/Out@642.0) 
###  |  - 185 (OMSLL In@710.0/Out@722.0) - 186 (OMSLL In@721.0/Out@736.0) - 7 (AEJEA In@858.0/Out@891.0) - 0 (DUMMY_END Ziel-
###  |  Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to   7
###  |   |- and carried Demand 62,  14.0 containers from 185 to   7
###  |   |- and carried Demand 63, 280.0 containers from 135 to   4
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234.0) - 214 (TRMER In@411.0/Out@455.0) - 5 (AEJEA In@690.0/Out@723.0) - 0 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [16, 218, 11, 0]
###  |  16 (BEANR Out@402.0) - 218 (TRMER In@1083.0/Out@1127.0) - 11 (AEJEA In@1530.0/Out@1563.0) - 0 (DUMMY_END Ziel-Service
###  |   7)
###  |- Vessel 12 has the path [19, 216, 81, 82, 9, 0]
###  |  19 (BEANR Out@570.0) - 216 (TRMER In@747.0/Out@791.0) - 81 (EGPSD In@867.0/Out@886.0) - 82 (EGPSD In@957.0/Out@973.0)
###  |   - 9 (AEJEA In@1194.0/Out@1227.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from  81 to   9
###  |   |- and carried Demand 64, 467.0 containers from  82 to   9
###  |- Vessel 17 has the path [115, 215, 187, 188, 8, 0]
###  |  115 (GBFXT Out@302.0) - 215 (TRMER In@579.0/Out@623.0) - 187 (OMSLL In@878.0/Out@890.0) - 188 (OMSLL In@889.0/Out@904
###  |  .0) - 8 (AEJEA In@1026.0/Out@1059.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to   8
###  |   |- and carried Demand 56,  35.0 containers from 187 to   8
###  |- Vessel 18 has the path [118, 201, 83, 10, 0]
###  |  118 (GBFXT Out@470.0) - 201 (TRALI In@836.0/Out@854.0) - 83 (EGPSD In@1035.0/Out@1054.0) - 10 (AEJEA In@1362.0/Out@13
###  |  95.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from  83 to  10
###  |   |- and carried Demand 54,   7.0 containers from  83 to  10
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
y[9,4,185] = 1
y[9,185,186] = 1
y[9,135,4] = 1
y[9,186,7] = 1
y[9,7,0] = 1
y[9,213,135] = 1
y[9,12,213] = 1
y[10,214,5] = 1
y[10,13,214] = 1
y[10,5,0] = 1
y[11,218,11] = 1
y[11,16,218] = 1
y[11,11,0] = 1
y[12,9,0] = 1
y[12,19,216] = 1
y[12,216,81] = 1
y[12,82,9] = 1
y[12,81,82] = 1
y[17,115,215] = 1
y[17,187,188] = 1
y[17,8,0] = 1
y[17,215,187] = 1
y[17,188,8] = 1
y[18,118,201] = 1
y[18,201,83] = 1
y[18,83,10] = 1
y[18,10,0] = 1
xD[53,83,10] = 188
xD[54,83,10] = 7
xD[55,187,188] = 111
xD[55,188,8] = 111
xD[56,187,188] = 35
xD[56,188,8] = 35
xD[60,82,9] = 467
xD[60,81,82] = 467
xD[61,185,186] = 288
xD[61,186,7] = 288
xD[62,185,186] = 14
xD[62,186,7] = 14
xD[63,135,4] = 280
xD[64,82,9] = 467
zE[4] = 617
zE[5] = 690
zE[7] = 858
zE[8] = 1026
zE[9] = 1194
zE[10] = 1362
zE[11] = 1530
zE[81] = 867
zE[82] = 957
zE[83] = 1035
zE[135] = 522
zE[185] = 710
zE[186] = 721
zE[187] = 878
zE[188] = 889
zE[201] = 836
zE[213] = 243
zE[214] = 411
zE[215] = 579
zE[216] = 747
zE[218] = 1083
zX[4] = 642
zX[5] = 723
zX[7] = 891
zX[8] = 1059
zX[9] = 1227
zX[10] = 1395
zX[11] = 1563
zX[12] = 66
zX[13] = 234
zX[16] = 402
zX[19] = 570
zX[81] = 886
zX[82] = 973
zX[83] = 1054
zX[115] = 302
zX[118] = 470
zX[135] = 546
zX[185] = 722
zX[186] = 736
zX[187] = 890
zX[188] = 904
zX[201] = 854
zX[213] = 287
zX[214] = 455
zX[215] = 623
zX[216] = 791
zX[218] = 1127
phi[9] = 825
phi[10] = 489
phi[11] = 1161
phi[12] = 657
phi[17] = 757
phi[18] = 925

### All variables with value smaller zero:

### End

real	1m13.298s
user	1m29.726s
sys	0m2.713s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
