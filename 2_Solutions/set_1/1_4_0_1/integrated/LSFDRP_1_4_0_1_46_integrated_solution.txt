	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_4_0_1_fr.p
Instance LSFDP: LSFDRP_1_4_0_1_fd_46.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_4_0_1_fd_46 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 221
*** |A|: 4463
*** |A^i|: 4463
*** |A^f|: 0
*** |M|: 75
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 30424 rows, 424452 columns and 1727894 nonzeros
Variable types: 344097 continuous, 80355 integer (80355 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [4e+01, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Presolve removed 26588 rows and 347571 columns (presolve time = 23s) ...
Presolve removed 26632 rows and 347671 columns
Presolve time: 23.48s
Presolved: 3792 rows, 76781 columns, 239415 nonzeros
Variable types: 416 continuous, 76365 integer (76330 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.5757897e+08   4.577502e+04   0.000000e+00     25s
    3554    2.8630328e+06   3.907935e+03   0.000000e+00     25s
   11322    3.3920097e+05   0.000000e+00   0.000000e+00     30s

Root relaxation: objective 3.392010e+05, 11322 iterations, 4.98 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 250076.745    0  170          - 250076.745      -     -   31s
H    0     0                    -2.02997e+08 250076.745   100%     -   32s
H    0     0                    -5115016.014 250076.745   105%     -   32s
H    0     0                    -1306952.758 250076.745   119%     -   35s
H    0     0                    -1046809.963 250076.745   124%     -   36s
     0     0 -671595.38    0   94 -1046810.0 -671595.38  35.8%     -   38s
     0     0 -671595.38    0   91 -1046810.0 -671595.38  35.8%     -   39s
     0     0 -851696.92    0   94 -1046810.0 -851696.92  18.6%     -   39s
     0     0 -887157.88    0   71 -1046810.0 -887157.88  15.3%     -   39s
     0     0 -890544.09    0   70 -1046810.0 -890544.09  14.9%     -   39s
     0     0 -892623.45    0   67 -1046810.0 -892623.45  14.7%     -   39s
     0     0 -912002.30    0   64 -1046810.0 -912002.30  12.9%     -   39s
     0     0 -927148.64    0   95 -1046810.0 -927148.64  11.4%     -   39s
     0     0 -927148.64    0   67 -1046810.0 -927148.64  11.4%     -   40s
H    0     0                    -1046809.845 -927148.64  11.4%     -   40s
H    0     0                    -1046809.844 -927148.64  11.4%     -   40s
     0     0 -927148.64    0   78 -1046809.8 -927148.64  11.4%     -   40s
     0     0 -927148.64    0   79 -1046809.8 -927148.64  11.4%     -   40s
     0     0 -927148.64    0   86 -1046809.8 -927148.64  11.4%     -   40s
     0     0 -937357.50    0  100 -1046809.8 -937357.50  10.5%     -   40s
H    0     0                    -1046809.787 -937357.50  10.5%     -   40s
     0     0 -937399.22    0  100 -1046809.8 -937399.22  10.5%     -   40s
     0     0 -947749.16    0  100 -1046809.8 -947749.16  9.46%     -   40s
     0     0 -947749.16    0   63 -1046809.8 -947749.16  9.46%     -   40s
     0     0 -947749.16    0   75 -1046809.8 -947749.16  9.46%     -   40s
     0     0 -947749.16    0   84 -1046809.8 -947749.16  9.46%     -   40s
     0     0 -947749.16    0   80 -1046809.8 -947749.16  9.46%     -   40s
     0     0 -967789.16    0   80 -1046809.8 -967789.16  7.55%     -   40s
     0     0 -974320.42    0   87 -1046809.8 -974320.42  6.92%     -   40s
     0     0 -982869.49    0   82 -1046809.8 -982869.49  6.11%     -   40s
     0     0 -982869.49    0   85 -1046809.8 -982869.49  6.11%     -   40s
     0     0 -998438.15    0   73 -1046809.8 -998438.15  4.62%     -   40s
     0     0 -998438.15    0   42 -1046809.8 -998438.15  4.62%     -   40s
     0     0 -998438.15    0   72 -1046809.8 -998438.15  4.62%     -   40s
     0     0 -1018279.0    0   71 -1046809.8 -1018279.0  2.73%     -   40s
     0     0 -1038753.5    0   44 -1046809.8 -1038753.5  0.77%     -   40s
     0     0 -1042150.7    0   68 -1046809.8 -1042150.7  0.45%     -   40s
     0     0 -1042150.7    0   23 -1046809.8 -1042150.7  0.45%     -   40s
     0     0 -1044664.8    0   21 -1046809.8 -1044664.8  0.20%     -   40s
     0     0     cutoff    0      -1046809.8 -1046809.8  0.00%     -   40s

Cutting planes:
  Gomory: 4
  Implied bound: 1
  MIR: 1

Explored 1 nodes (20271 simplex iterations) in 41.07 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -1.04681e+06 -1.04681e+06 -1.04681e+06 ... -2.02997e+08

Optimal solution found (tolerance 1.00e-04)
Best objective -1.046809963211e+06, best bound -1.046809786886e+06, gap 0.0000%

***
***  |- Calculation finished, took 41.08 (41.08) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,046,809.9632 (-1046809.9632) and is feasible with a Gap of 0.0
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [12, 213, 135, 4, 185, 186, 7, 0]
###  |  12 (BEANR Out@66.0) - 213 (TRMER In@243.0/Out@287.0) - 135 (INNSA In@522.0/Out@546.0) - 4 (AEJEA In@617.0/Out@642.0) 
###  |  - 185 (OMSLL In@710.0/Out@722.0) - 186 (OMSLL In@721.0/Out@736.0) - 7 (AEJEA In@858.0/Out@891.0) - 0 (DUMMY_END Ziel-
###  |  Service 7)
###  |   |- and carried Demand 61, 288.0 containers from 185 to   7
###  |   |- and carried Demand 62,  14.0 containers from 185 to   7
###  |   |- and carried Demand 63, 280.0 containers from 135 to   4
###  |- Vessel 10 has the path [13, 214, 5, 0]
###  |  13 (BEANR Out@234.0) - 214 (TRMER In@411.0/Out@455.0) - 5 (AEJEA In@690.0/Out@723.0) - 0 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [16, 215, 187, 188, 8, 0]
###  |  16 (BEANR Out@402.0) - 215 (TRMER In@579.0/Out@623.0) - 187 (OMSLL In@878.0/Out@890.0) - 188 (OMSLL In@889.0/Out@904.
###  |  0) - 8 (AEJEA In@1026.0/Out@1059.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 55, 111.0 containers from 187 to   8
###  |   |- and carried Demand 56,  35.0 containers from 187 to   8
###  |- Vessel 12 has the path [19, 216, 83, 10, 0]
###  |  19 (BEANR Out@570.0) - 216 (TRMER In@747.0/Out@791.0) - 83 (EGPSD In@1035.0/Out@1054.0) - 10 (AEJEA In@1362.0/Out@139
###  |  5.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 53, 188.0 containers from  83 to  10
###  |   |- and carried Demand 54,   7.0 containers from  83 to  10
###  |- Vessel 17 has the path [115, 200, 81, 82, 9, 0]
###  |  115 (GBFXT Out@302.0) - 200 (TRALI In@668.0/Out@686.0) - 81 (EGPSD In@867.0/Out@886.0) - 82 (EGPSD In@957.0/Out@973.0
###  |  ) - 9 (AEJEA In@1194.0/Out@1227.0) - 0 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 60, 467.0 containers from  81 to   9
###  |   |- and carried Demand 64, 467.0 containers from  82 to   9
###  |- Vessel 18 has the path [118, 218, 11, 0]
###  |  118 (GBFXT Out@470.0) - 218 (TRMER In@1083.0/Out@1127.0) - 11 (AEJEA In@1530.0/Out@1563.0) - 0 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
y[9,4,185] = 1
y[9,185,186] = 1
y[9,135,4] = 1
y[9,186,7] = 1
y[9,7,0] = 1
y[9,213,135] = 1
y[9,12,213] = 1
y[10,214,5] = 1
y[10,13,214] = 1
y[10,5,0] = 1
y[11,16,215] = 1
y[11,187,188] = 1
y[11,8,0] = 1
y[11,215,187] = 1
y[11,188,8] = 1
y[12,19,216] = 1
y[12,83,10] = 1
y[12,10,0] = 1
y[12,216,83] = 1
y[17,9,0] = 1
y[17,115,200] = 1
y[17,82,9] = 1
y[17,81,82] = 1
y[17,200,81] = 1
y[18,218,11] = 1
y[18,11,0] = 1
y[18,118,218] = 1
xD[53,83,10] = 188
xD[54,83,10] = 7
xD[55,187,188] = 111
xD[55,188,8] = 111
xD[56,187,188] = 35
xD[56,188,8] = 35
xD[60,82,9] = 467
xD[60,81,82] = 467
xD[61,185,186] = 288
xD[61,186,7] = 288
xD[62,185,186] = 14
xD[62,186,7] = 14
xD[63,135,4] = 280
xD[64,82,9] = 467
zE[4] = 617
zE[5] = 690
zE[7] = 858
zE[8] = 1026
zE[9] = 1194
zE[10] = 1362
zE[11] = 1530
zE[81] = 867
zE[82] = 957
zE[83] = 1035
zE[135] = 522
zE[185] = 710
zE[186] = 721
zE[187] = 878
zE[188] = 889
zE[200] = 668
zE[213] = 243
zE[214] = 411
zE[215] = 579
zE[216] = 747
zE[218] = 1083
zX[4] = 642
zX[5] = 723
zX[7] = 891
zX[8] = 1059
zX[9] = 1227
zX[10] = 1395
zX[11] = 1563
zX[12] = 66
zX[13] = 234
zX[16] = 402
zX[19] = 570
zX[81] = 886
zX[82] = 973
zX[83] = 1054
zX[115] = 302
zX[118] = 470
zX[135] = 546
zX[185] = 722
zX[186] = 736
zX[187] = 890
zX[188] = 904
zX[200] = 686
zX[213] = 287
zX[214] = 455
zX[215] = 623
zX[216] = 791
zX[218] = 1127
phi[9] = 825
phi[10] = 489
phi[11] = 657
phi[12] = 825
phi[17] = 925
phi[18] = 1093

### All variables with value smaller zero:

### End

real	1m3.617s
user	1m21.840s
sys	0m2.780s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
