	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_3_0_1_fr.p
Instance LSFDP: LSFDRP_1_3_0_1_fd_43.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 30 rows, 27 columns and 72 nonzeros
Variable types: 0 continuous, 27 integer (27 binary)
Coefficient statistics:
  Matrix range     [1e+00, 2e+00]
  Objective range  [1e+07, 9e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 2e+00]
Found heuristic solution: objective -1.56074e+07
Presolve removed 30 rows and 27 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.01 seconds
Thread count was 1 (of 16 available processors)

Solution count 2: -0 -1.56074e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective -0.000000000000e+00, best bound -0.000000000000e+00, gap 0.0000%

### Objective value in 1k: -0.00
### Objective value: -0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 121(SAE): 30, (PMax35), 2
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[121,30] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[11,121] = 1
y[18,121] = 1

### All variables with value zero:
rho[121,18] = 0
rho[121,19] = 0
eta[121,18,1] = 0
eta[121,18,2] = 0
eta[121,19,1] = 0
eta[121,19,2] = 0
y[1,121] = 0
y[2,121] = 0
y[3,121] = 0
y[4,121] = 0
y[5,121] = 0
y[6,121] = 0
y[7,121] = 0
y[8,121] = 0
y[9,121] = 0
y[10,121] = 0
y[12,121] = 0
y[13,121] = 0
y[14,121] = 0
y[15,121] = 0
y[16,121] = 0
y[17,121] = 0

### End
Warning: Failed to open log file 'gurobi.log'

real	0m1.093s
user	0m0.076s
sys	0m0.033s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
