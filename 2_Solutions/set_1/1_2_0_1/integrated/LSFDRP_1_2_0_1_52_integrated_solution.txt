	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_2_0_1_fr.p
Instance LSFDP: LSFDRP_1_2_0_1_fd_52.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_2_0_1_fd_52 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 112
*** |A|: 849
*** |A^i|: 849
*** |A^f|: 0
*** |M|: 45
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 9143 rows, 55432 columns and 242008 nonzeros
Variable types: 40135 continuous, 15297 integer (15297 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Presolve removed 7536 rows and 46953 columns
Presolve time: 4.38s
Presolved: 1607 rows, 8479 columns, 36766 nonzeros
Variable types: 805 continuous, 7674 integer (7650 binary)
Warning: Failed to open log file 'gurobi.log'

Root relaxation: objective 4.303583e+06, 3833 iterations, 0.23 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 4303583.34    0   57          - 4303583.34      -     -    4s
H    0     0                    -2.26571e+07 4303583.34   119%     -    4s
H    0     0                    -1.13806e+07 4303583.34   138%     -    4s
H    0     0                    1092979.0148 4303583.34   294%     -    4s
     0     0 1810053.68    0   14 1092979.01 1810053.68  65.6%     -    5s
H    0     0                    1579463.1535 1810053.68  14.6%     -    5s
     0     0 1810053.68    0    5 1579463.15 1810053.68  14.6%     -    5s
H    0     1                    1764567.4262 1810053.68  2.58%     -    5s

Explored 1 nodes (4374 simplex iterations) in 5.17 seconds
Thread count was 16 (of 16 available processors)

Solution count 5: 1.76457e+06 1.57946e+06 1.09298e+06 ... -2.26571e+07

Optimal solution found (tolerance 1.00e-04)
Best objective 1.764567426185e+06, best bound 1.764567426185e+06, gap 0.0000%

***
***  |- Calculation finished, took 5.18 (5.18) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262) and is feasible with a Gap of 0.0
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288.0) - 37 (ESALG In@496.0/Out@515.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456.0) - 39 (ESALG In@664.0/Out@683.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220.0) - 34 (ESALG In@328.0/Out@347.0) - 83 (MAPTM In@375.0/Out@396.0) - 84 (MAPTM In@379.0/Out@398.0) 
###  |  - 85 (MAPTM In@406.0/Out@427.0) - 36 (ESALG In@452.0/Out@464.0) - 41 (ESALG In@832.0/Out@851.0) - 0 (DUMMY_END Ziel-S
###  |  ervice 24)
###  |   |- and carried Demand 11, 214.0 containers from  83 to  41
###  |   |- and carried Demand 12,  10.0 containers from  83 to  41
###  |   |- and carried Demand 13, 484.0 containers from  83 to  36
###  |   |- and carried Demand 14,  18.0 containers from  83 to  36
###  |   |- and carried Demand 28, 214.0 containers from  85 to  41
###  |   |- and carried Demand 29,  10.0 containers from  85 to  41
###  |   |- and carried Demand 30, 484.0 containers from  85 to  36
###  |   |- and carried Demand 31,  18.0 containers from  85 to  36
###  |   |- and carried Demand 37, 214.0 containers from  84 to  41
###  |   |- and carried Demand 38,  10.0 containers from  84 to  41
###  |   |- and carried Demand 39, 484.0 containers from  84 to  36
###  |   |- and carried Demand 40,  18.0 containers from  84 to  36
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[1,25,37] = 1
y[1,37,0] = 1
y[2,39,0] = 1
y[2,26,39] = 1
y[13,36,41] = 1
y[13,34,83] = 1
y[13,84,85] = 1
y[13,83,84] = 1
y[13,41,0] = 1
y[13,29,34] = 1
y[13,85,36] = 1
xD[11,36,41] = 214
xD[11,84,85] = 214
xD[11,83,84] = 214
xD[11,85,36] = 214
xD[12,36,41] = 10
xD[12,84,85] = 10
xD[12,83,84] = 10
xD[12,85,36] = 10
xD[13,84,85] = 484
xD[13,83,84] = 484
xD[13,85,36] = 484
xD[14,84,85] = 18
xD[14,83,84] = 18
xD[14,85,36] = 18
xD[28,36,41] = 214
xD[28,85,36] = 214
xD[29,36,41] = 10
xD[29,85,36] = 10
xD[30,85,36] = 484
xD[31,85,36] = 18
xD[37,36,41] = 214
xD[37,84,85] = 214
xD[37,85,36] = 214
xD[38,36,41] = 10
xD[38,84,85] = 10
xD[38,85,36] = 10
xD[39,84,85] = 484
xD[39,85,36] = 484
xD[40,84,85] = 18
xD[40,85,36] = 18
zE[34] = 328
zE[36] = 452
zE[37] = 496
zE[39] = 664
zE[41] = 832
zE[83] = 375
zE[84] = 379
zE[85] = 406
zX[25] = 288
zX[26] = 456
zX[29] = 220
zX[34] = 347
zX[36] = 464
zX[37] = 515
zX[39] = 683
zX[41] = 851
zX[83] = 396
zX[84] = 398
zX[85] = 427
phi[1] = 227
phi[2] = 227
phi[13] = 631

### All variables with value smaller zero:

### End

real	0m8.586s
user	0m9.178s
sys	0m0.473s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
