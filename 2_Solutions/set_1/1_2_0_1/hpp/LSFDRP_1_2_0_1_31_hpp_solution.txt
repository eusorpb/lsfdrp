	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_2_0_1_fr.p
Instance LSFDP: LSFDRP_1_2_0_1_fd_31.p

This object contains the information regarding the instance 1_2_0_1 for a duration of 31 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.016488 sec.
***  |   |- Create additional vessel information,				took    9e-05 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 1321 rows, 1905 columns and 12678 nonzeros
Variable types: 843 continuous, 1062 integer (1062 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [1e+02, 7e+05]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Found heuristic solution: objective -1805272.762
Presolve removed 1152 rows and 1715 columns
Presolve time: 0.04s
Presolved: 169 rows, 190 columns, 1472 nonzeros
Found heuristic solution: objective -345397.7163
Variable types: 20 continuous, 170 integer (148 binary)

Root relaxation: objective 1.764567e+06, 100 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0    1764567.4262 1764567.43  0.00%     -    0s

Explored 0 nodes (100 simplex iterations) in 0.05 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: 1.76457e+06 -345398 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.764567426168e+06, best bound 1.764567426168e+06, gap 0.0000%
Took  0.16674304008483887
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 1031 rows, 770 columns and 3639 nonzeros
Variable types: 557 continuous, 213 integer (213 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 1031 rows and 770 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.00 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 1.76457e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.764567426168e+06, best bound 1.764567426168e+06, gap 0.0000%

### Instance
### LSFDRP_1_2_0_1_fd_31

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 12
### |A|: 11
### |A^i|: 11
### |A^f|: 0
### |M|: 45
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [25, 37, 0]
###  |  25 (DEHAM Out@288) - 37 (ESALG In@496/Out@515) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [26, 39, 0]
###  |  26 (DEHAM Out@456) - 39 (ESALG In@664/Out@683) - 0 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [29, 34, 83, 84, 85, 36, 41, 0]
###  |  29 (DKAAR Out@220) - 34 (ESALG In@328/Out@347) - 83 (MAPTM In@375/Out@396) - 84 (MAPTM In@379/Out@398) - 85 (MAPTM In
###  |  @406/Out@427) - 36 (ESALG In@452/Out@464) - 41 (ESALG In@832/Out@851) - 0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 11, 214.0 containers from 83 to 41
###  |   |- and carried Demand 12,  10.0 containers from 83 to 41
###  |   |- and carried Demand 13, 484.0 containers from 83 to 36
###  |   |- and carried Demand 14,  18.0 containers from 83 to 36
###  |   |- and carried Demand 28, 214.0 containers from 85 to 41
###  |   |- and carried Demand 29,  10.0 containers from 85 to 41
###  |   |- and carried Demand 30, 484.0 containers from 85 to 36
###  |   |- and carried Demand 31,  18.0 containers from 85 to 36
###  |   |- and carried Demand 37, 214.0 containers from 84 to 41
###  |   |- and carried Demand 38,  10.0 containers from 84 to 41
###  |   |- and carried Demand 39, 484.0 containers from 84 to 36
###  |   |- and carried Demand 40,  18.0 containers from 84 to 36
###  |
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m2.056s
user	0m0.327s
sys	0m0.047s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
