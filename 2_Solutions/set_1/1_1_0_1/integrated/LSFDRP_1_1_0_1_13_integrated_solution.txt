	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_1_fr.p
Instance LSFDP: LSFDRP_1_1_0_1_fd_13.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_1_fd_13 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 7e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35970 rows and 852685 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 53s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959201 columns (presolve time = 58s) ...
Presolve removed 46359 rows and 959201 columns
Presolve time: 57.93s
Presolved: 9214 rows, 49117 columns, 375500 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9931318e+08   7.349973e+04   0.000000e+00     59s
    2809   -9.0614691e+05   0.000000e+00   0.000000e+00     59s

Root relaxation: objective -9.061469e+05, 2809 iterations, 0.41 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -906146.91    0   41          - -906146.91      -     -   59s
H    0     0                    -3414159.462 -906146.91  73.5%     -   59s
H    0     0                    -2765081.804 -906146.91  67.2%     -   59s
H    0     0                    -1964311.428 -906146.91  53.9%     -   59s
H    0     0                    -1876028.928 -906146.91  51.7%     -   59s
     0     0 -1275274.8    0   66 -1876028.9 -1275274.8  32.0%     -   60s
     0     0 -1275274.8    0   67 -1876028.9 -1275274.8  32.0%     -   63s
     0     0 -1602137.4    0   68 -1876028.9 -1602137.4  14.6%     -   63s
     0     0 -1615963.3    0   54 -1876028.9 -1615963.3  13.9%     -   64s
     0     0 -1655040.2    0   27 -1876028.9 -1655040.2  11.8%     -   65s
H    0     0                    -1821789.867 -1655040.2  9.15%     -   65s
H    0     0                    -1760743.307 -1655040.2  6.00%     -   65s
     0     0 -1750337.0    0    8 -1760743.3 -1750337.0  0.59%     -   66s
*    0     0               0    -1750336.987 -1750337.0  0.00%     -   66s

Explored 1 nodes (7522 simplex iterations) in 66.27 seconds
Thread count was 16 (of 16 available processors)

Solution count 7: -1.75034e+06 -1.76074e+06 -1.82179e+06 ... -3.41416e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986697e+06, best bound -1.750336986697e+06, gap 0.0000%

***
***  |- Calculation finished, took 66.29 (66.29) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9867 (-1750336.9867) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 240 (PECLL In@1398.71/Out@1400.71) - 236 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel 16 has the path [136, 202, 235, 0]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,234,0] = 1
y[8,204,234] = 1
y[8,223,138] = 1
y[8,138,204] = 1
y[15,200,112] = 1
y[15,240,236] = 1
y[15,112,36] = 1
y[15,36,240] = 1
y[15,236,0] = 1
y[15,134,200] = 1
y[16,202,235] = 1
y[16,136,202] = 1
y[16,235,0] = 1
xD[46,200,112] = 580
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[36] = 1240
zE[112] = 284
zE[138] = 615
zE[200] = 238
zE[202] = 406
zE[204] = 711
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1398.71
zX[36] = 1258
zX[112] = 296
zX[134] = 158
zX[136] = 326
zX[138] = 631
zX[200] = 259
zX[202] = 427
zX[204] = 732
zX[223] = 596
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1400.71
w[15,(240,236)] = 125.884
w[15,(36,240)] = 140.711
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	2m11.537s
user	3m20.598s
sys	0m17.963s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
