	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_1_fr.p
Instance LSFDP: LSFDRP_1_1_0_1_fd_28.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_1_fd_28 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35966 rows and 852685 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 73s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959196 columns (presolve time = 75s) ...
Presolve removed 46359 rows and 959201 columns
Presolve time: 79.54s
Presolved: 9214 rows, 49117 columns, 375500 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9825099e+08   7.321791e+04   0.000000e+00     81s
    2821   -9.0614691e+05   0.000000e+00   0.000000e+00     81s

Root relaxation: objective -9.061469e+05, 2821 iterations, 0.48 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -906146.91    0   41          - -906146.91      -     -   81s
H    0     0                    -3125319.865 -906146.91  71.0%     -   81s
H    0     0                    -1964311.428 -906146.91  53.9%     -   81s
H    0     0                    -1855400.048 -906146.91  51.2%     -   81s
     0     0 -1346206.7    0   55 -1855400.0 -1346206.7  27.4%     -   82s
     0     0 -1558763.6    0   60 -1855400.0 -1558763.6  16.0%     -   85s
H    0     0                    -1791525.867 -1558763.6  13.0%     -   85s
H    0     0                    -1791525.866 -1558763.6  13.0%     -   85s
     0     0 -1644620.6    0   61 -1791525.9 -1644620.6  8.20%     -   86s
*    0     0               0    -1750336.986 -1750337.0  0.00%     -   86s

Explored 1 nodes (6472 simplex iterations) in 87.01 seconds
Thread count was 16 (of 16 available processors)

Solution count 7: -1.75034e+06 -1.79153e+06 -1.79153e+06 ... -3.12532e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986103e+06, best bound -1.750336986103e+06, gap 0.0000%

***
***  |- Calculation finished, took 87.04 (87.04) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9861 (-1750336.9861) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 240 (PECLL In@1512.12/Out@1514.12) - 236 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel 16 has the path [136, 202, 235, 0]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,234,0] = 1
y[8,204,234] = 1
y[8,223,138] = 1
y[8,138,204] = 1
y[15,200,112] = 1
y[15,240,236] = 1
y[15,112,36] = 1
y[15,36,240] = 1
y[15,236,0] = 1
y[15,134,200] = 1
y[16,202,235] = 1
y[16,136,202] = 1
y[16,235,0] = 1
xD[46,200,112] = 580
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[36] = 1240
zE[112] = 284
zE[138] = 615
zE[200] = 238
zE[202] = 406
zE[204] = 711
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1512.12
zX[36] = 1258
zX[112] = 296
zX[134] = 158
zX[136] = 326
zX[138] = 631
zX[200] = 259
zX[202] = 427
zX[204] = 732
zX[223] = 596
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1514.12
w[15,(240,236)] = 125.884
w[15,(36,240)] = 140.711
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	2m27.945s
user	3m22.791s
sys	0m14.510s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
