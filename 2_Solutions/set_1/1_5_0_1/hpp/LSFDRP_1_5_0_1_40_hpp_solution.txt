	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_1_fr.p
Instance LSFDP: LSFDRP_1_5_0_1_fd_40.p

This object contains the information regarding the instance 1_5_0_1 for a duration of 40 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.130457 sec.
***  |   |- Create additional vessel information,				took 0.000439 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 8525 rows, 24336 columns and 196010 nonzeros
Variable types: 6876 continuous, 17460 integer (17460 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [4e+01, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 6620 rows and 16440 columns
Presolve time: 0.44s
Presolved: 1905 rows, 7896 columns, 33129 nonzeros
Variable types: 134 continuous, 7762 integer (7734 binary)

Root relaxation: objective 3.860507e+06, 1960 iterations, 0.15 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 3121692.11    0  324          - 3121692.11      -     -    0s
H    0     0                    -1740289.118 3121692.11   279%     -    0s
H    0     0                    -271447.8587 3121692.11  1250%     -    0s
     0     0 2557373.76    0  371 -271447.86 2557373.76  1042%     -    1s
     0     0 2556993.44    0  368 -271447.86 2556993.44  1042%     -    1s
     0     0 2556884.76    0  376 -271447.86 2556884.76  1042%     -    1s
     0     0 2422791.55    0  387 -271447.86 2422791.55   993%     -    1s
     0     0 2225235.90    0  355 -271447.86 2225235.90   920%     -    1s
H    0     0                    -261828.5153 2225235.90   950%     -    1s
     0     0 2225162.17    0  358 -261828.52 2225162.17   950%     -    1s
     0     0 1448748.49    0  344 -261828.52 1448748.49   653%     -    1s
     0     0 1448748.49    0  352 -261828.52 1448748.49   653%     -    1s
     0     0 1408311.89    0  359 -261828.52 1408311.89   638%     -    1s
     0     0 1408311.89    0  359 -261828.52 1408311.89   638%     -    2s
     0     0 1408311.89    0  318 -261828.52 1408311.89   638%     -    2s
     0     0 1408311.89    0  350 -261828.52 1408311.89   638%     -    2s
     0     0 1408311.89    0  394 -261828.52 1408311.89   638%     -    2s
     0     0 1408311.89    0  350 -261828.52 1408311.89   638%     -    2s
H    0     0                    -193110.0159 1408311.89   829%     -    2s
     0     0 1408311.89    0  297 -193110.02 1408311.89   829%     -    2s
     0     0 1408311.89    0  158 -193110.02 1408311.89   829%     -    3s
     0     0 1408311.89    0  322 -193110.02 1408311.89   829%     -    3s
     0     0 1408311.89    0  288 -193110.02 1408311.89   829%     -    3s
     0     0 1408311.89    0  301 -193110.02 1408311.89   829%     -    3s
     0     0 1408311.89    0  292 -193110.02 1408311.89   829%     -    3s
     0     0 1408311.89    0  322 -193110.02 1408311.89   829%     -    3s
     0     0 1408311.89    0  321 -193110.02 1408311.89   829%     -    3s
     0     2 1408311.89    0  320 -193110.02 1408311.89   829%     -    3s

Cutting planes:
  Gomory: 8
  Clique: 18
  MIR: 2
  Zero half: 11

Explored 135 nodes (13346 simplex iterations) in 4.05 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -193110 -261829 -271448 -1.74029e+06 
No other solutions better than -193110

Optimal solution found (tolerance 1.00e-04)
Best objective -1.931100159500e+05, best bound -1.931100159500e+05, gap 0.0000%
Took  5.010387182235718
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 8732 rows, 8025 columns and 28859 nonzeros
Variable types: 7410 continuous, 615 integer (615 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 2e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 8732 rows and 8025 columns
Presolve time: 0.01s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.01 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -193110 

Optimal solution found (tolerance 1.00e-04)
Best objective -1.931100159500e+05, best bound -1.931100159500e+05, gap 0.0000%

### Instance
### LSFDRP_1_5_0_1_fd_40

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 34
### |A|: 33
### |A^i|: 33
### |A^f|: 0
### |M|: 220
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m6.750s
user	0m12.969s
sys	0m1.152s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
