	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_1_fr.p
Instance LSFDP: LSFDRP_1_5_0_1_fd_25.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_5_0_1_fd_25.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.139067 sec.
***  |   |- Create additional vessel information,				took  0.00047 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took    9e-05 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took  7.3e-05 sec.
***  |   |- Calculation finished,					took    30.48 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000175 sec.
***  |   |- Calculation finished,					took    29.84 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000285 sec.
***  |   |- Calculation finished,					took    28.24 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000271 sec.
***  |   |- Calculation finished,					took    28.36 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000182 sec.
***  |   |- Calculation finished,					took    27.34 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000216 sec.
***  |   |- Calculation finished,					took    27.57 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -193,110.016 (-193110.016)
###
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 2, 227, 223, 6, 231, 160, 0]
###  |  9 (BEANR Out@66) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL In@889/Out@904) - 6 (AEJEA In@1
###  |  235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 10 has the path [10, 247, 156, 0]
###  |  10 (BEANR Out@234) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 244, 4, 229, 224, 7, 232, 161, 0]
###  |  13 (BEANR Out@402) - 244 (TRALI In@668/Out@686) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 224 (OMSLL
###  |   In@1132/Out@1145) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA In@1578/Out@1594) - 0 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel 12 has the path [16, 245, 5, 230, 159, 0]
###  |  16 (BEANR Out@570) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (IN
###  |  NSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 17 has the path [132, 210, 254, 3, 228, 157, 0]
###  |  132 (GBFXT Out@302) - 210 (NLRTM In@332/Out@344) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM
###  |   In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel 18 has the path [135, 212, 255, 158, 0]
###  |  135 (GBFXT Out@470) - 212 (NLRTM In@500/Out@512) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 13, 14, 15, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -193,110.016 (-193110.016)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	2m59.074s
user	6m47.442s
sys	0m31.125s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
