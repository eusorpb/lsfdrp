	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_0_fr.p
Instance LSFDP: LSFDRP_1_5_0_0_fd_13.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 42 rows, 39 columns and 144 nonzeros
Variable types: 0 continuous, 39 integer (39 binary)
Coefficient statistics:
  Matrix range     [1e+00, 6e+00]
  Objective range  [3e+05, 7e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Found heuristic solution: objective -1108227.423
Presolve removed 42 rows and 39 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.00 seconds
Thread count was 1 (of 16 available processors)

Solution count 2: -0 -1.10823e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -0.000000000000e+00, best bound -0.000000000000e+00, gap 0.0000%

### Objective value in 1k: -0.00
### Objective value: -0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 81(MECL1): 18, (PMax25), 6
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[81,18] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
y[1,81] = 1
y[2,81] = 1
y[3,81] = 1
y[4,81] = 1
y[13,81] = 1
y[14,81] = 1

### All variables with value zero:
rho[81,19] = 0
rho[81,30] = 0
eta[81,19,1] = 0
eta[81,19,2] = 0
eta[81,19,3] = 0
eta[81,19,4] = 0
eta[81,19,5] = 0
eta[81,19,6] = 0
eta[81,30,1] = 0
eta[81,30,2] = 0
eta[81,30,3] = 0
eta[81,30,4] = 0
eta[81,30,5] = 0
eta[81,30,6] = 0
y[5,81] = 0
y[6,81] = 0
y[7,81] = 0
y[8,81] = 0
y[9,81] = 0
y[10,81] = 0
y[11,81] = 0
y[12,81] = 0
y[15,81] = 0
y[16,81] = 0
y[17,81] = 0
y[18,81] = 0

### End
Warning: Failed to open log file 'gurobi.log'

real	0m0.650s
user	0m0.125s
sys	0m0.033s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
