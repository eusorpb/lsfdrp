	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_0_fr.p
Instance LSFDP: LSFDRP_1_5_0_0_fd_16.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_5_0_0_fd_16.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took  0.14057 sec.
***  |   |- Create additional vessel information,				took 0.000473 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took  8.7e-05 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took  7.2e-05 sec.
***  |   |- Calculation finished,					took    28.63 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000241 sec.
***  |   |- Calculation finished,					took    28.15 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000341 sec.
***  |   |- Calculation finished,					took    30.04 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took  9.7e-05 sec.
***  |   |- Calculation finished,					took    28.58 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took   0.0001 sec.
***  |   |- Calculation finished,					took    28.72 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took  9.8e-05 sec.
***  |   |- Calculation finished,					took     28.6 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -320,514.7412 (-320514.7412)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -320,514.7412 (-320514.7412)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -320,514.7412 (-320514.7412)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -320,514.7412 (-320514.7412)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -320,514.7412 (-320514.7412)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -320,514.7412 (-320514.7412)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -320,514.7412 (-320514.7412)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	2m59.821s
user	6m49.975s
sys	0m30.853s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
