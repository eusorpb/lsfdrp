	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_0_fr.p
Instance LSFDP: LSFDRP_1_5_0_0_fd_52.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_5_0_0_fd_52.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.140156 sec.
***  |   |- Create additional vessel information,				took 0.000478 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took  8.7e-05 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took  7.2e-05 sec.
***  |   |- Calculation finished,					took    26.56 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took  9.6e-05 sec.
***  |   |- Calculation finished,					took    27.28 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000152 sec.
***  |   |- Calculation finished,					took    27.65 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000324 sec.
***  |   |- Calculation finished,					took    28.32 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000278 sec.
***  |   |- Calculation finished,					took     28.1 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000271 sec.
***  |   |- Calculation finished,					took    27.57 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -1,903,788.8561 (-1903788.8561)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	2m51.116s
user	6m30.930s
sys	0m30.832s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
