	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_0_fr.p
Instance LSFDP: LSFDRP_1_5_0_0_fd_52.p

This object contains the information regarding the instance 1_5_0_0 for a duration of 52 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took  0.15291 sec.
***  |   |- Create additional vessel information,				took 0.000498 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 7463 rows, 21760 columns and 169660 nonzeros
Variable types: 6052 continuous, 15708 integer (15708 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [5e+01, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Presolve removed 5982 rows and 16217 columns
Presolve time: 0.49s
Presolved: 1481 rows, 5543 columns, 22534 nonzeros
Variable types: 180 continuous, 5363 integer (5355 binary)

Root relaxation: objective 1.059961e+06, 1418 iterations, 0.13 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1059961.34    0  196          - 1059961.34      -     -    0s
H    0     0                    -3816192.907 1059961.34   128%     -    0s
H    0     0                    -3657673.673 1059961.34   129%     -    0s
H    0     0                    -2402640.857 1059961.34   144%     -    0s
     0     0 -397597.88    0  218 -2402640.9 -397597.88  83.5%     -    1s
H    0     0                    -2054048.583 -397597.88  80.6%     -    1s
H    0     0                    -1903788.856 -397597.88  79.1%     -    1s
     0     0 -415005.85    0  217 -1903788.9 -415005.85  78.2%     -    1s
     0     0 -589799.88    0  211 -1903788.9 -589799.88  69.0%     -    1s
     0     0 -589799.88    0  223 -1903788.9 -589799.88  69.0%     -    1s
     0     0 -589799.88    0  221 -1903788.9 -589799.88  69.0%     -    1s
     0     0 -837658.34    0  221 -1903788.9 -837658.34  56.0%     -    1s
     0     0 -837658.34    0  138 -1903788.9 -837658.34  56.0%     -    1s
     0     0 -837658.34    0  241 -1903788.9 -837658.34  56.0%     -    1s
H    0     0                    -1903788.798 -837658.34  56.0%     -    1s
     0     0 -837658.34    0  228 -1903788.8 -837658.34  56.0%     -    1s
     0     0 -837658.34    0  236 -1903788.8 -837658.34  56.0%     -    1s
     0     0 -837658.34    0  237 -1903788.8 -837658.34  56.0%     -    1s
     0     0 -906620.38    0  232 -1903788.8 -906620.38  52.4%     -    2s
     0     0 -1125210.4    0  190 -1903788.8 -1125210.4  40.9%     -    2s
     0     0 -1843178.6    0  194 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0  192 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0   93 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0  130 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0  122 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0  167 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0  102 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0  138 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0   37 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0   37 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0   28 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1843178.6    0   30 -1903788.8 -1843178.6  3.18%     -    2s
     0     0 -1874066.6    0   28 -1903788.8 -1874066.6  1.56%     -    2s

Cutting planes:
  Gomory: 4
  Implied bound: 1
  MIR: 1
  Zero half: 1

Explored 1 nodes (5991 simplex iterations) in 2.84 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -1.90379e+06 -1.90379e+06 -2.05405e+06 ... -3.81619e+06
No other solutions better than -1.90379e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (2.2250e-06) exceeds tolerance
Best objective -1.903787689276e+06, best bound -1.903787689276e+06, gap 0.0000%
Took  3.8036820888519287
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 6574 rows, 5847 columns and 20643 nonzeros
Variable types: 5394 continuous, 453 integer (453 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [1e+02, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 6574 rows and 5847 columns
Presolve time: 0.01s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.02 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -1.90379e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -1.903788856052e+06, best bound -1.903788856052e+06, gap 0.0000%

### Instance
### LSFDRP_1_5_0_0_fd_52

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 25
### |A|: 24
### |A^i|: 24
### |A^f|: 0
### |M|: 220
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,903,788.8561 (-1903788.8561)
###
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [88, 244, 157, 0]
###  |  88 (DEHAM Out@288) - 244 (TRALI In@668/Out@686) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  2 has the path [89, 255, 4, 229, 158, 0]
###  |  89 (DEHAM Out@456) - 255 (TRZMK In@785/Out@799) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@980/Out@1010) - 158 (INNSA
###  |   In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 184, 445.0 containers from  4 to 158
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |
###  |- Vessel  3 has the path [90, 159, 0]
###  |  90 (DEHAM Out@624) - 159 (INNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  4 has the path [91, 249, 161, 0]
###  |  91 (DEHAM Out@792) - 249 (TRAMB In@1416/Out@1443) - 161 (INNSA In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 13 has the path [92, 247, 156, 0]
###  |  92 (DKAAR Out@220) - 247 (TRAMB In@576/Out@603) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 14 has the path [93, 254, 3, 228, 224, 6, 231, 160, 0]
###  |  93 (DKAAR Out@388) - 254 (TRZMK In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 224 (OMSLL 
###  |  In@1132/Out@1145) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUM
###  |  MY_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 200, 244.0 containers from 228 to 160
###  |   |- and carried Demand 201,  67.0 containers from 228 to 160
###  |   |- and carried Demand 203, 118.0 containers from  3 to 160
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |   |- and carried Demand 206,  15.0 containers from  3 to 160
###  |   |- and carried Demand 207, 267.0 containers from  3 to 160
###  |   |- and carried Demand 208,   1.0 containers from  3 to 160
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m6.093s
user	0m9.003s
sys	0m0.945s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
