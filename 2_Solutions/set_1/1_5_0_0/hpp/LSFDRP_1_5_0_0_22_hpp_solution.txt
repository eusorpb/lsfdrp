	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_5_0_0_fr.p
Instance LSFDP: LSFDRP_1_5_0_0_fd_22.p

This object contains the information regarding the instance 1_5_0_0 for a duration of 22 weeks.

***  |- Create instance object for heuristic
***  |   |- Calculate demand structure for node flow model,		took 0.123902 sec.
***  |   |- Create additional vessel information,				took 0.000417 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 12227 rows, 36858 columns and 296581 nonzeros
Variable types: 10254 continuous, 26604 integer (26604 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [4e+01, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Presolve removed 10067 rows and 27321 columns
Presolve time: 0.66s
Presolved: 2160 rows, 9537 columns, 41566 nonzeros
Variable types: 266 continuous, 9271 integer (9241 binary)

Root relaxation: objective 5.695690e+06, 2355 iterations, 0.20 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 5695689.60    0  365          - 5695689.60      -     -    1s
H    0     0                    -533622.0381 5695689.60  1167%     -    1s
H    0     0                    1042954.9976 5695689.60   446%     -    1s
     0     0 4663119.92    0  299 1042955.00 4663119.92   347%     -    1s
     0     0 4661151.48    0  315 1042955.00 4661151.48   347%     -    1s
     0     0 4660729.58    0  302 1042955.00 4660729.58   347%     -    1s
     0     0 4659138.03    0  301 1042955.00 4659138.03   347%     -    1s
     0     0 4630741.54    0  294 1042955.00 4630741.54   344%     -    1s
     0     0 4617690.30    0  297 1042955.00 4617690.30   343%     -    1s
     0     0 3624800.43    0  252 1042955.00 3624800.43   248%     -    2s
     0     0 3624110.10    0  252 1042955.00 3624110.10   247%     -    2s
     0     0 3606089.76    0  286 1042955.00 3606089.76   246%     -    2s
     0     0 3125893.88    0  259 1042955.00 3125893.88   200%     -    3s
     0     0 2896312.94    0  255 1042955.00 2896312.94   178%     -    3s
     0     0 2895544.76    0  229 1042955.00 2895544.76   178%     -    3s
H    0     0                    1043457.4723 2895544.76   177%     -    3s
     0     0 2737774.33    0  235 1043457.47 2737774.33   162%     -    3s
     0     0 2736314.11    0  227 1043457.47 2736314.11   162%     -    3s
     0     0 2724641.95    0  254 1043457.47 2724641.95   161%     -    3s
     0     0 2724475.32    0  254 1043457.47 2724475.32   161%     -    3s
     0     0 2562541.56    0  253 1043457.47 2562541.56   146%     -    3s
     0     2 2562541.56    0  253 1043457.47 2562541.56   146%     -    3s
H   33    13                    1043457.8449 2223522.68   113%  79.8    4s

Cutting planes:
  Gomory: 7
  Clique: 17
  MIR: 2
  Zero half: 4

Explored 70 nodes (8316 simplex iterations) in 4.44 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: 1.04346e+06 1.04346e+06 1.04295e+06 -533622 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.043457472339e+06, best bound 1.043458047708e+06, gap 0.0001%
Took  5.753236293792725
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 10396 rows, 9719 columns and 35599 nonzeros
Variable types: 8978 continuous, 741 integer (741 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [6e+01, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 10396 rows and 9719 columns
Presolve time: 0.01s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.02 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -832004 

Optimal solution found (tolerance 1.00e-04)
Best objective -8.320043212644e+05, best bound -8.320043212644e+05, gap 0.0000%

### Instance
### LSFDRP_1_5_0_0_fd_22

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 41
### |A|: 40
### |A^i|: 40
### |A^f|: 0
### |M|: 220
### |E|: 0
#############################################################################################################################
###
### Results
###
### The objective of the solution is -832,004.3213 (-832004.3213)
###
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [208, 119, 187, 1, 226, 155, 221, 4, 229, 225, 7, 232, 161, 0]
###  |  208 (NLRTM Out@92) - 119 (FRLEH In@111/Out@127) - 187 (MAPTM In@207/Out@228) - 1 (AEJEA In@395/Out@419) - 226 (PKBQM 
###  |  In@476/Out@506) - 155 (INNSA In@570/Out@586) - 221 (OMSLL In@642/Out@661) - 4 (AEJEA In@899/Out@923) - 229 (PKBQM In@
###  |  980/Out@1010) - 225 (OMSLL In@1300/Out@1313) - 7 (AEJEA In@1403/Out@1427) - 232 (PKBQM In@1484/Out@1514) - 161 (INNSA
###  |   In@1578/Out@1594) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  11,  17.0 containers from 208 to 119
###  |   |- and carried Demand 106, 288.0 containers from 221 to  4
###  |   |- and carried Demand 107,  14.0 containers from 221 to  4
###  |   |- and carried Demand 115,  81.0 containers from 226 to 221
###  |   |- and carried Demand 116,   2.0 containers from 226 to 221
###  |   |- and carried Demand 119,   1.0 containers from 226 to 155
###  |   |- and carried Demand 120, 359.0 containers from  1 to 225
###  |   |- and carried Demand 122,  63.0 containers from  1 to 226
###  |   |- and carried Demand 123, 174.0 containers from  1 to 226
###  |   |- and carried Demand 127,  34.0 containers from 232 to 161
###  |   |- and carried Demand 128,  84.0 containers from  7 to 232
###  |   |- and carried Demand 129,  50.0 containers from  7 to 232
###  |   |- and carried Demand 180, 254.0 containers from 229 to 161
###  |   |- and carried Demand 181,   1.0 containers from 229 to 161
###  |   |- and carried Demand 182, 135.0 containers from 229 to 161
###  |   |- and carried Demand 185,  75.0 containers from  4 to 161
###  |   |- and carried Demand 186, 200.0 containers from  4 to 229
###  |   |- and carried Demand 187,  24.0 containers from  4 to 229
###  |   |- and carried Demand 188,  26.0 containers from  4 to 161
###  |   |- and carried Demand 189, 144.0 containers from  4 to 161
###  |   |- and carried Demand 190,   1.0 containers from  4 to 161
###  |
###  |- Vessel  6 has the path [209, 121, 189, 98, 254, 3, 228, 157, 0]
###  |  209 (NLRTM Out@260) - 121 (FRLEH In@279/Out@295) - 189 (MAPTM In@375/Out@396) - 98 (ESALG In@421/Out@433) - 254 (TRZM
###  |  K In@617/Out@631) - 3 (AEJEA In@731/Out@755) - 228 (PKBQM In@812/Out@842) - 157 (INNSA In@906/Out@922) - 0 (DUMMY_END
###  |   Ziel-Service 81)
###  |   |- and carried Demand  18,  74.0 containers from 209 to 98
###  |   |- and carried Demand  19,   5.0 containers from 209 to 121
###  |   |- and carried Demand  22, 484.0 containers from 189 to 98
###  |   |- and carried Demand  23,  18.0 containers from 189 to 98
###  |   |- and carried Demand 199,  44.0 containers from 228 to 157
###  |   |- and carried Demand 204, 133.0 containers from  3 to 228
###  |   |- and carried Demand 205,  61.0 containers from  3 to 228
###  |
###  |- Vessel  7 has the path [211, 255, 158, 0]
###  |  211 (NLRTM Out@428) - 255 (TRZMK In@785/Out@799) - 158 (INNSA In@1074/Out@1090) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [213, 245, 5, 230, 159, 0]
###  |  213 (NLRTM Out@596) - 245 (TRALI In@836/Out@854) - 5 (AEJEA In@1067/Out@1091) - 230 (PKBQM In@1148/Out@1178) - 159 (I
###  |  NNSA In@1242/Out@1258) - 0 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 167,  86.0 containers from  5 to 230
###  |   |- and carried Demand 168, 118.0 containers from  5 to 230
###  |
###  |- Vessel 15 has the path [120, 188, 2, 227, 223, 6, 231, 160, 0]
###  |  120 (FRLEH Out@158) - 188 (MAPTM In@238/Out@259) - 2 (AEJEA In@563/Out@587) - 227 (PKBQM In@644/Out@674) - 223 (OMSLL
###  |   In@889/Out@904) - 6 (AEJEA In@1235/Out@1259) - 231 (PKBQM In@1316/Out@1346) - 160 (INNSA In@1410/Out@1426) - 0 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 144,  31.0 containers from 231 to 160
###  |   |- and carried Demand 145,  37.0 containers from  6 to 160
###  |   |- and carried Demand 146, 211.0 containers from  6 to 160
###  |   |- and carried Demand 147, 176.0 containers from  6 to 231
###  |   |- and carried Demand 215, 704.0 containers from  2 to 160
###  |   |- and carried Demand 216,   8.0 containers from  2 to 160
###  |   |- and carried Demand 217, 156.0 containers from  2 to 227
###  |   |- and carried Demand 218,  45.0 containers from  2 to 227
###  |
###  |- Vessel 16 has the path [122, 190, 156, 0]
###  |  122 (FRLEH Out@326) - 190 (MAPTM In@406/Out@427) - 156 (INNSA In@738/Out@754) - 0 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m7.399s
user	0m13.875s
sys	0m1.280s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
