	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_25.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 33 rows, 30 columns and 90 nonzeros
Variable types: 0 continuous, 30 integer (30 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+00]
  Objective range  [3e+06, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+00]
Found heuristic solution: objective -0.0000000
Presolve removed 33 rows and 30 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.01 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -0 

Optimal solution found (tolerance 1.00e-04)
Best objective -0.000000000000e+00, best bound -0.000000000000e+00, gap 0.0000%

### Objective value in 1k: -0.00
### Objective value: -0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 177(WCSA): 18, (PMax25), 3
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[177,18] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[4,177] = 1
y[13,177] = 1
y[14,177] = 1

### All variables with value zero:
rho[177,19] = 0
rho[177,30] = 0
eta[177,19,1] = 0
eta[177,19,2] = 0
eta[177,19,3] = 0
eta[177,30,1] = 0
eta[177,30,2] = 0
eta[177,30,3] = 0
y[1,177] = 0
y[2,177] = 0
y[3,177] = 0
y[5,177] = 0
y[6,177] = 0
y[7,177] = 0
y[8,177] = 0
y[9,177] = 0
y[10,177] = 0
y[11,177] = 0
y[12,177] = 0
y[15,177] = 0
y[16,177] = 0
y[17,177] = 0
y[18,177] = 0

### End
Warning: Failed to open log file 'gurobi.log'

real	0m1.468s
user	0m0.207s
sys	0m0.096s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
