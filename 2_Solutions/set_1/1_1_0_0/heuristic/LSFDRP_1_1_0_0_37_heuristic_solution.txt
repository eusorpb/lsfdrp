	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_37.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_37.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.017232 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.108073 sec.
***  |   |- Create additional vessel information,				took 0.000437 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.000107 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took  9.7e-05 sec.
***  |   |- Calculation finished,					took     8.31 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took  0.00016 sec.
***  |   |- Calculation finished,					took     8.77 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took  0.00015 sec.
***  |   |- Calculation finished,					took      8.3 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000132 sec.
***  |   |- Calculation finished,					took      8.0 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000114 sec.
***  |   |- Calculation finished,					took     7.99 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000117 sec.
***  |   |- Calculation finished,					took     7.94 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -2,633,013.9962 (-2633013.9962)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	0m51.347s
user	1m21.078s
sys	0m4.659s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
