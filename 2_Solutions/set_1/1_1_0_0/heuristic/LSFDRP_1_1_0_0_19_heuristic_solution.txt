	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_19.p


*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_19.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.021508 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.136624 sec.
***  |   |- Create additional vessel information,				took 0.000559 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.000135 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took 0.000119 sec.
***  |   |- Calculation finished,					took     9.18 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.000192 sec.
***  |   |- Calculation finished,					took     9.17 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.000298 sec.
***  |   |- Calculation finished,					took     8.64 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.000297 sec.
***  |   |- Calculation finished,					took     9.03 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.000172 sec.
***  |   |- Calculation finished,					took     8.69 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.000298 sec.
***  |   |- Calculation finished,					took     8.65 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,331,106.1952 (-2331106.1952)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,331,106.1952 (-2331106.1952)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,331,106.1952 (-2331106.1952)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,331,106.1952 (-2331106.1952)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,331,106.1952 (-2331106.1952)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,331,106.1952 (-2331106.1952)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is -2,331,106.1952 (-2331106.1952)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	0m56.631s
user	1m26.709s
sys	0m4.371s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
