	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_40.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_40 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35970 rows and 852685 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 47s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959201 columns (presolve time = 52s) ...
Presolve removed 46359 rows and 959201 columns
Presolve time: 51.91s
Presolved: 9214 rows, 49117 columns, 375500 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9825099e+08   7.321791e+04   0.000000e+00     53s
    3233   -1.6731380e+06   0.000000e+00   0.000000e+00     53s

Root relaxation: objective -1.673138e+06, 3233 iterations, 0.53 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1673138.0    0   99          - -1673138.0      -     -   53s
H    0     0                    -3425020.878 -1673138.0  51.1%     -   53s
H    0     0                    -3103606.246 -1673138.0  46.1%     -   53s
     0     0 -1779766.7    0  112 -3103606.2 -1779766.7  42.7%     -   56s
     0     0 -1779766.7    0  104 -3103606.2 -1779766.7  42.7%     -   59s
H    0     0                    -2876141.535 -1779766.7  38.1%     -   59s
     0     0 -1806660.3    0  106 -2876141.5 -1806660.3  37.2%     -   60s
     0     0 -2032672.4    0   90 -2876141.5 -2032672.4  29.3%     -   63s
     0     0 -2140916.2    0  123 -2876141.5 -2140916.2  25.6%     -   64s
     0     0 -2198463.6    0   85 -2876141.5 -2198463.6  23.6%     -   70s
H    0     0                    -2876141.534 -2198463.6  23.6%     -   70s
H    0     0                    -2803797.305 -2198463.6  21.6%     -   70s
H    0     0                    -2661895.553 -2362553.5  11.2%     -   70s
     0     0 -2362553.5    0   97 -2661895.6 -2362553.5  11.2%     -   71s
     0     0 -2362553.5    0   90 -2661895.6 -2362553.5  11.2%     -   72s
     0     0 -2363249.6    0   48 -2661895.6 -2363249.6  11.2%     -   72s
     0     0 -2376764.7    0   46 -2661895.6 -2376764.7  10.7%     -   72s
     0     0 -2376879.4    0   48 -2661895.6 -2376879.4  10.7%     -   72s
     0     0 -2377011.9    0   51 -2661895.6 -2377011.9  10.7%     -   72s
     0     0 -2392655.5    0   60 -2661895.6 -2392655.5  10.1%     -   73s
     0     0 -2392655.5    0   71 -2661895.6 -2392655.5  10.1%     -   73s
     0     0 -2392655.5    0   88 -2661895.6 -2392655.5  10.1%     -   74s
     0     0 -2415047.7    0   32 -2661895.6 -2415047.7  9.27%     -   74s
     0     0 -2416747.1    0   29 -2661895.6 -2416747.1  9.21%     -   74s
     0     0 -2474379.5    0   81 -2661895.6 -2474379.5  7.04%     -   74s
     0     0 -2474379.5    0   33 -2661895.6 -2474379.5  7.04%     -   74s
H    0     0                    -2644844.759 -2474379.5  6.45%     -   74s
     0     0 -2474379.5    0   37 -2644844.8 -2474379.5  6.45%     -   74s
     0     0 -2485374.8    0   38 -2644844.8 -2485374.8  6.03%     -   74s
     0     0 -2491874.3    0   47 -2644844.8 -2491874.3  5.78%     -   74s
     0     0 -2491874.3    0   18 -2644844.8 -2491874.3  5.78%     -   74s
H    0     0                    -2633013.993 -2491874.3  5.36%     -   74s
     0     0 -2515513.8    0   32 -2633014.0 -2515513.8  4.46%     -   74s
     0     0 -2551676.5    0   19 -2633014.0 -2551676.5  3.09%     -   74s
     0     0 -2555127.9    0   19 -2633014.0 -2555127.9  2.96%     -   74s
     0     0 -2555136.5    0   19 -2633014.0 -2555136.5  2.96%     -   74s
     0     0 -2557579.7    0   38 -2633014.0 -2557579.7  2.86%     -   74s
     0     0 -2558208.5    0   27 -2633014.0 -2558208.5  2.84%     -   74s
     0     0 -2558459.6    0   38 -2633014.0 -2558459.6  2.83%     -   74s
     0     0     cutoff    0      -2633014.0 -2633014.0  0.00%     -   74s

Cutting planes:
  Gomory: 4
  Implied bound: 1
  MIR: 4

Explored 1 nodes (19479 simplex iterations) in 74.76 seconds
Thread count was 16 (of 16 available processors)

Solution count 8: -2.63301e+06 -2.64484e+06 -2.6619e+06 ... -3.42502e+06
No other solutions better than -2.63301e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -2.633013992526e+06, best bound -2.633013992526e+06, gap 0.0000%

***
***  |- Calculation finished, took 74.77 (74.77) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9925 (-2633013.9925) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288.0) - 35 (CLLQN In@1072.0/Out@1090.0) - 40 (CLSAI In@1120.0/Out@1136.0) - 234 (PABLB In@1304.0/Out@
###  |  1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456.0) - 36 (CLLQN In@1240.0/Out@1258.0) - 240 (PECLL In@1419.15/Out@1421.15) - 236 (PABLB In@1640.0/O
###  |  ut@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,18] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[1,234,0] = 1
y[1,40,234] = 1
y[1,35,40] = 1
y[1,100,35] = 1
y[2,101,36] = 1
y[2,240,236] = 1
y[2,36,240] = 1
y[2,236,0] = 1
y[4,235,0] = 1
y[4,103,235] = 1
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[35] = 1072
zE[36] = 1240
zE[40] = 1120
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1419.15
zX[35] = 1090
zX[36] = 1258
zX[40] = 1136
zX[100] = 288
zX[101] = 456
zX[103] = 792
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1421.15
w[2,(240,236)] = 144.166
w[2,(36,240)] = 161.145
phi[1] = 1028
phi[2] = 1196
phi[4] = 692

### All variables with value smaller zero:

### End

real	2m21.700s
user	6m23.060s
sys	1m1.208s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
