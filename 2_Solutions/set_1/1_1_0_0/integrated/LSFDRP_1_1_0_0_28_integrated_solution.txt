	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_28.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_28 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35966 rows and 852685 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 49s) ...
Presolve removed 45856 rows and 957512 columns (presolve time = 50s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959201 columns
Presolve time: 53.85s
Presolved: 9214 rows, 49117 columns, 375500 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9931318e+08   7.349973e+04   0.000000e+00     55s
    2795   -1.5551824e+06   2.194850e+01   0.000000e+00     55s
    2839   -1.5574705e+06   0.000000e+00   0.000000e+00     55s

Root relaxation: objective -1.557471e+06, 2839 iterations, 0.41 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1557470.5    0   84          - -1557470.5      -     -   55s
H    0     0                    -3425020.878 -1557470.5  54.5%     -   55s
H    0     0                    -2729777.930 -1557470.5  42.9%     -   55s
     0     0 -1751731.8    0  112 -2729777.9 -1751731.8  35.8%     -   57s
     0     0 -1751731.8    0   94 -2729777.9 -1751731.8  35.8%     -   59s
     0     0 -1796893.2    0  124 -2729777.9 -1796893.2  34.2%     -   61s
     0     0 -1798766.4    0  137 -2729777.9 -1798766.4  34.1%     -   61s
H    0     0                    -2729777.930 -1798766.4  34.1%     -   61s
     0     0 -1832326.4    0  125 -2729777.9 -1832326.4  32.9%     -   62s
     0     0 -1832326.4    0   83 -2729777.9 -1832326.4  32.9%     -   63s
H    0     0                    -2710684.490 -1832326.4  32.4%     -   63s
     0     0 -2108053.3    0   96 -2710684.5 -2108053.3  22.2%     -   64s
     0     0 -2112594.8    0   98 -2710684.5 -2112594.8  22.1%     -   64s
     0     0 -2122049.8    0  101 -2710684.5 -2122049.8  21.7%     -   65s
     0     0 -2184403.4    0   98 -2710684.5 -2184403.4  19.4%     -   65s
     0     0 -2184403.4    0   82 -2710684.5 -2184403.4  19.4%     -   68s
H    0     0                    -2654480.869 -2184403.4  17.7%     -   68s
H    0     0                    -2633013.990 -2387932.4  9.31%     -   68s
     0     0 -2396390.3    0   97 -2633014.0 -2396390.3  8.99%     -   68s
     0     0 -2410536.0    0   75 -2633014.0 -2410536.0  8.45%     -   68s
     0     0 -2413074.0    0   80 -2633014.0 -2413074.0  8.35%     -   68s
     0     0 -2413074.0    0   83 -2633014.0 -2413074.0  8.35%     -   69s
     0     0 -2430465.2    0   81 -2633014.0 -2430465.2  7.69%     -   69s
     0     0 -2430465.2    0   30 -2633014.0 -2430465.2  7.69%     -   69s
     0     0 -2430465.2    0   54 -2633014.0 -2430465.2  7.69%     -   70s
H    0     0                    -2615488.391 -2430465.2  7.07%     -   70s
H    0     0                    -2606207.395 -2430465.2  6.74%     -   70s
     0     0 -2430465.2    0   37 -2606207.4 -2430465.2  6.74%     -   70s
H    0     0                    -2606207.390 -2430465.2  6.74%     -   70s
     0     0 -2430465.2    0   45 -2606207.4 -2430465.2  6.74%     -   70s
     0     0 -2430465.2    0   25 -2606207.4 -2430465.2  6.74%     -   70s
     0     0 -2430465.2    0   55 -2606207.4 -2430465.2  6.74%     -   70s
     0     0 -2430465.2    0   43 -2606207.4 -2430465.2  6.74%     -   70s
     0     0 -2430465.2    0   48 -2606207.4 -2430465.2  6.74%     -   70s
     0     0 -2440587.1    0   50 -2606207.4 -2440587.1  6.35%     -   70s
     0     0 -2480058.8    0   53 -2606207.4 -2480058.8  4.84%     -   70s
     0     0 -2490603.1    0   53 -2606207.4 -2490603.1  4.44%     -   70s
     0     0 -2536270.2    0   42 -2606207.4 -2536270.2  2.68%     -   70s
     0     0 -2570815.7    0   46 -2606207.4 -2570815.7  1.36%     -   70s
     0     0 -2597675.8    0   46 -2606207.4 -2597675.8  0.33%     -   70s
     0     0 -2597675.8    0   50 -2606207.4 -2597675.8  0.33%     -   70s
     0     0 -2597675.8    0   51 -2606207.4 -2597675.8  0.33%     -   70s
     0     0 -2597675.8    0   32 -2606207.4 -2597675.8  0.33%     -   70s
     0     0 -2597675.8    0   31 -2606207.4 -2597675.8  0.33%     -   70s

Cutting planes:
  Gomory: 3
  Clique: 4
  Flow cover: 1

Explored 1 nodes (16140 simplex iterations) in 70.64 seconds
Thread count was 16 (of 16 available processors)

Solution count 10: -2.60621e+06 -2.60621e+06 -2.60621e+06 ... -3.42502e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (1.8121e-06) exceeds tolerance
Best objective -2.606207389678e+06, best bound -2.606207389678e+06, gap 0.0000%

***
***  |- Calculation finished, took 70.66 (70.66) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,606,207.3897 (-2606207.3897) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 240 (PECLL In@1512.12/Out@1514.12) - 236 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel 16 has the path [136, 202, 235, 0]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,234,0] = 1
y[8,204,234] = 1
y[8,223,138] = 1
y[8,138,204] = 1
y[15,200,112] = 1
y[15,240,236] = 1
y[15,112,36] = 1
y[15,36,240] = 1
y[15,236,0] = 1
y[15,134,200] = 1
y[16,202,235] = 1
y[16,136,202] = 1
y[16,235,0] = 1
xD[46,200,112] = 580
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[36] = 1240
zE[112] = 284
zE[138] = 615
zE[200] = 238
zE[202] = 406
zE[204] = 711
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1512.12
zX[36] = 1258
zX[112] = 296
zX[134] = 158
zX[136] = 326
zX[138] = 631
zX[200] = 259
zX[202] = 427
zX[204] = 732
zX[223] = 596
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1514.12
w[15,(240,236)] = 125.884
w[15,(36,240)] = 140.711
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	2m17.804s
user	5m6.537s
sys	0m42.048s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
