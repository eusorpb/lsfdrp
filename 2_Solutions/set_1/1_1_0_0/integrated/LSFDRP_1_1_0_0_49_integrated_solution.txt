	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_49 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter IntFeasTol to 1e-09
   Prev: 1e-05  Min: 1e-09  Max: 0.1  Default: 1e-05
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35970 rows and 852685 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 56s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959201 columns
Presolve time: 59.42s
Presolved: 9214 rows, 49117 columns, 352554 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9931318e+08   7.249000e+04   0.000000e+00     60s
    3332   -1.6975547e+06   0.000000e+00   0.000000e+00     61s

Root relaxation: objective -1.697555e+06, 3332 iterations, 0.69 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1697554.7    0  100          - -1697554.7      -     -   61s
H    0     0                    -3880739.842 -1697554.7  56.3%     -   61s
H    0     0                    -3419896.330 -1697554.7  50.4%     -   61s
H    0     0                    -2828019.116 -1697554.7  40.0%     -   61s
     0     0 -1763534.6    0  130 -2828019.1 -1763534.6  37.6%     -   63s
     0     0 -2198680.6    0   90 -2828019.1 -2198680.6  22.3%     -   70s
H    0     0                    -2803797.308 -2198680.6  21.6%     -   71s
H    0     0                    -2633013.996 -2235999.2  15.1%     -   71s
     0     0 -2298882.9    0   90 -2633014.0 -2298882.9  12.7%     -   71s
     0     0 -2334664.0    0   69 -2633014.0 -2334664.0  11.3%     -   73s
     0     0 -2443521.3    0   41 -2633014.0 -2443521.3  7.20%     -   73s
     0     0 -2443521.3    0   43 -2633014.0 -2443521.3  7.20%     -   73s
     0     0 -2443521.3    0   40 -2633014.0 -2443521.3  7.20%     -   73s
     0     0 -2459937.6    0   24 -2633014.0 -2459937.6  6.57%     -   73s
     0     0 -2524959.4    0   48 -2633014.0 -2524959.4  4.10%     -   73s
     0     0 -2524959.4    0   21 -2633014.0 -2524959.4  4.10%     -   73s
     0     0 -2572056.1    0   28 -2633014.0 -2572056.1  2.32%     -   73s
     0     0 -2581853.2    0   21 -2633014.0 -2581853.2  1.94%     -   73s
     0     0 -2591603.6    0   26 -2633014.0 -2591603.6  1.57%     -   73s
     0     0 -2592128.0    0   32 -2633014.0 -2592128.0  1.55%     -   73s
     0     0 -2593420.9    0   32 -2633014.0 -2593420.9  1.50%     -   73s
     0     0 -2593656.0    0   35 -2633014.0 -2593656.0  1.49%     -   73s
     0     0 infeasible    0      -2633014.0 -2633014.0  0.00%     -   73s

Cutting planes:
  Gomory: 5
  MIR: 3
  Flow cover: 5

Explored 1 nodes (8372 simplex iterations) in 73.64 seconds
Thread count was 4 (of 16 available processors)

Solution count 5: -2.63301e+06 -2.8038e+06 -2.82802e+06 ... -3.88074e+06
No other solutions better than -2.63301e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -2.633013996169e+06, best bound -2.633013996169e+06, gap 0.0000%

***
***  |- Calculation finished, took 73.66 (73.66) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288.0) - 35 (CLLQN In@1072.0/Out@1090.0) - 40 (CLSAI In@1120.0/Out@1136.0) - 234 (PABLB In@1304.0/Out@
###  |  1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456.0) - 36 (CLLQN In@1240.0/Out@1258.0) - 240 (PECLL In@1493.83/Out@1495.83) - 236 (PABLB In@1640.0/O
###  |  ut@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,18] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[1,234,0] = 1
y[1,40,234] = 1
y[1,35,40] = 1
y[1,100,35] = 1
y[2,101,36] = 1
y[2,240,236] = 1
y[2,36,240] = 1
y[2,236,0] = 1
y[4,235,0] = 1
y[4,103,235] = 1
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[35] = 1072
zE[36] = 1240
zE[40] = 1120
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1493.83
zX[35] = 1090
zX[36] = 1258
zX[40] = 1136
zX[100] = 288
zX[101] = 456
zX[103] = 792
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1495.83
w[2,(240,236)] = 144.166
w[2,(36,240)] = 161.145
phi[1] = 1028
phi[2] = 1196
phi[4] = 692

### All variables with value smaller zero:

### End

real	2m16.671s
user	2m47.235s
sys	0m7.728s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
