	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_46.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_46 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35935 rows and 852638 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 57s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959201 columns (presolve time = 62s) ...
Presolve removed 46359 rows and 959201 columns
Presolve time: 62.24s
Presolved: 9214 rows, 49117 columns, 375500 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9931318e+08   7.349973e+04   0.000000e+00     63s
    2947   -1.7183142e+06   0.000000e+00   0.000000e+00     64s

Root relaxation: objective -1.718314e+06, 2947 iterations, 0.52 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1718314.2    0   99          - -1718314.2      -     -   63s
H    0     0                    -3425020.878 -1718314.2  49.8%     -   63s
H    0     0                    -3260886.898 -1718314.2  47.3%     -   64s
H    0     0                    -2815628.075 -1718314.2  39.0%     -   64s
     0     0 -1789480.5    0  110 -2815628.1 -1789480.5  36.4%     -   65s
     0     0 -2095333.6    0   89 -2815628.1 -2095333.6  25.6%     -   74s
H    0     0                    -2803797.306 -2095333.6  25.3%     -   75s
H    0     0                    -2803797.299 -2096428.2  25.2%     -   75s
H    0     0                    -2633013.989 -2096428.2  20.4%     -   75s
     0     0 -2252780.0    0   89 -2633014.0 -2252780.0  14.4%     -   75s

Cutting planes:
  Implied bound: 4
  Clique: 1
  Flow cover: 1

Explored 1 nodes (6617 simplex iterations) in 75.64 seconds
Thread count was 16 (of 16 available processors)

Solution count 6: -2.63301e+06 -2.8038e+06 -2.8038e+06 ... -3.42502e+06
No other solutions better than -2.63301e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (4.4036e-06) exceeds tolerance
Warning: max bound violation (3.6993e-06) exceeds tolerance
Best objective -2.633013988754e+06, best bound -2.633013988754e+06, gap 0.0000%

***
***  |- Calculation finished, took 75.65 (75.65) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9888 (-2633013.9888) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288.0) - 35 (CLLQN In@1072.0/Out@1090.0) - 40 (CLSAI In@1120.0/Out@1136.0) - 234 (PABLB In@1304.0/Out@
###  |  1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456.0) - 36 (CLLQN In@1240.0/Out@1258.0) - 240 (PECLL In@1493.83/Out@1495.83) - 236 (PABLB In@1640.0/O
###  |  ut@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,18] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[1,234,0] = 1
y[1,40,234] = 1
y[1,35,40] = 1
y[1,100,35] = 1
y[2,101,36] = 1
y[2,240,236] = 1
y[2,36,240] = 1
y[2,236,0] = 1
y[4,235,0] = 1
y[4,103,235] = 1
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[35] = 1072
zE[36] = 1240
zE[40] = 1120
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1493.83
zX[35] = 1090
zX[36] = 1258
zX[40] = 1136
zX[100] = 288
zX[101] = 456
zX[103] = 792
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1495.83
w[2,(240,236)] = 144.166
w[2,(36,240)] = 161.145
phi[1] = 1028
phi[2] = 1196
phi[4] = 692

### All variables with value smaller zero:

### End

real	2m24.722s
user	4m44.151s
sys	0m34.134s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
