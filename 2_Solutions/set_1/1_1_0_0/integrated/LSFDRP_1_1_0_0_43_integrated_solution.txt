	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_1_1_0_0_fd_43 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 274
*** |A|: 7458
*** |A^i|: 7407
*** |A^f|: 51
*** |M|: 115
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 55573 rows, 1008318 columns and 3860340 nonzeros
Variable types: 874062 continuous, 134256 integer (134256 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 35935 rows and 852638 columns (presolve time = 5s) ...
Presolve removed 35980 rows and 956061 columns (presolve time = 57s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 46359 rows and 959196 columns (presolve time = 60s) ...
Presolve removed 46359 rows and 959201 columns
Presolve time: 62.50s
Presolved: 9214 rows, 49117 columns, 375500 nonzeros
Variable types: 8201 continuous, 40916 integer (40871 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9931318e+08   7.349973e+04   0.000000e+00     63s
    3064   -1.6957261e+06   0.000000e+00   0.000000e+00     64s

Root relaxation: objective -1.695726e+06, 3064 iterations, 0.56 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1695726.1    0   98          - -1695726.1      -     -   64s
H    0     0                    -3425020.878 -1695726.1  50.5%     -   64s
H    0     0                    -3188279.937 -1695726.1  46.8%     -   64s
H    0     0                    -2815628.075 -1695726.1  39.8%     -   64s
     0     0 -1784917.7    0  107 -2815628.1 -1784917.7  36.6%     -   66s
     0     0 -1784917.7    0   95 -2815628.1 -1784917.7  36.6%     -   68s
     0     0 -1807230.4    0  111 -2815628.1 -1807230.4  35.8%     -   69s
     0     0 -1818985.0    0  122 -2815628.1 -1818985.0  35.4%     -   69s
H    0     0                    -2803797.308 -1818985.0  35.1%     -   70s
     0     0 -1850809.8    0  143 -2803797.3 -1850809.8  34.0%     -   70s
     0     0 -1872789.0    0  145 -2803797.3 -1872789.0  33.2%     -   70s
     0     0 -1872989.1    0  146 -2803797.3 -1872989.1  33.2%     -   71s
     0     0 -1906923.6    0  149 -2803797.3 -1906923.6  32.0%     -   71s
     0     0 -1906923.6    0  131 -2803797.3 -1906923.6  32.0%     -   72s
     0     0 -2002832.5    0  116 -2803797.3 -2002832.5  28.6%     -   73s
     0     0 -2189313.9    0  123 -2803797.3 -2189313.9  21.9%     -   74s
     0     0 -2193999.9    0   94 -2803797.3 -2193999.9  21.7%     -   75s
     0     0 -2197561.5    0  136 -2803797.3 -2197561.5  21.6%     -   75s
     0     0 -2240255.4    0   87 -2803797.3 -2240255.4  20.1%     -   78s
H    0     0                    -2803797.308 -2240255.4  20.1%     -   78s
H    0     0                    -2778682.556 -2386501.5  14.1%     -   78s
     0     0 -2396390.3    0  103 -2778682.6 -2396390.3  13.8%     -   78s
H    0     0                    -2633013.996 -2396390.3  8.99%     -   78s
     0     0 -2412294.4    0   46 -2633014.0 -2412294.4  8.38%     -   78s
     0     0 -2420728.6    0   62 -2633014.0 -2420728.6  8.06%     -   78s
     0     0 -2422592.4    0   69 -2633014.0 -2422592.4  7.99%     -   78s
     0     0 -2422592.4    0   70 -2633014.0 -2422592.4  7.99%     -   78s
     0     0 -2437088.1    0   63 -2633014.0 -2437088.1  7.44%     -   79s
     0     0 -2437088.1    0   46 -2633014.0 -2437088.1  7.44%     -   79s
H    0     0                    -2633013.995 -2437088.1  7.44%     -   79s
     0     0 -2437088.1    0   32 -2633014.0 -2437088.1  7.44%     -   79s
     0     0 -2437088.1    0   40 -2633014.0 -2437088.1  7.44%     -   79s
     0     0 -2492748.6    0   37 -2633014.0 -2492748.6  5.33%     -   79s
     0     0 -2492748.6    0   24 -2633014.0 -2492748.6  5.33%     -   79s
     0     0 -2550961.0    0   28 -2633014.0 -2550961.0  3.12%     -   79s
H    0     0                    -2633013.995 -2550961.0  3.12%     -   79s
     0     0 -2560606.0    0   34 -2633014.0 -2560606.0  2.75%     -   79s
     0     0 -2565775.7    0   40 -2633014.0 -2565775.7  2.55%     -   79s
     0     0 -2566704.2    0   40 -2633014.0 -2566704.2  2.52%     -   79s
     0     0 -2568309.8    0   40 -2633014.0 -2568309.8  2.46%     -   79s
     0     0 -2568983.0    0   40 -2633014.0 -2568983.0  2.43%     -   79s
     0     0 -2569547.2    0   44 -2633014.0 -2569547.2  2.41%     -   79s
     0     0 -2609133.9    0   37 -2633014.0 -2609133.9  0.91%     -   79s
     0     0 infeasible    0      -2633014.0 -2633014.0  0.00%     -   79s

Cutting planes:
  Gomory: 4
  MIR: 1
  Flow cover: 1

Explored 1 nodes (18143 simplex iterations) in 79.60 seconds
Thread count was 16 (of 16 available processors)

Solution count 9: -2.63301e+06 -2.63301e+06 -2.63301e+06 ... -3.42502e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -2.633013994532e+06, best bound -2.633013994532e+06, gap 0.0000%

***
***  |- Calculation finished, took 79.62 (79.62) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9945 (-2633013.9945) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288.0) - 35 (CLLQN In@1072.0/Out@1090.0) - 40 (CLSAI In@1120.0/Out@1136.0) - 234 (PABLB In@1304.0/Out@
###  |  1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456.0) - 36 (CLLQN In@1240.0/Out@1258.0) - 240 (PECLL In@1419.15/Out@1421.15) - 236 (PABLB In@1640.0/O
###  |  ut@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from  36 to 236
###  |   |- and carried Demand  81,   20.0 containers from  36 to 236
###  |   |- and carried Demand  83,    5.0 containers from  36 to 236
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,18] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[1,234,0] = 1
y[1,40,234] = 1
y[1,35,40] = 1
y[1,100,35] = 1
y[2,101,36] = 1
y[2,240,236] = 1
y[2,36,240] = 1
y[2,236,0] = 1
y[4,235,0] = 1
y[4,103,235] = 1
xD[80,240,236] = 194
xD[80,36,240] = 194
xD[81,240,236] = 20
xD[81,36,240] = 20
xD[83,240,236] = 5
xD[83,36,240] = 5
zE[35] = 1072
zE[36] = 1240
zE[40] = 1120
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1419.15
zX[35] = 1090
zX[36] = 1258
zX[40] = 1136
zX[100] = 288
zX[101] = 456
zX[103] = 792
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1421.15
w[2,(240,236)] = 144.166
w[2,(36,240)] = 161.145
phi[1] = 1028
phi[2] = 1196
phi[4] = 692

### All variables with value smaller zero:

### End

real	2m31.770s
user	5m31.690s
sys	0m44.022s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
