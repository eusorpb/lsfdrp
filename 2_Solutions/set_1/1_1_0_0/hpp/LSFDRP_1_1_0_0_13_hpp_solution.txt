	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_13.p

This object contains the information regarding the instance 1_1_0_0 for a duration of 13 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.018861 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.118381 sec.
***  |   |- Create additional vessel information,				took 0.000485 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 5915 rows, 6058 columns and 34325 nonzeros
Variable types: 2488 continuous, 3570 integer (3570 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Found heuristic solution: objective -4892606.028
Presolve removed 5684 rows and 5184 columns
Presolve time: 0.12s
Presolved: 231 rows, 874 columns, 4546 nonzeros
Found heuristic solution: objective -4587436.206
Variable types: 0 continuous, 874 integer (874 binary)

Root relaxation: objective -1.700574e+06, 131 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1700574.2    0   26 -4587436.2 -1700574.2  62.9%     -    0s
H    0     0                    -1840645.114 -1700574.2  7.61%     -    0s
H    0     0                    -1793995.685 -1700574.2  5.21%     -    0s

Cutting planes:
  Gomory: 1

Explored 1 nodes (148 simplex iterations) in 0.16 seconds
Thread count was 4 (of 16 available processors)

Solution count 3: -1.794e+06 -1.84065e+06 -4.58744e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -1.793995684738e+06, best bound -1.793995684738e+06, gap 0.0000%
Took  0.541224479675293
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 2411 rows, 1847 columns and 7455 nonzeros
Variable types: 1601 continuous, 246 integer (246 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 7e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 2411 rows and 1847 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.00 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -2.14771e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -2.147705392484e+06, best bound -2.147705392484e+06, gap 0.0000%

### Instance
### LSFDRP_1_1_0_0_fd_13

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 14
### |A|: 13
### |A^i|: 11
### |A^f|: 2
### |M|: 115
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,147,705.3925 (-2147705.3925)
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [134, 200, 112, 36, 240, 236, 0]
###  |  134 (FRLEH Out@158) - 200 (MAPTM In@238/Out@259) - 112 (ESALG In@284/Out@296) - 36 (CLLQN In@1240/Out@1258) - 240 (PE
###  |  CLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel 16 has the path [136, 202, 234, 0]
###  |  136 (FRLEH Out@326) - 202 (MAPTM In@406/Out@427) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m1.634s
user	0m1.117s
sys	0m0.088s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
