	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_1_1_0_0_fr.p
Instance LSFDP: LSFDRP_1_1_0_0_fd_43.p

This object contains the information regarding the instance 1_1_0_0 for a duration of 43 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.018783 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.120765 sec.
***  |   |- Create additional vessel information,				took 0.000499 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 5361 rows, 4718 columns and 26500 nonzeros
Variable types: 1934 continuous, 2784 integer (2784 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Presolve removed 5186 rows and 4124 columns
Presolve time: 0.09s
Presolved: 175 rows, 594 columns, 3173 nonzeros
Variable types: 0 continuous, 594 integer (594 binary)
Found heuristic solution: objective -4299558.448
Found heuristic solution: objective -3095534.318

Root relaxation: objective -2.712963e+06, 121 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -2712962.7    0   33 -3095534.3 -2712962.7  12.4%     -    0s
H    0     0                    -2931297.365 -2712962.7  7.45%     -    0s
H    0     0                    -2920503.840 -2712962.7  7.11%     -    0s
H    0     0                    -2823888.683 -2712962.7  3.93%     -    0s
H    0     0                    -2737431.234 -2715243.2  0.81%     -    0s
     0     0     cutoff    0      -2737431.2 -2737431.2  0.00%     -    0s

Cutting planes:
  Gomory: 1
  Zero half: 3

Explored 1 nodes (124 simplex iterations) in 0.12 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -2.73743e+06 -2.82389e+06 -2.9205e+06 ... -4.29956e+06
No other solutions better than -2.73743e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -2.737431233703e+06, best bound -2.737431233703e+06, gap 0.0000%
Took  0.5479469299316406
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 1999 rows, 1436 columns and 5858 nonzeros
Variable types: 1244 continuous, 192 integer (192 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 1999 rows and 1436 columns
Presolve time: 0.00s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.00 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -2.63301e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -2.633013996199e+06, best bound -2.633013996199e+06, gap 0.0000%

### Instance
### LSFDRP_1_1_0_0_fd_43

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 11
### |A|: 10
### |A^i|: 8
### |A^f|: 2
### |M|: 115
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,633,013.9962 (-2633013.9962)
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 35, 40, 234, 0]
###  |  100 (DEHAM Out@288) - 35 (CLLQN In@1072/Out@1090) - 40 (CLSAI In@1120/Out@1136) - 234 (PABLB In@1304/Out@1316) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |
###  |- Vessel  2 has the path [101, 36, 240, 236, 0]
###  |  101 (DEHAM Out@456) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m1.515s
user	0m1.108s
sys	0m0.080s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
