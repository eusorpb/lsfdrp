	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_5_1_0_1_fr.p
Instance LSFDP: LSFDRP_5_1_0_1_fd_46.p

*****************************************************************************************************************************
*** Solve LSFDRP_5_1_0_1_fd_46 with Gurobi
***
*** Graph info
*** |S|: 7
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 98
*** |A|: 1125
*** |A^i|: 1125
*** |A^f|: 0
*** |M|: 58
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 9088 rows, 75574 columns and 287885 nonzeros
Variable types: 67695 continuous, 7879 integer (7879 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [4e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+00]
Presolve removed 8771 rows and 74787 columns
Presolve time: 0.37s
Presolved: 317 rows, 787 columns, 4109 nonzeros
Variable types: 47 continuous, 740 integer (725 binary)
Found heuristic solution: objective -1012523.683

Root relaxation: objective 2.351324e+05, 203 iterations, 0.01 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0    235132.37051 235132.371  0.00%     -    0s

Explored 0 nodes (203 simplex iterations) in 0.43 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: 235132 -1.01252e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 2.351323705090e+05, best bound 2.351323705090e+05, gap 0.0000%

***
***  |- Calculation finished, took 0.43 (0.43) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 235,132.3705 (235132.3705) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel 1 has the path [85, 25, 30, 72, 36, 45, 21, 0]
###  |  85 (USMSY Out@238.0) - 25 (CLLQN In@764.0/Out@782.0) - 30 (CLSAI In@812.0/Out@828.0) - 72 (PABLB In@996.0/Out@1008.0)
###  |   - 36 (COBUN In@1036.0/Out@1051.0) - 45 (ECGYE In@1107.0/Out@1119.0) - 21 (CLIQQ In@1204.0/Out@1210.0) - 0 (DUMMY_END
###  |   Ziel-Service 177)
###  |   |- and carried Demand 53, 119.0 containers from 30 to 72
###  |   |- and carried Demand 54, 129.0 containers from 30 to 72
###  |- Vessel 2 has the path [86, 26, 31, 73, 37, 46, 22, 0]
###  |  86 (USMSY Out@406.0) - 26 (CLLQN In@932.0/Out@950.0) - 31 (CLSAI In@980.0/Out@996.0) - 73 (PABLB In@1164.0/Out@1176.0
###  |  ) - 37 (COBUN In@1204.0/Out@1219.0) - 46 (ECGYE In@1275.0/Out@1287.0) - 22 (CLIQQ In@1372.0/Out@1378.0) - 0 (DUMMY_EN
###  |  D Ziel-Service 177)
###  |   |- and carried Demand 43,  81.0 containers from 73 to 22
###  |   |- and carried Demand 44, 423.0 containers from 73 to 22
###  |   |- and carried Demand 45, 178.0 containers from 31 to 73
###  |   |- and carried Demand 46, 163.0 containers from 31 to 73
###  |- Vessel 3 has the path [87, 27, 32, 74, 38, 47, 23, 0]
###  |  87 (USMSY Out@574.0) - 27 (CLLQN In@1100.0/Out@1118.0) - 32 (CLSAI In@1148.0/Out@1164.0) - 74 (PABLB In@1332.0/Out@13
###  |  44.0) - 38 (COBUN In@1372.0/Out@1387.0) - 47 (ECGYE In@1443.0/Out@1455.0) - 23 (CLIQQ In@1540.0/Out@1546.0) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 33, 141.0 containers from 74 to 23
###  |   |- and carried Demand 34, 423.0 containers from 74 to 23
###  |   |- and carried Demand 35, 137.0 containers from 32 to 74
###  |   |- and carried Demand 36, 112.0 containers from 32 to 74
###  |
### Vessels not used in the solution: [4, 5, 6, 7]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,18] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[1,85,25] = 1
y[1,45,21] = 1
y[1,72,36] = 1
y[1,21,0] = 1
y[1,36,45] = 1
y[1,30,72] = 1
y[1,25,30] = 1
y[2,86,26] = 1
y[2,31,73] = 1
y[2,22,0] = 1
y[2,46,22] = 1
y[2,73,37] = 1
y[2,26,31] = 1
y[2,37,46] = 1
y[3,27,32] = 1
y[3,74,38] = 1
y[3,23,0] = 1
y[3,32,74] = 1
y[3,47,23] = 1
y[3,87,27] = 1
y[3,38,47] = 1
xD[33,74,38] = 141
xD[33,47,23] = 141
xD[33,38,47] = 141
xD[34,74,38] = 423
xD[34,47,23] = 423
xD[34,38,47] = 423
xD[35,32,74] = 137
xD[36,32,74] = 112
xD[43,46,22] = 81
xD[43,73,37] = 81
xD[43,37,46] = 81
xD[44,46,22] = 423
xD[44,73,37] = 423
xD[44,37,46] = 423
xD[45,31,73] = 178
xD[46,31,73] = 163
xD[53,30,72] = 119
xD[54,30,72] = 129
zE[21] = 1204
zE[22] = 1372
zE[23] = 1540
zE[25] = 764
zE[26] = 932
zE[27] = 1100
zE[30] = 812
zE[31] = 980
zE[32] = 1148
zE[36] = 1036
zE[37] = 1204
zE[38] = 1372
zE[45] = 1107
zE[46] = 1275
zE[47] = 1443
zE[72] = 996
zE[73] = 1164
zE[74] = 1332
zX[21] = 1210
zX[22] = 1378
zX[23] = 1546
zX[25] = 782
zX[26] = 950
zX[27] = 1118
zX[30] = 828
zX[31] = 996
zX[32] = 1164
zX[36] = 1051
zX[37] = 1219
zX[38] = 1387
zX[45] = 1119
zX[46] = 1287
zX[47] = 1455
zX[72] = 1008
zX[73] = 1176
zX[74] = 1344
zX[85] = 238
zX[86] = 406
zX[87] = 574
phi[1] = 972
phi[2] = 972
phi[3] = 972

### All variables with value smaller zero:

### End

real	0m4.053s
user	0m3.717s
sys	0m0.120s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
