	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_5_2_1_1_fr.p
Instance LSFDP: LSFDRP_5_2_1_1_fd_16.p

*****************************************************************************************************************************
*** Solve LSFDRP_5_2_1_1_fd_16 with Gurobi
***
*** Graph info
*** |S|: 12
*** |S^E|: 0
*** |S^C|: 4
*** |V|: 172
*** |A|: 2651
*** |A^i|: 2651
*** |A^f|: 0
*** |M|: 118
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 28440 rows, 350281 columns and 1309416 nonzeros
Variable types: 318460 continuous, 31821 integer (31821 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [3e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+00]
Presolve removed 26287 rows and 319178 columns
Presolve time: 3.86s
Presolved: 2153 rows, 31103 columns, 92461 nonzeros
Variable types: 97 continuous, 31006 integer (30989 binary)
Warning: Failed to open log file 'gurobi.log'

Root relaxation: objective -2.433094e+06, 1176 iterations, 0.07 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -7887247.0    0    3          - -7887247.0      -     -    4s
H    0     0                    -7887246.956 -7887247.0  0.00%     -    4s

Explored 1 nodes (1573 simplex iterations) in 4.71 seconds
Thread count was 4 (of 16 available processors)

Solution count 1: -7.88725e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -7.887246955760e+06, best bound -7.887246955760e+06, gap 0.0000%

***
***  |- Calculation finished, took 4.71 (4.71) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -7,887,246.9558 (-7887246.9558) and is feasible with a Gap of 0.0
###
### Service 43 (AMEX) is operated by 7 vessels from type 18 (PMax25)
###  |- Vessel  2 has the path [129, 148, 0]
###  |  129 (USMSY Out@406.0) - 148 (ZACPT In@1334.0/Out@1357.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel  3 has the path [130, 149, 0]
###  |  130 (USMSY Out@574.0) - 149 (ZACPT In@1502.0/Out@1525.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel  4 has the path [131, 150, 0]
###  |  131 (USMSY Out@742.0) - 150 (ZACPT In@1670.0/Out@1693.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel  5 has the path [132, 151, 0]
###  |  132 (USMSY Out@910.0) - 151 (ZACPT In@1838.0/Out@1861.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel  6 has the path [133, 152, 0]
###  |  133 (USMSY Out@1078.0) - 152 (ZACPT In@2006.0/Out@2029.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel  7 has the path [134, 153, 0]
###  |  134 (USMSY Out@1246.0) - 153 (ZACPT In@2174.0/Out@2197.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel  8 has the path [46, 155, 0]
###  |  46 (DEHAM Out@276.0) - 155 (ZACPT In@2342.0/Out@2365.0) - 0 (DUMMY_END Ziel-Service 43)
###  |
### Vessels not used in the solution: [1, 9, 10, 11, 12]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[43,18] = 1
eta[43,18,1] = 1
eta[43,18,2] = 1
eta[43,18,3] = 1
eta[43,18,4] = 1
eta[43,18,5] = 1
eta[43,18,6] = 1
eta[43,18,7] = 1
y[2,148,0] = 1
y[2,129,148] = 1
y[3,149,0] = 1
y[3,130,149] = 1
y[4,131,150] = 1
y[4,150,0] = 1
y[5,151,0] = 1
y[5,132,151] = 1
y[6,133,152] = 1
y[6,152,0] = 1
y[7,134,153] = 1
y[7,153,0] = 1
y[8,46,155] = 1
y[8,155,0] = 1
zE[148] = 1334
zE[149] = 1502
zE[150] = 1670
zE[151] = 1838
zE[152] = 2006
zE[153] = 2174
zE[155] = 2342
zX[46] = 276
zX[129] = 406
zX[130] = 574
zX[131] = 742
zX[132] = 910
zX[133] = 1078
zX[134] = 1246
zX[148] = 1357
zX[149] = 1525
zX[150] = 1693
zX[151] = 1861
zX[152] = 2029
zX[153] = 2197
zX[155] = 2365
phi[2] = 951
phi[3] = 951
phi[4] = 951
phi[5] = 951
phi[6] = 951
phi[7] = 951
phi[8] = 2089

### All variables with value smaller zero:

### End

real	0m20.997s
user	0m20.997s
sys	0m0.411s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
