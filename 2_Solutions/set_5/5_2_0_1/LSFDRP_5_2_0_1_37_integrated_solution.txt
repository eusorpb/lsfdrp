	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_5_2_0_1_fr.p
Instance LSFDP: LSFDRP_5_2_0_1_fd_37.p

*****************************************************************************************************************************
*** Solve LSFDRP_5_2_0_1_fd_37 with Gurobi
***
*** Graph info
*** |S|: 7
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 172
*** |A|: 2651
*** |A^i|: 2651
*** |A^f|: 0
*** |M|: 118
*** |E|: 0

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 27655 rows, 337021 columns and 1207701 nonzeros
Variable types: 318455 continuous, 18566 integer (18566 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [3e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+00]
Presolve removed 27146 rows and 333134 columns
Presolve time: 1.62s
Presolved: 509 rows, 3887 columns, 11882 nonzeros
Variable types: 9 continuous, 3878 integer (3867 binary)
Warning: Failed to open log file 'gurobi.log'

Root relaxation: objective -1.364660e+07, 755 iterations, 0.02 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -1.365e+07    0    3          - -1.365e+07      -     -    1s
H    0     0                    -1.36466e+07 -1.365e+07  0.00%     -    1s

Explored 1 nodes (755 simplex iterations) in 1.84 seconds
Thread count was 4 (of 16 available processors)

Solution count 1: -1.36466e+07 
No other solutions better than -1.36466e+07

Optimal solution found (tolerance 1.00e-04)
Best objective -1.364659889317e+07, best bound -1.364659889317e+07, gap 0.0000%

***
***  |- Calculation finished, took 1.85 (1.85) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -13,646,598.8932 (-13646598.8932) and is feasible with a Gap of 0.0
###
### Service 43 (AMEX) is operated by 7 vessels from type 18 (PMax25)
###  |- Vessel 1 has the path [128, 148, 0]
###  |  128 (USMSY Out@238.0) - 148 (ZACPT In@1334.0/Out@1357.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel 2 has the path [129, 155, 0]
###  |  129 (USMSY Out@406.0) - 155 (ZACPT In@2342.0/Out@2365.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel 3 has the path [130, 149, 0]
###  |  130 (USMSY Out@574.0) - 149 (ZACPT In@1502.0/Out@1525.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel 4 has the path [131, 150, 0]
###  |  131 (USMSY Out@742.0) - 150 (ZACPT In@1670.0/Out@1693.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel 5 has the path [132, 151, 0]
###  |  132 (USMSY Out@910.0) - 151 (ZACPT In@1838.0/Out@1861.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel 6 has the path [133, 152, 0]
###  |  133 (USMSY Out@1078.0) - 152 (ZACPT In@2006.0/Out@2029.0) - 0 (DUMMY_END Ziel-Service 43)
###  |- Vessel 7 has the path [134, 153, 0]
###  |  134 (USMSY Out@1246.0) - 153 (ZACPT In@2174.0/Out@2197.0) - 0 (DUMMY_END Ziel-Service 43)
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[43,18] = 1
eta[43,18,1] = 1
eta[43,18,2] = 1
eta[43,18,3] = 1
eta[43,18,4] = 1
eta[43,18,5] = 1
eta[43,18,6] = 1
eta[43,18,7] = 1
y[1,148,0] = 1
y[1,128,148] = 1
y[2,129,155] = 1
y[2,155,0] = 1
y[3,149,0] = 1
y[3,130,149] = 1
y[4,131,150] = 1
y[4,150,0] = 1
y[5,151,0] = 1
y[5,132,151] = 1
y[6,133,152] = 1
y[6,152,0] = 1
y[7,134,153] = 1
y[7,153,0] = 1
zE[148] = 1334
zE[149] = 1502
zE[150] = 1670
zE[151] = 1838
zE[152] = 2006
zE[153] = 2174
zE[155] = 2342
zX[128] = 238
zX[129] = 406
zX[130] = 574
zX[131] = 742
zX[132] = 910
zX[133] = 1078
zX[134] = 1246
zX[148] = 1357
zX[149] = 1525
zX[150] = 1693
zX[151] = 1861
zX[152] = 2029
zX[153] = 2197
zX[155] = 2365
phi[1] = 1119
phi[2] = 1959
phi[3] = 951
phi[4] = 951
phi[5] = 951
phi[6] = 951
phi[7] = 951

### All variables with value smaller zero:

### End

real	0m17.348s
user	0m16.672s
sys	0m0.466s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
