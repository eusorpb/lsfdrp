	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_2_0_1_fr.p
Instance LSFDP: LSFDRP_2_2_0_1_fd_40.p

This object contains the information regarding the instance 2_2_0_1 for a duration of 40 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.059581 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.175084 sec.
***  |   |- Create additional vessel information,				took 0.001992 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 10973 rows, 15223 columns and 98960 nonzeros
Variable types: 6371 continuous, 8852 integer (8852 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -6318209.200
Presolve removed 9711 rows and 11324 columns
Presolve time: 0.61s
Presolved: 1262 rows, 3899 columns, 24375 nonzeros
Found heuristic solution: objective -5892697.120
Variable types: 148 continuous, 3751 integer (3715 binary)

Root relaxation: objective 3.875423e+06, 1080 iterations, 0.04 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 3875422.95    0  103 -5892697.1 3875422.95   166%     -    0s
H    0     0                    -2523013.896 3875422.95   254%     -    0s
H    0     0                    -1347588.031 3875422.95   388%     -    0s
H    0     0                    712685.34843 3875422.95   444%     -    0s
     0     0 3498654.16    0  111 712685.348 3498654.16   391%     -    0s
H    0     0                    749801.62191 3498654.16   367%     -    0s
     0     0 3462658.01    0  109 749801.622 3462658.01   362%     -    0s
H    0     0                    775087.43661 3462658.01   347%     -    1s
     0     0 2964313.01    0  133 775087.437 2964313.01   282%     -    1s
H    0     0                    1048893.8998 2964313.01   183%     -    1s
     0     0 2867771.18    0  128 1048893.90 2867771.18   173%     -    1s
     0     0 2817789.78    0  128 1048893.90 2817789.78   169%     -    1s
     0     0 2803332.87    0  132 1048893.90 2803332.87   167%     -    1s
H    0     0                    1082331.8823 2803332.87   159%     -    1s
     0     0 2764045.46    0  106 1082331.88 2764045.46   155%     -    1s
     0     0 2731319.57    0  115 1082331.88 2731319.57   152%     -    1s
     0     0 2731319.57    0  117 1082331.88 2731319.57   152%     -    1s
     0     0 2731319.57    0  115 1082331.88 2731319.57   152%     -    1s
     0     2 2731319.57    0  113 1082331.88 2731319.57   152%     -    1s
H   33    20                    1086787.4386 2330174.18   114%  56.9    1s
H   34    20                    1086787.4579 2279312.96   110%  60.4    1s
*   77    20              12    1306108.3969 2279312.96  74.5%  43.7    1s

Cutting planes:
  Gomory: 5
  MIR: 11
  Flow cover: 1
  Zero half: 4

Explored 157 nodes (7694 simplex iterations) in 1.89 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 1.30611e+06 1.08679e+06 1.08679e+06 ... -2.52301e+06
No other solutions better than 1.30611e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 1.306108396890e+06, best bound 1.306108396890e+06, gap 0.0000%
Took  2.835007667541504
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 9351 rows, 8045 columns and 29497 nonzeros
Variable types: 7343 continuous, 702 integer (702 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 9351 rows and 8045 columns
Presolve time: 0.01s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.02 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 1.34977e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.349767094930e+06, best bound 1.349767094930e+06, gap 0.0000%

### Instance
### LSFDRP_2_2_0_1_fd_40

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 40
### |A|: 37
### |A^i|: 35
### |A^f|: 2
### |M|: 193
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,349,767.0949 (1349767.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 234, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 234 (PABLB In@1304/Out@1316) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 36, 240, 236, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 36 (CLLQ
###  |  N In@1240/Out@1258) - 240 (PECLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m4.851s
user	0m6.796s
sys	0m0.448s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
