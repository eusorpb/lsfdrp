	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_2_0_1_fr.p
Instance LSFDP: LSFDRP_2_2_0_1_fd_43.p


*****************************************************************************************************************************
*** Solve LSFDRP_2_2_0_1_fd_43.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took  0.02711 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.196058 sec.
***  |   |- Create additional vessel information,				took 0.001916 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.012033 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took 0.012298 sec.
***  |   |- Calculation finished, timelimit reached 			took   601.91 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.022926 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.49 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.023987 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.74 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took  0.01704 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.14 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.023365 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.58 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.017072 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.39 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,340,486.0949 (1340486.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 236, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 236 (PABLB In@1640/Out@1652) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 35, 240, 235, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 35 (CLLQ
###  |  N In@1072/Out@1090) - 240 (PECLL In@1231/Out@1346) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  99,  540.0 containers from 35 to 235
###  |   |- and carried Demand 100,   69.0 containers from 35 to 235
###  |   |- and carried Demand 101,  138.0 containers from 35 to 235
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,340,486.0949 (1340486.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 236, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 236 (PABLB In@1640/Out@1652) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 35, 240, 235, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 35 (CLLQ
###  |  N In@1072/Out@1090) - 240 (PECLL In@1231/Out@1346) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  99,  540.0 containers from 35 to 235
###  |   |- and carried Demand 100,   69.0 containers from 35 to 235
###  |   |- and carried Demand 101,  138.0 containers from 35 to 235
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,340,486.0949 (1340486.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 236, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 236 (PABLB In@1640/Out@1652) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 35, 240, 235, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 35 (CLLQ
###  |  N In@1072/Out@1090) - 240 (PECLL In@1231/Out@1346) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  99,  540.0 containers from 35 to 235
###  |   |- and carried Demand 100,   69.0 containers from 35 to 235
###  |   |- and carried Demand 101,  138.0 containers from 35 to 235
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,340,486.0949 (1340486.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 236, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 236 (PABLB In@1640/Out@1652) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 35, 240, 235, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 35 (CLLQ
###  |  N In@1072/Out@1090) - 240 (PECLL In@1231/Out@1346) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  99,  540.0 containers from 35 to 235
###  |   |- and carried Demand 100,   69.0 containers from 35 to 235
###  |   |- and carried Demand 101,  138.0 containers from 35 to 235
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,340,486.0949 (1340486.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 236, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 236 (PABLB In@1640/Out@1652) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 35, 240, 235, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 35 (CLLQ
###  |  N In@1072/Out@1090) - 240 (PECLL In@1231/Out@1346) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  99,  540.0 containers from 35 to 235
###  |   |- and carried Demand 100,   69.0 containers from 35 to 235
###  |   |- and carried Demand 101,  138.0 containers from 35 to 235
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,340,486.0949 (1340486.0949)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 275, 203, 236, 0]
###  |  218 (NLRTM Out@92) - 275 (ESALG In@328/Out@347) - 203 (MAPTM In@543/Out@564) - 236 (PABLB In@1640/Out@1652) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |
###  |- Vessel  6 has the path [219, 135, 201, 113, 35, 240, 235, 0]
###  |  219 (NLRTM Out@260) - 135 (FRLEH In@279/Out@295) - 201 (MAPTM In@375/Out@396) - 113 (ESALG In@421/Out@433) - 35 (CLLQ
###  |  N In@1072/Out@1090) - 240 (PECLL In@1231/Out@1346) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  99,  540.0 containers from 35 to 235
###  |   |- and carried Demand 100,   69.0 containers from 35 to 235
###  |   |- and carried Demand 101,  138.0 containers from 35 to 235
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 10, 11, 12, 14, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is 1,340,486.0949 (1340486.0949)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	60m6.221s
user	101m18.491s
sys	5m54.607s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
