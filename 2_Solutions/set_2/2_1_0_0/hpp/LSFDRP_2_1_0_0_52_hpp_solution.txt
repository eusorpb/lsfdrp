	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_1_0_0_fr.p
Instance LSFDP: LSFDRP_2_1_0_0_fd_52.p

This object contains the information regarding the instance 2_1_0_0 for a duration of 52 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.022063 sec.
***  |   |- Calculate demand structure for node flow model,		took  0.17885 sec.
***  |   |- Create additional vessel information,				took 0.001212 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 9382 rows, 12239 columns and 78615 nonzeros
Variable types: 5048 continuous, 7191 integer (7191 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Found heuristic solution: objective -5819452.434
Presolve removed 8562 rows and 9700 columns
Presolve time: 0.44s
Presolved: 820 rows, 2539 columns, 12823 nonzeros
Variable types: 76 continuous, 2463 integer (2426 binary)

Root relaxation: objective 2.846778e+06, 518 iterations, 0.01 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 2846777.59    0   81 -5819452.4 2846777.59   149%     -    0s
H    0     0                    2052244.6357 2846777.59  38.7%     -    0s
     0     0 2787309.41    0   85 2052244.64 2787309.41  35.8%     -    0s
     0     0 2574154.17    0   57 2052244.64 2574154.17  25.4%     -    0s
     0     0 2204209.73    0   50 2052244.64 2204209.73  7.40%     -    0s
     0     0 2204209.73    0   35 2052244.64 2204209.73  7.40%     -    0s
     0     0     cutoff    0      2052244.64 2052244.64  0.00%     -    0s

Cutting planes:
  Gomory: 2
  MIR: 1

Explored 1 nodes (1364 simplex iterations) in 0.81 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: 2.05224e+06 -5.81945e+06 
No other solutions better than 2.05224e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 2.052244635704e+06, best bound 2.052244635704e+06, gap 0.0000%
Took  1.6192796230316162
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 5978 rows, 5121 columns and 19845 nonzeros
Variable types: 4554 continuous, 567 integer (567 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 5978 rows and 5121 columns
Presolve time: 0.02s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.02 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 506430 

Optimal solution found (tolerance 1.00e-04)
Best objective 5.064297106003e+05, best bound 5.064297106003e+05, gap 0.0000%

### Instance
### LSFDRP_2_1_0_0_fd_52

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 32
### |A|: 30
### |A^i|: 28
### |A^f|: 2
### |M|: 146
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is 506,429.7106 (506429.7106)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 275, 201, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAP
###  |  TM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  312.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 278
###  |   |- and carried Demand 120,   10.0 containers from 201 to 278
###  |   |- and carried Demand 121,  484.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 133, 199, 290, 200, 112, 284, 45, 36, 240, 236, 0]
###  |  218 (NLRTM Out@92) - 133 (FRLEH In@111/Out@127) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPT
###  |  M In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 36 (CLLQN I
###  |  n@1240/Out@1258) - 240 (PECLL In@1399/Out@1514) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,   19.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel  7 has the path [221, 137, 203, 234, 0]
###  |  221 (NLRTM Out@428) - 137 (FRLEH In@447/Out@463) - 203 (MAPTM In@543/Out@564) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596) - 138 (FRLEH In@615/Out@631) - 204 (MAPTM In@711/Out@732) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 6, 9, 10, 11, 12, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m3.455s
user	0m3.568s
sys	0m0.322s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
