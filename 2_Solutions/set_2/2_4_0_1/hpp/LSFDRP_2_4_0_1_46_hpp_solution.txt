	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_1_fr.p
Instance LSFDP: LSFDRP_2_4_0_1_fd_46.p

This object contains the information regarding the instance 2_4_0_1 for a duration of 46 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.033046 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.414907 sec.
***  |   |- Create additional vessel information,				took 0.004956 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 33463 rows, 102340 columns and 793823 nonzeros
Variable types: 24796 continuous, 77544 integer (77544 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 27856 rows and 65219 columns
Presolve time: 4.25s
Presolved: 5607 rows, 37121 columns, 171686 nonzeros
Variable types: 516 continuous, 36605 integer (36386 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
    4825    1.1123498e+07   8.749277e+03   0.000000e+00      5s
    7552    7.5815047e+06   0.000000e+00   0.000000e+00      6s

Root relaxation: objective 7.581505e+06, 7552 iterations, 1.42 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7581504.70    0  531          - 7581504.70      -     -    6s
H    0     0                    -2913350.134 7581504.70   360%     -    6s
H    0     0                    -688898.5052 7581504.70  1201%     -   12s
     0     0 6222105.44    0  664 -688898.51 6222105.44  1003%     -   14s
     0     0 6214139.12    0  524 -688898.51 6214139.12  1002%     -   15s
     0     0 6213812.13    0  527 -688898.51 6213812.13  1002%     -   15s
     0     0 6122036.01    0  564 -688898.51 6122036.01   989%     -   15s
     0     0 6121199.24    0  565 -688898.51 6121199.24   989%     -   16s
     0     0 6119134.98    0  571 -688898.51 6119134.98   988%     -   16s
     0     0 6104017.56    0  602 -688898.51 6104017.56   986%     -   16s
     0     0 5306061.64    0  504 -688898.51 5306061.64   870%     -   16s
     0     0 5305779.28    0  502 -688898.51 5305779.28   870%     -   17s
     0     0 5180344.38    0  556 -688898.51 5180344.38   852%     -   17s
     0     0 5180026.25    0  548 -688898.51 5180026.25   852%     -   17s
     0     0 5164372.46    0  469 -688898.51 5164372.46   850%     -   17s
     0     0 5164324.58    0  427 -688898.51 5164324.58   850%     -   18s
     0     0 5160637.44    0  437 -688898.51 5160637.44   849%     -   18s
     0     0 5160190.31    0  516 -688898.51 5160190.31   849%     -   18s
     0     0 5159936.38    0  514 -688898.51 5159936.38   849%     -   18s
     0     0 5159936.38    0  514 -688898.51 5159936.38   849%     -   20s
     0     0 5159936.38    0  416 -688898.51 5159936.38   849%     -   23s
H    0     0                    -688898.5043 5159936.38   849%     -   24s
     0     0 5144968.93    0  409 -688898.50 5144968.93   847%     -   25s
     0     0 5049925.08    0  557 -688898.50 5049925.08   833%     -   26s
     0     0 5048790.60    0  533 -688898.50 5048790.60   833%     -   26s
     0     0 4847473.65    0  403 -688898.50 4847473.65   804%     -   26s
     0     0 4847440.30    0  406 -688898.50 4847440.30   804%     -   26s
     0     0 4843213.25    0  557 -688898.50 4843213.25   803%     -   27s
     0     0 4843078.37    0  571 -688898.50 4843078.37   803%     -   27s
     0     0 4809598.16    0  540 -688898.50 4809598.16   798%     -   27s
     0     0 4808247.54    0  564 -688898.50 4808247.54   798%     -   27s
     0     0 4808185.74    0  560 -688898.50 4808185.74   798%     -   27s
     0     0 4777433.10    0  669 -688898.50 4777433.10   793%     -   27s
     0     0 4773342.59    0  585 -688898.50 4773342.59   793%     -   28s
     0     0 4770497.85    0  461 -688898.50 4770497.85   792%     -   28s
     0     0 4770497.85    0  449 -688898.50 4770497.85   792%     -   30s
     0     2 4770497.85    0  448 -688898.50 4770497.85   792%     -   30s
    11    13 4354536.70    4  496 -688898.50 4610350.54   769%   502   35s
H   28    27                    -92350.15187 4610350.54  5092%   398   43s
    65    59 3670793.91   16  600 -92350.152 4610350.54  5092%   315   45s
*  167   126              27    -41616.48183 4610350.54      -   214   48s
   260   159 2190395.90   10  464 -41616.482 4378959.28      -   192   50s
*  462   261              27    26846.550110 3667649.06      -   181   54s
   481   281 2562344.43   13  448 26846.5501 3667649.06      -   183   55s
   796   452 2189415.45   13  464 26846.5501 2884813.68      -   170   60s
  1026   553 1817927.35   12  514 26846.5501 2755521.54      -   159   70s
  1028   554 1397297.03   16  448 26846.5501 2755521.54      -   159   75s
  1035   559 1141637.45   19  471 26846.5501 2603940.86  9599%   158   80s
  1042   564 1671830.00   13  466 26846.5501 2415183.13  8896%   156   85s
  1044   565 2410329.41    7  468 26846.5501 2410329.41  8878%   156   91s
H 1045   537                    54892.307337 2410329.41  4291%   156   98s
  1047   541 2216110.24   15  460 54892.3073 2216110.24  3937%   166  116s
  1057   548 1395956.65   18  399 54892.3073 1484622.21  2605%   170  120s
* 1109   520              25    82003.900619 1146106.78  1298%   174  123s
  1150   516 1069626.11   21  348 82003.9006 1143244.48  1294%   171  125s
H 1182   484                    82003.900630 1127157.00  1275%   170  127s
  1247   471 220114.790   23  389 82003.9006 1025628.39  1151%   173  130s
  1275   474 582742.588   21  333 82003.9006 1025628.39  1151%   173  135s
H 1277   449                    82003.900837 1025628.39  1151%   172  135s

Cutting planes:
  Gomory: 16
  Implied bound: 11
  Projected implied bound: 18
  Clique: 11
  MIR: 13
  Flow cover: 20
  Zero half: 10

Explored 1640 nodes (284577 simplex iterations) in 139.85 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 82003.9 82003.9 82003.9 ... -2.91335e+06
No other solutions better than 82003.9

Optimal solution found (tolerance 1.00e-04)
Best objective 8.200390084908e+04, best bound 8.200390084908e+04, gap 0.0000%
Took  143.52982473373413
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 38659 rows, 35648 columns and 120385 nonzeros
Variable types: 33986 continuous, 1662 integer (1662 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 38659 rows and 35648 columns
Presolve time: 0.07s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.10 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -3.02513e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective -3.025125333905e+07, best bound -3.025125333905e+07, gap 0.0000%

### Instance
### LSFDRP_2_4_0_1_fd_46

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 93
### |A|: 88
### |A^i|: 88
### |A^f|: 0
### |M|: 382
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is -30,251,253.339 (-30251253.339)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 367, 330, -3]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 367 (TRMER In
###  |  @915/Out@959) - 330 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 10 has the path [2, 275, 201, 291, 202, 114, 285, 25, 118, 368, 331, -3]
###  |  2 (BEANR Out@234) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAPTM
###  |   In@406/Out@427) - 114 (ESALG In@452/Out@464) - 285 (GWOXB In@631/Out@710) - 25 (CIABJ In@751/Out@783) - 118 (ESALG I
###  |  n@932/Out@950) - 368 (TRMER In@1083/Out@1127) - 331 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 118
###  |   |- and carried Demand 120,   10.0 containers from 201 to 118
###  |   |- and carried Demand 121,  484.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 118
###  |   |- and carried Demand 133,   10.0 containers from 202 to 118
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 118
###  |   |- and carried Demand 139,   10.0 containers from 291 to 118
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402) - 365 (TRMER In@579/Out@623) - 327 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [8, 366, 328, -3]
###  |  8 (BEANR Out@570) - 366 (TRMER In@747/Out@791) - 328 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 17 has the path [145, 364, 325, -3]
###  |  145 (GBFXT Out@302) - 364 (TRMER In@411/Out@455) - 325 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [148, 338, 339, 329, -3]
###  |  148 (GBFXT Out@470) - 338 (EGPSD In@867/Out@886) - 339 (EGPSD In@957/Out@973) - 329 (AEJEA In@1194/Out@1227) - -3 (DU
###  |  MMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 347, 344, 324, 229, 405, -4]
###  |  218 (NLRTM Out@92) - 347 (INPAV In@467/Out@494) - 344 (INNSA In@522/Out@546) - 324 (AEJEA In@617/Out@642) - 229 (OMSL
###  |  L In@721/Out@736) - 405 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |
###  |- Vessel  6 has the path [219, 272, 372, 425, 231, 375, 428, 408, -4]
###  |  219 (NLRTM Out@260) - 272 (TRZMK In@617/Out@631) - 372 (AEJEA In@731/Out@755) - 425 (PKBQM In@812/Out@842) - 231 (OMS
###  |  LL In@1132/Out@1145) - 375 (AEJEA In@1235/Out@1259) - 428 (PKBQM In@1316/Out@1346) - 408 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 362,  244.0 containers from 425 to 408
###  |   |- and carried Demand 363,   67.0 containers from 425 to 408
###  |   |- and carried Demand 365,  118.0 containers from 372 to 408
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |   |- and carried Demand 368,   15.0 containers from 372 to 408
###  |   |- and carried Demand 369,  267.0 containers from 372 to 408
###  |   |- and carried Demand 370,    1.0 containers from 372 to 408
###  |
###  |- Vessel  7 has the path [221, 137, 203, 115, 269, 373, 426, 354, 376, 429, 409, -4]
###  |  221 (NLRTM Out@428) - 137 (FRLEH In@447/Out@463) - 203 (MAPTM In@543/Out@564) - 115 (ESALG In@589/Out@601) - 269 (TRA
###  |  MB In@744/Out@771) - 373 (AEJEA In@899/Out@923) - 426 (PKBQM In@980/Out@1010) - 354 (OMSLL In@1300/Out@1313) - 376 (A
###  |  EJEA In@1403/Out@1427) - 429 (PKBQM In@1484/Out@1514) - 409 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |   |- and carried Demand 342,  254.0 containers from 426 to 409
###  |   |- and carried Demand 343,    1.0 containers from 426 to 409
###  |   |- and carried Demand 344,  135.0 containers from 426 to 409
###  |   |- and carried Demand 347,   75.0 containers from 373 to 409
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |   |- and carried Demand 350,   26.0 containers from 373 to 409
###  |   |- and carried Demand 351,  144.0 containers from 373 to 409
###  |   |- and carried Demand 352,    1.0 containers from 373 to 409
###  |
###  |- Vessel  8 has the path [223, 267, 406, -4]
###  |  223 (NLRTM Out@596) - 267 (TRALI In@836/Out@854) - 406 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 15 has the path [134, 371, 424, 230, 374, 427, 407, -4]
###  |  134 (FRLEH Out@158) - 371 (AEJEA In@563/Out@587) - 424 (PKBQM In@644/Out@674) - 230 (OMSLL In@889/Out@904) - 374 (AEJ
###  |  EA In@1067/Out@1091) - 427 (PKBQM In@1148/Out@1178) - 407 (INNSA In@1242/Out@1258) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |   |- and carried Demand 372,  237.0 containers from 424 to 407
###  |   |- and carried Demand 373,    2.0 containers from 424 to 407
###  |   |- and carried Demand 374,  146.0 containers from 424 to 407
###  |   |- and carried Demand 375,   97.0 containers from 424 to 407
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |   |- and carried Demand 381,   16.0 containers from 371 to 407
###  |   |- and carried Demand 382,  264.0 containers from 371 to 407
###  |
###  |- Vessel 16 has the path [136, 268, 404, -4]
###  |  136 (FRLEH Out@326) - 268 (TRAMB In@576/Out@603) - 404 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 18 (PMax25)
###  |
###  |- Vessel  4 has the path [103, 312, 302, -2]
###  |  103 (DEHAM Out@792) - 312 (USMIA In@1103/Out@1111) - 302 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 13 has the path [104, 301, -2]
###  |  104 (DKAAR Out@220) - 301 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [102, 358, 273, 235, 0]
###  |  102 (DEHAM Out@624) - 358 (TRAMB In@683/Out@731) - 273 (TRZMK In@785/Out@799) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [105, 266, 234, 0]
###  |  105 (DKAAR Out@388) - 266 (TRALI In@668/Out@686) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	2m28.378s
user	8m2.687s
sys	0m42.562s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
