	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_1_fr.p
Instance LSFDP: LSFDRP_2_4_0_1_fd_34.p

This object contains the information regarding the instance 2_4_0_1 for a duration of 34 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.037969 sec.
***  |   |- Calculate demand structure for node flow model,		took  0.48395 sec.
***  |   |- Create additional vessel information,				took 0.005034 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 30274 rows, 96091 columns and 759060 nonzeros
Variable types: 22404 continuous, 73687 integer (73687 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 24635 rows and 59408 columns
Presolve time: 4.82s
Presolved: 5639 rows, 36683 columns, 169853 nonzeros
Variable types: 540 continuous, 36143 integer (35910 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    1.9198822e+08   2.849206e+04   0.000000e+00      5s
    7607    8.0560992e+06   0.000000e+00   0.000000e+00      7s

Root relaxation: objective 8.056099e+06, 7607 iterations, 1.53 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7341385.83    0  647          - 7341385.83      -     -    7s
H    0     0                    -3079559.501 7341385.83   338%     -    7s
H    0     0                    437365.33805 7341385.83  1579%     -   13s
     0     0 6786415.04    0  675 437365.338 6786415.04  1452%     -   13s
     0     0 6640658.35    0  669 437365.338 6640658.35  1418%     -   14s
     0     0 6631073.45    0  680 437365.338 6631073.45  1416%     -   14s
     0     0 6627854.00    0  691 437365.338 6627854.00  1415%     -   14s
     0     0 6626000.42    0  773 437365.338 6626000.42  1415%     -   14s
     0     0 6435037.35    0  649 437365.338 6435037.35  1371%     -   15s
     0     0 6376366.02    0  698 437365.338 6376366.02  1358%     -   15s
     0     0 6369669.23    0  767 437365.338 6369669.23  1356%     -   15s
     0     0 5711117.87    0  671 437365.338 5711117.87  1206%     -   16s
     0     0 5708846.42    0  714 437365.338 5708846.42  1205%     -   16s
     0     0 5708777.29    0  715 437365.338 5708777.29  1205%     -   16s
     0     0 5586620.41    0  622 437365.338 5586620.41  1177%     -   17s
     0     0 5576910.06    0  607 437365.338 5576910.06  1175%     -   17s
     0     0 5576910.06    0  607 437365.338 5576910.06  1175%     -   17s
     0     0 5546598.17    0  616 437365.338 5546598.17  1168%     -   17s
     0     0 5546434.34    0  601 437365.338 5546434.34  1168%     -   18s
     0     0 5546434.34    0  593 437365.338 5546434.34  1168%     -   20s
     0     0 5546434.34    0  486 437365.338 5546434.34  1168%     -   23s
     0     0 4822926.86    0  562 437365.338 4822926.86  1003%     -   25s
H    0     0                    580235.56481 4822926.86   731%     -   26s
     0     0 4775620.71    0  637 580235.565 4775620.71   723%     -   26s
     0     0 4771256.24    0  412 580235.565 4771256.24   722%     -   26s
     0     0 4767295.94    0  558 580235.565 4767295.94   722%     -   26s
     0     0 4766888.69    0  596 580235.565 4766888.69   722%     -   26s
     0     0 4761127.56    0  413 580235.565 4761127.56   721%     -   26s
     0     0 4585851.26    0  513 580235.565 4585851.26   690%     -   27s
     0     0 4585792.01    0  505 580235.565 4585792.01   690%     -   27s
     0     0 4569980.43    0  634 580235.565 4569980.43   688%     -   27s
     0     0 4568869.26    0  637 580235.565 4568869.26   687%     -   27s
     0     0 4568855.15    0  638 580235.565 4568855.15   687%     -   27s
     0     0 4563477.27    0  647 580235.565 4563477.27   686%     -   27s
     0     0 4562597.82    0  633 580235.565 4562597.82   686%     -   28s
     0     0 4562597.82    0  631 580235.565 4562597.82   686%     -   28s
     0     2 4562597.82    0  631 580235.565 4562597.82   686%     -   28s
    11    16 3090557.25    3  354 580235.565 3711393.24   540%   379   30s
*  105    96              33    733323.29308 3711393.24   406%   182   32s
H  145   132                    787041.79979 3711393.24   372%   170   34s
H  146   132                    827075.52205 3711393.24   349%   169   34s
   162   137 1851533.80   29  303 827075.522 3711393.24   349%   166   35s
   562   299 1981001.17    5  217 827075.522 3015959.25   265%   126   40s
*  878   389              20    1011802.1370 2781752.52   175%   121   43s
*  880   384              18    1027674.2553 2781752.52   171%   120   43s
  1015   417 1752359.61    9  268 1027674.26 2708287.22   164%   120   45s
* 1048   424              17    1037986.5720 2698271.50   160%   119   45s
  1511   522 1745392.20   10  462 1037986.57 2379128.10   129%   118   50s
* 1913   577              17    1054472.3219 2177126.34   106%   114   53s
* 1916   577              15    1054472.3220 2177126.34   106%   114   53s
  2127   608 1132904.16   11  425 1054472.32 2096040.78  98.8%   112   56s
H 2128   608                    1054472.7293 2096040.78  98.8%   112   56s
  2524   596 1484513.65   15  264 1054472.73 1976291.70  87.4%   108   60s
* 2575   589              16    1063801.8569 1974381.94  85.6%   108   60s
  2945   579 1112389.07   14  376 1063801.86 1843434.20  73.3%   106   70s
  3723   427     cutoff   16      1063801.86 1637762.04  54.0%   101   76s
H 3955   431                    1068017.1687 1570073.15  47.0%  98.7   76s
  4528   114     cutoff   15      1068017.17 1314408.57  23.1%  94.1   80s

Cutting planes:
  Gomory: 10
  Implied bound: 10
  Clique: 23
  MIR: 14
  Flow cover: 5
  Zero half: 28

Explored 4854 nodes (455112 simplex iterations) in 80.79 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 1.06802e+06 1.0638e+06 1.05447e+06 ... 787042
No other solutions better than 1.06802e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 1.068017168734e+06, best bound 1.068017168734e+06, gap 0.0000%
Took  84.94387149810791
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 35839 rows, 32820 columns and 110328 nonzeros
Variable types: 31284 continuous, 1536 integer (1536 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 2e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 35839 rows and 32820 columns
Presolve time: 0.05s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.07 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -2.17403e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective -2.174030169221e+07, best bound -2.174030169221e+07, gap 0.0000%

### Instance
### LSFDRP_2_4_0_1_fd_34

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 86
### |A|: 81
### |A^i|: 81
### |A^f|: 0
### |M|: 382
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is -21,740,301.6922 (-21740301.6922)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 368, 331, -3]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 368 (TRMER In
###  |  @1083/Out@1127) - 331 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234) - 364 (TRMER In@411/Out@455) - 325 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402) - 365 (TRMER In@579/Out@623) - 327 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [8, 367, 330, -3]
###  |  8 (BEANR Out@570) - 367 (TRMER In@915/Out@959) - 330 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 17 has the path [145, 338, 339, 329, -3]
###  |  145 (GBFXT Out@302) - 338 (EGPSD In@867/Out@886) - 339 (EGPSD In@957/Out@973) - 329 (AEJEA In@1194/Out@1227) - -3 (DU
###  |  MMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
###  |- Vessel 18 has the path [148, 366, 328, -3]
###  |  148 (GBFXT Out@470) - 366 (TRMER In@747/Out@791) - 328 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 275, 201, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAP
###  |  TM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 278
###  |   |- and carried Demand 120,   10.0 containers from 201 to 278
###  |   |- and carried Demand 121,  312.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 347, 344, 324, 229, 405, -4]
###  |  218 (NLRTM Out@92) - 347 (INPAV In@467/Out@494) - 344 (INNSA In@522/Out@546) - 324 (AEJEA In@617/Out@642) - 229 (OMSL
###  |  L In@721/Out@736) - 405 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |
###  |- Vessel  6 has the path [219, 272, 372, 425, 231, 375, 428, 408, -4]
###  |  219 (NLRTM Out@260) - 272 (TRZMK In@617/Out@631) - 372 (AEJEA In@731/Out@755) - 425 (PKBQM In@812/Out@842) - 231 (OMS
###  |  LL In@1132/Out@1145) - 375 (AEJEA In@1235/Out@1259) - 428 (PKBQM In@1316/Out@1346) - 408 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 362,  244.0 containers from 425 to 408
###  |   |- and carried Demand 363,   67.0 containers from 425 to 408
###  |   |- and carried Demand 365,  118.0 containers from 372 to 408
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |   |- and carried Demand 368,   15.0 containers from 372 to 408
###  |   |- and carried Demand 369,  267.0 containers from 372 to 408
###  |   |- and carried Demand 370,    1.0 containers from 372 to 408
###  |
###  |- Vessel  7 has the path [221, 266, 406, -4]
###  |  221 (NLRTM Out@428) - 266 (TRALI In@668/Out@686) - 406 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [223, 267, 374, 427, 407, -4]
###  |  223 (NLRTM Out@596) - 267 (TRALI In@836/Out@854) - 374 (AEJEA In@1067/Out@1091) - 427 (PKBQM In@1148/Out@1178) - 407 
###  |  (INNSA In@1242/Out@1258) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |
###  |- Vessel 15 has the path [134, 436, 452, 443, 273, 373, 426, 354, 376, 429, 409, -4]
###  |  134 (FRLEH Out@158) - 436 (USCHS In@378/Out@388) - 452 (USORF In@413/Out@426) - 443 (USEWR In@450/Out@466) - 273 (TRZ
###  |  MK In@785/Out@799) - 373 (AEJEA In@899/Out@923) - 426 (PKBQM In@980/Out@1010) - 354 (OMSLL In@1300/Out@1313) - 376 (A
###  |  EJEA In@1403/Out@1427) - 429 (PKBQM In@1484/Out@1514) - 409 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |   |- and carried Demand 342,  254.0 containers from 426 to 409
###  |   |- and carried Demand 343,    1.0 containers from 426 to 409
###  |   |- and carried Demand 344,  135.0 containers from 426 to 409
###  |   |- and carried Demand 347,   75.0 containers from 373 to 409
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |   |- and carried Demand 350,   26.0 containers from 373 to 409
###  |   |- and carried Demand 351,  144.0 containers from 373 to 409
###  |   |- and carried Demand 352,    1.0 containers from 373 to 409
###  |   |- and carried Demand 354,  339.0 containers from 452 to 373
###  |   |- and carried Demand 355,  115.0 containers from 452 to 373
###  |   |- and carried Demand 357,    2.0 containers from 452 to 426
###  |   |- and carried Demand 358,  194.0 containers from 436 to 373
###  |   |- and carried Demand 359,   49.0 containers from 436 to 373
###  |   |- and carried Demand 360,   40.0 containers from 436 to 426
###  |
###  |- Vessel 16 has the path [136, 268, 404, -4]
###  |  136 (FRLEH Out@326) - 268 (TRAMB In@576/Out@603) - 404 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 1 vessels from type 18 (PMax25)
###  |
###  |- Vessel 14 has the path [105, 301, -2]
###  |  105 (DKAAR Out@388) - 301 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [102, 358, 269, 235, 0]
###  |  102 (DEHAM Out@624) - 358 (TRAMB In@683/Out@731) - 269 (TRAMB In@744/Out@771) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [103, 234, 0]
###  |  103 (DEHAM Out@792) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	1m31.501s
user	4m34.565s
sys	0m15.565s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
