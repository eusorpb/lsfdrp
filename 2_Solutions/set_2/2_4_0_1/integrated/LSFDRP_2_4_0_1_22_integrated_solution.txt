	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_1_fr.p
Instance LSFDP: LSFDRP_2_4_0_1_fd_22.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_1_fd_22 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 13437 rows and 6312 columns (presolve time = 6s) ...
Presolve removed 76104 rows and 4384394 columns (presolve time = 10s) ...
Presolve removed 119693 rows and 5885092 columns (presolve time = 15s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 20s) ...
Presolve removed 167355 rows and 6806027 columns (presolve time = 25s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 30s) ...
Presolve removed 169457 rows and 6808029 columns (presolve time = 35s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 40s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 49s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 92s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 95s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 920s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 919.76s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.44189047e+15  4.10206017e+16  1.49e+08 3.48e+06  5.65e+12  1023s
   1  -2.43952655e+15  4.01425101e+16  8.20e+07 6.84e+07  3.19e+12  1078s
   2  -1.60770977e+15  3.86996825e+16  5.41e+07 4.37e+07  2.13e+12  1134s
   3  -1.20512792e+15  3.63809469e+16  4.06e+07 2.56e+07  1.58e+12  1189s
   4  -6.21783484e+14  3.30990415e+16  2.10e+07 1.31e+07  8.32e+11  1245s
   5  -2.95759522e+14  2.80237697e+16  1.00e+07 5.65e+06  4.02e+11  1301s

Barrier performed 5 iterations in 1312.38 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.09s

Solved with dual simplex

Root relaxation: objective 1.188428e+07, 120401 iterations, 385.57 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1792e+07    0  680          - 1.1792e+07      -     - 1364s
     0     0 1.0257e+07    0  565          - 1.0257e+07      -     - 1983s
     0     0 8411483.44    0  586          - 8411483.44      -     - 2047s
     0     0 8409115.99    0  594          - 8409115.99      -     - 2065s
     0     0 8409115.99    0  594          - 8409115.99      -     - 2066s
     0     0 7583372.21    0  662          - 7583372.21      -     - 2383s
H    0     0                    -2.43623e+07 7583372.21   131%     - 2385s
H    0     0                    -2.39274e+07 7583372.21   132%     - 2391s
     0     0 7565947.42    0  672 -2.393e+07 7565947.42   132%     - 2639s
     0     0 7565947.42    0  674 -2.393e+07 7565947.42   132%     - 2641s
     0     0 7273893.17    0  721 -2.393e+07 7273893.17   130%     - 2985s
     0     0 7266827.21    0  720 -2.393e+07 7266827.21   130%     - 3590s
     0     0 7266811.50    0  722 -2.393e+07 7266811.50   130%     - 3593s
     0     0 7176341.16    0  633 -2.393e+07 7176341.16   130%     - 3975s
     0     0 7171348.61    0  661 -2.393e+07 7171348.61   130%     - 4024s
     0     0 7171343.11    0  662 -2.393e+07 7171343.11   130%     - 4025s
     0     0 7162509.95    0  648 -2.393e+07 7162509.95   130%     - 4057s
     0     0 7162312.69    0  629 -2.393e+07 7162312.69   130%     - 4067s
     0     0 7162287.60    0  629 -2.393e+07 7162287.60   130%     - 4124s
     0     0 7162262.73    0  647 -2.393e+07 7162262.73   130%     - 4172s
     0     0 7162262.73    0  649 -2.393e+07 7162262.73   130%     - 4177s
     0     0 7161860.54    0  662 -2.393e+07 7161860.54   130%     - 4188s
     0     0 7161860.54    0  662 -2.393e+07 7161860.54   130%     - 4191s
     0     0 7160722.06    0  675 -2.393e+07 7160722.06   130%     - 4202s
     0     0 7160702.38    0  673 -2.393e+07 7160702.38   130%     - 4206s
     0     0 7160044.91    0  665 -2.393e+07 7160044.91   130%     - 4213s
     0     0 7160042.78    0  662 -2.393e+07 7160042.78   130%     - 4219s
     0     0 7159299.84    0  656 -2.393e+07 7159299.84   130%     - 4228s
     0     0 7159299.84    0  653 -2.393e+07 7159299.84   130%     - 4538s
H    0     0                    -2.14844e+07 7159299.84   133%     - 4924s
H    0     0                    -2.13593e+07 7159299.84   134%     - 5376s
     0     2 7159299.84    0  651 -2.136e+07 7159299.84   134%     - 5411s
     1     4 7022588.43    1  613 -2.136e+07 7022588.43   133% 312025 13027s
     3     5 -1.084e+07    2  558 -2.136e+07 3127937.61   115% 522075 42579s
     7     8 -1.207e+07    3  547 -2.136e+07 2238981.66   110% 227850 43062s
    11    12 -1.454e+07    4  533 -2.136e+07 1198428.74   106% 154161 44398s
    15    16 -1.454e+07    5  531 -2.136e+07 -714690.30  96.7% 118245 44976s
    19    18 -1.455e+07    6  500 -2.136e+07 -962449.36  95.5% 98447 45707s
    23    16 -1.455e+07    7  501 -2.136e+07 -966768.44  95.5% 82990 49457s
    30    24 -1.509e+07    9  451 -2.136e+07 -1595276.1  92.5% 79338 62227s
    36    30 -1.473e+07    9  512 -2.136e+07 -2933144.3  86.3% 81306 62931s
    43    32 -1.507e+07   10  528 -2.136e+07 -3056595.0  85.7% 73102 82883s
    51    31 -1.511e+07   11  551 -2.136e+07 -3056595.0  85.7% 77196 86400s

Cutting planes:
  Implied bound: 8
  Clique: 35
  MIR: 43
  Flow cover: 1
  Zero half: 7

Explored 55 nodes (4238799 simplex iterations) in 86400.54 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -2.13593e+07 -2.14844e+07 -2.39274e+07 -2.43623e+07 

Time limit reached
Best objective -2.135933448281e+07, best bound -3.056594956081e+06, gap 85.6897%

***
***  |- Calculation finished, timelimit reached 86,400.65 (86400.65) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -21,359,334.4828 (-21359334.4828) and is feasible with a Gap of 0.8569
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 111, 284, 45, 368, 331, -3]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 111 (ESALG In@253.0/Out@265.0)
###  |   - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@765.0/Out@811.0) - 368 (TRMER In@1083.0/Out@1127.0) - 331 (AEJEA In@
###  |  1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 111
###  |   |- and carried Demand 117,  580.0 containers from 199 to 111
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 367, 330, -3]
###  |  8 (BEANR Out@570.0) - 367 (TRMER In@915.0/Out@959.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 17 has the path [145, 335, 336, 353, 230, 328, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718.0) - 353 (OMSLL In@878.0/Out@890
###  |  .0) - 230 (OMSLL In@889.0/Out@904.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |   |- and carried Demand 211,   54.0 containers from 336 to 328
###  |- Vessel 18 has the path [148, 338, 339, 329, -3]
###  |  148 (GBFXT Out@470.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@12
###  |  27.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  6 has the path [219, 135, 201, 113, 276, -1]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433
###  |  .0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |- Vessel  7 has the path [221, 277, -1]
###  |  221 (NLRTM Out@428.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 123,   68.0 containers from 221 to 277
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 266, 373, 426, 406, -4]
###  |  100 (DEHAM Out@288.0) - 266 (TRALI In@668.0/Out@686.0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@101
###  |  0.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel  2 has the path [101, 273, 374, 427, 407, -4]
###  |  101 (DEHAM Out@456.0) - 273 (TRZMK In@785.0/Out@799.0) - 374 (AEJEA In@1067.0/Out@1091.0) - 427 (PKBQM In@1148.0/Out@
###  |  1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel  3 has the path [102, 366, 267, 375, 428, 408, -4]
###  |  102 (DEHAM Out@624.0) - 366 (TRMER In@747.0/Out@791.0) - 267 (TRALI In@836.0/Out@854.0) - 375 (AEJEA In@1235.0/Out@12
###  |  59.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |- Vessel  4 has the path [103, 362, 409, -4]
###  |  103 (DEHAM Out@792.0) - 362 (TRAMB In@1416.0/Out@1443.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Serv
###  |  ice 81)
###  |- Vessel 13 has the path [104, 291, 202, 371, 424, 404, -4]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 371 (AEJEA In@563.0/Out@587
###  |  .0) - 424 (PKBQM In@644.0/Out@674.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 371,  108.0 containers from 424 to 404
###  |   |- and carried Demand 376,    1.0 containers from 424 to 404
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel 14 has the path [105, 272, 372, 425, 405, -4]
###  |  105 (DKAAR Out@388.0) - 272 (TRZMK In@617.0/Out@631.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 361,   44.0 containers from 425 to 405
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |
### Service 121 (SAE) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [223, 302, -2]
###  |  223 (NLRTM Out@596.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |- Vessel 16 has the path [136, 315, 311, 301, -2]
###  |  136 (FRLEH Out@326.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 290, 203, 235, 0]
###  |  218 (NLRTM Out@92.0) - 290 (MAPTM In@211.0/Out@230.0) - 203 (MAPTM In@543.0/Out@564.0) - 235 (PABLB In@1472.0/Out@148
###  |  4.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [134, 200, 275, 204, 234, 0]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 275 (ESALG In@328.0/Out@347.0) - 204 (MAPTM In@711.0/Out@732
###  |  .0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 131,  580.0 containers from 200 to 275
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
rho[81,18] = 1
rho[177,19] = 1
rho[24,19] = 1
rho[121,19] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[24,19,1] = 1
eta[24,19,2] = 1
eta[121,19,1] = 1
eta[121,19,2] = 1
y[1,426,406] = 1
y[1,373,426] = 1
y[1,406,-4] = 1
y[1,100,266] = 1
y[1,266,373] = 1
y[2,101,273] = 1
y[2,407,-4] = 1
y[2,273,374] = 1
y[2,427,407] = 1
y[2,374,427] = 1
y[3,375,428] = 1
y[3,366,267] = 1
y[3,428,408] = 1
y[3,267,375] = 1
y[3,408,-4] = 1
y[3,102,366] = 1
y[4,362,409] = 1
y[4,103,362] = 1
y[4,409,-4] = 1
y[5,218,290] = 1
y[5,290,203] = 1
y[5,203,235] = 1
y[5,235,0] = 1
y[6,113,276] = 1
y[6,276,-1] = 1
y[6,135,201] = 1
y[6,201,113] = 1
y[6,219,135] = 1
y[7,277,-1] = 1
y[7,221,277] = 1
y[8,302,-2] = 1
y[8,223,302] = 1
y[9,274,199] = 1
y[9,1,274] = 1
y[9,368,331] = 1
y[9,331,-3] = 1
y[9,45,368] = 1
y[9,111,284] = 1
y[9,199,111] = 1
y[9,284,45] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,367,330] = 1
y[12,8,367] = 1
y[12,330,-3] = 1
y[13,424,404] = 1
y[13,404,-4] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,371,424] = 1
y[13,202,371] = 1
y[14,372,425] = 1
y[14,425,405] = 1
y[14,405,-4] = 1
y[14,105,272] = 1
y[14,272,372] = 1
y[15,234,0] = 1
y[15,275,204] = 1
y[15,204,234] = 1
y[15,134,200] = 1
y[15,200,275] = 1
y[16,301,-2] = 1
y[16,315,311] = 1
y[16,311,301] = 1
y[16,136,315] = 1
y[17,328,-3] = 1
y[17,336,353] = 1
y[17,353,230] = 1
y[17,230,328] = 1
y[17,145,335] = 1
y[17,335,336] = 1
y[18,329,-3] = 1
y[18,148,338] = 1
y[18,339,329] = 1
y[18,338,339] = 1
xD[7,111,284] = 161
xD[7,199,111] = 161
xD[7,284,45] = 161
xD[8,199,111] = 580
xD[13,135,201] = 74
xD[13,201,113] = 74
xD[13,219,135] = 74
xD[14,219,135] = 5
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[117,199,111] = 580
xD[118,135,201] = 74
xD[118,201,113] = 74
xD[118,219,135] = 74
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[123,221,277] = 68
xD[131,200,275] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[201,353,230] = 111
xD[201,230,328] = 111
xD[202,353,230] = 35
xD[202,230,328] = 35
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
xD[211,336,353] = 54
xD[211,353,230] = 54
xD[211,230,328] = 54
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[361,425,405] = 44
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[371,424,404] = 108
xD[376,424,404] = 1
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[45] = 765
zE[111] = 253
zE[113] = 421
zE[135] = 279
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[272] = 617
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[335] = 621
zE[336] = 699
zE[338] = 867
zE[339] = 957
zE[353] = 878
zE[362] = 1416
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[367] = 915
zE[368] = 1083
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[424] = 644
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[113] = 433
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[145] = 302
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[272] = 631
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[335] = 637
zX[336] = 718
zX[338] = 886
zX[339] = 973
zX[353] = 890
zX[362] = 1443
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[367] = 959
zX[368] = 1127
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[424] = 674
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
phi[1] = 802
phi[2] = 802
phi[3] = 802
phi[4] = 802
phi[5] = 1392
phi[6] = 255
phi[7] = 255
phi[8] = 583
phi[9] = 1497
phi[10] = 489
phi[11] = 489
phi[12] = 825
phi[13] = 534
phi[14] = 534
phi[15] = 1158
phi[16] = 685
phi[17] = 757
phi[18] = 757

### All variables with value smaller zero:

### End

real	1448m46.349s
user	4945m30.029s
sys	824m8.424s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
