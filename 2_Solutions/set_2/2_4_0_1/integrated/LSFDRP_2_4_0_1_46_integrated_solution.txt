	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_1_fr.p
Instance LSFDP: LSFDRP_2_4_0_1_fd_46.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_1_fd_46 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 10650 rows and 1127 columns (presolve time = 5s) ...
Presolve removed 76104 rows and 4384394 columns (presolve time = 11s) ...
Presolve removed 116409 rows and 5743382 columns (presolve time = 15s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 21s) ...
Presolve removed 166986 rows and 6806027 columns (presolve time = 25s) ...
Presolve removed 168598 rows and 6807021 columns (presolve time = 30s) ...
Presolve removed 169401 rows and 6807829 columns (presolve time = 36s) ...
Presolve removed 169460 rows and 6808029 columns (presolve time = 40s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 53s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 100s) ...
Presolve removed 169716 rows and 6808346 columns (presolve time = 101s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 106s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 965s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 964.61s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309380 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.286e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.835e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.45953152e+15  8.57710284e+16  1.49e+08 7.27e+06  1.18e+13  1073s
   1  -2.44962159e+15  8.39387276e+16  8.21e+07 1.43e+08  6.67e+12  1130s
   2  -1.61498050e+15  8.09261771e+16  5.42e+07 9.14e+07  4.46e+12  1187s
   3  -1.21078910e+15  7.60837178e+16  4.06e+07 5.35e+07  3.31e+12  1244s
   4  -6.20951340e+14  6.91765167e+16  2.09e+07 2.73e+07  1.73e+12  1302s

Barrier performed 4 iterations in 1314.13 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.33s

Solved with dual simplex

Root relaxation: objective 1.165288e+07, 127122 iterations, 342.62 seconds
Total elapsed time = 1316.16s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1552e+07    0  475          - 1.1552e+07      -     - 1453s
     0     0 7040369.48    0  496          - 7040369.48      -     - 79996s
     0     0 6588700.90    0  497          - 6588700.90      -     - 80092s
     0     0 5408563.40    0  726          - 5408563.40      -     - 84965s
     0     0 5407321.36    0  729          - 5407321.36      -     - 85326s
     0     0 5406349.54    0  735          - 5406349.54      -     - 85556s
     0     0 5406193.76    0  735          - 5406193.76      -     - 85565s
     0     0          -    0               - 5406193.76      -     - 86400s

Cutting planes:
  Cover: 2
  Implied bound: 8
  Clique: 93
  MIR: 18
  Zero half: 8

Explored 1 nodes (4645912 simplex iterations) in 86400.10 seconds
Thread count was 4 (of 16 available processors)

Solution count 0

Time limit reached
Best objective -, best bound 5.406193757402e+06, gap -
Traceback (most recent call last):
  File "lsfdrp_mathmodel.py", line 605, in <module>
    solve_lsifdp(instance, data)
  File "lsfdrp_mathmodel.py", line 465, in solve_lsifdp
    if var.x > 0.1:
  File "var.pxi", line 76, in gurobipy.Var.__getattr__
  File "var.pxi", line 142, in gurobipy.Var.getAttr
AttributeError: b"Unable to retrieve attribute 'x'"

real	1448m19.699s
user	4837m31.573s
sys	930m54.305s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
