	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_1_fr.p
Instance LSFDP: LSFDRP_2_4_0_1_fd_40.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_1_fd_40 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 13437 rows and 6312 columns (presolve time = 6s) ...
Presolve removed 76104 rows and 4384394 columns (presolve time = 11s) ...
Presolve removed 119678 rows and 5885077 columns (presolve time = 15s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 21s) ...
Presolve removed 167259 rows and 6806027 columns (presolve time = 25s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 30s) ...
Presolve removed 169401 rows and 6807973 columns (presolve time = 35s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 40s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 49s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 93s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 95s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 984s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 984.11s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.45190278e+15  7.45828363e+16  1.49e+08 6.32e+06  1.03e+13  1085s
   1  -2.44532378e+15  7.29863446e+16  8.20e+07 1.24e+08  5.79e+12  1141s
   2  -1.61197987e+15  7.03630267e+16  5.41e+07 7.95e+07  3.88e+12  1197s
   3  -1.20849093e+15  6.61486361e+16  4.06e+07 4.65e+07  2.88e+12  1252s
   4  -6.19654245e+14  6.01165305e+16  2.09e+07 2.37e+07  1.50e+12  1309s

Barrier performed 4 iterations in 1309.46 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 26.41s

Solved with dual simplex

Root relaxation: objective 1.170883e+07, 116154 iterations, 318.81 seconds
Total elapsed time = 1310.69s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1613e+07    0  499          - 1.1613e+07      -     - 1468s
     0     0 9650703.42    0  670          - 9650703.42      -     - 2172s
     0     0 9257634.87    0  680          - 9257634.87      -     - 2288s
     0     0 8145140.34    0  736          - 8145140.34      -     - 3115s
     0     0 8145047.55    0  726          - 8145047.55      -     - 3124s
     0     0 5096631.54    0  536          - 5096631.54      -     - 4179s
     0     0 5070665.37    0  556          - 5070665.37      -     - 4246s
     0     0 5067564.96    0  559          - 5067564.96      -     - 4274s
     0     0 5049475.60    0  576          - 5049475.60      -     - 4439s
     0     0 5049450.44    0  570          - 5049450.44      -     - 4442s
     0     0 4963024.97    0  551          - 4963024.97      -     - 4676s
     0     0 4963024.97    0  552          - 4963024.97      -     - 4684s
     0     0 4958680.27    0  592          - 4958680.27      -     - 4800s
     0     0 4957832.05    0  582          - 4957832.05      -     - 4812s
     0     0 4957735.21    0  580          - 4957735.21      -     - 4820s
     0     0 4940174.80    0  603          - 4940174.80      -     - 4919s
     0     0 4939829.90    0  619          - 4939829.90      -     - 4935s
     0     0 4937340.01    0  611          - 4937340.01      -     - 4955s
     0     0 4936511.59    0  614          - 4936511.59      -     - 4967s
     0     0 4935997.47    0  618          - 4935997.47      -     - 4977s
     0     0 4934447.93    0  590          - 4934447.93      -     - 4988s
     0     0 4934428.02    0  618          - 4934428.02      -     - 5007s
     0     0 4921348.59    0  613          - 4921348.59      -     - 5074s
     0     0 4919726.45    0  632          - 4919726.45      -     - 5092s
     0     0 4919231.98    0  624          - 4919231.98      -     - 5100s
     0     0 4917515.02    0  640          - 4917515.02      -     - 5112s
     0     0 4917509.78    0  639          - 4917509.78      -     - 5119s
     0     0 4916588.85    0  645          - 4916588.85      -     - 5126s
     0     0 4916472.56    0  642          - 4916472.56      -     - 5134s
     0     0 4915165.75    0  612          - 4915165.75      -     - 5144s
H    0     0                    -3.60015e+08 4915165.75   101%     - 5144s
     0     0 4915165.75    0  611 -3.600e+08 4915165.75   101%     - 5469s
H    0     0                    -2.87148e+07 4915165.75   117%     - 8024s
     0     2 4915165.75    0  607 -2.871e+07 4915165.75   117%     - 8055s
     1     4 -442372.08    1  682 -2.871e+07 4826165.18   117% 1461358 46435s
     3     4 infeasible    2      -2.871e+07 -2739590.5  90.5% 495535 50143s
     7     8 -1.516e+07    3  575 -2.871e+07 -2855265.6  90.1% 239124 74724s
    11    12 -1.518e+07    4  570 -2.871e+07 -4825588.9  83.2% 260091 76159s
    15    12 -1.528e+07    5  576 -2.871e+07 -4825588.9  83.2% 199185 77453s
    19    16 -1.537e+07    6  573 -2.871e+07 -6185966.8  78.5% 164234 79207s
    23    17 -1.539e+07    7  600 -2.871e+07 -6185994.2  78.5% 143066 80485s
    31    25 -8702325.9    7  536 -2.871e+07 -7645667.7  73.4% 108968 86400s

Cutting planes:
  Implied bound: 13
  Clique: 26
  MIR: 56
  Zero half: 9

Explored 35 nodes (3908450 simplex iterations) in 86400.53 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -2.87148e+07 -3.60015e+08 

Time limit reached
Best objective -2.871475732729e+07, best bound -7.645667748893e+06, gap 73.3737%

***
***  |- Calculation finished, timelimit reached 86,400.63 (86400.63) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -28,714,757.3273 (-28714757.3273) and is feasible with a Gap of 0.7337
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 111, 284, 45, 368, 331, -3]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 111 (ESALG In@253.0/Out@265.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@765.0/Out@811.0) - 368 (TRMER In@10
###  |  83.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 111
###  |   |- and carried Demand 117,  580.0 containers from 199 to 111
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 111
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 328, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 17 has the path [145, 335, 338, 339, 329, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973
###  |  .0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel 18 has the path [148, 367, 330, -3]
###  |  148 (GBFXT Out@470.0) - 367 (TRMER In@915.0/Out@959.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 277, -1]
###  |  100 (DEHAM Out@288.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 276, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 276 (ESALG In@496.0/Out@515
###  |  .0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 134,  484.0 containers from 202 to 276
###  |   |- and carried Demand 135,   18.0 containers from 202 to 276
###  |   |- and carried Demand 140,  484.0 containers from 291 to 276
###  |   |- and carried Demand 141,   18.0 containers from 291 to 276
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 347, 344, 324, 352, 229, 353, 230, 406, -4]
###  |  218 (NLRTM Out@92.0) - 347 (INPAV In@467.0/Out@494.0) - 344 (INNSA In@522.0/Out@546.0) - 324 (AEJEA In@617.0/Out@642.
###  |  0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In
###  |  @889.0/Out@904.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |- Vessel  6 has the path [219, 135, 201, 371, 424, 404, -4]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 371 (AEJEA In@563.0/Out@587
###  |  .0) - 424 (PKBQM In@644.0/Out@674.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand 371,  108.0 containers from 424 to 404
###  |   |- and carried Demand 376,    1.0 containers from 424 to 404
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel  7 has the path [221, 137, 203, 373, 426, 354, 376, 429, 409, -4]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 373 (AEJEA In@899.0/Out@923
###  |  .0) - 426 (PKBQM In@980.0/Out@1010.0) - 354 (OMSLL In@1300.0/Out@1313.0) - 376 (AEJEA In@1403.0/Out@1427.0) - 429 (PK
###  |  BQM In@1484.0/Out@1514.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |   |- and carried Demand 342,  254.0 containers from 426 to 409
###  |   |- and carried Demand 343,    1.0 containers from 426 to 409
###  |   |- and carried Demand 344,  135.0 containers from 426 to 409
###  |   |- and carried Demand 347,   75.0 containers from 373 to 409
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |   |- and carried Demand 350,   26.0 containers from 373 to 409
###  |   |- and carried Demand 351,  144.0 containers from 373 to 409
###  |   |- and carried Demand 352,    1.0 containers from 373 to 409
###  |- Vessel  8 has the path [223, 267, 374, 427, 407, -4]
###  |  223 (NLRTM Out@596.0) - 267 (TRALI In@836.0/Out@854.0) - 374 (AEJEA In@1067.0/Out@1091.0) - 427 (PKBQM In@1148.0/Out@
###  |  1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel 15 has the path [134, 200, 370, 423, 403, 422, 405, -4]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 370 (AEJEA In@395.0/Out@419.0) - 423 (PKBQM In@476.0/Out@506
###  |  .0) - 403 (INNSA In@570.0/Out@586.0) - 422 (OMSLL In@642.0/Out@661.0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_EN
###  |  D Ziel-Service 81)
###  |   |- and carried Demand 277,   81.0 containers from 423 to 422
###  |   |- and carried Demand 278,    2.0 containers from 423 to 422
###  |   |- and carried Demand 281,    1.0 containers from 423 to 403
###  |   |- and carried Demand 284,   63.0 containers from 370 to 423
###  |   |- and carried Demand 285,  174.0 containers from 370 to 423
###  |- Vessel 16 has the path [136, 268, 372, 425, 231, 375, 428, 408, -4]
###  |  136 (FRLEH Out@326.0) - 268 (TRAMB In@576.0/Out@603.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 231 (OMSLL In@1132.0/Out@1145.0) - 375 (AEJEA In@1235.0/Out@1259.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (I
###  |  NNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 362,  244.0 containers from 425 to 408
###  |   |- and carried Demand 363,   67.0 containers from 425 to 408
###  |   |- and carried Demand 365,  118.0 containers from 372 to 408
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |   |- and carried Demand 368,   15.0 containers from 372 to 408
###  |   |- and carried Demand 369,  267.0 containers from 372 to 408
###  |   |- and carried Demand 370,    1.0 containers from 372 to 408
###  |
### Service 121 (SAE) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  2 has the path [101, 315, 311, 301, -2]
###  |  101 (DEHAM Out@456.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |- Vessel  4 has the path [103, 312, 302, -2]
###  |  103 (DEHAM Out@792.0) - 312 (USMIA In@1103.0/Out@1111.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Serv
###  |  ice 121)
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 358, 273, 235, 0]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 14 has the path [105, 266, 234, 0]
###  |  105 (DKAAR Out@388.0) - 266 (TRALI In@668.0/Out@686.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
rho[81,19] = 1
rho[177,18] = 1
rho[24,18] = 1
rho[121,18] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
eta[81,19,1] = 1
eta[81,19,2] = 1
eta[81,19,3] = 1
eta[81,19,4] = 1
eta[81,19,5] = 1
eta[81,19,6] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[121,18,1] = 1
eta[121,18,2] = 1
y[1,277,-1] = 1
y[1,100,277] = 1
y[2,301,-2] = 1
y[2,315,311] = 1
y[2,101,315] = 1
y[2,311,301] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,235,0] = 1
y[3,273,235] = 1
y[4,103,312] = 1
y[4,312,302] = 1
y[4,302,-2] = 1
y[5,324,352] = 1
y[5,352,229] = 1
y[5,347,344] = 1
y[5,229,353] = 1
y[5,230,406] = 1
y[5,353,230] = 1
y[5,218,347] = 1
y[5,406,-4] = 1
y[5,344,324] = 1
y[6,424,404] = 1
y[6,404,-4] = 1
y[6,135,201] = 1
y[6,201,371] = 1
y[6,371,424] = 1
y[6,219,135] = 1
y[7,354,376] = 1
y[7,426,354] = 1
y[7,137,203] = 1
y[7,373,426] = 1
y[7,221,137] = 1
y[7,376,429] = 1
y[7,409,-4] = 1
y[7,429,409] = 1
y[7,203,373] = 1
y[8,407,-4] = 1
y[8,427,407] = 1
y[8,267,374] = 1
y[8,374,427] = 1
y[8,223,267] = 1
y[9,274,199] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,368,331] = 1
y[9,331,-3] = 1
y[9,45,368] = 1
y[9,290,111] = 1
y[9,111,284] = 1
y[9,284,45] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,328,-3] = 1
y[12,8,366] = 1
y[12,366,328] = 1
y[13,202,276] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,276,-1] = 1
y[14,105,266] = 1
y[14,234,0] = 1
y[14,266,234] = 1
y[15,403,422] = 1
y[15,405,-4] = 1
y[15,422,405] = 1
y[15,200,370] = 1
y[15,370,423] = 1
y[15,423,403] = 1
y[15,134,200] = 1
y[16,372,425] = 1
y[16,425,231] = 1
y[16,375,428] = 1
y[16,268,372] = 1
y[16,136,268] = 1
y[16,231,375] = 1
y[16,428,408] = 1
y[16,408,-4] = 1
y[17,329,-3] = 1
y[17,339,329] = 1
y[17,145,335] = 1
y[17,338,339] = 1
y[17,335,338] = 1
y[18,148,367] = 1
y[18,367,330] = 1
y[18,330,-3] = 1
xD[7,199,290] = 161
xD[7,290,111] = 161
xD[7,111,284] = 161
xD[7,284,45] = 161
xD[8,199,290] = 580
xD[8,290,111] = 580
xD[14,219,135] = 5
xD[26,221,137] = 6
xD[117,199,290] = 580
xD[117,290,111] = 580
xD[134,202,276] = 484
xD[135,202,276] = 18
xD[140,202,276] = 484
xD[140,291,202] = 484
xD[141,202,276] = 18
xD[141,291,202] = 18
xD[144,290,111] = 161
xD[144,111,284] = 161
xD[144,284,45] = 161
xD[145,290,111] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[209,344,324] = 280
xD[210,339,329] = 467
xD[277,403,422] = 81
xD[277,423,403] = 81
xD[278,403,422] = 2
xD[278,423,403] = 2
xD[281,423,403] = 1
xD[284,370,423] = 63
xD[285,370,423] = 174
xD[289,429,409] = 34
xD[290,376,429] = 84
xD[291,376,429] = 50
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[342,354,376] = 254
xD[342,426,354] = 254
xD[342,376,429] = 254
xD[342,429,409] = 254
xD[343,354,376] = 1
xD[343,426,354] = 1
xD[343,376,429] = 1
xD[343,429,409] = 1
xD[344,354,376] = 135
xD[344,426,354] = 135
xD[344,376,429] = 135
xD[344,429,409] = 135
xD[347,354,376] = 75
xD[347,426,354] = 75
xD[347,373,426] = 75
xD[347,376,429] = 75
xD[347,429,409] = 75
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[350,354,376] = 26
xD[350,426,354] = 26
xD[350,373,426] = 26
xD[350,376,429] = 26
xD[350,429,409] = 26
xD[351,354,376] = 144
xD[351,426,354] = 144
xD[351,373,426] = 144
xD[351,376,429] = 144
xD[351,429,409] = 144
xD[352,354,376] = 1
xD[352,426,354] = 1
xD[352,373,426] = 1
xD[352,376,429] = 1
xD[352,429,409] = 1
xD[362,425,231] = 244
xD[362,375,428] = 244
xD[362,231,375] = 244
xD[362,428,408] = 244
xD[363,425,231] = 67
xD[363,375,428] = 67
xD[363,231,375] = 67
xD[363,428,408] = 67
xD[365,372,425] = 118
xD[365,425,231] = 118
xD[365,375,428] = 118
xD[365,231,375] = 118
xD[365,428,408] = 118
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[368,372,425] = 15
xD[368,425,231] = 15
xD[368,375,428] = 15
xD[368,231,375] = 15
xD[368,428,408] = 15
xD[369,372,425] = 267
xD[369,425,231] = 267
xD[369,375,428] = 267
xD[369,231,375] = 267
xD[369,428,408] = 267
xD[370,372,425] = 1
xD[370,425,231] = 1
xD[370,375,428] = 1
xD[370,231,375] = 1
xD[370,428,408] = 1
xD[371,424,404] = 108
xD[376,424,404] = 1
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[45] = 765
zE[111] = 253
zE[135] = 279
zE[137] = 447
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[229] = 721
zE[230] = 889
zE[231] = 1132
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[273] = 785
zE[274] = 160
zE[276] = 496
zE[277] = 664
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[312] = 1103
zE[315] = 873
zE[324] = 617
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[335] = 621
zE[338] = 867
zE[339] = 957
zE[344] = 522
zE[347] = 467
zE[352] = 710
zE[353] = 878
zE[354] = 1300
zE[358] = 683
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[367] = 915
zE[368] = 1083
zE[370] = 395
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[376] = 1403
zE[403] = 570
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[422] = 642
zE[423] = 476
zE[424] = 644
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zE[429] = 1484
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[137] = 463
zX[145] = 302
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[231] = 1145
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[273] = 799
zX[274] = 179
zX[276] = 515
zX[277] = 683
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[312] = 1111
zX[315] = 881
zX[324] = 642
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[335] = 637
zX[338] = 886
zX[339] = 973
zX[344] = 546
zX[347] = 494
zX[352] = 722
zX[353] = 890
zX[354] = 1313
zX[358] = 731
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[367] = 959
zX[368] = 1127
zX[370] = 419
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[376] = 1427
zX[403] = 586
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[422] = 661
zX[423] = 506
zX[424] = 674
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
zX[429] = 1514
phi[1] = 395
phi[2] = 555
phi[3] = 860
phi[4] = 387
phi[5] = 998
phi[6] = 494
phi[7] = 1166
phi[8] = 662
phi[9] = 1497
phi[10] = 489
phi[11] = 489
phi[12] = 489
phi[13] = 295
phi[14] = 928
phi[15] = 764
phi[16] = 1100
phi[17] = 925
phi[18] = 925

### All variables with value smaller zero:

### End

real	1448m49.828s
user	4906m9.556s
sys	863m44.557s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
