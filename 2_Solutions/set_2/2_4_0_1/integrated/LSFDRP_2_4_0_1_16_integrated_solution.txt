	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_1_fr.p
Instance LSFDP: LSFDRP_2_4_0_1_fd_16.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_1_fd_16 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 1e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 13437 rows and 6312 columns (presolve time = 6s) ...
Presolve removed 76104 rows and 4384394 columns (presolve time = 10s) ...
Presolve removed 124352 rows and 5992965 columns (presolve time = 15s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 20s) ...
Presolve removed 167355 rows and 6806027 columns (presolve time = 25s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 30s) ...
Presolve removed 169457 rows and 6808029 columns (presolve time = 35s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 40s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 49s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 93s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 96s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 900s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 899.80s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309380 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.286e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.835e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.44282827e+15  2.98334616e+16  1.49e+08 2.53e+06  4.12e+12  1004s
   1  -2.43995622e+15  2.91961091e+16  8.21e+07 4.97e+07  2.32e+12  1058s
   2  -1.60785829e+15  2.81482614e+16  5.42e+07 3.18e+07  1.55e+12  1112s
   3  -1.20517503e+15  2.64627386e+16  4.06e+07 1.86e+07  1.15e+12  1166s
   4  -6.18186906e+14  2.40800028e+16  2.09e+07 9.58e+06  6.02e+11  1222s
   5  -2.96096176e+14  2.02296269e+16  1.01e+07 4.15e+06  2.93e+11  1276s
   6  -1.72285950e+14  1.49992085e+16  5.85e+06 1.75e+06  1.76e+11  1329s
   7  -4.45209129e+13  9.60948499e+15  1.51e+06 1.52e+05  4.82e+10  1384s

Barrier performed 7 iterations in 1383.99 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 30.07s

Solved with dual simplex

Root relaxation: objective 1.201373e+07, 144670 iterations, 482.05 seconds
Total elapsed time = 1391.08s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1902e+07    0  670          - 1.1902e+07      -     - 1531s
     0     0 1.0595e+07    0  587          - 1.0595e+07      -     - 2013s
     0     0 8823861.54    0  566          - 8823861.54      -     - 2143s
     0     0 8823667.72    0  566          - 8823667.72      -     - 2145s
     0     0 8181812.58    0  708          - 8181812.58      -     - 2456s
H    0     0                    -1.76848e+07 8181812.58   146%     - 2539s
     0     0 8162181.00    0  705 -1.768e+07 8162181.00   146%     - 2555s
     0     0 8161975.09    0  702 -1.768e+07 8161975.09   146%     - 2559s
     0     0 7953269.58    0  690 -1.768e+07 7953269.58   145%     - 2669s
     0     0 7938130.23    0  672 -1.768e+07 7938130.23   145%     - 2769s
     0     0 7938045.53    0  670 -1.768e+07 7938045.53   145%     - 2777s
     0     0 7705428.82    0  696 -1.768e+07 7705428.82   144%     - 3051s
     0     0 7703480.64    0  696 -1.768e+07 7703480.64   144%     - 3077s
     0     0 7700684.85    0  696 -1.768e+07 7700684.85   144%     - 3101s
     0     0 7700397.83    0  713 -1.768e+07 7700397.83   144%     - 3106s
     0     0 7676411.17    0  749 -1.768e+07 7676411.17   143%     - 3169s
     0     0 7676366.17    0  754 -1.768e+07 7676366.17   143%     - 3176s
     0     0 7673514.02    0  756 -1.768e+07 7673514.02   143%     - 3209s
     0     0 7672664.20    0  750 -1.768e+07 7672664.20   143%     - 3258s
     0     0 7671271.76    0  749 -1.768e+07 7671271.76   143%     - 3259s
     0     0 7670752.14    0  751 -1.768e+07 7670752.14   143%     - 3261s
     0     0 7670752.14    0  751 -1.768e+07 7670752.14   143%     - 3261s
     0     0 7665889.14    0  732 -1.768e+07 7665889.14   143%     - 3276s
     0     0 7665208.25    0  732 -1.768e+07 7665208.25   143%     - 3288s
     0     0 7665208.25    0  732 -1.768e+07 7665208.25   143%     - 3289s
     0     0 7664158.92    0  739 -1.768e+07 7664158.92   143%     - 3298s
     0     0 7663658.71    0  746 -1.768e+07 7663658.71   143%     - 42363s
     0     0 7663655.18    0  754 -1.768e+07 7663655.18   143%     - 42384s
     0     0 7661374.30    0  762 -1.768e+07 7661374.30   143%     - 43174s
     0     0 7661359.22    0  762 -1.768e+07 7661359.22   143%     - 43295s
     0     0 7661325.57    0  762 -1.768e+07 7661325.57   143%     - 43423s
     0     0 7661325.57    0  753 -1.768e+07 7661325.57   143%     - 44574s
     0     2 7661325.57    0  753 -1.768e+07 7661325.57   143%     - 59409s
     1     4 7555711.10    1  670 -1.768e+07 7555711.10   143% 205678 61224s
     3     7 1859568.50    2  739 -1.768e+07 7523645.31   143% 141067 86400s

Cutting planes:
  Implied bound: 15
  Clique: 43
  MIR: 32
  Flow cover: 10
  Zero half: 8

Explored 6 nodes (2888075 simplex iterations) in 86400.60 seconds
Thread count was 4 (of 16 available processors)

Solution count 1: -1.76848e+07 

Time limit reached
Best objective -1.768479302290e+07, best bound 7.442581328432e+06, gap 142.0846%

***
***  |- Calculation finished, timelimit reached 86,400.72 (86400.72) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -17,684,793.0229 (-17684793.0229) and is feasible with a Gap of 1.4208
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 290, 111, 284, 45, 368, 331, -3]
###  |  1 (BEANR Out@66.0) - 290 (MAPTM In@211.0/Out@230.0) - 111 (ESALG In@253.0/Out@265.0) - 284 (GWOXB In@463.0/Out@542.0)
###  |   - 45 (CMDLA In@765.0/Out@811.0) - 368 (TRMER In@1083.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_EN
###  |  D Ziel-Service 7)
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 111
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 266, 327, -3]
###  |  5 (BEANR Out@402.0) - 266 (TRALI In@668.0/Out@686.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 328, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 17 has the path [145, 335, 336, 337, 339, 330, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718.0) - 337 (EGPSD In@789.0/Out@805
###  |  .0) - 339 (EGPSD In@957.0/Out@973.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 18 has the path [148, 338, 329, -3]
###  |  148 (GBFXT Out@470.0) - 338 (EGPSD In@867.0/Out@886.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  6 has the path [219, 276, -1]
###  |  219 (NLRTM Out@260.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 118,   74.0 containers from 219 to 276
###  |- Vessel 15 has the path [134, 200, 112, 275, 201, 113, 277, -1]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347
###  |  .0) - 201 (MAPTM In@375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_EN
###  |  D Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 268, 404, -4]
###  |  100 (DEHAM Out@288.0) - 268 (TRAMB In@576.0/Out@603.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 
###  |  81)
###  |- Vessel  2 has the path [101, 365, 353, 230, 374, 427, 407, -4]
###  |  101 (DEHAM Out@456.0) - 365 (TRMER In@579.0/Out@623.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904
###  |  .0) - 374 (AEJEA In@1067.0/Out@1091.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel  3 has the path [102, 358, 273, 375, 428, 408, -4]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 375 (AEJEA In@1235.0/Out@12
###  |  59.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |- Vessel  4 has the path [103, 362, 409, -4]
###  |  103 (DEHAM Out@792.0) - 362 (TRAMB In@1416.0/Out@1443.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Serv
###  |  ice 81)
###  |- Vessel 13 has the path [104, 356, 334, 352, 229, 373, 426, 406, -4]
###  |  104 (DKAAR Out@220.0) - 356 (TRAMB In@347.0/Out@395.0) - 334 (EGPSD In@531.0/Out@550.0) - 352 (OMSLL In@710.0/Out@722
###  |  .0) - 229 (OMSLL In@721.0/Out@736.0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@1010.0) - 406 (INNSA 
###  |  In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel 14 has the path [105, 272, 372, 425, 405, -4]
###  |  105 (DKAAR Out@388.0) - 272 (TRZMK In@617.0/Out@631.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 361,   44.0 containers from 425 to 405
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |
### Service 121 (SAE) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  7 has the path [221, 315, 311, 301, -2]
###  |  221 (NLRTM Out@428.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |- Vessel 16 has the path [136, 202, 114, 302, -2]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0) - 302 (HNPCR In@1166.0/Out@11
###  |  79.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 133, 199, 370, 423, 403, 422, 267, 235, 0]
###  |  218 (NLRTM Out@92.0) - 133 (FRLEH In@111.0/Out@127.0) - 199 (MAPTM In@207.0/Out@228.0) - 370 (AEJEA In@395.0/Out@419.
###  |  0) - 423 (PKBQM In@476.0/Out@506.0) - 403 (INNSA In@570.0/Out@586.0) - 422 (OMSLL In@642.0/Out@661.0) - 267 (TRALI In
###  |  @836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand 277,   81.0 containers from 423 to 422
###  |   |- and carried Demand 278,    2.0 containers from 423 to 422
###  |   |- and carried Demand 281,    1.0 containers from 423 to 403
###  |   |- and carried Demand 284,   63.0 containers from 370 to 423
###  |   |- and carried Demand 285,  174.0 containers from 370 to 423
###  |- Vessel  8 has the path [223, 138, 204, 234, 0]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
rho[81,18] = 1
rho[177,19] = 1
rho[24,19] = 1
rho[121,19] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[24,19,1] = 1
eta[24,19,2] = 1
eta[121,19,1] = 1
eta[121,19,2] = 1
y[1,404,-4] = 1
y[1,100,268] = 1
y[1,268,404] = 1
y[2,365,353] = 1
y[2,407,-4] = 1
y[2,101,365] = 1
y[2,230,374] = 1
y[2,427,407] = 1
y[2,353,230] = 1
y[2,374,427] = 1
y[3,375,428] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,273,375] = 1
y[3,428,408] = 1
y[3,408,-4] = 1
y[4,362,409] = 1
y[4,103,362] = 1
y[4,409,-4] = 1
y[5,403,422] = 1
y[5,133,199] = 1
y[5,370,423] = 1
y[5,423,403] = 1
y[5,199,370] = 1
y[5,235,0] = 1
y[5,218,133] = 1
y[5,422,267] = 1
y[5,267,235] = 1
y[6,219,276] = 1
y[6,276,-1] = 1
y[7,301,-2] = 1
y[7,315,311] = 1
y[7,311,301] = 1
y[7,221,315] = 1
y[8,234,0] = 1
y[8,204,234] = 1
y[8,223,138] = 1
y[8,138,204] = 1
y[9,368,331] = 1
y[9,331,-3] = 1
y[9,1,290] = 1
y[9,45,368] = 1
y[9,290,111] = 1
y[9,111,284] = 1
y[9,284,45] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,327,-3] = 1
y[11,266,327] = 1
y[11,5,266] = 1
y[12,328,-3] = 1
y[12,8,366] = 1
y[12,366,328] = 1
y[13,356,334] = 1
y[13,352,229] = 1
y[13,426,406] = 1
y[13,373,426] = 1
y[13,334,352] = 1
y[13,229,373] = 1
y[13,406,-4] = 1
y[13,104,356] = 1
y[14,372,425] = 1
y[14,425,405] = 1
y[14,405,-4] = 1
y[14,105,272] = 1
y[14,272,372] = 1
y[15,200,112] = 1
y[15,112,275] = 1
y[15,275,201] = 1
y[15,277,-1] = 1
y[15,113,277] = 1
y[15,201,113] = 1
y[15,134,200] = 1
y[16,136,202] = 1
y[16,302,-2] = 1
y[16,202,114] = 1
y[16,114,302] = 1
y[17,337,339] = 1
y[17,336,337] = 1
y[17,339,330] = 1
y[17,145,335] = 1
y[17,335,336] = 1
y[17,330,-3] = 1
y[18,338,329] = 1
y[18,329,-3] = 1
y[18,148,338] = 1
xD[6,218,133] = 17
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[118,219,276] = 74
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[144,290,111] = 161
xD[144,111,284] = 161
xD[144,284,45] = 161
xD[145,290,111] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[206,338,329] = 467
xD[277,403,422] = 81
xD[277,423,403] = 81
xD[278,403,422] = 2
xD[278,423,403] = 2
xD[281,423,403] = 1
xD[284,370,423] = 63
xD[285,370,423] = 174
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[361,425,405] = 44
xD[366,372,425] = 133
xD[367,372,425] = 61
zE[45] = 765
zE[111] = 253
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[133] = 111
zE[138] = 615
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[204] = 711
zE[229] = 721
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[272] = 617
zE[273] = 785
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[284] = 463
zE[290] = 211
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[336] = 699
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[358] = 683
zE[362] = 1416
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[368] = 1083
zE[370] = 395
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[403] = 570
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[422] = 642
zE[423] = 476
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[133] = 127
zX[134] = 158
zX[136] = 326
zX[138] = 631
zX[145] = 302
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[272] = 631
zX[273] = 799
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[284] = 542
zX[290] = 230
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[336] = 718
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[358] = 731
zX[362] = 1443
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[368] = 1127
zX[370] = 419
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[403] = 586
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[422] = 661
zX[423] = 506
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
phi[1] = 466
phi[2] = 802
phi[3] = 802
phi[4] = 802
phi[5] = 1392
phi[6] = 255
phi[7] = 583
phi[8] = 720
phi[9] = 1497
phi[10] = 489
phi[11] = 489
phi[12] = 489
phi[13] = 870
phi[14] = 534
phi[15] = 525
phi[16] = 853
phi[17] = 1093
phi[18] = 757

### All variables with value smaller zero:

### End

real	1448m51.813s
user	4877m24.061s
sys	891m48.902s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
