	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_34.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_34 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 18952 rows and 800208 columns (presolve time = 5s) ...
Presolve removed 48555 rows and 2122493 columns (presolve time = 10s) ...
Presolve removed 70591 rows and 2743928 columns (presolve time = 15s) ...
Presolve removed 75978 rows and 2753864 columns (presolve time = 20s) ...
Presolve removed 78252 rows and 2757008 columns (presolve time = 25s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757047 columns (presolve time = 33s) ...
Presolve removed 78275 rows and 2757212 columns (presolve time = 56s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 430s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 430.22s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 30 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09469649e+15  8.55971939e+15  1.11e+08 2.52e+06  1.13e+12   496s
   1  -1.14501147e+15  8.30339363e+15  6.05e+07 1.74e+07  6.32e+11   531s
   2  -8.46488495e+14  7.82701287e+15  4.48e+07 1.13e+07  4.61e+11   567s

Barrier performed 2 iterations in 595.99 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.05s

Solved with dual simplex

Root relaxation: objective 7.954310e+06, 49191 iterations, 160.34 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7750993.41    0  343          - 7750993.41      -     -  640s
     0     0 6643354.94    0  381          - 6643354.94      -     -  707s
     0     0 6509965.63    0  427          - 6509965.63      -     -  773s
     0     0 6051562.72    0  392          - 6051562.72      -     -  811s
H    0     0                    -2.99345e+07 6051562.72   120%     -  813s
     0     0 6051562.72    0  392 -2.993e+07 6051562.72   120%     -  817s
     0     0 6018601.66    0  449 -2.993e+07 6018601.66   120%     -  847s
H    0     0                    -2.99297e+07 6018601.66   120%     -  848s
     0     0 6017305.29    0  465 -2.993e+07 6017305.29   120%     -  857s
     0     0 6017305.29    0  466 -2.993e+07 6017305.29   120%     -  858s
     0     0 6013486.51    0  470 -2.993e+07 6013486.51   120%     -  863s
H    0     0                    -2.98557e+07 6013486.51   120%     -  863s
     0     0 6012953.99    0  481 -2.986e+07 6012953.99   120%     -  883s
     0     0 6012953.99    0  481 -2.986e+07 6012953.99   120%     -  884s
     0     0 6011339.43    0  481 -2.986e+07 6011339.43   120%     -  895s
     0     0 6011329.93    0  480 -2.986e+07 6011329.93   120%     -  900s
     0     0 6011248.01    0  484 -2.986e+07 6011248.01   120%     -  904s
     0     0 6011238.41    0  488 -2.986e+07 6011238.41   120%     -  909s
     0     0 6010744.62    0  486 -2.986e+07 6010744.62   120%     -  914s
     0     0 6010744.62    0  488 -2.986e+07 6010744.62   120%     -  919s
     0     0 6010639.21    0  492 -2.986e+07 6010639.21   120%     -  922s
     0     0 6010637.89    0  493 -2.986e+07 6010637.89   120%     -  927s
     0     0 6010637.89    0  495 -2.986e+07 6010637.89   120%     -  929s
     0     0 6010637.89    0  495 -2.986e+07 6010637.89   120%     - 1114s
H    0     0                    -8462239.066 6010637.89   171%     - 1193s
H    0     0                    -8267532.186 6010637.89   173%     - 1208s
     0     2 6010637.89    0  495 -8267532.2 6010637.89   173%     - 1216s
     1     4 2249770.79    1  458 -8267532.2 6003602.98   173% 724762 22497s
     3     8 1628608.36    2  453 -8267532.2 5871709.71   171% 246600 23303s
     7    10 infeasible    3      -8267532.2 5450794.34   166% 109591 27123s
    11    12 -1049523.5    3  501 -8267532.2 5224816.35   163% 89788 50205s
    15    15 -1178240.9    4  521 -8267532.2 5224514.60   163% 139452 79112s
    19    17 -1288144.8    5  373 -8267532.2 5166160.65   162% 186156 86400s

Cutting planes:
  Implied bound: 5
  Clique: 21
  MIR: 32
  Flow cover: 1
  Zero half: 2

Explored 22 nodes (3710997 simplex iterations) in 86400.31 seconds
Thread count was 4 (of 16 available processors)

Solution count 5: -8.26753e+06 -8.46224e+06 -2.98557e+07 ... -2.99345e+07

Time limit reached
Best objective -8.267532186260e+06, best bound 5.166160647134e+06, gap 162.4873%

***
***  |- Calculation finished, timelimit reached 86,400.37 (86400.37) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -8,267,532.1863 (-8267532.1863) and is feasible with a Gap of 1.6249
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 347, 344, 324, 352, 229, 327, -3]
###  |  218 (NLRTM Out@92.0) - 347 (INPAV In@467.0/Out@494.0) - 344 (INNSA In@522.0/Out@546.0) - 324 (AEJEA In@617.0/Out@642.
###  |  0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END
###  |   Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |- Vessel  6 has the path [219, 364, 325, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel  7 has the path [221, 365, 353, 230, 329, -3]
###  |  221 (NLRTM Out@428.0) - 365 (TRMER In@579.0/Out@623.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904
###  |  .0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel  8 has the path [223, 366, 338, 339, 341, 330, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973
###  |  .0) - 341 (EGPSD In@1125.0/Out@1141.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 204,  188.0 containers from 341 to 330
###  |   |- and carried Demand 205,    7.0 containers from 341 to 330
###  |- Vessel 15 has the path [134, 200, 284, 45, 368, 331, -3]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@765.0/Out@811.
###  |  0) - 368 (TRMER In@1083.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  45,  161.0 containers from 200 to  45
###  |- Vessel 16 has the path [136, 357, 335, 336, 337, 328, -3]
###  |  136 (FRLEH Out@326.0) - 357 (TRAMB In@515.0/Out@563.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718
###  |  .0) - 337 (EGPSD In@789.0/Out@805.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 211,   54.0 containers from 336 to 328
###  |   |- and carried Demand 213,   54.0 containers from 337 to 328
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 290, 201, 113, 301, -2]
###  |  1 (BEANR Out@66.0) - 290 (MAPTM In@211.0/Out@230.0) - 201 (MAPTM In@375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0)
###  |   - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 275, 204, 234, 0]
###  |  2 (BEANR Out@234.0) - 275 (ESALG In@328.0/Out@347.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@1316
###  |  .0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 17 has the path [145, 291, 202, 285, 46, 37, 42, 240, 236, 0]
###  |  145 (GBFXT Out@302.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 285 (GWOXB In@631.0/Out@710
###  |  .0) - 46 (CMDLA In@796.0/Out@842.0) - 37 (CLLQN In@1408.0/Out@1426.0) - 42 (CLSAI In@1456.0/Out@1472.0) - 240 (PECLL 
###  |  In@1533.93/Out@1535.93) - 236 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 18 has the path [148, 267, 235, 0]
###  |  148 (GBFXT Out@470.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [4, 11, 13, 14]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,30] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,30,1] = 1
eta[177,30,2] = 1
eta[177,30,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,229,327] = 1
y[5,327,-3] = 1
y[5,324,352] = 1
y[5,352,229] = 1
y[5,347,344] = 1
y[5,218,347] = 1
y[5,344,324] = 1
y[6,364,325] = 1
y[6,219,364] = 1
y[6,325,-3] = 1
y[7,365,353] = 1
y[7,329,-3] = 1
y[7,221,365] = 1
y[7,353,230] = 1
y[7,230,329] = 1
y[8,223,366] = 1
y[8,339,341] = 1
y[8,366,338] = 1
y[8,341,330] = 1
y[8,338,339] = 1
y[8,330,-3] = 1
y[9,301,-2] = 1
y[9,290,201] = 1
y[9,1,290] = 1
y[9,201,113] = 1
y[9,113,301] = 1
y[10,234,0] = 1
y[10,275,204] = 1
y[10,204,234] = 1
y[10,2,275] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[15,200,284] = 1
y[15,368,331] = 1
y[15,331,-3] = 1
y[15,45,368] = 1
y[15,284,45] = 1
y[15,134,200] = 1
y[16,336,337] = 1
y[16,337,328] = 1
y[16,328,-3] = 1
y[16,357,335] = 1
y[16,335,336] = 1
y[16,136,357] = 1
y[17,37,42] = 1
y[17,285,46] = 1
y[17,145,291] = 1
y[17,240,236] = 1
y[17,42,240] = 1
y[17,46,37] = 1
y[17,291,202] = 1
y[17,202,285] = 1
y[17,236,0] = 1
y[18,148,267] = 1
y[18,235,0] = 1
y[18,267,235] = 1
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[45,200,284] = 161
xD[45,284,45] = 161
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[204,341,330] = 188
xD[205,341,330] = 7
xD[207,229,327] = 288
xD[207,352,229] = 288
xD[208,229,327] = 14
xD[208,352,229] = 14
xD[209,344,324] = 280
xD[211,336,337] = 54
xD[211,337,328] = 54
xD[213,337,328] = 54
zE[37] = 1408
zE[42] = 1456
zE[45] = 765
zE[46] = 796
zE[113] = 421
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[204] = 711
zE[229] = 721
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1533.93
zE[267] = 836
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[284] = 463
zE[285] = 631
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[324] = 617
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[335] = 621
zE[336] = 699
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[341] = 1125
zE[344] = 522
zE[347] = 467
zE[352] = 710
zE[353] = 878
zE[357] = 515
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[368] = 1083
zX[1] = 66
zX[2] = 234
zX[8] = 570
zX[37] = 1426
zX[42] = 1472
zX[45] = 811
zX[46] = 842
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[113] = 433
zX[134] = 158
zX[136] = 326
zX[145] = 302
zX[148] = 470
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1535.93
zX[267] = 854
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[284] = 542
zX[285] = 710
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[324] = 642
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[335] = 637
zX[336] = 718
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[341] = 1141
zX[344] = 546
zX[347] = 494
zX[352] = 722
zX[353] = 890
zX[357] = 563
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[368] = 1127
w[17,(240,236)] = 104.071
w[17,(42,240)] = 61.9294
phi[1] = 227
phi[2] = 227
phi[3] = 227
phi[5] = 799
phi[6] = 463
phi[7] = 799
phi[8] = 799
phi[9] = 945
phi[10] = 1082
phi[12] = 609
phi[15] = 1405
phi[16] = 733
phi[17] = 1350
phi[18] = 1014

### All variables with value smaller zero:

### End

real	1444m8.461s
user	4878m33.955s
sys	887m4.556s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
