	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_13.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_13 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 37611 rows and 1750586 columns (presolve time = 5s) ...
Presolve removed 70591 rows and 2743928 columns (presolve time = 10s) ...
Presolve removed 77879 rows and 2756626 columns (presolve time = 15s) ...
Presolve removed 78275 rows and 2757047 columns (presolve time = 20s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757212 columns (presolve time = 47s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 422s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 422.32s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 40 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09396123e+15  3.27287078e+15  1.11e+08 1.61e+06  4.32e+11   483s

Barrier performed 0 iterations in 512.50 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.37s

Solved with dual simplex

Root relaxation: objective 8.019044e+06, 43415 iterations, 85.02 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7811053.91    0  332          - 7811053.91      -     -  543s
     0     0 7000960.59    0  370          - 7000960.59      -     -  594s
H    0     0                    -1.84585e+07 7000960.59   138%     -  596s
H    0     0                    -1.79239e+07 7000960.59   139%     -  601s
     0     0 6873556.95    0  371 -1.792e+07 6873556.95   138%     -  601s
     0     0 6607400.17    0  382 -1.792e+07 6607400.17   137%     -  628s
     0     0 6603675.32    0  367 -1.792e+07 6603675.32   137%     -  638s
     0     0 6585675.38    0  396 -1.792e+07 6585675.38   137%     -  677s
     0     0 6585395.84    0  399 -1.792e+07 6585395.84   137%     -  682s
     0     0 6585395.84    0  399 -1.792e+07 6585395.84   137%     -  683s
     0     0 6582489.98    0  407 -1.792e+07 6582489.98   137%     -  696s
     0     0 6581735.80    0  409 -1.792e+07 6581735.80   137%     -  701s
     0     0 6581735.80    0  411 -1.792e+07 6581735.80   137%     -  701s
     0     0 6581696.95    0  417 -1.792e+07 6581696.95   137%     -  704s
     0     0 6581675.89    0  417 -1.792e+07 6581675.89   137%     -  707s
     0     0 6581514.69    0  420 -1.792e+07 6581514.69   137%     -  709s
     0     0 6581514.69    0  418 -1.792e+07 6581514.69   137%     -  840s
H    0     0                    -1.11475e+07 6581514.69   159%     - 1053s
H    0     2                    -4778031.585 6581514.69   238%     - 1511s
     0     2 6581514.69    0  415 -4778031.6 6581514.69   238%     - 1512s
     1     4 5365756.71    1  453 -4778031.6 6361294.59   233% 674228 14382s
     3     8 5328475.17    2  434 -4778031.6 5845263.85   222% 237491 19152s
     7    12 5242040.11    3  397 -4778031.6 5633308.01   218% 139964 19483s
    11    16 4440768.14    4  343 -4778031.6 5631911.22   218% 175802 37784s
    15    17 4387683.94    5  275 -4778031.6 5631911.22   218% 167547 55574s
    19    21 1063122.82    6  215 -4778031.6 5631911.22   218% 201239 69435s
    23    22 3402279.30    6  255 -4778031.6 5631911.22   218% 167551 86400s

Cutting planes:
  Implied bound: 5
  Clique: 22
  MIR: 26
  Flow cover: 1
  GUB cover: 1
  Zero half: 2
  Mod-K: 1

Explored 29 nodes (4418365 simplex iterations) in 86400.28 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -4.77803e+06 -1.11475e+07 -1.79239e+07 -1.84585e+07 

Time limit reached
Best objective -4.778031584611e+06, best bound 5.631911216822e+06, gap 217.8709%

***
***  |- Calculation finished, timelimit reached 86,400.33 (86400.33) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -4,778,031.5846 (-4778031.5846) and is feasible with a Gap of 2.1787
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 347, 344, 324, 352, 229, 327, -3]
###  |  218 (NLRTM Out@92.0) - 347 (INPAV In@467.0/Out@494.0) - 344 (INNSA In@522.0/Out@546.0) - 324 (AEJEA In@617.0/Out@642.
###  |  0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END
###  |   Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |- Vessel  6 has the path [219, 266, 353, 330, -3]
###  |  219 (NLRTM Out@260.0) - 266 (TRALI In@668.0/Out@686.0) - 353 (OMSLL In@878.0/Out@890.0) - 330 (AEJEA In@1362.0/Out@13
###  |  95.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel  7 has the path [221, 137, 203, 115, 273, 339, 342, 331, -3]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 115 (ESALG In@589.0/Out@601
###  |  .0) - 273 (TRZMK In@785.0/Out@799.0) - 339 (EGPSD In@957.0/Out@973.0) - 342 (EGPSD In@1293.0/Out@1309.0) - 331 (AEJEA
###  |   In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |   |- and carried Demand 197,  113.0 containers from 342 to 331
###  |   |- and carried Demand 198,   54.0 containers from 342 to 331
###  |- Vessel  8 has the path [223, 366, 328, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 356, 334, 335, 338, 329, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 334 (EGPSD In@531.0/Out@550.0) - 335 (EGPSD In@621.0/Out@637
###  |  .0) - 338 (EGPSD In@867.0/Out@886.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |- Vessel 16 has the path [136, 268, 325, -3]
###  |  136 (FRLEH Out@326.0) - 268 (TRAMB In@576.0/Out@603.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 277, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 275, 201, 113, 301, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@3
###  |  75.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  2 has the path [101, 267, 235, 0]
###  |  101 (DEHAM Out@456.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel 14 has the path [105, 357, 272, 234, 0]
###  |  105 (DKAAR Out@388.0) - 357 (TRAMB In@515.0/Out@563.0) - 272 (TRZMK In@617.0/Out@631.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [4, 10, 11, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,267] = 1
y[2,235,0] = 1
y[2,267,235] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,229,327] = 1
y[5,327,-3] = 1
y[5,324,352] = 1
y[5,352,229] = 1
y[5,347,344] = 1
y[5,218,347] = 1
y[5,344,324] = 1
y[6,219,266] = 1
y[6,353,330] = 1
y[6,266,353] = 1
y[6,330,-3] = 1
y[7,339,342] = 1
y[7,137,203] = 1
y[7,115,273] = 1
y[7,342,331] = 1
y[7,221,137] = 1
y[7,273,339] = 1
y[7,331,-3] = 1
y[7,203,115] = 1
y[8,223,366] = 1
y[8,328,-3] = 1
y[8,366,328] = 1
y[9,200,112] = 1
y[9,301,-2] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,112,275] = 1
y[9,275,201] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,201,113] = 1
y[9,113,301] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,277,-1] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,114,277] = 1
y[13,202,114] = 1
y[14,234,0] = 1
y[14,272,234] = 1
y[14,105,357] = 1
y[14,357,272] = 1
y[15,356,334] = 1
y[15,338,329] = 1
y[15,329,-3] = 1
y[15,134,356] = 1
y[15,334,335] = 1
y[15,335,338] = 1
y[16,136,268] = 1
y[16,268,325] = 1
y[16,325,-3] = 1
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[25,137,203] = 68
xD[25,221,137] = 68
xD[25,203,115] = 68
xD[26,221,137] = 6
xD[29,203,115] = 60
xD[30,203,115] = 11
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[123,137,203] = 68
xD[123,221,137] = 68
xD[123,203,115] = 68
xD[126,203,115] = 60
xD[127,203,115] = 11
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[197,342,331] = 113
xD[198,342,331] = 54
xD[206,338,329] = 467
xD[207,229,327] = 288
xD[207,352,229] = 288
xD[208,229,327] = 14
xD[208,352,229] = 14
xD[209,344,324] = 280
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[115] = 589
zE[137] = 447
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[229] = 721
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[272] = 617
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[324] = 617
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[338] = 867
zE[339] = 957
zE[342] = 1293
zE[344] = 522
zE[347] = 467
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[357] = 515
zE[366] = 747
zX[1] = 66
zX[8] = 570
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[115] = 601
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[272] = 631
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[324] = 642
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[338] = 886
zX[339] = 973
zX[342] = 1309
zX[344] = 546
zX[347] = 494
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[357] = 563
zX[366] = 791
phi[1] = 227
phi[2] = 1028
phi[3] = 227
phi[5] = 799
phi[6] = 1135
phi[7] = 1135
phi[8] = 463
phi[9] = 945
phi[12] = 609
phi[13] = 463
phi[14] = 928
phi[15] = 1069
phi[16] = 397

### All variables with value smaller zero:

### End

real	1443m50.257s
user	4919m32.203s
sys	846m42.498s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
