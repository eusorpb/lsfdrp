	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_22.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_22 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 33971 rows and 1627817 columns (presolve time = 5s) ...
Presolve removed 70591 rows and 2743928 columns (presolve time = 10s) ...
Presolve removed 77879 rows and 2756626 columns (presolve time = 15s) ...
Presolve removed 78275 rows and 2757047 columns (presolve time = 20s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757212 columns (presolve time = 48s) ...
Presolve removed 78506 rows and 2757232 columns (presolve time = 50s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 436s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 436.19s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 22
 AA' NZ     : 2.915e+06
 Factor NZ  : 9.237e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.723e+11 (roughly 30 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09427633e+15  5.53866304e+15  1.11e+08 1.63e+06  7.31e+11   496s
   1  -1.14457140e+15  5.37275100e+15  6.05e+07 1.12e+07  4.09e+11   529s

Barrier performed 1 iterations in 535.72 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.08s

Solved with dual simplex

Root relaxation: objective 7.983951e+06, 47160 iterations, 95.18 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7790330.09    0  346          - 7790330.09      -     -  569s
     0     0 6833059.58    0  380          - 6833059.58      -     -  705s
H    0     0                    -2.34864e+07 6833059.58   129%     -  707s
H    0     0                    -2.28709e+07 6833059.58   130%     -  712s
     0     0 6700629.37    0  377 -2.287e+07 6700629.37   129%     -  713s
     0     0 6327375.55    0  442 -2.287e+07 6327375.55   128%     -  750s
H    0     0                    -2.07071e+07 6327375.55   131%     -  751s
     0     0 6321544.79    0  387 -2.071e+07 6321544.79   131%     -  758s
     0     0 6321544.79    0  387 -2.071e+07 6321544.79   131%     -  759s
     0     0 6303084.61    0  425 -2.071e+07 6303084.61   130%     -  767s
H    0     0                    -2.01132e+07 6303084.61   131%     -  769s
     0     0 6303084.61    0  426 -2.011e+07 6303084.61   131%     -  772s
     0     0 6302883.60    0  416 -2.011e+07 6302883.60   131%     -  775s
     0     0 6302856.80    0  433 -2.011e+07 6302856.80   131%     -  779s
     0     0 6302492.90    0  424 -2.011e+07 6302492.90   131%     -  782s
     0     0 6302272.56    0  431 -2.011e+07 6302272.56   131%     -  786s
     0     0 6302272.56    0  431 -2.011e+07 6302272.56   131%     -  786s
     0     0 6302078.18    0  434 -2.011e+07 6302078.18   131%     -  789s
     0     0 6301998.71    0  434 -2.011e+07 6301998.71   131%     -  794s
     0     0 6301832.42    0  448 -2.011e+07 6301832.42   131%     -  796s
     0     0 6301832.42    0  445 -2.011e+07 6301832.42   131%     -  946s
H    0     0                    -8994290.743 6301832.42   170%     - 1001s
H    0     0                    -8417907.127 6301832.42   175%     - 1013s
H    0     2                    -7725832.879 6301832.42   182%     - 1143s
     0     2 6301832.42    0  442 -7725832.9 6301832.42   182%     - 1143s
     1     4 4349742.50    1  383 -7725832.9 6152142.67   180% 954005 19367s
     3     8 4238445.62    2  383 -7725832.9 5692098.27   174% 328571 24634s
     7    10 infeasible    3      -7725832.9 5492523.74   171% 176783 24803s
    11    14 3263501.46    3  367 -7725832.9 5464036.32   171% 116311 27630s
    15    15 2698464.38    4  296 -7725832.9 5245320.06   168% 237208 67753s
    19    19 2617744.30    4  300 -7725832.9 5245320.06   168% 188250 68716s
    23    19 1444572.89    5  336 -7725832.9 5245320.06   168% 191138 82207s
    27    23 1343303.18    6  288 -7725832.9 5245320.06   168% 178270 85912s
    32    28 -4155037.6    7  354 -7725832.9 5245320.06   168% 160144 86400s

Cutting planes:
  Implied bound: 8
  Clique: 25
  MIR: 33
  Flow cover: 1
  GUB cover: 1
  Zero half: 4

Explored 37 nodes (5216047 simplex iterations) in 86400.29 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -7.72583e+06 -8.41791e+06 -8.99429e+06 ... -2.34864e+07

Time limit reached
Best objective -7.725832878513e+06, best bound 5.245320055919e+06, gap 167.8933%

***
***  |- Calculation finished, timelimit reached 86,400.35 (86400.35) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -7,725,832.8785 (-7725832.8785) and is feasible with a Gap of 1.6789
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 347, 344, 324, 352, 230, 330, -3]
###  |  218 (NLRTM Out@92.0) - 347 (INPAV In@467.0/Out@494.0) - 344 (INNSA In@522.0/Out@546.0) - 324 (AEJEA In@617.0/Out@642.
###  |  0) - 352 (OMSLL In@710.0/Out@722.0) - 230 (OMSLL In@889.0/Out@904.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_E
###  |  ND Ziel-Service 7)
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |- Vessel  6 has the path [219, 364, 334, 335, 337, 340, 341, 342, 331, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 334 (EGPSD In@531.0/Out@550.0) - 335 (EGPSD In@621.0/Out@637
###  |  .0) - 337 (EGPSD In@789.0/Out@805.0) - 340 (EGPSD In@1035.0/Out@1054.0) - 341 (EGPSD In@1125.0/Out@1141.0) - 342 (EGP
###  |  SD In@1293.0/Out@1309.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 197,  113.0 containers from 342 to 331
###  |   |- and carried Demand 198,   54.0 containers from 342 to 331
###  |- Vessel  7 has the path [221, 365, 336, 338, 339, 329, -3]
###  |  221 (NLRTM Out@428.0) - 365 (TRMER In@579.0/Out@623.0) - 336 (EGPSD In@699.0/Out@718.0) - 338 (EGPSD In@867.0/Out@886
###  |  .0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel  8 has the path [223, 366, 328, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [136, 202, 114, 161, 269, 327, -3]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0) - 161 (GRPIR In@696.0/Out@721
###  |  .0) - 269 (TRAMB In@744.0/Out@771.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 275, 201, 113, 301, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@3
###  |  75.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel 13 has the path [104, 291, 204, 234, 0]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 14 has the path [105, 267, 235, 0]
###  |  105 (DKAAR Out@388.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [4, 10, 11, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,324,352] = 1
y[5,352,230] = 1
y[5,347,344] = 1
y[5,230,330] = 1
y[5,218,347] = 1
y[5,330,-3] = 1
y[5,344,324] = 1
y[6,364,334] = 1
y[6,342,331] = 1
y[6,337,340] = 1
y[6,341,342] = 1
y[6,331,-3] = 1
y[6,219,364] = 1
y[6,340,341] = 1
y[6,335,337] = 1
y[6,334,335] = 1
y[7,336,338] = 1
y[7,329,-3] = 1
y[7,221,365] = 1
y[7,339,329] = 1
y[7,365,336] = 1
y[7,338,339] = 1
y[8,223,366] = 1
y[8,328,-3] = 1
y[8,366,328] = 1
y[9,200,112] = 1
y[9,301,-2] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,112,275] = 1
y[9,275,201] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,201,113] = 1
y[9,113,301] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,234,0] = 1
y[13,104,291] = 1
y[13,204,234] = 1
y[13,291,204] = 1
y[14,105,267] = 1
y[14,235,0] = 1
y[14,267,235] = 1
y[15,356,325] = 1
y[15,134,356] = 1
y[15,325,-3] = 1
y[16,327,-3] = 1
y[16,136,202] = 1
y[16,161,269] = 1
y[16,269,327] = 1
y[16,114,161] = 1
y[16,202,114] = 1
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[197,342,331] = 113
xD[198,342,331] = 54
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[209,344,324] = 280
xD[210,339,329] = 467
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[161] = 696
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[204] = 711
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[267] = 836
zE[269] = 744
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[324] = 617
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[336] = 699
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[341] = 1125
zE[342] = 1293
zE[344] = 522
zE[347] = 467
zE[352] = 710
zE[356] = 347
zE[364] = 411
zE[365] = 579
zE[366] = 747
zX[1] = 66
zX[8] = 570
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[134] = 158
zX[136] = 326
zX[161] = 721
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[267] = 854
zX[269] = 771
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[324] = 642
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[336] = 718
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[341] = 1141
zX[342] = 1309
zX[344] = 546
zX[347] = 494
zX[352] = 722
zX[356] = 395
zX[364] = 455
zX[365] = 623
zX[366] = 791
phi[1] = 227
phi[2] = 227
phi[3] = 227
phi[5] = 1303
phi[6] = 1303
phi[7] = 799
phi[8] = 463
phi[9] = 945
phi[12] = 609
phi[13] = 1096
phi[14] = 1096
phi[15] = 565
phi[16] = 565

### All variables with value smaller zero:

### End

real	1444m17.370s
user	4887m12.087s
sys	879m10.847s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
