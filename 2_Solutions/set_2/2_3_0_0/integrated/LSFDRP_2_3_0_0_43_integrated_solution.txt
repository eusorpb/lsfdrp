	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_43 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 5e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 33971 rows and 1627817 columns (presolve time = 5s) ...
Presolve removed 66215 rows and 2656507 columns (presolve time = 10s) ...
Presolve removed 75424 rows and 2753798 columns (presolve time = 15s) ...
Presolve removed 78252 rows and 2757008 columns (presolve time = 21s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757047 columns (presolve time = 27s) ...
Presolve removed 78275 rows and 2757212 columns (presolve time = 51s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 430s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 430.38s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 34 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09501162e+15  1.08255116e+16  1.11e+08 3.18e+06  1.43e+12   495s
   1  -1.14532011e+15  1.05013756e+16  6.05e+07 2.20e+07  7.99e+11   530s

Barrier performed 1 iterations in 561.21 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.21s

Solved with dual simplex

Root relaxation: objective 7.953903e+06, 48890 iterations, 125.95 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7828116.44    0  342          - 7828116.44      -     -  584s
     0     0 6788508.81    0  317          - 6788508.81      -     -  646s
     0     0 6526003.25    0  395          - 6526003.25      -     -  682s
     0     0 6526003.25    0  395          - 6526003.25      -     -  683s
     0     0 5990447.80    0  409          - 5990447.80      -     -  713s
H    0     0                    -3.73045e+07 5990447.80   116%     -  715s
     0     0 5934023.61    0  418 -3.730e+07 5934023.61   116%     -  731s
     0     0 5934023.61    0  423 -3.730e+07 5934023.61   116%     -  733s
     0     0 5920174.52    0  404 -3.730e+07 5920174.52   116%     -  742s
H    0     0                    -3.72921e+07 5920174.52   116%     -  743s
     0     0 5919896.34    0  384 -3.729e+07 5919896.34   116%     -  749s
     0     0 5919896.34    0  385 -3.729e+07 5919896.34   116%     -  750s
     0     0 5918847.29    0  383 -3.729e+07 5918847.29   116%     -  756s
H    0     0                    -1.58919e+07 5918847.29   137%     -  757s
     0     0 5917882.94    0  418 -1.589e+07 5917882.94   137%     -  762s
     0     0 5917818.51    0  421 -1.589e+07 5917818.51   137%     -  764s
     0     0 5915727.75    0  422 -1.589e+07 5915727.75   137%     -  770s
     0     0 5915218.59    0  430 -1.589e+07 5915218.59   137%     -  776s
     0     0 5915102.48    0  431 -1.589e+07 5915102.48   137%     -  777s
     0     0 5913994.52    0  435 -1.589e+07 5913994.52   137%     -  782s
     0     0 5913993.49    0  437 -1.589e+07 5913993.49   137%     -  787s
     0     0 5913701.53    0  436 -1.589e+07 5913701.53   137%     -  791s
     0     0 5913701.21    0  438 -1.589e+07 5913701.21   137%     -  795s
     0     0 5912813.94    0  439 -1.589e+07 5912813.94   137%     -  800s
H    0     0                    -1.24142e+07 5912813.94   148%     -  821s
     0     0 5912813.94    0  439 -1.241e+07 5912813.94   148%     -  822s
     0     0 5912752.29    0  443 -1.241e+07 5912752.29   148%     -  826s
H    0     0                    -1.24016e+07 5912752.29   148%     -  848s
     0     0 5911914.35    0  442 -1.240e+07 5911914.35   148%     -  850s
     0     0 5911914.35    0  442 -1.240e+07 5911914.35   148%     -  851s
     0     0 5911847.07    0  438 -1.240e+07 5911847.07   148%     -  854s
     0     0 5911847.07    0  434 -1.240e+07 5911847.07   148%     - 1015s
H    0     0                    -1.23975e+07 5911847.07   148%     - 1091s
     0     2 5911847.07    0  434 -1.240e+07 5911847.07   148%     - 1126s
     1     3 5794477.35    1  561 -1.240e+07 5794477.35   147% 546446 14604s
     3     6 5169502.44    2  485 -1.240e+07 5592880.78   145% 283680 26672s
     6    10 3094072.00    3  374 -1.240e+07 5167897.10   142% 175077 28315s
H    7    10                    -1.05266e+07 5167897.10   149% 150066 28315s
    10    10 infeasible    4      -1.053e+07 5022201.06   148% 110250 29360s
    14    12 -3178979.3    4  310 -1.053e+07 4991155.60   147% 86220 30213s
    18    10 infeasible    5      -1.053e+07 4990728.46   147% 68219 30290s
    22    15 -6135082.6    5  297 -1.053e+07 4990667.89   147% 56527 45888s
    27    18 -8427885.8    6  231 -1.053e+07 4990667.89   147% 68613 80108s
    33    21 -9485799.0    7  244 -1.053e+07 4990667.89   147% 102824 83034s
    40    24 -9486791.8    8  255 -1.053e+07 4990667.89   147% 85588 86400s

Cutting planes:
  Implied bound: 4
  Clique: 28
  MIR: 36
  Flow cover: 1
  Zero half: 4

Explored 46 nodes (3512654 simplex iterations) in 86400.31 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -1.05266e+07 -1.23975e+07 -1.24016e+07 ... -3.73045e+07

Time limit reached
Best objective -1.052662824786e+07, best bound 4.990667892207e+06, gap 147.4099%

***
***  |- Calculation finished, timelimit reached 86,400.36 (86400.36) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -10,526,628.2479 (-10526628.2479) and is feasible with a Gap of 1.4741
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 201, 285, 46, 368, 331, -3]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 201 (MAPTM In@375.0/Out@396.0) - 285 (GWOXB In@631.0/Out@710.0) - 46 (CMDLA In@796.0/Out@842.0) - 368 (TRMER In@10
###  |  83.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  46
###  |   |- and carried Demand 144,  161.0 containers from 290 to  46
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 338, 329, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 338 (EGPSD In@867.0/Out@886.0) - 329 (AEJEA In@1194.0/Out@1227
###  |  .0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |- Vessel 17 has the path [145, 335, 337, 339, 330, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 337 (EGPSD In@789.0/Out@805.0) - 339 (EGPSD In@957.0/Out@973
###  |  .0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 18 has the path [148, 267, 328, -3]
###  |  148 (GBFXT Out@470.0) - 267 (TRALI In@836.0/Out@854.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 121 (SAE) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  6 has the path [219, 315, 311, 301, -2]
###  |  219 (NLRTM Out@260.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |- Vessel 16 has the path [136, 202, 114, 302, -2]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0) - 302 (HNPCR In@1166.0/Out@11
###  |  79.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 363, 260, 190, 170, 49, 75, 72, 53, 61, 236, 0]
###  |  218 (NLRTM Out@92.0) - 363 (TRMER In@243.0/Out@287.0) - 260 (SGSIN In@618.0/Out@651.0) - 190 (KRPUS In@788.0/Out@810.
###  |  0) - 170 (JPHKT In@824.0/Out@833.0) - 49 (CNDLC In@871.0/Out@886.0) - 75 (CNXGG In@904.0/Out@923.0) - 72 (CNTAO In@96
###  |  0.0/Out@978.0) - 53 (CNNGB In@1031.0/Out@1040.0) - 61 (CNSHA In@1069.0/Out@1079.0) - 236 (PABLB In@1640.0/Out@1652.0)
###  |   - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  36,  907.0 containers from  61 to 236
###  |   |- and carried Demand  37,    3.0 containers from  61 to 236
###  |- Vessel  7 has the path [221, 137, 203, 234, 0]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |- Vessel 15 has the path [134, 200, 275, 204, 235, 0]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 275 (ESALG In@328.0/Out@347.0) - 204 (MAPTM In@711.0/Out@732
###  |  .0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 131,  580.0 containers from 200 to 275
###  |
### Vessels not used in the solution: [4, 8, 13, 14]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,19] = 1
rho[121,19] = 1
rho[7,30] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
eta[121,19,1] = 1
eta[121,19,2] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,190,170] = 1
y[5,218,363] = 1
y[5,49,75] = 1
y[5,53,61] = 1
y[5,260,190] = 1
y[5,61,236] = 1
y[5,363,260] = 1
y[5,75,72] = 1
y[5,72,53] = 1
y[5,236,0] = 1
y[5,170,49] = 1
y[6,301,-2] = 1
y[6,315,311] = 1
y[6,219,315] = 1
y[6,311,301] = 1
y[7,234,0] = 1
y[7,137,203] = 1
y[7,203,234] = 1
y[7,221,137] = 1
y[9,285,46] = 1
y[9,274,199] = 1
y[9,290,201] = 1
y[9,1,274] = 1
y[9,46,368] = 1
y[9,199,290] = 1
y[9,368,331] = 1
y[9,331,-3] = 1
y[9,201,285] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,366,338] = 1
y[12,338,329] = 1
y[12,329,-3] = 1
y[12,8,366] = 1
y[15,275,204] = 1
y[15,235,0] = 1
y[15,204,235] = 1
y[15,134,200] = 1
y[15,200,275] = 1
y[16,136,202] = 1
y[16,302,-2] = 1
y[16,202,114] = 1
y[16,114,302] = 1
y[17,337,339] = 1
y[17,339,330] = 1
y[17,145,335] = 1
y[17,335,337] = 1
y[17,330,-3] = 1
y[18,148,267] = 1
y[18,328,-3] = 1
y[18,267,328] = 1
xD[7,285,46] = 161
xD[7,290,201] = 161
xD[7,199,290] = 161
xD[7,201,285] = 161
xD[26,221,137] = 6
xD[36,61,236] = 907
xD[37,61,236] = 3
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[131,200,275] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[144,285,46] = 161
xD[144,290,201] = 161
xD[144,201,285] = 161
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[206,338,329] = 467
zE[46] = 796
zE[49] = 871
zE[53] = 1031
zE[61] = 1069
zE[72] = 960
zE[75] = 904
zE[114] = 452
zE[137] = 447
zE[170] = 824
zE[190] = 788
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[260] = 618
zE[267] = 836
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[285] = 631
zE[290] = 211
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[335] = 621
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[363] = 243
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[368] = 1083
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[46] = 842
zX[49] = 886
zX[53] = 1040
zX[61] = 1079
zX[72] = 978
zX[75] = 923
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[114] = 464
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[145] = 302
zX[148] = 470
zX[170] = 833
zX[190] = 810
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[260] = 651
zX[267] = 854
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[285] = 710
zX[290] = 230
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[335] = 637
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[363] = 287
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[368] = 1127
phi[1] = 227
phi[2] = 227
phi[3] = 227
phi[5] = 1560
phi[6] = 751
phi[7] = 888
phi[9] = 1497
phi[10] = 489
phi[11] = 489
phi[12] = 657
phi[15] = 1326
phi[16] = 853
phi[17] = 1093
phi[18] = 589

### All variables with value smaller zero:

### End

real	1444m22.379s
user	4892m21.888s
sys	873m38.616s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
