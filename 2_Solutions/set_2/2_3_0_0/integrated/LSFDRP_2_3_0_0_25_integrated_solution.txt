	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_25.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_25 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 33971 rows and 1627817 columns (presolve time = 5s) ...
Presolve removed 66215 rows and 2656507 columns (presolve time = 10s) ...
Presolve removed 75424 rows and 2753798 columns (presolve time = 15s) ...
Presolve removed 78252 rows and 2757008 columns (presolve time = 21s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757047 columns (presolve time = 28s) ...
Presolve removed 78275 rows and 2757212 columns (presolve time = 52s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 56s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 424s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 423.78s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 30 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09438137e+15  6.29392713e+15  1.11e+08 1.85e+06  8.31e+11   485s

Barrier performed 0 iterations in 506.74 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.79s

Solved with dual simplex

Root relaxation: objective 7.974720e+06, 39537 iterations, 78.57 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7750043.74    0  344          - 7750043.74      -     -  548s
     0     0 6854843.40    0  288          - 6854843.40      -     -  610s
H    0     0                    -3.32859e+07 6854843.40   121%     -  612s
     0     0 6637357.92    0  351 -3.329e+07 6637357.92   120%     -  631s
     0     0 6637357.92    0  351 -3.329e+07 6637357.92   120%     -  632s
     0     0 6238137.91    0  379 -3.329e+07 6238137.91   119%     -  667s
H    0     0                    -2.25673e+07 6238137.91   128%     -  668s
     0     0 6236910.97    0  380 -2.257e+07 6236910.97   128%     -  672s
     0     0 6208942.93    0  402 -2.257e+07 6208942.93   128%     -  689s
     0     0 6208942.01    0  377 -2.257e+07 6208942.01   128%     -  694s
     0     0 6207270.44    0  425 -2.257e+07 6207270.44   128%     -  699s
     0     0 6200922.56    0  423 -2.257e+07 6200922.56   127%     -  706s
     0     0 6199955.93    0  389 -2.257e+07 6199955.93   127%     -  709s
     0     0 6199955.93    0  390 -2.257e+07 6199955.93   127%     -  711s
     0     0 6199955.93    0  391 -2.257e+07 6199955.93   127%     -  713s
     0     0 6199955.93    0  392 -2.257e+07 6199955.93   127%     -  717s
     0     0 6199702.08    0  405 -2.257e+07 6199702.08   127%     -  721s
     0     0 6199702.08    0  405 -2.257e+07 6199702.08   127%     -  727s
H    0     0                    -2.17894e+07 6199702.08   128%     -  737s
     0     0 6199539.92    0  397 -2.179e+07 6199539.92   128%     -  743s
     0     0 6199539.92    0  397 -2.179e+07 6199539.92   128%     -  749s
H    0     0                    -7512503.176 6199539.92   183%     -  790s
     0     0 6199539.92    0  397 -7512503.2 6199539.92   183%     -  912s
H    0     0                    -7159576.407 6199539.92   187%     -  948s
     0     2 6199539.92    0  396 -7159576.4 6199539.92   187%     - 1013s
     1     4 3946711.95    1  398 -7159576.4 6130356.06   186% 296792 6669s
     3     8 3837702.81    2  407 -7159576.4 5854269.47   182% 107122 7063s
     7    10 infeasible    3      -7159576.4 5612639.91   178% 50282 16938s
    11    14 2818826.92    3  334 -7159576.4 5612639.91   178% 79981 17572s
    15    15 2309590.15    4  276 -7159576.4 5074826.53   171% 97971 27967s
    19    15 2264666.81    5  283 -7159576.4 5074826.53   171% 78345 36368s
    23    16 infeasible    5      -7159576.4 5074826.53   171% 83453 49460s
H   30    22                    -6241642.345 5074826.53   181% 75993 49979s
    37    20 infeasible    8      -6241642.3 5074826.53   181% 62763 50271s
    44    29 -4283704.2    8  260 -6241642.3 5074826.53   181% 52994 74140s
    55    37 -4374915.9   11  294 -6241642.3 5074826.53   181% 68861 86252s
    62    43 -2021204.4   15  244 -6241642.3 5074826.53   181% 72728 86400s

Cutting planes:
  Implied bound: 8
  Clique: 25
  MIR: 32
  GUB cover: 1
  Zero half: 2

Explored 65 nodes (4585069 simplex iterations) in 86400.36 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -6.24164e+06 -7.15958e+06 -7.5125e+06 ... -3.32859e+07

Time limit reached
Best objective -6.241642345255e+06, best bound 5.074826531901e+06, gap 181.3059%

***
***  |- Calculation finished, timelimit reached 86,400.41 (86400.41) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -6,241,642.3453 (-6241642.3453) and is feasible with a Gap of 1.8131
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 363, 334, 352, 229, 327, -3]
###  |  218 (NLRTM Out@92.0) - 363 (TRMER In@243.0/Out@287.0) - 334 (EGPSD In@531.0/Out@550.0) - 352 (OMSLL In@710.0/Out@722.
###  |  0) - 229 (OMSLL In@721.0/Out@736.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 214,  164.0 containers from 334 to 327
###  |   |- and carried Demand 215,   58.0 containers from 334 to 327
###  |- Vessel  6 has the path [219, 135, 201, 113, 161, 338, 339, 329, -3]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433
###  |  .0) - 161 (GRPIR In@696.0/Out@721.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA I
###  |  n@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel  7 has the path [221, 365, 353, 230, 328, -3]
###  |  221 (NLRTM Out@428.0) - 365 (TRMER In@579.0/Out@623.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904
###  |  .0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |- Vessel  8 has the path [223, 367, 330, -3]
###  |  223 (NLRTM Out@596.0) - 367 (TRMER In@915.0/Out@959.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [136, 273, 340, 368, 331, -3]
###  |  136 (FRLEH Out@326.0) - 273 (TRZMK In@785.0/Out@799.0) - 340 (EGPSD In@1035.0/Out@1054.0) - 368 (TRMER In@1083.0/Out@
###  |  1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 20, 45, 302, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 284 (GWOXB In@463.0/Out@542.0) - 20 (BJCOO In@68
###  |  1.0/Out@718.0) - 45 (CMDLA In@765.0/Out@811.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to  45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 366, 267, 235, 0]
###  |  102 (DEHAM Out@624.0) - 366 (TRMER In@747.0/Out@791.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 14 has the path [105, 357, 266, 234, 0]
###  |  105 (DKAAR Out@388.0) - 357 (TRAMB In@515.0/Out@563.0) - 266 (TRALI In@668.0/Out@686.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [4, 10, 11, 12, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,366,267] = 1
y[3,235,0] = 1
y[3,267,235] = 1
y[3,102,366] = 1
y[5,229,327] = 1
y[5,218,363] = 1
y[5,327,-3] = 1
y[5,352,229] = 1
y[5,334,352] = 1
y[5,363,334] = 1
y[6,329,-3] = 1
y[6,113,161] = 1
y[6,339,329] = 1
y[6,135,201] = 1
y[6,161,338] = 1
y[6,201,113] = 1
y[6,338,339] = 1
y[6,219,135] = 1
y[7,365,353] = 1
y[7,328,-3] = 1
y[7,221,365] = 1
y[7,353,230] = 1
y[7,230,328] = 1
y[8,367,330] = 1
y[8,223,367] = 1
y[8,330,-3] = 1
y[9,200,112] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,1,274] = 1
y[9,45,302] = 1
y[9,199,290] = 1
y[9,20,45] = 1
y[9,302,-2] = 1
y[9,284,20] = 1
y[9,112,284] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,114,278] = 1
y[13,202,114] = 1
y[13,278,-1] = 1
y[14,234,0] = 1
y[14,105,357] = 1
y[14,357,266] = 1
y[14,266,234] = 1
y[15,356,325] = 1
y[15,134,356] = 1
y[15,325,-3] = 1
y[16,340,368] = 1
y[16,136,273] = 1
y[16,368,331] = 1
y[16,331,-3] = 1
y[16,273,340] = 1
y[18,301,-2] = 1
y[18,148,315] = 1
y[18,315,311] = 1
y[18,311,301] = 1
xD[7,200,112] = 161
xD[7,290,200] = 161
xD[7,199,290] = 161
xD[7,20,45] = 161
xD[7,284,20] = 161
xD[7,112,284] = 161
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[13,135,201] = 74
xD[13,201,113] = 74
xD[13,219,135] = 74
xD[14,219,135] = 5
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[45,200,112] = 161
xD[45,20,45] = 161
xD[45,284,20] = 161
xD[45,112,284] = 161
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[118,135,201] = 74
xD[118,201,113] = 74
xD[118,219,135] = 74
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[131,200,112] = 580
xD[132,114,278] = 214
xD[132,202,114] = 214
xD[133,114,278] = 10
xD[133,202,114] = 10
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[138,291,202] = 214
xD[138,114,278] = 214
xD[138,202,114] = 214
xD[139,291,202] = 10
xD[139,114,278] = 10
xD[139,202,114] = 10
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,200,112] = 161
xD[144,290,200] = 161
xD[144,20,45] = 161
xD[144,284,20] = 161
xD[144,112,284] = 161
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[201,353,230] = 111
xD[201,230,328] = 111
xD[202,353,230] = 35
xD[202,230,328] = 35
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[207,229,327] = 288
xD[207,352,229] = 288
xD[208,229,327] = 14
xD[208,352,229] = 14
xD[210,339,329] = 467
xD[214,229,327] = 164
xD[214,352,229] = 164
xD[214,334,352] = 164
xD[215,229,327] = 58
xD[215,352,229] = 58
xD[215,334,352] = 58
zE[20] = 681
zE[45] = 765
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[135] = 279
zE[161] = 696
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[229] = 721
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[273] = 785
zE[274] = 160
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[357] = 515
zE[363] = 243
zE[365] = 579
zE[366] = 747
zE[367] = 915
zE[368] = 1083
zX[1] = 66
zX[20] = 718
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[148] = 470
zX[161] = 721
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[273] = 799
zX[274] = 179
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[357] = 563
zX[363] = 287
zX[365] = 623
zX[366] = 791
zX[367] = 959
zX[368] = 1127
phi[1] = 227
phi[2] = 227
phi[3] = 860
phi[5] = 799
phi[6] = 967
phi[7] = 631
phi[8] = 799
phi[9] = 1113
phi[13] = 631
phi[14] = 928
phi[15] = 565
phi[16] = 1237
phi[18] = 541

### All variables with value smaller zero:

### End

real	1443m53.825s
user	4886m0.738s
sys	879m52.087s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
