	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_19 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 33976 rows and 1627822 columns (presolve time = 5s) ...
Presolve removed 70591 rows and 2743928 columns (presolve time = 10s) ...
Presolve removed 77879 rows and 2756626 columns (presolve time = 15s) ...
Presolve removed 78275 rows and 2757047 columns (presolve time = 20s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757212 columns (presolve time = 49s) ...
Presolve removed 78498 rows and 2757224 columns (presolve time = 50s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 447s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 446.63s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 30 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09417130e+15  4.78339896e+15  1.11e+08 1.61e+06  6.32e+11   510s

Barrier performed 0 iterations in 528.75 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.73s

Solved with dual simplex

Root relaxation: objective 7.993682e+06, 42336 iterations, 76.69 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7795703.30    0  351          - 7795703.30      -     -  559s
     0     0 7035839.58    0  351          - 7035839.58      -     -  665s
H    0     0                    -2.08797e+07 7035839.58   134%     -  667s
H    0     0                    -2.03313e+07 7035839.58   135%     -  671s
     0     0 6741427.89    0  369 -2.033e+07 6741427.89   133%     -  693s
     0     0 6741230.81    0  366 -2.033e+07 6741230.81   133%     -  695s
     0     0 6741230.81    0  366 -2.033e+07 6741230.81   133%     -  696s
     0     0 6391884.68    0  442 -2.033e+07 6391884.68   131%     -  777s
H    0     0                    -1.76666e+07 6391884.68   136%     -  779s
     0     0 6383332.53    0  417 -1.767e+07 6383332.53   136%     -  787s
     0     0 6383332.53    0  418 -1.767e+07 6383332.53   136%     -  788s
     0     0 6370292.68    0  421 -1.767e+07 6370292.68   136%     -  802s
H    0     0                    -1.76590e+07 6370292.68   136%     -  803s
     0     0 6370292.68    0  422 -1.766e+07 6370292.68   136%     -  807s
     0     0 6358381.92    0  445 -1.766e+07 6358381.92   136%     -  813s
H    0     0                    -1.75161e+07 6358381.92   136%     -  813s
     0     0 6358381.92    0  447 -1.752e+07 6358381.92   136%     -  817s
     0     0 6357073.71    0  439 -1.752e+07 6357073.71   136%     -  822s
     0     0 6357073.71    0  441 -1.752e+07 6357073.71   136%     -  825s
     0     0 6355946.80    0  452 -1.752e+07 6355946.80   136%     -  829s
     0     0 6355946.56    0  456 -1.752e+07 6355946.56   136%     -  833s
     0     0 6355799.21    0  458 -1.752e+07 6355799.21   136%     -  835s
     0     0 6355784.22    0  457 -1.752e+07 6355784.22   136%     -  840s
     0     0 6355683.88    0  466 -1.752e+07 6355683.88   136%     -  842s
     0     0 6355683.88    0  464 -1.752e+07 6355683.88   136%     -  846s
     0     0 6355683.88    0  464 -1.752e+07 6355683.88   136%     -  848s
     0     0 6355683.88    0  459 -1.752e+07 6355683.88   136%     - 1002s
H    0     0                    -7022248.078 6355683.88   191%     - 1057s
     0     2 6355683.88    0  455 -7022248.1 6355683.88   191%     - 1079s
     1     4 4644217.52    1  442 -7022248.1 6159184.28   188% 615683 12304s
     3     8 4561175.56    2  420 -7022248.1 5714351.11   181% 258497 15513s
     7    12 4457706.97    3  378 -7022248.1 5669202.78   181% 127597 15894s
    11    16 3635488.21    4  367 -7022248.1 5669081.10   181% 86752 16245s
    15    18 -1597223.6    5  322 -7022248.1 5356568.91   176% 66328 16512s
    19    20 2064150.31    5  348 -7022248.1 5324376.68   176% 54699 16702s
    23    16 1397224.49    6  218 -7022248.1 4589260.85   165% 47786 16899s
    30    27 1704991.88    6  343 -7022248.1 4580306.03   165% 37774 17649s
H   35    29                    -4949991.131 4580306.03   193% 32457 17649s
    36    32 1278030.35    7  322 -4949991.1 4580306.03   193% 31705 17776s
    44    34     cutoff    8      -4949991.1 4580306.03   193% 26669 17919s
    53    45 -1400286.4    8  251 -4949991.1 4580306.03   193% 22428 19320s
    57    51 -1433659.0    9  248 -4949991.1 4580306.03   193% 22867 19423s
H   68    54                    -1150380.531 4580306.03   498% 19390 86400s
H   71    53                    -775616.6048 4580306.03   691% 18695 86400s

Cutting planes:
  Implied bound: 6
  Clique: 24
  MIR: 35
  Flow cover: 1
  GUB cover: 1
  Zero half: 7

Explored 73 nodes (1424953 simplex iterations) in 86400.54 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: -775617 -1.15038e+06 -4.94999e+06 ... -2.08797e+07

Time limit reached
Best objective -7.756166048360e+05, best bound 4.580306026258e+06, gap 690.5374%

***
***  |- Calculation finished, timelimit reached 86,400.6 (86400.6) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -775,616.6048 (-775616.6048) and is feasible with a Gap of 6.9054
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 290, 200, 112, 275, 201, 113, 285, 46, 368, 331, -3]
###  |  218 (NLRTM Out@92.0) - 290 (MAPTM In@211.0/Out@230.0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.
###  |  0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 285 (GWOXB In
###  |  @631.0/Out@710.0) - 46 (CMDLA In@796.0/Out@842.0) - 368 (TRMER In@1083.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.
###  |  0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   5,   42.0 containers from 218 to 112
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  45,  161.0 containers from 200 to  46
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 116,   42.0 containers from 218 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to  46
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel  6 has the path [219, 364, 334, 335, 338, 339, 329, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 334 (EGPSD In@531.0/Out@550.0) - 335 (EGPSD In@621.0/Out@637
###  |  .0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_
###  |  END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel  7 has the path [221, 137, 203, 115, 267, 328, -3]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 115 (ESALG In@589.0/Out@601
###  |  .0) - 267 (TRALI In@836.0/Out@854.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |- Vessel  8 has the path [223, 366, 330, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [136, 272, 327, -3]
###  |  136 (FRLEH Out@326.0) - 272 (TRZMK In@617.0/Out@631.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 111, 301, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 111 (ESALG In@253.0/Out@265.0)
###  |   - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   8,  580.0 containers from 199 to 111
###  |   |- and carried Demand 117,  580.0 containers from 199 to 111
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 358, 273, 235, 0]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel  4 has the path [103, 37, 42, 236, 0]
###  |  103 (DEHAM Out@792.0) - 37 (CLLQN In@1408.0/Out@1426.0) - 42 (CLSAI In@1456.0/Out@1472.0) - 236 (PABLB In@1640.0/Out@
###  |  1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 14 has the path [105, 365, 266, 234, 0]
###  |  105 (DKAAR Out@388.0) - 365 (TRMER In@579.0/Out@623.0) - 266 (TRALI In@668.0/Out@686.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [10, 11, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,235,0] = 1
y[3,273,235] = 1
y[4,37,42] = 1
y[4,103,37] = 1
y[4,236,0] = 1
y[4,42,236] = 1
y[5,218,290] = 1
y[5,200,112] = 1
y[5,285,46] = 1
y[5,290,200] = 1
y[5,112,275] = 1
y[5,275,201] = 1
y[5,113,285] = 1
y[5,46,368] = 1
y[5,368,331] = 1
y[5,331,-3] = 1
y[5,201,113] = 1
y[6,364,334] = 1
y[6,329,-3] = 1
y[6,339,329] = 1
y[6,219,364] = 1
y[6,338,339] = 1
y[6,334,335] = 1
y[6,335,338] = 1
y[7,115,267] = 1
y[7,137,203] = 1
y[7,221,137] = 1
y[7,328,-3] = 1
y[7,203,115] = 1
y[7,267,328] = 1
y[8,223,366] = 1
y[8,366,330] = 1
y[8,330,-3] = 1
y[9,301,-2] = 1
y[9,274,199] = 1
y[9,1,274] = 1
y[9,111,301] = 1
y[9,199,111] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,114,278] = 1
y[13,202,114] = 1
y[13,278,-1] = 1
y[14,234,0] = 1
y[14,365,266] = 1
y[14,105,365] = 1
y[14,266,234] = 1
y[15,356,325] = 1
y[15,134,356] = 1
y[15,325,-3] = 1
y[16,327,-3] = 1
y[16,136,272] = 1
y[16,272,327] = 1
xD[5,218,290] = 42
xD[5,200,112] = 42
xD[5,290,200] = 42
xD[8,199,111] = 580
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[25,137,203] = 68
xD[25,221,137] = 68
xD[25,203,115] = 68
xD[26,221,137] = 6
xD[29,203,115] = 60
xD[30,203,115] = 11
xD[45,200,112] = 161
xD[45,285,46] = 161
xD[45,112,275] = 161
xD[45,275,201] = 161
xD[45,113,285] = 161
xD[45,201,113] = 161
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[116,218,290] = 42
xD[116,200,112] = 42
xD[116,290,200] = 42
xD[117,199,111] = 580
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[123,137,203] = 68
xD[123,221,137] = 68
xD[123,203,115] = 68
xD[126,203,115] = 60
xD[127,203,115] = 11
xD[131,200,112] = 580
xD[132,114,278] = 214
xD[132,202,114] = 214
xD[133,114,278] = 10
xD[133,202,114] = 10
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[138,291,202] = 214
xD[138,114,278] = 214
xD[138,202,114] = 214
xD[139,291,202] = 10
xD[139,114,278] = 10
xD[139,202,114] = 10
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,200,112] = 161
xD[144,285,46] = 161
xD[144,290,200] = 161
xD[144,112,275] = 161
xD[144,275,201] = 161
xD[144,113,285] = 161
xD[144,201,113] = 161
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
zE[37] = 1408
zE[42] = 1456
zE[46] = 796
zE[111] = 253
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[115] = 589
zE[137] = 447
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 4085
zE[266] = 668
zE[267] = 836
zE[272] = 617
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[285] = 631
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[338] = 867
zE[339] = 957
zE[356] = 347
zE[358] = 683
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[368] = 1083
zX[1] = 66
zX[8] = 570
zX[37] = 1426
zX[42] = 1472
zX[46] = 842
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[115] = 601
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 4085
zX[266] = 686
zX[267] = 854
zX[272] = 631
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[285] = 710
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[338] = 886
zX[339] = 973
zX[356] = 395
zX[358] = 731
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[368] = 1127
phi[1] = 227
phi[2] = 227
phi[3] = 860
phi[4] = 860
phi[5] = 1471
phi[6] = 967
phi[7] = 631
phi[8] = 799
phi[9] = 945
phi[12] = 609
phi[13] = 631
phi[14] = 928
phi[15] = 565
phi[16] = 565

### All variables with value smaller zero:

### End

real	1443m50.581s
user	4863m51.889s
sys	902m46.288s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
