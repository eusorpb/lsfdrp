	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_0_fr.p
Instance LSFDP: LSFDRP_2_3_0_0_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_0_fd_49 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 6e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 37611 rows and 1750586 columns (presolve time = 5s) ...
Presolve removed 70591 rows and 2743928 columns (presolve time = 10s) ...
Presolve removed 77879 rows and 2756626 columns (presolve time = 15s) ...
Presolve removed 78275 rows and 2757047 columns (presolve time = 20s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757212 columns (presolve time = 47s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 431s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 431.06s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 22
 AA' NZ     : 2.915e+06
 Factor NZ  : 9.237e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.723e+11 (roughly 40 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09522170e+15  1.23360398e+16  1.11e+08 3.62e+06  1.63e+12   491s
   1  -1.14552128e+15  1.19666969e+16  6.05e+07 2.51e+07  9.11e+11   525s
   2  -8.46880499e+14  1.12789121e+16  4.48e+07 1.63e+07  6.64e+11   560s
   3  -5.36448510e+14  1.04233077e+16  2.85e+07 7.67e+06  4.20e+11   594s

Barrier performed 3 iterations in 607.49 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.60s

Solved with dual simplex

Root relaxation: objective 7.953903e+06, 54558 iterations, 172.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7734295.10    0  322          - 7734295.10      -     -  662s
     0     0 6669318.55    0  320          - 6669318.55      -     -  720s
     0     0 6353881.85    0  410          - 6353881.85      -     -  756s
     0     0 5786425.63    0  469          - 5786425.63      -     -  779s
H    0     0                    -1.78173e+07 5786425.63   132%     -  780s
     0     0 5786425.63    0  469 -1.782e+07 5786425.63   132%     -  784s
     0     0 5764464.59    0  514 -1.782e+07 5764464.59   132%     -  847s
H    0     0                    -1.72066e+07 5764464.59   134%     -  847s
     0     0 5763703.62    0  510 -1.721e+07 5763703.62   133%     -  869s
     0     0 5763703.62    0  510 -1.721e+07 5763703.62   133%     -  870s
     0     0 5762506.43    0  513 -1.721e+07 5762506.43   133%     -  876s
H    0     0                    -1.67050e+07 5762506.43   134%     -  877s
     0     0 5762432.44    0  517 -1.671e+07 5762432.44   134%     -  882s
     0     0 5761259.72    0  522 -1.671e+07 5761259.72   134%     -  887s
H    0     0                    -1.62592e+07 5761259.72   135%     -  887s
     0     0 5760720.48    0  529 -1.626e+07 5760720.48   135%     -  893s
     0     0 5760629.41    0  543 -1.626e+07 5760629.41   135%     -  894s
     0     0 5760132.65    0  525 -1.626e+07 5760132.65   135%     -  898s
     0     0 5759472.62    0  517 -1.626e+07 5759472.62   135%     -  903s
     0     0 5759206.64    0  514 -1.626e+07 5759206.64   135%     -  905s
     0     0 5759192.25    0  523 -1.626e+07 5759192.25   135%     -  906s
     0     0 5758039.29    0  517 -1.626e+07 5758039.29   135%     -  912s
H    0     0                    -1.62327e+07 5758039.29   135%     -  913s
H    0     0                    -1.54549e+07 5758039.29   137%     -  936s
     0     0 5757732.25    0  529 -1.545e+07 5757732.25   137%     -  938s
     0     0 5757731.62    0  534 -1.545e+07 5757731.62   137%     -  939s
     0     0 5757669.08    0  529 -1.545e+07 5757669.08   137%     -  942s
     0     0 5757669.08    0  529 -1.545e+07 5757669.08   137%     - 1108s
H    0     0                    -1.50717e+07 5757669.08   138%     - 1192s
H    0     2                    -1.40698e+07 5757669.08   141%     - 1257s
     0     2 5757669.08    0  529 -1.407e+07 5757669.08   141%     - 1257s
     1     4 2899166.80    1  350 -1.407e+07 5757657.02   141% 209603 4680s
     3     6 1325946.71    2  368 -1.407e+07 5580035.43   140% 80220 5137s
     7     9 809286.063    3  319 -1.407e+07 5390033.22   138% 40837 5269s
    11    12 699781.418    4  313 -1.407e+07 5308215.05   138% 26878 5379s
    15    13 220363.327    5  320 -1.407e+07 4885568.59   135% 20464 5432s
    19    14 -631097.27    6  297 -1.407e+07 4885568.59   135% 17250 5705s
    23    16 -1091511.6    6  342 -1.407e+07 4885284.41   135% 15627 5806s
    27    20 -1149312.3    7  368 -1.407e+07 4885284.41   135% 14519 5995s
H   30    21                    -1.37383e+07 4885284.41   136% 13727 5995s
    31    17 -5897767.8    8  342 -1.374e+07 4885284.41   136% 28851 14973s
    37    22 -694844.32    7  286 -1.374e+07 4885284.41   136% 26028 79663s
    42    27 infeasible    8      -1.374e+07 4885284.41   136% 66106 81609s
    50    26 infeasible   12      -1.374e+07 4885284.41   136% 58173 86400s

Cutting planes:
  Implied bound: 7
  Clique: 27
  MIR: 39
  Flow cover: 1
  Zero half: 5

Explored 56 nodes (3059589 simplex iterations) in 86400.27 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: -1.37383e+07 -1.40698e+07 -1.50717e+07 ... -1.78173e+07

Time limit reached
Best objective -1.373829166017e+07, best bound 4.885284410562e+06, gap 135.5596%

***
***  |- Calculation finished, timelimit reached 86,400.32 (86400.32) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -13,738,291.6602 (-13738291.6602) and is feasible with a Gap of 1.3556
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 133, 199, 290, 200, 112, 275, 201, 113, 285, 46, 368, 331, -3]
###  |  218 (NLRTM Out@92.0) - 133 (FRLEH In@111.0/Out@127.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.
###  |  0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In
###  |  @375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 285 (GWOXB In@631.0/Out@710.0) - 46 (CMDLA In@796.0/Out@842.0) -
###  |   368 (TRMER In@1083.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand   7,  161.0 containers from 199 to  46
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  45,  161.0 containers from 200 to  46
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to  46
###  |   |- and carried Demand 145,   19.0 containers from 290 to 112
###  |- Vessel  6 has the path [219, 364, 334, 335, 336, 353, 230, 328, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 334 (EGPSD In@531.0/Out@550.0) - 335 (EGPSD In@621.0/Out@637
###  |  .0) - 336 (EGPSD In@699.0/Out@718.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904.0) - 328 (AEJEA I
###  |  n@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |   |- and carried Demand 211,   54.0 containers from 336 to 328
###  |- Vessel  7 has the path [221, 137, 203, 115, 367, 330, -3]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 115 (ESALG In@589.0/Out@601
###  |  .0) - 367 (TRMER In@915.0/Out@959.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |- Vessel  8 has the path [223, 366, 338, 339, 329, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973
###  |  .0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [136, 365, 327, -3]
###  |  136 (FRLEH Out@326.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 276, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 266, 234, 0]
###  |  100 (DEHAM Out@288.0) - 266 (TRALI In@668.0/Out@686.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel 14 has the path [105, 267, 235, 0]
###  |  105 (DKAAR Out@388.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [4, 9, 10, 11, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,234,0] = 1
y[1,100,266] = 1
y[1,266,234] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,200,112] = 1
y[5,285,46] = 1
y[5,290,200] = 1
y[5,112,275] = 1
y[5,275,201] = 1
y[5,133,199] = 1
y[5,113,285] = 1
y[5,46,368] = 1
y[5,199,290] = 1
y[5,368,331] = 1
y[5,331,-3] = 1
y[5,218,133] = 1
y[5,201,113] = 1
y[6,364,334] = 1
y[6,328,-3] = 1
y[6,219,364] = 1
y[6,336,353] = 1
y[6,353,230] = 1
y[6,230,328] = 1
y[6,335,336] = 1
y[6,334,335] = 1
y[7,137,203] = 1
y[7,221,137] = 1
y[7,367,330] = 1
y[7,203,115] = 1
y[7,115,367] = 1
y[7,330,-3] = 1
y[8,223,366] = 1
y[8,366,338] = 1
y[8,329,-3] = 1
y[8,339,329] = 1
y[8,338,339] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,276,-1] = 1
y[13,114,276] = 1
y[13,202,114] = 1
y[14,105,267] = 1
y[14,235,0] = 1
y[14,267,235] = 1
y[15,356,325] = 1
y[15,134,356] = 1
y[15,325,-3] = 1
y[16,365,327] = 1
y[16,327,-3] = 1
y[16,136,365] = 1
y[18,301,-2] = 1
y[18,148,315] = 1
y[18,315,311] = 1
y[18,311,301] = 1
xD[6,218,133] = 17
xD[7,200,112] = 161
xD[7,285,46] = 161
xD[7,290,200] = 161
xD[7,112,275] = 161
xD[7,275,201] = 161
xD[7,113,285] = 161
xD[7,199,290] = 161
xD[7,201,113] = 161
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[25,137,203] = 68
xD[25,221,137] = 68
xD[25,203,115] = 68
xD[26,221,137] = 6
xD[29,203,115] = 60
xD[30,203,115] = 11
xD[45,200,112] = 161
xD[45,285,46] = 161
xD[45,112,275] = 161
xD[45,275,201] = 161
xD[45,113,285] = 161
xD[45,201,113] = 161
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[123,137,203] = 68
xD[123,221,137] = 68
xD[123,203,115] = 68
xD[126,203,115] = 60
xD[127,203,115] = 11
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,200,112] = 161
xD[144,285,46] = 161
xD[144,290,200] = 161
xD[144,112,275] = 161
xD[144,275,201] = 161
xD[144,113,285] = 161
xD[144,201,113] = 161
xD[145,200,112] = 19
xD[145,290,200] = 19
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[201,353,230] = 111
xD[201,230,328] = 111
xD[202,353,230] = 35
xD[202,230,328] = 35
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
xD[211,336,353] = 54
xD[211,353,230] = 54
xD[211,230,328] = 54
zE[46] = 796
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[115] = 589
zE[133] = 111
zE[137] = 447
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[285] = 631
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[336] = 699
zE[338] = 867
zE[339] = 957
zE[353] = 878
zE[356] = 347
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[367] = 915
zE[368] = 1083
zX[8] = 570
zX[46] = 842
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[115] = 601
zX[133] = 127
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[285] = 710
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[336] = 718
zX[338] = 886
zX[339] = 973
zX[353] = 890
zX[356] = 395
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[367] = 959
zX[368] = 1127
phi[1] = 1028
phi[2] = 227
phi[3] = 227
phi[5] = 1471
phi[6] = 799
phi[7] = 967
phi[8] = 631
phi[12] = 609
phi[13] = 295
phi[14] = 1096
phi[15] = 565
phi[16] = 565
phi[18] = 541

### All variables with value smaller zero:

### End

real	1444m14.146s
user	4857m58.885s
sys	908m57.921s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
