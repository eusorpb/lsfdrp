	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_2_0_0_fr.p
Instance LSFDP: LSFDRP_2_2_0_0_fd_52.p

This object contains the information regarding the instance 2_2_0_0 for a duration of 52 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.060965 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.179893 sec.
***  |   |- Create additional vessel information,				took 0.002018 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 9724 rows, 12367 columns and 74634 nonzeros
Variable types: 5375 continuous, 6992 integer (6992 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -1.02061e+07
Presolve removed 8994 rows and 10570 columns
Presolve time: 0.50s
Presolved: 730 rows, 1797 columns, 12074 nonzeros
Variable types: 72 continuous, 1725 integer (1700 binary)

Root relaxation: objective 7.343985e+05, 315 iterations, 0.01 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 734398.541    0   41 -1.021e+07 734398.541   107%     -    0s
H    0     0                    -4955996.863 734398.541   115%     -    0s
H    0     0                    435882.51349 734398.541  68.5%     -    0s
     0     0 680390.226    0   22 435882.513 680390.226  56.1%     -    0s
     0     0 564991.914    0    3 435882.513 564991.914  29.6%     -    0s
     0     0     cutoff    0      435882.513 435882.513  0.00%     -    0s
     0     0     cutoff    0      435882.513 435882.513  0.00%     -    0s

Cutting planes:
  MIR: 1

Explored 1 nodes (364 simplex iterations) in 0.61 seconds
Thread count was 4 (of 16 available processors)

Solution count 3: 435883 -4.956e+06 -1.02061e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective 4.358825134891e+05, best bound 4.358825134891e+05, gap 0.0000%
Took  1.5018789768218994
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 8293 rows, 6970 columns and 25250 nonzeros
Variable types: 6358 continuous, 612 integer (612 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 8293 rows and 6970 columns
Presolve time: 0.02s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.03 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 540300 

Optimal solution found (tolerance 1.00e-04)
Best objective 5.402997509928e+05, best bound 5.402997509928e+05, gap 0.0000%

### Instance
### LSFDRP_2_2_0_0_fd_52

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 35
### |A|: 32
### |A^i|: 30
### |A^f|: 2
### |M|: 193
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is 540,299.751 (540299.751)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 275, 201, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAP
###  |  TM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  312.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 278
###  |   |- and carried Demand 120,   10.0 containers from 201 to 278
###  |   |- and carried Demand 121,  484.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 302 (HNPCR In
###  |  @1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [102, 234, 0]
###  |  102 (DEHAM Out@624) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [105, 36, 240, 236, 0]
###  |  105 (DKAAR Out@388) - 36 (CLLQN In@1240/Out@1258) - 240 (PECLL In@1419/Out@1496) - 236 (PABLB In@1640/Out@1652) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 10, 11, 12, 15, 16, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m3.358s
user	0m2.834s
sys	0m0.219s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
