	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_46.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 111 rows, 168 columns and 558 nonzeros
Variable types: 0 continuous, 168 integer (168 binary)
Coefficient statistics:
  Matrix range     [1e+00, 6e+00]
  Objective range  [9e+05, 5e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Found heuristic solution: objective -9.71081e+07
Presolve removed 43 rows and 60 columns
Presolve time: 0.01s
Presolved: 68 rows, 108 columns, 319 nonzeros
Variable types: 0 continuous, 108 integer (108 binary)

Root relaxation: objective -9.085949e+06, 79 iterations, 0.00 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -9085949.0    0   31 -9.711e+07 -9085949.0  90.6%     -    0s
H    0     0                    -7.87302e+07 -9085949.0  88.5%     -    0s
H    0     0                    -5.39194e+07 -9085949.0  83.1%     -    0s
H    0     0                    -5.05883e+07 -9085949.0  82.0%     -    0s
H    0     0                    -4.80543e+07 -9913215.5  79.4%     -    0s
H    0     0                    -3.77357e+07 -9913215.5  73.7%     -    0s
H    0     0                    -3.36684e+07 -9913215.5  70.6%     -    0s
H    0     0                    -3.34894e+07 -9913215.5  70.4%     -    0s
H    0     0                    -3.20885e+07 -9913215.5  69.1%     -    0s
     0     0 -3.156e+07    0   46 -3.209e+07 -3.156e+07  1.64%     -    0s
     0     0 -3.156e+07    0   45 -3.209e+07 -3.156e+07  1.64%     -    0s
     0     2 -3.156e+07    0   45 -3.209e+07 -3.156e+07  1.64%     -    0s

Cutting planes:
  Gomory: 1
  Clique: 8
  Zero half: 1

Explored 11 nodes (262 simplex iterations) in 0.04 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: -3.20885e+07 -3.34894e+07 -3.36684e+07 ... -9.71081e+07

Optimal solution found (tolerance 1.00e-04)
Best objective -3.208847381081e+07, best bound -3.208847381081e+07, gap 0.0000%

### Objective value in 1k: -32088.47
### Objective value: -32088473.8108
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 7(ME3): 19, (PMax28), 5
### 81(MECL1): 30, (PMax35), 6
### 177(WCSA): 18, (PMax25), 3
### 24(WAF7): 18, (PMax25), 3
### 121(SAE): 19, (PMax28), 1
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[7,19] = 1
rho[81,30] = 1
rho[177,18] = 1
rho[24,18] = 1
rho[121,19] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[81,30,1] = 1
eta[81,30,2] = 1
eta[81,30,3] = 1
eta[81,30,4] = 1
eta[81,30,5] = 1
eta[81,30,6] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[121,19,1] = 1
y[1,177] = 1
y[2,24] = 1
y[3,177] = 1
y[4,24] = 1
y[5,7] = 1
y[6,7] = 1
y[7,7] = 1
y[8,121] = 1
y[9,81] = 1
y[10,81] = 1
y[11,81] = 1
y[12,81] = 1
y[13,177] = 1
y[14,24] = 1
y[15,7] = 1
y[16,7] = 1
y[17,81] = 1
y[18,81] = 1

### All variables with value zero:
rho[7,18] = 0
rho[7,30] = 0
rho[81,18] = 0
rho[81,19] = -0
rho[177,19] = 0
rho[177,30] = 0
rho[24,19] = 0
rho[24,30] = 0
rho[121,18] = 0
rho[121,30] = 0
eta[7,18,1] = 0
eta[7,18,2] = 0
eta[7,18,3] = 0
eta[7,18,4] = 0
eta[7,18,5] = 0
eta[7,18,6] = -0
eta[7,19,6] = -0
eta[7,30,1] = 0
eta[7,30,2] = 0
eta[7,30,3] = 0
eta[7,30,4] = 0
eta[7,30,5] = 0
eta[7,30,6] = -0
eta[81,18,1] = -0
eta[81,18,2] = 0
eta[81,18,3] = 0
eta[81,18,4] = 0
eta[81,18,5] = 0
eta[81,18,6] = 0
eta[81,19,1] = 0
eta[81,19,2] = 0
eta[81,19,3] = 0
eta[81,19,4] = 0
eta[81,19,5] = 0
eta[81,19,6] = 0
eta[177,19,1] = 0
eta[177,19,2] = 0
eta[177,19,3] = 0
eta[177,30,1] = 0
eta[177,30,2] = 0
eta[177,30,3] = 0
eta[24,18,4] = -0
eta[24,19,1] = 0
eta[24,19,2] = 0
eta[24,19,3] = 0
eta[24,19,4] = -0
eta[24,30,1] = 0
eta[24,30,2] = 0
eta[24,30,3] = 0
eta[24,30,4] = -0
eta[121,18,1] = -0
eta[121,18,2] = 0
eta[121,19,2] = 0
eta[121,30,1] = 0
eta[121,30,2] = 0
y[1,7] = -0
y[1,81] = 0
y[1,24] = -0
y[1,121] = -0
y[2,7] = -0
y[2,81] = 0
y[2,177] = 0
y[2,121] = -0
y[3,7] = 0
y[3,81] = 0
y[3,24] = 0
y[3,121] = -0
y[4,7] = -0
y[4,81] = 0
y[4,177] = -0
y[4,121] = -0
y[5,81] = 0
y[5,177] = -0
y[5,24] = -0
y[5,121] = -0
y[6,81] = 0
y[6,177] = -0
y[6,24] = -0
y[6,121] = -0
y[7,81] = 0
y[7,177] = -0
y[7,24] = -0
y[7,121] = -0
y[8,7] = 0
y[8,81] = 0
y[8,177] = -0
y[8,24] = -0
y[9,7] = -0
y[9,177] = -0
y[9,24] = -0
y[9,121] = 0
y[10,7] = -0
y[10,177] = -0
y[10,24] = -0
y[10,121] = 0
y[11,7] = -0
y[11,177] = -0
y[11,24] = -0
y[11,121] = 0
y[12,7] = -0
y[12,177] = -0
y[12,24] = -0
y[12,121] = 0
y[13,7] = -0
y[13,81] = 0
y[13,24] = 0
y[13,121] = -0
y[14,7] = -0
y[14,81] = 0
y[14,177] = 0
y[14,121] = -0
y[15,81] = 0
y[15,177] = -0
y[15,24] = -0
y[15,121] = -0
y[16,81] = 0
y[16,177] = -0
y[16,24] = -0
y[16,121] = -0
y[17,7] = -0
y[17,177] = -0
y[17,24] = -0
y[17,121] = 0
y[18,7] = -0
y[18,177] = -0
y[18,24] = -0
y[18,121] = 0

### End

real	0m0.699s
user	0m0.415s
sys	0m0.093s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
