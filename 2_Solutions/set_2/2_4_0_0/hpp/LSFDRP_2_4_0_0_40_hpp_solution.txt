	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_40.p

This object contains the information regarding the instance 2_4_0_0 for a duration of 40 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.031907 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.409768 sec.
***  |   |- Create additional vessel information,				took 0.004881 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 33989 rows, 100787 columns and 777439 nonzeros
Variable types: 24642 continuous, 76145 integer (76145 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 28417 rows and 64708 columns
Presolve time: 3.67s
Presolved: 5572 rows, 36079 columns, 164175 nonzeros
Variable types: 388 continuous, 35691 integer (35481 binary)

Root relaxation: objective 6.626463e+06, 6722 iterations, 0.97 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 5711698.43    0  576          - 5711698.43      -     -    5s
H    0     0                    -1583107.213 5711698.43   461%     -    5s
H    0     0                    -499226.1775 5711698.43  1244%     -   10s
     0     0 4687522.04    0  746 -499226.18 4687522.04  1039%     -   10s
     0     0 4686803.19    0  744 -499226.18 4686803.19  1039%     -   10s
     0     0 4389987.85    0  688 -499226.18 4389987.85   979%     -   11s
     0     0 4371417.65    0  660 -499226.18 4371417.65   976%     -   12s
     0     0 4360816.83    0  679 -499226.18 4360816.83   974%     -   12s
     0     0 4360816.83    0  678 -499226.18 4360816.83   974%     -   12s
     0     0 4283041.74    0  696 -499226.18 4283041.74   958%     -   12s
     0     0 4282183.84    0  689 -499226.18 4282183.84   958%     -   12s
     0     0 4272603.27    0  693 -499226.18 4272603.27   956%     -   12s
     0     0 4265009.47    0  684 -499226.18 4265009.47   954%     -   13s
     0     0 4012898.29    0  588 -499226.18 4012898.29   904%     -   13s
     0     0 4012553.40    0  600 -499226.18 4012553.40   904%     -   13s
     0     0 3694935.25    0  528 -499226.18 3694935.25   840%     -   13s
     0     0 3646755.03    0  565 -499226.18 3646755.03   830%     -   14s
     0     0 3646755.03    0  437 -499226.18 3646755.03   830%     -   16s
     0     0 3527491.20    0  467 -499226.18 3527491.20   807%     -   17s
     0     0 3369473.44    0  447 -499226.18 3369473.44   775%     -   17s
     0     0 3367593.68    0  435 -499226.18 3367593.68   775%     -   18s
     0     0 3367037.62    0  466 -499226.18 3367037.62   774%     -   18s
     0     0 3196346.44    0  568 -499226.18 3196346.44   740%     -   18s
H    0     0                    -479689.0353 3196346.44   766%     -   18s
     0     0 3196085.80    0  500 -479689.04 3196085.80   766%     -   18s
     0     0 3181284.50    0  480 -479689.04 3181284.50   763%     -   18s
     0     0 3181099.93    0  509 -479689.04 3181099.93   763%     -   18s
     0     0 3173910.74    0  481 -479689.04 3173910.74   762%     -   18s
     0     0 3171224.77    0  488 -479689.04 3171224.77   761%     -   19s
     0     0 3171186.10    0  503 -479689.04 3171186.10   761%     -   19s
     0     0 3169901.37    0  532 -479689.04 3169901.37   761%     -   19s
     0     0 3166626.89    0  504 -479689.04 3166626.89   760%     -   19s
     0     0 3111019.43    0  547 -479689.04 3111019.43   749%     -   19s
     0     0 3094385.20    0  543 -479689.04 3094385.20   745%     -   19s
     0     0 3094385.20    0  543 -479689.04 3094385.20   745%     -   19s
     0     0 3088800.86    0  538 -479689.04 3088800.86   744%     -   19s
     0     0 3088566.16    0  516 -479689.04 3088566.16   744%     -   19s
     0     0 3082762.77    0  525 -479689.04 3082762.77   743%     -   20s
     0     0 3079116.97    0  539 -479689.04 3079116.97   742%     -   20s
     0     0 3079116.97    0  546 -479689.04 3079116.97   742%     -   20s
     0     0 3064398.96    0  585 -479689.04 3064398.96   739%     -   20s
     0     0 3056828.12    0  553 -479689.04 3056828.12   737%     -   20s
     0     0 3056042.06    0  593 -479689.04 3056042.06   737%     -   20s
     0     0 3055939.11    0  592 -479689.04 3055939.11   737%     -   20s
     0     0 3004574.15    0  580 -479689.04 3004574.15   726%     -   20s
     0     0 3003149.58    0  620 -479689.04 3003149.58   726%     -   21s
     0     0 3002750.58    0  620 -479689.04 3002750.58   726%     -   21s
     0     0 3002736.75    0  620 -479689.04 3002736.75   726%     -   21s
     0     0 3002736.75    0  619 -479689.04 3002736.75   726%     -   21s
     0     2 3002736.75    0  619 -479689.04 3002736.75   726%     -   22s
    92    89 1402040.52   23  359 -479689.04 2682378.01   659%   177   25s
   199   185 1223602.19   39  504 -479689.04 2682378.01   659%   109   30s
H  211   190                    -309589.6514 2682378.01   966%   105   30s
   350   309 419731.054   61  463 -309589.65 2682378.01   966%   122   35s
H  658   473                    -278811.4432 1786476.80   741%   111   37s
*  659   473              24    -278613.9137 1786476.80   741%   111   37s
   887   596     cutoff   12      -278613.91 1630443.67   685%   111   40s
H 1060   676                    -252790.1845 1590550.71   729%   109   41s
  1061   677 1522749.04   10  565 -252790.18 1590550.71   729%   109   48s
H 1062   644                    -252790.1347 1590550.71   729%   108   51s
  1069   648 -196483.25   36  566 -252790.13 1590550.71   729%   108   55s
  1082   657 721423.482   10  502 -252790.13 1590550.71   729%   106   60s
  1091   663 -61403.621   41  528 -252790.13 1590550.71   729%   106   65s
  1093   664 789408.110   21  530 -252790.13 1590550.71   729%   105   70s
H 1094   631                    -213451.7723 1590550.71   845%   105   76s
  1096   635 1435171.37   11  580 -213451.77 1590550.71   845%   115   91s
  1102   639 963102.675   13  523 -213451.77 1255441.90   688%   117   95s
* 1171   594              16    -213451.7319 882057.459   513%   117   97s
* 1275   543              20    -151853.6890 479858.719   416%   115   98s

Cutting planes:
  Gomory: 15
  Implied bound: 17
  Projected implied bound: 17
  Clique: 14
  MIR: 8
  Flow cover: 11
  Zero half: 24

Explored 1393 nodes (174795 simplex iterations) in 99.99 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -151854 -213452 -213452 ... -499226

Optimal solution found (tolerance 1.00e-04)
Best objective -1.518536889780e+05, best bound -1.518536889780e+05, gap 0.0000%
Took  103.64779353141785
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 37859 rows, 34840 columns and 117445 nonzeros
Variable types: 33214 continuous, 1626 integer (1626 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 5e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 37859 rows and 34840 columns
Presolve time: 0.05s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.07 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -2.80549e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective -2.805487439403e+07, best bound -2.805487439403e+07, gap 0.0000%

### Instance
### LSFDRP_2_4_0_0_fd_40

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 91
### |A|: 86
### |A^i|: 86
### |A^f|: 0
### |M|: 382
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is -28,054,874.394 (-28054874.394)
###
### Service 7 (ME3) is operated by 5 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 133, 199, 290, 200, 112, 284, 45, 367, 330, -3]
###  |  218 (NLRTM Out@92) - 133 (FRLEH In@111/Out@127) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPT
###  |  M In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 367 (TRMER 
###  |  In@915/Out@959) - 330 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,   19.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel  6 has the path [219, 364, 334, 352, 229, 327, -3]
###  |  219 (NLRTM Out@260) - 364 (TRMER In@411/Out@455) - 334 (EGPSD In@531/Out@550) - 352 (OMSLL In@710/Out@722) - 229 (OMS
###  |  LL In@721/Out@736) - 327 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 214,  164.0 containers from 334 to 327
###  |   |- and carried Demand 215,   58.0 containers from 334 to 327
###  |
###  |- Vessel  7 has the path [221, 365, 353, 230, 328, -3]
###  |  221 (NLRTM Out@428) - 365 (TRMER In@579/Out@623) - 353 (OMSLL In@878/Out@890) - 230 (OMSLL In@889/Out@904) - 328 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |
###  |- Vessel  8 has the path [223, 366, 338, 339, 329, -3]
###  |  223 (NLRTM Out@596) - 366 (TRMER In@747/Out@791) - 338 (EGPSD In@867/Out@886) - 339 (EGPSD In@957/Out@973) - 329 (AEJ
###  |  EA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158) - 356 (TRAMB In@347/Out@395) - 325 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 275, 201, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAP
###  |  TM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 278
###  |   |- and carried Demand 120,   10.0 containers from 201 to 278
###  |   |- and carried Demand 121,  484.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  312.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 436, 452, 443, 273, 373, 426, 354, 376, 429, 409, -4]
###  |  1 (BEANR Out@66) - 436 (USCHS In@378/Out@388) - 452 (USORF In@413/Out@426) - 443 (USEWR In@450/Out@466) - 273 (TRZMK 
###  |  In@785/Out@799) - 373 (AEJEA In@899/Out@923) - 426 (PKBQM In@980/Out@1010) - 354 (OMSLL In@1300/Out@1313) - 376 (AEJE
###  |  A In@1403/Out@1427) - 429 (PKBQM In@1484/Out@1514) - 409 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |   |- and carried Demand 342,  254.0 containers from 426 to 409
###  |   |- and carried Demand 343,    1.0 containers from 426 to 409
###  |   |- and carried Demand 344,  135.0 containers from 426 to 409
###  |   |- and carried Demand 347,   75.0 containers from 373 to 409
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |   |- and carried Demand 350,   26.0 containers from 373 to 409
###  |   |- and carried Demand 351,  144.0 containers from 373 to 409
###  |   |- and carried Demand 352,    1.0 containers from 373 to 409
###  |   |- and carried Demand 354,  339.0 containers from 452 to 373
###  |   |- and carried Demand 355,  115.0 containers from 452 to 373
###  |   |- and carried Demand 357,    2.0 containers from 452 to 426
###  |   |- and carried Demand 358,  194.0 containers from 436 to 373
###  |   |- and carried Demand 359,   49.0 containers from 436 to 373
###  |   |- and carried Demand 360,   40.0 containers from 436 to 426
###  |
###  |- Vessel 10 has the path [2, 160, 404, -4]
###  |  2 (BEANR Out@234) - 160 (GRPIR In@528/Out@553) - 404 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [5, 266, 405, -4]
###  |  5 (BEANR Out@402) - 266 (TRALI In@668/Out@686) - 405 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 12 has the path [8, 267, 374, 427, 407, -4]
###  |  8 (BEANR Out@570) - 267 (TRALI In@836/Out@854) - 374 (AEJEA In@1067/Out@1091) - 427 (PKBQM In@1148/Out@1178) - 407 (I
###  |  NNSA In@1242/Out@1258) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |
###  |- Vessel 17 has the path [145, 268, 372, 425, 231, 375, 428, 408, -4]
###  |  145 (GBFXT Out@302) - 268 (TRAMB In@576/Out@603) - 372 (AEJEA In@731/Out@755) - 425 (PKBQM In@812/Out@842) - 231 (OMS
###  |  LL In@1132/Out@1145) - 375 (AEJEA In@1235/Out@1259) - 428 (PKBQM In@1316/Out@1346) - 408 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 362,  244.0 containers from 425 to 408
###  |   |- and carried Demand 363,   67.0 containers from 425 to 408
###  |   |- and carried Demand 365,  118.0 containers from 372 to 408
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |   |- and carried Demand 368,   15.0 containers from 372 to 408
###  |   |- and carried Demand 369,  267.0 containers from 372 to 408
###  |   |- and carried Demand 370,    1.0 containers from 372 to 408
###  |
###  |- Vessel 18 has the path [148, 161, 406, -4]
###  |  148 (GBFXT Out@470) - 161 (GRPIR In@696/Out@721) - 406 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 1 vessels from type 19 (PMax28)
###  |
###  |- Vessel 16 has the path [136, 315, 311, 301, -2]
###  |  136 (FRLEH Out@326) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [102, 358, 269, 235, 0]
###  |  102 (DEHAM Out@624) - 358 (TRAMB In@683/Out@731) - 269 (TRAMB In@744/Out@771) - 235 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [103, 236, 0]
###  |  103 (DEHAM Out@792) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [105, 272, 234, 0]
###  |  105 (DKAAR Out@388) - 272 (TRZMK In@617/Out@631) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	1m48.273s
user	5m42.167s
sys	0m32.587s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
