	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_49 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 6e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 8252 rows and 1127 columns (presolve time = 6s) ...
Presolve removed 34358 rows and 1827944 columns (presolve time = 11s) ...
Presolve removed 87853 rows and 4785194 columns (presolve time = 15s) ...
Presolve removed 128330 rows and 6150778 columns (presolve time = 20s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 25s) ...
Presolve removed 167259 rows and 6806027 columns (presolve time = 30s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 36s) ...
Presolve removed 169401 rows and 6807829 columns (presolve time = 42s) ...
Presolve removed 169457 rows and 6808029 columns (presolve time = 45s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 50s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 61s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 104s) ...
Presolve removed 169716 rows and 6808346 columns (presolve time = 106s) ...
Presolve removed 169729 rows and 6808357 columns (presolve time = 110s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 956s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 956.46s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43263256e+15  1.58531903e+16  1.49e+08 3.62e+06  2.18e+12  1063s
   1  -2.43453301e+15  1.55138076e+16  8.21e+07 2.64e+07  1.23e+12  1120s
   2  -1.60439208e+15  1.49561976e+16  5.41e+07 1.69e+07  8.25e+11  1178s
   3  -1.20259302e+15  1.40589575e+16  4.06e+07 9.87e+06  6.12e+11  1231s
   4  -6.14390868e+14  1.28272215e+16  2.08e+07 5.22e+06  3.19e+11  1289s
   5  -2.94922822e+14  1.07470585e+16  1.00e+07 2.24e+06  1.56e+11  1344s
   6  -1.56977900e+14  7.99387541e+15  5.34e+06 9.79e+05  8.70e+10  1398s
   7  -4.34684724e+13  4.67644664e+15  1.48e+06 1.42e+05  2.52e+10  1454s

Barrier performed 7 iterations in 1453.90 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 159.13s (can be avoided by choosing Method=3)

Solved with dual simplex

Root relaxation: objective 1.317767e+07, 141757 iterations, 644.98 seconds
Total elapsed time = 1610.74s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2897e+07    0  554          - 1.2897e+07      -     - 1715s
     0     0 7314960.63    0  654          - 7314960.63      -     - 2741s
H    0     0                    -9.15425e+07 7314960.63   108%     - 2744s
     0     0 6878807.01    0  624 -9.154e+07 6878807.01   108%     - 3004s
     0     0 6878807.01    0  624 -9.154e+07 6878807.01   108%     - 3005s
     0     0 6276422.69    0  687 -9.154e+07 6276422.69   107%     - 3721s
     0     0 6258684.61    0  711 -9.154e+07 6258684.61   107%     - 3957s
     0     0 6258642.07    0  716 -9.154e+07 6258642.07   107%     - 3967s
     0     0 6072573.20    0  780 -9.154e+07 6072573.20   107%     - 4561s
H    0     0                    -7.91450e+07 6072573.20   108%     - 4563s
     0     0 6066746.91    0  784 -7.915e+07 6066746.91   108%     - 4635s
     0     0 6066746.91    0  784 -7.915e+07 6066746.91   108%     - 4636s
     0     0 6040550.94    0  788 -7.915e+07 6040550.94   108%     - 5108s
     0     0 6029555.31    0  778 -7.915e+07 6029555.31   108%     - 5140s
     0     0 6029555.31    0  780 -7.915e+07 6029555.31   108%     - 5142s
     0     0 6028465.48    0  773 -7.915e+07 6028465.48   108%     - 5156s
     0     0 6028465.48    0  773 -7.915e+07 6028465.48   108%     - 5161s
     0     0 6028041.50    0  821 -7.915e+07 6028041.50   108%     - 5175s
     0     0 6028032.79    0  819 -7.915e+07 6028032.79   108%     - 5181s
     0     0 6027583.74    0  821 -7.915e+07 6027583.74   108%     - 5188s
     0     0 6027583.74    0  821 -7.915e+07 6027583.74   108%     - 5194s
     0     0 6026243.26    0  822 -7.915e+07 6026243.26   108%     - 5204s
     0     0 6025738.69    0  823 -7.915e+07 6025738.69   108%     - 5212s
     0     0 6025736.62    0  824 -7.915e+07 6025736.62   108%     - 5216s
     0     0 6024384.64    0  833 -7.915e+07 6024384.64   108%     - 5226s
     0     0 6023739.64    0  829 -7.915e+07 6023739.64   108%     - 5231s
     0     0 6023739.56    0  829 -7.915e+07 6023739.56   108%     - 5233s
     0     0 6023739.56    0  821 -7.915e+07 6023739.56   108%     - 5713s
H    0     0                    -6.09195e+07 6023739.56   110%     - 5723s
H    0     0                    -4.98975e+07 6023739.56   112%     - 8260s
     0     2 6023739.56    0  819 -4.990e+07 6023739.56   112%     - 8910s
     1     4 2973354.58    1  655 -4.990e+07 5997141.10   112% 2965438 66498s
     3     8 -1318563.4    2  582 -4.990e+07 5739230.69   112% 1001390 68503s

Cutting planes:
  Implied bound: 7
  Clique: 48
  MIR: 42
  GUB cover: 1
  Zero half: 8

Explored 7 nodes (3500808 simplex iterations) in 86400.54 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -4.98975e+07 -6.09195e+07 -7.9145e+07 -9.15425e+07 

Time limit reached
Best objective -4.989753474648e+07, best bound 4.540617081950e+06, gap 109.0999%

***
***  |- Calculation finished, timelimit reached 86,400.65 (86400.65) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -49,897,534.7465 (-49897534.7465) and is feasible with a Gap of 1.091
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 275, 201, 371, 424, 230, 328, -3]
###  |  218 (NLRTM Out@92.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@375.0/Out@396.0) - 371 (AEJEA In@563.0/Out@587.
###  |  0) - 424 (PKBQM In@644.0/Out@674.0) - 230 (OMSLL In@889.0/Out@904.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_E
###  |  ND Ziel-Service 7)
###  |   |- and carried Demand 116,   42.0 containers from 218 to 275
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel  6 has the path [219, 364, 325, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel  7 has the path [221, 266, 327, -3]
###  |  221 (NLRTM Out@428.0) - 266 (TRALI In@668.0/Out@686.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel  8 has the path [223, 366, 329, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 437, 453, 445, 362, 331, -3]
###  |  134 (FRLEH Out@158.0) - 437 (USCHS In@546.0/Out@556.0) - 453 (USORF In@581.0/Out@594.0) - 445 (USEWR In@618.0/Out@634
###  |  .0) - 362 (TRAMB In@1416.0/Out@1443.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 16 has the path [136, 357, 335, 336, 337, 338, 339, 330, -3]
###  |  136 (FRLEH Out@326.0) - 357 (TRAMB In@515.0/Out@563.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718
###  |  .0) - 337 (EGPSD In@789.0/Out@805.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 330 (AEJEA I
###  |  n@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 11 has the path [5, 277, -1]
###  |  5 (BEANR Out@402.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 17 has the path [145, 276, -1]
###  |  145 (GBFXT Out@302.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 268, 404, -4]
###  |  100 (DEHAM Out@288.0) - 268 (TRAMB In@576.0/Out@603.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 
###  |  81)
###  |- Vessel  2 has the path [101, 365, 353, 231, 375, 428, 408, -4]
###  |  101 (DEHAM Out@456.0) - 365 (TRMER In@579.0/Out@623.0) - 353 (OMSLL In@878.0/Out@890.0) - 231 (OMSLL In@1132.0/Out@11
###  |  45.0) - 375 (AEJEA In@1235.0/Out@1259.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (
###  |  DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |- Vessel  3 has the path [102, 358, 273, 374, 427, 407, -4]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 374 (AEJEA In@1067.0/Out@10
###  |  91.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel  4 has the path [103, 376, 429, 409, -4]
###  |  103 (DEHAM Out@792.0) - 376 (AEJEA In@1403.0/Out@1427.0) - 429 (PKBQM In@1484.0/Out@1514.0) - 409 (INNSA In@1578.0/Ou
###  |  t@1594.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |- Vessel 13 has the path [104, 356, 352, 229, 373, 426, 406, -4]
###  |  104 (DKAAR Out@220.0) - 356 (TRAMB In@347.0/Out@395.0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736
###  |  .0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@1010.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY
###  |  _END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel 14 has the path [105, 272, 372, 425, 405, -4]
###  |  105 (DKAAR Out@388.0) - 272 (TRZMK In@617.0/Out@631.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 361,   44.0 containers from 425 to 405
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@76
###  |  5.0/Out@811.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to  45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 10 has the path [2, 291, 202, 114, 301, -2]
###  |  2 (BEANR Out@234.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0
###  |  ) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 12 has the path [8, 267, 235, 0]
###  |  8 (BEANR Out@570.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 1
###  |  77)
###  |- Vessel 18 has the path [148, 234, 0]
###  |  148 (GBFXT Out@470.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,19] = 1
rho[81,18] = 1
rho[177,30] = 1
rho[24,30] = 1
rho[121,30] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
eta[177,30,1] = 1
eta[177,30,2] = 1
eta[24,30,1] = 1
eta[24,30,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,404,-4] = 1
y[1,100,268] = 1
y[1,268,404] = 1
y[2,365,353] = 1
y[2,375,428] = 1
y[2,101,365] = 1
y[2,231,375] = 1
y[2,428,408] = 1
y[2,353,231] = 1
y[2,408,-4] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,407,-4] = 1
y[3,273,374] = 1
y[3,427,407] = 1
y[3,374,427] = 1
y[4,376,429] = 1
y[4,409,-4] = 1
y[4,103,376] = 1
y[4,429,409] = 1
y[5,275,201] = 1
y[5,424,230] = 1
y[5,328,-3] = 1
y[5,230,328] = 1
y[5,218,275] = 1
y[5,201,371] = 1
y[5,371,424] = 1
y[6,364,325] = 1
y[6,219,364] = 1
y[6,325,-3] = 1
y[7,327,-3] = 1
y[7,266,327] = 1
y[7,221,266] = 1
y[8,223,366] = 1
y[8,329,-3] = 1
y[8,366,329] = 1
y[9,200,112] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,1,274] = 1
y[9,45,302] = 1
y[9,199,290] = 1
y[9,302,-2] = 1
y[9,284,45] = 1
y[9,112,284] = 1
y[10,301,-2] = 1
y[10,114,301] = 1
y[10,291,202] = 1
y[10,202,114] = 1
y[10,2,291] = 1
y[11,277,-1] = 1
y[11,5,277] = 1
y[12,235,0] = 1
y[12,267,235] = 1
y[12,8,267] = 1
y[13,352,229] = 1
y[13,426,406] = 1
y[13,373,426] = 1
y[13,229,373] = 1
y[13,406,-4] = 1
y[13,104,356] = 1
y[13,356,352] = 1
y[14,372,425] = 1
y[14,425,405] = 1
y[14,405,-4] = 1
y[14,105,272] = 1
y[14,272,372] = 1
y[15,134,437] = 1
y[15,453,445] = 1
y[15,445,362] = 1
y[15,331,-3] = 1
y[15,362,331] = 1
y[15,437,453] = 1
y[16,337,338] = 1
y[16,336,337] = 1
y[16,339,330] = 1
y[16,357,335] = 1
y[16,335,336] = 1
y[16,338,339] = 1
y[16,136,357] = 1
y[16,330,-3] = 1
y[17,276,-1] = 1
y[17,145,276] = 1
y[18,234,0] = 1
y[18,148,234] = 1
xD[7,200,112] = 161
xD[7,290,200] = 161
xD[7,199,290] = 161
xD[7,284,45] = 161
xD[7,112,284] = 161
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[45,200,112] = 161
xD[45,284,45] = 161
xD[45,112,284] = 161
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[116,218,275] = 42
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,200,112] = 161
xD[144,290,200] = 161
xD[144,284,45] = 161
xD[144,112,284] = 161
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[289,429,409] = 34
xD[290,376,429] = 84
xD[291,376,429] = 50
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[361,425,405] = 44
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[45] = 765
zE[112] = 284
zE[114] = 452
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[229] = 721
zE[230] = 889
zE[231] = 1132
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[272] = 617
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[335] = 621
zE[336] = 699
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[357] = 515
zE[358] = 683
zE[362] = 1416
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[376] = 1403
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[424] = 644
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zE[429] = 1484
zE[437] = 546
zE[445] = 618
zE[453] = 581
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[114] = 464
zX[134] = 158
zX[136] = 326
zX[145] = 302
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[231] = 1145
zX[234] = 1316
zX[235] = 1484
zX[240] = 4085
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[272] = 631
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[335] = 637
zX[336] = 718
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[357] = 563
zX[358] = 731
zX[362] = 1443
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[376] = 1427
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[424] = 674
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
zX[429] = 1514
zX[437] = 556
zX[445] = 634
zX[453] = 594
phi[1] = 466
phi[2] = 970
phi[3] = 634
phi[4] = 802
phi[5] = 967
phi[6] = 463
phi[7] = 463
phi[8] = 631
phi[9] = 1113
phi[10] = 777
phi[11] = 281
phi[12] = 914
phi[13] = 870
phi[14] = 534
phi[15] = 1405
phi[16] = 1069
phi[17] = 213
phi[18] = 846

### All variables with value smaller zero:

### End

real	1450m24.971s
user	5050m35.209s
sys	719m41.839s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
