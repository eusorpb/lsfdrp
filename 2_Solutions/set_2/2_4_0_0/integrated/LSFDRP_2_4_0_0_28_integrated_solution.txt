	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_28.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_28 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 8252 rows and 1127 columns (presolve time = 5s) ...
Presolve removed 59838 rows and 3738761 columns (presolve time = 10s) ...
Presolve removed 102467 rows and 5244575 columns (presolve time = 15s) ...
Presolve removed 144271 rows and 6521856 columns (presolve time = 20s) ...
Presolve removed 166986 rows and 6806027 columns (presolve time = 27s) ...
Presolve removed 168048 rows and 6806306 columns (presolve time = 30s) ...
Presolve removed 169397 rows and 6807829 columns (presolve time = 36s) ...
Presolve removed 169401 rows and 6807973 columns (presolve time = 40s) ...
Presolve removed 169460 rows and 6808139 columns (presolve time = 45s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 57s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 98s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 100s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 899s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 898.72s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43135568e+15  9.05900047e+15  1.49e+08 2.07e+06  1.25e+12  1004s
   1  -2.43374704e+15  8.86504748e+15  8.21e+07 1.51e+07  7.04e+11  1061s
   2  -1.60361232e+15  8.54641059e+15  5.41e+07 9.63e+06  4.71e+11  1118s
   3  -1.20186772e+15  8.03291171e+15  4.06e+07 5.63e+06  3.50e+11  1173s
   4  -6.11078687e+14  7.31951464e+15  2.07e+07 2.94e+06  1.81e+11  1229s
   5  -2.88236440e+14  6.18005207e+15  9.81e+06 1.27e+06  8.69e+10  1284s
   6  -1.92914015e+14  4.48245130e+15  6.56e+06 5.02e+05  5.88e+10  1339s
   7  -6.18000174e+13  2.88206022e+15  2.10e+06 3.55e+04  1.97e+10  1394s

Barrier performed 7 iterations in 1394.22 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 419.27s (can be avoided by choosing Method=3)

Solved with dual simplex

Root relaxation: objective 1.319284e+07, 135414 iterations, 900.40 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2916e+07    0  687          - 1.2916e+07      -     - 1898s
     0     0 1.0765e+07    0  735          - 1.0765e+07      -     - 2287s
     0     0 1.0230e+07    0  725          - 1.0230e+07      -     - 2399s
     0     0 8572558.88    0  717          - 8572558.88      -     - 2496s
     0     0 8245316.60    0  766          - 8245316.60      -     - 2659s
H    0     0                    -4.40433e+07 8245316.60   119%     - 2661s
     0     0 8141138.83    0  715 -4.404e+07 8141138.83   118%     - 2820s
     0     0 8133730.83    0  722 -4.404e+07 8133730.83   118%     - 2838s
     0     0 8133718.38    0  722 -4.404e+07 8133718.38   118%     - 2840s
     0     0 8041719.73    0  732 -4.404e+07 8041719.73   118%     - 2985s
     0     0 8033302.31    0  750 -4.404e+07 8033302.31   118%     - 3011s
     0     0 8029306.92    0  762 -4.404e+07 8029306.92   118%     - 3039s
     0     0 8028161.08    0  768 -4.404e+07 8028161.08   118%     - 3050s
     0     0 8027985.20    0  751 -4.404e+07 8027985.20   118%     - 3060s
     0     0 8017179.70    0  789 -4.404e+07 8017179.70   118%     - 3099s
     0     0 8007032.49    0  813 -4.404e+07 8007032.49   118%     - 3150s
     0     0 8006595.38    0  807 -4.404e+07 8006595.38   118%     - 3181s
     0     0 7997264.81    0  801 -4.404e+07 7997264.81   118%     - 3221s
     0     0 7995752.29    0  793 -4.404e+07 7995752.29   118%     - 3234s
     0     0 7995140.30    0  796 -4.404e+07 7995140.30   118%     - 3239s
     0     0 7995130.61    0  799 -4.404e+07 7995130.61   118%     - 3241s
     0     0 7993547.48    0  795 -4.404e+07 7993547.48   118%     - 3256s
     0     0 7993499.70    0  793 -4.404e+07 7993499.70   118%     - 3263s
     0     0 7993158.00    0  785 -4.404e+07 7993158.00   118%     - 3267s
     0     0 7993155.68    0  784 -4.404e+07 7993155.68   118%     - 3274s
     0     0 7993155.68    0  784 -4.404e+07 7993155.68   118%     - 3276s
     0     0 7993153.40    0  784 -4.404e+07 7993153.40   118%     - 3282s
     0     0 7993151.97    0  788 -4.404e+07 7993151.97   118%     - 3286s
     0     0 7991666.23    0  792 -4.404e+07 7991666.23   118%     - 3302s
     0     0 7991653.23    0  798 -4.404e+07 7991653.23   118%     - 3304s
     0     0 7991653.23    0  798 -4.404e+07 7991653.23   118%     - 3307s
     0     0 7991653.23    0  798 -4.404e+07 7991653.23   118%     - 3763s
H    0     0                    -2.99221e+07 7991653.23   127%     - 3884s
     0     2 7991653.23    0  798 -2.992e+07 7991653.23   127%     - 3901s
     1     4 6006470.12    1  714 -2.992e+07 7991566.24   127% 2223078 59673s
     3     8 5719859.74    2  646 -2.992e+07 6382737.30   121% 777703 64507s
     7    10 5704253.98    3  625 -2.992e+07 5719852.95   119% 366196 86400s

Cutting planes:
  Implied bound: 11
  Clique: 51
  MIR: 49
  Flow cover: 4
  GUB cover: 1
  Zero half: 8

Explored 9 nodes (3349907 simplex iterations) in 86400.52 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -2.99221e+07 -4.40433e+07 

Time limit reached
Best objective -2.992207620351e+07, best bound 5.719852949360e+06, gap 119.1158%

***
***  |- Calculation finished, timelimit reached 86,400.63 (86400.63) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -29,922,076.2035 (-29922076.2035) and is feasible with a Gap of 1.1912
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 363, 260, 190, 170, 49, 75, 72, 53, 61, 79, 349, 331, -3]
###  |  218 (NLRTM Out@92.0) - 363 (TRMER In@243.0/Out@287.0) - 260 (SGSIN In@618.0/Out@651.0) - 190 (KRPUS In@788.0/Out@810.
###  |  0) - 170 (JPHKT In@824.0/Out@833.0) - 49 (CNDLC In@871.0/Out@886.0) - 75 (CNXGG In@904.0/Out@923.0) - 72 (CNTAO In@96
###  |  0.0/Out@978.0) - 53 (CNNGB In@1031.0/Out@1040.0) - 61 (CNSHA In@1069.0/Out@1079.0) - 79 (CNYTN In@1160.0/Out@1176.0) 
###  |  - 349 (MYTPP In@1250.0/Out@1270.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 194,  729.0 containers from  53 to 349
###  |   |- and carried Demand 195,   11.0 containers from  53 to 349
###  |   |- and carried Demand 196,    7.0 containers from  61 to 349
###  |- Vessel  6 has the path [219, 364, 325, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel  7 has the path [221, 266, 327, -3]
###  |  221 (NLRTM Out@428.0) - 266 (TRALI In@668.0/Out@686.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel  8 has the path [223, 366, 328, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 436, 452, 443, 168, 340, 330, -3]
###  |  134 (FRLEH Out@158.0) - 436 (USCHS In@378.0/Out@388.0) - 452 (USORF In@413.0/Out@426.0) - 443 (USEWR In@450.0/Out@466
###  |  .0) - 168 (ITGIT In@904.0/Out@923.0) - 340 (EGPSD In@1035.0/Out@1054.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMM
###  |  Y_END Ziel-Service 7)
###  |   |- and carried Demand 199,  188.0 containers from 340 to 330
###  |   |- and carried Demand 200,    7.0 containers from 340 to 330
###  |- Vessel 16 has the path [136, 202, 167, 338, 339, 329, -3]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 167 (ITGIT In@736.0/Out@755.0) - 338 (EGPSD In@867.0/Out@886
###  |  .0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 11 has the path [5, 277, -1]
###  |  5 (BEANR Out@402.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 17 has the path [145, 276, -1]
###  |  145 (GBFXT Out@302.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 268, 404, -4]
###  |  100 (DEHAM Out@288.0) - 268 (TRAMB In@576.0/Out@603.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 
###  |  81)
###  |- Vessel  2 has the path [101, 365, 353, 230, 374, 427, 407, -4]
###  |  101 (DEHAM Out@456.0) - 365 (TRMER In@579.0/Out@623.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904
###  |  .0) - 374 (AEJEA In@1067.0/Out@1091.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DU
###  |  MMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel  3 has the path [102, 358, 273, 373, 426, 406, -4]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 373 (AEJEA In@899.0/Out@923
###  |  .0) - 426 (PKBQM In@980.0/Out@1010.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel  4 has the path [103, 362, 409, -4]
###  |  103 (DEHAM Out@792.0) - 362 (TRAMB In@1416.0/Out@1443.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Serv
###  |  ice 81)
###  |- Vessel 13 has the path [104, 356, 352, 229, 405, -4]
###  |  104 (DKAAR Out@220.0) - 356 (TRAMB In@347.0/Out@395.0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel 14 has the path [105, 272, 372, 425, 231, 375, 428, 408, -4]
###  |  105 (DKAAR Out@388.0) - 272 (TRZMK In@617.0/Out@631.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 231 (OMSLL In@1132.0/Out@1145.0) - 375 (AEJEA In@1235.0/Out@1259.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (I
###  |  NNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 362,  244.0 containers from 425 to 408
###  |   |- and carried Demand 363,   67.0 containers from 425 to 408
###  |   |- and carried Demand 365,  118.0 containers from 372 to 408
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |   |- and carried Demand 368,   15.0 containers from 372 to 408
###  |   |- and carried Demand 369,  267.0 containers from 372 to 408
###  |   |- and carried Demand 370,    1.0 containers from 372 to 408
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 275, 201, 113, 301, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@3
###  |  75.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 291, 204, 234, 0]
###  |  2 (BEANR Out@234.0) - 291 (MAPTM In@379.0/Out@398.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@1316
###  |  .0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 18 has the path [148, 267, 235, 0]
###  |  148 (GBFXT Out@470.0) - 267 (TRALI In@836.0/Out@854.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,19] = 1
rho[81,18] = 1
rho[177,30] = 1
rho[24,30] = 1
rho[121,30] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
eta[177,30,1] = 1
eta[177,30,2] = 1
eta[24,30,1] = 1
eta[24,30,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,404,-4] = 1
y[1,100,268] = 1
y[1,268,404] = 1
y[2,365,353] = 1
y[2,407,-4] = 1
y[2,101,365] = 1
y[2,230,374] = 1
y[2,427,407] = 1
y[2,353,230] = 1
y[2,374,427] = 1
y[3,102,358] = 1
y[3,426,406] = 1
y[3,373,426] = 1
y[3,358,273] = 1
y[3,273,373] = 1
y[3,406,-4] = 1
y[4,362,409] = 1
y[4,103,362] = 1
y[4,409,-4] = 1
y[5,190,170] = 1
y[5,218,363] = 1
y[5,49,75] = 1
y[5,79,349] = 1
y[5,53,61] = 1
y[5,260,190] = 1
y[5,349,331] = 1
y[5,331,-3] = 1
y[5,363,260] = 1
y[5,61,79] = 1
y[5,75,72] = 1
y[5,72,53] = 1
y[5,170,49] = 1
y[6,364,325] = 1
y[6,219,364] = 1
y[6,325,-3] = 1
y[7,327,-3] = 1
y[7,266,327] = 1
y[7,221,266] = 1
y[8,223,366] = 1
y[8,328,-3] = 1
y[8,366,328] = 1
y[9,200,112] = 1
y[9,301,-2] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,112,275] = 1
y[9,275,201] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,201,113] = 1
y[9,113,301] = 1
y[10,234,0] = 1
y[10,204,234] = 1
y[10,291,204] = 1
y[10,2,291] = 1
y[11,277,-1] = 1
y[11,5,277] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,352,229] = 1
y[13,405,-4] = 1
y[13,229,405] = 1
y[13,104,356] = 1
y[13,356,352] = 1
y[14,372,425] = 1
y[14,425,231] = 1
y[14,375,428] = 1
y[14,105,272] = 1
y[14,231,375] = 1
y[14,428,408] = 1
y[14,408,-4] = 1
y[14,272,372] = 1
y[15,436,452] = 1
y[15,134,436] = 1
y[15,452,443] = 1
y[15,168,340] = 1
y[15,340,330] = 1
y[15,443,168] = 1
y[15,330,-3] = 1
y[16,136,202] = 1
y[16,202,167] = 1
y[16,329,-3] = 1
y[16,339,329] = 1
y[16,167,338] = 1
y[16,338,339] = 1
y[17,276,-1] = 1
y[17,145,276] = 1
y[18,148,267] = 1
y[18,235,0] = 1
y[18,267,235] = 1
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[46,200,112] = 580
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[131,200,112] = 580
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[194,79,349] = 729
xD[194,53,61] = 729
xD[194,61,79] = 729
xD[195,79,349] = 11
xD[195,53,61] = 11
xD[195,61,79] = 11
xD[196,79,349] = 7
xD[196,61,79] = 7
xD[199,340,330] = 188
xD[200,340,330] = 7
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[362,425,231] = 244
xD[362,375,428] = 244
xD[362,231,375] = 244
xD[362,428,408] = 244
xD[363,425,231] = 67
xD[363,375,428] = 67
xD[363,231,375] = 67
xD[363,428,408] = 67
xD[365,372,425] = 118
xD[365,425,231] = 118
xD[365,375,428] = 118
xD[365,231,375] = 118
xD[365,428,408] = 118
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[368,372,425] = 15
xD[368,425,231] = 15
xD[368,375,428] = 15
xD[368,231,375] = 15
xD[368,428,408] = 15
xD[369,372,425] = 267
xD[369,425,231] = 267
xD[369,375,428] = 267
xD[369,231,375] = 267
xD[369,428,408] = 267
xD[370,372,425] = 1
xD[370,425,231] = 1
xD[370,375,428] = 1
xD[370,231,375] = 1
xD[370,428,408] = 1
zE[49] = 871
zE[53] = 1031
zE[61] = 1069
zE[72] = 960
zE[75] = 904
zE[79] = 1160
zE[112] = 284
zE[113] = 421
zE[167] = 736
zE[168] = 904
zE[170] = 824
zE[190] = 788
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[204] = 711
zE[229] = 721
zE[230] = 889
zE[231] = 1132
zE[234] = 1304
zE[235] = 1472
zE[240] = 4085
zE[260] = 618
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[272] = 617
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[349] = 1250
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[358] = 683
zE[362] = 1416
zE[363] = 243
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zE[436] = 378
zE[443] = 450
zE[452] = 413
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[49] = 886
zX[53] = 1040
zX[61] = 1079
zX[72] = 978
zX[75] = 923
zX[79] = 1176
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[113] = 433
zX[134] = 158
zX[136] = 326
zX[145] = 302
zX[148] = 470
zX[167] = 755
zX[168] = 923
zX[170] = 833
zX[190] = 810
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[231] = 1145
zX[234] = 1316
zX[235] = 1484
zX[240] = 4085
zX[260] = 651
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[272] = 631
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[349] = 1270
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[358] = 731
zX[362] = 1443
zX[363] = 287
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
zX[436] = 388
zX[443] = 466
zX[452] = 426
phi[1] = 466
phi[2] = 802
phi[3] = 466
phi[4] = 802
phi[5] = 1471
phi[6] = 463
phi[7] = 463
phi[8] = 463
phi[9] = 945
phi[10] = 1082
phi[11] = 281
phi[12] = 609
phi[13] = 702
phi[14] = 1038
phi[15] = 1237
phi[16] = 901
phi[17] = 213
phi[18] = 1014

### All variables with value smaller zero:

### End

real	1449m8.175s
user	5008m41.572s
sys	760m34.208s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
