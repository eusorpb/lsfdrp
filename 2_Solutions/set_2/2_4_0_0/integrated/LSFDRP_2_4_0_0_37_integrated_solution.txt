	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_37.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_37 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 8252 rows and 1127 columns (presolve time = 7s) ...
Presolve removed 34347 rows and 1827933 columns (presolve time = 10s) ...
Presolve removed 87837 rows and 4785178 columns (presolve time = 16s) ...
Presolve removed 116409 rows and 5743382 columns (presolve time = 20s) ...
Presolve removed 147430 rows and 6588461 columns (presolve time = 25s) ...
Presolve removed 166986 rows and 6806027 columns (presolve time = 33s) ...
Presolve removed 167355 rows and 6806027 columns (presolve time = 35s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 41s) ...
Presolve removed 169401 rows and 6807829 columns (presolve time = 46s) ...
Presolve removed 169460 rows and 6808029 columns (presolve time = 51s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 63s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 117s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 120s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 934s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 933.66s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309380 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.286e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.835e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43617175e+15  1.19709050e+16  1.49e+08 2.74e+06  1.65e+12  1043s
   1  -2.43641921e+15  1.17151356e+16  8.21e+07 1.99e+07  9.31e+11  1099s
   2  -1.60550844e+15  1.12946769e+16  5.42e+07 1.27e+07  6.23e+11  1155s
   3  -1.20335495e+15  1.06172526e+16  4.07e+07 7.45e+06  4.62e+11  1211s
   4  -6.13375829e+14  9.68921920e+15  2.08e+07 3.95e+06  2.41e+11  1268s
   5  -3.00193260e+14  8.15820229e+15  1.02e+07 1.66e+06  1.19e+11  1324s
   6  -1.78774767e+14  6.06931302e+15  6.08e+06 7.09e+05  7.29e+10  1380s

Barrier performed 6 iterations in 1382.31 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.17s

Solved with dual simplex

Root relaxation: objective 1.317767e+07, 127752 iterations, 440.58 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2897e+07    0  660          - 1.2897e+07      -     - 1461s
     0     0 1.0048e+07    0  651          - 1.0048e+07      -     - 1788s
H    0     0                    -7.02176e+07 1.0048e+07   114%     - 1791s
     0     0 9697521.57    0  697 -7.022e+07 9697521.57   114%     - 1845s
     0     0 9697521.57    0  697 -7.022e+07 9697521.57   114%     - 1846s
     0     0 9164011.98    0  772 -7.022e+07 9164011.98   113%     - 2025s
     0     0 8919854.71    0  705 -7.022e+07 8919854.71   113%     - 2174s
     0     0 8919565.38    0  705 -7.022e+07 8919565.38   113%     - 2180s
     0     0 6870271.06    0  729 -7.022e+07 6870271.06   110%     - 2508s
H    0     0                    -6.10261e+07 6870271.06   111%     - 2509s
     0     0 6858974.23    0  724 -6.103e+07 6858974.23   111%     - 2528s
     0     0 6858932.73    0  723 -6.103e+07 6858932.73   111%     - 2530s
     0     0 6846674.22    0  738 -6.103e+07 6846674.22   111%     - 2562s
     0     0 6845683.17    0  737 -6.103e+07 6845683.17   111%     - 2568s
     0     0 6845181.18    0  737 -6.103e+07 6845181.18   111%     - 2571s
     0     0 6843397.31    0  731 -6.103e+07 6843397.31   111%     - 2583s
     0     0 6843172.69    0  733 -6.103e+07 6843172.69   111%     - 2590s
     0     0 6842903.10    0  732 -6.103e+07 6842903.10   111%     - 2595s
     0     0 6842755.90    0  732 -6.103e+07 6842755.90   111%     - 2602s
     0     0 6840789.86    0  731 -6.103e+07 6840789.86   111%     - 2606s
     0     0 6840143.26    0  739 -6.103e+07 6840143.26   111%     - 2613s
     0     0 6840143.26    0  741 -6.103e+07 6840143.26   111%     - 2614s
     0     0 6840143.06    0  741 -6.103e+07 6840143.06   111%     - 2617s
     0     0 6839709.00    0  733 -6.103e+07 6839709.00   111%     - 2623s
     0     0 6839634.29    0  737 -6.103e+07 6839634.29   111%     - 2628s
     0     0 6839634.29    0  731 -6.103e+07 6839634.29   111%     - 3007s
H    0     0                    -4.38772e+07 6839634.29   116%     - 3957s
     0     2 6839634.29    0  731 -4.388e+07 6839634.29   116%     - 4106s
     1     4 5619801.05    1  660 -4.388e+07 6799580.61   115% 2640055 72934s
     3     8 4792985.33    2  721 -4.388e+07 5619026.25   113% 943646 77608s
     7    12 4610990.60    3  689 -4.388e+07 4835578.30   111% 422391 78577s
    11    15 1200000.53    4  541 -4.388e+07 4835578.30   111% 273373 86400s

Cutting planes:
  Implied bound: 5
  Clique: 44
  MIR: 54
  GUB cover: 1
  Zero half: 7

Explored 14 nodes (3276450 simplex iterations) in 86400.60 seconds
Thread count was 4 (of 16 available processors)

Solution count 3: -4.38772e+07 -6.10261e+07 -7.02176e+07 

Time limit reached
Best objective -4.387721051438e+07, best bound 4.835578300529e+06, gap 111.0207%

***
***  |- Calculation finished, timelimit reached 86,400.71 (86400.71) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -43,877,210.5144 (-43877210.5144) and is feasible with a Gap of 1.1102
###
### Service 7 (ME3) is operated by 4 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 328, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 17 has the path [145, 335, 336, 338, 339, 329, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718.0) - 338 (EGPSD In@867.0/Out@886
###  |  .0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 278, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 278 (ESALG In@832.0/Out@851
###  |  .0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 363, 260, 190, 170, 49, 75, 72, 53, 61, 79, 349, 408, -4]
###  |  218 (NLRTM Out@92.0) - 363 (TRMER In@243.0/Out@287.0) - 260 (SGSIN In@618.0/Out@651.0) - 190 (KRPUS In@788.0/Out@810.
###  |  0) - 170 (JPHKT In@824.0/Out@833.0) - 49 (CNDLC In@871.0/Out@886.0) - 75 (CNXGG In@904.0/Out@923.0) - 72 (CNTAO In@96
###  |  0.0/Out@978.0) - 53 (CNNGB In@1031.0/Out@1040.0) - 61 (CNSHA In@1069.0/Out@1079.0) - 79 (CNYTN In@1160.0/Out@1176.0) 
###  |  - 349 (MYTPP In@1250.0/Out@1270.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 194,  729.0 containers from  53 to 349
###  |   |- and carried Demand 195,   11.0 containers from  53 to 349
###  |   |- and carried Demand 196,    7.0 containers from  61 to 349
###  |- Vessel  6 has the path [219, 135, 201, 371, 424, 404, -4]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 371 (AEJEA In@563.0/Out@587
###  |  .0) - 424 (PKBQM In@644.0/Out@674.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand 371,  108.0 containers from 424 to 404
###  |   |- and carried Demand 376,    1.0 containers from 424 to 404
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel  7 has the path [221, 137, 203, 373, 426, 406, -4]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 373 (AEJEA In@899.0/Out@923
###  |  .0) - 426 (PKBQM In@980.0/Out@1010.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel  8 has the path [223, 138, 204, 374, 427, 407, -4]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 374 (AEJEA In@1067.0/Out@10
###  |  91.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel 15 has the path [134, 200, 370, 423, 403, 422, 229, 353, 354, 376, 429, 409, -4]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 370 (AEJEA In@395.0/Out@419.0) - 423 (PKBQM In@476.0/Out@506
###  |  .0) - 403 (INNSA In@570.0/Out@586.0) - 422 (OMSLL In@642.0/Out@661.0) - 229 (OMSLL In@721.0/Out@736.0) - 353 (OMSLL I
###  |  n@878.0/Out@890.0) - 354 (OMSLL In@1300.0/Out@1313.0) - 376 (AEJEA In@1403.0/Out@1427.0) - 429 (PKBQM In@1484.0/Out@1
###  |  514.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 277,   81.0 containers from 423 to 422
###  |   |- and carried Demand 278,    2.0 containers from 423 to 422
###  |   |- and carried Demand 281,    1.0 containers from 423 to 403
###  |   |- and carried Demand 282,  359.0 containers from 370 to 354
###  |   |- and carried Demand 284,   63.0 containers from 370 to 423
###  |   |- and carried Demand 285,  174.0 containers from 370 to 423
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |- Vessel 16 has the path [136, 268, 372, 425, 405, -4]
###  |  136 (FRLEH Out@326.0) - 268 (TRAMB In@576.0/Out@603.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 361,   44.0 containers from 425 to 405
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 111, 284, 45, 302, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 111 (ESALG In@253.0/Out@265.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@765.0/Out@811.0) - 302 (HNPCR In@11
###  |  66.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 111
###  |   |- and carried Demand 117,  580.0 containers from 199 to 111
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 111
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 358, 273, 235, 0]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 14 has the path [105, 266, 234, 0]
###  |  105 (DKAAR Out@388.0) - 266 (TRALI In@668.0/Out@686.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [4]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
rho[81,19] = 1
rho[177,18] = 1
rho[24,18] = 1
rho[121,30] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[81,19,1] = 1
eta[81,19,2] = 1
eta[81,19,3] = 1
eta[81,19,4] = 1
eta[81,19,5] = 1
eta[81,19,6] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,235,0] = 1
y[3,273,235] = 1
y[5,190,170] = 1
y[5,218,363] = 1
y[5,349,408] = 1
y[5,49,75] = 1
y[5,79,349] = 1
y[5,53,61] = 1
y[5,260,190] = 1
y[5,363,260] = 1
y[5,61,79] = 1
y[5,75,72] = 1
y[5,408,-4] = 1
y[5,72,53] = 1
y[5,170,49] = 1
y[6,424,404] = 1
y[6,404,-4] = 1
y[6,135,201] = 1
y[6,201,371] = 1
y[6,371,424] = 1
y[6,219,135] = 1
y[7,137,203] = 1
y[7,426,406] = 1
y[7,373,426] = 1
y[7,221,137] = 1
y[7,406,-4] = 1
y[7,203,373] = 1
y[8,204,374] = 1
y[8,407,-4] = 1
y[8,223,138] = 1
y[8,427,407] = 1
y[8,374,427] = 1
y[8,138,204] = 1
y[9,274,199] = 1
y[9,1,274] = 1
y[9,45,302] = 1
y[9,199,290] = 1
y[9,302,-2] = 1
y[9,290,111] = 1
y[9,111,284] = 1
y[9,284,45] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,328,-3] = 1
y[12,8,366] = 1
y[12,366,328] = 1
y[13,202,278] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,278,-1] = 1
y[14,105,266] = 1
y[14,234,0] = 1
y[14,266,234] = 1
y[15,403,422] = 1
y[15,354,376] = 1
y[15,200,370] = 1
y[15,229,353] = 1
y[15,370,423] = 1
y[15,423,403] = 1
y[15,422,229] = 1
y[15,376,429] = 1
y[15,409,-4] = 1
y[15,429,409] = 1
y[15,134,200] = 1
y[15,353,354] = 1
y[16,372,425] = 1
y[16,268,372] = 1
y[16,425,405] = 1
y[16,405,-4] = 1
y[16,136,268] = 1
y[17,336,338] = 1
y[17,329,-3] = 1
y[17,339,329] = 1
y[17,145,335] = 1
y[17,335,336] = 1
y[17,338,339] = 1
y[18,301,-2] = 1
y[18,148,315] = 1
y[18,315,311] = 1
y[18,311,301] = 1
xD[7,199,290] = 161
xD[7,290,111] = 161
xD[7,111,284] = 161
xD[7,284,45] = 161
xD[8,199,290] = 580
xD[8,290,111] = 580
xD[14,219,135] = 5
xD[26,221,137] = 6
xD[117,199,290] = 580
xD[117,290,111] = 580
xD[132,202,278] = 214
xD[133,202,278] = 10
xD[138,202,278] = 214
xD[138,291,202] = 214
xD[139,202,278] = 10
xD[139,291,202] = 10
xD[144,290,111] = 161
xD[144,111,284] = 161
xD[144,284,45] = 161
xD[145,290,111] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[194,79,349] = 729
xD[194,53,61] = 729
xD[194,61,79] = 729
xD[195,79,349] = 11
xD[195,53,61] = 11
xD[195,61,79] = 11
xD[196,79,349] = 7
xD[196,61,79] = 7
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
xD[277,403,422] = 81
xD[277,423,403] = 81
xD[278,403,422] = 2
xD[278,423,403] = 2
xD[281,423,403] = 1
xD[282,403,422] = 359
xD[282,229,353] = 359
xD[282,370,423] = 359
xD[282,423,403] = 359
xD[282,422,229] = 359
xD[282,353,354] = 359
xD[284,370,423] = 63
xD[285,370,423] = 174
xD[289,429,409] = 34
xD[290,376,429] = 84
xD[291,376,429] = 50
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[361,425,405] = 44
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[371,424,404] = 108
xD[376,424,404] = 1
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[45] = 765
zE[49] = 871
zE[53] = 1031
zE[61] = 1069
zE[72] = 960
zE[75] = 904
zE[79] = 1160
zE[111] = 253
zE[135] = 279
zE[137] = 447
zE[138] = 615
zE[170] = 824
zE[190] = 788
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[229] = 721
zE[234] = 1304
zE[235] = 1472
zE[240] = 4085
zE[260] = 618
zE[266] = 668
zE[268] = 576
zE[273] = 785
zE[274] = 160
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[335] = 621
zE[336] = 699
zE[338] = 867
zE[339] = 957
zE[349] = 1250
zE[353] = 878
zE[354] = 1300
zE[358] = 683
zE[363] = 243
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[370] = 395
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[376] = 1403
zE[403] = 570
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[422] = 642
zE[423] = 476
zE[424] = 644
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[429] = 1484
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[45] = 811
zX[49] = 886
zX[53] = 1040
zX[61] = 1079
zX[72] = 978
zX[75] = 923
zX[79] = 1176
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[137] = 463
zX[138] = 631
zX[145] = 302
zX[148] = 470
zX[170] = 833
zX[190] = 810
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[234] = 1316
zX[235] = 1484
zX[240] = 4085
zX[260] = 651
zX[266] = 686
zX[268] = 603
zX[273] = 799
zX[274] = 179
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[335] = 637
zX[336] = 718
zX[338] = 886
zX[339] = 973
zX[349] = 1270
zX[353] = 890
zX[354] = 1313
zX[358] = 731
zX[363] = 287
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[370] = 419
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[376] = 1427
zX[403] = 586
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[422] = 661
zX[423] = 506
zX[424] = 674
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[429] = 1514
phi[1] = 227
phi[2] = 227
phi[3] = 860
phi[5] = 1334
phi[6] = 494
phi[7] = 662
phi[8] = 662
phi[9] = 1113
phi[10] = 489
phi[11] = 489
phi[12] = 489
phi[13] = 631
phi[14] = 928
phi[15] = 1436
phi[16] = 596
phi[17] = 925
phi[18] = 541

### All variables with value smaller zero:

### End

real	1449m27.796s
user	4867m50.939s
sys	901m53.670s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
