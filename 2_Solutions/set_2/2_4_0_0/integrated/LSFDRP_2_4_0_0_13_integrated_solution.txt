	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_13.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_13 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 8252 rows and 1127 columns (presolve time = 6s) ...
Presolve removed 59838 rows and 3738761 columns (presolve time = 11s) ...
Presolve removed 102467 rows and 5244575 columns (presolve time = 16s) ...
Presolve removed 140170 rows and 6426095 columns (presolve time = 20s) ...
Presolve removed 166986 rows and 6806027 columns (presolve time = 27s) ...
Presolve removed 168048 rows and 6806306 columns (presolve time = 30s) ...
Presolve removed 169397 rows and 6807829 columns (presolve time = 35s) ...
Presolve removed 169457 rows and 6808029 columns (presolve time = 41s) ...
Presolve removed 169460 rows and 6808139 columns (presolve time = 45s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 58s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 109s) ...
Presolve removed 169716 rows and 6808346 columns (presolve time = 110s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 116s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 998s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 997.58s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309380 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.286e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.835e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43471105e+15  4.20604602e+15  1.49e+08 1.33e+06  5.80e+11  1113s
   1  -2.43578497e+15  4.11615071e+15  8.21e+07 6.99e+06  3.27e+11  1172s
   2  -1.60473453e+15  3.96841784e+15  5.42e+07 4.45e+06  2.19e+11  1231s
   3  -1.20248630e+15  3.72919491e+15  4.07e+07 2.60e+06  1.62e+11  1290s
   4  -6.38095840e+14  3.37733279e+15  2.17e+07 1.27e+06  8.74e+10  1349s
   5  -2.98514116e+14  2.85235275e+15  1.02e+07 5.51e+05  4.17e+10  1406s
   6  -1.79549519e+14  1.96422239e+15  6.11e+06 1.65e+05  2.54e+10  1464s
   7  -7.37210617e+13  1.22132317e+15  2.51e+06 2.73e+04  1.05e+10  1521s
   8  -1.78574464e+13  7.38291858e+14  6.08e+05 1.87e+03  2.73e+09  1578s
   9  -7.40824918e+12  3.62700922e+14  2.52e+05 5.87e-07  1.12e+09  1635s

Barrier performed 9 iterations in 1634.82 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 17.42s

Solved with dual simplex

Root relaxation: objective 1.338484e+07, 149250 iterations, 627.76 seconds
Total elapsed time = 1636.14s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.3160e+07    0  645          - 1.3160e+07      -     - 1701s
     0     0 1.1670e+07    0  757          - 1.1670e+07      -     - 2045s
     0     0 9721248.03    0  721          - 9721248.03      -     - 2220s
     0     0 9720996.60    0  716          - 9720996.60      -     - 2224s
     0     0 9715202.33    0  697          - 9715202.33      -     - 2250s
     0     0 9714812.52    0  706          - 9714812.52      -     - 2253s
     0     0 9714811.73    0  705          - 9714811.73      -     - 2255s
     0     0 9460304.76    0  769          - 9460304.76      -     - 2544s
     0     0 9416783.48    0  765          - 9416783.48      -     - 2881s
     0     0 9416678.04    0  759          - 9416678.04      -     - 2889s
     0     0 9271815.98    0  704          - 9271815.98      -     - 3036s
     0     0 9251944.69    0  774          - 9251944.69      -     - 3108s
     0     0 9251129.42    0  753          - 9251129.42      -     - 3124s
     0     0 9247607.74    0  774          - 9247607.74      -     - 3134s
     0     0 9245903.84    0  745          - 9245903.84      -     - 3147s
     0     0 9245879.00    0  745          - 9245879.00      -     - 3149s
     0     0 9228702.91    0  756          - 9228702.91      -     - 3175s
     0     0 9227180.28    0  756          - 9227180.28      -     - 3186s
     0     0 9225678.92    0  784          - 9225678.92      -     - 3190s
     0     0 9225495.86    0  786          - 9225495.86      -     - 3195s
     0     0 9224933.15    0  781          - 9224933.15      -     - 3203s
     0     0 9222850.30    0  781          - 9222850.30      -     - 3214s
     0     0 9222807.17    0  780          - 9222807.17      -     - 3216s
     0     0 9217466.99    0  792          - 9217466.99      -     - 3229s
     0     0 9217133.53    0  825          - 9217133.53      -     - 3239s
     0     0 9217024.73    0  801          - 9217024.73      -     - 3249s
     0     0 9216900.45    0  785          - 9216900.45      -     - 3258s
H    0     0                    -8.05255e+07 9216900.45   111%     - 3265s
     0     0 9216900.45    0  781 -8.053e+07 9216900.45   111%     - 3742s
H    0     0                    -4.35871e+07 9216900.45   121%     - 4546s
     0     2 9216900.45    0  781 -4.359e+07 9216900.45   121%     - 4561s
     1     4 9114837.47    1  740 -4.359e+07 9153802.14   121% 785359 29926s
     3     8 8460347.32    2  613 -4.359e+07 9113698.96   121% 373915 44137s
     7    12 7932249.21    3  590 -4.359e+07 8177732.18   119% 266894 48714s
    11    14 7292780.89    4  527 -4.359e+07 8177732.18   119% 268819 69104s
    15    18 2791111.53    4  480 -4.359e+07 8177732.18   119% 204784 83875s
    19    18 7292764.95    5  523 -4.359e+07 8177732.18   119% 194876 86400s

Cutting planes:
  Implied bound: 10
  Clique: 40
  MIR: 38
  Flow cover: 5
  GUB cover: 1
  Zero half: 11

Explored 22 nodes (4006914 simplex iterations) in 86400.50 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -4.35871e+07 -8.05255e+07 

Time limit reached
Best objective -4.358706667373e+07, best bound 8.177732179374e+06, gap 118.7618%

***
***  |- Calculation finished, timelimit reached 86,400.61 (86400.61) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -43,587,066.6737 (-43587066.6737) and is feasible with a Gap of 1.1876
###
### Service 7 (ME3) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  2 has the path [101, 327, -3]
###  |  101 (DEHAM Out@456.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel  3 has the path [102, 328, -3]
###  |  102 (DEHAM Out@624.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 13 has the path [104, 325, -3]
###  |  104 (DKAAR Out@220.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 277, -1]
###  |  1 (BEANR Out@66.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 17 has the path [145, 276, -1]
###  |  145 (GBFXT Out@302.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 407, -4]
###  |  218 (NLRTM Out@92.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel  6 has the path [219, 409, -4]
###  |  219 (NLRTM Out@260.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel  7 has the path [221, 404, -4]
###  |  221 (NLRTM Out@428.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel  8 has the path [223, 405, -4]
###  |  223 (NLRTM Out@596.0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel 15 has the path [134, 406, -4]
###  |  134 (FRLEH Out@158.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel 16 has the path [136, 408, -4]
###  |  136 (FRLEH Out@326.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |
### Service 121 (SAE) is operated by 1 vessels from type 18 (PMax25)
###  |- Vessel 14 has the path [105, 301, -2]
###  |  105 (DKAAR Out@388.0) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 234, 0]
###  |  2 (BEANR Out@234.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 18 has the path [148, 235, 0]
###  |  148 (GBFXT Out@470.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [1, 4, 11, 12]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,18] = 1
rho[81,19] = 1
rho[177,30] = 1
rho[24,30] = 1
rho[121,18] = 1
eta[7,18,1] = 1
eta[7,18,2] = 1
eta[7,18,3] = 1
eta[81,19,1] = 1
eta[81,19,2] = 1
eta[81,19,3] = 1
eta[81,19,4] = 1
eta[81,19,5] = 1
eta[81,19,6] = 1
eta[177,30,1] = 1
eta[177,30,2] = 1
eta[24,30,1] = 1
eta[24,30,2] = 1
eta[121,18,1] = 1
y[2,327,-3] = 1
y[2,101,327] = 1
y[3,328,-3] = 1
y[3,102,328] = 1
y[5,218,407] = 1
y[5,407,-4] = 1
y[6,219,409] = 1
y[6,409,-4] = 1
y[7,404,-4] = 1
y[7,221,404] = 1
y[8,405,-4] = 1
y[8,223,405] = 1
y[9,277,-1] = 1
y[9,1,277] = 1
y[10,234,0] = 1
y[10,2,234] = 1
y[13,104,325] = 1
y[13,325,-3] = 1
y[14,301,-2] = 1
y[14,105,301] = 1
y[15,134,406] = 1
y[15,406,-4] = 1
y[16,136,408] = 1
y[16,408,-4] = 1
y[17,276,-1] = 1
y[17,145,276] = 1
y[18,235,0] = 1
y[18,148,235] = 1
zE[234] = 1304
zE[235] = 1472
zE[276] = 496
zE[277] = 664
zE[301] = 998
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zX[1] = 66
zX[2] = 234
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[134] = 158
zX[136] = 326
zX[145] = 302
zX[148] = 470
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[234] = 1316
zX[235] = 1484
zX[240] = 4085
zX[276] = 515
zX[277] = 683
zX[301] = 1011
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
phi[2] = 435
phi[3] = 435
phi[5] = 1166
phi[6] = 1334
phi[7] = 326
phi[8] = 326
phi[9] = 617
phi[10] = 1082
phi[13] = 503
phi[14] = 623
phi[15] = 932
phi[16] = 1100
phi[17] = 213
phi[18] = 1014

### All variables with value smaller zero:

### End

real	1449m23.772s
user	4920m3.399s
sys	847m21.520s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
