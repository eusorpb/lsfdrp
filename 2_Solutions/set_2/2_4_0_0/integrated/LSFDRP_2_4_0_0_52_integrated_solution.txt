	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_52.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_52 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 6e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 8252 rows and 1127 columns (presolve time = 6s) ...
Presolve removed 59838 rows and 3738761 columns (presolve time = 11s) ...
Presolve removed 95584 rows and 5025231 columns (presolve time = 16s) ...
Presolve removed 133224 rows and 6224370 columns (presolve time = 20s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 25s) ...
Presolve removed 167259 rows and 6806027 columns (presolve time = 30s) ...
Presolve removed 168598 rows and 6806919 columns (presolve time = 35s) ...
Presolve removed 169401 rows and 6807829 columns (presolve time = 42s) ...
Presolve removed 169457 rows and 6808029 columns (presolve time = 45s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 50s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 59s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 111s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 115s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 1083s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 1082.73s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43281498e+15  1.68237888e+16  1.49e+08 3.84e+06  2.32e+12  1188s
   1  -2.43465191e+15  1.64636305e+16  8.21e+07 2.80e+07  1.31e+12  1245s
   2  -1.60450591e+15  1.58718815e+16  5.41e+07 1.79e+07  8.75e+11  1301s
   3  -1.20269441e+15  1.49198207e+16  4.06e+07 1.05e+07  6.49e+11  1356s
   4  -6.14700015e+14  1.36089236e+16  2.08e+07 5.53e+06  3.39e+11  1413s
   5  -2.95029944e+14  1.14050750e+16  1.00e+07 2.37e+06  1.65e+11  1471s
   6  -1.54982756e+14  8.48246971e+15  5.27e+06 1.04e+06  9.12e+10  1528s
   7  -2.29019865e+13  4.52879998e+15  7.78e+05 4.65e+04  1.55e+10  1589s

Barrier performed 7 iterations in 1589.29 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 342.04s (can be avoided by choosing Method=3)

Solved with dual simplex

Root relaxation: objective 1.317767e+07, 142377 iterations, 839.04 seconds
Total elapsed time = 1941.70s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2898e+07    0  550          - 1.2898e+07      -     - 2039s
     0     0 9270388.50    0  676          - 9270388.50      -     - 3053s
H    0     0                    -9.66921e+07 9270388.50   110%     - 3056s
     0     0 8829755.77    0  665 -9.669e+07 8829755.77   109%     - 3728s
     0     0 8139855.33    0  666 -9.669e+07 8139855.33   108%     - 4815s
     0     0 8120740.59    0  676 -9.669e+07 8120740.59   108%     - 5216s
     0     0 8120740.59    0  676 -9.669e+07 8120740.59   108%     - 5218s
     0     0 7529805.19    0  790 -9.669e+07 7529805.19   108%     - 6009s
     0     0 5806690.63    0  705 -9.669e+07 5806690.63   106%     - 8007s
     0     0 5806024.55    0  754 -9.669e+07 5806024.55   106%     - 8120s
     0     0 5806024.55    0  754 -9.669e+07 5806024.55   106%     - 8122s
     0     0 5734104.59    0  765 -9.669e+07 5734104.59   106%     - 8606s
H    0     0                    -7.56403e+07 5734104.59   108%     - 8608s
     0     0 5718536.24    0  734 -7.564e+07 5718536.24   108%     - 8706s
     0     0 5718385.82    0  744 -7.564e+07 5718385.82   108%     - 8715s
     0     0 5700434.38    0  791 -7.564e+07 5700434.38   108%     - 8900s
     0     0 5697624.90    0  787 -7.564e+07 5697624.90   108%     - 8956s
     0     0 5697377.70    0  788 -7.564e+07 5697377.70   108%     - 8966s
     0     0 5669270.42    0  788 -7.564e+07 5669270.42   107%     - 9145s
     0     0 5669270.42    0  788 -7.564e+07 5669270.42   107%     - 9151s
     0     0 5664910.82    0  758 -7.564e+07 5664910.82   107%     - 9177s
H    0     0                    -7.56136e+07 5664910.82   107%     - 9183s
     0     0 5664910.82    0  762 -7.561e+07 5664910.82   107%     - 9184s
     0     0 5664002.09    0  794 -7.561e+07 5664002.09   107%     - 9257s
     0     0 5663987.68    0  785 -7.561e+07 5663987.68   107%     - 9263s
     0     0 5663519.53    0  798 -7.561e+07 5663519.53   107%     - 9268s
     0     0 5662200.63    0  798 -7.561e+07 5662200.63   107%     - 9275s
     0     0 5660275.93    0  803 -7.561e+07 5660275.93   107%     - 9280s
     0     0 5659737.07    0  805 -7.561e+07 5659737.07   107%     - 9281s
     0     0 5658664.73    0  802 -7.561e+07 5658664.73   107%     - 9291s
     0     0 5658664.73    0  795 -7.561e+07 5658664.73   107%     - 9698s
H    0     0                    -7.38346e+07 5658664.73   108%     - 9709s
H    0     0                    -5.96253e+07 5658664.73   109%     - 10613s
H    0     0                    -5.93868e+07 5658664.73   110%     - 11151s
     0     2 5658664.73    0  790 -5.939e+07 5658664.73   110%     - 11190s
     1     4 2104804.97    1  685 -5.939e+07 5658155.79   110% 2186780 62979s
     3     8 1737728.97    2  639 -5.939e+07 3078232.01   105% 780898 72790s
     7     9 -215830.04    3  702 -5.939e+07 1261345.71   102% 401946 86400s

Cutting planes:
  Implied bound: 7
  Clique: 72
  MIR: 52
  Flow cover: 1
  GUB cover: 1
  Zero half: 9

Explored 10 nodes (3362428 simplex iterations) in 86400.63 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -5.93868e+07 -5.96253e+07 -7.38346e+07 ... -9.66921e+07

Time limit reached
Best objective -5.938680871371e+07, best bound 1.261345710114e+06, gap 102.1239%

***
***  |- Calculation finished, timelimit reached 86,400.75 (86400.75) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -59,386,808.7137 (-59386808.7137) and is feasible with a Gap of 1.0212
###
### Service 7 (ME3) is operated by 5 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 328, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 17 has the path [145, 335, 336, 338, 339, 329, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718.0) - 338 (EGPSD In@867.0/Out@886
###  |  .0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel 18 has the path [148, 167, 168, 340, 330, -3]
###  |  148 (GBFXT Out@470.0) - 167 (ITGIT In@736.0/Out@755.0) - 168 (ITGIT In@904.0/Out@923.0) - 340 (EGPSD In@1035.0/Out@10
###  |  54.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 199,  188.0 containers from 340 to 330
###  |   |- and carried Demand 200,    7.0 containers from 340 to 330
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 14 has the path [105, 272, 278, -1]
###  |  105 (DKAAR Out@388.0) - 272 (TRZMK In@617.0/Out@631.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 
###  |  24)
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 133, 199, 370, 423, 403, 422, 405, -4]
###  |  218 (NLRTM Out@92.0) - 133 (FRLEH In@111.0/Out@127.0) - 199 (MAPTM In@207.0/Out@228.0) - 370 (AEJEA In@395.0/Out@419.
###  |  0) - 423 (PKBQM In@476.0/Out@506.0) - 403 (INNSA In@570.0/Out@586.0) - 422 (OMSLL In@642.0/Out@661.0) - 405 (INNSA In
###  |  @906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand 277,   81.0 containers from 423 to 422
###  |   |- and carried Demand 278,    2.0 containers from 423 to 422
###  |   |- and carried Demand 281,    1.0 containers from 423 to 403
###  |   |- and carried Demand 284,   63.0 containers from 370 to 423
###  |   |- and carried Demand 285,  174.0 containers from 370 to 423
###  |- Vessel  6 has the path [219, 135, 201, 371, 424, 231, 375, 428, 408, -4]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 371 (AEJEA In@563.0/Out@587
###  |  .0) - 424 (PKBQM In@644.0/Out@674.0) - 231 (OMSLL In@1132.0/Out@1145.0) - 375 (AEJEA In@1235.0/Out@1259.0) - 428 (PKB
###  |  QM In@1316.0/Out@1346.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 377,  704.0 containers from 371 to 408
###  |   |- and carried Demand 378,    8.0 containers from 371 to 408
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel  7 has the path [221, 266, 373, 426, 406, -4]
###  |  221 (NLRTM Out@428.0) - 266 (TRALI In@668.0/Out@686.0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@101
###  |  0.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel  8 has the path [223, 138, 204, 374, 427, 354, 376, 429, 409, -4]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 374 (AEJEA In@1067.0/Out@10
###  |  91.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 354 (OMSLL In@1300.0/Out@1313.0) - 376 (AEJEA In@1403.0/Out@1427.0) - 429 
###  |  (PKBQM In@1484.0/Out@1514.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 289,   34.0 containers from 429 to 409
###  |   |- and carried Demand 290,   84.0 containers from 376 to 429
###  |   |- and carried Demand 291,   50.0 containers from 376 to 429
###  |   |- and carried Demand 325,   18.0 containers from 427 to 354
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel 15 has the path [134, 200, 421, 404, -4]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 421 (OMSLL In@474.0/Out@493.0) - 404 (INNSA In@738.0/Out@754
###  |  .0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel 16 has the path [136, 268, 372, 230, 407, -4]
###  |  136 (FRLEH Out@326.0) - 268 (TRAMB In@576.0/Out@603.0) - 372 (AEJEA In@731.0/Out@755.0) - 230 (OMSLL In@889.0/Out@904
###  |  .0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |
### Service 121 (SAE) is operated by 1 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 290, 111, 275, 202, 114, 301, -2]
###  |  1 (BEANR Out@66.0) - 290 (MAPTM In@211.0/Out@230.0) - 111 (ESALG In@253.0/Out@265.0) - 275 (ESALG In@328.0/Out@347.0)
###  |   - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END 
###  |  Ziel-Service 121)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 145,  580.0 containers from 290 to 111
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 358, 273, 235, 0]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 13 has the path [104, 291, 203, 234, 0]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 203 (MAPTM In@543.0/Out@564.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [4]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
rho[81,19] = 1
rho[177,18] = 1
rho[24,18] = 1
rho[121,30] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[81,19,1] = 1
eta[81,19,2] = 1
eta[81,19,3] = 1
eta[81,19,4] = 1
eta[81,19,5] = 1
eta[81,19,6] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[121,30,1] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,235,0] = 1
y[3,273,235] = 1
y[5,403,422] = 1
y[5,133,199] = 1
y[5,405,-4] = 1
y[5,422,405] = 1
y[5,370,423] = 1
y[5,423,403] = 1
y[5,199,370] = 1
y[5,218,133] = 1
y[6,375,428] = 1
y[6,231,375] = 1
y[6,424,231] = 1
y[6,428,408] = 1
y[6,135,201] = 1
y[6,408,-4] = 1
y[6,201,371] = 1
y[6,371,424] = 1
y[6,219,135] = 1
y[7,426,406] = 1
y[7,373,426] = 1
y[7,221,266] = 1
y[7,406,-4] = 1
y[7,266,373] = 1
y[8,354,376] = 1
y[8,204,374] = 1
y[8,223,138] = 1
y[8,376,429] = 1
y[8,409,-4] = 1
y[8,429,409] = 1
y[8,427,354] = 1
y[8,374,427] = 1
y[8,138,204] = 1
y[9,301,-2] = 1
y[9,114,301] = 1
y[9,275,202] = 1
y[9,1,290] = 1
y[9,290,111] = 1
y[9,202,114] = 1
y[9,111,275] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,328,-3] = 1
y[12,8,366] = 1
y[12,366,328] = 1
y[13,234,0] = 1
y[13,203,234] = 1
y[13,291,203] = 1
y[13,104,291] = 1
y[14,105,272] = 1
y[14,272,278] = 1
y[14,278,-1] = 1
y[15,200,421] = 1
y[15,404,-4] = 1
y[15,421,404] = 1
y[15,134,200] = 1
y[16,268,372] = 1
y[16,407,-4] = 1
y[16,136,268] = 1
y[16,230,407] = 1
y[16,372,230] = 1
y[17,336,338] = 1
y[17,329,-3] = 1
y[17,339,329] = 1
y[17,145,335] = 1
y[17,335,336] = 1
y[17,338,339] = 1
y[18,168,340] = 1
y[18,340,330] = 1
y[18,148,167] = 1
y[18,167,168] = 1
y[18,330,-3] = 1
xD[6,218,133] = 17
xD[14,219,135] = 5
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[145,290,111] = 580
xD[199,340,330] = 188
xD[200,340,330] = 7
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
xD[277,403,422] = 81
xD[277,423,403] = 81
xD[278,403,422] = 2
xD[278,423,403] = 2
xD[281,423,403] = 1
xD[284,370,423] = 63
xD[285,370,423] = 174
xD[289,429,409] = 34
xD[290,376,429] = 84
xD[291,376,429] = 50
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[325,427,354] = 18
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[377,375,428] = 704
xD[377,231,375] = 704
xD[377,424,231] = 704
xD[377,428,408] = 704
xD[377,371,424] = 704
xD[378,375,428] = 8
xD[378,231,375] = 8
xD[378,424,231] = 8
xD[378,428,408] = 8
xD[378,371,424] = 8
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[111] = 253
zE[114] = 452
zE[133] = 111
zE[135] = 279
zE[138] = 615
zE[167] = 736
zE[168] = 904
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[230] = 889
zE[231] = 1132
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[268] = 576
zE[272] = 617
zE[273] = 785
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[335] = 621
zE[336] = 699
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[354] = 1300
zE[358] = 683
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[370] = 395
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[376] = 1403
zE[403] = 570
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[421] = 474
zE[422] = 642
zE[423] = 476
zE[424] = 644
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zE[429] = 1484
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[114] = 464
zX[133] = 127
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[138] = 631
zX[145] = 302
zX[148] = 470
zX[167] = 755
zX[168] = 923
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[230] = 904
zX[231] = 1145
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[268] = 603
zX[272] = 631
zX[273] = 799
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[335] = 637
zX[336] = 718
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[354] = 1313
zX[358] = 731
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[370] = 419
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[376] = 1427
zX[403] = 586
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[421] = 493
zX[422] = 661
zX[423] = 506
zX[424] = 674
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
zX[429] = 1514
phi[1] = 227
phi[2] = 227
phi[3] = 860
phi[5] = 830
phi[6] = 1166
phi[7] = 662
phi[8] = 998
phi[9] = 945
phi[10] = 489
phi[11] = 489
phi[12] = 489
phi[13] = 1096
phi[14] = 463
phi[15] = 596
phi[16] = 932
phi[17] = 925
phi[18] = 925

### All variables with value smaller zero:

### End

real	1448m45.892s
user	4875m34.373s
sys	893m51.483s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
