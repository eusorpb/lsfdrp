	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_31.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_31 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 13437 rows and 6312 columns (presolve time = 6s) ...
Presolve removed 59838 rows and 3738761 columns (presolve time = 11s) ...
Presolve removed 102467 rows and 5244575 columns (presolve time = 15s) ...
Presolve removed 146026 rows and 6559956 columns (presolve time = 20s) ...
Presolve removed 161356 rows and 6800366 columns (presolve time = 25s) ...
Presolve removed 167259 rows and 6806027 columns (presolve time = 30s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 35s) ...
Presolve removed 169401 rows and 6807973 columns (presolve time = 40s) ...
Presolve removed 169460 rows and 6808139 columns (presolve time = 45s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 55s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 95s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 101s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 937s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 937.37s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43153809e+15  1.00295990e+16  1.49e+08 2.29e+06  1.38e+12  1044s
   1  -2.43385051e+15  9.81487036e+15  8.21e+07 1.67e+07  7.79e+11  1100s
   2  -1.60372027e+15  9.46209448e+15  5.41e+07 1.07e+07  5.22e+11  1156s
   3  -1.20197377e+15  8.89377597e+15  4.06e+07 6.24e+06  3.87e+11  1213s
   4  -6.11704019e+14  8.10796541e+15  2.08e+07 3.28e+06  2.01e+11  1271s
   5  -2.87891126e+14  6.84279285e+15  9.80e+06 1.41e+06  9.62e+10  1328s
   6  -1.88698879e+14  4.94737272e+15  6.42e+06 5.50e+05  6.39e+10  1383s
   7  -3.96895001e+13  3.09965395e+15  1.35e+06 1.76e+04  1.49e+10  1444s

Barrier performed 7 iterations in 1461.91 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.23s

Solved with dual simplex

Root relaxation: objective 1.318264e+07, 142579 iterations, 517.36 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2880e+07    0  672          - 1.2880e+07      -     - 1671s
     0     0 1.0683e+07    0  744          - 1.0683e+07      -     - 3851s
H    0     0                    -5.19122e+07 1.0683e+07   121%     - 3855s
     0     0 1.0412e+07    0  729 -5.191e+07 1.0412e+07   120%     - 4973s
     0     0 1.0412e+07    0  729 -5.191e+07 1.0412e+07   120%     - 4974s
     0     0 9865062.61    0  766 -5.191e+07 9865062.61   119%     - 6595s
H    0     0                    -4.97846e+07 9865062.61   120%     - 6597s
     0     0 9805819.31    0  801 -4.978e+07 9805819.31   120%     - 6968s
     0     0 9805499.12    0  803 -4.978e+07 9805499.12   120%     - 6986s
     0     0 9805499.12    0  803 -4.978e+07 9805499.12   120%     - 6988s
     0     0 9295247.39    0  795 -4.978e+07 9295247.39   119%     - 8600s
     0     0 7514940.63    0  653 -4.978e+07 7514940.63   115%     - 10190s
     0     0 7514816.73    0  665 -4.978e+07 7514816.73   115%     - 10207s
     0     0 7448358.02    0  722 -4.978e+07 7448358.02   115%     - 11013s
     0     0 7432127.63    0  735 -4.978e+07 7432127.63   115%     - 11375s
     0     0 7432127.63    0  735 -4.978e+07 7432127.63   115%     - 11377s
     0     0 7430589.16    0  744 -4.978e+07 7430589.16   115%     - 11425s
     0     0 7429340.64    0  754 -4.978e+07 7429340.64   115%     - 11484s
     0     0 7429340.64    0  754 -4.978e+07 7429340.64   115%     - 11485s
     0     0 7420018.00    0  738 -4.978e+07 7420018.00   115%     - 11514s
     0     0 7419942.70    0  737 -4.978e+07 7419942.70   115%     - 11520s
     0     0 7419709.77    0  746 -4.978e+07 7419709.77   115%     - 11542s
     0     0 7419629.01    0  741 -4.978e+07 7419629.01   115%     - 11558s
     0     0 7419452.63    0  753 -4.978e+07 7419452.63   115%     - 11569s
     0     0 7419311.11    0  763 -4.978e+07 7419311.11   115%     - 11591s
     0     0 7418586.43    0  739 -4.978e+07 7418586.43   115%     - 11611s
     0     0 7418249.87    0  745 -4.978e+07 7418249.87   115%     - 11620s
     0     0 7418249.87    0  747 -4.978e+07 7418249.87   115%     - 11624s
     0     0 7418249.87    0  743 -4.978e+07 7418249.87   115%     - 12401s
     0     2 7418249.87    0  741 -4.978e+07 7418249.87   115%     - 28532s
     1     4 7413750.06    1  747 -4.978e+07 7413750.06   115% 10859 30549s
     3     8 7408314.42    2  742 -4.978e+07 7408314.42   115% 26956 32810s
     7    12 7336332.74    3  682 -4.978e+07 7336332.74   115% 36907 33785s
    11    16 7336207.24    4  665 -4.978e+07 7336332.74   115% 31547 34797s
    15    18 4361946.43    4  502 -4.978e+07 7336332.74   115% 31872 86400s

Cutting planes:
  Implied bound: 4
  Clique: 36
  MIR: 66
  Flow cover: 1
  GUB cover: 1
  Zero half: 6

Explored 18 nodes (1200920 simplex iterations) in 86400.53 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -4.97846e+07 -5.19122e+07 

Time limit reached
Best objective -4.978455029115e+07, best bound 7.336332742459e+06, gap 114.7362%

***
***  |- Calculation finished, timelimit reached 86,400.64 (86400.64) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -49,784,550.2911 (-49784550.2911) and is feasible with a Gap of 1.1474
###
### Service 7 (ME3) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 133, 199, 290, 112, 275, 201, 371, 424, 230, 328, -3]
###  |  218 (NLRTM Out@92.0) - 133 (FRLEH In@111.0/Out@127.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.
###  |  0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@375.0/Out@396.0) - 371 (AEJEA In
###  |  @563.0/Out@587.0) - 424 (PKBQM In@644.0/Out@674.0) - 230 (OMSLL In@889.0/Out@904.0) - 328 (AEJEA In@1026.0/Out@1059.0
###  |  ) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel  6 has the path [219, 364, 325, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 15 has the path [134, 200, 370, 423, 403, 422, 229, 327, -3]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 370 (AEJEA In@395.0/Out@419.0) - 423 (PKBQM In@476.0/Out@506
###  |  .0) - 403 (INNSA In@570.0/Out@586.0) - 422 (OMSLL In@642.0/Out@661.0) - 229 (OMSLL In@721.0/Out@736.0) - 327 (AEJEA I
###  |  n@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 277,   81.0 containers from 423 to 422
###  |   |- and carried Demand 278,    2.0 containers from 423 to 422
###  |   |- and carried Demand 281,    1.0 containers from 423 to 403
###  |   |- and carried Demand 284,   63.0 containers from 370 to 423
###  |   |- and carried Demand 285,  174.0 containers from 370 to 423
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 11 has the path [5, 277, -1]
###  |  5 (BEANR Out@402.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 17 has the path [145, 276, -1]
###  |  145 (GBFXT Out@302.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 268, 404, -4]
###  |  100 (DEHAM Out@288.0) - 268 (TRAMB In@576.0/Out@603.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 
###  |  81)
###  |- Vessel  2 has the path [101, 273, 374, 427, 407, -4]
###  |  101 (DEHAM Out@456.0) - 273 (TRZMK In@785.0/Out@799.0) - 374 (AEJEA In@1067.0/Out@1091.0) - 427 (PKBQM In@1148.0/Out@
###  |  1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel  3 has the path [102, 366, 267, 375, 428, 408, -4]
###  |  102 (DEHAM Out@624.0) - 366 (TRMER In@747.0/Out@791.0) - 267 (TRALI In@836.0/Out@854.0) - 375 (AEJEA In@1235.0/Out@12
###  |  59.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |- Vessel  4 has the path [103, 367, 341, 342, 362, 409, -4]
###  |  103 (DEHAM Out@792.0) - 367 (TRMER In@915.0/Out@959.0) - 341 (EGPSD In@1125.0/Out@1141.0) - 342 (EGPSD In@1293.0/Out@
###  |  1309.0) - 362 (TRAMB In@1416.0/Out@1443.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel 13 has the path [104, 356, 334, 335, 266, 373, 426, 406, -4]
###  |  104 (DKAAR Out@220.0) - 356 (TRAMB In@347.0/Out@395.0) - 334 (EGPSD In@531.0/Out@550.0) - 335 (EGPSD In@621.0/Out@637
###  |  .0) - 266 (TRALI In@668.0/Out@686.0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@1010.0) - 406 (INNSA 
###  |  In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel 14 has the path [105, 272, 372, 425, 405, -4]
###  |  105 (DKAAR Out@388.0) - 272 (TRZMK In@617.0/Out@631.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 361,   44.0 containers from 425 to 405
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  7 has the path [221, 137, 203, 234, 0]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 16 has the path [136, 202, 285, 216, 22, 47, 37, 42, 240, 236, 0]
###  |  136 (FRLEH Out@326.0) - 202 (MAPTM In@406.0/Out@427.0) - 285 (GWOXB In@631.0/Out@710.0) - 216 (NGAPP In@793.0/Out@840
###  |  .0) - 22 (BJCOO In@849.0/Out@886.0) - 47 (CMDLA In@933.0/Out@979.0) - 37 (CLLQN In@1408.0/Out@1426.0) - 42 (CLSAI In@
###  |  1456.0/Out@1472.0) - 240 (PECLL In@1582.12/Out@1584.12) - 236 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Servic
###  |  e 177)
###  |   |- and carried Demand  50,  214.0 containers from 202 to  47
###  |   |- and carried Demand  51,   10.0 containers from 202 to  47
###  |
### Vessels not used in the solution: [9, 10]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,19] = 1
rho[81,18] = 1
rho[177,19] = 1
rho[24,30] = 1
rho[121,30] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
eta[24,30,1] = 1
eta[24,30,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,404,-4] = 1
y[1,100,268] = 1
y[1,268,404] = 1
y[2,101,273] = 1
y[2,407,-4] = 1
y[2,273,374] = 1
y[2,427,407] = 1
y[2,374,427] = 1
y[3,375,428] = 1
y[3,366,267] = 1
y[3,428,408] = 1
y[3,267,375] = 1
y[3,408,-4] = 1
y[3,102,366] = 1
y[4,103,367] = 1
y[4,362,409] = 1
y[4,342,362] = 1
y[4,341,342] = 1
y[4,367,341] = 1
y[4,409,-4] = 1
y[5,112,275] = 1
y[5,275,201] = 1
y[5,133,199] = 1
y[5,199,290] = 1
y[5,424,230] = 1
y[5,218,133] = 1
y[5,328,-3] = 1
y[5,230,328] = 1
y[5,290,112] = 1
y[5,201,371] = 1
y[5,371,424] = 1
y[6,364,325] = 1
y[6,219,364] = 1
y[6,325,-3] = 1
y[7,234,0] = 1
y[7,137,203] = 1
y[7,203,234] = 1
y[7,221,137] = 1
y[8,235,0] = 1
y[8,204,235] = 1
y[8,223,138] = 1
y[8,138,204] = 1
y[11,277,-1] = 1
y[11,5,277] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,356,334] = 1
y[13,426,406] = 1
y[13,373,426] = 1
y[13,406,-4] = 1
y[13,104,356] = 1
y[13,266,373] = 1
y[13,334,335] = 1
y[13,335,266] = 1
y[14,372,425] = 1
y[14,425,405] = 1
y[14,405,-4] = 1
y[14,105,272] = 1
y[14,272,372] = 1
y[15,229,327] = 1
y[15,403,422] = 1
y[15,327,-3] = 1
y[15,200,370] = 1
y[15,370,423] = 1
y[15,423,403] = 1
y[15,422,229] = 1
y[15,134,200] = 1
y[16,37,42] = 1
y[16,216,22] = 1
y[16,136,202] = 1
y[16,240,236] = 1
y[16,42,240] = 1
y[16,285,216] = 1
y[16,47,37] = 1
y[16,202,285] = 1
y[16,22,47] = 1
y[16,236,0] = 1
y[17,276,-1] = 1
y[17,145,276] = 1
y[18,301,-2] = 1
y[18,148,315] = 1
y[18,315,311] = 1
y[18,311,301] = 1
xD[6,218,133] = 17
xD[8,199,290] = 580
xD[8,290,112] = 580
xD[26,221,137] = 6
xD[50,216,22] = 214
xD[50,285,216] = 214
xD[50,202,285] = 214
xD[50,22,47] = 214
xD[51,216,22] = 10
xD[51,285,216] = 10
xD[51,202,285] = 10
xD[51,22,47] = 10
xD[145,290,112] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[277,403,422] = 81
xD[277,423,403] = 81
xD[278,403,422] = 2
xD[278,423,403] = 2
xD[281,423,403] = 1
xD[284,370,423] = 63
xD[285,370,423] = 174
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[361,425,405] = 44
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[22] = 849
zE[37] = 1408
zE[42] = 1456
zE[47] = 933
zE[112] = 284
zE[133] = 111
zE[137] = 447
zE[138] = 615
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[216] = 793
zE[229] = 721
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 1582.12
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[272] = 617
zE[273] = 785
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[285] = 631
zE[290] = 211
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[334] = 531
zE[335] = 621
zE[341] = 1125
zE[342] = 1293
zE[356] = 347
zE[362] = 1416
zE[364] = 411
zE[366] = 747
zE[367] = 915
zE[370] = 395
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[403] = 570
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[422] = 642
zE[423] = 476
zE[424] = 644
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zX[5] = 402
zX[8] = 570
zX[22] = 886
zX[37] = 1426
zX[42] = 1472
zX[47] = 979
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[133] = 127
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[138] = 631
zX[145] = 302
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[216] = 840
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 1584.12
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[272] = 631
zX[273] = 799
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[285] = 710
zX[290] = 230
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[334] = 550
zX[335] = 637
zX[341] = 1141
zX[342] = 1309
zX[356] = 395
zX[362] = 1443
zX[364] = 455
zX[366] = 791
zX[367] = 959
zX[370] = 419
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[403] = 586
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[422] = 661
zX[423] = 506
zX[424] = 674
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
w[16,(240,236)] = 55.8798
w[16,(42,240)] = 110.12
phi[1] = 466
phi[2] = 802
phi[3] = 802
phi[4] = 802
phi[5] = 967
phi[6] = 463
phi[7] = 888
phi[8] = 888
phi[11] = 281
phi[12] = 609
phi[13] = 870
phi[14] = 534
phi[15] = 733
phi[16] = 1326
phi[17] = 213
phi[18] = 541

### All variables with value smaller zero:

### End

real	1448m36.127s
user	4868m35.422s
sys	900m41.597s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
