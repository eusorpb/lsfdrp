	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_34.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_34 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 13437 rows and 6312 columns (presolve time = 6s) ...
Presolve removed 76104 rows and 4384394 columns (presolve time = 11s) ...
Presolve removed 119678 rows and 5885077 columns (presolve time = 15s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 21s) ...
Presolve removed 166986 rows and 6806027 columns (presolve time = 26s) ...
Presolve removed 168595 rows and 6806868 columns (presolve time = 30s) ...
Presolve removed 169397 rows and 6807829 columns (presolve time = 35s) ...
Presolve removed 169457 rows and 6808029 columns (presolve time = 40s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 45s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 55s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 97s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 100s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 958s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 957.88s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309412 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.314e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.952e+11 (roughly 60 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43172051e+15  1.10001976e+16  1.49e+08 2.52e+06  1.52e+12  1071s
   1  -2.43395837e+15  1.07646932e+16  8.21e+07 1.83e+07  8.54e+11  1127s
   2  -1.60383000e+15  1.03777784e+16  5.41e+07 1.17e+07  5.72e+11  1181s
   3  -1.20207878e+15  9.75463996e+15  4.06e+07 6.84e+06  4.24e+11  1235s
   4  -6.12288584e+14  8.89638641e+15  2.08e+07 3.61e+06  2.21e+11  1292s
   5  -2.96903838e+14  7.49096328e+15  1.01e+07 1.52e+06  1.08e+11  1347s
   6  -1.70754869e+14  5.42741168e+15  5.81e+06 5.94e+05  6.42e+10  1402s
   7  -4.56283846e+13  3.19987349e+15  1.55e+06 7.15e+04  1.79e+10  1458s

Barrier performed 7 iterations in 1468.25 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.63s

Solved with dual simplex

Root relaxation: objective 1.317917e+07, 146490 iterations, 502.24 seconds
Total elapsed time = 1470.01s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2897e+07    0  646          - 1.2897e+07      -     - 1572s
     0     0 8268379.48    0  651          - 8268379.48      -     - 2392s
     0     0 7989083.27    0  677          - 7989083.27      -     - 2496s
     0     0 7989083.27    0  677          - 7989083.27      -     - 2498s
     0     0 7455497.96    0  690          - 7455497.96      -     - 2964s
H    0     0                    -6.30586e+07 7455497.96   112%     - 2966s
     0     0 7448266.07    0  697 -6.306e+07 7448266.07   112%     - 3137s
     0     0 7447546.72    0  702 -6.306e+07 7447546.72   112%     - 3163s
     0     0 7447538.27    0  702 -6.306e+07 7447538.27   112%     - 3166s
     0     0 7327608.60    0  794 -6.306e+07 7327608.60   112%     - 3516s
     0     0 7316431.27    0  816 -6.306e+07 7316431.27   112%     - 3800s
     0     0 7315092.00    0  817 -6.306e+07 7315092.00   112%     - 3868s
     0     0 7315068.00    0  819 -6.306e+07 7315068.00   112%     - 3870s
     0     0 7277027.00    0  782 -6.306e+07 7277027.00   112%     - 4026s
H    0     0                    -4.41175e+07 7277027.00   116%     - 4028s
     0     0 7276198.00    0  789 -4.412e+07 7276198.00   116%     - 4056s
     0     0 7276075.92    0  799 -4.412e+07 7276075.92   116%     - 4067s
     0     0 7273773.27    0  806 -4.412e+07 7273773.27   116%     - 4089s
     0     0 7273756.38    0  809 -4.412e+07 7273756.38   116%     - 4097s
     0     0 7271891.38    0  801 -4.412e+07 7271891.38   116%     - 4107s
     0     0 7271890.48    0  794 -4.412e+07 7271890.48   116%     - 4113s
     0     0 7271629.66    0  794 -4.412e+07 7271629.66   116%     - 4191s
     0     0 7270675.71    0  831 -4.412e+07 7270675.71   116%     - 4203s
     0     0 7270639.90    0  827 -4.412e+07 7270639.90   116%     - 4206s
     0     0 7259715.89    0  797 -4.412e+07 7259715.89   116%     - 4217s
     0     0 7259575.11    0  807 -4.412e+07 7259575.11   116%     - 4227s
     0     0 7258612.13    0  794 -4.412e+07 7258612.13   116%     - 4242s
     0     0 7258610.88    0  795 -4.412e+07 7258610.88   116%     - 4248s
     0     0 7258322.34    0  791 -4.412e+07 7258322.34   116%     - 4253s
     0     0 7258316.07    0  801 -4.412e+07 7258316.07   116%     - 4259s
     0     0 7258316.07    0  803 -4.412e+07 7258316.07   116%     - 4262s
     0     0 7258316.07    0  796 -4.412e+07 7258316.07   116%     - 4738s
H    0     0                    -4.23715e+07 7258316.07   117%     - 6641s
     0     2 7258316.07    0  794 -4.237e+07 7258316.07   117%     - 6665s
     1     4 5041337.35    1  704 -4.237e+07 7198127.61   117% 1878025 56329s
     3     8 2006584.28    2  687 -4.237e+07 7058315.81   117% 665041 59734s
     7    12 -1248029.5    3  694 -4.237e+07 6673413.04   116% 335010 66047s
    11    15 -4903658.4    3  605 -4.237e+07 6673392.75   116% 221359 86400s

Cutting planes:
  Implied bound: 10
  Clique: 57
  MIR: 52
  GUB cover: 1
  Zero half: 10

Explored 14 nodes (2801557 simplex iterations) in 86400.61 seconds
Thread count was 4 (of 16 available processors)

Solution count 3: -4.23715e+07 -4.41175e+07 -6.30586e+07 

Time limit reached
Best objective -4.237150906713e+07, best bound 6.673392749669e+06, gap 115.7497%

***
***  |- Calculation finished, timelimit reached 86,400.71 (86400.71) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -42,371,509.0671 (-42371509.0671) and is feasible with a Gap of 1.1575
###
### Service 7 (ME3) is operated by 5 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 290, 200, 112, 275, 203, 271, 367, 330, -3]
###  |  1 (BEANR Out@66.0) - 290 (MAPTM In@211.0/Out@230.0) - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0)
###  |   - 275 (ESALG In@328.0/Out@347.0) - 203 (MAPTM In@543.0/Out@564.0) - 271 (TRGEM In@809.0/Out@817.0) - 367 (TRMER In@9
###  |  15.0/Out@959.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 328, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 17 has the path [145, 335, 336, 337, 338, 339, 329, -3]
###  |  145 (GBFXT Out@302.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718.0) - 337 (EGPSD In@789.0/Out@805
###  |  .0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_
###  |  END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 272, 278, -1]
###  |  100 (DEHAM Out@288.0) - 272 (TRZMK In@617.0/Out@631.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 
###  |  24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 276, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 133, 199, 370, 423, 403, 422, 405, -4]
###  |  218 (NLRTM Out@92.0) - 133 (FRLEH In@111.0/Out@127.0) - 199 (MAPTM In@207.0/Out@228.0) - 370 (AEJEA In@395.0/Out@419.
###  |  0) - 423 (PKBQM In@476.0/Out@506.0) - 403 (INNSA In@570.0/Out@586.0) - 422 (OMSLL In@642.0/Out@661.0) - 405 (INNSA In
###  |  @906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand 277,   81.0 containers from 423 to 422
###  |   |- and carried Demand 278,    2.0 containers from 423 to 422
###  |   |- and carried Demand 281,    1.0 containers from 423 to 403
###  |   |- and carried Demand 284,   63.0 containers from 370 to 423
###  |   |- and carried Demand 285,  174.0 containers from 370 to 423
###  |- Vessel  6 has the path [219, 135, 201, 371, 424, 404, -4]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 371 (AEJEA In@563.0/Out@587
###  |  .0) - 424 (PKBQM In@644.0/Out@674.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand 371,  108.0 containers from 424 to 404
###  |   |- and carried Demand 376,    1.0 containers from 424 to 404
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel  7 has the path [221, 266, 373, 426, 406, -4]
###  |  221 (NLRTM Out@428.0) - 266 (TRALI In@668.0/Out@686.0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@101
###  |  0.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |- Vessel  8 has the path [223, 138, 204, 374, 427, 407, -4]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 374 (AEJEA In@1067.0/Out@10
###  |  91.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel 15 has the path [134, 436, 452, 443, 267, 451, 409, -4]
###  |  134 (FRLEH Out@158.0) - 436 (USCHS In@378.0/Out@388.0) - 452 (USORF In@413.0/Out@426.0) - 443 (USEWR In@450.0/Out@466
###  |  .0) - 267 (TRALI In@836.0/Out@854.0) - 451 (USEWR In@1122.0/Out@1138.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMM
###  |  Y_END Ziel-Service 81)
###  |- Vessel 16 has the path [136, 268, 372, 425, 231, 375, 428, 408, -4]
###  |  136 (FRLEH Out@326.0) - 268 (TRAMB In@576.0/Out@603.0) - 372 (AEJEA In@731.0/Out@755.0) - 425 (PKBQM In@812.0/Out@842
###  |  .0) - 231 (OMSLL In@1132.0/Out@1145.0) - 375 (AEJEA In@1235.0/Out@1259.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (I
###  |  NNSA In@1410.0/Out@1426.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |   |- and carried Demand 362,  244.0 containers from 425 to 408
###  |   |- and carried Demand 363,   67.0 containers from 425 to 408
###  |   |- and carried Demand 365,  118.0 containers from 372 to 408
###  |   |- and carried Demand 366,  133.0 containers from 372 to 425
###  |   |- and carried Demand 367,   61.0 containers from 372 to 425
###  |   |- and carried Demand 368,   15.0 containers from 372 to 408
###  |   |- and carried Demand 369,  267.0 containers from 372 to 408
###  |   |- and carried Demand 370,    1.0 containers from 372 to 408
###  |
### Service 121 (SAE) is operated by 1 vessels from type 30 (PMax35)
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 358, 273, 235, 0]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel  4 has the path [103, 234, 0]
###  |  103 (DEHAM Out@792.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [14]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,30] = 1
rho[81,19] = 1
rho[177,18] = 1
rho[24,18] = 1
rho[121,30] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[81,19,1] = 1
eta[81,19,2] = 1
eta[81,19,3] = 1
eta[81,19,4] = 1
eta[81,19,5] = 1
eta[81,19,6] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[121,30,1] = 1
y[1,272,278] = 1
y[1,278,-1] = 1
y[1,100,272] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,235,0] = 1
y[3,273,235] = 1
y[4,234,0] = 1
y[4,103,234] = 1
y[5,403,422] = 1
y[5,133,199] = 1
y[5,405,-4] = 1
y[5,422,405] = 1
y[5,370,423] = 1
y[5,423,403] = 1
y[5,199,370] = 1
y[5,218,133] = 1
y[6,424,404] = 1
y[6,404,-4] = 1
y[6,135,201] = 1
y[6,201,371] = 1
y[6,371,424] = 1
y[6,219,135] = 1
y[7,426,406] = 1
y[7,373,426] = 1
y[7,221,266] = 1
y[7,406,-4] = 1
y[7,266,373] = 1
y[8,204,374] = 1
y[8,407,-4] = 1
y[8,223,138] = 1
y[8,427,407] = 1
y[8,374,427] = 1
y[8,138,204] = 1
y[9,200,112] = 1
y[9,290,200] = 1
y[9,112,275] = 1
y[9,275,203] = 1
y[9,203,271] = 1
y[9,1,290] = 1
y[9,367,330] = 1
y[9,271,367] = 1
y[9,330,-3] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,328,-3] = 1
y[12,8,366] = 1
y[12,366,328] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,276,-1] = 1
y[13,114,276] = 1
y[13,202,114] = 1
y[15,436,452] = 1
y[15,134,436] = 1
y[15,451,409] = 1
y[15,452,443] = 1
y[15,443,267] = 1
y[15,409,-4] = 1
y[15,267,451] = 1
y[16,372,425] = 1
y[16,425,231] = 1
y[16,375,428] = 1
y[16,268,372] = 1
y[16,136,268] = 1
y[16,231,375] = 1
y[16,428,408] = 1
y[16,408,-4] = 1
y[17,337,338] = 1
y[17,336,337] = 1
y[17,329,-3] = 1
y[17,339,329] = 1
y[17,145,335] = 1
y[17,335,336] = 1
y[17,338,339] = 1
y[18,301,-2] = 1
y[18,148,315] = 1
y[18,315,311] = 1
y[18,311,301] = 1
xD[6,218,133] = 17
xD[14,219,135] = 5
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
xD[277,403,422] = 81
xD[277,423,403] = 81
xD[278,403,422] = 2
xD[278,423,403] = 2
xD[281,423,403] = 1
xD[284,370,423] = 63
xD[285,370,423] = 174
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[362,425,231] = 244
xD[362,375,428] = 244
xD[362,231,375] = 244
xD[362,428,408] = 244
xD[363,425,231] = 67
xD[363,375,428] = 67
xD[363,231,375] = 67
xD[363,428,408] = 67
xD[365,372,425] = 118
xD[365,425,231] = 118
xD[365,375,428] = 118
xD[365,231,375] = 118
xD[365,428,408] = 118
xD[366,372,425] = 133
xD[367,372,425] = 61
xD[368,372,425] = 15
xD[368,425,231] = 15
xD[368,375,428] = 15
xD[368,231,375] = 15
xD[368,428,408] = 15
xD[369,372,425] = 267
xD[369,425,231] = 267
xD[369,375,428] = 267
xD[369,231,375] = 267
xD[369,428,408] = 267
xD[370,372,425] = 1
xD[370,425,231] = 1
xD[370,375,428] = 1
xD[370,231,375] = 1
xD[370,428,408] = 1
xD[371,424,404] = 108
xD[376,424,404] = 1
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[112] = 284
zE[114] = 452
zE[133] = 111
zE[135] = 279
zE[138] = 615
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[231] = 1132
zE[234] = 1304
zE[235] = 1472
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[271] = 809
zE[272] = 617
zE[273] = 785
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[335] = 621
zE[336] = 699
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[358] = 683
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[367] = 915
zE[370] = 395
zE[371] = 563
zE[372] = 731
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[403] = 570
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[422] = 642
zE[423] = 476
zE[424] = 644
zE[425] = 812
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zE[436] = 378
zE[443] = 450
zE[451] = 1122
zE[452] = 413
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[112] = 296
zX[114] = 464
zX[133] = 127
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[138] = 631
zX[145] = 302
zX[148] = 470
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[231] = 1145
zX[234] = 1316
zX[235] = 1484
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[271] = 817
zX[272] = 631
zX[273] = 799
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[335] = 637
zX[336] = 718
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[358] = 731
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[367] = 959
zX[370] = 419
zX[371] = 587
zX[372] = 755
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[403] = 586
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[422] = 661
zX[423] = 506
zX[424] = 674
zX[425] = 842
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
zX[436] = 388
zX[443] = 466
zX[451] = 1138
zX[452] = 426
phi[1] = 563
phi[2] = 227
phi[3] = 860
phi[4] = 524
phi[5] = 830
phi[6] = 494
phi[7] = 662
phi[8] = 662
phi[9] = 1329
phi[10] = 489
phi[11] = 489
phi[12] = 489
phi[13] = 295
phi[15] = 1436
phi[16] = 1100
phi[17] = 925
phi[18] = 541

### All variables with value smaller zero:

### End

real	1448m35.354s
user	4871m4.063s
sys	897m43.338s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
