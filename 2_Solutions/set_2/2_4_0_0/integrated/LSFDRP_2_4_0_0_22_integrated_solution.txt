	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_4_0_0_fr.p
Instance LSFDP: LSFDRP_2_4_0_0_fd_22.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_4_0_0_fd_22 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 458
*** |A|: 18883
*** |A^i|: 18832
*** |A^f|: 51
*** |M|: 382
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 226516 rows, 7592842 columns and 26848578 nonzeros
Variable types: 7252870 continuous, 339972 integer (339972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 13437 rows and 6312 columns (presolve time = 6s) ...
Presolve removed 76104 rows and 4384394 columns (presolve time = 10s) ...
Presolve removed 119693 rows and 5885092 columns (presolve time = 15s) ...
Presolve removed 161356 rows and 6797191 columns (presolve time = 21s) ...
Presolve removed 167259 rows and 6806027 columns (presolve time = 25s) ...
Presolve removed 169396 rows and 6807829 columns (presolve time = 30s) ...
Presolve removed 169401 rows and 6807973 columns (presolve time = 35s) ...
Presolve removed 169472 rows and 6808151 columns (presolve time = 40s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 169474 rows and 6808151 columns (presolve time = 50s) ...
Presolve removed 169474 rows and 6808334 columns (presolve time = 92s) ...
Presolve removed 169725 rows and 6808357 columns (presolve time = 95s) ...
Presolve removed 171506 rows and 6813753 columns (presolve time = 907s) ...
Presolve removed 171506 rows and 6813753 columns
Presolve time: 906.87s
Presolved: 55010 rows, 779089 columns, 3309465 nonzeros
Variable types: 440087 continuous, 339002 integer (338954 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 12 rows and 12 columns
Presolved: 54998 rows, 779077 columns, 3309380 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 3.677e+06
 Factor NZ  : 1.286e+08 (roughly 1.4 GBytes of memory)
 Factor Ops : 8.835e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -4.43525881e+15  7.11786815e+15  1.49e+08 1.63e+06  9.82e+11  1015s
   1  -2.43591114e+15  6.96577008e+15  8.21e+07 1.18e+07  5.53e+11  1070s
   2  -1.60497265e+15  6.71576505e+15  5.42e+07 7.55e+06  3.71e+11  1126s
   3  -1.20282287e+15  6.31222248e+15  4.07e+07 4.42e+06  2.75e+11  1182s
   4  -6.10550460e+14  5.74519214e+15  2.07e+07 2.28e+06  1.42e+11  1239s
   5  -2.93093582e+14  4.85492268e+15  9.98e+06 9.88e+05  6.94e+10  1295s
   6  -1.99368692e+14  3.55205640e+15  6.79e+06 4.00e+05  4.77e+10  1352s
   7  -5.00533169e+13  2.26530375e+15  1.70e+06 2.12e+04  1.28e+10  1410s
   8  -7.67587680e+12  9.90639040e+14  2.61e+05 2.65e-06  2.27e+09  1469s

Barrier performed 8 iterations in 1469.09 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 5.93s

Solved with dual simplex

Root relaxation: objective 1.322701e+07, 149529 iterations, 554.97 seconds
Total elapsed time = 1470.33s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2973e+07    0  658          - 1.2973e+07      -     - 1545s
     0     0 1.1038e+07    0  745          - 1.1038e+07      -     - 1865s
     0     0 1.0787e+07    0  731          - 1.0787e+07      -     - 1947s
     0     0 1.0787e+07    0  733          - 1.0787e+07      -     - 1949s
     0     0 1.0262e+07    0  816          - 1.0262e+07      -     - 2372s
H    0     0                    -5.48656e+07 1.0262e+07   119%     - 2375s
H    0     0                    -5.40231e+07 1.0262e+07   119%     - 2381s
     0     0 8528046.04    0  773 -5.402e+07 8528046.04   116%     - 3521s
     0     0 8528046.04    0  773 -5.402e+07 8528046.04   116%     - 3522s
     0     0 8320649.99    0  762 -5.402e+07 8320649.99   115%     - 4162s
H    0     0                    -4.64870e+07 8320649.99   118%     - 4164s
     0     0 8309937.88    0  757 -4.649e+07 8309937.88   118%     - 4291s
     0     0 8307694.79    0  793 -4.649e+07 8307694.79   118%     - 4331s
     0     0 8307298.94    0  788 -4.649e+07 8307298.94   118%     - 4341s
     0     0 8205291.05    0  800 -4.649e+07 8205291.05   118%     - 4680s
     0     0 8200462.45    0  805 -4.649e+07 8200462.45   118%     - 4741s
     0     0 8193962.58    0  790 -4.649e+07 8193962.58   118%     - 4795s
     0     0 8193961.18    0  790 -4.649e+07 8193961.18   118%     - 4797s
     0     0 8189758.07    0  782 -4.649e+07 8189758.07   118%     - 4814s
     0     0 8187857.60    0  788 -4.649e+07 8187857.60   118%     - 4828s
     0     0 8186663.44    0  783 -4.649e+07 8186663.44   118%     - 4843s
     0     0 8186655.53    0  777 -4.649e+07 8186655.53   118%     - 4844s
     0     0 8183372.20    0  783 -4.649e+07 8183372.20   118%     - 4855s
     0     0 8183371.30    0  783 -4.649e+07 8183371.30   118%     - 4861s
     0     0 8183369.86    0  783 -4.649e+07 8183369.86   118%     - 4863s
     0     0 8181946.14    0  779 -4.649e+07 8181946.14   118%     - 4871s
     0     0 8181549.81    0  799 -4.649e+07 8181549.81   118%     - 4877s
     0     0 8181549.81    0  799 -4.649e+07 8181549.81   118%     - 4880s
     0     0 8180179.80    0  785 -4.649e+07 8180179.80   118%     - 4892s
     0     0 8179686.67    0  781 -4.649e+07 8179686.67   118%     - 4894s
     0     0 8179686.67    0  781 -4.649e+07 8179686.67   118%     - 5627s
H    0     0                    -2.63008e+07 8179686.67   131%     - 5783s
     0     2 8179686.67    0  781 -2.630e+07 8179686.67   131%     - 6514s
     1     4 6656879.07    1  705 -2.630e+07 8179487.87   131% 1045891 33989s
     3     8 6508371.22    2  829 -2.630e+07 6825142.64   126% 391424 37469s
     7    12 6373259.81    3  725 -2.630e+07 6508207.81   125% 194321 38854s
    11    12 infeasible    3      -2.630e+07 6508207.81   125% 130874 86400s

Cutting planes:
  Implied bound: 11
  Clique: 38
  MIR: 55
  Flow cover: 3
  GUB cover: 1
  Zero half: 9

Explored 13 nodes (1875068 simplex iterations) in 86400.55 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -2.63008e+07 -4.6487e+07 -5.40231e+07 -5.48656e+07 

Time limit reached
Best objective -2.630078133514e+07, best bound 6.508207810829e+06, gap 124.7453%

***
***  |- Calculation finished, timelimit reached 86,400.66 (86400.66) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -26,300,781.3351 (-26300781.3351) and is feasible with a Gap of 1.2475
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 436, 452, 443, 168, 340, 341, 331, -3]
###  |  218 (NLRTM Out@92.0) - 436 (USCHS In@378.0/Out@388.0) - 452 (USORF In@413.0/Out@426.0) - 443 (USEWR In@450.0/Out@466.
###  |  0) - 168 (ITGIT In@904.0/Out@923.0) - 340 (EGPSD In@1035.0/Out@1054.0) - 341 (EGPSD In@1125.0/Out@1141.0) - 331 (AEJE
###  |  A In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel  6 has the path [219, 364, 325, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel  7 has the path [221, 137, 203, 327, -3]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 327 (AEJEA In@858.0/Out@891
###  |  .0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |- Vessel  8 has the path [223, 366, 329, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 200, 371, 424, 230, 328, -3]
###  |  134 (FRLEH Out@158.0) - 200 (MAPTM In@238.0/Out@259.0) - 371 (AEJEA In@563.0/Out@587.0) - 424 (PKBQM In@644.0/Out@674
###  |  .0) - 230 (OMSLL In@889.0/Out@904.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 379,  156.0 containers from 371 to 424
###  |   |- and carried Demand 380,   45.0 containers from 371 to 424
###  |- Vessel 16 has the path [136, 357, 335, 336, 337, 338, 339, 330, -3]
###  |  136 (FRLEH Out@326.0) - 357 (TRAMB In@515.0/Out@563.0) - 335 (EGPSD In@621.0/Out@637.0) - 336 (EGPSD In@699.0/Out@718
###  |  .0) - 337 (EGPSD In@789.0/Out@805.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 330 (AEJEA I
###  |  n@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |
### Service 24 (WAF7) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 12 has the path [8, 277, -1]
###  |  8 (BEANR Out@570.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 17 has the path [145, 276, -1]
###  |  145 (GBFXT Out@302.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 268, 404, -4]
###  |  100 (DEHAM Out@288.0) - 268 (TRAMB In@576.0/Out@603.0) - 404 (INNSA In@738.0/Out@754.0) - -4 (DUMMY_END Ziel-Service 
###  |  81)
###  |- Vessel  2 has the path [101, 365, 353, 231, 375, 428, 408, -4]
###  |  101 (DEHAM Out@456.0) - 365 (TRMER In@579.0/Out@623.0) - 353 (OMSLL In@878.0/Out@890.0) - 231 (OMSLL In@1132.0/Out@11
###  |  45.0) - 375 (AEJEA In@1235.0/Out@1259.0) - 428 (PKBQM In@1316.0/Out@1346.0) - 408 (INNSA In@1410.0/Out@1426.0) - -4 (
###  |  DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 306,   31.0 containers from 428 to 408
###  |   |- and carried Demand 307,   37.0 containers from 375 to 408
###  |   |- and carried Demand 308,  211.0 containers from 375 to 408
###  |   |- and carried Demand 309,  176.0 containers from 375 to 428
###  |- Vessel  3 has the path [102, 358, 273, 374, 427, 407, -4]
###  |  102 (DEHAM Out@624.0) - 358 (TRAMB In@683.0/Out@731.0) - 273 (TRZMK In@785.0/Out@799.0) - 374 (AEJEA In@1067.0/Out@10
###  |  91.0) - 427 (PKBQM In@1148.0/Out@1178.0) - 407 (INNSA In@1242.0/Out@1258.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 329,   86.0 containers from 374 to 427
###  |   |- and carried Demand 330,  118.0 containers from 374 to 427
###  |- Vessel  4 has the path [103, 362, 409, -4]
###  |  103 (DEHAM Out@792.0) - 362 (TRAMB In@1416.0/Out@1443.0) - 409 (INNSA In@1578.0/Out@1594.0) - -4 (DUMMY_END Ziel-Serv
###  |  ice 81)
###  |- Vessel 13 has the path [104, 356, 352, 229, 405, -4]
###  |  104 (DKAAR Out@220.0) - 356 (TRAMB In@347.0/Out@395.0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736
###  |  .0) - 405 (INNSA In@906.0/Out@922.0) - -4 (DUMMY_END Ziel-Service 81)
###  |- Vessel 14 has the path [105, 266, 373, 426, 406, -4]
###  |  105 (DKAAR Out@388.0) - 266 (TRALI In@668.0/Out@686.0) - 373 (AEJEA In@899.0/Out@923.0) - 426 (PKBQM In@980.0/Out@101
###  |  0.0) - 406 (INNSA In@1074.0/Out@1090.0) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 346,  445.0 containers from 373 to 406
###  |   |- and carried Demand 348,  200.0 containers from 373 to 426
###  |   |- and carried Demand 349,   24.0 containers from 373 to 426
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 291, 202, 114, 302, -2]
###  |  2 (BEANR Out@234.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0
###  |  ) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |- Vessel 11 has the path [5, 315, 311, 301, -2]
###  |  5 (BEANR Out@402.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@1011.
###  |  0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 111, 284, 45, 36, 41, 240, 235, 0]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 111 (ESALG In@253.0/Out@265.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@765.0/Out@811.0) - 36 (CLLQN In@124
###  |  0.0/Out@1258.0) - 41 (CLSAI In@1288.0/Out@1304.0) - 240 (PECLL In@1365.93/Out@1367.93) - 235 (PABLB In@1472.0/Out@148
###  |  4.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 111
###  |   |- and carried Demand 117,  580.0 containers from 199 to 111
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 111
###  |- Vessel 18 has the path [148, 267, 234, 0]
###  |  148 (GBFXT Out@470.0) - 267 (TRALI In@836.0/Out@854.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: []
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[7,19] = 1
rho[81,18] = 1
rho[177,30] = 1
rho[24,30] = 1
rho[121,30] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
eta[81,18,1] = 1
eta[81,18,2] = 1
eta[81,18,3] = 1
eta[81,18,4] = 1
eta[81,18,5] = 1
eta[81,18,6] = 1
eta[177,30,1] = 1
eta[177,30,2] = 1
eta[24,30,1] = 1
eta[24,30,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,404,-4] = 1
y[1,100,268] = 1
y[1,268,404] = 1
y[2,365,353] = 1
y[2,375,428] = 1
y[2,101,365] = 1
y[2,231,375] = 1
y[2,428,408] = 1
y[2,353,231] = 1
y[2,408,-4] = 1
y[3,102,358] = 1
y[3,358,273] = 1
y[3,407,-4] = 1
y[3,273,374] = 1
y[3,427,407] = 1
y[3,374,427] = 1
y[4,362,409] = 1
y[4,103,362] = 1
y[4,409,-4] = 1
y[5,436,452] = 1
y[5,218,436] = 1
y[5,341,331] = 1
y[5,452,443] = 1
y[5,168,340] = 1
y[5,331,-3] = 1
y[5,340,341] = 1
y[5,443,168] = 1
y[6,364,325] = 1
y[6,219,364] = 1
y[6,325,-3] = 1
y[7,327,-3] = 1
y[7,137,203] = 1
y[7,203,327] = 1
y[7,221,137] = 1
y[8,223,366] = 1
y[8,329,-3] = 1
y[8,366,329] = 1
y[9,274,199] = 1
y[9,240,235] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,45,36] = 1
y[9,41,240] = 1
y[9,235,0] = 1
y[9,290,111] = 1
y[9,36,41] = 1
y[9,111,284] = 1
y[9,284,45] = 1
y[10,291,202] = 1
y[10,302,-2] = 1
y[10,202,114] = 1
y[10,2,291] = 1
y[10,114,302] = 1
y[11,301,-2] = 1
y[11,5,315] = 1
y[11,315,311] = 1
y[11,311,301] = 1
y[12,8,277] = 1
y[12,277,-1] = 1
y[13,352,229] = 1
y[13,405,-4] = 1
y[13,229,405] = 1
y[13,104,356] = 1
y[13,356,352] = 1
y[14,105,266] = 1
y[14,426,406] = 1
y[14,373,426] = 1
y[14,406,-4] = 1
y[14,266,373] = 1
y[15,200,371] = 1
y[15,424,230] = 1
y[15,328,-3] = 1
y[15,230,328] = 1
y[15,134,200] = 1
y[15,371,424] = 1
y[16,337,338] = 1
y[16,336,337] = 1
y[16,339,330] = 1
y[16,357,335] = 1
y[16,335,336] = 1
y[16,338,339] = 1
y[16,136,357] = 1
y[16,330,-3] = 1
y[17,276,-1] = 1
y[17,145,276] = 1
y[18,234,0] = 1
y[18,148,267] = 1
y[18,267,234] = 1
xD[7,199,290] = 161
xD[7,290,111] = 161
xD[7,111,284] = 161
xD[7,284,45] = 161
xD[8,199,290] = 580
xD[8,290,111] = 580
xD[26,221,137] = 6
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,199,290] = 580
xD[117,290,111] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,290,111] = 161
xD[144,111,284] = 161
xD[144,284,45] = 161
xD[145,290,111] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[306,428,408] = 31
xD[307,375,428] = 37
xD[307,428,408] = 37
xD[308,375,428] = 211
xD[308,428,408] = 211
xD[309,375,428] = 176
xD[329,374,427] = 86
xD[330,374,427] = 118
xD[346,426,406] = 445
xD[346,373,426] = 445
xD[348,373,426] = 200
xD[349,373,426] = 24
xD[379,371,424] = 156
xD[380,371,424] = 45
zE[36] = 1240
zE[41] = 1288
zE[45] = 765
zE[111] = 253
zE[114] = 452
zE[137] = 447
zE[168] = 904
zE[199] = 207
zE[200] = 238
zE[202] = 406
zE[203] = 543
zE[229] = 721
zE[230] = 889
zE[231] = 1132
zE[234] = 1304
zE[235] = 1472
zE[240] = 1365.93
zE[266] = 668
zE[267] = 836
zE[268] = 576
zE[273] = 785
zE[274] = 160
zE[276] = 496
zE[277] = 664
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[335] = 621
zE[336] = 699
zE[337] = 789
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[341] = 1125
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[357] = 515
zE[358] = 683
zE[362] = 1416
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[371] = 563
zE[373] = 899
zE[374] = 1067
zE[375] = 1235
zE[404] = 738
zE[405] = 906
zE[406] = 1074
zE[407] = 1242
zE[408] = 1410
zE[409] = 1578
zE[424] = 644
zE[426] = 980
zE[427] = 1148
zE[428] = 1316
zE[436] = 378
zE[443] = 450
zE[452] = 413
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[36] = 1258
zX[41] = 1304
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[103] = 792
zX[104] = 220
zX[105] = 388
zX[111] = 265
zX[114] = 464
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[145] = 302
zX[148] = 470
zX[168] = 923
zX[199] = 228
zX[200] = 259
zX[202] = 427
zX[203] = 564
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[231] = 1145
zX[234] = 1316
zX[235] = 1484
zX[240] = 1367.93
zX[266] = 686
zX[267] = 854
zX[268] = 603
zX[273] = 799
zX[274] = 179
zX[276] = 515
zX[277] = 683
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[335] = 637
zX[336] = 718
zX[337] = 805
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[341] = 1141
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[357] = 563
zX[358] = 731
zX[362] = 1443
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[371] = 587
zX[373] = 923
zX[374] = 1091
zX[375] = 1259
zX[404] = 754
zX[405] = 922
zX[406] = 1090
zX[407] = 1258
zX[408] = 1426
zX[409] = 1594
zX[424] = 674
zX[426] = 1010
zX[427] = 1178
zX[428] = 1346
zX[436] = 388
zX[443] = 466
zX[452] = 426
w[9,(240,235)] = 104.071
w[9,(41,240)] = 61.9294
phi[1] = 466
phi[2] = 970
phi[3] = 634
phi[4] = 802
phi[5] = 1471
phi[6] = 463
phi[7] = 463
phi[8] = 631
phi[9] = 1418
phi[10] = 945
phi[11] = 609
phi[12] = 113
phi[13] = 702
phi[14] = 702
phi[15] = 901
phi[16] = 1069
phi[17] = 213
phi[18] = 846

### All variables with value smaller zero:

### End

real	1448m49.495s
user	5035m48.908s
sys	733m34.585s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
