	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_1_fr.p
Instance LSFDP: LSFDRP_2_3_0_1_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_1_fd_19 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 33971 rows and 1627817 columns (presolve time = 5s) ...
Presolve removed 62778 rows and 2578278 columns (presolve time = 10s) ...
Presolve removed 75348 rows and 2753798 columns (presolve time = 15s) ...
Presolve removed 78252 rows and 2757008 columns (presolve time = 20s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757047 columns (presolve time = 28s) ...
Presolve removed 78275 rows and 2757212 columns (presolve time = 52s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 56s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 435s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 435.36s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 30 seconds per iteration)
 Threads    : 1

Barrier performed 0 iterations in 488.81 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.07s

Solved with dual simplex

Root relaxation: objective 8.087418e+06, 34350 iterations, 49.05 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7880998.89    0  290          - 7880998.89      -     -  511s
     0     0 7165627.74    0  364          - 7165627.74      -     -  627s
H    0     0                    -2.13603e+07 7165627.74   134%     -  629s
H    0     0                    -2.07356e+07 7165627.74   135%     -  633s
     0     0 7022791.36    0  365 -2.074e+07 7022791.36   134%     -  640s
     0     0 7022559.27    0  372 -2.074e+07 7022559.27   134%     -  643s
     0     0 6695748.77    0  385 -2.074e+07 6695748.77   132%     -  732s
     0     0 6691933.25    0  394 -2.074e+07 6691933.25   132%     -  740s
     0     0 6691933.25    0  395 -2.074e+07 6691933.25   132%     -  740s
     0     0 6684205.95    0  397 -2.074e+07 6684205.95   132%     -  747s
     0     0 6676305.49    0  395 -2.074e+07 6676305.49   132%     -  755s
     0     0 6676305.49    0  395 -2.074e+07 6676305.49   132%     -  756s
     0     0 6673416.27    0  401 -2.074e+07 6673416.27   132%     -  760s
     0     0 6673416.27    0  403 -2.074e+07 6673416.27   132%     -  764s
     0     0 6673199.67    0  402 -2.074e+07 6673199.67   132%     -  767s
     0     0 6673009.53    0  403 -2.074e+07 6673009.53   132%     -  770s
     0     0 6672809.14    0  408 -2.074e+07 6672809.14   132%     -  773s
     0     0 6672809.14    0  410 -2.074e+07 6672809.14   132%     -  776s
     0     0 6672763.76    0  408 -2.074e+07 6672763.76   132%     -  778s
     0     0 6672722.25    0  410 -2.074e+07 6672722.25   132%     -  782s
     0     0 6672684.06    0  415 -2.074e+07 6672684.06   132%     -  784s
     0     0 6672620.60    0  418 -2.074e+07 6672620.60   132%     -  789s
     0     0 6672620.60    0  417 -2.074e+07 6672620.60   132%     -  937s
H    0     0                    -2092329.582 6672620.60   419%     -  996s
     0     2 6672620.60    0  417 -2092329.6 6672620.60   419%     - 1006s
     1     4 4994233.16    1  395 -2092329.6 6442233.61   408% 252127 5152s
     3     5 4929893.60    2  365 -2092329.6 5852192.51   380% 93400 9435s
     7     9 4814391.37    3  418 -2092329.6 5820608.47   378% 82137 11343s
    11    13 3961647.90    4  301 -2092329.6 5534889.12   365% 99507 23720s
    15    14 3032844.49    5  341 -2092329.6 5531422.70   364% 118681 30575s
    19    16 2559866.31    6  315 -2092329.6 4950722.54   337% 119546 42994s
    23    19 2494645.88    7  316 -2092329.6 4950722.54   337% 134872 47956s
    30    24 2463802.53    8  304 -2092329.6 4950722.54   337% 105821 58352s
    36    31 2026189.63    9  321 -2092329.6 4950722.54   337% 99859 59193s
    43    37 1932510.39   10  236 -2092329.6 4950722.54   337% 94666 69230s
    50    41     cutoff   11      -2092329.6 4950722.54   337% 84789 80125s
    60    48 infeasible    4      -2092329.6 4950722.54   337% 77134 83711s
H   62    48                    -2092329.581 4932383.25   336% 74651 83711s
H   71    55                    -2044822.746 4932383.25   341% 67238 83711s
    72    60 4871099.94    8  244 -2044822.7 4932383.25   341% 66570 86400s

Cutting planes:
  Implied bound: 3
  Clique: 16
  MIR: 37
  GUB cover: 1
  Zero half: 3

Explored 84 nodes (4893938 simplex iterations) in 86400.32 seconds
Thread count was 4 (of 16 available processors)

Solution count 5: -2.04482e+06 -2.09233e+06 -2.09233e+06 ... -2.13603e+07

Time limit reached
Best objective -2.044822746146e+06, best bound 4.932383248594e+06, gap 341.2132%

***
***  |- Calculation finished, timelimit reached 86,400.38 (86400.38) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,044,822.7461 (-2044822.7461) and is feasible with a Gap of 3.4121
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 368, 331, -3]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@76
###  |  5.0/Out@811.0) - 368 (TRMER In@1083.0/Out@1127.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to  45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 10 has the path [2, 364, 325, -3]
###  |  2 (BEANR Out@234.0) - 364 (TRMER In@411.0/Out@455.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 11 has the path [5, 365, 327, -3]
###  |  5 (BEANR Out@402.0) - 365 (TRMER In@579.0/Out@623.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel 12 has the path [8, 366, 338, 339, 329, -3]
###  |  8 (BEANR Out@570.0) - 366 (TRMER In@747.0/Out@791.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0
###  |  ) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel 17 has the path [145, 266, 353, 230, 328, -3]
###  |  145 (GBFXT Out@302.0) - 266 (TRALI In@668.0/Out@686.0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904
###  |  .0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |- Vessel 18 has the path [148, 267, 340, 330, -3]
###  |  148 (GBFXT Out@470.0) - 267 (TRALI In@836.0/Out@854.0) - 340 (EGPSD In@1035.0/Out@1054.0) - 330 (AEJEA In@1362.0/Out@
###  |  1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 199,  188.0 containers from 340 to 330
###  |   |- and carried Demand 200,    7.0 containers from 340 to 330
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
### Service 121 (SAE) is operated by 2 vessels from type 19 (PMax28)
###  |- Vessel  6 has the path [219, 135, 201, 113, 302, -2]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 113 (ESALG In@421.0/Out@433
###  |  .0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  13,   74.0 containers from 219 to 113
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand 118,   74.0 containers from 219 to 113
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |- Vessel  7 has the path [221, 315, 311, 301, -2]
###  |  221 (NLRTM Out@428.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@101
###  |  1.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 363, 260, 190, 170, 49, 75, 72, 53, 61, 236, 0]
###  |  218 (NLRTM Out@92.0) - 363 (TRMER In@243.0/Out@287.0) - 260 (SGSIN In@618.0/Out@651.0) - 190 (KRPUS In@788.0/Out@810.
###  |  0) - 170 (JPHKT In@824.0/Out@833.0) - 49 (CNDLC In@871.0/Out@886.0) - 75 (CNXGG In@904.0/Out@923.0) - 72 (CNTAO In@96
###  |  0.0/Out@978.0) - 53 (CNNGB In@1031.0/Out@1040.0) - 61 (CNSHA In@1069.0/Out@1079.0) - 236 (PABLB In@1640.0/Out@1652.0)
###  |   - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  36,  907.0 containers from  61 to 236
###  |   |- and carried Demand  37,    3.0 containers from  61 to 236
###  |- Vessel  8 has the path [223, 138, 204, 235, 0]
###  |  223 (NLRTM Out@596.0) - 138 (FRLEH In@615.0/Out@631.0) - 204 (MAPTM In@711.0/Out@732.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [134, 275, 203, 234, 0]
###  |  134 (FRLEH Out@158.0) - 275 (ESALG In@328.0/Out@347.0) - 203 (MAPTM In@543.0/Out@564.0) - 234 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [3, 4, 14, 16]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,19] = 1
rho[121,19] = 1
rho[7,30] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
eta[121,19,1] = 1
eta[121,19,2] = 1
eta[7,30,1] = 1
eta[7,30,2] = 1
eta[7,30,3] = 1
eta[7,30,4] = 1
eta[7,30,5] = 1
eta[7,30,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[5,190,170] = 1
y[5,218,363] = 1
y[5,49,75] = 1
y[5,53,61] = 1
y[5,260,190] = 1
y[5,61,236] = 1
y[5,363,260] = 1
y[5,75,72] = 1
y[5,72,53] = 1
y[5,236,0] = 1
y[5,170,49] = 1
y[6,135,201] = 1
y[6,302,-2] = 1
y[6,201,113] = 1
y[6,113,302] = 1
y[6,219,135] = 1
y[7,301,-2] = 1
y[7,315,311] = 1
y[7,311,301] = 1
y[7,221,315] = 1
y[8,235,0] = 1
y[8,204,235] = 1
y[8,223,138] = 1
y[8,138,204] = 1
y[9,200,112] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,368,331] = 1
y[9,331,-3] = 1
y[9,45,368] = 1
y[9,284,45] = 1
y[9,112,284] = 1
y[10,2,364] = 1
y[10,364,325] = 1
y[10,325,-3] = 1
y[11,365,327] = 1
y[11,327,-3] = 1
y[11,5,365] = 1
y[12,366,338] = 1
y[12,329,-3] = 1
y[12,339,329] = 1
y[12,8,366] = 1
y[12,338,339] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,114,278] = 1
y[13,202,114] = 1
y[13,278,-1] = 1
y[15,234,0] = 1
y[15,275,203] = 1
y[15,203,234] = 1
y[15,134,275] = 1
y[17,145,266] = 1
y[17,328,-3] = 1
y[17,266,353] = 1
y[17,353,230] = 1
y[17,230,328] = 1
y[18,148,267] = 1
y[18,267,340] = 1
y[18,340,330] = 1
y[18,330,-3] = 1
xD[7,200,112] = 161
xD[7,290,200] = 161
xD[7,199,290] = 161
xD[7,284,45] = 161
xD[7,112,284] = 161
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[13,135,201] = 74
xD[13,201,113] = 74
xD[13,219,135] = 74
xD[14,219,135] = 5
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[36,61,236] = 907
xD[37,61,236] = 3
xD[45,200,112] = 161
xD[45,284,45] = 161
xD[45,112,284] = 161
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[118,135,201] = 74
xD[118,201,113] = 74
xD[118,219,135] = 74
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[131,200,112] = 580
xD[132,114,278] = 214
xD[132,202,114] = 214
xD[133,114,278] = 10
xD[133,202,114] = 10
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[138,291,202] = 214
xD[138,114,278] = 214
xD[138,202,114] = 214
xD[139,291,202] = 10
xD[139,114,278] = 10
xD[139,202,114] = 10
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,200,112] = 161
xD[144,290,200] = 161
xD[144,284,45] = 161
xD[144,112,284] = 161
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[199,340,330] = 188
xD[200,340,330] = 7
xD[201,353,230] = 111
xD[201,230,328] = 111
xD[202,353,230] = 35
xD[202,230,328] = 35
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
zE[45] = 765
zE[49] = 871
zE[53] = 1031
zE[61] = 1069
zE[72] = 960
zE[75] = 904
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[135] = 279
zE[138] = 615
zE[170] = 824
zE[190] = 788
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[260] = 618
zE[266] = 668
zE[267] = 836
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[353] = 878
zE[363] = 243
zE[364] = 411
zE[365] = 579
zE[366] = 747
zE[368] = 1083
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[8] = 570
zX[45] = 811
zX[49] = 886
zX[53] = 1040
zX[61] = 1079
zX[72] = 978
zX[75] = 923
zX[100] = 288
zX[101] = 456
zX[104] = 220
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[134] = 158
zX[135] = 295
zX[138] = 631
zX[145] = 302
zX[148] = 470
zX[170] = 833
zX[190] = 810
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[260] = 651
zX[266] = 686
zX[267] = 854
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[353] = 890
zX[363] = 287
zX[364] = 455
zX[365] = 623
zX[366] = 791
zX[368] = 1127
phi[1] = 227
phi[2] = 227
phi[5] = 1560
phi[6] = 919
phi[7] = 583
phi[8] = 888
phi[9] = 1497
phi[10] = 489
phi[11] = 489
phi[12] = 657
phi[13] = 631
phi[15] = 1158
phi[17] = 757
phi[18] = 925

### All variables with value smaller zero:

### End

real	1444m11.849s
user	4923m29.193s
sys	835m31.137s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
