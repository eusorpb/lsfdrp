	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_1_fr.p
Instance LSFDP: LSFDRP_2_3_0_1_fd_37.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_1_fd_37 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 37611 rows and 1750586 columns (presolve time = 5s) ...
Presolve removed 70591 rows and 2743928 columns (presolve time = 11s) ...
Presolve removed 75978 rows and 2753864 columns (presolve time = 15s) ...
Presolve removed 78252 rows and 2757008 columns (presolve time = 21s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757047 columns (presolve time = 27s) ...
Presolve removed 78275 rows and 2757212 columns (presolve time = 51s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 55s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 433s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 433.10s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 22
 AA' NZ     : 2.915e+06
 Factor NZ  : 9.237e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.723e+11 (roughly 34 seconds per iteration)
 Threads    : 1

Barrier performed 0 iterations in 489.72 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.05s

Solved with dual simplex

Root relaxation: objective 8.087418e+06, 37904 iterations, 51.47 seconds
Total elapsed time = 491.34s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7880998.89    0  297          - 7880998.89      -     -  517s
     0     0 7092634.58    0  350          - 7092634.58      -     -  570s
H    0     0                    -3.58230e+07 7092634.58   120%     -  572s
     0     0 6942119.60    0  353 -3.582e+07 6942119.60   119%     -  578s
     0     0 6941966.16    0  358 -3.582e+07 6941966.16   119%     -  580s
     0     0 6941966.16    0  359 -3.582e+07 6941966.16   119%     -  580s
     0     0 6459831.92    0  408 -3.582e+07 6459831.92   118%     -  720s
H    0     0                    -2.88427e+07 6459831.92   122%     -  722s
     0     0 6436734.82    0  409 -2.884e+07 6436734.82   122%     -  740s
     0     0 6436734.82    0  409 -2.884e+07 6436734.82   122%     -  741s
     0     0 6420417.00    0  419 -2.884e+07 6420417.00   122%     -  751s
H    0     0                    -2.86447e+07 6420417.00   122%     -  752s
     0     0 6418167.14    0  423 -2.864e+07 6418167.14   122%     -  756s
     0     0 6418167.14    0  423 -2.864e+07 6418167.14   122%     -  756s
     0     0 6413742.30    0  418 -2.864e+07 6413742.30   122%     -  763s
     0     0 6413317.44    0  424 -2.864e+07 6413317.44   122%     -  767s
     0     0 6413317.44    0  424 -2.864e+07 6413317.44   122%     -  768s
     0     0 6413029.99    0  429 -2.864e+07 6413029.99   122%     -  771s
     0     0 6412956.32    0  442 -2.864e+07 6412956.32   122%     -  775s
     0     0 6412693.69    0  443 -2.864e+07 6412693.69   122%     -  778s
     0     0 6412693.69    0  443 -2.864e+07 6412693.69   122%     -  781s
     0     0 6412477.79    0  436 -2.864e+07 6412477.79   122%     -  784s
     0     0 6412469.60    0  444 -2.864e+07 6412469.60   122%     -  787s
     0     0 6412129.19    0  444 -2.864e+07 6412129.19   122%     -  791s
     0     0 6412129.19    0  445 -2.864e+07 6412129.19   122%     -  794s
     0     0 6411906.38    0  445 -2.864e+07 6411906.38   122%     -  796s
     0     0 6411906.38    0  444 -2.864e+07 6411906.38   122%     -  964s
H    0     0                    -2.86311e+07 6411906.38   122%     - 1012s
H    0     0                    -1.12957e+07 6411906.38   157%     - 1023s
H    0     2                    -1.12898e+07 6411906.38   157%     - 1045s
     0     2 6411906.38    0  444 -1.129e+07 6411906.38   157%     - 1045s
     1     3 5650317.21    1  382 -1.129e+07 5650317.21   150% 21559 86400s

Cutting planes:
  Implied bound: 3
  Clique: 19
  MIR: 34
  Flow cover: 1
  GUB cover: 1
  Zero half: 6

Explored 2 nodes (103688 simplex iterations) in 86400.32 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -1.12898e+07 -1.12957e+07 -2.86311e+07 ... -3.5823e+07

Time limit reached
Best objective -1.128978047730e+07, best bound 5.642114575159e+06, gap 149.9754%

***
***  |- Calculation finished, timelimit reached 86,400.37 (86400.37) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -11,289,780.4773 (-11289780.4773) and is feasible with a Gap of 1.4998
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 347, 344, 324, 352, 229, 327, -3]
###  |  218 (NLRTM Out@92.0) - 347 (INPAV In@467.0/Out@494.0) - 344 (INNSA In@522.0/Out@546.0) - 324 (AEJEA In@617.0/Out@642.
###  |  0) - 352 (OMSLL In@710.0/Out@722.0) - 229 (OMSLL In@721.0/Out@736.0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END
###  |   Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 209,  280.0 containers from 344 to 324
###  |- Vessel  6 has the path [219, 364, 334, 326, 353, 230, 328, -3]
###  |  219 (NLRTM Out@260.0) - 364 (TRMER In@411.0/Out@455.0) - 334 (EGPSD In@531.0/Out@550.0) - 326 (AEJEA In@785.0/Out@810
###  |  .0) - 353 (OMSLL In@878.0/Out@890.0) - 230 (OMSLL In@889.0/Out@904.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_
###  |  END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |   |- and carried Demand 214,  164.0 containers from 334 to 326
###  |   |- and carried Demand 215,   58.0 containers from 334 to 326
###  |- Vessel  7 has the path [221, 137, 203, 115, 267, 330, -3]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 115 (ESALG In@589.0/Out@601
###  |  .0) - 267 (TRALI In@836.0/Out@854.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |- Vessel  8 has the path [223, 366, 338, 339, 329, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 338 (EGPSD In@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973
###  |  .0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [136, 357, 335, 340, 341, 342, 331, -3]
###  |  136 (FRLEH Out@326.0) - 357 (TRAMB In@515.0/Out@563.0) - 335 (EGPSD In@621.0/Out@637.0) - 340 (EGPSD In@1035.0/Out@10
###  |  54.0) - 341 (EGPSD In@1125.0/Out@1141.0) - 342 (EGPSD In@1293.0/Out@1309.0) - 331 (AEJEA In@1530.0/Out@1563.0) - -3 (
###  |  DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 197,  113.0 containers from 342 to 331
###  |   |- and carried Demand 198,   54.0 containers from 342 to 331
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [104, 291, 202, 114, 276, -1]
###  |  104 (DKAAR Out@220.0) - 291 (MAPTM In@379.0/Out@398.0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464
###  |  .0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |- Vessel 14 has the path [105, 277, -1]
###  |  105 (DKAAR Out@388.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 275, 201, 113, 301, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 275 (ESALG In@328.0/Out@347.0) - 201 (MAPTM In@3
###  |  75.0/Out@396.0) - 113 (ESALG In@421.0/Out@433.0) - 301 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  17,  484.0 containers from 201 to 113
###  |   |- and carried Demand  18,   18.0 containers from 201 to 113
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 121,  484.0 containers from 201 to 113
###  |   |- and carried Demand 122,   18.0 containers from 201 to 113
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |
### Service 177 (WCSA) is operated by 2 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 266, 234, 0]
###  |  100 (DEHAM Out@288.0) - 266 (TRALI In@668.0/Out@686.0) - 234 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel  2 has the path [101, 273, 235, 0]
###  |  101 (DEHAM Out@456.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [4, 10, 11, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,234,0] = 1
y[1,100,266] = 1
y[1,266,234] = 1
y[2,101,273] = 1
y[2,235,0] = 1
y[2,273,235] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,229,327] = 1
y[5,327,-3] = 1
y[5,324,352] = 1
y[5,352,229] = 1
y[5,347,344] = 1
y[5,218,347] = 1
y[5,344,324] = 1
y[6,364,334] = 1
y[6,326,353] = 1
y[6,328,-3] = 1
y[6,334,326] = 1
y[6,219,364] = 1
y[6,353,230] = 1
y[6,230,328] = 1
y[7,115,267] = 1
y[7,137,203] = 1
y[7,221,137] = 1
y[7,203,115] = 1
y[7,267,330] = 1
y[7,330,-3] = 1
y[8,223,366] = 1
y[8,366,338] = 1
y[8,329,-3] = 1
y[8,339,329] = 1
y[8,338,339] = 1
y[9,200,112] = 1
y[9,301,-2] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,112,275] = 1
y[9,275,201] = 1
y[9,1,274] = 1
y[9,199,290] = 1
y[9,201,113] = 1
y[9,113,301] = 1
y[12,8,302] = 1
y[12,302,-2] = 1
y[13,104,291] = 1
y[13,291,202] = 1
y[13,276,-1] = 1
y[13,114,276] = 1
y[13,202,114] = 1
y[14,277,-1] = 1
y[14,105,277] = 1
y[15,356,325] = 1
y[15,134,356] = 1
y[15,325,-3] = 1
y[16,342,331] = 1
y[16,341,342] = 1
y[16,331,-3] = 1
y[16,335,340] = 1
y[16,340,341] = 1
y[16,357,335] = 1
y[16,136,357] = 1
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[17,201,113] = 484
xD[18,201,113] = 18
xD[25,137,203] = 68
xD[25,221,137] = 68
xD[25,203,115] = 68
xD[26,221,137] = 6
xD[29,203,115] = 60
xD[30,203,115] = 11
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[121,201,113] = 484
xD[122,201,113] = 18
xD[123,137,203] = 68
xD[123,221,137] = 68
xD[123,203,115] = 68
xD[126,203,115] = 60
xD[127,203,115] = 11
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[197,342,331] = 113
xD[198,342,331] = 54
xD[201,353,230] = 111
xD[201,230,328] = 111
xD[202,353,230] = 35
xD[202,230,328] = 35
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[207,229,327] = 288
xD[207,352,229] = 288
xD[208,229,327] = 14
xD[208,352,229] = 14
xD[209,344,324] = 280
xD[210,339,329] = 467
xD[214,334,326] = 164
xD[215,334,326] = 58
zE[112] = 284
zE[113] = 421
zE[114] = 452
zE[115] = 589
zE[137] = 447
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[229] = 721
zE[230] = 889
zE[234] = 1304
zE[235] = 1472
zE[240] = 4085
zE[266] = 668
zE[267] = 836
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[324] = 617
zE[325] = 690
zE[326] = 785
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[341] = 1125
zE[342] = 1293
zE[344] = 522
zE[347] = 467
zE[352] = 710
zE[353] = 878
zE[356] = 347
zE[357] = 515
zE[364] = 411
zE[366] = 747
zX[1] = 66
zX[8] = 570
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[104] = 220
zX[105] = 388
zX[112] = 296
zX[113] = 433
zX[114] = 464
zX[115] = 601
zX[134] = 158
zX[136] = 326
zX[137] = 463
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[223] = 596
zX[229] = 736
zX[230] = 904
zX[234] = 1316
zX[235] = 1484
zX[240] = 4085
zX[266] = 686
zX[267] = 854
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[324] = 642
zX[325] = 723
zX[326] = 810
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[341] = 1141
zX[342] = 1309
zX[344] = 546
zX[347] = 494
zX[352] = 722
zX[353] = 890
zX[356] = 395
zX[357] = 563
zX[364] = 455
zX[366] = 791
phi[1] = 1028
phi[2] = 1028
phi[3] = 227
phi[5] = 799
phi[6] = 799
phi[7] = 967
phi[8] = 631
phi[9] = 945
phi[12] = 609
phi[13] = 295
phi[14] = 295
phi[15] = 565
phi[16] = 1237

### All variables with value smaller zero:

### End

real	1444m8.491s
user	4832m49.352s
sys	933m0.908s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
