	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_1_fr.p
Instance LSFDP: LSFDRP_2_3_0_1_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_2_3_0_1_fd_43 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 370
*** |A|: 13829
*** |A^i|: 13778
*** |A^f|: 51
*** |M|: 221
*** |E|: 22

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 121084 rows, 3334484 columns and 12198130 nonzeros
Variable types: 3085505 continuous, 248979 integer (248979 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 5e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 37611 rows and 1750586 columns (presolve time = 5s) ...
Presolve removed 70591 rows and 2744110 columns (presolve time = 10s) ...
Presolve removed 78252 rows and 2757008 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 78275 rows and 2757047 columns (presolve time = 23s) ...
Presolve removed 78275 rows and 2757212 columns (presolve time = 47s) ...
Presolve removed 79572 rows and 2760420 columns (presolve time = 410s) ...
Presolve removed 79572 rows and 2760420 columns
Presolve time: 409.61s
Presolved: 41512 rows, 574064 columns, 2554575 nonzeros
Variable types: 325196 continuous, 248868 integer (248820 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 23
 AA' NZ     : 2.913e+06
 Factor NZ  : 9.213e+07 (roughly 1.0 GBytes of memory)
 Factor Ops : 5.526e+11 (roughly 30 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.09491874e+15  1.08255117e+16  1.11e+08 3.18e+06  1.43e+12   469s

Barrier performed 0 iterations in 474.41 seconds
Barrier solve interrupted - model solved by another algorithm

Concurrent spin time: 0.04s

Solved with dual simplex

Root relaxation: objective 8.087418e+06, 37351 iterations, 60.21 seconds
Total elapsed time = 475.86s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7812408.70    0  331          - 7812408.70      -     -  514s
     0     0 6943079.32    0  336          - 6943079.32      -     -  583s
H    0     0                    -5.23714e+07 6943079.32   113%     -  585s
     0     0 6793366.85    0  374 -5.237e+07 6793366.85   113%     -  594s
     0     0 6219456.61    0  414 -5.237e+07 6219456.61   112%     -  798s
H    0     0                    -4.89503e+07 6219456.61   113%     -  799s
     0     0 6218720.69    0  408 -4.895e+07 6218720.69   113%     -  803s
     0     0 6218676.29    0  412 -4.895e+07 6218676.29   113%     -  804s
     0     0 6205188.27    0  429 -4.895e+07 6205188.27   113%     -  816s
H    0     0                    -4.23765e+07 6205188.27   115%     -  817s
     0     0 6203558.77    0  423 -4.238e+07 6203558.77   115%     -  822s
     0     0 6195978.19    0  428 -4.238e+07 6195978.19   115%     -  825s
     0     0 6195914.94    0  435 -4.238e+07 6195914.94   115%     -  827s
     0     0 6178155.74    0  429 -4.238e+07 6178155.74   115%     -  837s
H    0     0                    -3.34518e+07 6178155.74   118%     -  838s
     0     0 6178129.01    0  430 -3.345e+07 6178129.01   118%     -  842s
     0     0 6177349.44    0  426 -3.345e+07 6177349.44   118%     -  844s
     0     0 6176517.50    0  428 -3.345e+07 6176517.50   118%     -  849s
     0     0 6176516.32    0  430 -3.345e+07 6176516.32   118%     -  850s
     0     0 6175650.22    0  430 -3.345e+07 6175650.22   118%     -  853s
     0     0 6175650.22    0  430 -3.345e+07 6175650.22   118%     -  858s
     0     0 6175650.22    0  430 -3.345e+07 6175650.22   118%     -  861s
     0     0 6175577.70    0  428 -3.345e+07 6175577.70   118%     -  867s
     0     0 6175577.70    0  428 -3.345e+07 6175577.70   118%     -  870s
     0     0 6175380.51    0  419 -3.345e+07 6175380.51   118%     -  873s
     0     0 6175380.51    0  419 -3.345e+07 6175380.51   118%     - 1041s
H    0     0                    -2.01949e+07 6175380.51   131%     - 1083s
     0     2 6175380.51    0  419 -2.019e+07 6175380.51   131%     - 1100s
     1     4 3861779.90    1  331 -2.019e+07 6172956.95   131% 1316327 29244s
     3     8 2368187.36    2  367 -2.019e+07 5592124.88   128% 483060 30122s
     7    10 1203044.23    3  397 -2.019e+07 5529156.40   127% 216882 30379s
    11    10 1586381.59    3  345 -2.019e+07 5529156.40   127% 140311 30996s
    15    14 1281729.28    4  354 -2.019e+07 5414854.22   127% 116326 42171s
    19    14 25901.2112    5  238 -2.019e+07 5392116.25   127% 139137 47582s
    23    18 -3167283.6    5  344 -2.019e+07 4769433.37   124% 132943 50301s
    30    21 16304.7960    6  248 -2.019e+07 4769433.37   124% 108342 50830s
H   32    23                    -1.21730e+07 4769433.37   139% 101571 50830s
H   33    22                    -7393758.628 4769433.37   165% 98709 50830s
H   34    22                    -3243703.258 4769433.37   247% 96590 50830s
    35    17 15665.7443    8  244 -3243703.3 4769433.37   247% 94275 86400s

Cutting planes:
  Implied bound: 7
  Clique: 23
  MIR: 25
  Flow cover: 2
  Zero half: 5

Explored 40 nodes (3416238 simplex iterations) in 86400.32 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: -3.2437e+06 -7.39376e+06 -1.2173e+07 ... -5.23714e+07

Time limit reached
Best objective -3.243703257725e+06, best bound 4.769433365551e+06, gap 247.0367%

***
***  |- Calculation finished, timelimit reached 86,400.37 (86400.37) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -3,243,703.2577 (-3243703.2577) and is feasible with a Gap of 2.4704
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [218, 363, 334, 335, 367, 330, -3]
###  |  218 (NLRTM Out@92.0) - 363 (TRMER In@243.0/Out@287.0) - 334 (EGPSD In@531.0/Out@550.0) - 335 (EGPSD In@621.0/Out@637.
###  |  0) - 367 (TRMER In@915.0/Out@959.0) - 330 (AEJEA In@1362.0/Out@1395.0) - -3 (DUMMY_END Ziel-Service 7)
###  |- Vessel  6 has the path [219, 135, 201, 291, 202, 114, 161, 338, 339, 329, -3]
###  |  219 (NLRTM Out@260.0) - 135 (FRLEH In@279.0/Out@295.0) - 201 (MAPTM In@375.0/Out@396.0) - 291 (MAPTM In@379.0/Out@398
###  |  .0) - 202 (MAPTM In@406.0/Out@427.0) - 114 (ESALG In@452.0/Out@464.0) - 161 (GRPIR In@696.0/Out@721.0) - 338 (EGPSD I
###  |  n@867.0/Out@886.0) - 339 (EGPSD In@957.0/Out@973.0) - 329 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Service 7
###  |  )
###  |   |- and carried Demand  13,   74.0 containers from 219 to 114
###  |   |- and carried Demand  14,    5.0 containers from 219 to 135
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 118,   74.0 containers from 219 to 114
###  |   |- and carried Demand 121,  484.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |- Vessel  7 has the path [221, 137, 203, 115, 168, 340, 342, 331, -3]
###  |  221 (NLRTM Out@428.0) - 137 (FRLEH In@447.0/Out@463.0) - 203 (MAPTM In@543.0/Out@564.0) - 115 (ESALG In@589.0/Out@601
###  |  .0) - 168 (ITGIT In@904.0/Out@923.0) - 340 (EGPSD In@1035.0/Out@1054.0) - 342 (EGPSD In@1293.0/Out@1309.0) - 331 (AEJ
###  |  EA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |   |- and carried Demand 197,  113.0 containers from 342 to 331
###  |   |- and carried Demand 198,   54.0 containers from 342 to 331
###  |- Vessel  8 has the path [223, 366, 328, -3]
###  |  223 (NLRTM Out@596.0) - 366 (TRMER In@747.0/Out@791.0) - 328 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158.0) - 356 (TRAMB In@347.0/Out@395.0) - 325 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [136, 272, 270, 266, 327, -3]
###  |  136 (FRLEH Out@326.0) - 272 (TRZMK In@617.0/Out@631.0) - 270 (TRGEM In@641.0/Out@649.0) - 266 (TRALI In@668.0/Out@686
###  |  .0) - 327 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END Ziel-Service 7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288.0) - 276 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456.0) - 277 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [102, 278, -1]
###  |  102 (DEHAM Out@624.0) - 278 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 302, -2]
###  |  1 (BEANR Out@66.0) - 274 (ESALG In@160.0/Out@179.0) - 199 (MAPTM In@207.0/Out@228.0) - 290 (MAPTM In@211.0/Out@230.0)
###  |   - 200 (MAPTM In@238.0/Out@259.0) - 112 (ESALG In@284.0/Out@296.0) - 284 (GWOXB In@463.0/Out@542.0) - 45 (CMDLA In@76
###  |  5.0/Out@811.0) - 302 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand   7,  161.0 containers from 199 to  45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to  45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to  45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |- Vessel 11 has the path [5, 315, 311, 301, -2]
###  |  5 (BEANR Out@402.0) - 315 (USORF In@873.0/Out@881.0) - 311 (USMIA In@935.0/Out@943.0) - 301 (HNPCR In@998.0/Out@1011.
###  |  0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 30 (PMax35)
###  |- Vessel 10 has the path [2, 275, 204, 234, 0]
###  |  2 (BEANR Out@234.0) - 275 (ESALG In@328.0/Out@347.0) - 204 (MAPTM In@711.0/Out@732.0) - 234 (PABLB In@1304.0/Out@1316
###  |  .0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 17 has the path [145, 365, 267, 236, 0]
###  |  145 (GBFXT Out@302.0) - 365 (TRMER In@579.0/Out@623.0) - 267 (TRALI In@836.0/Out@854.0) - 236 (PABLB In@1640.0/Out@16
###  |  52.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 18 has the path [148, 222, 273, 235, 0]
###  |  148 (GBFXT Out@470.0) - 222 (NLRTM In@500.0/Out@512.0) - 273 (TRZMK In@785.0/Out@799.0) - 235 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |
### Vessels not used in the solution: [4, 12, 13, 14]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,30] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,30,1] = 1
eta[177,30,2] = 1
eta[177,30,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,276,-1] = 1
y[1,100,276] = 1
y[2,101,277] = 1
y[2,277,-1] = 1
y[3,102,278] = 1
y[3,278,-1] = 1
y[5,218,363] = 1
y[5,335,367] = 1
y[5,367,330] = 1
y[5,334,335] = 1
y[5,330,-3] = 1
y[5,363,334] = 1
y[6,329,-3] = 1
y[6,291,202] = 1
y[6,339,329] = 1
y[6,135,201] = 1
y[6,114,161] = 1
y[6,161,338] = 1
y[6,202,114] = 1
y[6,201,291] = 1
y[6,338,339] = 1
y[6,219,135] = 1
y[7,137,203] = 1
y[7,342,331] = 1
y[7,221,137] = 1
y[7,168,340] = 1
y[7,331,-3] = 1
y[7,203,115] = 1
y[7,340,342] = 1
y[7,115,168] = 1
y[8,223,366] = 1
y[8,328,-3] = 1
y[8,366,328] = 1
y[9,200,112] = 1
y[9,274,199] = 1
y[9,290,200] = 1
y[9,1,274] = 1
y[9,45,302] = 1
y[9,199,290] = 1
y[9,302,-2] = 1
y[9,284,45] = 1
y[9,112,284] = 1
y[10,234,0] = 1
y[10,275,204] = 1
y[10,204,234] = 1
y[10,2,275] = 1
y[11,301,-2] = 1
y[11,5,315] = 1
y[11,315,311] = 1
y[11,311,301] = 1
y[15,356,325] = 1
y[15,134,356] = 1
y[15,325,-3] = 1
y[16,270,266] = 1
y[16,272,270] = 1
y[16,327,-3] = 1
y[16,266,327] = 1
y[16,136,272] = 1
y[17,365,267] = 1
y[17,145,365] = 1
y[17,267,236] = 1
y[17,236,0] = 1
y[18,148,222] = 1
y[18,235,0] = 1
y[18,222,273] = 1
y[18,273,235] = 1
xD[7,200,112] = 161
xD[7,290,200] = 161
xD[7,199,290] = 161
xD[7,284,45] = 161
xD[7,112,284] = 161
xD[8,200,112] = 580
xD[8,290,200] = 580
xD[8,199,290] = 580
xD[13,291,202] = 74
xD[13,135,201] = 74
xD[13,202,114] = 74
xD[13,201,291] = 74
xD[13,219,135] = 74
xD[14,219,135] = 5
xD[17,291,202] = 484
xD[17,202,114] = 484
xD[17,201,291] = 484
xD[18,291,202] = 18
xD[18,202,114] = 18
xD[18,201,291] = 18
xD[25,137,203] = 68
xD[25,221,137] = 68
xD[25,203,115] = 68
xD[26,221,137] = 6
xD[29,203,115] = 60
xD[30,203,115] = 11
xD[45,200,112] = 161
xD[45,284,45] = 161
xD[45,112,284] = 161
xD[46,200,112] = 580
xD[52,202,114] = 484
xD[53,202,114] = 18
xD[117,200,112] = 580
xD[117,290,200] = 580
xD[117,199,290] = 580
xD[118,291,202] = 74
xD[118,135,201] = 74
xD[118,202,114] = 74
xD[118,201,291] = 74
xD[118,219,135] = 74
xD[121,291,202] = 484
xD[121,202,114] = 484
xD[121,201,291] = 484
xD[122,291,202] = 18
xD[122,202,114] = 18
xD[122,201,291] = 18
xD[123,137,203] = 68
xD[123,221,137] = 68
xD[123,203,115] = 68
xD[126,203,115] = 60
xD[127,203,115] = 11
xD[131,200,112] = 580
xD[134,202,114] = 484
xD[135,202,114] = 18
xD[140,291,202] = 484
xD[140,202,114] = 484
xD[141,291,202] = 18
xD[141,202,114] = 18
xD[144,200,112] = 161
xD[144,290,200] = 161
xD[144,284,45] = 161
xD[144,112,284] = 161
xD[145,200,112] = 580
xD[145,290,200] = 580
xD[170,311,301] = 10
xD[171,315,311] = 75
xD[171,311,301] = 75
xD[172,315,311] = 114
xD[172,311,301] = 114
xD[197,342,331] = 113
xD[198,342,331] = 54
xD[206,339,329] = 467
xD[206,338,339] = 467
xD[210,339,329] = 467
zE[45] = 765
zE[112] = 284
zE[114] = 452
zE[115] = 589
zE[135] = 279
zE[137] = 447
zE[161] = 696
zE[168] = 904
zE[199] = 207
zE[200] = 238
zE[201] = 375
zE[202] = 406
zE[203] = 543
zE[204] = 711
zE[222] = 500
zE[234] = 1304
zE[235] = 1472
zE[236] = 1640
zE[240] = 4085
zE[266] = 668
zE[267] = 836
zE[270] = 641
zE[272] = 617
zE[273] = 785
zE[274] = 160
zE[275] = 328
zE[276] = 496
zE[277] = 664
zE[278] = 832
zE[284] = 463
zE[290] = 211
zE[291] = 379
zE[301] = 998
zE[302] = 1166
zE[311] = 935
zE[315] = 873
zE[325] = 690
zE[327] = 858
zE[328] = 1026
zE[329] = 1194
zE[330] = 1362
zE[331] = 1530
zE[334] = 531
zE[335] = 621
zE[338] = 867
zE[339] = 957
zE[340] = 1035
zE[342] = 1293
zE[356] = 347
zE[363] = 243
zE[365] = 579
zE[366] = 747
zE[367] = 915
zX[1] = 66
zX[2] = 234
zX[5] = 402
zX[45] = 811
zX[100] = 288
zX[101] = 456
zX[102] = 624
zX[112] = 296
zX[114] = 464
zX[115] = 601
zX[134] = 158
zX[135] = 295
zX[136] = 326
zX[137] = 463
zX[145] = 302
zX[148] = 470
zX[161] = 721
zX[168] = 923
zX[199] = 228
zX[200] = 259
zX[201] = 396
zX[202] = 427
zX[203] = 564
zX[204] = 732
zX[218] = 92
zX[219] = 260
zX[221] = 428
zX[222] = 512
zX[223] = 596
zX[234] = 1316
zX[235] = 1484
zX[236] = 1652
zX[240] = 4085
zX[266] = 686
zX[267] = 854
zX[270] = 649
zX[272] = 631
zX[273] = 799
zX[274] = 179
zX[275] = 347
zX[276] = 515
zX[277] = 683
zX[278] = 851
zX[284] = 542
zX[290] = 230
zX[291] = 398
zX[301] = 1011
zX[302] = 1179
zX[311] = 943
zX[315] = 881
zX[325] = 723
zX[327] = 891
zX[328] = 1059
zX[329] = 1227
zX[330] = 1395
zX[331] = 1563
zX[334] = 550
zX[335] = 637
zX[338] = 886
zX[339] = 973
zX[340] = 1054
zX[342] = 1309
zX[356] = 395
zX[363] = 287
zX[365] = 623
zX[366] = 791
zX[367] = 959
phi[1] = 227
phi[2] = 227
phi[3] = 227
phi[5] = 1303
phi[6] = 967
phi[7] = 1135
phi[8] = 463
phi[9] = 1113
phi[10] = 1082
phi[11] = 609
phi[15] = 565
phi[16] = 565
phi[17] = 1350
phi[18] = 1014

### All variables with value smaller zero:

### End

real	1444m5.137s
user	5016m50.778s
sys	749m54.941s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
