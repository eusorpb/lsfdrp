	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_1_fr.p
Instance LSFDP: LSFDRP_2_3_0_1_fd_13.p

This object contains the information regarding the instance 2_3_0_1 for a duration of 13 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.072317 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.239736 sec.
***  |   |- Create additional vessel information,				took 0.003134 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 21069 rows, 52853 columns and 369432 nonzeros
Variable types: 14545 continuous, 38308 integer (38308 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -1.61433e+07
Presolve removed 18038 rows and 32848 columns
Presolve time: 2.10s
Presolved: 3031 rows, 20005 columns, 92311 nonzeros
Found heuristic solution: objective -1.58092e+07
Variable types: 182 continuous, 19823 integer (19652 binary)

Root relaxation: objective 3.536472e+06, 3146 iterations, 0.27 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 3536472.44    0  270 -1.581e+07 3536472.44   122%     -    2s
H    0     0                    -2448345.553 3536472.44   244%     -    2s
H    0     0                    455705.61669 3536472.44   676%     -    3s
     0     0 2988387.02    0  314 455705.617 2988387.02   556%     -    3s
     0     0 2969873.07    0  317 455705.617 2969873.07   552%     -    4s
     0     0 2964491.33    0  315 455705.617 2964491.33   551%     -    4s
     0     0 2766470.18    0  288 455705.617 2766470.18   507%     -    4s
     0     0 2752072.24    0  288 455705.617 2752072.24   504%     -    4s
     0     0 2752072.24    0  280 455705.617 2752072.24   504%     -    5s
     0     2 2752072.24    0  280 455705.617 2752072.24   504%     -    5s
*  210   102              37    562310.83028 1709221.69   204%  66.5    8s
H  253    94                    601253.33980 1505455.32   150%  66.8    9s
H  255    92                    619576.65138 1502000.47   142%  66.7    9s
*  383   102              23    646381.29567 1390272.78   115%  61.6    9s
   468    94 817777.061   20  143 646381.296 1154006.52  78.5%  57.4   10s

Cutting planes:
  Gomory: 4
  Implied bound: 6
  Clique: 2
  MIR: 8
  Flow cover: 1
  Zero half: 3

Explored 1348 nodes (51424 simplex iterations) in 11.43 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: 646381 619577 601253 ... -1.58092e+07
No other solutions better than 646381

Optimal solution found (tolerance 1.00e-04)
Best objective 6.463812956753e+05, best bound 6.463812956753e+05, gap 0.0000%
Took  13.4004385471344
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 16899 rows, 15177 columns and 54174 nonzeros
Variable types: 14004 continuous, 1173 integer (1173 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 16899 rows and 15177 columns
Presolve time: 0.02s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.03 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 159247 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.592465664764e+05, best bound 1.592465664764e+05, gap 0.0000%

### Instance
### LSFDRP_2_3_0_1_fd_13

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 66
### |A|: 62
### |A^i|: 60
### |A^f|: 2
### |M|: 221
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is 159,246.5665 (159246.5665)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 363, 325, -3]
###  |  218 (NLRTM Out@92) - 363 (TRMER In@243/Out@287) - 325 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  6 has the path [219, 366, 338, 339, 329, -3]
###  |  219 (NLRTM Out@260) - 366 (TRMER In@747/Out@791) - 338 (EGPSD In@867/Out@886) - 339 (EGPSD In@957/Out@973) - 329 (AEJ
###  |  EA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
###  |- Vessel  7 has the path [221, 137, 203, 115, 367, 330, -3]
###  |  221 (NLRTM Out@428) - 137 (FRLEH In@447/Out@463) - 203 (MAPTM In@543/Out@564) - 115 (ESALG In@589/Out@601) - 367 (TRM
###  |  ER In@915/Out@959) - 330 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |
###  |- Vessel  8 has the path [223, 368, 331, -3]
###  |  223 (NLRTM Out@596) - 368 (TRMER In@1083/Out@1127) - 331 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [134, 364, 334, 352, 229, 327, -3]
###  |  134 (FRLEH Out@158) - 364 (TRMER In@411/Out@455) - 334 (EGPSD In@531/Out@550) - 352 (OMSLL In@710/Out@722) - 229 (OMS
###  |  LL In@721/Out@736) - 327 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 214,  164.0 containers from 334 to 327
###  |   |- and carried Demand 215,   58.0 containers from 334 to 327
###  |
###  |- Vessel 16 has the path [136, 365, 353, 230, 328, -3]
###  |  136 (FRLEH Out@326) - 365 (TRMER In@579/Out@623) - 353 (OMSLL In@878/Out@890) - 230 (OMSLL In@889/Out@904) - 328 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 275, 201, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAP
###  |  TM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  484.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 278
###  |   |- and carried Demand 120,   10.0 containers from 201 to 278
###  |   |- and carried Demand 121,  312.0 containers from 201 to 114
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570) - 302 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [1, 274, 199, 290, 200, 112, 284, 45, 36, 240, 236, 0]
###  |  1 (BEANR Out@66) - 274 (ESALG In@160/Out@179) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPTM 
###  |  In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 45 (CMDLA In@765/Out@811) - 36 (CLLQN In@
###  |  1240/Out@1258) - 240 (PECLL In@1403/Out@1510) - 236 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   7,  161.0 containers from 199 to 45
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 45
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand  80,  194.0 containers from 36 to 236
###  |   |- and carried Demand  81,   20.0 containers from 36 to 236
###  |   |- and carried Demand  83,    5.0 containers from 36 to 236
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 45
###  |   |- and carried Demand 145,  580.0 containers from 290 to 112
###  |
###  |- Vessel 11 has the path [5, 267, 235, 0]
###  |  5 (BEANR Out@402) - 267 (TRALI In@836/Out@854) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 17 has the path [145, 266, 234, 0]
###  |  145 (GBFXT Out@302) - 266 (TRALI In@668/Out@686) - 234 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 4, 10, 14]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m16.105s
user	0m38.484s
sys	0m2.076s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
