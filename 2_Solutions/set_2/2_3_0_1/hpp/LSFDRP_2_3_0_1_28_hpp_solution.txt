	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_2_3_0_1_fr.p
Instance LSFDP: LSFDRP_2_3_0_1_fd_28.p

This object contains the information regarding the instance 2_3_0_1 for a duration of 28 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.069926 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.268603 sec.
***  |   |- Create additional vessel information,				took  0.00367 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 19069 rows, 44645 columns and 303703 nonzeros
Variable types: 12793 continuous, 31852 integer (31852 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 16634 rows and 30304 columns
Presolve time: 1.67s
Presolved: 2435 rows, 14341 columns, 65482 nonzeros
Variable types: 138 continuous, 14203 integer (14081 binary)

Root relaxation: objective 1.603647e+06, 2280 iterations, 0.17 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1603646.72    0  224          - 1603646.72      -     -    2s
H    0     0                    -9178933.823 1603646.72   117%     -    2s
H    0     0                    -2676845.478 1603646.72   160%     -    2s
H    0     0                    -217255.0350 1603646.72   838%     -    2s
     0     0 1257222.00    0  266 -217255.03 1257222.00   679%     -    2s
     0     0 1212041.69    0  288 -217255.03 1212041.69   658%     -    3s
     0     0 1118384.55    0  255 -217255.03 1118384.55   615%     -    3s
     0     0 1098897.81    0  275 -217255.03 1098897.81   606%     -    3s
     0     0 1097324.01    0  259 -217255.03 1097324.01   605%     -    3s
     0     0 1044762.67    0  252 -217255.03 1044762.67   581%     -    3s
     0     0 1040100.34    0  228 -217255.03 1040100.34   579%     -    3s
     0     0 984771.687    0  252 -217255.03 984771.687   553%     -    3s
     0     0 983299.112    0  251 -217255.03 983299.112   553%     -    3s
     0     0 982475.434    0  263 -217255.03 982475.434   552%     -    3s
     0     0 982475.434    0  263 -217255.03 982475.434   552%     -    4s
     0     0 982475.434    0  221 -217255.03 982475.434   552%     -    4s
     0     0 982475.434    0  254 -217255.03 982475.434   552%     -    5s
     0     0 982475.434    0  264 -217255.03 982475.434   552%     -    5s
     0     0 982475.434    0  253 -217255.03 982475.434   552%     -    5s
     0     0 975210.359    0  248 -217255.03 975210.359   549%     -    5s
     0     0 862351.826    0  239 -217255.03 862351.826   497%     -    5s
     0     0 858792.466    0  249 -217255.03 858792.466   495%     -    5s
     0     0 858792.466    0  249 -217255.03 858792.466   495%     -    6s
     0     2 858792.466    0  246 -217255.03 858792.466   495%     -    6s
*   66    15              13    -217254.9011 512580.191   336%  59.1    6s
H   75    18                    -212470.0183 485941.404   329%  57.2    7s
*  123     4              10    -212469.9194 88061.7851   141%  51.2    7s

Cutting planes:
  Gomory: 5
  Implied bound: 3
  Clique: 1
  MIR: 7
  Flow cover: 3
  Zero half: 8

Explored 139 nodes (12789 simplex iterations) in 7.57 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -212470 -212470 -217255 ... -9.17893e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -2.124700183280e+05, best bound -2.124700183280e+05, gap 0.0000%
Took  9.401206731796265
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 16780 rows, 15141 columns and 53786 nonzeros
Variable types: 13968 continuous, 1173 integer (1173 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 16780 rows and 15141 columns
Presolve time: 0.04s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.06 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -403883 

Optimal solution found (tolerance 1.00e-04)
Best objective -4.038833350123e+05, best bound -4.038833350123e+05, gap 0.0000%

### Instance
### LSFDRP_2_3_0_1_fd_28

### Graph info
### |S|: 18
### |S^E|: 0
### |S^C|: 0
### |V|: 66
### |A|: 62
### |A^i|: 62
### |A^f|: 0
### |M|: 221
### |E|: 22
#############################################################################################################################
###
### Results
###
### The objective of the solution is -403,883.335 (-403883.335)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [218, 133, 199, 290, 200, 112, 284, 46, 368, 331, -3]
###  |  218 (NLRTM Out@92) - 133 (FRLEH In@111/Out@127) - 199 (MAPTM In@207/Out@228) - 290 (MAPTM In@211/Out@230) - 200 (MAPT
###  |  M In@238/Out@259) - 112 (ESALG In@284/Out@296) - 284 (GWOXB In@463/Out@542) - 46 (CMDLA In@796/Out@842) - 368 (TRMER 
###  |  In@1083/Out@1127) - 331 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   6,   17.0 containers from 218 to 133
###  |   |- and carried Demand   7,  161.0 containers from 199 to 46
###  |   |- and carried Demand   8,  580.0 containers from 199 to 112
###  |   |- and carried Demand  45,  161.0 containers from 200 to 46
###  |   |- and carried Demand  46,  580.0 containers from 200 to 112
###  |   |- and carried Demand 117,  580.0 containers from 199 to 112
###  |   |- and carried Demand 131,  580.0 containers from 200 to 112
###  |   |- and carried Demand 144,  161.0 containers from 290 to 46
###  |   |- and carried Demand 145,   19.0 containers from 290 to 112
###  |
###  |- Vessel  6 has the path [219, 364, 334, 352, 229, 327, -3]
###  |  219 (NLRTM Out@260) - 364 (TRMER In@411/Out@455) - 334 (EGPSD In@531/Out@550) - 352 (OMSLL In@710/Out@722) - 229 (OMS
###  |  LL In@721/Out@736) - 327 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 207,  288.0 containers from 352 to 327
###  |   |- and carried Demand 208,   14.0 containers from 352 to 327
###  |   |- and carried Demand 214,  164.0 containers from 334 to 327
###  |   |- and carried Demand 215,   58.0 containers from 334 to 327
###  |
###  |- Vessel  7 has the path [221, 137, 203, 115, 273, 338, 339, 329, -3]
###  |  221 (NLRTM Out@428) - 137 (FRLEH In@447/Out@463) - 203 (MAPTM In@543/Out@564) - 115 (ESALG In@589/Out@601) - 273 (TRZ
###  |  MK In@785/Out@799) - 338 (EGPSD In@867/Out@886) - 339 (EGPSD In@957/Out@973) - 329 (AEJEA In@1194/Out@1227) - -3 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand  25,   68.0 containers from 221 to 115
###  |   |- and carried Demand  26,    6.0 containers from 221 to 137
###  |   |- and carried Demand  29,   60.0 containers from 203 to 115
###  |   |- and carried Demand  30,   11.0 containers from 203 to 115
###  |   |- and carried Demand 123,   68.0 containers from 221 to 115
###  |   |- and carried Demand 126,   60.0 containers from 203 to 115
###  |   |- and carried Demand 127,   11.0 containers from 203 to 115
###  |   |- and carried Demand 206,  467.0 containers from 338 to 329
###  |   |- and carried Demand 210,  467.0 containers from 339 to 329
###  |
###  |- Vessel  8 has the path [223, 367, 330, -3]
###  |  223 (NLRTM Out@596) - 367 (TRMER In@915/Out@959) - 330 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [134, 356, 325, -3]
###  |  134 (FRLEH Out@158) - 356 (TRAMB In@347/Out@395) - 325 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 16 has the path [136, 272, 353, 230, 328, -3]
###  |  136 (FRLEH Out@326) - 272 (TRZMK In@617/Out@631) - 353 (OMSLL In@878/Out@890) - 230 (OMSLL In@889/Out@904) - 328 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 201,  111.0 containers from 353 to 328
###  |   |- and carried Demand 202,   35.0 containers from 353 to 328
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [100, 276, -1]
###  |  100 (DEHAM Out@288) - 276 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [101, 277, -1]
###  |  101 (DEHAM Out@456) - 277 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [104, 275, 201, 291, 202, 114, 278, -1]
###  |  104 (DKAAR Out@220) - 275 (ESALG In@328/Out@347) - 201 (MAPTM In@375/Out@396) - 291 (MAPTM In@379/Out@398) - 202 (MAP
###  |  TM In@406/Out@427) - 114 (ESALG In@452/Out@464) - 278 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  17,  484.0 containers from 201 to 114
###  |   |- and carried Demand  18,   18.0 containers from 201 to 114
###  |   |- and carried Demand  52,  312.0 containers from 202 to 114
###  |   |- and carried Demand  53,   18.0 containers from 202 to 114
###  |   |- and carried Demand 119,  214.0 containers from 201 to 278
###  |   |- and carried Demand 120,   10.0 containers from 201 to 278
###  |   |- and carried Demand 122,   18.0 containers from 201 to 114
###  |   |- and carried Demand 132,  214.0 containers from 202 to 278
###  |   |- and carried Demand 133,   10.0 containers from 202 to 278
###  |   |- and carried Demand 134,  484.0 containers from 202 to 114
###  |   |- and carried Demand 135,   18.0 containers from 202 to 114
###  |   |- and carried Demand 138,  214.0 containers from 291 to 278
###  |   |- and carried Demand 139,   10.0 containers from 291 to 278
###  |   |- and carried Demand 140,  484.0 containers from 291 to 114
###  |   |- and carried Demand 141,   18.0 containers from 291 to 114
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 12 has the path [8, 302, -2]
###  |  8 (BEANR Out@570) - 302 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 18 has the path [148, 315, 311, 301, -2]
###  |  148 (GBFXT Out@470) - 315 (USORF In@873/Out@881) - 311 (USMIA In@935/Out@943) - 301 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 170,   10.0 containers from 311 to 301
###  |   |- and carried Demand 171,   75.0 containers from 315 to 301
###  |   |- and carried Demand 172,  114.0 containers from 315 to 301
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [102, 366, 267, 236, 0]
###  |  102 (DEHAM Out@624) - 366 (TRMER In@747/Out@791) - 267 (TRALI In@836/Out@854) - 236 (PABLB In@1640/Out@1652) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [103, 235, 0]
###  |  103 (DEHAM Out@792) - 235 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [105, 365, 266, 234, 0]
###  |  105 (DKAAR Out@388) - 365 (TRMER In@579/Out@623) - 266 (TRALI In@668/Out@686) - 234 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [9, 10, 11, 17]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m12.528s
user	0m23.468s
sys	0m1.795s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
