	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_0_fr.p
Instance LSFDP: LSFDRP_3_4_1_0_fd_31.p

This object contains the information regarding the instance 3_4_1_0 for a duration of 31 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took  0.60367 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.552591 sec.
***  |   |- Create additional vessel information,				took 0.008628 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 52715 rows, 163370 columns and 1318301 nonzeros
Variable types: 37299 continuous, 126071 integer (126071 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 43239 rows and 96401 columns
Presolve time: 3.99s
Presolved: 9476 rows, 66969 columns, 317387 nonzeros
Variable types: 613 continuous, 66356 integer (65651 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
    7761    2.0846447e+07   4.126160e+03   0.000000e+00      5s
   13793    1.3262789e+07   0.000000e+00   0.000000e+00      8s

Root relaxation: objective 1.326279e+07, 13793 iterations, 3.81 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2437e+07    0 1082          - 1.2437e+07      -     -    9s
H    0     0                    -3670933.433 1.2437e+07   439%     -   10s
H    0     0                    -1031172.615 1.2437e+07  1306%     -   10s
H    0     0                    3923683.5545 1.2437e+07   217%     -   22s
     0     0 1.1092e+07    0  918 3923683.55 1.1092e+07   183%     -   25s
     0     0 1.1089e+07    0  888 3923683.55 1.1089e+07   183%     -   25s
     0     0 1.0694e+07    0  916 3923683.55 1.0694e+07   173%     -   27s
     0     0 1.0662e+07    0  875 3923683.55 1.0662e+07   172%     -   28s
     0     0 1.0662e+07    0  879 3923683.55 1.0662e+07   172%     -   28s
     0     0 1.0645e+07    0  903 3923683.55 1.0645e+07   171%     -   28s
     0     0 1.0644e+07    0  907 3923683.55 1.0644e+07   171%     -   29s
     0     0 1.0407e+07    0  889 3923683.55 1.0407e+07   165%     -   29s
     0     0 9890115.90    0  664 3923683.55 9890115.90   152%     -   30s
     0     0 9865705.86    0  638 3923683.55 9865705.86   151%     -   30s
     0     0 9673311.18    0  592 3923683.55 9673311.18   147%     -   31s
     0     0 9654658.72    0  600 3923683.55 9654658.72   146%     -   32s
     0     0 9654182.19    0  613 3923683.55 9654182.19   146%     -   32s
     0     0 9534541.22    0  617 3923683.55 9534541.22   143%     -   32s
     0     0 9527850.89    0  592 3923683.55 9527850.89   143%     -   33s
     0     0 9527834.01    0  596 3923683.55 9527834.01   143%     -   33s
     0     0 9510870.39    0  628 3923683.55 9510870.39   142%     -   33s
     0     0 9510631.76    0  627 3923683.55 9510631.76   142%     -   34s
     0     0 9483269.00    0  665 3923683.55 9483269.00   142%     -   34s
     0     0 9483172.21    0  655 3923683.55 9483172.21   142%     -   34s
     0     0 9471779.20    0  631 3923683.55 9471779.20   141%     -   35s
     0     0 9460416.74    0  631 3923683.55 9460416.74   141%     -   35s
     0     0 9459467.48    0  640 3923683.55 9459467.48   141%     -   35s
     0     0 9453951.59    0  636 3923683.55 9453951.59   141%     -   36s
     0     0 9453951.59    0  637 3923683.55 9453951.59   141%     -   36s
     0     0 9453946.96    0  643 3923683.55 9453946.96   141%     -   36s
     0     0 9453946.96    0  642 3923683.55 9453946.96   141%     -   41s
     0     2 9453946.96    0  641 3923683.55 9453946.96   141%     -   46s
     1     4 9077951.83    1  630 3923683.55 9077951.83   131%   506   51s
    11    16 8427910.38    4  552 3923683.55 8975257.78   129%   357   55s
    33    34 8013505.64    9  503 3923683.55 8975257.78   129%   339   60s
    82    84 6180929.65   18  643 3923683.55 8975257.78   129%   299   65s
   146   144 5508985.62   27  570 3923683.55 8975257.78   129%   257   70s
   296   250 3981013.84   56  277 3923683.55 8427418.64   115%   183   75s
   394   332 6054276.26   14  416 3923683.55 8427418.64   115%   198   80s
   528   427 4630766.36   24  369 3923683.55 8427418.64   115%   194   85s
H  711   508                    4100394.1989 8228694.69   101%   180   99s
H  712   483                    4158701.8208 8228694.69  97.9%   180   99s
H  714   481                    4166081.8552 8228694.69  97.5%   180   99s
   715   475 6763339.26   12  540 4166081.86 8228694.69  97.5%   180  100s
   861   543 5581015.32   13  295 4166081.86 7556496.73  81.4%   184  106s
*  987   605              19    4173584.9188 7527166.07  80.4%   179  109s
  1065   640 6945385.05    7  642 4173584.92 7527166.07  80.4%   173  134s
H 1066   608                    4173584.9438 7527166.07  80.4%   173  148s
  1068   609 4423649.61   37  598 4173584.94 7527166.07  80.4%   172  153s
  1070   610 4225956.24   40  505 4173584.94 7527166.07  80.4%   172  155s
  1071   611 5073922.00   25  598 4173584.94 7366598.90  76.5%   172  160s
  1075   614 6910711.37    7  627 4173584.94 6910711.37  65.6%   171  167s
  1077   615 6666267.09    6  693 4173584.94 6666267.09  59.7%   171  173s
  1078   616 5833103.37   19  752 4173584.94 6663007.22  59.6%   171  176s
  1079   616 4567191.66   26  651 4173584.94 6512168.54  56.0%   170  180s
  1081   618 4437653.68   24  680 4173584.94 6492669.66  55.6%   170  185s
  1083   619 4892301.98   38  671 4173584.94 6489627.90  55.5%   170  204s
  1084   620 6489627.90    5  671 4173584.94 6489627.90  55.5%   170  207s
H 1084   589                    4372855.8732 6489627.90  48.4%   170  239s
  1086   593 6340496.01   14  725 4372855.87 6456087.79  47.6%   192  300s
  1088   594 5906123.66   15  683 4372855.87 6062892.56  38.6%   195  307s
  1092   597 5591740.86   16  584 4372855.87 5903675.29  35.0%   197  310s
  1100   602 5301406.41   17  596 4372855.87 5576637.49  27.5%   200  315s
  1108   607 5371586.88   18  592 4372855.87 5576637.49  27.5%   202  320s
  1141   611 4918878.81   21  496 4372855.87 5576637.49  27.5%   207  325s
  1215   594     cutoff   30      4372855.87 5576637.49  27.5%   201  330s
  1289   585 4587582.23   23  459 4372855.87 5458640.27  24.8%   199  335s
  1373   581 4733402.32   24  517 4372855.87 5425248.08  24.1%   197  340s
  1486   579 5024627.78   20  463 4372855.87 5336299.09  22.0%   198  345s
  1580   573 4487810.03   29  590 4372855.87 5303813.01  21.3%   200  350s
  1704   566 4884169.37   20  472 4372855.87 5226359.28  19.5%   199  355s
  1860   549 4643551.41   25  496 4372855.87 5137391.20  17.5%   200  360s
H 1914   506                    4372856.0171 5134293.45  17.4%   201  362s
H 1946   479                    4382599.2780 5129138.31  17.0%   200  362s
  1964   472     cutoff   24      4382599.28 5129138.31  17.0%   200  385s
H 1975   442                    4382599.4622 5129138.31  17.0%   199  385s
  2010   438     cutoff   31      4382599.46 5106479.66  16.5%   199  397s
  2022   426 4668414.76   24  474 4382599.46 5105392.92  16.5%   199  400s
  2241   351 4715886.20   22  358 4382599.46 5004054.02  14.2%   199  406s
  2385   303 4685127.87   26  609 4382599.46 4948074.37  12.9%   200  411s
  2583   216     cutoff   27      4382599.46 4897289.47  11.7%   198  415s
  2796   182     cutoff   21      4382599.46 4801056.83  9.55%   196  425s
  2966   106 4397540.63   29  626 4382599.46 4743500.86  8.23%   193  430s

Cutting planes:
  Gomory: 13
  Implied bound: 17
  Projected implied bound: 30
  Clique: 16
  MIR: 35
  StrongCG: 1
  Flow cover: 52
  Zero half: 38

Explored 3321 nodes (627641 simplex iterations) in 432.77 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 4.3826e+06 4.3826e+06 4.37286e+06 ... 3.92368e+06
No other solutions better than 4.3826e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 4.382599670206e+06, best bound 4.382599670206e+06, gap 0.0000%
Took  440.20647859573364
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 72691 rows, 68241 columns and 231070 nonzeros
Variable types: 65451 continuous, 2790 integer (2790 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 72691 rows and 68241 columns
Presolve time: 0.12s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.17 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -5.46327e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -5.463273316779e+06, best bound -5.463273316779e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_0_fd_31

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 118
### |A|: 113
### |A^i|: 113
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -5,463,273.3168 (-5463273.3168)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 400, 261, 147, 394, 68, 480, 441, -3]
###  |  282 (NLRTM Out@92) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOX
###  |  B In@463/Out@542) - 68 (CMDLA In@796/Out@842) - 480 (TRMER In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUM
###  |  MY_END Ziel-Service 7)
###  |   |- and carried Demand   8,   42.0 containers from 282 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  68
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 256,   42.0 containers from 282 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  68
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 444, 464, 295, 437, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 444 (EGPSD In@531/Out@550) - 464 (OMSLL In@710/Out@722) - 295 (OMS
###  |  LL In@721/Out@736) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 365,  288.0 containers from 464 to 437
###  |   |- and carried Demand 366,   14.0 containers from 464 to 437
###  |   |- and carried Demand 372,  164.0 containers from 444 to 437
###  |   |- and carried Demand 373,   58.0 containers from 444 to 437
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 479, 440, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 479 (TRM
###  |  ER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |
###  |- Vessel  8 has the path [287, 478, 448, 449, 369, 439, -3]
###  |  287 (NLRTM Out@596) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRU
###  |  SK In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 15 has the path [179, 468, 435, -3]
###  |  179 (FRLEH Out@158) - 468 (TRAMB In@347/Out@395) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 16 has the path [181, 477, 465, 296, 438, -3]
###  |  181 (FRLEH Out@326) - 477 (TRMER In@579/Out@623) - 465 (OMSLL In@878/Out@890) - 296 (OMSLL In@889/Out@904) - 438 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 359,  111.0 containers from 465 to 438
###  |   |- and carried Demand 360,   35.0 containers from 465 to 438
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 387, -1]
###  |  131 (DEHAM Out@288) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 386, -1]
###  |  135 (DKAAR Out@220) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  312.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 556, 572, 563, 372, 485, 545, 297, 488, 548, 525, -4]
###  |  9 (BEANR Out@66) - 556 (USCHS In@378/Out@388) - 572 (USORF In@413/Out@426) - 563 (USEWR In@450/Out@466) - 372 (TRZMK 
###  |  In@785/Out@799) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 297 (OMSLL In@1132/Out@1145) - 488 (AEJE
###  |  A In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |   |- and carried Demand 547,  339.0 containers from 572 to 485
###  |   |- and carried Demand 548,  115.0 containers from 572 to 485
###  |   |- and carried Demand 549,    3.0 containers from 572 to 297
###  |   |- and carried Demand 550,    2.0 containers from 572 to 545
###  |   |- and carried Demand 551,  194.0 containers from 556 to 485
###  |   |- and carried Demand 552,   49.0 containers from 556 to 485
###  |   |- and carried Demand 553,   40.0 containers from 556 to 545
###  |
###  |- Vessel 10 has the path [10, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  10 (BEANR Out@234) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAIL
###  |  K In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 (
###  |  DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 11 has the path [13, 360, 521, -4]
###  |  13 (BEANR Out@402) - 360 (TRALI In@668/Out@686) - 521 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 12 has the path [16, 368, 522, -4]
###  |  16 (BEANR Out@570) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 17 has the path [190, 362, 484, 544, 217, 487, 547, 524, -4]
###  |  190 (GBFXT Out@302) - 362 (TRAMB In@576/Out@603) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 217 (INP
###  |  AV In@1114/Out@1126) - 487 (AEJEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 555,  244.0 containers from 544 to 524
###  |   |- and carried Demand 556,   67.0 containers from 544 to 524
###  |   |- and carried Demand 558,  118.0 containers from 484 to 524
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |   |- and carried Demand 561,   15.0 containers from 484 to 524
###  |   |- and carried Demand 562,  267.0 containers from 484 to 524
###  |   |- and carried Demand 563,    1.0 containers from 484 to 524
###  |
###  |- Vessel 23 has the path [375, 520, -4]
###  |  375 (UYMVD Out@216) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 18 has the path [193, 425, 421, 411, -2]
###  |  193 (GBFXT Out@470) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 361, 303, 0]
###  |  132 (DEHAM Out@456) - 361 (TRALI In@836/Out@854) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 370, 302, 0]
###  |  136 (DKAAR Out@388) - 370 (TRZMK In@617/Out@631) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
###  |
### Vessels not used in the solution: [3, 4, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	7m30.496s
user	25m36.689s
sys	2m13.649s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
