	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_0_fr.p
Instance LSFDP: LSFDRP_3_4_1_0_fd_25.p

This object contains the information regarding the instance 3_4_1_0 for a duration of 25 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.479705 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.500542 sec.
***  |   |- Create additional vessel information,				took  0.00929 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 53115 rows, 165210 columns and 1330634 nonzeros
Variable types: 37822 continuous, 127388 integer (127388 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 42837 rows and 95107 columns (presolve time = 7s) ...
Presolve removed 43693 rows and 95226 columns
Presolve time: 7.99s
Presolved: 9422 rows, 69984 columns, 330350 nonzeros
Variable types: 563 continuous, 69421 integer (68865 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.0459770e+08   3.216703e+04   0.000000e+00      8s
    9822    1.6588689e+07   4.367110e+03   0.000000e+00     10s
   13531    1.3066169e+07   0.000000e+00   0.000000e+00     12s

Root relaxation: objective 1.306617e+07, 13531 iterations, 3.81 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2372e+07    0 1108          - 1.2372e+07      -     -   13s
H    0     0                    -3462576.057 1.2372e+07   457%     -   14s
H    0     0                    3432094.7892 1.2372e+07   260%     -   25s
     0     0 1.0196e+07    0  701 3432094.79 1.0196e+07   197%     -   27s
     0     0 1.0156e+07    0  711 3432094.79 1.0156e+07   196%     -   28s
     0     0 9272134.53    0  633 3432094.79 9272134.53   170%     -   29s
H    0     0                    3457967.1111 9272134.53   168%     -   30s
     0     0 9148774.67    0  738 3457967.11 9148774.67   165%     -   30s
     0     0 9126882.31    0  667 3457967.11 9126882.31   164%     -   30s
     0     0 9107283.79    0  632 3457967.11 9107283.79   163%     -   30s
     0     0 9094026.80    0  633 3457967.11 9094026.80   163%     -   30s
     0     0 9086641.89    0  642 3457967.11 9086641.89   163%     -   31s
     0     0 8996213.50    0  679 3457967.11 8996213.50   160%     -   31s
     0     0 8994681.90    0  662 3457967.11 8994681.90   160%     -   31s
     0     0 8962964.14    0  655 3457967.11 8962964.14   159%     -   32s
     0     0 8941282.20    0  585 3457967.11 8941282.20   159%     -   32s
     0     0 8939231.38    0  585 3457967.11 8939231.38   159%     -   33s
     0     0 8937908.83    0  578 3457967.11 8937908.83   158%     -   33s
     0     0 8923919.16    0  603 3457967.11 8923919.16   158%     -   33s
     0     0 8923792.32    0  603 3457967.11 8923792.32   158%     -   33s
     0     0 8922410.28    0  625 3457967.11 8922410.28   158%     -   33s
     0     0 8919761.04    0  589 3457967.11 8919761.04   158%     -   34s
     0     0 8919319.90    0  593 3457967.11 8919319.90   158%     -   34s
     0     0 8916480.31    0  637 3457967.11 8916480.31   158%     -   35s
     0     0 8916480.31    0  635 3457967.11 8916480.31   158%     -   39s
H    0     0                    3642785.2297 8916480.31   145%     -   45s
     0     2 8916480.31    0  633 3642785.23 8916480.31   145%     -   45s
    11    16 7439014.59    3  560 3642785.23 8035406.87   121%   425   51s
    34    35 6733748.88    9  604 3642785.23 8035406.87   121%   339   56s
    65    66 6197064.97   15  734 3642785.23 8035406.87   121%   297   61s
    88    87 5861987.27   19  670 3642785.23 8035406.87   121%   287   65s
   239   201 4277315.16   41  549 3642785.23 8035406.87   121%   170   70s
   356   282 6179893.01   10  688 3642785.23 7939977.11   118%   166   75s
   394   311 5125391.52   14  686 3642785.23 7939977.11   118%   165   83s
H  395   252                    3995953.7872 7939977.11  98.7%   165   83s
H  396   191                    4486659.5449 7939977.11  77.0%   165   83s
H  400   190                    4505651.3252 7939977.11  76.2%   166   83s
   427   199     cutoff   20      4505651.33 7248300.65  60.9%   167   85s
   564   259 5350771.20   22  668 4505651.33 7130038.24  58.2%   170   90s
   697   333 6249001.37    8  590 4505651.33 6884184.46  52.8%   173   95s
   841   419 5327883.28    5  493 4505651.33 6818354.90  51.3%   175  100s
   969   482 4950885.77   17  519 4505651.33 6777591.21  50.4%   177  105s
H 1027   512                    4505652.4349 6733607.29  49.4%   176  105s
  1028   513 5563390.35   10  635 4505652.43 6733607.29  49.4%   176  142s
H 1029   488                    4505653.2886 6733607.29  49.4%   176  157s
  1031   489 4805610.48   11  683 4505653.29 6733607.29  49.4%   175  164s
  1032   490 4767539.14   15  631 4505653.29 6733607.29  49.4%   175  165s
  1035   492 5049323.51   27  567 4505653.29 6733607.29  49.4%   175  171s
  1037   493 6391861.05   11  624 4505653.29 6733607.29  49.4%   174  177s
  1039   494 5074719.00   14  670 4505653.29 6676283.94  48.2%   174  186s
  1041   496 6628391.75    6  557 4505653.29 6628391.75  47.1%   174  193s
  1042   496 5933144.87   11  574 4505653.29 6623049.36  47.0%   173  195s
  1043   497 5704621.47   14  659 4505653.29 6529153.12  44.9%   173  201s
  1045   498 5346432.93   17  627 4505653.29 6509623.20  44.5%   173  223s
  1046   499 5060631.73   18  627 4505653.29 6509623.20  44.5%   173  225s
  1047   500 4877570.34   29  627 4505653.29 6509623.20  44.5%   173  255s
  1048   503 6237700.15   15  574 4505653.29 6452663.81  43.2%   196  310s
  1050   507 6079486.87   16  608 4505653.29 6114442.47  35.7%   198  320s
  1054   509 5897371.37   17  666 4505653.29 6077936.09  34.9%   200  325s
  1066   513 5531591.54   18  378 4505653.29 5887644.55  30.7%   203  330s
  1108   504 4673940.33   22  173 4505653.29 5887644.55  30.7%   205  335s
  1148   508 5100583.37   21  528 4505653.29 5542070.01  23.0%   208  340s
  1219   494 5278633.16   20  338 4505653.29 5459460.09  21.2%   206  345s
  1324   498 4829509.52   23  364 4505653.29 5434121.65  20.6%   203  350s
  1441   484 4983014.20   24  390 4505653.29 5318772.06  18.0%   202  355s
  1579   474 4535744.16   25  563 4505653.29 5172538.08  14.8%   201  360s
  1736   419 4554231.42   28  637 4505653.29 5081175.98  12.8%   200  365s
  1988   265     cutoff   24      4505653.29 4872947.68  8.15%   193  371s
* 2147   137              35    4527565.3584 4859805.05  7.34%   186  372s

Cutting planes:
  Gomory: 20
  Implied bound: 15
  Projected implied bound: 30
  Clique: 15
  MIR: 39
  Flow cover: 37
  Zero half: 42

Explored 2255 nodes (423875 simplex iterations) in 373.42 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 4.52757e+06 4.50565e+06 4.50565e+06 ... -3.46258e+06
No other solutions better than 4.52757e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 4.527565358447e+06, best bound 4.527565358447e+06, gap 0.0000%
Took  380.39892506599426
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 72691 rows, 68241 columns and 231057 nonzeros
Variable types: 65451 continuous, 2790 integer (2790 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 72691 rows and 68241 columns
Presolve time: 0.10s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.13 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -4.47105e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -4.471054476073e+06, best bound -4.471054476073e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_0_fd_25

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 118
### |A|: 113
### |A^i|: 113
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -4,471,054.4761 (-4471054.4761)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 480, 441, -3]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 480 (TRMER 
###  |  In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to  67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  67
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,   19.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 435, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 438, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |
###  |- Vessel 15 has the path [179, 557, 573, 565, 374, 486, 546, 349, 440, -3]
###  |  179 (FRLEH Out@158) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAI
###  |  LK In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 349 (PKBQM In@1218/Out@1246) - 440
###  |   (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 16 has the path [181, 478, 448, 449, 369, 439, -3]
###  |  181 (FRLEH Out@326) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRU
###  |  SK In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 21 has the path [382, 464, 295, 437, -3]
###  |  382 (ZAPLZ Out@205) - 464 (OMSLL In@710/Out@722) - 295 (OMSLL In@721/Out@736) - 437 (AEJEA In@858/Out@891) - -3 (DUMM
###  |  Y_END Ziel-Service 7)
###  |   |- and carried Demand 365,  288.0 containers from 464 to 437
###  |   |- and carried Demand 366,   14.0 containers from 464 to 437
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPT
###  |  M In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 556, 572, 563, 372, 485, 545, 297, 488, 548, 525, -4]
###  |  9 (BEANR Out@66) - 556 (USCHS In@378/Out@388) - 572 (USORF In@413/Out@426) - 563 (USEWR In@450/Out@466) - 372 (TRZMK 
###  |  In@785/Out@799) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 297 (OMSLL In@1132/Out@1145) - 488 (AEJE
###  |  A In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |   |- and carried Demand 547,  339.0 containers from 572 to 485
###  |   |- and carried Demand 548,  115.0 containers from 572 to 485
###  |   |- and carried Demand 549,    3.0 containers from 572 to 297
###  |   |- and carried Demand 550,    2.0 containers from 572 to 545
###  |   |- and carried Demand 551,  194.0 containers from 556 to 485
###  |   |- and carried Demand 552,   49.0 containers from 556 to 485
###  |   |- and carried Demand 553,   40.0 containers from 556 to 545
###  |
###  |- Vessel 10 has the path [10, 362, 520, -4]
###  |  10 (BEANR Out@234) - 362 (TRAMB In@576/Out@603) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 11 has the path [13, 477, 296, 522, -4]
###  |  13 (BEANR Out@402) - 477 (TRMER In@579/Out@623) - 296 (OMSLL In@889/Out@904) - 522 (INNSA In@1074/Out@1090) - -4 (DUM
###  |  MY_END Ziel-Service 81)
###  |
###  |- Vessel 12 has the path [16, 368, 523, -4]
###  |  16 (BEANR Out@570) - 368 (TRUSK In@844/Out@854) - 523 (INNSA In@1242/Out@1258) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 17 has the path [190, 284, 370, 484, 544, 217, 487, 547, 524, -4]
###  |  190 (GBFXT Out@302) - 284 (NLRTM In@332/Out@344) - 370 (TRZMK In@617/Out@631) - 484 (AEJEA In@731/Out@755) - 544 (PKB
###  |  QM In@812/Out@842) - 217 (INPAV In@1114/Out@1126) - 487 (AEJEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524
###  |   (INNSA In@1410/Out@1426) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 555,  244.0 containers from 544 to 524
###  |   |- and carried Demand 556,   67.0 containers from 544 to 524
###  |   |- and carried Demand 558,  118.0 containers from 484 to 524
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |   |- and carried Demand 561,   15.0 containers from 484 to 524
###  |   |- and carried Demand 562,  267.0 containers from 484 to 524
###  |   |- and carried Demand 563,    1.0 containers from 484 to 524
###  |
###  |- Vessel 23 has the path [375, 521, -4]
###  |  375 (UYMVD Out@216) - 521 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 18 has the path [193, 425, 421, 411, -2]
###  |  193 (GBFXT Out@470) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel 13 has the path [135, 385, 151, 361, 303, 0]
###  |  135 (DKAAR Out@220) - 385 (ESALG In@328/Out@347) - 151 (ESALG In@625/Out@670) - 361 (TRALI In@836/Out@854) - 303 (PAB
###  |  LB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 140,  552.0 containers from 151 to 303
###  |   |- and carried Demand 141,   13.0 containers from 151 to 303
###  |
###  |- Vessel 14 has the path [136, 360, 302, 0]
###  |  136 (DKAAR Out@388) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
###  |
### Vessels not used in the solution: [3, 4, 8, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	6m30.455s
user	21m51.530s
sys	2m0.661s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
