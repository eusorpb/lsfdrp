	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_0_fr.p
Instance LSFDP: LSFDRP_3_4_1_0_fd_37.p

This object contains the information regarding the instance 3_4_1_0 for a duration of 37 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.487445 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.494755 sec.
***  |   |- Create additional vessel information,				took 0.008673 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 48671 rows, 147824 columns and 1172279 nonzeros
Variable types: 33801 continuous, 114023 integer (114023 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 39915 rows and 89751 columns (presolve time = 6s) ...
Presolve removed 40738 rows and 89889 columns
Presolve time: 6.80s
Presolved: 7933 rows, 57935 columns, 271817 nonzeros
Variable types: 409 continuous, 57526 integer (57042 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.4064442e+08   1.861797e+04   0.000000e+00      7s
   10977    1.0206802e+07   0.000000e+00   0.000000e+00      9s

Root relaxation: objective 1.020680e+07, 10977 iterations, 2.44 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 9515508.88    0  773          - 9515508.88      -     -   10s
H    0     0                    -1902059.957 9515508.88   600%     -   10s
H    0     0                    1935329.2135 9515508.88   392%     -   23s
     0     0 7756594.00    0  490 1935329.21 7756594.00   301%     -   24s
     0     0 7679322.03    0  495 1935329.21 7679322.03   297%     -   24s
     0     0 7203741.85    0  607 1935329.21 7203741.85   272%     -   25s
     0     0 7103441.56    0  614 1935329.21 7103441.56   267%     -   25s
     0     0 7050049.47    0  627 1935329.21 7050049.47   264%     -   26s
     0     0 6963952.91    0  590 1935329.21 6963952.91   260%     -   26s
     0     0 6961055.27    0  472 1935329.21 6961055.27   260%     -   26s
     0     0 6919947.84    0  584 1935329.21 6919947.84   258%     -   27s
     0     0 6917397.55    0  424 1935329.21 6917397.55   257%     -   27s
     0     0 6917260.30    0  422 1935329.21 6917260.30   257%     -   27s
     0     0 6910308.47    0  590 1935329.21 6910308.47   257%     -   27s
     0     0 6909549.31    0  510 1935329.21 6909549.31   257%     -   28s
     0     0 6909119.45    0  595 1935329.21 6909119.45   257%     -   28s
     0     0 6909032.68    0  597 1935329.21 6909032.68   257%     -   28s
     0     0 6898423.76    0  619 1935329.21 6898423.76   256%     -   29s
     0     0 6898411.33    0  535 1935329.21 6898411.33   256%     -   29s
     0     0 6894027.14    0  602 1935329.21 6894027.14   256%     -   29s
     0     0 6893505.47    0  601 1935329.21 6893505.47   256%     -   29s
     0     0 6890573.99    0  632 1935329.21 6890573.99   256%     -   30s
     0     0 6890558.98    0  635 1935329.21 6890558.98   256%     -   30s
     0     0 6884127.42    0  624 1935329.21 6884127.42   256%     -   30s
     0     0 6881503.73    0  590 1935329.21 6881503.73   256%     -   31s
     0     0 6881462.24    0  615 1935329.21 6881462.24   256%     -   31s
     0     0 6881084.81    0  605 1935329.21 6881084.81   256%     -   31s
     0     0 6881084.81    0  605 1935329.21 6881084.81   256%     -   37s
     0     0 6881084.81    0  554 1935329.21 6881084.81   256%     -   44s
     0     0 6881084.81    0  527 1935329.21 6881084.81   256%     -   48s
     0     0 6881084.81    0  593 1935329.21 6881084.81   256%     -   48s
     0     0 6881084.81    0  593 1935329.21 6881084.81   256%     -   48s
     0     0 6881084.81    0  554 1935329.21 6881084.81   256%     -   49s
     0     0 6821389.39    0  549 1935329.21 6821389.39   252%     -   49s
     0     0 6761493.08    0  498 1935329.21 6761493.08   249%     -   49s
     0     0 6761080.33    0  542 1935329.21 6761080.33   249%     -   49s
     0     0 6755021.36    0  544 1935329.21 6755021.36   249%     -   49s
     0     0 6640391.21    0  551 1935329.21 6640391.21   243%     -   50s
     0     0 6635647.39    0  561 1935329.21 6635647.39   243%     -   50s
     0     0 6635629.63    0  533 1935329.21 6635629.63   243%     -   50s
     0     0 6622449.28    0  563 1935329.21 6622449.28   242%     -   51s
     0     0 6620438.05    0  533 1935329.21 6620438.05   242%     -   51s
     0     0 6620433.66    0  528 1935329.21 6620433.66   242%     -   51s
     0     0 6607573.14    0  539 1935329.21 6607573.14   241%     -   51s
     0     0 6599969.96    0  554 1935329.21 6599969.96   241%     -   52s
     0     0 6596740.88    0  606 1935329.21 6596740.88   241%     -   52s
     0     0 6595274.35    0  637 1935329.21 6595274.35   241%     -   52s
     0     0 6589474.26    0  605 1935329.21 6589474.26   240%     -   52s
     0     0 6589071.12    0  586 1935329.21 6589071.12   240%     -   52s
     0     0 6583489.79    0  605 1935329.21 6583489.79   240%     -   52s
     0     0 6583489.79    0  605 1935329.21 6583489.79   240%     -   56s
     0     2 6583489.79    0  605 1935329.21 6583489.79   240%     -   57s
     3     6 6080657.87    2  558 1935329.21 6277077.20   224%   715   61s
    18    21 5390245.46    6  611 1935329.21 5977023.30   209%   446   65s
    30    34 4692708.92    7  428 1935329.21 5977023.30   209%   537   81s
H   31    34                    1935329.2290 5977023.30   209%   520   81s
    57    61 4049069.82   13  377 1935329.23 5977023.30   209%   450   85s
   175   135 2226137.59   35  305 1935329.23 5977023.30   209%   266   90s
   368   234 2520736.31   19  460 1935329.23 4812620.33   149%   205   95s
   487   314 2779355.69   22  606 1935329.23 4698257.34   143%   200  100s
   731   477 3586200.92   17  631 1935329.23 4475491.80   131%   176  105s
   874   574 2899374.88   16  598 1935329.23 4385351.03   127%   174  120s
H  882   495                    2174426.5423 4385351.03   102%   174  120s
  1030   567 2298200.93   39  605 2174426.54 4310118.53  98.2%   169  137s
H 1031   539                    2174426.7549 4310118.53  98.2%   169  143s
  1033   540 2242728.52   25  573 2174426.75 4310118.53  98.2%   168  146s
  1037   543 4159511.48   12  527 2174426.75 4310118.53  98.2%   168  150s
  1040   545 3288010.56   17  545 2174426.75 4310118.53  98.2%   167  156s
  1043   547 2684457.27    9  660 2174426.75 4310118.53  98.2%   167  160s
  1047   549 4127247.96    9  590 2174426.75 4310118.53  98.2%   166  166s
  1048   550 2748330.64   30  591 2174426.75 4310118.53  98.2%   166  178s
  1050   551 2455444.66   35  591 2174426.75 4310118.53  98.2%   165  193s
  1051   555 4211203.41   12  518 2174426.75 4211203.41  93.7%   180  226s
  1057   561 3791229.64   14  563 2174426.75 3999466.09  83.9%   183  232s
  1065   563 3533204.78   15  363 2174426.75 3790624.08  74.3%   186  235s
  1117   564 2228900.73   22  173 2174426.75 3790624.08  74.3%   186  240s
  1182   574 2764443.91   19  188 2174426.75 3532858.18  62.5%   186  245s
  1280   558 2724558.56   18  581 2174426.75 3316109.79  52.5%   185  250s
  1405   567 2805695.26   21  704 2174426.75 3155004.06  45.1%   185  255s
H 1407   539                    2174426.7810 3155004.06  45.1%   185  255s
  1574   544 2203155.63   23  126 2174426.78 3002333.24  38.1%   183  260s
  1660   531 2384205.31   17  418 2174426.78 2977757.09  36.9%   182  271s
  1803   513 2263678.10   26  395 2174426.78 2887030.08  32.8%   181  275s
  2029   433 2648487.38   18  379 2174426.78 2782835.44  28.0%   178  280s
  2327   259 2498969.83   21  367 2174426.78 2662983.20  22.5%   175  285s
  2656    15     cutoff   22      2174426.78 2428707.80  11.7%   168  291s

Cutting planes:
  Gomory: 24
  Implied bound: 18
  Projected implied bound: 11
  Clique: 12
  MIR: 23
  Flow cover: 22
  Zero half: 33
  Mod-K: 2

Explored 2762 nodes (478034 simplex iterations) in 292.03 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: 2.17443e+06 2.17443e+06 2.17443e+06 ... -1.90206e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 2.174426542261e+06, best bound 2.174426542261e+06, gap 0.0000%
Took  298.6718394756317
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 67254 rows, 62814 columns and 211685 nonzeros
Variable types: 60240 continuous, 2574 integer (2574 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 67254 rows and 62814 columns
Presolve time: 0.09s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.11 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -6.56855e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -6.568554812809e+06, best bound -6.568554812809e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_0_fd_37

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 109
### |A|: 104
### |A^i|: 104
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -6,568,554.8128 (-6568554.8128)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 472, 373, 440, -3]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 472 (TRAMB 
###  |  In@1019/Out@1067) - 373 (TRZMK In@1074/Out@1089) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to  67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  67
###  |   |- and carried Demand  60,   19.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 435, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 361, 438, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 361 (TRA
###  |  LI In@836/Out@854) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |
###  |- Vessel  8 has the path [287, 478, 448, 449, 369, 439, -3]
###  |  287 (NLRTM Out@596) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRU
###  |  SK In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 15 has the path [179, 385, 262, 401, 263, 149, 395, 47, 155, 480, 441, -3]
###  |  179 (FRLEH Out@158) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAP
###  |  TM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 395 (GWOXB In@631/Out@710) - 47 (CIABJ In@751/Out@783) - 155 (ESALG
###  |   In@932/Out@950) - 480 (TRMER In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  20,  124.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 155
###  |   |- and carried Demand 260,   10.0 containers from 262 to 155
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 155
###  |   |- and carried Demand 273,   10.0 containers from 263 to 155
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 155
###  |   |- and carried Demand 286,   10.0 containers from 401 to 155
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 16 has the path [181, 477, 437, -3]
###  |  181 (FRLEH Out@326) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 387, -1]
###  |  131 (DEHAM Out@288) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  3 has the path [133, 388, -1]
###  |  133 (DEHAM Out@624) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 386, -1]
###  |  135 (DKAAR Out@220) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 556, 572, 563, 372, 485, 545, 297, 488, 548, 525, -4]
###  |  9 (BEANR Out@66) - 556 (USCHS In@378/Out@388) - 572 (USORF In@413/Out@426) - 563 (USEWR In@450/Out@466) - 372 (TRZMK 
###  |  In@785/Out@799) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 297 (OMSLL In@1132/Out@1145) - 488 (AEJE
###  |  A In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |   |- and carried Demand 547,  339.0 containers from 572 to 485
###  |   |- and carried Demand 548,  115.0 containers from 572 to 485
###  |   |- and carried Demand 549,    3.0 containers from 572 to 297
###  |   |- and carried Demand 550,    2.0 containers from 572 to 545
###  |   |- and carried Demand 551,  194.0 containers from 556 to 485
###  |   |- and carried Demand 552,   49.0 containers from 556 to 485
###  |   |- and carried Demand 553,   40.0 containers from 556 to 545
###  |
###  |- Vessel 10 has the path [10, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  10 (BEANR Out@234) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAIL
###  |  K In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 (
###  |  DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 11 has the path [13, 360, 521, -4]
###  |  13 (BEANR Out@402) - 360 (TRALI In@668/Out@686) - 521 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 12 has the path [16, 368, 522, -4]
###  |  16 (BEANR Out@570) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 17 has the path [190, 362, 484, 544, 217, 487, 547, 524, -4]
###  |  190 (GBFXT Out@302) - 362 (TRAMB In@576/Out@603) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 217 (INP
###  |  AV In@1114/Out@1126) - 487 (AEJEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 555,  244.0 containers from 544 to 524
###  |   |- and carried Demand 556,   67.0 containers from 544 to 524
###  |   |- and carried Demand 558,  118.0 containers from 484 to 524
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |   |- and carried Demand 561,   15.0 containers from 484 to 524
###  |   |- and carried Demand 562,  267.0 containers from 484 to 524
###  |   |- and carried Demand 563,    1.0 containers from 484 to 524
###  |
###  |- Vessel 23 has the path [375, 520, -4]
###  |  375 (UYMVD Out@216) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 18 has the path [193, 425, 421, 411, -2]
###  |  193 (GBFXT Out@470) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 371, 303, 0]
###  |  132 (DEHAM Out@456) - 371 (TRZMK In@778/Out@793) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [134, 365, 305, 0]
###  |  134 (DEHAM Out@792) - 365 (TRAMB In@1040/Out@1064) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 370, 302, 0]
###  |  136 (DKAAR Out@388) - 370 (TRZMK In@617/Out@631) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [19, 20, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	5m8.250s
user	17m10.218s
sys	1m23.553s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
