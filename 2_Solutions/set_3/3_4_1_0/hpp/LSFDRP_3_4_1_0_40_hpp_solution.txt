	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_0_fr.p
Instance LSFDP: LSFDRP_3_4_1_0_fd_40.p

This object contains the information regarding the instance 3_4_1_0 for a duration of 40 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.409922 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.516858 sec.
***  |   |- Create additional vessel information,				took 0.008206 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 51811 rows, 160428 columns and 1295358 nonzeros
Variable types: 36592 continuous, 123836 integer (123836 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 42342 rows and 96552 columns (presolve time = 7s) ...
Presolve removed 43215 rows and 96787 columns
Presolve time: 8.15s
Presolved: 8596 rows, 63641 columns, 300053 nonzeros
Variable types: 495 continuous, 63146 integer (62632 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.7774741e+08   2.696509e+04   0.000000e+00      9s
    8894    1.5464059e+07   1.528787e+04   0.000000e+00     10s
   12219    1.1724958e+07   0.000000e+00   0.000000e+00     12s

Root relaxation: objective 1.172496e+07, 12219 iterations, 3.13 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1030e+07    0  939          - 1.1030e+07      -     -   12s
H    0     0                    -3148022.774 1.1030e+07   450%     -   13s
H    0     0                    -885544.6041 1.1030e+07  1346%     -   13s
H    0     0                    3078799.3193 1.1030e+07   258%     -   29s
     0     0 9711630.54    0  843 3078799.32 9711630.54   215%     -   31s
     0     0 9639707.69    0  831 3078799.32 9639707.69   213%     -   32s
     0     0 9286914.54    0  893 3078799.32 9286914.54   202%     -   33s
     0     0 9284088.10    0  884 3078799.32 9284088.10   202%     -   34s
     0     0 9283091.09    0  885 3078799.32 9283091.09   202%     -   34s
     0     0 9282224.86    0  883 3078799.32 9282224.86   201%     -   34s
     0     0 9226039.89    0  853 3078799.32 9226039.89   200%     -   34s
     0     0 8733088.95    0  586 3078799.32 8733088.95   184%     -   35s
     0     0 8651089.16    0  500 3078799.32 8651089.16   181%     -   35s
     0     0 8649554.29    0  533 3078799.32 8649554.29   181%     -   35s
     0     0 8649349.80    0  435 3078799.32 8649349.80   181%     -   36s
     0     0 8529787.55    0  546 3078799.32 8529787.55   177%     -   36s
     0     0 8529787.55    0  547 3078799.32 8529787.55   177%     -   36s
     0     0 8457474.79    0  513 3078799.32 8457474.79   175%     -   37s
     0     0 8457134.92    0  534 3078799.32 8457134.92   175%     -   37s
     0     0 8442066.63    0  540 3078799.32 8442066.63   174%     -   37s
     0     0 8432465.14    0  576 3078799.32 8432465.14   174%     -   38s
     0     0 8432425.15    0  576 3078799.32 8432425.15   174%     -   38s
     0     0 8403731.08    0  576 3078799.32 8403731.08   173%     -   38s
     0     0 8403057.54    0  517 3078799.32 8403057.54   173%     -   38s
     0     0 8401660.39    0  598 3078799.32 8401660.39   173%     -   39s
     0     0 8394396.28    0  571 3078799.32 8394396.28   173%     -   39s
     0     0 8394352.69    0  574 3078799.32 8394352.69   173%     -   39s
     0     0 8393241.77    0  572 3078799.32 8393241.77   173%     -   39s
     0     0 8392182.99    0  572 3078799.32 8392182.99   173%     -   40s
     0     0 8392182.99    0  572 3078799.32 8392182.99   173%     -   45s
H    0     0                    3090182.4801 8392182.99   172%     -   52s
     0     2 8392182.99    0  572 3090182.48 8392182.99   172%     -   52s
     3     8 8021110.87    2  491 3090182.48 8021110.87   160%   457   56s
    15    17 6648687.88    4  486 3090182.48 7948686.97   157%   493   60s
    32    33 5978947.05    9  489 3090182.48 7948686.97   157%   402   65s
    73    69 5215078.15   17  553 3090182.48 7948686.97   157%   317   70s
   160   144 4131471.82   31  499 3090182.48 7948686.97   157%   256   75s
   263   212 3262078.66   15  267 3090182.48 6810396.33   120%   227   80s
   373   254 4533633.79   12  613 3090182.48 6772630.61   119%   210   85s
   558   355 6134397.82    7  309 3090182.48 6409808.58   107%   188   90s
   772   505 4907155.44    5  434 3090182.48 6283642.54   103%   175   95s
   850   552 5717900.80    9  402 3090182.48 6255756.77   102%   176  105s
H  851   546                    3163249.8785 6255756.77  97.8%   176  105s
   988   636 4732408.10    8  652 3163249.88 6096337.36  92.7%   173  110s
  1043   676 3998407.55   26  572 3163249.88 6096337.36  92.7%   171  130s
H 1044   643                    3163250.0095 6096337.36  92.7%   171  142s
H 1044   610                    3329281.1379 6096337.36  83.1%   171  143s
  1046   611 4906038.97    6  420 3329281.14 6096337.36  83.1%   171  148s
  1049   613 4176968.40   14  437 3329281.14 6096337.36  83.1%   170  152s
  1052   615 5175338.58   19  660 3329281.14 6096337.36  83.1%   170  158s
  1054   616 4059413.36   21  451 3329281.14 6086493.55  82.8%   170  160s
  1057   618 5175338.58   19  536 3329281.14 5796461.02  74.1%   169  166s
  1058   619 3629644.16   30  505 3329281.14 5722232.12  71.9%   169  172s
  1059   620 3361359.29   57  505 3329281.14 5722232.12  71.9%   169  184s
  1060   620 4471572.20   10  504 3329281.14 5722232.12  71.9%   169  186s
H 1060   589                    3386080.7628 5722232.12  69.0%   169  211s
  1062   594 5470083.06   11  517 3386080.76 5470083.06  61.5%   189  250s
  1064   596 5092320.56   12  593 3386080.76 5336169.03  57.6%   190  256s
  1072   599 4686885.09   13  431 3386080.76 4820255.53  42.4%   192  260s
  1103   607 4500592.64   17  358 3386080.76 4768442.00  40.8%   196  265s
  1187   602     cutoff   25      3386080.76 4768412.92  40.8%   196  270s
  1268   603 4174493.71   19  503 3386080.76 4768412.92  40.8%   196  275s
  1345   611 4331644.74   16  395 3386080.76 4745893.47  40.2%   199  280s
  1425   610 3421841.44   26  291 3386080.76 4659350.88  37.6%   200  285s
  1547   613 3397148.29   28  350 3386080.76 4480029.76  32.3%   202  290s
  1702   600 3479134.08   34  408 3386080.76 4263833.85  25.9%   202  295s
  1879   560 3800266.32   21  489 3386080.76 4177969.51  23.4%   203  301s
  2056   537 3497179.36   18  149 3386080.76 4129591.94  22.0%   204  306s
  2199   505 3511961.82   24  183 3386080.76 4094733.27  20.9%   203  312s
  2279   484 3822935.68   22  298 3386080.76 4053906.16  19.7%   204  315s
  2443   459 3784282.94   27  359 3386080.76 4018522.83  18.7%   203  329s
H 2466   430                    3386080.8583 4018522.83  18.7%   202  329s
H 2485   397                    3386081.0117 4018522.83  18.7%   201  329s
  2496   363 3399137.06   39  344 3386081.01 3994761.94  18.0%   201  331s
  2612   324     cutoff   23      3386081.01 3936538.85  16.3%   200  338s
  2637   290 3430700.90   22  378 3386081.01 3904277.42  15.3%   201  340s
  2917   221 3408338.75   20  247 3386081.01 3822713.82  12.9%   198  363s
H 2958   221                    3386081.6002 3822713.82  12.9%   197  363s
  3063   161 3674364.18   19  182 3386081.60 3779266.31  11.6%   196  366s
  3355    26 3390670.34   20  437 3386081.60 3629944.24  7.20%   191  370s

Cutting planes:
  Gomory: 18
  Implied bound: 14
  Projected implied bound: 14
  Clique: 11
  MIR: 23
  Flow cover: 28
  Zero half: 33
  Mod-K: 1

Explored 3564 nodes (670939 simplex iterations) in 371.28 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 3.38608e+06 3.38608e+06 3.38608e+06 ... -885545
No other solutions better than 3.38608e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 3.386081836755e+06, best bound 3.386081836755e+06, gap 0.0000%
Took  377.6369230747223
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 71482 rows, 67035 columns and 226645 nonzeros
Variable types: 64293 continuous, 2742 integer (2742 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 5e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 71482 rows and 67035 columns
Presolve time: 0.13s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.17 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -7.69203e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -7.692030972446e+06, best bound -7.692030972446e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_0_fd_40

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 116
### |A|: 111
### |A^i|: 111
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -7,692,030.9724 (-7692030.9724)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 472, 373, 440, -3]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 472 (TRAMB 
###  |  In@1019/Out@1067) - 373 (TRZMK In@1074/Out@1089) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to  67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  67
###  |   |- and carried Demand  60,   19.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 435, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 361, 438, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 361 (TRA
###  |  LI In@836/Out@854) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |
###  |- Vessel  8 has the path [287, 478, 448, 449, 369, 439, -3]
###  |  287 (NLRTM Out@596) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRU
###  |  SK In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 15 has the path [179, 385, 262, 401, 263, 149, 395, 47, 155, 480, 441, -3]
###  |  179 (FRLEH Out@158) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAP
###  |  TM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 395 (GWOXB In@631/Out@710) - 47 (CIABJ In@751/Out@783) - 155 (ESALG
###  |   In@932/Out@950) - 480 (TRMER In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  20,  124.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 155
###  |   |- and carried Demand 260,   10.0 containers from 262 to 155
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 155
###  |   |- and carried Demand 273,   10.0 containers from 263 to 155
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 155
###  |   |- and carried Demand 286,   10.0 containers from 401 to 155
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 16 has the path [181, 477, 437, -3]
###  |  181 (FRLEH Out@326) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  3 has the path [133, 388, -1]
###  |  133 (DEHAM Out@624) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 387, -1]
###  |  135 (DKAAR Out@220) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 556, 572, 563, 372, 485, 545, 297, 488, 548, 525, -4]
###  |  9 (BEANR Out@66) - 556 (USCHS In@378/Out@388) - 572 (USORF In@413/Out@426) - 563 (USEWR In@450/Out@466) - 372 (TRZMK 
###  |  In@785/Out@799) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 297 (OMSLL In@1132/Out@1145) - 488 (AEJE
###  |  A In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |   |- and carried Demand 547,  339.0 containers from 572 to 485
###  |   |- and carried Demand 548,  115.0 containers from 572 to 485
###  |   |- and carried Demand 549,    3.0 containers from 572 to 297
###  |   |- and carried Demand 550,    2.0 containers from 572 to 545
###  |   |- and carried Demand 551,  194.0 containers from 556 to 485
###  |   |- and carried Demand 552,   49.0 containers from 556 to 485
###  |   |- and carried Demand 553,   40.0 containers from 556 to 545
###  |
###  |- Vessel 10 has the path [10, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  10 (BEANR Out@234) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAIL
###  |  K In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 (
###  |  DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 11 has the path [13, 360, 521, -4]
###  |  13 (BEANR Out@402) - 360 (TRALI In@668/Out@686) - 521 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 12 has the path [16, 368, 522, -4]
###  |  16 (BEANR Out@570) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 17 has the path [190, 362, 484, 544, 217, 487, 547, 524, -4]
###  |  190 (GBFXT Out@302) - 362 (TRAMB In@576/Out@603) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 217 (INP
###  |  AV In@1114/Out@1126) - 487 (AEJEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 555,  244.0 containers from 544 to 524
###  |   |- and carried Demand 556,   67.0 containers from 544 to 524
###  |   |- and carried Demand 558,  118.0 containers from 484 to 524
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |   |- and carried Demand 561,   15.0 containers from 484 to 524
###  |   |- and carried Demand 562,  267.0 containers from 484 to 524
###  |   |- and carried Demand 563,    1.0 containers from 484 to 524
###  |
###  |- Vessel 23 has the path [375, 520, -4]
###  |  375 (UYMVD Out@216) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 18 has the path [193, 425, 421, 411, -2]
###  |  193 (GBFXT Out@470) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 371, 303, 0]
###  |  132 (DEHAM Out@456) - 371 (TRZMK In@778/Out@793) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 370, 302, 0]
###  |  136 (DKAAR Out@388) - 370 (TRZMK In@617/Out@631) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
###  |
### Vessels not used in the solution: [4, 20, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	6m27.766s
user	21m40.722s
sys	1m51.495s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
