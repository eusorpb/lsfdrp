	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_0_fr.p
Instance LSFDP: LSFDRP_3_4_1_0_fd_37.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 117 rows, 198 columns and 618 nonzeros
Variable types: 0 continuous, 198 integer (198 binary)
Coefficient statistics:
  Matrix range     [1e+00, 6e+00]
  Objective range  [7e+05, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Found heuristic solution: objective -8.33953e+07
Presolve removed 41 rows and 43 columns
Presolve time: 0.01s
Presolved: 76 rows, 155 columns, 398 nonzeros
Variable types: 0 continuous, 155 integer (155 binary)

Root relaxation: objective -5.145248e+06, 102 iterations, 0.00 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -5145248.1    0   15 -8.340e+07 -5145248.1  93.8%     -    0s
H    0     0                    -1.53517e+07 -5145248.1  66.5%     -    0s
     0     0 -6598259.5    0   26 -1.535e+07 -6598259.5  57.0%     -    0s
H    0     0                    -8886946.400 -6598259.5  25.8%     -    0s
H    0     0                    -8742981.355 -6598259.5  24.5%     -    0s
     0     0 -7229414.0    0   43 -8742981.4 -7229414.0  17.3%     -    0s
     0     0 -8148113.5    0    1 -8742981.4 -8148113.5  6.80%     -    0s
     0     0 -8244090.2    0   12 -8742981.4 -8244090.2  5.71%     -    0s
     0     0 -8244090.2    0   12 -8742981.4 -8244090.2  5.71%     -    0s

Cutting planes:
  Gomory: 1
  Cover: 4
  Clique: 5

Explored 1 nodes (258 simplex iterations) in 0.04 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -8.74298e+06 -8.88695e+06 -1.53517e+07 -8.33953e+07 
No other solutions better than -8.74298e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -8.742981355070e+06, best bound -8.742981355070e+06, gap 0.0000%

### Objective value in 1k: -8742.98
### Objective value: -8742981.3551
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 7(ME3): 19, (PMax28), 6
### 81(MECL1): 30, (PMax35), 6
### 177(WCSA): 18, (PMax25), 3
### 24(WAF7): 18, (PMax25), 3
### 121(SAE): 30, (PMax35), 2
### ---
### Charter vessels chartered in:
### 23 on service 81
### 24 on service 81
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[7,19] = 1
rho[81,30] = 1
rho[177,18] = 1
rho[24,18] = 1
rho[121,30] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
eta[81,30,1] = 1
eta[81,30,2] = 1
eta[81,30,3] = 1
eta[81,30,4] = 1
eta[81,30,5] = 1
eta[81,30,6] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,177] = 1
y[2,177] = 1
y[3,24] = 1
y[4,24] = 1
y[5,7] = 1
y[6,7] = 1
y[7,7] = 1
y[8,7] = 1
y[9,121] = 1
y[10,121] = 1
y[11,81] = 1
y[12,81] = 1
y[13,24] = 1
y[14,177] = 1
y[15,7] = 1
y[16,7] = 1
y[17,81] = 1
y[18,81] = 1
y[23,81] = 1
y[24,81] = 1

### All variables with value zero:
rho[7,18] = 0
rho[7,30] = -0
rho[81,18] = 0
rho[81,19] = 0
rho[177,19] = -0
rho[177,30] = -0
rho[24,19] = -0
rho[24,30] = -0
rho[121,18] = 0
rho[121,19] = -0
eta[7,18,1] = 0
eta[7,18,2] = -0
eta[7,18,3] = 0
eta[7,18,4] = -0
eta[7,18,5] = -0
eta[7,18,6] = -0
eta[7,30,1] = 0
eta[7,30,2] = 0
eta[7,30,3] = 0
eta[7,30,4] = -0
eta[7,30,5] = -0
eta[7,30,6] = -0
eta[81,18,1] = -0
eta[81,18,2] = 0
eta[81,18,3] = 0
eta[81,18,4] = 0
eta[81,18,5] = 0
eta[81,18,6] = 0
eta[81,19,1] = 0
eta[81,19,2] = 0
eta[81,19,3] = 0
eta[81,19,4] = 0
eta[81,19,5] = 0
eta[81,19,6] = 0
eta[177,19,1] = 0
eta[177,19,2] = 0
eta[177,19,3] = -0
eta[177,30,1] = 0
eta[177,30,2] = 0
eta[177,30,3] = -0
eta[24,18,4] = -0
eta[24,19,1] = 0
eta[24,19,2] = 0
eta[24,19,3] = -0
eta[24,19,4] = -0
eta[24,30,1] = 0
eta[24,30,2] = 0
eta[24,30,3] = -0
eta[24,30,4] = -0
eta[121,18,1] = -0
eta[121,18,2] = -0
eta[121,19,1] = 0
eta[121,19,2] = -0
y[1,7] = -0
y[1,81] = -0
y[1,24] = -0
y[1,121] = -0
y[2,7] = -0
y[2,81] = -0
y[2,24] = -0
y[2,121] = -0
y[3,7] = -0
y[3,81] = -0
y[3,177] = -0
y[3,121] = -0
y[4,7] = -0
y[4,81] = -0
y[4,177] = -0
y[4,121] = -0
y[5,81] = -0
y[5,177] = -0
y[5,24] = -0
y[5,121] = -0
y[6,81] = -0
y[6,177] = -0
y[6,24] = -0
y[6,121] = -0
y[7,81] = -0
y[7,177] = -0
y[7,24] = -0
y[7,121] = -0
y[8,81] = -0
y[8,177] = -0
y[8,24] = -0
y[8,121] = -0
y[9,7] = -0
y[9,81] = -0
y[9,177] = -0
y[9,24] = -0
y[10,7] = -0
y[10,81] = -0
y[10,177] = -0
y[10,24] = -0
y[11,7] = -0
y[11,177] = -0
y[11,24] = -0
y[11,121] = -0
y[12,7] = -0
y[12,177] = -0
y[12,24] = -0
y[12,121] = -0
y[13,7] = -0
y[13,81] = -0
y[13,177] = -0
y[13,121] = -0
y[14,7] = -0
y[14,81] = -0
y[14,24] = -0
y[14,121] = -0
y[15,81] = -0
y[15,177] = -0
y[15,24] = -0
y[15,121] = -0
y[16,81] = -0
y[16,177] = -0
y[16,24] = -0
y[16,121] = -0
y[17,7] = -0
y[17,177] = -0
y[17,24] = -0
y[17,121] = -0
y[18,7] = -0
y[18,177] = -0
y[18,24] = -0
y[18,121] = -0
y[19,7] = -0
y[19,81] = -0
y[19,177] = -0
y[19,24] = -0
y[19,121] = -0
y[20,7] = -0
y[20,81] = -0
y[20,177] = -0
y[20,24] = -0
y[20,121] = -0
y[21,7] = 0
y[21,81] = -0
y[21,177] = -0
y[21,24] = -0
y[21,121] = -0
y[22,7] = 0
y[22,81] = -0
y[22,177] = -0
y[22,24] = -0
y[22,121] = -0
y[23,7] = -0
y[23,177] = -0
y[23,24] = -0
y[23,121] = -0
y[24,7] = -0
y[24,177] = -0
y[24,24] = -0
y[24,121] = -0

### End

real	0m0.903s
user	0m0.589s
sys	0m0.146s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
