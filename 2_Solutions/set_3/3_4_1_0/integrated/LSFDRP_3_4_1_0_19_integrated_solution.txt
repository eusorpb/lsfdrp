	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_0_fr.p
Instance LSFDP: LSFDRP_3_4_1_0_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_3_4_1_0_fd_19 with Gurobi
***
*** Graph info
*** |S|: 24
*** |S^E|: 0
*** |S^C|: 6
*** |V|: 578
*** |A|: 29983
*** |A^i|: 29836
*** |A^f|: 147
*** |M|: 575
*** |E|: 28

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 420318 rows, 18024515 columns and 63796805 nonzeros
Variable types: 17304845 continuous, 719670 integer (719670 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 13847 rows and 0 columns (presolve time = 7s) ...
Presolve removed 15748 rows and 1924 columns (presolve time = 15s) ...
Presolve removed 58986 rows and 3878181 columns (presolve time = 22s) ...
Presolve removed 101589 rows and 8266332 columns (presolve time = 28s) ...
Presolve removed 136627 rows and 10265393 columns (presolve time = 32s) ...
Presolve removed 155945 rows and 11253168 columns (presolve time = 35s) ...
Presolve removed 176165 rows and 12438842 columns (presolve time = 41s) ...
Presolve removed 182842 rows and 12758201 columns (presolve time = 45s) ...
Presolve removed 189402 rows and 12895280 columns (presolve time = 58s) ...
Presolve removed 189402 rows and 12901515 columns (presolve time = 60s) ...
Presolve removed 189402 rows and 12912538 columns (presolve time = 65s) ...
Presolve removed 196864 rows and 12920000 columns (presolve time = 72s) ...
Presolve removed 200070 rows and 12950424 columns (presolve time = 75s) ...
Presolve removed 200598 rows and 12950424 columns (presolve time = 80s) ...
Presolve removed 202204 rows and 12950424 columns (presolve time = 87s) ...
Presolve removed 203822 rows and 12991410 columns (presolve time = 92s) ...
Presolve removed 203822 rows and 12991410 columns (presolve time = 102s) ...
Presolve removed 203822 rows and 12991926 columns (presolve time = 105s) ...
Presolve removed 206045 rows and 12994260 columns (presolve time = 111s) ...
Presolve removed 206045 rows and 12994260 columns (presolve time = 116s) ...
Presolve removed 206046 rows and 12994260 columns (presolve time = 120s) ...
Presolve removed 206047 rows and 12994260 columns (presolve time = 130s) ...
Presolve removed 206101 rows and 12995499 columns (presolve time = 139s) ...
Presolve removed 206101 rows and 12995499 columns (presolve time = 140s) ...
Presolve removed 206101 rows and 12995499 columns (presolve time = 145s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 206141 rows and 12995499 columns (presolve time = 172s) ...
Presolve removed 206141 rows and 13195176 columns (presolve time = 4869s) ...
Presolve removed 214350 rows and 13195234 columns (presolve time = 4872s) ...
Presolve removed 283349 rows and 16270472 columns (presolve time = 4876s) ...
Presolve removed 297683 rows and 16461976 columns (presolve time = 4882s) ...
Presolve removed 302190 rows and 16468229 columns (presolve time = 4888s) ...
Presolve removed 302190 rows and 16468229 columns (presolve time = 4890s) ...
Presolve removed 302435 rows and 16469629 columns (presolve time = 4897s) ...
Presolve removed 302435 rows and 16469701 columns (presolve time = 4900s) ...
Presolve removed 303048 rows and 16470315 columns (presolve time = 4906s) ...
Presolve removed 303065 rows and 16470315 columns (presolve time = 4914s) ...
Presolve removed 303065 rows and 16470315 columns (presolve time = 4915s) ...
Presolve removed 303085 rows and 16470336 columns (presolve time = 4920s) ...
Presolve removed 305114 rows and 16528290 columns (presolve time = 4925s) ...
Presolve removed 307339 rows and 16560860 columns (presolve time = 4930s) ...
Presolve removed 307662 rows and 16566422 columns (presolve time = 4935s) ...
Presolve removed 308003 rows and 16574219 columns (presolve time = 4940s) ...
Presolve removed 308133 rows and 16574386 columns (presolve time = 4945s) ...
Presolve removed 315474 rows and 16658576 columns (presolve time = 4972s) ...
Presolve removed 315512 rows and 16658611 columns (presolve time = 6648s) ...
Presolve removed 315473 rows and 16658572 columns
Presolve time: 6647.86s
Presolved: 104845 rows, 1365943 columns, 7589608 nonzeros
Variable types: 904284 continuous, 461659 integer (461596 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 25 rows and 529 columns
Presolved: 104820 rows, 1365414 columns, 7577847 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 101
 AA' NZ     : 8.110e+06
 Factor NZ  : 2.284e+08 (roughly 2.4 GBytes of memory)
 Factor Ops : 1.735e+12 (roughly 100 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -6.10966148e+15  5.86994364e+16  8.49e+08 1.50e+06  2.18e+12  6836s
   1  -3.86215955e+15  5.73737705e+16  5.38e+08 1.02e+07  1.40e+12  6946s
   2  -2.72251952e+15  5.56972988e+16  3.79e+08 6.68e+06  9.95e+11  7056s
   3  -1.94064313e+15  5.44488919e+16  2.70e+08 5.69e+06  7.19e+11  7164s
   4  -1.47709624e+15  5.17932059e+16  2.06e+08 3.96e+06  5.48e+11  7270s
   5  -1.00602226e+15  4.90732639e+16  1.40e+08 2.86e+06  3.76e+11  7379s
   6  -6.98891064e+14  4.61349758e+16  9.71e+07 2.22e+06  2.64e+11  7488s
   7  -3.77627068e+14  4.28283850e+16  5.22e+07 1.76e+06  1.50e+11  7599s
   8  -1.90339004e+14  3.47060221e+16  2.61e+07 1.09e+06  7.89e+10  7706s
   9  -6.71337841e+13  2.77057009e+16  9.05e+06 7.61e+05  3.31e+10  7813s
  10  -3.61359437e+13  1.91305815e+16  4.83e+06 4.78e+05  1.90e+10  7918s
  11  -1.00045907e+13  5.81694166e+15  1.34e+06 9.76e+04  5.31e+09  8029s
  12  -1.66609094e+12  1.00837394e+15  2.23e+05 3.34e+03  8.65e+08  8144s
  13  -7.83209542e+11  1.87708453e+14  1.05e+05 1.90e-05  2.79e+08  8252s
  14  -4.02645673e+11  9.84876136e+13  5.39e+04 5.44e-06  1.43e+08  8356s
  15  -2.18585680e+11  4.78832138e+13  2.93e+04 1.37e-05  7.46e+07  8462s
  16  -9.98701897e+10  1.08438601e+13  1.34e+04 2.01e-05  2.96e+07  8574s
  17  -5.54104889e+10  7.98070912e+12  7.40e+03 1.09e-05  1.71e+07  8679s
  18  -3.33202464e+10  6.14896034e+12  4.44e+03 7.37e-06  1.08e+07  8783s
  19  -2.09910965e+10  3.53089615e+12  2.78e+03 3.97e-06  6.55e+06  8889s
  20  -1.15920611e+10  2.95165193e+12  1.53e+03 3.08e-06  3.98e+06  8996s
  21  -4.91440194e+09  1.98146184e+12  6.38e+02 1.86e-06  1.96e+06  9104s
  22  -2.69030210e+09  1.37477163e+12  3.43e+02 1.13e-06  1.17e+06  9211s
  23  -2.09446644e+09  1.04177356e+12  2.64e+02 7.95e-07  8.89e+05  9315s
  24  -1.42993394e+09  8.45512241e+11  1.75e+02 6.89e-07  6.45e+05  9423s
  25  -9.47846748e+08  6.64932701e+11  1.12e+02 6.22e-07  4.55e+05  9528s
  26  -6.68014393e+08  4.67322664e+11  7.53e+01 6.51e-07  3.10e+05  9634s
  27  -3.73859334e+08  3.13177726e+11  3.73e+01 4.87e-07  1.82e+05  9740s
  28  -2.36603802e+08  2.42008088e+11  2.02e+01 4.56e-07  1.25e+05  9848s
  29  -1.78725466e+08  1.87875467e+11  1.34e+01 4.06e-07  9.29e+04  9956s
  30  -1.35130856e+08  1.53505475e+11  8.70e+00 3.97e-07  7.25e+04  10063s
  31  -1.08773041e+08  1.04245924e+11  6.24e+00 5.42e-07  5.02e+04  10172s
  32  -8.79508659e+07  9.01022419e+10  4.37e+00 5.65e-07  4.15e+04  10278s
  33  -7.56831060e+07  7.77292360e+10  3.25e+00 3.82e-07  3.48e+04  10384s
  34  -5.49455777e+07  4.78041400e+10  1.29e+00 3.05e-07  2.03e+04  10492s
  35  -4.23154049e+07  2.79240558e+10  2.11e-01 2.31e-07  1.12e+04  10604s
  36  -3.76210986e+07  2.38752413e+10  1.07e-01 2.35e-07  9.22e+03  10709s
  37  -3.52933897e+07  1.01912007e+10  6.70e-02 3.25e-07  4.03e+03  10814s
  38  -3.41875733e+07  8.36984555e+09  5.55e-02 2.36e-07  3.31e+03  10923s
  39  -3.18708471e+07  6.55370076e+09  3.56e-02 2.92e-07  2.55e+03  11034s
  40  -2.96221856e+07  5.04398677e+09  2.28e-02 2.59e-07  1.95e+03  11141s
  41  -2.74389093e+07  3.86502893e+09  1.58e-02 2.62e-07  1.48e+03  11248s
  42  -2.39239896e+07  3.06719112e+09  1.03e-02 3.11e-07  1.17e+03  11354s
  43  -2.06227465e+07  1.57082516e+09  6.48e-03 2.61e-07  6.07e+02  11464s
  44  -1.82216109e+07  1.08026634e+09  4.84e-03 2.26e-07  4.21e+02  11569s
  45  -1.61913702e+07  8.16508957e+08  3.98e-03 3.24e-07  3.21e+02  11673s
  46  -1.30012236e+07  6.68568609e+08  2.93e-03 2.80e-07  2.61e+02  11778s
  47  -1.12920325e+07  5.23823217e+08  2.53e-03 2.77e-07  2.06e+02  11883s
  48  -9.90224769e+06  4.22420337e+08  2.27e-03 2.89e-07  1.68e+02  11988s
  49  -7.59800072e+06  3.46309004e+08  1.90e-03 2.98e-07  1.38e+02  12093s
  50  -5.26265711e+06  3.15722569e+08  1.65e-03 2.99e-07  1.25e+02  12200s
  51  -2.75214400e+06  2.49632450e+08  1.29e-03 2.96e-07  9.79e+01  12304s
  52   1.00517756e+06  1.96746943e+08  9.47e-04 3.13e-07  7.57e+01  12416s
  53   3.73462533e+06  1.65074347e+08  7.60e-04 2.85e-07  6.23e+01  12521s
  54   5.96469101e+06  1.16475676e+08  6.00e-04 3.18e-07  4.31e+01  12629s
  55   8.02716905e+06  8.63765308e+07  4.58e-04 3.38e-07  3.08e+01  12739s
  56   9.35835172e+06  7.61719057e+07  3.69e-04 3.16e-07  2.61e+01  12844s
  57   1.14478159e+07  5.91357467e+07  2.36e-04 2.99e-07  1.86e+01  12957s
  58   1.33104121e+07  4.65918040e+07  1.28e-04 2.83e-07  1.28e+01  13070s
  59   1.36835852e+07  4.03369423e+07  1.11e-04 2.89e-07  1.03e+01  13175s
  60   1.38147054e+07  3.85576861e+07  1.06e-04 3.15e-07  9.55e+00  13280s
  61   1.43506072e+07  3.62043186e+07  8.46e-05 3.26e-07  8.39e+00  13385s
  62   1.45569056e+07  3.53002189e+07  8.02e-05 3.62e-07  7.96e+00  13490s
  63   1.45606979e+07  3.45080697e+07  7.79e-05 4.17e-07  7.66e+00  13599s
  64   1.49733530e+07  3.18688164e+07  6.61e-05 3.79e-07  6.49e+00  13705s
  65   1.50983506e+07  3.03897966e+07  6.29e-05 3.64e-07  5.89e+00  13812s
  66   1.54722194e+07  2.93796801e+07  5.27e-05 3.95e-07  5.34e+00  13919s
  67   1.55719448e+07  2.84562075e+07  4.97e-05 4.63e-07  4.95e+00  14025s
  68   1.57338154e+07  2.78958630e+07  4.68e-05 4.48e-07  4.67e+00  14130s
  69   1.59181195e+07  2.67680281e+07  4.21e-05 4.07e-07  4.17e+00  14237s
  70   1.61091950e+07  2.58497317e+07  3.68e-05 4.40e-07  3.74e+00  14342s
  71   1.64128847e+07  2.47679630e+07  2.92e-05 4.04e-07  3.19e+00  14447s
  72   1.65163024e+07  2.44317201e+07  2.69e-05 4.59e-07  3.02e+00  14558s
  73   1.66062955e+07  2.38579534e+07  2.51e-05 4.12e-07  2.77e+00  14662s
  74   1.68412767e+07  2.32034628e+07  2.16e-05 3.83e-07  2.43e+00  14775s
  75   1.69595076e+07  2.28773796e+07  2.03e-05 4.19e-07  2.26e+00  14880s
  76   1.73314421e+07  2.23390452e+07  1.63e-05 4.20e-07  1.91e+00  14988s
  77   1.75794854e+07  2.19196148e+07  1.38e-05 4.42e-07  1.65e+00  15096s
  78   1.78755878e+07  2.14743891e+07  1.10e-05 3.95e-07  1.37e+00  15202s
  79   1.82271509e+07  2.05760001e+07  7.76e-06 3.66e-07  8.97e-01  15316s
  80   1.84060296e+07  2.00855313e+07  6.16e-06 3.32e-07  6.45e-01  15425s
  81   1.86251337e+07  1.97703590e+07  4.25e-06 3.40e-07  4.41e-01  15532s
  82   1.87825705e+07  1.96056962e+07  2.84e-06 3.58e-07  3.16e-01  15641s
  83   1.89191794e+07  1.94805944e+07  1.66e-06 3.34e-07  2.14e-01  15750s
  84   1.89837356e+07  1.93501023e+07  1.46e-06 2.92e-07  1.40e-01  15856s
  85   1.90206027e+07  1.92966077e+07  1.07e-06 4.88e-07  1.05e-01  15963s
  86   1.90620527e+07  1.92529491e+07  8.93e-07 4.78e-07  7.23e-02  16072s
  87   1.90867786e+07  1.92043471e+07  1.26e-06 4.00e-07  4.45e-02  16179s
  88   1.91084230e+07  1.91536546e+07  5.62e-07 3.65e-07  1.72e-02  16292s
  89   1.91115387e+07  1.91442082e+07  4.45e-07 3.78e-07  1.25e-02  16398s
  90   1.91190071e+07  1.91270315e+07  5.69e-07 4.14e-07  3.15e-03  16506s
  91   1.91225363e+07  1.91251748e+07  1.20e-07 4.04e-07  1.01e-03  16611s
  92   1.91230560e+07  1.91237138e+07  4.74e-08 5.24e-07  2.62e-04  16716s
  93   1.91232669e+07  1.91235638e+07  3.49e-08 4.16e-07  1.19e-04  16823s
  94   1.91234281e+07  1.91234790e+07  3.22e-08 6.65e-07  2.08e-05  16929s
  95   1.91234285e+07  1.91234786e+07  1.06e-06 7.72e-07  2.05e-05  17035s
  96   1.91234637e+07  1.91234673e+07  3.59e-08 6.12e-07  1.46e-06  17139s
  97   1.91234641e+07  1.91234673e+07  2.25e-05 5.42e-07  1.26e-06  17245s
  98   1.91234665e+07  1.91234667e+07  9.08e-07 6.00e-07  4.68e-08  17350s
  99   1.91234541e+07  1.91234667e+07  5.66e-06 4.97e-07  6.75e-09  17455s

Barrier solved model in 99 iterations and 17455.26 seconds
Optimal objective 1.91234541e+07


Root crossover log...

  100417 DPushes remaining with DInf 0.0000000e+00             17456s
     558 DPushes remaining with DInf 0.0000000e+00             17462s
     377 DPushes remaining with DInf 0.0000000e+00             17466s
     207 DPushes remaining with DInf 0.0000000e+00             17472s
     117 DPushes remaining with DInf 0.0000000e+00             17476s
       0 DPushes remaining with DInf 8.8771379e-07             17480s

     273 PPushes remaining with PInf 4.2649035e-04             17480s
       0 PPushes remaining with PInf 0.0000000e+00             17480s

  Push phase complete: Pinf 0.0000000e+00, Dinf 1.6333982e+03  17480s


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
   86219    1.9123467e+07   0.000000e+00   1.633398e+03  17480s
   86320    1.9123467e+07   0.000000e+00   0.000000e+00  17481s
   86320    1.9123467e+07   0.000000e+00   0.000000e+00  17482s
Concurrent spin time: 1983.69s (can be avoided by choosing Method=3)

Solved with barrier

Root relaxation: objective 1.912347e+07, 86320 iterations, 12799.77 seconds
Total elapsed time = 19476.16s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.9123e+07    0  963          - 1.9123e+07      -     - 19544s
     0     0 1.7557e+07    0 1004          - 1.7557e+07      -     - 28895s
     0     0 1.7373e+07    0 1018          - 1.7373e+07      -     - 33437s
     0     0 1.7368e+07    0 1043          - 1.7368e+07      -     - 35229s
     0     0 1.7368e+07    0 1051          - 1.7368e+07      -     - 35634s
     0     0 1.7368e+07    0 1048          - 1.7368e+07      -     - 35681s
     0     0          -    0               - 1.7368e+07      -     - 86400s

Cutting planes:
  Implied bound: 13
  Clique: 44
  MIR: 17
  Flow cover: 78
  GUB cover: 1
  Zero half: 1

Explored 1 nodes (470447 simplex iterations) in 86400.10 seconds
Thread count was 4 (of 16 available processors)

Solution count 0

Time limit reached
Best objective -, best bound 1.736793530421e+07, gap -
Traceback (most recent call last):
  File "lsfdrp_mathmodel.py", line 605, in <module>
    solve_lsifdp(instance, data)
  File "lsfdrp_mathmodel.py", line 465, in solve_lsifdp
    if var.x > 0.1:
  File "var.pxi", line 76, in gurobipy.Var.__getattr__
  File "var.pxi", line 142, in gurobipy.Var.getAttr
AttributeError: b"Unable to retrieve attribute 'x'"

real	1470m23.190s
user	4980m14.547s
sys	797m55.237s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
