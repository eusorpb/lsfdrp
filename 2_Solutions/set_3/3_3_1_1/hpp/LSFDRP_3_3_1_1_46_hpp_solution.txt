	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_3_1_1_fr.p
Instance LSFDP: LSFDRP_3_3_1_1_fd_46.p

This object contains the information regarding the instance 3_3_1_1 for a duration of 46 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.433399 sec.
***  |   |- Calculate demand structure for node flow model,		took  1.22666 sec.
***  |   |- Create additional vessel information,				took 0.006655 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 25322 rows, 61529 columns and 427547 nonzeros
Variable types: 17674 continuous, 43855 integer (43855 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -1.60266e+07
Presolve removed 21969 rows and 40550 columns
Presolve time: 2.61s
Presolved: 3353 rows, 20979 columns, 96919 nonzeros
Found heuristic solution: objective -1.56964e+07
Variable types: 156 continuous, 20823 integer (20571 binary)

Root relaxation: objective 5.374971e+06, 3607 iterations, 0.34 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 5374971.06    0  195 -1.570e+07 5374971.06   134%     -    3s
H    0     0                    841124.53897 5374971.06   539%     -    3s
H    0     0                    1400371.2808 5374971.06   284%     -    3s
H    0     0                    3095835.4276 5374971.06  73.6%     -    3s
H    0     0                    3095835.4513 5374971.06  73.6%     -    4s
     0     0 5016072.29    0  222 3095835.45 5016072.29  62.0%     -    4s
H    0     0                    3095835.7447 5016072.29  62.0%     -    4s
     0     0 4939390.48    0  221 3095835.74 4939390.48  59.5%     -    4s
     0     0 4933770.84    0  243 3095835.74 4933770.84  59.4%     -    4s
     0     0 4832475.51    0  198 3095835.74 4832475.51  56.1%     -    4s
     0     0 4832065.83    0  200 3095835.74 4832065.83  56.1%     -    5s
     0     0 4783016.31    0  228 3095835.74 4783016.31  54.5%     -    5s
H    0     0                    3143841.3504 4783016.31  52.1%     -    5s
     0     0 4783016.31    0  228 3143841.35 4783016.31  52.1%     -    5s
     0     0 4783016.31    0  228 3143841.35 4783016.31  52.1%     -    5s
     0     0 4779505.76    0  255 3143841.35 4779505.76  52.0%     -    5s
     0     0 4779505.76    0  255 3143841.35 4779505.76  52.0%     -    6s
H    0     0                    3148626.3671 4779505.76  51.8%     -    7s
     0     2 4779505.76    0  255 3148626.37 4779505.76  51.8%     -    7s

Cutting planes:
  Gomory: 5
  Implied bound: 2
  MIR: 4
  Zero half: 5

Explored 203 nodes (15334 simplex iterations) in 9.15 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: 3.14863e+06 3.14384e+06 3.09584e+06 ... -1.56964e+07

Optimal solution found (tolerance 1.00e-04)
Best objective 3.148626367085e+06, best bound 3.148626367085e+06, gap 0.0000%
Took  12.762326717376709
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 32157 rows, 29457 columns and 102948 nonzeros
Variable types: 27672 continuous, 1785 integer (1785 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 5e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 32157 rows and 29457 columns
Presolve time: 0.07s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.09 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 1.09536e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.095361632532e+06, best bound 1.095361632532e+06, gap 0.0000%

### Instance
### LSFDRP_3_3_1_1_fd_46

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 76
### |A|: 72
### |A^i|: 72
### |A^f|: 0
### |M|: 380
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,095,361.6325 (1095361.6325)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 480, 441, -3]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 480 (TRMER 
###  |  In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to 67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to 67
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to 67
###  |   |- and carried Demand 295,   19.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 444, 464, 295, 437, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 444 (EGPSD In@531/Out@550) - 464 (OMSLL In@710/Out@722) - 295 (OMS
###  |  LL In@721/Out@736) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 365,  288.0 containers from 464 to 437
###  |   |- and carried Demand 366,   14.0 containers from 464 to 437
###  |   |- and carried Demand 372,  164.0 containers from 444 to 437
###  |   |- and carried Demand 373,   58.0 containers from 444 to 437
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 448, 449, 369, 439, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUSK In@1018/Out@1028) - 439 (AE
###  |  JEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel  8 has the path [287, 479, 440, -3]
###  |  287 (NLRTM Out@596) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [179, 468, 435, -3]
###  |  179 (FRLEH Out@158) - 468 (TRAMB In@347/Out@395) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 16 has the path [181, 370, 465, 296, 438, -3]
###  |  181 (FRLEH Out@326) - 370 (TRZMK In@617/Out@631) - 465 (OMSLL In@878/Out@890) - 296 (OMSLL In@889/Out@904) - 438 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 359,  111.0 containers from 465 to 438
###  |   |- and carried Demand 360,   35.0 containers from 465 to 438
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 385, 262, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAP
###  |  TM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  312.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 18 has the path [193, 425, 421, 411, -2]
###  |  193 (GBFXT Out@470) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [133, 478, 361, 303, 0]
###  |  133 (DEHAM Out@624) - 478 (TRMER In@747/Out@791) - 361 (TRALI In@836/Out@854) - 303 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [134, 365, 305, 0]
###  |  134 (DEHAM Out@792) - 365 (TRAMB In@1040/Out@1064) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 477, 360, 302, 0]
###  |  136 (DKAAR Out@388) - 477 (TRMER In@579/Out@623) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [9, 10, 11, 12, 17, 19, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m19.437s
user	0m32.308s
sys	0m2.138s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
