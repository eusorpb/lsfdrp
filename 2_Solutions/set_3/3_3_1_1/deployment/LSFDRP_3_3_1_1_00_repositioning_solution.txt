	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_3_1_1_fr.p
Instance LSFDP: LSFDRP_3_3_1_1_fd_00.p

*****************************************************************************************************************************
*** Solve LSFDRP_3_3_1_1_fd_00 with Gurobi
***
*** Graph info
*** |S|: 24
*** |S^E|: 0
*** |S^C|: 6
*** |V|: 482
*** |A|: 22408
*** |A^i|: 22261
*** |A^f|: 147
*** |M|: 380
*** |E|: 28

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 253055 rows, 9102180 columns and 32933890 nonzeros
Variable types: 8564334 continuous, 537846 integer (537846 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 6e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10959 rows and 1426 columns (presolve time = 7s) ...
Presolve removed 36201 rows and 2042395 columns (presolve time = 11s) ...
Presolve removed 58464 rows and 3806846 columns (presolve time = 15s) ...
Presolve removed 77623 rows and 4937023 columns (presolve time = 20s) ...
Presolve removed 93557 rows and 5788408 columns (presolve time = 25s) ...
Presolve removed 98525 rows and 5893394 columns (presolve time = 32s) ...
Presolve removed 98525 rows and 5904801 columns (presolve time = 35s) ...
Presolve removed 103724 rows and 5910000 columns (presolve time = 40s) ...
Presolve removed 104451 rows and 5926773 columns (presolve time = 45s) ...
Presolve removed 106035 rows and 5936076 columns (presolve time = 51s) ...
Presolve removed 106035 rows and 5936076 columns (presolve time = 56s) ...
Presolve removed 107338 rows and 5937787 columns (presolve time = 63s) ...
Presolve removed 107338 rows and 5937787 columns (presolve time = 65s) ...
Presolve removed 107339 rows and 5937787 columns (presolve time = 74s) ...
Presolve removed 107339 rows and 5937973 columns (presolve time = 75s) ...
Presolve removed 107348 rows and 5938543 columns (presolve time = 80s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 107372 rows and 5938543 columns (presolve time = 96s) ...
Presolve removed 107372 rows and 6216264 columns (presolve time = 1928s) ...
Presolve removed 119398 rows and 6216322 columns (presolve time = 1930s) ...
Presolve removed 175301 rows and 8257486 columns (presolve time = 1936s) ...
Presolve removed 178515 rows and 8261898 columns (presolve time = 1940s) ...
Presolve removed 178844 rows and 8263685 columns (presolve time = 1945s) ...
Presolve removed 178894 rows and 8268899 columns (presolve time = 1950s) ...
Presolve removed 178918 rows and 8271519 columns (presolve time = 1955s) ...
Presolve removed 180371 rows and 8287736 columns (presolve time = 1968s) ...
Presolve removed 180380 rows and 8288601 columns (presolve time = 2375s) ...
Presolve removed 180356 rows and 8288577 columns
Presolve time: 2375.43s
Presolved: 72699 rows, 813603 columns, 3659018 nonzeros
Variable types: 605282 continuous, 208321 integer (208156 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 411 rows and 2910 columns
Presolved: 72288 rows, 810693 columns, 3652641 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 2
 AA' NZ     : 4.006e+06
 Factor NZ  : 1.206e+08 (roughly 1.3 GBytes of memory)
 Factor Ops : 6.532e+11 (roughly 50 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -5.88640815e+14  9.62409287e+14  7.52e+07 3.61e+03  8.85e+10  2456s
   1  -3.36337148e+14  9.40726916e+14  4.39e+07 1.48e+06  5.24e+10  2497s
   2  -1.68234302e+14  9.02806703e+14  2.22e+07 8.32e+05  2.72e+10  2540s
   3  -7.27737965e+13  8.52165300e+14  9.56e+06 5.09e+05  1.23e+10  2582s
   4  -3.51258378e+13  7.54948048e+14  4.58e+06 2.61e+05  6.04e+09  2623s
   5  -1.65458163e+13  5.91193437e+14  2.14e+06 1.01e+05  2.89e+09  2664s
   6  -1.00200909e+13  4.56851143e+14  1.29e+06 5.50e+04  1.75e+09  2704s
   7  -4.66280251e+12  2.61051439e+14  5.93e+05 1.02e+04  7.95e+08  2744s
   8  -1.53137268e+12  1.54342588e+14  1.95e+05 4.53e+03  2.91e+08  2781s
   9  -2.81651314e+11  3.98062742e+13  3.58e+04 3.57e+02  5.76e+07  2825s
  10  -1.17244445e+11  1.51987073e+13  1.49e+04 5.96e+01  2.26e+07  2862s
  11  -5.00145355e+10  4.09729477e+12  6.36e+03 6.25e-07  8.07e+06  2904s
  12  -1.11441198e+10  1.39048043e+12  1.41e+03 4.88e-07  2.10e+06  2944s
  13  -3.95753327e+09  8.97327011e+11  4.96e+02 3.46e-07  9.85e+05  2986s
  14  -2.08110842e+09  5.77149771e+11  2.56e+02 2.10e-07  5.77e+05  3025s
  15  -1.00712459e+09  3.69796139e+11  1.20e+02 1.30e-07  3.32e+05  3065s
  16  -5.12933433e+08  1.94853105e+11  5.72e+01 7.17e-08  1.72e+05  3109s
  17  -3.69244817e+08  1.48362920e+11  3.92e+01 7.36e-08  1.27e+05  3148s
  18  -2.09073275e+08  8.98553002e+10  1.94e+01 5.07e-08  7.25e+04  3187s
  19  -1.47052596e+08  6.64738697e+10  1.21e+01 4.80e-08  5.17e+04  3226s
  20  -1.01502353e+08  4.82432908e+10  7.30e+00 4.37e-08  3.65e+04  3268s
  21  -7.09719568e+07  3.74109814e+10  4.40e+00 3.61e-08  2.74e+04  3307s
  22  -5.90165537e+07  2.82915270e+10  3.35e+00 3.27e-08  2.07e+04  3350s
  23  -3.94795706e+07  2.07560125e+10  1.75e+00 3.06e-08  1.45e+04  3390s
  24  -2.52783469e+07  8.82974658e+09  6.71e-01 4.05e-08  6.09e+03  3436s
  25  -1.99142859e+07  5.65722247e+09  3.06e-01 2.89e-08  3.79e+03  3478s
  26  -1.36397881e+07  2.40901841e+09  7.51e-02 4.97e-08  1.56e+03  3520s
  27  -1.18634850e+07  1.40700377e+09  4.94e-02 3.22e-08  9.13e+02  3563s
  28  -9.56805925e+06  8.44367893e+08  3.57e-02 3.13e-08  5.53e+02  3608s
  29  -6.93509710e+06  5.44002310e+08  2.29e-02 2.91e-08  3.57e+02  3647s
  30  -3.09317936e+06  3.44703079e+08  1.20e-02 2.65e-08  2.23e+02  3690s
  31  -5.10805112e+05  2.13318665e+08  8.61e-03 3.20e-08  1.38e+02  3733s
  32   1.65091450e+06  1.63191790e+08  5.67e-03 3.26e-08  1.04e+02  3774s
  33   4.11640846e+06  9.41575374e+07  2.74e-03 3.71e-08  5.76e+01  3823s
  34   5.19923052e+06  7.26460222e+07  1.92e-03 3.10e-08  4.30e+01  3863s
  35   6.53064951e+06  5.50982778e+07  1.18e-03 3.03e-08  3.08e+01  3907s
  36   7.74326248e+06  4.10955988e+07  6.55e-04 3.47e-08  2.11e+01  3949s
  37   8.34391780e+06  3.19397932e+07  4.51e-04 3.66e-08  1.49e+01  3990s
  38   8.62678350e+06  2.89492151e+07  3.98e-04 3.31e-08  1.28e+01  4032s
  39   9.02975595e+06  2.56287079e+07  3.05e-04 3.88e-08  1.05e+01  4075s
  40   9.29395523e+06  2.36332136e+07  2.62e-04 5.11e-08  9.05e+00  4116s
  41   9.34883600e+06  2.15965887e+07  2.46e-04 4.12e-08  7.75e+00  4156s
  42   9.78799834e+06  1.97678534e+07  1.64e-04 5.36e-08  6.28e+00  4199s
  43   9.92509560e+06  1.82581828e+07  1.46e-04 9.53e-08  5.25e+00  4242s
  44   1.00427348e+07  1.76669456e+07  1.33e-04 5.32e-08  4.80e+00  4282s
  45   1.02027173e+07  1.67282432e+07  1.17e-04 9.83e-08  4.12e+00  4323s
  46   1.03928864e+07  1.62951629e+07  1.01e-04 4.70e-08  3.72e+00  4367s
  47   1.07422291e+07  1.54655534e+07  7.65e-05 4.19e-08  2.97e+00  4409s
  48   1.09989475e+07  1.47580456e+07  6.07e-05 4.40e-08  2.37e+00  4448s
  49   1.11831904e+07  1.43965659e+07  5.00e-05 3.90e-08  2.02e+00  4487s
  50   1.13461999e+07  1.37667758e+07  4.09e-05 3.66e-08  1.53e+00  4527s
  51   1.14378237e+07  1.35200598e+07  3.57e-05 4.39e-08  1.31e+00  4567s
  52   1.15498359e+07  1.34619836e+07  2.97e-05 5.37e-08  1.20e+00  4606s
  53   1.16546878e+07  1.30839177e+07  2.36e-05 5.32e-08  9.02e-01  4648s
  54   1.17714814e+07  1.28531242e+07  1.72e-05 6.05e-08  6.82e-01  4690s
  55   1.18898950e+07  1.25274780e+07  1.09e-05 5.29e-08  4.03e-01  4730s
  56   1.19582767e+07  1.22617132e+07  7.40e-06 4.90e-08  1.95e-01  4770s
  57   1.20614283e+07  1.21930812e+07  2.15e-06 4.86e-08  8.35e-02  4812s
  58   1.20827314e+07  1.21475223e+07  1.13e-06 6.87e-08  4.12e-02  4853s
  59   1.20948693e+07  1.21265487e+07  6.48e-07 5.39e-08  2.02e-02  4896s
  60   1.21000423e+07  1.21187116e+07  3.55e-07 7.12e-08  1.19e-02  4936s
  61   1.21042730e+07  1.21133630e+07  4.71e-07 6.44e-08  5.72e-03  4977s
  62   1.21047792e+07  1.21132479e+07  6.07e-07 7.17e-08  5.30e-03  5017s
  63   1.21057402e+07  1.21095423e+07  9.08e-07 7.24e-08  2.38e-03  5061s
  64   1.21063784e+07  1.21068500e+07  1.85e-07 9.71e-08  2.98e-04  5102s
  65   1.21064536e+07  1.21067075e+07  7.09e-08 1.03e-07  1.59e-04  5142s
  66   1.21064557e+07  1.21066877e+07  8.15e-08 1.02e-07  1.46e-04  5182s
  67   1.21064850e+07  1.21065595e+07  1.46e-07 9.71e-08  4.70e-05  5226s
  68   1.21064901e+07  1.21065406e+07  2.19e-07 9.55e-08  3.20e-05  5266s
  69   1.21064901e+07  1.21065406e+07  2.40e-07 1.01e-07  3.20e-05  5307s
  70   1.21064904e+07  1.21065382e+07  2.28e-07 1.04e-07  3.02e-05  5347s
  71   1.21064941e+07  1.21065186e+07  1.66e-07 7.37e-08  1.58e-05  5387s
  72   1.21064999e+07  1.21065110e+07  3.73e-07 1.08e-07  6.97e-06  5432s
  73   1.21065002e+07  1.21065083e+07  3.38e-07 8.73e-08  5.14e-06  5471s
  74   1.21065025e+07  1.21065045e+07  1.02e-07 1.04e-07  1.30e-06  5513s
  75   1.21065027e+07  1.21065040e+07  2.06e-07 1.11e-07  8.68e-07  5554s
  76   1.21065027e+07  1.21065040e+07  5.17e-07 1.41e-07  8.68e-07  5595s
  77   1.21065028e+07  1.21065039e+07  7.56e-07 1.34e-07  7.13e-07  5636s
  78   1.21065029e+07  1.21065038e+07  6.21e-06 1.21e-07  5.79e-07  5676s
  79   1.21065029e+07  1.21065038e+07  6.63e-06 1.73e-07  5.79e-07  5717s
  80   1.21065029e+07  1.21065038e+07  7.25e-06 1.84e-07  5.79e-07  5757s
  81   1.21065029e+07  1.21065038e+07  1.82e-05 1.90e-07  5.75e-07  5797s
  82   1.21065029e+07  1.21065038e+07  1.66e-05 2.05e-07  5.73e-07  5838s
  83   1.21065030e+07  1.21065038e+07  1.88e-05 2.25e-07  5.72e-07  5879s
  84   1.21065030e+07  1.21065038e+07  1.88e-05 2.17e-07  5.70e-07  5919s
  85   1.21065030e+07  1.21065038e+07  1.86e-05 2.55e-07  5.70e-07  5959s
  86   1.21065030e+07  1.21065038e+07  1.83e-05 2.76e-07  5.69e-07  5999s
  87   1.21065030e+07  1.21065037e+07  1.52e-05 1.72e-07  4.34e-07  6039s
  88   1.21065030e+07  1.21065036e+07  1.52e-05 2.05e-07  4.30e-07  6084s
  89   1.21065030e+07  1.21065036e+07  1.33e-05 2.10e-07  4.01e-07  6125s
  90   1.21065030e+07  1.21065036e+07  1.01e-05 1.97e-07  3.76e-07  6167s
  91   1.21065031e+07  1.21065035e+07  2.05e-05 1.62e-07  2.67e-07  6211s
  92   1.21065031e+07  1.21065035e+07  2.04e-05 1.77e-07  2.67e-07  6252s
  93   1.21065032e+07  1.21065035e+07  1.12e-05 1.35e-07  1.52e-07  6293s
  94   1.21065032e+07  1.21065035e+07  2.65e-05 1.42e-07  1.52e-07  6334s
  95   1.21065033e+07  1.21065034e+07  5.70e-06 2.43e-07  6.19e-08  6379s
  96   1.21065034e+07  1.21065034e+07  2.60e-06 2.39e-07  2.81e-08  6420s
  97   1.21065033e+07  1.21065034e+07  8.19e-06 2.23e-07  2.81e-08  6461s
  98   1.21065033e+07  1.21065034e+07  6.29e-06 2.19e-07  1.74e-08  6502s
  99   1.21065033e+07  1.21065034e+07  9.55e-06 2.06e-07  1.15e-08  6542s
 100   1.21065034e+07  1.21065034e+07  6.90e-07 2.30e-07  7.89e-10  6584s

Barrier solved model in 100 iterations and 6584.40 seconds
Optimal objective 1.21065034e+07


Root crossover log...

   69540 DPushes remaining with DInf 0.0000000e+00              6585s
    5014 DPushes remaining with DInf 0.0000000e+00              6585s
      43 DPushes remaining with DInf 0.0000000e+00              6591s
       0 DPushes remaining with DInf 2.3216994e-07              6592s

     170 PPushes remaining with PInf 4.8478985e-05              6592s
       0 PPushes remaining with PInf 1.0058284e-06              6592s

  Push phase complete: Pinf 1.0058284e-06, Dinf 1.7702636e-07   6592s


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
   58279    1.2106503e+07   1.005828e-06   0.000000e+00   6592s
   58280    1.2106503e+07   0.000000e+00   0.000000e+00   6592s
   59213    1.2106503e+07   0.000000e+00   6.175427e+04   6595s
   60774    1.2106504e+07   0.000000e+00   4.280354e+05   6601s
   61707    1.2106504e+07   0.000000e+00   1.689624e+03   6605s
   62948    1.2106504e+07   0.000000e+00   5.785295e+03   6611s
   63050    1.2106503e+07   0.000000e+00   0.000000e+00   6613s
Extra 4770 simplex iterations after uncrush
Concurrent spin time: 0.00s

Solved with barrier

Root relaxation: objective 1.210650e+07, 63050 iterations, 4228.38 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2085e+07    0  528          - 1.2085e+07      -     - 6937s
H    0     0                    -1285998.969 1.2085e+07  1040%     - 6943s
H    0     0                    -25158.96257 1.2085e+07      -     - 15440s
     0     0 1.1436e+07    0  503 -25158.963 1.1436e+07      -     - 62532s
     0     0 1.1434e+07    0  534 -25158.963 1.1434e+07      -     - 63048s
     0     0 1.1430e+07    0  512 -25158.963 1.1430e+07      -     - 63917s
     0     0 1.1429e+07    0  532 -25158.963 1.1429e+07      -     - 64096s
     0     0 1.1332e+07    0  560 -25158.963 1.1332e+07      -     - 66375s
     0     0 1.1330e+07    0  577 -25158.963 1.1330e+07      -     - 67022s
     0     0 1.1323e+07    0  574 -25158.963 1.1323e+07      -     - 67750s
     0     0 1.1323e+07    0  579 -25158.963 1.1323e+07      -     - 67813s
     0     0 1.1323e+07    0  577 -25158.963 1.1323e+07      -     - 67895s
     0     0 1.1323e+07    0  577 -25158.963 1.1323e+07      -     - 67896s
     0     0 1.1283e+07    0  593 -25158.963 1.1283e+07      -     - 70231s
     0     0 1.1262e+07    0  567 -25158.963 1.1262e+07      -     - 72715s
     0     0 1.1261e+07    0  573 -25158.963 1.1261e+07      -     - 74027s
     0     0 1.1260e+07    0  583 -25158.963 1.1260e+07      -     - 74529s
     0     0 1.1260e+07    0  583 -25158.963 1.1260e+07      -     - 74586s
     0     0 1.1241e+07    0  602 -25158.963 1.1241e+07      -     - 76608s
     0     0 1.1236e+07    0  598 -25158.963 1.1236e+07      -     - 77197s
     0     0 1.1233e+07    0  609 -25158.963 1.1233e+07      -     - 77678s
     0     0 1.1233e+07    0  600 -25158.963 1.1233e+07      -     - 77920s
     0     0 1.1225e+07    0  584 -25158.963 1.1225e+07      -     - 79504s
     0     0 1.1224e+07    0  614 -25158.963 1.1224e+07      -     - 79747s
     0     0 1.1224e+07    0  606 -25158.963 1.1224e+07      -     - 79779s
     0     0 1.1224e+07    0  603 -25158.963 1.1224e+07      -     - 79919s
     0     0 1.1204e+07    0  606 -25158.963 1.1204e+07      -     - 82335s
     0     0 1.1202e+07    0  612 -25158.963 1.1202e+07      -     - 82797s
     0     0 1.1202e+07    0  612 -25158.963 1.1202e+07      -     - 82905s
     0     0 1.1190e+07    0  646 -25158.963 1.1190e+07      -     - 85253s
     0     0 1.1189e+07    0  632 -25158.963 1.1189e+07      -     - 85846s
     0     0 1.1189e+07    0  634 -25158.963 1.1189e+07      -     - 86007s
     0     0 1.1189e+07    0  648 -25158.963 1.1189e+07      -     - 86172s
     0     0 1.1189e+07    0  654 -25158.963 1.1189e+07      -     - 86177s
     0     0          -    0      -25158.963 1.1189e+07      -     - 86400s

Cutting planes:
  Implied bound: 26
  Clique: 5
  MIR: 31
  Flow cover: 139
  Zero half: 2

Explored 1 nodes (2956127 simplex iterations) in 86400.66 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -25159 -1.286e+06 

Time limit reached
Warning: max constraint violation (6.3347e-06) exceeds tolerance
Warning: max bound violation (7.4706e-06) exceeds tolerance
Best objective -2.515896246087e+04, best bound 1.118864849189e+07, gap 44571.8200%

***
***  |- Calculation finished, timelimit reached 86,400.79 (86400.79) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -25,158.9625 (-25158.9625) and is feasible with a Gap of 445.7182
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [282, 457, 454, 434, 464, 295, 437, -3]
###  |  282 (NLRTM Out@92.0) - 457 (INPAV In@467.0/Out@494.0) - 454 (INNSA In@522.0/Out@546.0) - 434 (AEJEA In@617.0/Out@642.
###  |  0) - 464 (OMSLL In@710.0/Out@722.0) - 295 (OMSLL In@721.0/Out@736.0) - 437 (AEJEA In@858.0/Out@891.0) - -3 (DUMMY_END
###  |   Ziel-Service 7)
###  |   |- and carried Demand 365,  288.0 containers from 464 to 437
###  |   |- and carried Demand 366,   14.0 containers from 464 to 437
###  |   |- and carried Demand 367,  280.0 containers from 454 to 434
###  |- Vessel  6 has the path [283, 476, 444, 445, 446, 448, 450, 451, 452, 441, -3]
###  |  283 (NLRTM Out@260.0) - 476 (TRMER In@411.0/Out@455.0) - 444 (EGPSD In@531.0/Out@550.0) - 445 (EGPSD In@621.0/Out@637
###  |  .0) - 446 (EGPSD In@699.0/Out@718.0) - 448 (EGPSD In@867.0/Out@886.0) - 450 (EGPSD In@1035.0/Out@1054.0) - 451 (EGPSD
###  |   In@1125.0/Out@1141.0) - 452 (EGPSD In@1293.0/Out@1309.0) - 441 (AEJEA In@1530.0/Out@1563.0) - -3 (DUMMY_END Ziel-Ser
###  |  vice 7)
###  |   |- and carried Demand 355,  113.0 containers from 452 to 441
###  |   |- and carried Demand 356,   54.0 containers from 452 to 441
###  |- Vessel  7 has the path [285, 368, 438, -3]
###  |  285 (NLRTM Out@428.0) - 368 (TRUSK In@844.0/Out@854.0) - 438 (AEJEA In@1026.0/Out@1059.0) - -3 (DUMMY_END Ziel-Servic
###  |  e 7)
###  |- Vessel  8 has the path [287, 369, 439, -3]
###  |  287 (NLRTM Out@596.0) - 369 (TRUSK In@1018.0/Out@1028.0) - 439 (AEJEA In@1194.0/Out@1227.0) - -3 (DUMMY_END Ziel-Serv
###  |  ice 7)
###  |- Vessel 15 has the path [179, 468, 435, -3]
###  |  179 (FRLEH Out@158.0) - 468 (TRAMB In@347.0/Out@395.0) - 435 (AEJEA In@690.0/Out@723.0) - -3 (DUMMY_END Ziel-Service 
###  |  7)
###  |- Vessel 16 has the path [181, 477, 465, 440, -3]
###  |  181 (FRLEH Out@326.0) - 477 (TRMER In@579.0/Out@623.0) - 465 (OMSLL In@878.0/Out@890.0) - 440 (AEJEA In@1362.0/Out@13
###  |  95.0) - -3 (DUMMY_END Ziel-Service 7)
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288.0) - 386 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456.0) - 387 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  3 has the path [133, 388, -1]
###  |  133 (DEHAM Out@624.0) - 388 (ESALG In@832.0/Out@851.0) - -1 (DUMMY_END Ziel-Service 24)
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [9, 400, 261, 385, 263, 149, 411, -2]
###  |  9 (BEANR Out@66.0) - 400 (MAPTM In@211.0/Out@230.0) - 261 (MAPTM In@238.0/Out@259.0) - 385 (ESALG In@328.0/Out@347.0)
###  |   - 263 (MAPTM In@406.0/Out@427.0) - 149 (ESALG In@452.0/Out@464.0) - 411 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END 
###  |  Ziel-Service 121)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 271,  580.0 containers from 261 to 385
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 295,  580.0 containers from 400 to 385
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171.0) - 106 (CNXMN In@238.0/Out@260.0) - 78 (CNNSA In@287.0/Out@297.0) - 210 (HKHKG In@334.0/Out@350.
###  |  0) - 107 (CNYTN In@358.0/Out@376.0) - 75 (CNNGB In@430.0/Out@439.0) - 86 (CNSHA In@465.0/Out@483.0) - 270 (MXLZC In@8
###  |  73.0/Out@896.0) - 300 (PABLB In@982.0/Out@1030.0) - 412 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121
###  |  )
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel 13 has the path [135, 401, 264, 302, 0]
###  |  135 (DKAAR Out@220.0) - 401 (MAPTM In@379.0/Out@398.0) - 264 (MAPTM In@543.0/Out@564.0) - 302 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 14 has the path [136, 371, 303, 0]
###  |  136 (DKAAR Out@388.0) - 371 (TRZMK In@778.0/Out@793.0) - 303 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel 20 has the path [164, 475, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  164 (ESBCN Out@94.0) - 475 (TRMER In@243.0/Out@287.0) - 354 (SGSIN In@618.0/Out@651.0) - 249 (KRPUS In@788.0/Out@810.
###  |  0) - 226 (JPHKT In@824.0/Out@833.0) - 71 (CNDLC In@871.0/Out@886.0) - 102 (CNXGG In@904.0/Out@923.0) - 99 (CNTAO In@9
###  |  60.0/Out@978.0) - 76 (CNNGB In@1031.0/Out@1040.0) - 88 (CNSHA In@1069.0/Out@1079.0) - 305 (PABLB In@1640.0/Out@1652.0
###  |  ) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
### Vessels not used in the solution: [4, 10, 11, 12, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,131,386] = 1
y[1,386,-1] = 1
y[2,132,387] = 1
y[2,387,-1] = 1
y[3,133,388] = 1
y[3,388,-1] = 1
y[5,457,454] = 1
y[5,282,457] = 1
y[5,295,437] = 1
y[5,464,295] = 1
y[5,434,464] = 1
y[5,437,-3] = 1
y[5,454,434] = 1
y[6,448,450] = 1
y[6,450,451] = 1
y[6,444,445] = 1
y[6,445,446] = 1
y[6,451,452] = 1
y[6,476,444] = 1
y[6,452,441] = 1
y[6,441,-3] = 1
y[6,446,448] = 1
y[6,283,476] = 1
y[7,285,368] = 1
y[7,368,438] = 1
y[7,438,-3] = 1
y[8,287,369] = 1
y[8,369,439] = 1
y[8,439,-3] = 1
y[9,149,411] = 1
y[9,9,400] = 1
y[9,411,-2] = 1
y[9,400,261] = 1
y[9,263,149] = 1
y[9,385,263] = 1
y[9,261,385] = 1
y[13,302,0] = 1
y[13,401,264] = 1
y[13,264,302] = 1
y[13,135,401] = 1
y[14,303,0] = 1
y[14,136,371] = 1
y[14,371,303] = 1
y[15,468,435] = 1
y[15,179,468] = 1
y[15,435,-3] = 1
y[16,465,440] = 1
y[16,477,465] = 1
y[16,181,477] = 1
y[16,440,-3] = 1
y[20,71,102] = 1
y[20,354,249] = 1
y[20,249,226] = 1
y[20,475,354] = 1
y[20,164,475] = 1
y[20,76,88] = 1
y[20,99,76] = 1
y[20,102,99] = 1
y[20,305,0] = 1
y[20,226,71] = 1
y[20,88,305] = 1
y[24,210,107] = 1
y[24,75,86] = 1
y[24,106,78] = 1
y[24,270,300] = 1
y[24,231,106] = 1
y[24,86,270] = 1
y[24,300,412] = 1
y[24,412,-2] = 1
y[24,78,210] = 1
y[24,107,75] = 1
xD[45,76,88] = 975
xD[45,88,305] = 975
xD[46,76,88] = 1
xD[46,88,305] = 1
xD[48,88,305] = 907
xD[49,88,305] = 3
xD[66,263,149] = 484
xD[67,263,149] = 18
xD[87,210,107] = 541
xD[87,75,86] = 541
xD[87,106,78] = 541
xD[87,270,300] = 541
xD[87,231,106] = 541
xD[87,86,270] = 541
xD[87,78,210] = 541
xD[87,107,75] = 541
xD[178,106,78] = 785
xD[178,78,210] = 785
xD[179,106,78] = 1
xD[179,78,210] = 1
xD[181,210,107] = 121
xD[181,107,75] = 121
xD[182,210,107] = 75
xD[182,75,86] = 75
xD[182,107,75] = 75
xD[183,210,107] = 1
xD[184,210,107] = 17
xD[184,75,86] = 17
xD[184,86,270] = 17
xD[184,107,75] = 17
xD[185,75,86] = 339
xD[185,86,270] = 339
xD[185,107,75] = 339
xD[186,75,86] = 6
xD[186,270,300] = 6
xD[186,86,270] = 6
xD[186,107,75] = 6
xD[187,75,86] = 166
xD[187,86,270] = 166
xD[188,75,86] = 773
xD[188,270,300] = 773
xD[188,86,270] = 773
xD[189,75,86] = 1
xD[189,270,300] = 1
xD[189,86,270] = 1
xD[192,270,300] = 298
xD[193,270,300] = 199
xD[271,261,385] = 580
xD[274,263,149] = 484
xD[275,263,149] = 18
xD[295,400,261] = 580
xD[295,261,385] = 580
xD[297,75,86] = 6
xD[297,270,300] = 6
xD[297,86,270] = 6
xD[297,107,75] = 6
xD[298,75,86] = 773
xD[298,270,300] = 773
xD[298,86,270] = 773
xD[299,75,86] = 1
xD[299,270,300] = 0.999999
xD[299,86,270] = 1
xD[300,270,300] = 298
xD[301,270,300] = 199
xD[355,452,441] = 113
xD[356,452,441] = 54
xD[365,295,437] = 288
xD[365,464,295] = 288
xD[366,295,437] = 14
xD[366,464,295] = 14
xD[367,454,434] = 280
zE[71] = 871
zE[75] = 430
zE[76] = 1031
zE[78] = 287
zE[86] = 465
zE[88] = 1069
zE[99] = 960
zE[102] = 904
zE[106] = 238
zE[107] = 358
zE[149] = 452
zE[210] = 334
zE[226] = 824
zE[249] = 788
zE[261] = 238
zE[263] = 406
zE[264] = 543
zE[270] = 873
zE[295] = 721
zE[300] = 982
zE[302] = 1304
zE[303] = 1472
zE[305] = 1640
zE[324] = 5093
zE[354] = 618
zE[368] = 844
zE[369] = 1018
zE[371] = 778
zE[385] = 328
zE[386] = 496
zE[387] = 664
zE[388] = 832
zE[400] = 211
zE[401] = 379
zE[411] = 998
zE[412] = 1166
zE[434] = 617
zE[435] = 690
zE[437] = 858
zE[438] = 1026
zE[439] = 1194
zE[440] = 1362
zE[441] = 1530
zE[444] = 531
zE[445] = 621
zE[446] = 699
zE[448] = 867
zE[450] = 1035
zE[451] = 1125
zE[452] = 1293
zE[454] = 522
zE[457] = 467
zE[464] = 710
zE[465] = 878
zE[468] = 347
zE[475] = 243
zE[476] = 411
zE[477] = 579
zX[9] = 66
zX[71] = 886
zX[75] = 439
zX[76] = 1040
zX[78] = 297
zX[86] = 483
zX[88] = 1079
zX[99] = 978
zX[102] = 923
zX[106] = 260
zX[107] = 376
zX[131] = 288
zX[132] = 456
zX[133] = 624
zX[135] = 220
zX[136] = 388
zX[149] = 464
zX[164] = 94
zX[179] = 158
zX[181] = 326
zX[210] = 350
zX[226] = 833
zX[231] = 171
zX[249] = 810
zX[261] = 259
zX[263] = 427
zX[264] = 564
zX[270] = 896
zX[282] = 92
zX[283] = 260
zX[285] = 428
zX[287] = 596
zX[295] = 736
zX[300] = 1030
zX[302] = 1316
zX[303] = 1484
zX[305] = 1652
zX[324] = 5093
zX[354] = 651
zX[368] = 854
zX[369] = 1028
zX[371] = 793
zX[385] = 347
zX[386] = 515
zX[387] = 683
zX[388] = 851
zX[400] = 230
zX[401] = 398
zX[411] = 1011
zX[412] = 1179
zX[434] = 642
zX[435] = 723
zX[437] = 891
zX[438] = 1059
zX[439] = 1227
zX[440] = 1395
zX[441] = 1563
zX[444] = 550
zX[445] = 637
zX[446] = 718
zX[448] = 886
zX[450] = 1054
zX[451] = 1141
zX[452] = 1309
zX[454] = 546
zX[457] = 494
zX[464] = 722
zX[465] = 890
zX[468] = 395
zX[475] = 287
zX[476] = 455
zX[477] = 623
phi[1] = 227
phi[2] = 227
phi[3] = 227
phi[5] = 799
phi[6] = 1303
phi[7] = 631
phi[8] = 631
phi[9] = 945
phi[13] = 1096
phi[14] = 1096
phi[15] = 565
phi[16] = 1069
phi[20] = 1558
phi[24] = 1008

### All variables with value smaller zero:

### End

real	1454m23.324s
user	4878m18.351s
sys	893m22.743s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
