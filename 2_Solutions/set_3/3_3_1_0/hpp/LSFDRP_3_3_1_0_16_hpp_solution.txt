	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_3_1_0_fr.p
Instance LSFDP: LSFDRP_3_3_1_0_fd_16.p

This object contains the information regarding the instance 3_3_1_0 for a duration of 16 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took   0.4118 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.999619 sec.
***  |   |- Create additional vessel information,				took 0.005467 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 28056 rows, 72142 columns and 518215 nonzeros
Variable types: 20332 continuous, 51810 integer (51810 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -1.45872e+07
Presolve removed 23968 rows and 45647 columns
Presolve time: 1.54s
Presolved: 4088 rows, 26495 columns, 122449 nonzeros
Variable types: 246 continuous, 26249 integer (25882 binary)

Root relaxation: objective 6.664725e+06, 4406 iterations, 0.38 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 6664725.18    0  309 -1.459e+07 6664725.18   146%     -    2s
H    0     0                    2579966.1260 6664725.18   158%     -    2s
H    0     0                    3955671.2419 6664725.18  68.5%     -    3s
     0     0 6226057.37    0  268 3955671.24 6226057.37  57.4%     -    3s
     0     0 6221874.17    0  248 3955671.24 6221874.17  57.3%     -    3s
     0     0 6221466.58    0  266 3955671.24 6221466.58  57.3%     -    3s
     0     0 6091076.26    0  236 3955671.24 6091076.26  54.0%     -    4s
     0     0 6080377.47    0  233 3955671.24 6080377.47  53.7%     -    4s
     0     0 6078000.53    0  254 3955671.24 6078000.53  53.7%     -    4s
     0     0 6071704.99    0  237 3955671.24 6071704.99  53.5%     -    4s
     0     0 6065798.03    0  243 3955671.24 6065798.03  53.3%     -    4s
     0     0 6064226.74    0  247 3955671.24 6064226.74  53.3%     -    4s
H    0     0                    4168463.7527 6064226.74  45.5%     -    4s
     0     0 6064226.74    0  247 4168463.75 6064226.74  45.5%     -    5s
     0     2 6064226.74    0  247 4168463.75 6064226.74  45.5%     -    5s
H   65    26                    4168463.7939 5026994.47  20.6%   107    8s
*  162    29              17    4214992.6168 4838259.93  14.8%  73.8    9s
*  164    29              15    4257089.9254 4838259.93  13.7%  73.0    9s
*  251     4              14    4281406.4046 4420174.96  3.24%  62.8    9s
*  271     1              19    4286191.4212 4333614.53  1.11%  59.0    9s

Cutting planes:
  Gomory: 9
  Implied bound: 8
  Clique: 2
  MIR: 2
  Flow cover: 1
  Zero half: 8

Explored 277 nodes (21240 simplex iterations) in 9.69 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: 4.28619e+06 4.28141e+06 4.25709e+06 ... -1.45872e+07
No other solutions better than 4.28619e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 4.286191421211e+06, best bound 4.286191421211e+06, gap 0.0000%
Took  12.924251556396484
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 35024 rows, 32313 columns and 113277 nonzeros
Variable types: 30360 continuous, 1953 integer (1953 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 35024 rows and 32313 columns
Presolve time: 0.04s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.06 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 3.0309e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 3.030895421208e+06, best bound 3.030895421208e+06, gap 0.0000%

### Instance
### LSFDRP_3_3_1_0_fd_16

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 83
### |A|: 79
### |A^i|: 79
### |A^f|: 0
### |M|: 380
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 3,030,895.4212 (3030895.4212)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 480, 441, -3]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 480 (TRMER 
###  |  In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to 67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to 67
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to 67
###  |   |- and carried Demand 295,   19.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 444, 464, 295, 437, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 444 (EGPSD In@531/Out@550) - 464 (OMSLL In@710/Out@722) - 295 (OMS
###  |  LL In@721/Out@736) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 365,  288.0 containers from 464 to 437
###  |   |- and carried Demand 366,   14.0 containers from 464 to 437
###  |   |- and carried Demand 372,  164.0 containers from 444 to 437
###  |   |- and carried Demand 373,   58.0 containers from 444 to 437
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 448, 449, 369, 439, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUSK In@1018/Out@1028) - 439 (AE
###  |  JEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel  8 has the path [287, 479, 440, -3]
###  |  287 (NLRTM Out@596) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [179, 468, 435, -3]
###  |  179 (FRLEH Out@158) - 468 (TRAMB In@347/Out@395) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 16 has the path [181, 370, 465, 296, 438, -3]
###  |  181 (FRLEH Out@326) - 370 (TRZMK In@617/Out@631) - 465 (OMSLL In@878/Out@890) - 296 (OMSLL In@889/Out@904) - 438 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 359,  111.0 containers from 465 to 438
###  |   |- and carried Demand 360,   35.0 containers from 465 to 438
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 385, 262, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAP
###  |  TM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  312.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 14 has the path [136, 387, -1]
###  |  136 (DKAAR Out@388) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 11 has the path [13, 425, 421, 411, -2]
###  |  13 (BEANR Out@402) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUMM
###  |  Y_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 477, 360, 302, 0]
###  |  132 (DEHAM Out@456) - 477 (TRMER In@579/Out@623) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel  3 has the path [133, 478, 361, 303, 0]
###  |  133 (DEHAM Out@624) - 478 (TRMER In@747/Out@791) - 361 (TRALI In@836/Out@854) - 303 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from 76 to 305
###  |   |- and carried Demand  46,    1.0 containers from 76 to 305
###  |   |- and carried Demand  48,  907.0 containers from 88 to 305
###  |   |- and carried Demand  49,    3.0 containers from 88 to 305
###  |
###  |
### Vessels not used in the solution: [4, 9, 10, 12, 17, 18, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m18.817s
user	0m36.569s
sys	0m2.117s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
