	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_3_1_0_fr.p
Instance LSFDP: LSFDRP_3_3_1_0_fd_31.p

This object contains the information regarding the instance 3_3_1_0 for a duration of 31 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took   0.4185 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.031506 sec.
***  |   |- Create additional vessel information,				took 0.005775 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 26519 rows, 66706 columns and 475517 nonzeros
Variable types: 18600 continuous, 48106 integer (48106 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 23085 rows and 44335 columns
Presolve time: 2.29s
Presolved: 3434 rows, 22371 columns, 104177 nonzeros
Variable types: 172 continuous, 22199 integer (21928 binary)

Root relaxation: objective 5.168367e+06, 3438 iterations, 0.31 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 5168366.78    0  360          - 5168366.78      -     -    2s
H    0     0                    422819.04110 5168366.78  1122%     -    2s
H    0     0                    2434479.4918 5168366.78   112%     -    3s
     0     0 4618726.10    0  316 2434479.49 4618726.10  89.7%     -    4s
     0     0 4614755.30    0  209 2434479.49 4614755.30  89.6%     -    4s
     0     0 4500326.29    0  212 2434479.49 4500326.29  84.9%     -    4s
     0     0 4484399.06    0  342 2434479.49 4484399.06  84.2%     -    4s
H    0     0                    2671627.1822 4484399.06  67.9%     -    4s
     0     0 4309787.07    0  209 2671627.18 4309787.07  61.3%     -    4s
     0     0 4309787.07    0  209 2671627.18 4309787.07  61.3%     -    4s
     0     0 4309787.07    0  209 2671627.18 4309787.07  61.3%     -    4s
     0     0 4309787.07    0  209 2671627.18 4309787.07  61.3%     -    4s
     0     0 4309787.07    0  208 2671627.18 4309787.07  61.3%     -    5s
H    0     0                    2674659.8753 4309787.07  61.1%     -    6s
     0     2 4309787.07    0  208 2674659.88 4309787.07  61.1%     -    6s
   171    52 2938366.18   21  253 2674659.88 3552268.02  32.8%  75.3   10s
H  173    52                    2707787.2991 3552268.02  31.2%  74.7   10s
H  177    53                    2738240.8672 3552268.02  29.7%  73.6   10s

Cutting planes:
  Gomory: 4
  Implied bound: 12
  Clique: 8
  MIR: 7
  Flow cover: 1
  Zero half: 8

Explored 477 nodes (23561 simplex iterations) in 10.73 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: 2.73824e+06 2.70779e+06 2.67466e+06 ... 422819

Optimal solution found (tolerance 1.00e-04)
Best objective 2.738240867169e+06, best bound 2.738240867169e+06, gap 0.0000%
Took  13.794066667556763
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 32147 rows, 29457 columns and 102938 nonzeros
Variable types: 27672 continuous, 1785 integer (1785 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 32147 rows and 29457 columns
Presolve time: 0.04s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.05 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 1.56644e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.566440867167e+06, best bound 1.566440867167e+06, gap 0.0000%

### Instance
### LSFDRP_3_3_1_0_fd_31

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 76
### |A|: 72
### |A^i|: 72
### |A^f|: 0
### |M|: 380
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,566,440.8672 (1566440.8672)
###
### Service 7 (ME3) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 385, 262, 148, 395, 68, 480, 441, -3]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 148 (ESALG
###  |   In@421/Out@433) - 395 (GWOXB In@631/Out@710) - 68 (CMDLA In@796/Out@842) - 480 (TRMER In@1083/Out@1127) - 441 (AEJEA
###  |   In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to 68
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand  59,  161.0 containers from 261 to 68
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to 68
###  |   |- and carried Demand 295,   19.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 476, 444, 464, 295, 437, -3]
###  |  283 (NLRTM Out@260) - 476 (TRMER In@411/Out@455) - 444 (EGPSD In@531/Out@550) - 464 (OMSLL In@710/Out@722) - 295 (OMS
###  |  LL In@721/Out@736) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 365,  288.0 containers from 464 to 437
###  |   |- and carried Demand 366,   14.0 containers from 464 to 437
###  |   |- and carried Demand 372,  164.0 containers from 444 to 437
###  |   |- and carried Demand 373,   58.0 containers from 444 to 437
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 448, 449, 369, 439, -3]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUSK In@1018/Out@1028) - 439 (AE
###  |  JEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel  8 has the path [287, 479, 440, -3]
###  |  287 (NLRTM Out@596) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 15 has the path [179, 468, 435, -3]
###  |  179 (FRLEH Out@158) - 468 (TRAMB In@347/Out@395) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 16 has the path [181, 477, 465, 296, 438, -3]
###  |  181 (FRLEH Out@326) - 477 (TRMER In@579/Out@623) - 465 (OMSLL In@878/Out@890) - 296 (OMSLL In@889/Out@904) - 438 (AEJ
###  |  EA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 359,  111.0 containers from 465 to 438
###  |   |- and carried Demand 360,   35.0 containers from 465 to 438
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 401, 263, 149, 388, -1]
###  |  131 (DEHAM Out@288) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 14 has the path [136, 386, -1]
###  |  136 (DKAAR Out@388) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 18 has the path [193, 425, 421, 411, -2]
###  |  193 (GBFXT Out@470) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  3 has the path [133, 478, 361, 303, 0]
###  |  133 (DEHAM Out@624) - 478 (TRMER In@747/Out@791) - 361 (TRALI In@836/Out@854) - 303 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel  4 has the path [134, 365, 305, 0]
###  |  134 (DEHAM Out@792) - 365 (TRAMB In@1040/Out@1064) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 13 has the path [135, 360, 302, 0]
###  |  135 (DKAAR Out@220) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [9, 10, 11, 12, 17, 19, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m19.222s
user	0m35.807s
sys	0m2.654s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
