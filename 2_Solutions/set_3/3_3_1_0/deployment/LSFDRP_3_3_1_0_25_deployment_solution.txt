	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_3_1_0_fr.p
Instance LSFDP: LSFDRP_3_3_1_0_fd_25.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 93 rows, 153 columns and 462 nonzeros
Variable types: 0 continuous, 153 integer (153 binary)
Coefficient statistics:
  Matrix range     [1e+00, 6e+00]
  Objective range  [5e+05, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 6e+00]
Found heuristic solution: objective -5.89441e+07
Presolve removed 20 rows and 24 columns
Presolve time: 0.01s
Presolved: 73 rows, 129 columns, 346 nonzeros
Variable types: 0 continuous, 129 integer (129 binary)

Root relaxation: objective -0.000000e+00, 46 iterations, 0.00 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0   -0.00000    0    5 -5.894e+07   -0.00000   100%     -    0s
H    0     0                      -0.0000000   -0.00000  0.00%     -    0s

Explored 1 nodes (46 simplex iterations) in 0.01 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -0 -5.89441e+07 
No other solutions better than -0

Optimal solution found (tolerance 1.00e-04)
Best objective 1.862645149231e-09, best bound 1.862645149231e-09, gap 0.0000%

### Objective value in 1k: 0.00
### Objective value: 0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 24(WAF7): 18, (PMax25), 3
### 177(WCSA): 18, (PMax25), 3
### 121(SAE): 30, (PMax35), 2
### 7(ME3): 19, (PMax28), 6
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
rho[121,30] = 1
rho[7,19] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
eta[7,19,1] = 1
eta[7,19,2] = 1
eta[7,19,3] = 1
eta[7,19,4] = 1
eta[7,19,5] = 1
eta[7,19,6] = 1
y[1,24] = 1
y[2,24] = 1
y[3,24] = 1
y[4,177] = 1
y[5,7] = 1
y[6,7] = 1
y[7,7] = 1
y[8,7] = 1
y[12,121] = 1
y[13,177] = 1
y[14,177] = 1
y[15,7] = 1
y[16,7] = 1
y[17,121] = 1

### All variables with value zero:
rho[24,19] = 0
rho[24,30] = 0
rho[177,19] = 0
rho[177,30] = 0
rho[121,18] = 0
rho[121,19] = 0
rho[7,18] = 0
rho[7,30] = 0
eta[24,18,4] = 0
eta[24,19,1] = 0
eta[24,19,2] = 0
eta[24,19,3] = 0
eta[24,19,4] = 0
eta[24,30,1] = 0
eta[24,30,2] = 0
eta[24,30,3] = 0
eta[24,30,4] = 0
eta[177,19,1] = 0
eta[177,19,2] = 0
eta[177,19,3] = 0
eta[177,30,1] = 0
eta[177,30,2] = 0
eta[177,30,3] = 0
eta[121,18,1] = 0
eta[121,18,2] = 0
eta[121,19,1] = 0
eta[121,19,2] = 0
eta[7,18,1] = 0
eta[7,18,2] = 0
eta[7,18,3] = 0
eta[7,18,4] = 0
eta[7,18,5] = 0
eta[7,18,6] = 0
eta[7,30,1] = 0
eta[7,30,2] = 0
eta[7,30,3] = 0
eta[7,30,4] = 0
eta[7,30,5] = 0
eta[7,30,6] = 0
y[1,177] = 0
y[1,121] = 0
y[1,7] = 0
y[2,177] = 0
y[2,121] = 0
y[2,7] = 0
y[3,177] = 0
y[3,121] = 0
y[3,7] = 0
y[4,24] = 0
y[4,121] = 0
y[4,7] = 0
y[5,24] = 0
y[5,177] = 0
y[5,121] = 0
y[6,24] = 0
y[6,177] = 0
y[6,121] = 0
y[7,24] = 0
y[7,177] = 0
y[7,121] = 0
y[8,24] = 0
y[8,177] = 0
y[8,121] = 0
y[9,24] = 0
y[9,177] = 0
y[9,121] = 0
y[9,7] = 0
y[10,24] = 0
y[10,177] = 0
y[10,121] = 0
y[10,7] = 0
y[11,24] = 0
y[11,177] = 0
y[11,121] = 0
y[11,7] = 0
y[12,24] = 0
y[12,177] = 0
y[12,7] = 0
y[13,24] = 0
y[13,121] = 0
y[13,7] = 0
y[14,24] = 0
y[14,121] = 0
y[14,7] = 0
y[15,24] = 0
y[15,177] = 0
y[15,121] = 0
y[16,24] = 0
y[16,177] = 0
y[16,121] = 0
y[17,24] = 0
y[17,177] = 0
y[17,7] = 0
y[18,24] = 0
y[18,177] = 0
y[18,121] = 0
y[18,7] = 0
y[19,24] = 0
y[19,177] = 0
y[19,121] = 0
y[19,7] = 0
y[20,24] = 0
y[20,177] = 0
y[20,121] = 0
y[20,7] = 0
y[21,24] = 0
y[21,177] = 0
y[21,121] = 0
y[21,7] = 0
y[22,24] = 0
y[22,177] = 0
y[22,121] = 0
y[22,7] = 0
y[23,24] = 0
y[23,177] = 0
y[23,121] = 0
y[23,7] = 0
y[24,24] = 0
y[24,177] = 0
y[24,121] = 0
y[24,7] = 0

### End

real	0m0.957s
user	0m0.418s
sys	0m0.111s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
