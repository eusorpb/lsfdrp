	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_1_1_1_fr.p
Instance LSFDP: LSFDRP_3_1_1_1_fd_22.p

This object contains the information regarding the instance 3_1_1_1 for a duration of 22 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.336537 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.709283 sec.
***  |   |- Create additional vessel information,				took 0.001401 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 11900 rows, 14946 columns and 103105 nonzeros
Variable types: 6216 continuous, 8730 integer (8730 binary)
Coefficient statistics:
  Matrix range     [1e+00, 3e+03]
  Objective range  [2e+00, 2e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 3e+03]
Found heuristic solution: objective -6934321.387
Presolve removed 10370 rows and 9843 columns
Presolve time: 0.64s
Presolved: 1530 rows, 5103 columns, 31102 nonzeros
Variable types: 266 continuous, 4837 integer (4762 binary)

Root relaxation: objective 6.118422e+06, 1510 iterations, 0.07 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 5650080.24    0  205 -6934321.4 5650080.24   181%     -    0s
H    0     0                    1524436.7542 5650080.24   271%     -    0s
H    0     0                    1759591.2820 5650080.24   221%     -    0s
     0     0 4804051.24    0  190 1759591.28 4804051.24   173%     -    1s
     0     0 4574657.02    0  187 1759591.28 4574657.02   160%     -    1s
     0     0 4140867.69    0  177 1759591.28 4140867.69   135%     -    1s
     0     0 4139991.76    0  202 1759591.28 4139991.76   135%     -    1s
     0     0 4130337.56    0  199 1759591.28 4130337.56   135%     -    1s
     0     0 4128577.73    0  189 1759591.28 4128577.73   135%     -    1s
     0     0 4111832.20    0  199 1759591.28 4111832.20   134%     -    1s
     0     0 4088839.46    0  222 1759591.28 4088839.46   132%     -    1s
     0     0 4088760.28    0  209 1759591.28 4088760.28   132%     -    1s
     0     0 4088760.28    0  208 1759591.28 4088760.28   132%     -    1s
     0     2 4088760.28    0  207 1759591.28 4088760.28   132%     -    1s
H   27    21                    1955609.5760 3552536.77  81.7%  94.9    2s
H   28    22                    2094741.0739 3552536.77  69.6%  98.8    2s
*  184   101              13    2153358.1053 3253754.28  51.1%  48.2    2s
H  654    59                    2190908.9005 2384263.09  8.83%  32.2    3s

Cutting planes:
  Gomory: 12
  Implied bound: 2
  MIR: 18
  Flow cover: 2
  Zero half: 1

Explored 756 nodes (24900 simplex iterations) in 3.14 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: 2.19091e+06 2.15336e+06 2.09474e+06 ... -6.93432e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 2.190908900463e+06, best bound 2.190908900463e+06, gap 0.0000%
Took  4.1459550857543945
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 12400 rows, 10791 columns and 38982 nonzeros
Variable types: 9972 continuous, 819 integer (819 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 12400 rows and 10791 columns
Presolve time: 0.02s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.02 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 2.23457e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 2.234567598503e+06, best bound 2.234567598503e+06, gap 0.0000%

### Instance
### LSFDRP_3_1_1_1_fd_22

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 35
### |A|: 33
### |A^i|: 31
### |A^f|: 2
### |M|: 296
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 2,234,567.5985 (2234567.5985)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 58, 324, 305, 0]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 58 (CLLQN I
###  |  n@1240/Out@1258) - 324 (PECLL In@1399/Out@1514) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to 67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to 67
###  |   |- and carried Demand  60,   19.0 containers from 261 to 147
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to 67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel  6 has the path [283, 180, 262, 148, 395, 280, 30, 302, 0]
###  |  283 (NLRTM Out@260) - 180 (FRLEH In@279/Out@295) - 262 (MAPTM In@375/Out@396) - 148 (ESALG In@421/Out@433) - 395 (GWO
###  |  XB In@631/Out@710) - 280 (NGAPP In@793/Out@840) - 30 (BJCOO In@849/Out@886) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY
###  |  _END Ziel-Service 177)
###  |   |- and carried Demand  16,   74.0 containers from 283 to 148
###  |   |- and carried Demand  17,    5.0 containers from 283 to 180
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand 258,   74.0 containers from 283 to 148
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |
###  |- Vessel 15 has the path [179, 385, 151, 371, 303, 0]
###  |  179 (FRLEH Out@158) - 385 (ESALG In@328/Out@347) - 151 (ESALG In@625/Out@670) - 371 (TRZMK In@778/Out@793) - 303 (PAB
###  |  LB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 140,  552.0 containers from 151 to 303
###  |   |- and carried Demand 141,   13.0 containers from 151 to 303
###  |
###  |
### Vessels not used in the solution: [3, 4, 7, 8, 9, 10, 11, 12, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m7.143s
user	0m12.530s
sys	0m0.696s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
