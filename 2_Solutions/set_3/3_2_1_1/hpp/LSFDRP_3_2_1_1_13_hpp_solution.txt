	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_2_1_1_fr.p
Instance LSFDP: LSFDRP_3_2_1_1_fd_13.p

This object contains the information regarding the instance 3_2_1_1 for a duration of 13 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.394213 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.928573 sec.
***  |   |- Create additional vessel information,				took 0.003115 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 14169 rows, 19249 columns and 136457 nonzeros
Variable types: 7987 continuous, 11262 integer (11262 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 12353 rows and 13677 columns
Presolve time: 0.89s
Presolved: 1816 rows, 5572 columns, 34157 nonzeros
Variable types: 250 continuous, 5322 integer (5243 binary)
Found heuristic solution: objective -4812220.703

Root relaxation: objective 7.423120e+06, 1709 iterations, 0.09 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 7412901.95    0  210 -4812220.7 7412901.95   254%     -    1s
H    0     0                    3676387.5134 7412901.95   102%     -    1s
     0     0 6343249.41    0  263 3676387.51 6343249.41  72.5%     -    1s
     0     0 6314346.31    0  262 3676387.51 6314346.31  71.8%     -    1s
H    0     0                    3755103.6925 6314346.31  68.2%     -    1s
     0     0 6080728.76    0  217 3755103.69 6080728.76  61.9%     -    1s
     0     0 6052354.88    0  248 3755103.69 6052354.88  61.2%     -    1s
     0     0 6019473.56    0  258 3755103.69 6019473.56  60.3%     -    1s
     0     0 6004612.31    0  256 3755103.69 6004612.31  59.9%     -    1s
     0     0 5850823.63    0  242 3755103.69 5850823.63  55.8%     -    1s
     0     0 5850823.63    0  242 3755103.69 5850823.63  55.8%     -    2s
     0     2 5850823.63    0  242 3755103.69 5850823.63  55.8%     -    2s
H   27    16                    3826484.8918 5451956.71  42.5%  95.9    2s
H   30    18                    4083735.6403 5406680.02  32.4%  92.3    2s
*  152    76              41    4143410.1253 5147518.02  24.2%  55.1    2s
H  188    97                    4180960.9205 5014746.96  19.9%  59.3    3s
H  190    80                    4361209.4651 5014746.96  15.0%  58.7    3s
*  268    15              19    4398760.2602 4801362.65  9.15%  51.7    3s

Cutting planes:
  Gomory: 10
  Implied bound: 3
  MIR: 18
  Flow cover: 1
  Zero half: 4

Explored 306 nodes (17444 simplex iterations) in 3.39 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: 4.39876e+06 4.36121e+06 4.18096e+06 ... -4.81222e+06
No other solutions better than 4.39876e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 4.398760260237e+06, best bound 4.398760260237e+06, gap 0.0000%
Took  5.134742021560669
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 18869 rows, 16652 columns and 59392 nonzeros
Variable types: 15560 continuous, 1092 integer (1092 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+01, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 18869 rows and 16652 columns
Presolve time: 0.04s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.06 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 3.95102e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 3.951018958277e+06, best bound 3.951018958277e+06, gap 0.0000%

### Instance
### LSFDRP_3_2_1_1_fd_13

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 47
### |A|: 44
### |A^i|: 42
### |A^f|: 2
### |M|: 348
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 3,951,018.9583 (3951018.9583)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 262, 148, 411, -2]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 262 (MAPTM In@375/Out@396) - 148 (ESALG In@421/Out@433) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 178, 260, 400, 261, 147, 394, 67, 58, 324, 305, 0]
###  |  282 (NLRTM Out@92) - 178 (FRLEH In@111/Out@127) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPT
###  |  M In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 58 (CLLQN I
###  |  n@1240/Out@1258) - 324 (PECLL In@1399/Out@1514) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  10,  161.0 containers from 260 to 67
###  |   |- and carried Demand  11,   19.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to 67
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to 67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel  8 has the path [287, 183, 265, 302, 0]
###  |  287 (NLRTM Out@596) - 183 (FRLEH In@615/Out@631) - 265 (MAPTM In@711/Out@732) - 302 (PABLB In@1304/Out@1316) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 15 has the path [179, 385, 151, 371, 303, 0]
###  |  179 (FRLEH Out@158) - 385 (ESALG In@328/Out@347) - 151 (ESALG In@625/Out@670) - 371 (TRZMK In@778/Out@793) - 303 (PAB
###  |  LB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 140,  552.0 containers from 151 to 303
###  |   |- and carried Demand 141,   13.0 containers from 151 to 303
###  |
###  |
### Vessels not used in the solution: [3, 4, 6, 7, 10, 11, 12, 14, 16, 17, 18, 19, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m10.135s
user	0m14.346s
sys	0m0.844s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
