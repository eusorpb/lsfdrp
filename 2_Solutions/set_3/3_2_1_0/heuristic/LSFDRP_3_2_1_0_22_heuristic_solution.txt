	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_2_1_0_fr.p
Instance LSFDP: LSFDRP_3_2_1_0_fd_22.p


*****************************************************************************************************************************
*** Solve LSFDRP_3_2_1_0_fd_22.p with matheuristics.
***  |
***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.366835 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.871058 sec.
***  |   |- Create additional vessel information,				took 0.003107 sec.
***  |
***  |   |- Create ranking for FDP solutions,				took 0.013469 sec.
***  |- Start solving instance with matheuristics, run 1
***  |   |- Create ranking for FDP solutions,				took 0.013503 sec.
***  |   |- Calculation finished, timelimit reached 			took   598.69 sec.
***  |
***  |- Start solving instance with matheuristics, run 2
***  |   |- Create ranking for FDP solutions,				took 0.022395 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.15 sec.
***  |
***  |- Start solving instance with matheuristics, run 3
***  |   |- Create ranking for FDP solutions,				took 0.017812 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.87 sec.
***  |
***  |- Start solving instance with matheuristics, run 4
***  |   |- Create ranking for FDP solutions,				took 0.026613 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.81 sec.
***  |
***  |- Start solving instance with matheuristics, run 5
***  |   |- Create ranking for FDP solutions,				took 0.021112 sec.
***  |   |- Calculation finished, timelimit reached 			took   600.67 sec.
***  |
***  |- Start solving instance with matheuristics, run 6
***  |   |- Create ranking for FDP solutions,				took 0.011636 sec.
***  |   |- Calculation finished, timelimit reached 			took   599.61 sec.
***  |
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
###
###
#############################################################################################################################
### Solution run 1
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,774,659.7219 (1774659.7219)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 400, 261, 147, 411, -2]
###  |  9 (BEANR Out@66) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  4 has the path [134, 302, 0]
###  |  134 (DEHAM Out@792) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 13 has the path [135, 57, 324, 303, 0]
###  |  135 (DKAAR Out@220) - 57 (CLLQN In@1072/Out@1090) - 324 (PECLL In@1251/Out@1328) - 303 (PABLB In@1472/Out@1484) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 239,  540.0 containers from 57 to 303
###  |   |- and carried Demand 240,   69.0 containers from 57 to 303
###  |   |- and carried Demand 241,  138.0 containers from 57 to 303
###  |
###  |- Vessel 14 has the path [136, 58, 63, 305, 0]
###  |  136 (DKAAR Out@388) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 2
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,774,659.7219 (1774659.7219)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 400, 261, 147, 411, -2]
###  |  9 (BEANR Out@66) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  4 has the path [134, 302, 0]
###  |  134 (DEHAM Out@792) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 13 has the path [135, 57, 324, 303, 0]
###  |  135 (DKAAR Out@220) - 57 (CLLQN In@1072/Out@1090) - 324 (PECLL In@1251/Out@1328) - 303 (PABLB In@1472/Out@1484) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 239,  540.0 containers from 57 to 303
###  |   |- and carried Demand 240,   69.0 containers from 57 to 303
###  |   |- and carried Demand 241,  138.0 containers from 57 to 303
###  |
###  |- Vessel 14 has the path [136, 58, 63, 305, 0]
###  |  136 (DKAAR Out@388) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 3
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,774,659.7219 (1774659.7219)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 400, 261, 147, 411, -2]
###  |  9 (BEANR Out@66) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  4 has the path [134, 302, 0]
###  |  134 (DEHAM Out@792) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 13 has the path [135, 57, 324, 303, 0]
###  |  135 (DKAAR Out@220) - 57 (CLLQN In@1072/Out@1090) - 324 (PECLL In@1251/Out@1328) - 303 (PABLB In@1472/Out@1484) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 239,  540.0 containers from 57 to 303
###  |   |- and carried Demand 240,   69.0 containers from 57 to 303
###  |   |- and carried Demand 241,  138.0 containers from 57 to 303
###  |
###  |- Vessel 14 has the path [136, 58, 63, 305, 0]
###  |  136 (DKAAR Out@388) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 4
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,704,767.0211 (1704767.0211)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 14 has the path [136, 386, -1]
###  |  136 (DKAAR Out@388) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 400, 261, 147, 411, -2]
###  |  9 (BEANR Out@66) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 394, 29, 302, 0]
###  |  131 (DEHAM Out@288) - 394 (GWOXB In@463/Out@542) - 29 (BJCOO In@712/Out@749) - 302 (PABLB In@1304/Out@1316) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |
###  |- Vessel  3 has the path [133, 58, 63, 305, 0]
###  |  133 (DEHAM Out@624) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |- Vessel 13 has the path [135, 57, 324, 303, 0]
###  |  135 (DKAAR Out@220) - 57 (CLLQN In@1072/Out@1090) - 324 (PECLL In@1251/Out@1328) - 303 (PABLB In@1472/Out@1484) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 239,  540.0 containers from 57 to 303
###  |   |- and carried Demand 240,   69.0 containers from 57 to 303
###  |   |- and carried Demand 241,  138.0 containers from 57 to 303
###  |
###  |
### Vessels not used in the solution: [4, 5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 5
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,704,767.0211 (1704767.0211)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 14 has the path [136, 386, -1]
###  |  136 (DKAAR Out@388) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 400, 261, 147, 411, -2]
###  |  9 (BEANR Out@66) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 394, 29, 302, 0]
###  |  131 (DEHAM Out@288) - 394 (GWOXB In@463/Out@542) - 29 (BJCOO In@712/Out@749) - 302 (PABLB In@1304/Out@1316) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |
###  |- Vessel  3 has the path [133, 58, 63, 305, 0]
###  |  133 (DEHAM Out@624) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |- Vessel 13 has the path [135, 57, 324, 303, 0]
###  |  135 (DKAAR Out@220) - 57 (CLLQN In@1072/Out@1090) - 324 (PECLL In@1251/Out@1328) - 303 (PABLB In@1472/Out@1484) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 239,  540.0 containers from 57 to 303
###  |   |- and carried Demand 240,   69.0 containers from 57 to 303
###  |   |- and carried Demand 241,  138.0 containers from 57 to 303
###  |
###  |
### Vessels not used in the solution: [4, 5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
###
###
#############################################################################################################################
### Solution run 6
#############################################################################################################################
###
### Results
###
### The objective of the solution is 2,037,207.1913 (2037207.1913)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 14 has the path [136, 386, -1]
###  |  136 (DKAAR Out@388) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 385, 262, 148, 411, -2]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 148 (ESALG I
###  |  n@421/Out@433) - 411 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 394, 28, 302, 0]
###  |  131 (DEHAM Out@288) - 394 (GWOXB In@463/Out@542) - 28 (BJCOO In@681/Out@718) - 302 (PABLB In@1304/Out@1316) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |
###  |- Vessel  3 has the path [133, 58, 63, 305, 0]
###  |  133 (DEHAM Out@624) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |- Vessel  4 has the path [134, 303, 0]
###  |  134 (DEHAM Out@792) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++
+++ The average objective is 1,795,120.0665 (1795120.0665)
+++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

real	60m11.240s
user	80m49.932s
sys	3m29.389s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
