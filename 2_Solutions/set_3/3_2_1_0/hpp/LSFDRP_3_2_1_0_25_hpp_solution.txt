	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_2_1_0_fr.p
Instance LSFDP: LSFDRP_3_2_1_0_fd_25.p

This object contains the information regarding the instance 3_2_1_0 for a duration of 25 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.324689 sec.
***  |   |- Calculate demand structure for node flow model,		took 0.864274 sec.
***  |   |- Create additional vessel information,				took 0.002831 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 14657 rows, 20553 columns and 140974 nonzeros
Variable types: 8580 continuous, 11973 integer (11973 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Found heuristic solution: objective -1.09179e+07
Presolve removed 13236 rows and 16504 columns
Presolve time: 0.74s
Presolved: 1421 rows, 4049 columns, 27125 nonzeros
Variable types: 189 continuous, 3860 integer (3817 binary)

Root relaxation: objective 4.850542e+06, 1006 iterations, 0.03 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 4850542.17    0  175 -1.092e+07 4850542.17   144%     -    0s
H    0     0                    697983.43094 4850542.17   595%     -    0s
H    0     0                    3191035.0638 4850542.17  52.0%     -    0s
     0     0 3974747.05    0   90 3191035.06 3974747.05  24.6%     -    1s
H    0     0                    3205798.2968 3974747.05  24.0%     -    1s
     0     0 3950793.14    0   94 3205798.30 3950793.14  23.2%     -    1s
     0     0 3850039.24    0   81 3205798.30 3850039.24  20.1%     -    1s
     0     0 3850039.24    0  150 3205798.30 3850039.24  20.1%     -    1s
     0     0 3849887.52    0   54 3205798.30 3849887.52  20.1%     -    1s
H    0     0                    3248039.6701 3849887.52  18.5%     -    1s
H    0     0                    3255839.1316 3849887.52  18.2%     -    1s
H    0     0                    3346763.3415 3849887.52  15.0%     -    1s
     0     0 3711977.71    0   44 3346763.34 3711977.71  10.9%     -    1s
H    0     0                    3362222.5873 3711977.71  10.4%     -    1s
H    0     0                    3438515.8576 3711977.71  7.95%     -    1s
     0     0 3707292.58    0   44 3438515.86 3707292.58  7.82%     -    1s
     0     0     cutoff    0      3438515.86 3438515.86  0.00%     -    1s

Cutting planes:
  Gomory: 10
  Implied bound: 5
  MIR: 8
  Flow cover: 1
  Zero half: 6

Explored 1 nodes (2273 simplex iterations) in 1.60 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: 3.43852e+06 3.36222e+06 3.34676e+06 ... -1.09179e+07

Optimal solution found (tolerance 1.00e-04)
Best objective 3.438515857627e+06, best bound 3.438515857627e+06, gap 0.0000%
Took  3.174471139907837
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 16845 rows, 14724 columns and 52074 nonzeros
Variable types: 13752 continuous, 972 integer (972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 16845 rows and 14724 columns
Presolve time: 0.02s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.03 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 1.47712e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.477115857627e+06, best bound 1.477115857627e+06, gap 0.0000%

### Instance
### LSFDRP_3_2_1_0_fd_25

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 42
### |A|: 39
### |A^i|: 39
### |A^f|: 0
### |M|: 348
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,477,115.8576 (1477115.8576)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 384, 260, 146, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 146 (ESALG In@253/Out@265) - 385 (ESAL
###  |  G In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG
###  |   In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand  20,  312.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 400, 261, 147, 411, -2]
###  |  9 (BEANR Out@66) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM In@238/Out@259) - 147 (ESALG In@284/Out@296) - 411 (HNPCR 
###  |  In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  4 has the path [134, 303, 0]
###  |  134 (DEHAM Out@792) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 13 has the path [135, 394, 28, 302, 0]
###  |  135 (DKAAR Out@220) - 394 (GWOXB In@463/Out@542) - 28 (BJCOO In@681/Out@718) - 302 (PABLB In@1304/Out@1316) - 0 (DUMM
###  |  Y_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 58, 63, 305, 0]
###  |  136 (DKAAR Out@388) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |
### Vessels not used in the solution: [3, 5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m7.021s
user	0m7.912s
sys	0m0.615s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
