	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_2_1_0_fr.p
Instance LSFDP: LSFDRP_3_2_1_0_fd_52.p

This object contains the information regarding the instance 3_2_1_0 for a duration of 52 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.316462 sec.
***  |   |- Calculate demand structure for node flow model,		took  0.72691 sec.
***  |   |- Create additional vessel information,				took 0.002247 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 13719 rows, 17498 columns and 118235 nonzeros
Variable types: 7565 continuous, 9933 integer (9933 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 12625 rows and 14564 columns
Presolve time: 0.72s
Presolved: 1094 rows, 2934 columns, 19657 nonzeros
Variable types: 128 continuous, 2806 integer (2779 binary)
Found heuristic solution: objective -5846252.444

Root relaxation: objective 3.261689e+06, 650 iterations, 0.02 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 3261689.44    0   93 -5846252.4 3261689.44   156%     -    0s
H    0     0                    2191826.2402 3261689.44  48.8%     -    0s
H    0     0                    2465060.1445 3261689.44  32.3%     -    0s
H    0     0                    2897401.6542 3261689.44  12.6%     -    0s
     0     0 3165119.53    0   22 2897401.65 3165119.53  9.24%     -    0s
H    0     0                    2917573.4975 3165119.53  8.48%     -    0s
     0     0     cutoff    0      2917573.50 2917573.50  0.00%     -    0s

Explored 1 nodes (760 simplex iterations) in 0.91 seconds
Thread count was 4 (of 16 available processors)

Solution count 5: 2.91757e+06 2.8974e+06 2.46506e+06 ... -5.84625e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 2.917573497466e+06, best bound 2.917573497466e+06, gap 0.0000%
Took  2.311842679977417
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 17004 rows, 14772 columns and 52518 nonzeros
Variable types: 13800 continuous, 972 integer (972 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 17004 rows and 14772 columns
Presolve time: 0.02s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.03 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: 1.05639e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.056390734970e+06, best bound 1.056390734970e+06, gap 0.0000%

### Instance
### LSFDRP_3_2_1_0_fd_52

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 42
### |A|: 39
### |A^i|: 37
### |A^f|: 2
### |M|: 348
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,056,390.735 (1056390.735)
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESA
###  |  LG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 14 has the path [136, 386, -1]
###  |  136 (DKAAR Out@388) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 385, 262, 148, 411, -2]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 148 (ESALG I
###  |  n@421/Out@433) - 411 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to 75
###  |   |- and carried Demand 182,   75.0 containers from 210 to 86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from 75 to 270
###  |   |- and carried Demand 188,  773.0 containers from 75 to 300
###  |   |- and carried Demand 189,    1.0 containers from 75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from 75 to 300
###  |   |- and carried Demand 299,    1.0 containers from 75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 57, 324, 303, 0]
###  |  131 (DEHAM Out@288) - 57 (CLLQN In@1072/Out@1090) - 324 (PECLL In@1251/Out@1328) - 303 (PABLB In@1472/Out@1484) - 0 (
###  |  DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 239,  540.0 containers from 57 to 303
###  |   |- and carried Demand 240,   69.0 containers from 57 to 303
###  |   |- and carried Demand 241,  138.0 containers from 57 to 303
###  |
###  |- Vessel  3 has the path [133, 58, 63, 305, 0]
###  |  133 (DEHAM Out@624) - 58 (CLLQN In@1240/Out@1258) - 63 (CLSAI In@1288/Out@1304) - 305 (PABLB In@1640/Out@1652) - 0 (D
###  |  UMMY_END Ziel-Service 177)
###  |   |- and carried Demand 218,  194.0 containers from 58 to 305
###  |   |- and carried Demand 219,   20.0 containers from 58 to 305
###  |   |- and carried Demand 221,    5.0 containers from 58 to 305
###  |
###  |- Vessel  4 has the path [134, 302, 0]
###  |  134 (DEHAM Out@792) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [5, 6, 7, 8, 10, 11, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	0m5.867s
user	0m5.453s
sys	0m0.295s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
