	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_2_1_0_fr.p
Instance LSFDP: LSFDRP_3_2_1_0_fd_52.p

*****************************************************************************************************************************
*** Solve LSFDRP_3_2_1_0_fd_52 with Gurobi
***
*** Graph info
*** |S|: 24
*** |S^E|: 0
*** |S^C|: 6
*** |V|: 431
*** |A|: 14925
*** |A^i|: 14778
*** |A^f|: 147
*** |M|: 348
*** |E|: 28

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 203293 rows, 5586376 columns and 20275474 nonzeros
Variable types: 5228140 continuous, 358236 integer (358236 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 31003 rows and 1191981 columns (presolve time = 5s) ...
Presolve removed 83178 rows and 3393705 columns (presolve time = 10s) ...
Presolve removed 90596 rows and 3609084 columns (presolve time = 16s) ...
Presolve removed 96238 rows and 3642624 columns (presolve time = 21s) ...
Presolve removed 97683 rows and 3642624 columns (presolve time = 25s) ...
Presolve removed 100417 rows and 3718915 columns (presolve time = 30s) ...
Presolve removed 101377 rows and 3720254 columns (presolve time = 35s) ...
Presolve removed 101377 rows and 3720935 columns (presolve time = 40s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 101410 rows and 3720941 columns (presolve time = 51s) ...
Presolve removed 101410 rows and 3926308 columns (presolve time = 662s) ...
Presolve removed 150641 rows and 5119985 columns (presolve time = 665s) ...
Presolve removed 153772 rows and 5124677 columns (presolve time = 671s) ...
Presolve removed 154141 rows and 5128045 columns (presolve time = 676s) ...
Presolve removed 155240 rows and 5139190 columns (presolve time = 685s) ...
Presolve removed 155242 rows and 5140133 columns (presolve time = 755s) ...
Presolve removed 155215 rows and 5140106 columns
Presolve time: 755.26s
Presolved: 48078 rows, 446270 columns, 2147272 nonzeros
Variable types: 286994 continuous, 159276 integer (158571 binary)

Deterministic concurrent LP optimizer: primal simplex, dual simplex, and barrier
Showing barrier log only...

Presolve removed 382 rows and 3821 columns
Presolved: 47696 rows, 442449 columns, 2142123 nonzeros

Root barrier log...

Ordering time: 0.00s

Barrier statistics:
 Dense cols : 22
 AA' NZ     : 2.575e+06
 Factor NZ  : 4.628e+07 (roughly 600 MBytes of memory)
 Factor Ops : 1.468e+11 (roughly 9 seconds per iteration)
 Threads    : 1

                  Objective                Residual
Iter       Primal          Dual         Primal    Dual     Compl     Time
   0  -2.54715677e+15  3.57874212e+15  1.82e+08 3.90e+06  2.23e+12   784s
   1  -1.53398143e+15  3.55765979e+15  1.09e+08 1.30e+07  1.35e+12   795s
   2  -1.08254505e+15  3.52443956e+15  7.70e+07 8.43e+06  9.56e+11   807s
   3  -6.63688660e+14  3.47262243e+15  4.72e+07 4.39e+06  5.90e+11   818s
   4  -3.35777667e+14  3.42088813e+15  2.39e+07 3.07e+06  3.05e+11   829s
   5  -1.17682838e+14  3.23624855e+15  8.36e+06 1.41e+06  1.10e+11   841s
   6  -3.70012423e+13  2.77360934e+15  2.63e+06 4.57e+05  3.69e+10   853s
   7  -1.50912726e+13  2.08839684e+15  1.07e+06 1.80e+05  1.57e+10   864s
   8  -4.05716566e+12  9.11507284e+14  2.87e+05 3.60e+03  4.32e+09   876s
   9  -3.66676477e+11  2.88890896e+14  2.57e+04 3.52e-03  5.95e+08   889s
  10  -2.67670915e+11  1.38667387e+14  1.87e+04 1.37e-03  3.39e+08   900s
  11  -1.33426401e+11  1.21485778e+14  9.25e+03 1.14e-03  2.25e+08   911s
  12  -6.61775291e+10  6.29620375e+13  4.52e+03 5.63e-04  1.13e+08   922s
  13  -4.08423204e+10  4.54133403e+13  2.78e+03 4.04e-04  7.80e+07   933s
  14  -2.53654098e+10  2.97140657e+13  1.73e+03 2.64e-04  5.02e+07   944s
  15  -1.30096200e+10  2.28900395e+13  8.84e+02 2.01e-04  3.44e+07   955s
  16  -7.19143990e+09  1.58210325e+13  4.86e+02 1.40e-04  2.25e+07   966s
  17  -2.97814785e+09  7.75655556e+12  1.97e+02 6.70e-05  1.06e+07   977s
  18  -1.29999628e+09  4.76785317e+12  8.25e+01 4.04e-05  6.16e+06   988s
  19  -5.71962510e+08  1.69227853e+12  3.29e+01 1.35e-05  2.24e+06  1000s
  20  -3.67092282e+08  1.05816445e+12  1.93e+01 8.23e-06  1.38e+06  1011s
  21  -2.53469107e+08  7.02232778e+11  1.19e+01 5.42e-06  9.07e+05  1022s
  22  -1.62783868e+08  5.48485652e+11  6.37e+00 4.26e-06  6.79e+05  1033s
  23  -1.32366575e+08  4.09881398e+11  4.57e+00 3.19e-06  5.05e+05  1044s
  24  -1.06554985e+08  3.55540590e+11  3.13e+00 2.79e-06  4.29e+05  1055s
  25  -9.41012509e+07  2.70520082e+11  2.50e+00 2.15e-06  3.27e+05  1066s
  26  -7.38707729e+07  2.11853857e+11  1.55e+00 1.69e-06  2.52e+05  1077s
  27  -5.92537719e+07  1.28511008e+11  9.62e-01 9.95e-07  1.53e+05  1089s
  28  -4.40245358e+07  7.99681530e+10  3.89e-01 6.35e-07  9.32e+04  1101s
  29  -3.75439158e+07  4.56839364e+10  1.96e-01 4.03e-07  5.29e+04  1113s
  30  -3.60545633e+07  3.59986751e+10  1.62e-01 3.67e-07  4.18e+04  1124s
  31  -3.24822547e+07  1.83881644e+10  1.04e-01 5.35e-07  2.15e+04  1136s
  32  -2.65627653e+07  9.45643921e+09  4.57e-02 4.92e-07  1.10e+04  1148s
  33  -2.17602347e+07  6.09671305e+09  1.81e-02 5.46e-07  7.00e+03  1159s
  34  -1.50109992e+07  2.64321612e+09  1.00e-02 5.21e-07  3.07e+03  1171s
  35  -1.30984211e+07  1.78812861e+09  8.32e-03 6.93e-07  2.09e+03  1182s
  36  -8.80806887e+06  6.36015984e+08  2.92e-03 4.89e-07  7.55e+02  1194s
  37  -4.77364431e+06  3.98009215e+08  1.51e-03 6.86e-07  4.69e+02  1206s
  38  -4.04133507e+06  3.53768979e+08  1.36e-03 6.53e-07  4.18e+02  1216s
  39  -2.44626518e+06  2.76944189e+08  1.09e-03 8.52e-07  3.27e+02  1226s
  40  -8.64660312e+05  1.81664161e+08  8.75e-04 6.87e-07  2.16e+02  1237s
  41   1.93392199e+06  1.20903074e+08  5.05e-04 8.79e-07  1.40e+02  1249s
  42   3.00239546e+06  9.39342081e+07  3.75e-04 8.39e-07  1.07e+02  1260s
  43   4.43334099e+06  6.27235633e+07  2.35e-04 6.52e-07  6.87e+01  1271s
  44   5.16064061e+06  4.82366949e+07  1.75e-04 9.57e-07  5.09e+01  1283s
  45   5.61164655e+06  4.19463498e+07  1.48e-04 8.72e-07  4.29e+01  1294s
  46   6.55579719e+06  3.48081633e+07  9.26e-05 7.67e-07  3.30e+01  1306s
  47   6.93389909e+06  3.10883984e+07  7.86e-05 1.06e-06  2.82e+01  1318s
  48   7.08798990e+06  2.69705837e+07  7.39e-05 1.00e-06  2.34e+01  1329s
  49   7.36401147e+06  2.43771228e+07  6.60e-05 1.16e-06  2.01e+01  1341s
  50   7.53121688e+06  2.19964365e+07  6.06e-05 1.33e-06  1.71e+01  1353s
  51   7.87471337e+06  2.00509866e+07  4.76e-05 9.41e-07  1.44e+01  1365s
  52   8.28465436e+06  1.83095219e+07  3.54e-05 1.11e-06  1.18e+01  1377s
  53   8.63002777e+06  1.71353136e+07  2.70e-05 1.11e-06  9.94e+00  1388s
  54   8.80952528e+06  1.62272316e+07  2.36e-05 1.72e-06  8.68e+00  1400s
  55   9.16251952e+06  1.52496422e+07  1.80e-05 1.78e-06  7.10e+00  1411s
  56   9.40899444e+06  1.44940848e+07  1.43e-05 1.67e-06  5.92e+00  1423s
  57   9.57026409e+06  1.37990861e+07  1.21e-05 1.76e-06  4.93e+00  1434s
  58   1.00797120e+07  1.18976735e+07  6.31e-06 1.32e-06  2.14e+00  1448s
  59   1.03745388e+07  1.13996120e+07  3.08e-06 1.15e-06  1.20e+00  1459s
  60   1.04254012e+07  1.11479306e+07  2.47e-06 1.02e-06  8.51e-01  1470s
  61   1.04606745e+07  1.10562284e+07  2.10e-06 1.52e-06  7.03e-01  1481s
  62   1.04907557e+07  1.10012186e+07  1.78e-06 1.66e-06  6.02e-01  1492s
  63   1.05157760e+07  1.09735322e+07  1.50e-06 2.13e-06  5.38e-01  1504s
  64   1.05383398e+07  1.09245233e+07  1.26e-06 1.84e-06  4.54e-01  1515s
  65   1.06008340e+07  1.07350550e+07  9.08e-07 1.45e-06  1.61e-01  1527s
  66   1.06369571e+07  1.06824409e+07  4.61e-07 1.26e-06  5.56e-02  1540s
  67   1.06532261e+07  1.06788622e+07  3.00e-07 1.59e-06  3.03e-02  1551s
  68   1.06564364e+07  1.06710649e+07  2.41e-07 1.52e-06  1.74e-02  1561s
  69   1.06593057e+07  1.06632505e+07  1.33e-07 1.56e-06  4.97e-03  1572s
  70   1.06613220e+07  1.06624388e+07  1.84e-07 2.18e-06  1.43e-03  1583s
  71   1.06621347e+07  1.06622265e+07  1.97e-08 2.05e-06  1.20e-04  1594s
  72   1.06622033e+07  1.06622217e+07  7.22e-09 4.61e-06  2.42e-05  1606s
  73   1.06622083e+07  1.06622216e+07  9.16e-08 4.11e-06  1.74e-05  1617s
  74   1.06622122e+07  1.06622215e+07  3.59e-07 3.75e-06  1.21e-05  1628s
  75   1.06622122e+07  1.06622215e+07  2.69e-06 4.29e-06  1.21e-05  1639s
  76   1.06622136e+07  1.06622214e+07  2.27e-06 3.62e-06  1.02e-05  1650s
  77   1.06622154e+07  1.06622214e+07  1.74e-06 3.73e-06  7.91e-06  1661s
  78   1.06622172e+07  1.06622213e+07  1.20e-06 3.52e-06  5.48e-06  1671s
  79   1.06622197e+07  1.06622213e+07  4.39e-07 2.29e-06  2.08e-06  1682s
  80   1.06622206e+07  1.06622212e+07  3.87e-07 2.39e-06  6.24e-07  1692s
  81   1.06622194e+07  1.06622212e+07  8.35e-08 2.02e-06  1.48e-07  1703s
  82   1.06622206e+07  1.06622212e+07  1.57e-07 5.32e-06  3.97e-08  1714s
  83   1.06622211e+07  1.06622212e+07  1.39e-07 3.91e-06  9.99e-09  1724s

Barrier solved model in 83 iterations and 1724.47 seconds
Optimal objective 1.06622211e+07


Root crossover log...

   44717 DPushes remaining with DInf 0.0000000e+00              1725s
     715 DPushes remaining with DInf 3.8146973e-06              1725s
       0 DPushes remaining with DInf 1.1845434e-06              1728s

     129 PPushes remaining with PInf 0.0000000e+00              1728s
       0 PPushes remaining with PInf 0.0000000e+00              1728s

  Push phase complete: Pinf 0.0000000e+00, Dinf 1.0984224e-06   1728s


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
   39367    1.0662221e+07   0.000000e+00   0.000000e+00   1728s
   40021    1.0662543e+07   0.000000e+00   4.548541e+08   1730s
   41302    1.0662783e+07   0.000000e+00   3.579675e+09   1735s
   42455    1.0662928e+07   0.000000e+00   3.445069e+09   1740s
   43737    1.0663105e+07   0.000000e+00   1.464800e+10   1745s
   45018    1.0663323e+07   0.000000e+00   6.365761e+09   1750s
   46301    1.0663546e+07   0.000000e+00   5.915665e+09   1755s
   47583    1.0663707e+07   0.000000e+00   1.178454e+10   1760s
   48993    1.0663935e+07   0.000000e+00   1.712636e+10   1765s
   50274    1.0664105e+07   0.000000e+00   1.306271e+09   1770s
   51557    1.0664291e+07   0.000000e+00   1.216602e+10   1775s
   52971    1.0664471e+07   0.000000e+00   3.500780e+09   1780s
   54512    1.0664692e+07   0.000000e+00   3.673743e+09   1785s
   56049    1.0664859e+07   0.000000e+00   1.719572e+09   1790s
   57719    1.0665108e+07   0.000000e+00   1.422639e+09   1795s
   59259    1.0665293e+07   0.000000e+00   3.298521e+09   1800s
   60925    1.0665477e+07   0.000000e+00   1.080710e+09   1805s
   62591    1.0665695e+07   0.000000e+00   2.232708e+09   1810s
   63615    1.0665785e+07   0.000000e+00   5.967729e+08   1816s
   64639    1.0665908e+07   0.000000e+00   1.509797e+09   1820s
   65792    1.0666029e+07   0.000000e+00   2.566323e+10   1825s
   66816    1.0666235e+07   0.000000e+00   3.549264e+09   1830s
   67968    1.0666398e+07   0.000000e+00   2.200327e+10   1835s
   69377    1.0666541e+07   0.000000e+00   4.346554e+09   1840s
   70785    1.0666679e+07   0.000000e+00   3.432315e+10   1845s
   72194    1.0666832e+07   0.000000e+00   1.597506e+09   1850s
   73607    1.0667009e+07   0.000000e+00   6.352206e+08   1855s
   75272    1.0667172e+07   0.000000e+00   3.394043e+11   1860s
   76938    1.0667344e+07   0.000000e+00   4.804360e+09   1865s
   78733    1.0667468e+07   0.000000e+00   5.198499e+09   1870s
   80525    1.0667608e+07   0.000000e+00   3.056457e+10   1875s
   82064    1.0667724e+07   0.000000e+00   9.924951e+08   1880s
   83731    1.0667907e+07   0.000000e+00   2.637873e+10   1885s
   84887    1.0667980e+07   0.000000e+00   1.916893e+08   1890s
   85913    1.0668035e+07   0.000000e+00   8.698904e+08   1895s
   87065    1.0668133e+07   0.000000e+00   8.570616e+09   1900s
   88607    1.0668210e+07   0.000000e+00   3.181151e+09   1905s
   90282    1.0668302e+07   0.000000e+00   1.489362e+09   1910s
   91437    1.0668354e+07   0.000000e+00   1.799858e+10   1915s
   92721    1.0668407e+07   0.000000e+00   2.007332e+09   1920s
   93876    1.0668492e+07   0.000000e+00   4.446377e+09   1925s
   95164    1.0668553e+07   0.000000e+00   1.620326e+10   1931s
   96581    1.0668653e+07   0.000000e+00   5.804699e+08   1935s
   98125    1.0668787e+07   0.000000e+00   4.167509e+09   1940s
   99790    1.0668898e+07   0.000000e+00   5.872553e+08   1945s
  101472    1.0669004e+07   0.000000e+00   1.281983e+09   1950s
  103146    1.0669081e+07   0.000000e+00   2.461855e+10   1955s
  104819    1.0669172e+07   0.000000e+00   6.094848e+09   1960s
  106365    1.0669273e+07   0.000000e+00   7.473984e+08   1965s
  107912    1.0669379e+07   0.000000e+00   4.351718e+08   1970s
  109584    1.0669487e+07   0.000000e+00   3.133379e+10   1975s
  111126    1.0669574e+07   0.000000e+00   6.943270e+08   1980s
  112799    1.0669640e+07   0.000000e+00   4.137238e+09   1985s
  114471    1.0669712e+07   0.000000e+00   8.419754e+08   1990s
  116227    1.0669786e+07   0.000000e+00   3.444082e+08   1995s
  118003    1.0669857e+07   0.000000e+00   4.626110e+10   2000s
  119805    1.0669922e+07   0.000000e+00   9.163889e+08   2005s
  121215    1.0669995e+07   0.000000e+00   5.515727e+09   2010s
  122248    1.0670034e+07   0.000000e+00   2.318405e+08   2015s
  123406    1.0670083e+07   0.000000e+00   1.034319e+10   2020s
  124946    1.0670125e+07   0.000000e+00   3.219914e+08   2025s
  126878    1.0670208e+07   0.000000e+00   2.148010e+07   2030s
  128558    1.0670261e+07   0.000000e+00   1.927863e+09   2035s
  130377    1.0670305e+07   0.000000e+00   2.657554e+08   2040s
  132181    1.0670345e+07   0.000000e+00   3.525982e+08   2045s
  133852    1.0670386e+07   0.000000e+00   2.497713e+08   2050s
  135143    1.0670421e+07   0.000000e+00   8.117436e+07   2055s
  136439    1.0670444e+07   0.000000e+00   5.599378e+09   2060s
  137598    1.0670466e+07   0.000000e+00   1.998249e+09   2065s
  138626    1.0670482e+07   0.000000e+00   1.540145e+08   2070s
  139657    1.0670502e+07   0.000000e+00   1.203837e+08   2075s
  140830    1.0670527e+07   0.000000e+00   9.257815e+07   2080s
  142119    1.0670543e+07   0.000000e+00   5.843929e+07   2085s
  143412    1.0670622e+07   0.000000e+00   4.930325e+11   2091s
  144697    1.0670674e+07   0.000000e+00   3.770143e+08   2095s
  146114    1.0670693e+07   0.000000e+00   1.014492e+09   2100s
  147532    1.0670762e+07   0.000000e+00   8.871225e+08   2105s
  148820    1.0670783e+07   0.000000e+00   8.680545e+07   2110s
  150235    1.0670812e+07   0.000000e+00   4.592162e+09   2116s
  151393    1.0670881e+07   0.000000e+00   1.414480e+08   2120s
  152806    1.0670916e+07   0.000000e+00   2.312345e+07   2125s
  154226    1.0670932e+07   0.000000e+00   1.573086e+08   2130s
  155635    1.0670943e+07   0.000000e+00   1.782392e+08   2135s
  157047    1.0670996e+07   0.000000e+00   3.463148e+07   2140s
  158337    1.0671011e+07   0.000000e+00   7.166730e+07   2145s
  159882    1.0671031e+07   0.000000e+00   2.588463e+07   2150s
  161422    1.0671044e+07   0.000000e+00   2.843207e+07   2155s
  162838    1.0671058e+07   0.000000e+00   5.255622e+10   2160s
  164122    1.0671074e+07   0.000000e+00   1.253453e+07   2165s
  165540    1.0671085e+07   0.000000e+00   2.579755e+07   2170s
  166958    1.0671096e+07   0.000000e+00   4.691436e+06   2175s
  168374    1.0671106e+07   0.000000e+00   1.094995e+09   2180s
  169789    1.0671122e+07   0.000000e+00   1.626566e+07   2185s
  171204    1.0671130e+07   0.000000e+00   6.304128e+06   2190s
  172749    1.0671138e+07   0.000000e+00   4.080413e+06   2195s
  174296    1.0671145e+07   0.000000e+00   2.235766e+06   2200s
  175837    1.0671148e+07   0.000000e+00   1.568131e+07   2205s
  177258    1.0671151e+07   0.000000e+00   5.767524e+05   2210s
  178552    1.0671157e+07   0.000000e+00   9.893334e+05   2215s
  179846    1.0671161e+07   0.000000e+00   9.713835e+06   2220s
  181137    1.0671163e+07   0.000000e+00   3.537292e+05   2226s
  182428    1.0671164e+07   0.000000e+00   2.660314e+05   2230s
  183853    1.0671165e+07   0.000000e+00   1.735134e+05   2235s
  185273    1.0671165e+07   0.000000e+00   4.239751e+04   2240s
  186294    1.0662221e+07   3.654577e-02   0.000000e+00   2245s
  186447    1.0662221e+07   0.000000e+00   0.000000e+00   2247s
Extra 147080 simplex iterations after uncrush
Concurrent spin time: 0.01s

Solved with barrier

Root relaxation: objective 1.066222e+07, 186447 iterations, 1487.79 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.0617e+07    0  368          - 1.0617e+07      -     - 2476s
     0     0 8909363.85    0  475          - 8909363.85      -     - 4542s
H    0     0                    -4835985.572 8909363.85   284%     - 4544s
H    0     0                    -3432550.309 8909363.85   360%     - 4570s
     0     0 8909228.39    0  475 -3432550.3 8909228.39   360%     - 4621s
     0     0 8753709.67    0  479 -3432550.3 8753709.67   355%     - 5639s
     0     0 8738588.31    0  474 -3432550.3 8738588.31   355%     - 6696s
     0     0 8737830.48    0  482 -3432550.3 8737830.48   355%     - 6780s
     0     0 8735702.88    0  463 -3432550.3 8735702.88   354%     - 7052s
     0     0 8735702.88    0  463 -3432550.3 8735702.88   354%     - 7054s
     0     0 8568616.67    0  456 -3432550.3 8568616.67   350%     - 8109s
     0     0 8567539.41    0  445 -3432550.3 8567539.41   350%     - 8347s
     0     0 8567474.04    0  444 -3432550.3 8567474.04   350%     - 8428s
     0     0 8563319.09    0  472 -3432550.3 8563319.09   349%     - 8994s
     0     0 8560146.65    0  472 -3432550.3 8560146.65   349%     - 9203s
     0     0 8560146.65    0  472 -3432550.3 8560146.65   349%     - 9204s
     0     0 8559093.28    0  496 -3432550.3 8559093.28   349%     - 9374s
     0     0 8558905.43    0  500 -3432550.3 8558905.43   349%     - 9408s
     0     0 8558162.01    0  505 -3432550.3 8558162.01   349%     - 9534s
     0     0 8558158.64    0  507 -3432550.3 8558158.64   349%     - 9553s
     0     0 8551933.29    0  477 -3432550.3 8551933.29   349%     - 9829s
     0     0 8549398.86    0  497 -3432550.3 8549398.86   349%     - 9961s
     0     0 8549031.77    0  502 -3432550.3 8549031.77   349%     - 10129s
     0     0 8548864.78    0  480 -3432550.3 8548864.78   349%     - 10217s
     0     0 8538338.98    0  480 -3432550.3 8538338.98   349%     - 10741s
     0     0 8537850.49    0  464 -3432550.3 8537850.49   349%     - 10805s
     0     0 8537470.15    0  475 -3432550.3 8537470.15   349%     - 11008s
     0     0 8536967.01    0  498 -3432550.3 8536967.01   349%     - 11122s
     0     0 8536967.01    0  498 -3432550.3 8536967.01   349%     - 11124s
     0     0 8533756.92    0  463 -3432550.3 8533756.92   349%     - 11300s
     0     0 8533756.92    0  468 -3432550.3 8533756.92   349%     - 11305s
     0     0 8530939.05    0  490 -3432550.3 8530939.05   349%     - 11491s
     0     0 8530888.48    0  497 -3432550.3 8530888.48   349%     - 11548s
     0     0 8530088.60    0  480 -3432550.3 8530088.60   349%     - 11627s
     0     0 8530040.72    0  474 -3432550.3 8530040.72   349%     - 11649s
     0     0 8529698.02    0  480 -3432550.3 8529698.02   348%     - 11769s
H    0     0                    -2893376.946 8529698.02   395%     - 14338s
     0     0 8529504.50    0  477 -2893376.9 8529504.50   395%     - 14367s
     0     0 8528437.05    0  456 -2893376.9 8528437.05   395%     - 14497s
     0     0 8528437.05    0  452 -2893376.9 8528437.05   395%     - 14841s
     0     2 8528437.05    0  446 -2893376.9 8528437.05   395%     - 14866s
     1     4 8297472.04    1  435 -2893376.9 8528436.49   395% 100505 16667s
     3     8 8283691.74    2  441 -2893376.9 8488221.13   393% 77602 22109s
     7    12 7960477.49    3  419 -2893376.9 8283688.44   386% 131495 23885s
    11    16 8116588.09    3  483 -2893376.9 8116588.09   381% 109111 26456s
    15    19 7751856.37    4  313 -2893376.9 8116538.92   381% 107464 28199s
    19    17 7747902.55    5  332 -2893376.9 8116538.92   381% 87999 28940s
    23    24 7741358.60    6  320 -2893376.9 8116538.92   381% 76417 29728s
    28    28 7733455.76    7  337 -2893376.9 8116538.92   381% 67376 30985s
    35    37 7729642.25    8  334 -2893376.9 8116538.92   381% 58127 34125s
    43    41 7612916.64   10  296 -2893376.9 8116538.92   381% 54582 35660s
    48    48 7583389.56   11  314 -2893376.9 8116538.92   381% 53210 38136s
    53    51 7290007.43   11  209 -2893376.9 8116538.92   381% 56353 40898s
    58    54 7583303.05   12  318 -2893376.9 8116538.92   381% 56090 47329s
H   66    61                    -2628143.805 8116538.92   409% 58820 47329s
    67    64 7462543.20   15  312 -2628143.8 8116538.92   409% 58500 48734s
    77    76 6576022.20   17  177 -2628143.8 8116538.92   409% 56714 54043s
    85    80 7267401.92   18  307 -2628143.8 8116538.92   409% 53668 55925s
    97    95 7240803.57   20  311 -2628143.8 8116538.92   409% 49627 57834s
   108   103 7204730.89   23  283 -2628143.8 8116538.92   409% 47716 61032s
   118   115 7171207.69   27  273 -2628143.8 8116538.92   409% 47384 63255s
   127   119 7134732.40   29  252 -2628143.8 8116538.92   409% 47090 64673s
   140   136 7087253.53   33  284 -2628143.8 8116538.92   409% 45206 66385s
   152   147 7083162.46   34  287 -2628143.8 8116538.92   409% 42934 68273s
   171   165 6967250.61   40  306 -2628143.8 8116538.92   409% 40425 70356s
   186   178 6904408.19   44  301 -2628143.8 8116538.92   409% 39176 76973s
   195   185 5848766.47   46  229 -2628143.8 8116538.92   409% 41871 82642s
   205   193 6782540.36   47  280 -2628143.8 8116538.92   409% 42599 85311s
   215   200 6660134.91   51  285 -2628143.8 8116538.92   409% 43247 86400s

Cutting planes:
  Gomory: 4
  Implied bound: 28
  Clique: 14
  MIR: 50
  Flow cover: 103
  Zero half: 6
  Mod-K: 1

Explored 221 nodes (10231881 simplex iterations) in 86400.49 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: -2.62814e+06 -2.89338e+06 -3.43255e+06 -4.83599e+06 

Time limit reached
Warning: max constraint violation (8.2213e-06) exceeds tolerance
Warning: max bound violation (7.8873e-06) exceeds tolerance
Best objective -2.628143805076e+06, best bound 8.116538916366e+06, gap 408.8316%

***
***  |- Calculation finished, timelimit reached 86,400.59 (86400.59) sec.))
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -2,628,143.8051 (-2628143.8051) and is feasible with a Gap of 4.0883
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288.0) - 386 (ESALG In@496.0/Out@515.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456.0) - 387 (ESALG In@664.0/Out@683.0) - -1 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [135, 401, 263, 388, -1]
###  |  135 (DKAAR Out@220.0) - 401 (MAPTM In@379.0/Out@398.0) - 263 (MAPTM In@406.0/Out@427.0) - 388 (ESALG In@832.0/Out@851
###  |  .0) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |- Vessel  9 has the path [9, 400, 262, 148, 411, -2]
###  |  9 (BEANR Out@66.0) - 400 (MAPTM In@211.0/Out@230.0) - 262 (MAPTM In@375.0/Out@396.0) - 148 (ESALG In@421.0/Out@433.0)
###  |   - 411 (HNPCR In@998.0/Out@1011.0) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 140, 300, 412, -2]
###  |  231 (JPYOK Out@171.0) - 106 (CNXMN In@238.0/Out@260.0) - 78 (CNNSA In@287.0/Out@297.0) - 210 (HKHKG In@334.0/Out@350.
###  |  0) - 107 (CNYTN In@358.0/Out@376.0) - 75 (CNNGB In@430.0/Out@439.0) - 86 (CNSHA In@465.0/Out@483.0) - 140 (ECGYE In@9
###  |  11.0/Out@923.0) - 300 (PABLB In@982.0/Out@1030.0) - 412 (HNPCR In@1166.0/Out@1179.0) - -2 (DUMMY_END Ziel-Service 121
###  |  )
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  5 has the path [282, 178, 260, 146, 407, 270, 301, 305, 0]
###  |  282 (NLRTM Out@92.0) - 178 (FRLEH In@111.0/Out@127.0) - 260 (MAPTM In@207.0/Out@228.0) - 146 (ESALG In@253.0/Out@265.
###  |  0) - 407 (GTSTC In@687.0/Out@701.0) - 270 (MXLZC In@873.0/Out@896.0) - 301 (PABLB In@1136.0/Out@1148.0) - 305 (PABLB 
###  |  In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand   8,   42.0 containers from 282 to 146
###  |   |- and carried Demand   9,   17.0 containers from 282 to 178
###  |   |- and carried Demand  11,  580.0 containers from 260 to 146
###  |   |- and carried Demand 256,   42.0 containers from 282 to 146
###  |   |- and carried Demand 257,  580.0 containers from 260 to 146
###  |- Vessel  7 has the path [285, 182, 264, 302, 0]
###  |  285 (NLRTM Out@428.0) - 182 (FRLEH In@447.0/Out@463.0) - 264 (MAPTM In@543.0/Out@564.0) - 302 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |- Vessel 15 has the path [179, 261, 385, 151, 371, 303, 0]
###  |  179 (FRLEH Out@158.0) - 261 (MAPTM In@238.0/Out@259.0) - 385 (ESALG In@328.0/Out@347.0) - 151 (ESALG In@625.0/Out@670
###  |  .0) - 371 (TRZMK In@778.0/Out@793.0) - 303 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 140,  552.0 containers from 151 to 303
###  |   |- and carried Demand 141,   13.0 containers from 151 to 303
###  |   |- and carried Demand 271,  580.0 containers from 261 to 385
###  |
### Vessels not used in the solution: [3, 4, 6, 8, 10, 11, 12, 14, 16, 17, 18, 19, 20, 21, 22, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,19] = 1
rho[121,30] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
eta[121,30,1] = 1
eta[121,30,2] = 1
y[1,131,386] = 1
y[1,386,-1] = 1
y[2,387,-1] = 1
y[2,132,387] = 1
y[5,270,301] = 1
y[5,146,407] = 1
y[5,260,146] = 1
y[5,282,178] = 1
y[5,305,0] = 1
y[5,178,260] = 1
y[5,301,305] = 1
y[5,407,270] = 1
y[7,302,0] = 1
y[7,285,182] = 1
y[7,264,302] = 1
y[7,182,264] = 1
y[9,148,411] = 1
y[9,9,400] = 1
y[9,411,-2] = 1
y[9,262,148] = 1
y[9,400,262] = 1
y[13,263,388] = 1
y[13,401,263] = 1
y[13,135,401] = 1
y[13,388,-1] = 1
y[15,385,151] = 1
y[15,303,0] = 1
y[15,151,371] = 1
y[15,261,385] = 1
y[15,371,303] = 1
y[15,179,261] = 1
y[24,300,412] = 1
y[24,231,106] = 1
y[24,210,107] = 1
y[24,412,-2] = 1
y[24,106,78] = 1
y[24,75,86] = 1
y[24,140,300] = 1
y[24,78,210] = 1
y[24,86,140] = 1
y[24,107,75] = 1
xD[8,260,146] = 42
xD[8,282,178] = 42
xD[8,178,260] = 42
xD[9,282,178] = 17
xD[11,260,146] = 580
xD[20,262,148] = 484
xD[21,262,148] = 18
xD[32,285,182] = 6
xD[87,231,106] = 541
xD[87,210,107] = 541
xD[87,106,78] = 541
xD[87,75,86] = 541
xD[87,140,300] = 541
xD[87,78,210] = 541
xD[87,86,140] = 541
xD[87,107,75] = 541
xD[140,151,371] = 552
xD[140,371,303] = 552
xD[141,151,371] = 13
xD[141,371,303] = 13
xD[178,106,78] = 785
xD[178,78,210] = 785
xD[179,106,78] = 1
xD[179,78,210] = 1
xD[181,210,107] = 121
xD[181,107,75] = 121
xD[182,210,107] = 75
xD[182,75,86] = 75
xD[182,107,75] = 75
xD[183,210,107] = 1
xD[186,75,86] = 6
xD[186,140,300] = 6
xD[186,86,140] = 6
xD[186,107,75] = 6
xD[188,75,86] = 773
xD[188,140,300] = 773
xD[188,86,140] = 773
xD[189,75,86] = 1
xD[189,140,300] = 1
xD[189,86,140] = 1
xD[256,260,146] = 42
xD[256,282,178] = 42
xD[256,178,260] = 42
xD[257,260,146] = 580
xD[261,262,148] = 484
xD[262,262,148] = 18
xD[271,261,385] = 580
xD[272,263,388] = 214
xD[273,263,388] = 10
xD[285,263,388] = 214
xD[285,401,263] = 214
xD[286,263,388] = 10
xD[286,401,263] = 10
xD[297,75,86] = 6
xD[297,140,300] = 6
xD[297,86,140] = 6
xD[297,107,75] = 6
xD[298,75,86] = 773
xD[298,140,300] = 773
xD[298,86,140] = 773
xD[299,75,86] = 1
xD[299,140,300] = 0.999999
xD[299,86,140] = 1
zE[75] = 430
zE[78] = 287
zE[86] = 465
zE[106] = 238
zE[107] = 358
zE[140] = 911
zE[146] = 253
zE[148] = 421
zE[151] = 625
zE[178] = 111
zE[182] = 447
zE[210] = 334
zE[260] = 207
zE[261] = 238
zE[262] = 375
zE[263] = 406
zE[264] = 543
zE[270] = 873
zE[300] = 982
zE[301] = 1136
zE[302] = 1304
zE[303] = 1472
zE[305] = 1640
zE[371] = 778
zE[385] = 328
zE[386] = 496
zE[387] = 664
zE[388] = 832
zE[400] = 211
zE[401] = 379
zE[407] = 687
zE[411] = 998
zE[412] = 1166
zX[9] = 66
zX[75] = 439
zX[78] = 297
zX[86] = 483
zX[106] = 260
zX[107] = 376
zX[131] = 288
zX[132] = 456
zX[135] = 220
zX[140] = 923
zX[146] = 265
zX[148] = 433
zX[151] = 670
zX[178] = 127
zX[179] = 158
zX[182] = 463
zX[210] = 350
zX[231] = 171
zX[260] = 228
zX[261] = 259
zX[262] = 396
zX[263] = 427
zX[264] = 564
zX[270] = 896
zX[282] = 92
zX[285] = 428
zX[300] = 1030
zX[301] = 1148
zX[302] = 1316
zX[303] = 1484
zX[305] = 1652
zX[371] = 793
zX[385] = 347
zX[386] = 515
zX[387] = 683
zX[388] = 851
zX[400] = 230
zX[401] = 398
zX[407] = 701
zX[411] = 1011
zX[412] = 1179
phi[1] = 227
phi[2] = 227
phi[5] = 1560
phi[7] = 888
phi[9] = 945
phi[13] = 631
phi[15] = 1326
phi[24] = 1008

### All variables with value smaller zero:

### End

real	1446m48.764s
user	5044m29.095s
sys	493m28.624s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
