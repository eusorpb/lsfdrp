	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_1_fr.p
Instance LSFDP: LSFDRP_3_4_1_1_fd_28.p

This object contains the information regarding the instance 3_4_1_1 for a duration of 28 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took  0.43414 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.736347 sec.
***  |   |- Create additional vessel information,				took 0.010442 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 51819 rows, 160270 columns and 1291987 nonzeros
Variable types: 36677 continuous, 123593 integer (123593 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 42382 rows and 93801 columns
Presolve time: 4.95s
Presolved: 9437 rows, 66469 columns, 317773 nonzeros
Variable types: 768 continuous, 65701 integer (65011 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.1963526e+08   4.342291e+04   0.000000e+00      5s
   14008    1.3916391e+07   0.000000e+00   0.000000e+00     10s

Root relaxation: objective 1.391639e+07, 14008 iterations, 4.44 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.3037e+07    0 1021          - 1.3037e+07      -     -   11s
H    0     0                    -3427916.027 1.3037e+07   480%     -   11s
H    0     0                    3085454.5451 1.3037e+07   323%     -   25s
H    0     0                    3278482.1935 1.3037e+07   298%     -   26s
     0     0 1.1849e+07    0  937 3278482.19 1.1849e+07   261%     -   28s
     0     0 1.1842e+07    0  926 3278482.19 1.1842e+07   261%     -   29s
     0     0 1.1839e+07    0  938 3278482.19 1.1839e+07   261%     -   29s
     0     0 1.1839e+07    0  938 3278482.19 1.1839e+07   261%     -   29s
     0     0 1.1278e+07    0  943 3278482.19 1.1278e+07   244%     -   30s
     0     0 1.1247e+07    0  836 3278482.19 1.1247e+07   243%     -   31s
     0     0 1.1247e+07    0  837 3278482.19 1.1247e+07   243%     -   31s
     0     0 1.1214e+07    0  847 3278482.19 1.1214e+07   242%     -   31s
     0     0 1.1208e+07    0  794 3278482.19 1.1208e+07   242%     -   32s
     0     0 1.1208e+07    0  793 3278482.19 1.1208e+07   242%     -   32s
     0     0 1.1203e+07    0  867 3278482.19 1.1203e+07   242%     -   32s
     0     0 1.1203e+07    0  868 3278482.19 1.1203e+07   242%     -   33s
     0     0 1.1190e+07    0  912 3278482.19 1.1190e+07   241%     -   33s
     0     0 1.1189e+07    0  903 3278482.19 1.1189e+07   241%     -   34s
     0     0 1.1188e+07    0  928 3278482.19 1.1188e+07   241%     -   34s
     0     0 1.1183e+07    0  875 3278482.19 1.1183e+07   241%     -   35s
     0     0 1.1181e+07    0  889 3278482.19 1.1181e+07   241%     -   35s
     0     0 1.1181e+07    0  890 3278482.19 1.1181e+07   241%     -   36s
     0     0 1.1177e+07    0  886 3278482.19 1.1177e+07   241%     -   36s
     0     0 1.1177e+07    0  882 3278482.19 1.1177e+07   241%     -   37s
     0     0 1.1177e+07    0  885 3278482.19 1.1177e+07   241%     -   37s
     0     0 1.1171e+07    0  901 3278482.19 1.1171e+07   241%     -   38s
     0     0 1.1171e+07    0  898 3278482.19 1.1171e+07   241%     -   38s
     0     0 1.1171e+07    0  901 3278482.19 1.1171e+07   241%     -   38s
     0     0 1.1171e+07    0  902 3278482.19 1.1171e+07   241%     -   39s
     0     0 1.1166e+07    0  883 3278482.19 1.1166e+07   241%     -   39s
     0     0 1.1165e+07    0  952 3278482.19 1.1165e+07   241%     -   40s
     0     0 1.1165e+07    0  954 3278482.19 1.1165e+07   241%     -   40s
     0     0 1.1164e+07    0  960 3278482.19 1.1164e+07   241%     -   40s
     0     0 1.1160e+07    0  955 3278482.19 1.1160e+07   240%     -   41s
     0     0 1.1160e+07    0 1007 3278482.19 1.1160e+07   240%     -   41s
     0     0 1.1159e+07    0  988 3278482.19 1.1159e+07   240%     -   41s
     0     0 1.1158e+07    0  939 3278482.19 1.1158e+07   240%     -   41s
     0     0 1.0867e+07    0  793 3278482.19 1.0867e+07   231%     -   43s
     0     0 1.0866e+07    0  795 3278482.19 1.0866e+07   231%     -   43s
     0     0 1.0814e+07    0  813 3278482.19 1.0814e+07   230%     -   44s
     0     0 1.0810e+07    0  821 3278482.19 1.0810e+07   230%     -   44s
     0     0 1.0782e+07    0  790 3278482.19 1.0782e+07   229%     -   45s
     0     0 1.0781e+07    0  797 3278482.19 1.0781e+07   229%     -   45s
     0     0 1.0781e+07    0  768 3278482.19 1.0781e+07   229%     -   46s
     0     0 1.0773e+07    0  800 3278482.19 1.0773e+07   229%     -   46s
     0     0 1.0773e+07    0  799 3278482.19 1.0773e+07   229%     -   46s
     0     0 1.0771e+07    0  791 3278482.19 1.0771e+07   229%     -   47s
     0     0 1.0771e+07    0  802 3278482.19 1.0771e+07   229%     -   47s
     0     0 1.0768e+07    0  816 3278482.19 1.0768e+07   228%     -   48s
     0     0 1.0768e+07    0  804 3278482.19 1.0768e+07   228%     -   55s
H    0     0                    3864826.4807 1.0768e+07   179%     -   62s
     0     0 1.0768e+07    0  765 3864826.48 1.0768e+07   179%     -   66s
     0     0 1.0343e+07    0  667 3864826.48 1.0343e+07   168%     -   71s
     0     0 1.0304e+07    0  677 3864826.48 1.0304e+07   167%     -   71s
     0     0 1.0303e+07    0  654 3864826.48 1.0303e+07   167%     -   71s
     0     0 1.0242e+07    0  695 3864826.48 1.0242e+07   165%     -   71s
     0     0 9963933.54    0  725 3864826.48 9963933.54   158%     -   73s
     0     0 9909461.39    0  735 3864826.48 9909461.39   156%     -   73s
     0     0 9886023.35    0  638 3864826.48 9886023.35   156%     -   74s
     0     0 9854083.11    0  644 3864826.48 9854083.11   155%     -   74s
     0     0 9801737.59    0  667 3864826.48 9801737.59   154%     -   74s
     0     0 9750490.58    0  699 3864826.48 9750490.58   152%     -   75s
     0     0 9745970.74    0  709 3864826.48 9745970.74   152%     -   75s
     0     0 9745287.41    0  719 3864826.48 9745287.41   152%     -   75s
     0     0 9728906.59    0  689 3864826.48 9728906.59   152%     -   76s
     0     0 9728463.35    0  675 3864826.48 9728463.35   152%     -   76s
     0     0 9724420.24    0  653 3864826.48 9724420.24   152%     -   76s
     0     0 9724420.24    0  653 3864826.48 9724420.24   152%     -   77s
     0     0 9716680.84    0  657 3864826.48 9716680.84   151%     -   77s
     0     0 9704390.34    0  658 3864826.48 9704390.34   151%     -   77s
     0     0 9701827.85    0  654 3864826.48 9701827.85   151%     -   77s
     0     0 9701390.35    0  674 3864826.48 9701390.35   151%     -   78s
     0     0 9694961.01    0  701 3864826.48 9694961.01   151%     -   78s
     0     0 9693270.88    0  727 3864826.48 9693270.88   151%     -   78s
     0     0 9693270.88    0  720 3864826.48 9693270.88   151%     -   84s
     0     2 9693270.88    0  720 3864826.48 9693270.88   151%     -   96s
     3     8 9367870.73    2  732 3864826.48 9394424.57   143%   840  101s
    28    29 8592631.71    8  631 3864826.48 9245709.01   139%   452  105s
    72    73 6970953.00   14  521 3864826.48 9245709.01   139%   366  110s
   157   153 6545951.80   23  631 3864826.48 9245709.01   139%   280  115s
   274   244 5418561.14   37  420 3864826.48 9189190.51   138%   231  120s
   392   338 4415979.90   72  382 3864826.48 9189190.51   138%   206  133s
   438   377 3896060.84   99  295 3864826.48 8836079.43   129%   201  135s
   576   464 5334331.10   20  550 3864826.48 8836079.43   129%   193  140s
   667   528 6834270.04    9  529 3864826.48 8474535.62   119%   201  145s
H  687   372                    4672262.5903 8474535.62  81.4%   203  155s
   828   457 4706878.45   20  330 4672262.59 8474535.62  81.4%   201  161s
   967   539 5376665.02   16  479 4672262.59 7728182.48  65.4%   199  166s
  1032   570 5348251.38   31  804 4672262.59 7671066.59  64.2%   197  184s
H 1033   542                    4713291.0247 7671066.59  62.8%   197  195s
  1039   546 5903120.37   26  630 4713291.02 7671066.59  62.8%   196  200s
  1043   548 5537480.31   16  658 4713291.02 7671066.59  62.8%   195  205s
  1046   550 6859069.52   11  686 4713291.02 7671066.59  62.8%   195  211s
  1049   552 7337105.65   10  709 4713291.02 7597184.12  61.2%   194  216s
  1052   554 5553027.91   17  713 4713291.02 7569817.03  60.6%   193  221s
  1055   556 4945012.70   30  773 4713291.02 7552322.83  60.2%   193  225s
  1058   558 6760701.00    7  771 4713291.02 7513646.69  59.4%   192  246s
H 1059   531                    5007454.3077 7513646.69  50.0%   192  277s
  1061   535 7354061.23   10  678 5007454.31 7354061.23  46.9%   208  334s
  1063   538 7018159.87   11  682 5007454.31 7195506.16  43.7%   210  344s
  1067   540 6163962.66   12  488 5007454.31 7016549.10  40.1%   214  346s
  1075   544 6153023.86   13  518 5007454.31 6635470.93  32.5%   218  351s
  1111   548 5470743.96   16  574 5007454.31 6635470.93  32.5%   221  355s
  1144   545 5338591.87   20  461 5007454.31 6632438.72  32.5%   225  360s
  1234   548 6047365.12   14  542 5007454.31 6353713.71  26.9%   220  365s
  1313   531 5066400.25   20  517 5007454.31 6310498.96  26.0%   221  370s
  1415   533 5260023.45   29  467 5007454.31 5978809.05  19.4%   220  375s
* 1536   505              31    5033561.0238 5758234.63  14.4%   216  378s
  1607   504 5508203.18   30  564 5033561.02 5750935.90  14.3%   213  380s
  1763   474 5243186.53   28  409 5033561.02 5652256.36  12.3%   210  385s
  1955   422 5275238.35   20  442 5033561.02 5498242.48  9.23%   207  405s
H 1988   387                    5033561.0366 5496020.22  9.19%   206  405s
  2020   392 5231730.56   27  502 5033561.04 5479408.63  8.86%   205  414s
  2025   365 5231234.21   28  480 5033561.04 5468324.54  8.64%   205  415s
  2301   239     cutoff   27      5033561.04 5375999.37  6.80%   197  421s
  2538    88     cutoff   31      5033561.04 5291051.11  5.12%   192  425s

Cutting planes:
  Gomory: 10
  Implied bound: 9
  Projected implied bound: 16
  Clique: 13
  MIR: 25
  Flow cover: 16
  Zero half: 31
  Mod-K: 1

Explored 2757 nodes (538037 simplex iterations) in 427.50 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: 5.03356e+06 5.03356e+06 5.00745e+06 ... -3.42792e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 5.033561023846e+06, best bound 5.033561023846e+06, gap 0.0000%
Took  435.1312885284424
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 67246 rows, 62814 columns and 212062 nonzeros
Variable types: 60240 continuous, 2574 integer (2574 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 2e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 67246 rows and 62814 columns
Presolve time: 0.11s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.15 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -1.93834e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -1.938339666515e+06, best bound -1.938339666515e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_1_fd_28

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 109
### |A|: 104
### |A^i|: 104
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,938,339.6665 (-1938339.6665)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 394, 68, 480, 441, -3]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 68 (CMDLA In@796/Out@842) - 480 (TRMER In
###  |  @1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  10,  161.0 containers from 260 to  68
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  68
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  68
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 10 has the path [10, 476, 435, -3]
###  |  10 (BEANR Out@234) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [13, 477, 437, -3]
###  |  13 (BEANR Out@402) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [16, 478, 448, 449, 369, 439, -3]
###  |  16 (BEANR Out@570) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUS
###  |  K In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 17 has the path [190, 361, 438, -3]
###  |  190 (GBFXT Out@302) - 361 (TRALI In@836/Out@854) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [193, 479, 440, -3]
###  |  193 (GBFXT Out@470) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 387, -1]
###  |  131 (DEHAM Out@288) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 386, -1]
###  |  135 (DKAAR Out@220) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPT
###  |  M In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  312.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 483, 543, 215, 487, 547, 524, -4]
###  |  282 (NLRTM Out@92) - 483 (AEJEA In@563/Out@587) - 543 (PKBQM In@644/Out@674) - 215 (INNSA In@1144/Out@1176) - 487 (AE
###  |  JEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 567,  146.0 containers from 543 to 215
###  |   |- and carried Demand 570,  704.0 containers from 483 to 524
###  |   |- and carried Demand 571,    8.0 containers from 483 to 524
###  |   |- and carried Demand 572,  156.0 containers from 483 to 543
###  |   |- and carried Demand 573,   45.0 containers from 483 to 543
###  |
###  |- Vessel  6 has the path [283, 370, 484, 544, 521, -4]
###  |  283 (NLRTM Out@260) - 370 (TRZMK In@617/Out@631) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 521 (INN
###  |  SA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 554,   44.0 containers from 544 to 521
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 485, 545, 349, 488, 548, 525, -4]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 349 (PKBQM In@1218/Out@1246) - 488 (A
###  |  EJEA In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |
###  |- Vessel  8 has the path [287, 368, 522, -4]
###  |  287 (NLRTM Out@596) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 15 has the path [179, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  179 (FRLEH Out@158) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAI
###  |  LK In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 
###  |  (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 16 has the path [181, 362, 520, -4]
###  |  181 (FRLEH Out@326) - 362 (TRAMB In@576/Out@603) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 23 has the path [375, 411, -2]
###  |  375 (UYMVD Out@216) - 411 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 372, 303, 0]
###  |  132 (DEHAM Out@456) - 372 (TRZMK In@785/Out@799) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 360, 302, 0]
###  |  136 (DKAAR Out@388) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
###  |
### Vessels not used in the solution: [3, 4, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	7m25.730s
user	24m49.415s
sys	2m16.977s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
