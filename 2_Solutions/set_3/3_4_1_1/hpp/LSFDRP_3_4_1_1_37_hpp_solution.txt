	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_1_fr.p
Instance LSFDP: LSFDRP_3_4_1_1_fd_37.p

This object contains the information regarding the instance 3_4_1_1 for a duration of 37 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.492689 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.594085 sec.
***  |   |- Create additional vessel information,				took 0.009659 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 51289 rows, 159410 columns and 1282706 nonzeros
Variable types: 36504 continuous, 122906 integer (122906 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 41292 rows and 91468 columns (presolve time = 12s) ...
Presolve removed 42166 rows and 91585 columns
Presolve time: 12.89s
Presolved: 9123 rows, 67825 columns, 321849 nonzeros
Variable types: 719 continuous, 67106 integer (66539 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.9662096e+08   4.008797e+04   0.000000e+00     13s
    9560    1.8887306e+07   1.238302e+04   0.000000e+00     15s
   13642    1.3021351e+07   0.000000e+00   0.000000e+00     17s

Root relaxation: objective 1.302135e+07, 13642 iterations, 3.97 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.2591e+07    0  938          - 1.2591e+07      -     -   18s
H    0     0                    -1655597.479 1.2591e+07   861%     -   18s
H    0     0                    -1501623.850 1.2591e+07   939%     -   19s
H    0     0                    4102378.1708 1.2591e+07   207%     -   33s
     0     0 1.1872e+07    0  703 4102378.17 1.1872e+07   189%     -   35s
     0     0 1.1812e+07    0  759 4102378.17 1.1812e+07   188%     -   35s
     0     0 1.1806e+07    0  755 4102378.17 1.1806e+07   188%     -   35s
     0     0 1.1749e+07    0  762 4102378.17 1.1749e+07   186%     -   37s
     0     0 1.1736e+07    0  759 4102378.17 1.1736e+07   186%     -   37s
     0     0 1.1736e+07    0  828 4102378.17 1.1736e+07   186%     -   37s
     0     0 1.1699e+07    0  838 4102378.17 1.1699e+07   185%     -   38s
     0     0 1.1699e+07    0  845 4102378.17 1.1699e+07   185%     -   38s
     0     0 1.1698e+07    0  846 4102378.17 1.1698e+07   185%     -   39s
     0     0 1.1682e+07    0  752 4102378.17 1.1682e+07   185%     -   39s
     0     0 1.1680e+07    0  832 4102378.17 1.1680e+07   185%     -   40s
     0     0 1.1680e+07    0  765 4102378.17 1.1680e+07   185%     -   40s
     0     0 1.1679e+07    0  767 4102378.17 1.1679e+07   185%     -   41s
     0     0 1.1669e+07    0  846 4102378.17 1.1669e+07   184%     -   41s
     0     0 1.1668e+07    0  866 4102378.17 1.1668e+07   184%     -   42s
     0     0 1.1668e+07    0  868 4102378.17 1.1668e+07   184%     -   42s
     0     0 1.1666e+07    0  871 4102378.17 1.1666e+07   184%     -   43s
     0     0 1.1666e+07    0  872 4102378.17 1.1666e+07   184%     -   43s
     0     0 1.1664e+07    0  774 4102378.17 1.1664e+07   184%     -   44s
     0     0 1.1663e+07    0  771 4102378.17 1.1663e+07   184%     -   44s
     0     0 1.1663e+07    0  772 4102378.17 1.1663e+07   184%     -   44s
     0     0 1.1662e+07    0  774 4102378.17 1.1662e+07   184%     -   45s
     0     0 1.1662e+07    0  780 4102378.17 1.1662e+07   184%     -   45s
     0     0 1.1661e+07    0  855 4102378.17 1.1661e+07   184%     -   46s
     0     0 1.1661e+07    0  852 4102378.17 1.1661e+07   184%     -   46s
     0     0 1.1661e+07    0  857 4102378.17 1.1661e+07   184%     -   46s
     0     0 1.1659e+07    0  775 4102378.17 1.1659e+07   184%     -   47s
     0     0 1.1659e+07    0  783 4102378.17 1.1659e+07   184%     -   47s
     0     0 1.1659e+07    0  776 4102378.17 1.1659e+07   184%     -   48s
     0     0 1.1659e+07    0  778 4102378.17 1.1659e+07   184%     -   48s
     0     0 1.1657e+07    0  806 4102378.17 1.1657e+07   184%     -   49s
     0     0 1.1657e+07    0  802 4102378.17 1.1657e+07   184%     -   55s
     0     2 1.1657e+07    0  801 4102378.17 1.1657e+07   184%     -   62s
     3     6 1.1461e+07    2  804 4102378.17 1.1461e+07   179%   627   65s
    10    14 1.1373e+07    4  776 4102378.17 1.1373e+07   177%   391   70s
    33    32 8714384.90    9  358 4102378.17 1.1371e+07   177%   357   75s
    52    48 9069375.31   13  628 4102378.17 1.1371e+07   177%   366   82s
    69    69 8344023.76   15  537 4102378.17 1.1371e+07   177%   392   86s
   113   105 6953206.71   21  523 4102378.17 1.1371e+07   177%   343   90s
   198   173     cutoff   41      4102378.17 1.1371e+07   177%   291   95s
*  293   198              47    4477092.7228 1.1233e+07   151%   242   98s
   314   212 9567031.02    5  522 4477092.72 1.1167e+07   149%   246  100s
   399   280 9939648.88    5  450 4477092.72 1.1098e+07   148%   249  105s
   459   314 7661190.72   10  368 4477092.72 1.1098e+07   148%   252  110s
   490   341 6685672.29   15  321 4477092.72 1.1098e+07   148%   249  120s
H  559   379                    4547972.9349 1.0466e+07   130%   244  131s
H  561   374                    4620787.6506 1.0466e+07   127%   245  131s
   588   387 7262444.76    8  491 4620787.65 1.0466e+07   127%   249  135s
   702   445 6655131.80   18  605 4620787.65 1.0191e+07   121%   249  141s
   842   529 5390857.95   14  339 4620787.65 9921881.68   115%   237  146s
   976   580 6089164.07   19  437 4620787.65 9512373.97   106%   232  151s
  1044   617 6126378.81   22  802 4620787.65 9262648.46   100%   230  175s
H 1045   587                    4747041.1481 8527471.53  79.6%   230  183s
  1048   589 5642810.97   25  598 4747041.15 8323681.58  75.3%   229  185s
  1049   589 5795170.53   25  533 4747041.15 8323492.78  75.3%   229  194s
  1050   590 6802244.33    8  529 4747041.15 8323492.78  75.3%   229  195s
  1051   591 8306394.95   11  529 4747041.15 8323492.78  75.3%   229  200s
  1052   594 8229338.69   12  537 4747041.15 8229338.69  73.4%   240  227s
  1054   596 7978411.35   13  436 4747041.15 7978411.35  68.1%   241  232s
  1062   599 7009018.47   15  416 4747041.15 7670535.22  61.6%   245  235s
  1080   606 5134767.04   17  331 4747041.15 7007394.19  47.6%   246  246s
H 1083   577                    4837729.0291 7007394.19  44.8%   246  246s
  1132   572     cutoff   25      4837729.03 7007029.97  44.8%   246  250s
  1217   573 5234920.93   24  418 4837729.03 6480245.09  34.0%   244  255s
  1378   562 5059984.69   19  459 4837729.03 5869283.50  21.3%   233  260s
  1614   488 4990074.40   27  309 4837729.03 5526022.06  14.2%   222  265s
  1900   391 4856725.56   27  222 4837729.03 5359299.45  10.8%   207  272s
  2033   298 4986864.45   31  350 4837729.03 5249850.81  8.52%   202  275s

Cutting planes:
  Implied bound: 2
  Clique: 12
  MIR: 5
  Zero half: 6

Explored 2295 nodes (449894 simplex iterations) in 276.82 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: 4.83773e+06 4.74704e+06 4.62079e+06 ... -1.6556e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 4.837729029106e+06, best bound 4.837729029106e+06, gap 0.0000%
Took  283.96533012390137
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 71485 rows, 67035 columns and 226803 nonzeros
Variable types: 64293 continuous, 2742 integer (2742 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 71485 rows and 67035 columns
Presolve time: 0.16s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.20 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -4.4373e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -4.437299740300e+06, best bound -4.437299740300e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_1_fd_37

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 116
### |A|: 111
### |A^i|: 111
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -4,437,299.7403 (-4437299.7403)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 394, 67, 472, 373, 440, -3]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 472 (TRAMB In
###  |  @1019/Out@1067) - 373 (TRZMK In@1074/Out@1089) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  10,  161.0 containers from 260 to  67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  67
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 10 has the path [10, 385, 262, 401, 263, 149, 395, 47, 155, 480, 441, -3]
###  |  10 (BEANR Out@234) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPT
###  |  M In@406/Out@427) - 149 (ESALG In@452/Out@464) - 395 (GWOXB In@631/Out@710) - 47 (CIABJ In@751/Out@783) - 155 (ESALG 
###  |  In@932/Out@950) - 480 (TRMER In@1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 155
###  |   |- and carried Demand 260,   10.0 containers from 262 to 155
###  |   |- and carried Demand 261,  484.0 containers from 262 to 149
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 155
###  |   |- and carried Demand 273,   10.0 containers from 263 to 155
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 155
###  |   |- and carried Demand 286,   10.0 containers from 401 to 155
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 11 has the path [13, 477, 437, -3]
###  |  13 (BEANR Out@402) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [16, 478, 448, 449, 369, 439, -3]
###  |  16 (BEANR Out@570) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUS
###  |  K In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 17 has the path [190, 476, 435, -3]
###  |  190 (GBFXT Out@302) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [193, 368, 438, -3]
###  |  193 (GBFXT Out@470) - 368 (TRUSK In@844/Out@854) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  3 has the path [133, 388, -1]
###  |  133 (DEHAM Out@624) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 387, -1]
###  |  135 (DKAAR Out@220) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 556, 572, 563, 372, 485, 545, 349, 488, 548, 525, -4]
###  |  282 (NLRTM Out@92) - 556 (USCHS In@378/Out@388) - 572 (USORF In@413/Out@426) - 563 (USEWR In@450/Out@466) - 372 (TRZM
###  |  K In@785/Out@799) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 349 (PKBQM In@1218/Out@1246) - 488 (AE
###  |  JEA In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |   |- and carried Demand 547,  339.0 containers from 572 to 485
###  |   |- and carried Demand 548,  115.0 containers from 572 to 485
###  |   |- and carried Demand 550,    2.0 containers from 572 to 545
###  |   |- and carried Demand 551,  194.0 containers from 556 to 485
###  |   |- and carried Demand 552,   49.0 containers from 556 to 485
###  |   |- and carried Demand 553,   40.0 containers from 556 to 545
###  |
###  |- Vessel  6 has the path [283, 370, 521, -4]
###  |  283 (NLRTM Out@260) - 370 (TRZMK In@617/Out@631) - 521 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 522, -4]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |
###  |- Vessel 15 has the path [179, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  179 (FRLEH Out@158) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAI
###  |  LK In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 
###  |  (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 16 has the path [181, 362, 484, 544, 217, 487, 547, 524, -4]
###  |  181 (FRLEH Out@326) - 362 (TRAMB In@576/Out@603) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 217 (INP
###  |  AV In@1114/Out@1126) - 487 (AEJEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 555,  244.0 containers from 544 to 524
###  |   |- and carried Demand 556,   67.0 containers from 544 to 524
###  |   |- and carried Demand 558,  118.0 containers from 484 to 524
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |   |- and carried Demand 561,   15.0 containers from 484 to 524
###  |   |- and carried Demand 562,  267.0 containers from 484 to 524
###  |   |- and carried Demand 563,    1.0 containers from 484 to 524
###  |
###  |- Vessel 21 has the path [382, 483, 543, 520, -4]
###  |  382 (ZAPLZ Out@205) - 483 (AEJEA In@563/Out@587) - 543 (PKBQM In@644/Out@674) - 520 (INNSA In@738/Out@754) - -4 (DUMM
###  |  Y_END Ziel-Service 81)
###  |   |- and carried Demand 564,  108.0 containers from 543 to 520
###  |   |- and carried Demand 569,    1.0 containers from 543 to 520
###  |   |- and carried Demand 572,  156.0 containers from 483 to 543
###  |   |- and carried Demand 573,   45.0 containers from 483 to 543
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 23 has the path [375, 411, -2]
###  |  375 (UYMVD Out@216) - 411 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 361, 303, 0]
###  |  132 (DEHAM Out@456) - 361 (TRALI In@836/Out@854) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 360, 302, 0]
###  |  136 (DKAAR Out@388) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
###  |
### Vessels not used in the solution: [4, 8, 20, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	4m54.051s
user	15m44.121s
sys	1m8.658s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
