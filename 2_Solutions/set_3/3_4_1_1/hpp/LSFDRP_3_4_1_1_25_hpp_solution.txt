	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_1_fr.p
Instance LSFDP: LSFDRP_3_4_1_1_fd_25.p

This object contains the information regarding the instance 3_4_1_1 for a duration of 25 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.467155 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.753456 sec.
***  |   |- Create additional vessel information,				took 0.009567 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 51446 rows, 158492 columns and 1279295 nonzeros
Variable types: 36197 continuous, 122295 integer (122295 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 42535 rows and 95557 columns
Presolve time: 4.72s
Presolved: 8911 rows, 62935 columns, 300200 nonzeros
Variable types: 703 continuous, 62232 integer (61590 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.0280927e+08   4.175091e+04   0.000000e+00      5s
   12825    1.2435575e+07   0.000000e+00   0.000000e+00      9s

Root relaxation: objective 1.243557e+07, 12825 iterations, 3.52 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1656e+07    0  807          - 1.1656e+07      -     -    9s
H    0     0                    -1641350.838 1.1656e+07   810%     -   10s
H    0     0                    3954233.2367 1.1656e+07   195%     -   17s
     0     0 1.0733e+07    0  708 3954233.24 1.0733e+07   171%     -   19s
     0     0 1.0729e+07    0  728 3954233.24 1.0729e+07   171%     -   19s
     0     0 1.0651e+07    0  714 3954233.24 1.0651e+07   169%     -   19s
     0     0 1.0651e+07    0  717 3954233.24 1.0651e+07   169%     -   19s
     0     0 1.0503e+07    0  683 3954233.24 1.0503e+07   166%     -   20s
     0     0 1.0462e+07    0  673 3954233.24 1.0462e+07   165%     -   21s
     0     0 1.0462e+07    0  675 3954233.24 1.0462e+07   165%     -   21s
     0     0 1.0449e+07    0  678 3954233.24 1.0449e+07   164%     -   21s
     0     0 1.0448e+07    0  695 3954233.24 1.0448e+07   164%     -   22s
     0     0 1.0448e+07    0  578 3954233.24 1.0448e+07   164%     -   22s
     0     0 1.0448e+07    0  578 3954233.24 1.0448e+07   164%     -   22s
     0     0 1.0369e+07    0  795 3954233.24 1.0369e+07   162%     -   23s
     0     0 1.0364e+07    0  769 3954233.24 1.0364e+07   162%     -   23s
     0     0 1.0358e+07    0  764 3954233.24 1.0358e+07   162%     -   23s
     0     0 1.0337e+07    0  768 3954233.24 1.0337e+07   161%     -   24s
     0     0 1.0336e+07    0  773 3954233.24 1.0336e+07   161%     -   24s
     0     0 1.0328e+07    0  776 3954233.24 1.0328e+07   161%     -   24s
     0     0 1.0328e+07    0  778 3954233.24 1.0328e+07   161%     -   25s
     0     0 1.0328e+07    0  782 3954233.24 1.0328e+07   161%     -   25s
     0     0 1.0327e+07    0  781 3954233.24 1.0327e+07   161%     -   25s
     0     0 1.0327e+07    0  783 3954233.24 1.0327e+07   161%     -   26s
     0     0 1.0325e+07    0  683 3954233.24 1.0325e+07   161%     -   26s
     0     0 1.0325e+07    0  690 3954233.24 1.0325e+07   161%     -   26s
     0     0 1.0303e+07    0  889 3954233.24 1.0303e+07   161%     -   27s
     0     0 1.0243e+07    0  817 3954233.24 1.0243e+07   159%     -   28s
     0     0 1.0242e+07    0  733 3954233.24 1.0242e+07   159%     -   28s
     0     0 1.0241e+07    0  739 3954233.24 1.0241e+07   159%     -   28s
     0     0 1.0201e+07    0  614 3954233.24 1.0201e+07   158%     -   29s
     0     0 1.0199e+07    0  616 3954233.24 1.0199e+07   158%     -   29s
     0     0 1.0199e+07    0  616 3954233.24 1.0199e+07   158%     -   29s
     0     0 1.0197e+07    0  620 3954233.24 1.0197e+07   158%     -   30s
     0     0 1.0195e+07    0  787 3954233.24 1.0195e+07   158%     -   30s
     0     0 1.0193e+07    0  729 3954233.24 1.0193e+07   158%     -   31s
     0     0 1.0193e+07    0  722 3954233.24 1.0193e+07   158%     -   37s
H    0     2                    3954233.4053 1.0193e+07   158%     -   54s
     0     2 1.0193e+07    0  722 3954233.41 1.0193e+07   158%     -   54s
     1     4 9856072.95    1  632 3954233.41 9856072.95   149%   633   56s
    11    16 9767274.05    3  725 3954233.41 9767274.05   147%   455   60s
    19    18 9662697.22    5  798 3954233.41 9767211.58   147%   368   65s
    51    55 7756686.61   12  748 3954233.41 9767211.58   147%   369   71s
    76    75 8520690.55   16  699 3954233.41 9767211.58   147%   393   75s
H   88    89                    4261890.3504 9767211.58   129%   366   77s
   110   114 8116738.64   23  819 4261890.35 9767211.58   129%   351   80s
   209   199 5425859.11   38  270 4261890.35 9767211.58   129%   281   85s
   323   281 8132878.69    6  699 4261890.35 9354873.51   120%   245   90s
   466   383 4340352.90   38  308 4261890.35 9216806.16   116%   226   95s
   666   505 5946197.27   10  443 4261890.35 9110218.82   114%   199  100s
   808   567 5109724.03   22  294 4261890.35 8761328.36   106%   197  105s
   942   655 4492274.29   24  482 4261890.35 8633519.60   103%   197  110s
  1041   704 6211100.37   41  722 4261890.35 8633519.60   103%   195  128s
H 1042   669                    4441097.4868 8075020.61  81.8%   195  136s
H 1043   636                    4441097.6459 7536880.97  69.7%   195  139s
  1044   637 5453286.93   15  487 4441097.65 7356917.52  65.7%   195  140s
  1048   640 4688569.12   27  515 4441097.65 6999136.22  57.6%   194  145s
  1051   642 6408584.40   28  603 4441097.65 6802831.84  53.2%   194  150s
  1055   644 6372637.86   13  575 4441097.65 6632157.76  49.3%   193  164s
  1056   645 6172784.17   42  575 4441097.65 6632157.76  49.3%   193  166s
H 1056   612                    4589562.6151 6632157.76  44.5%   193  175s
  1058   615 6527160.67   11  547 4589562.62 6527160.67  42.2%   207  211s
  1063   619 6060844.06   13  526 4589562.62 6228565.60  35.7%   210  216s
  1086   619 4917392.01   16  347 4589562.62 5570256.60  21.4%   214  220s
  1223   590 4976546.12   24  345 4589562.62 5370686.11  17.0%   203  225s
* 1267   555              37    4599233.9180 5265702.22  14.5%   198  225s
  1363   541 4721410.85   18  423 4599233.92 5148454.44  11.9%   192  231s

Cutting planes:
  Gomory: 17
  Implied bound: 5
  Projected implied bound: 22
  Clique: 18
  MIR: 8
  Flow cover: 2
  Zero half: 22
  Mod-K: 1

Explored 1574 nodes (296334 simplex iterations) in 233.94 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: 4.59923e+06 4.58956e+06 4.4411e+06 ... -1.64135e+06
No other solutions better than 4.59923e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 4.599233918037e+06, best bound 4.599233918037e+06, gap 0.0000%
Took  241.24714922904968
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 67270 rows, 62814 columns and 211844 nonzeros
Variable types: 60240 continuous, 2574 integer (2574 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 2e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 67270 rows and 62814 columns
Presolve time: 0.11s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.16 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -609277 

Optimal solution found (tolerance 1.00e-04)
Best objective -6.092774127098e+05, best bound -6.092774127098e+05, gap 0.0000%

### Instance
### LSFDRP_3_4_1_1_fd_25

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 109
### |A|: 104
### |A^i|: 104
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -609,277.4127 (-609277.4127)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 394, 67, 480, 441, -3]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 67 (CMDLA In@765/Out@811) - 480 (TRMER In
###  |  @1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  10,  161.0 containers from 260 to  67
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  67
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  67
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 10 has the path [10, 476, 435, -3]
###  |  10 (BEANR Out@234) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [13, 477, 437, -3]
###  |  13 (BEANR Out@402) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [16, 479, 440, -3]
###  |  16 (BEANR Out@570) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [193, 448, 449, 369, 439, -3]
###  |  193 (GBFXT Out@470) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUSK In@1018/Out@1028) - 439 (A
###  |  EJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 23 has the path [375, 438, -3]
###  |  375 (UYMVD Out@216) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 385, 262, 401, 263, 149, 388, -1]
###  |  135 (DKAAR Out@220) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAP
###  |  TM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  312.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |- Vessel 14 has the path [136, 387, -1]
###  |  136 (DKAAR Out@388) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 556, 572, 563, 372, 485, 545, 349, 488, 548, 525, -4]
###  |  282 (NLRTM Out@92) - 556 (USCHS In@378/Out@388) - 572 (USORF In@413/Out@426) - 563 (USEWR In@450/Out@466) - 372 (TRZM
###  |  K In@785/Out@799) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 349 (PKBQM In@1218/Out@1246) - 488 (AE
###  |  JEA In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |   |- and carried Demand 547,  339.0 containers from 572 to 485
###  |   |- and carried Demand 548,  115.0 containers from 572 to 485
###  |   |- and carried Demand 550,    2.0 containers from 572 to 545
###  |   |- and carried Demand 551,  194.0 containers from 556 to 485
###  |   |- and carried Demand 552,   49.0 containers from 556 to 485
###  |   |- and carried Demand 553,   40.0 containers from 556 to 545
###  |
###  |- Vessel  6 has the path [283, 370, 484, 544, 217, 487, 547, 524, -4]
###  |  283 (NLRTM Out@260) - 370 (TRZMK In@617/Out@631) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 217 (INP
###  |  AV In@1114/Out@1126) - 487 (AEJEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -
###  |  4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 555,  244.0 containers from 544 to 524
###  |   |- and carried Demand 556,   67.0 containers from 544 to 524
###  |   |- and carried Demand 558,  118.0 containers from 484 to 524
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |   |- and carried Demand 561,   15.0 containers from 484 to 524
###  |   |- and carried Demand 562,  267.0 containers from 484 to 524
###  |   |- and carried Demand 563,    1.0 containers from 484 to 524
###  |
###  |- Vessel  7 has the path [285, 360, 521, -4]
###  |  285 (NLRTM Out@428) - 360 (TRALI In@668/Out@686) - 521 (INNSA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel  8 has the path [287, 368, 522, -4]
###  |  287 (NLRTM Out@596) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 15 has the path [179, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  179 (FRLEH Out@158) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAI
###  |  LK In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 
###  |  (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 16 has the path [181, 362, 520, -4]
###  |  181 (FRLEH Out@326) - 362 (TRAMB In@576/Out@603) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 17 has the path [190, 425, 421, 411, -2]
###  |  190 (GBFXT Out@302) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUM
###  |  MY_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 363, 302, 0]
###  |  132 (DEHAM Out@456) - 363 (TRAMB In@744/Out@771) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel  3 has the path [133, 478, 361, 303, 0]
###  |  133 (DEHAM Out@624) - 478 (TRMER In@747/Out@791) - 361 (TRALI In@836/Out@854) - 303 (PABLB In@1472/Out@1484) - 0 (DUM
###  |  MY_END Ziel-Service 177)
###  |
###  |- Vessel 19 has the path [216, 354, 249, 226, 71, 102, 99, 76, 88, 305, 0]
###  |  216 (INPAV Out@118) - 354 (SGSIN In@618/Out@651) - 249 (KRPUS In@788/Out@810) - 226 (JPHKT In@824/Out@833) - 71 (CNDL
###  |  C In@871/Out@886) - 102 (CNXGG In@904/Out@923) - 99 (CNTAO In@960/Out@978) - 76 (CNNGB In@1031/Out@1040) - 88 (CNSHA 
###  |  In@1069/Out@1079) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  45,  975.0 containers from  76 to 305
###  |   |- and carried Demand  46,    1.0 containers from  76 to 305
###  |   |- and carried Demand  48,  907.0 containers from  88 to 305
###  |   |- and carried Demand  49,    3.0 containers from  88 to 305
###  |
###  |
### Vessels not used in the solution: [4, 20, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	4m11.728s
user	13m31.774s
sys	1m13.853s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
