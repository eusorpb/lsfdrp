	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_1_fr.p
Instance LSFDP: LSFDRP_3_4_1_1_fd_49.p

This object contains the information regarding the instance 3_4_1_1 for a duration of 49 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took  0.43847 sec.
***  |   |- Calculate demand structure for node flow model,		took  1.43807 sec.
***  |   |- Create additional vessel information,				took  0.00833 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 48736 rows, 147965 columns and 1170258 nonzeros
Variable types: 34005 continuous, 113960 integer (113960 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 39911 rows and 86598 columns
Presolve time: 3.73s
Presolved: 8825 rows, 61367 columns, 292155 nonzeros
Variable types: 679 continuous, 60688 integer (60048 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
    7009    2.1100761e+07   5.175459e+03   0.000000e+00      5s
   12721    1.2523739e+07   0.000000e+00   0.000000e+00      8s

Root relaxation: objective 1.252374e+07, 12721 iterations, 3.41 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.1644e+07    0  889          - 1.1644e+07      -     -    8s
H    0     0                    -1918069.425 1.1644e+07   707%     -    9s
H    0     0                    2921553.2887 1.1644e+07   299%     -   18s
H    0     0                    2921553.4590 1.1644e+07   299%     -   20s
     0     0 1.0989e+07    0 1026 2921553.46 1.0989e+07   276%     -   21s
     0     0 1.0971e+07    0 1058 2921553.46 1.0971e+07   276%     -   22s
     0     0 1.0970e+07    0 1044 2921553.46 1.0970e+07   275%     -   22s
     0     0 1.0968e+07    0 1063 2921553.46 1.0968e+07   275%     -   22s
     0     0 1.0485e+07    0 1028 2921553.46 1.0485e+07   259%     -   25s
     0     0 1.0465e+07    0 1031 2921553.46 1.0465e+07   258%     -   25s
     0     0 1.0404e+07    0 1023 2921553.46 1.0404e+07   256%     -   26s
     0     0 1.0393e+07    0 1044 2921553.46 1.0393e+07   256%     -   26s
     0     0 1.0390e+07    0 1047 2921553.46 1.0390e+07   256%     -   27s
     0     0 1.0380e+07    0  936 2921553.46 1.0380e+07   255%     -   27s
     0     0 1.0369e+07    0 1052 2921553.46 1.0369e+07   255%     -   27s
     0     0 1.0368e+07    0 1056 2921553.46 1.0368e+07   255%     -   28s
     0     0 1.0368e+07    0 1059 2921553.46 1.0368e+07   255%     -   28s
     0     0 1.0366e+07    0 1076 2921553.46 1.0366e+07   255%     -   28s
     0     0 1.0345e+07    0  984 2921553.46 1.0345e+07   254%     -   28s
     0     0 1.0344e+07    0  995 2921553.46 1.0344e+07   254%     -   29s
     0     0 1.0344e+07    0  995 2921553.46 1.0344e+07   254%     -   29s
     0     0 1.0324e+07    0 1003 2921553.46 1.0324e+07   253%     -   30s
     0     0 1.0324e+07    0 1007 2921553.46 1.0324e+07   253%     -   30s
     0     0 1.0303e+07    0 1039 2921553.46 1.0303e+07   253%     -   31s
     0     0 1.0083e+07    0  704 2921553.46 1.0083e+07   245%     -   32s
     0     0 1.0083e+07    0  705 2921553.46 1.0083e+07   245%     -   32s
     0     0 9868965.19    0  704 2921553.46 9868965.19   238%     -   33s
     0     0 9779908.38    0  892 2921553.46 9779908.38   235%     -   39s
H    0     0                    3333011.5880 9779908.38   193%     -   42s
     0     0 9072706.22    0  830 3333011.59 9072706.22   172%     -   44s
     0     0 9009732.80    0  849 3333011.59 9009732.80   170%     -   45s
H    0     0                    3334411.2310 9009732.80   170%     -   45s
     0     0 8980802.99    0  859 3334411.23 8980802.99   169%     -   45s
H    0     0                    3425806.1683 8980802.99   162%     -   45s
     0     0 8980788.67    0  859 3425806.17 8980788.67   162%     -   45s
     0     0 8953367.79    0  951 3425806.17 8953367.79   161%     -   47s
     0     0 8813719.00    0  865 3425806.17 8813719.00   157%     -   47s
     0     0 8809627.47    0  781 3425806.17 8809627.47   157%     -   47s
     0     0 8705153.92    0  907 3425806.17 8705153.92   154%     -   48s
     0     0 8704190.15    0  915 3425806.17 8704190.15   154%     -   48s
     0     0 8704157.50    0  899 3425806.17 8704157.50   154%     -   48s
     0     0 8698515.33    0  804 3425806.17 8698515.33   154%     -   48s
     0     0 8685119.98    0  862 3425806.17 8685119.98   154%     -   49s
     0     0 8673472.49    0  719 3425806.17 8673472.49   153%     -   49s
     0     0 8671501.62    0  765 3425806.17 8671501.62   153%     -   49s
     0     0 8666336.39    0  760 3425806.17 8666336.39   153%     -   49s
     0     0 8666334.45    0  762 3425806.17 8666334.45   153%     -   49s
     0     0 8655232.52    0  729 3425806.17 8655232.52   153%     -   50s
     0     0 8655187.39    0  729 3425806.17 8655187.39   153%     -   50s
     0     0 8212722.34    0  616 3425806.17 8212722.34   140%     -   51s
     0     0 8195766.14    0  627 3425806.17 8195766.14   139%     -   52s
     0     0 8195027.69    0  636 3425806.17 8195027.69   139%     -   52s
     0     0 8169230.03    0  646 3425806.17 8169230.03   138%     -   52s
     0     0 8163656.04    0  647 3425806.17 8163656.04   138%     -   52s
     0     0 8163656.04    0  649 3425806.17 8163656.04   138%     -   52s
     0     0 8143068.04    0  640 3425806.17 8143068.04   138%     -   53s
     0     0 8143000.76    0  656 3425806.17 8143000.76   138%     -   53s
     0     0 8138976.76    0  634 3425806.17 8138976.76   138%     -   53s
     0     0 8138636.84    0  635 3425806.17 8138636.84   138%     -   54s
     0     0 8135353.50    0  677 3425806.17 8135353.50   137%     -   54s
     0     0 8135350.70    0  678 3425806.17 8135350.70   137%     -   54s
     0     0 8122641.50    0  700 3425806.17 8122641.50   137%     -   54s
     0     0 8119230.02    0  667 3425806.17 8119230.02   137%     -   55s
     0     0 8118790.63    0  662 3425806.17 8118790.63   137%     -   55s
     0     0 8118727.37    0  664 3425806.17 8118727.37   137%     -   55s
     0     0 8114422.36    0  662 3425806.17 8114422.36   137%     -   55s
     0     0 8114006.19    0  663 3425806.17 8114006.19   137%     -   55s
H    0     0                    3441592.8536 8114006.19   136%     -   56s
     0     0 8106049.58    0  633 3441592.85 8106049.58   136%     -   56s
     0     0 8106045.99    0  634 3441592.85 8106045.99   136%     -   56s
     0     0 8104923.63    0  678 3441592.85 8104923.63   135%     -   56s
     0     0 8104770.67    0  678 3441592.85 8104770.67   135%     -   56s
     0     0 8104038.73    0  676 3441592.85 8104038.73   135%     -   56s
     0     0 8104038.73    0  676 3441592.85 8104038.73   135%     -   62s
     0     2 8104038.73    0  674 3441592.85 8104038.73   135%     -   64s
     1     4 7810106.88    1  670 3441592.85 8040151.45   134%  1014   67s
     7    12 7370457.95    3  698 3441592.85 7435958.00   116%   733   70s
H   27    28                    3512185.7076 7097949.73   102%   438   85s
H   28    30                    3712232.5829 7097949.73  91.2%   441   85s
    96    84 6407796.46   20  704 3712232.58 7097949.73  91.2%   250   90s
   216   181 5370019.32   32  649 3712232.58 7097949.73  91.2%   190   95s
   351   268 4547205.43   42  498 3712232.58 7097949.73  91.2%   170  100s
   516   395 4934064.16    7  526 3712232.58 6686063.92  80.1%   166  105s
   645   472 4791026.19   18  561 3712232.58 6686063.92  80.1%   172  110s
   850   606 4712922.67   29  474 3712232.58 6433843.78  73.3%   168  116s
  1012   717 5554229.49   23  673 3712232.58 6193403.36  66.8%   162  120s
  1085   764 4240624.59   55  704 3712232.58 6193403.36  66.8%   160  137s
H 1086   726                    3726741.9309 6193403.36  66.2%   160  143s
  1087   726 4014063.14   52  530 3726741.93 6193403.36  66.2%   159  145s
  1091   729 3881674.35   37  640 3726741.93 6193403.36  66.2%   159  150s
  1096   732 4877343.64   20  665 3726741.93 6193403.36  66.2%   158  156s
  1099   734 3903068.84   16  661 3726741.93 6193403.36  66.2%   158  160s
  1102   736 4487839.45   24  724 3726741.93 6193403.36  66.2%   157  165s
  1107   740 4658402.06   22  716 3726741.93 6193403.36  66.2%   157  170s
  1111   742 4935439.15   34  721 3726741.93 6193403.36  66.2%   156  175s
  1117   746 4315471.57   27  711 3726741.93 6193403.36  66.2%   155  181s
  1118   747 4026174.34   31  748 3726741.93 6193403.36  66.2%   155  198s
  1119   748 5528636.94    9  747 3726741.93 6193403.36  66.2%   155  201s
H 1119   710                    3809608.5349 6193403.36  62.6%   155  221s
  1121   714 6149634.47   10  711 3809608.53 6193403.36  62.6%   170  273s
  1123   716 5900577.80   11  686 3809608.53 5900577.80  54.9%   172  278s
  1127   718 5567575.40   12  665 3809608.53 5900096.67  54.9%   174  281s
  1143   720 4927711.23   14  512 3809608.53 5567570.76  46.1%   177  285s
  1173   718 4608492.82   17  548 3809608.53 5567570.76  46.1%   180  291s
  1213   715 4526744.37   20  493 3809608.53 5519312.03  44.9%   183  295s
  1289   721     cutoff   28      3809608.53 5519312.03  44.9%   187  300s
  1360   721 4214899.94   18  543 3809608.53 5193872.15  36.3%   191  305s
  1487   717 4608963.48   19  599 3809608.53 5029888.68  32.0%   193  311s
H 1517   682                    3883125.1360 4855896.62  25.1%   196  321s
  1568   682 3899840.63   17  517 3883125.14 4786298.15  23.3%   201  325s
  1730   671 4373119.82   23  555 3883125.14 4625280.88  19.1%   203  331s
  1861   656 4353586.05   25  509 3883125.14 4522432.91  16.5%   207  336s
  1978   633 4266904.38   25  552 3883125.14 4508523.14  16.1%   207  340s
  2168   597 4102466.77   19  637 3883125.14 4425925.77  14.0%   209  346s
  2318   550 3898117.23   24  519 3883125.14 4371238.91  12.6%   210  351s
  2422   507 4056976.77   26  535 3883125.14 4352861.45  12.1%   210  355s
  2603   447 3888339.52   20  463 3883125.14 4301140.13  10.8%   210  360s
  2775   389 4013928.86   28  439 3883125.14 4262469.96  9.77%   208  366s
  2914   298 4161164.23   19  428 3883125.14 4229325.44  8.92%   207  386s
H 3016   232                    3883125.5348 4213740.03  8.51%   206  386s
  3208    99 4083509.66   36  439 3883125.53 4122370.01  6.16%   202  392s
* 3480    20              28    3909231.8522 3952200.38  1.10%   192  394s

Cutting planes:
  Gomory: 12
  Implied bound: 11
  Projected implied bound: 8
  Clique: 10
  MIR: 28
  Flow cover: 38
  Zero half: 27
  Mod-K: 2

Explored 3522 nodes (707788 simplex iterations) in 394.45 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: 3.90923e+06 3.88313e+06 3.88313e+06 ... 3.33441e+06
No other solutions better than 3.90923e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 3.909231852193e+06, best bound 3.909231852193e+06, gap 0.0000%
Took  400.5598576068878
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 63018 rows, 58593 columns and 197102 nonzeros
Variable types: 56187 continuous, 2406 integer (2406 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 63018 rows and 58593 columns
Presolve time: 0.09s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.12 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -6.29945e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -6.299450355961e+06, best bound -6.299450355961e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_1_fd_49

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 102
### |A|: 97
### |A^i|: 97
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -6,299,450.356 (-6299450.356)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 394, 68, 480, 441, -3]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 68 (CMDLA In@796/Out@842) - 480 (TRMER In
###  |  @1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  10,  161.0 containers from 260 to  68
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  68
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  68
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 10 has the path [10, 476, 435, -3]
###  |  10 (BEANR Out@234) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 11 has the path [13, 478, 448, 449, 369, 439, -3]
###  |  13 (BEANR Out@402) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUS
###  |  K In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 12 has the path [16, 479, 440, -3]
###  |  16 (BEANR Out@570) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 17 has the path [190, 477, 437, -3]
###  |  190 (GBFXT Out@302) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [193, 361, 438, -3]
###  |  193 (GBFXT Out@470) - 361 (TRALI In@836/Out@854) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 13 has the path [135, 387, -1]
###  |  135 (DKAAR Out@220) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 385, 262, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 385 (ESALG In@328/Out@347) - 262 (MAPTM In@375/Out@396) - 401 (MAPTM In@379/Out@398) - 263 (MAPT
###  |  M In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESALG In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  20,  484.0 containers from 262 to 149
###  |   |- and carried Demand  21,   18.0 containers from 262 to 149
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 259,  214.0 containers from 262 to 388
###  |   |- and carried Demand 260,   10.0 containers from 262 to 388
###  |   |- and carried Demand 262,   18.0 containers from 262 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  312.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 483, 543, 215, 487, 547, 524, -4]
###  |  282 (NLRTM Out@92) - 483 (AEJEA In@563/Out@587) - 543 (PKBQM In@644/Out@674) - 215 (INNSA In@1144/Out@1176) - 487 (AE
###  |  JEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 567,  146.0 containers from 543 to 215
###  |   |- and carried Demand 570,  704.0 containers from 483 to 524
###  |   |- and carried Demand 571,    8.0 containers from 483 to 524
###  |   |- and carried Demand 572,  156.0 containers from 483 to 543
###  |   |- and carried Demand 573,   45.0 containers from 483 to 543
###  |
###  |- Vessel  6 has the path [283, 370, 484, 544, 521, -4]
###  |  283 (NLRTM Out@260) - 370 (TRZMK In@617/Out@631) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 521 (INN
###  |  SA In@906/Out@922) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 554,   44.0 containers from 544 to 521
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 485, 545, 349, 488, 548, 525, -4]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 349 (PKBQM In@1218/Out@1246) - 488 (A
###  |  EJEA In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |
###  |- Vessel  8 has the path [287, 368, 522, -4]
###  |  287 (NLRTM Out@596) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 15 has the path [179, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  179 (FRLEH Out@158) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAI
###  |  LK In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 
###  |  (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 16 has the path [181, 362, 520, -4]
###  |  181 (FRLEH Out@326) - 362 (TRAMB In@576/Out@603) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 23 has the path [375, 411, -2]
###  |  375 (UYMVD Out@216) - 411 (HNPCR In@998/Out@1011) - -2 (DUMMY_END Ziel-Service 121)
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  2 has the path [132, 372, 303, 0]
###  |  132 (DEHAM Out@456) - 372 (TRZMK In@785/Out@799) - 303 (PABLB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel  3 has the path [133, 350, 305, 0]
###  |  133 (DEHAM Out@624) - 350 (RUNVS In@895/Out@925) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 14 has the path [136, 360, 302, 0]
###  |  136 (DKAAR Out@388) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [4, 19, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	6m49.178s
user	23m17.018s
sys	2m2.873s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
