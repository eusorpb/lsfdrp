	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_4_1_1_fr.p
Instance LSFDP: LSFDRP_3_4_1_1_fd_52.p

This object contains the information regarding the instance 3_4_1_1 for a duration of 52 weeks.

***  |- Create instance object for heuristic
***  |   |- Convert flexible visitations into inflexible visitations,	took 0.509637 sec.
***  |   |- Calculate demand structure for node flow model,		took 1.519593 sec.
***  |   |- Create additional vessel information,				took 0.008353 sec.
***  |
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 51120 rows, 157136 columns and 1267210 nonzeros
Variable types: 35374 continuous, 121762 integer (121762 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+00, 3e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+03]
Presolve removed 41915 rows and 96833 columns (presolve time = 6s) ...
Presolve removed 42703 rows and 96930 columns
Presolve time: 6.22s
Presolved: 8417 rows, 60206 columns, 285591 nonzeros
Variable types: 608 continuous, 59598 integer (59084 binary)

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.6381755e+08   3.224528e+04   0.000000e+00      7s
   11682    1.1561004e+07   0.000000e+00   0.000000e+00      9s

Root relaxation: objective 1.156100e+07, 11682 iterations, 2.81 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1.0822e+07    0  852          - 1.0822e+07      -     -   10s
H    0     0                    -2173967.930 1.0822e+07   598%     -   10s
H    0     0                    2609202.9394 1.0822e+07   315%     -   23s
     0     0 9855446.83    0  858 2609202.94 9855446.83   278%     -   25s
     0     0 9808065.27    0  855 2609202.94 9808065.27   276%     -   25s
     0     0 9782337.40    0  855 2609202.94 9782337.40   275%     -   25s
     0     0 9492777.43    0  844 2609202.94 9492777.43   264%     -   27s
     0     0 9462085.23    0  757 2609202.94 9462085.23   263%     -   27s
     0     0 9460341.04    0  780 2609202.94 9460341.04   263%     -   27s
     0     0 9459605.46    0  800 2609202.94 9459605.46   263%     -   27s
     0     0 9425842.49    0  867 2609202.94 9425842.49   261%     -   28s
     0     0 9422495.06    0  863 2609202.94 9422495.06   261%     -   28s
     0     0 9422433.98    0  872 2609202.94 9422433.98   261%     -   28s
     0     0 9393864.89    0  895 2609202.94 9393864.89   260%     -   28s
     0     0 9384791.37    0  707 2609202.94 9384791.37   260%     -   29s
     0     0 9382586.20    0  690 2609202.94 9382586.20   260%     -   29s
     0     0 9367644.35    0  735 2609202.94 9367644.35   259%     -   29s
     0     0 9367641.02    0  736 2609202.94 9367641.02   259%     -   30s
     0     0 9360234.29    0  719 2609202.94 9360234.29   259%     -   30s
     0     0 9355269.50    0  920 2609202.94 9355269.50   259%     -   30s
     0     0 9355121.31    0  933 2609202.94 9355121.31   259%     -   30s
     0     0 9320998.08    0  901 2609202.94 9320998.08   257%     -   31s
     0     0 9316809.15    0  928 2609202.94 9316809.15   257%     -   31s
     0     0 9316453.27    0  924 2609202.94 9316453.27   257%     -   31s
     0     0 9314101.38    0  921 2609202.94 9314101.38   257%     -   31s
     0     0 9309826.96    0  922 2609202.94 9309826.96   257%     -   32s
     0     0 9303666.56    0  925 2609202.94 9303666.56   257%     -   32s
     0     0 9302073.01    0  720 2609202.94 9302073.01   257%     -   33s
     0     0 9301593.95    0  725 2609202.94 9301593.95   256%     -   33s
     0     0 9295959.72    0  925 2609202.94 9295959.72   256%     -   33s
     0     0 9294548.07    0  970 2609202.94 9294548.07   256%     -   34s
     0     0 9294541.42    0  978 2609202.94 9294541.42   256%     -   34s
     0     0 9288849.46    0  921 2609202.94 9288849.46   256%     -   34s
     0     0 9285473.43    0  920 2609202.94 9285473.43   256%     -   35s
     0     0 9284295.60    0  913 2609202.94 9284295.60   256%     -   36s
     0     0 9284295.60    0  912 2609202.94 9284295.60   256%     -   42s
     0     2 9284295.60    0  910 2609202.94 9284295.60   256%     -   50s
     7    12 9205453.67    3  918 2609202.94 9246552.43   254%   560   55s
    27    28 infeasible    6      2609202.94 9246552.43   254%   363   65s
H   28    28                    3472704.9007 9246552.43   166%   350   65s
    55    52 8366584.86   15  564 3472704.90 9246552.43   166%   389   71s
    87    84 8096532.44   18  645 3472704.90 9246552.43   166%   366   75s
   200   163 6046333.51   30  600 3472704.90 9246552.43   166%   268   80s
   357   289 6529814.24   63  577 3472704.90 9246552.43   166%   211   85s
H  417   329                    3472705.1297 9246552.43   166%   199   94s
   424   330 6123317.67   82  611 3472705.13 9246552.43   166%   200   95s
   569   454 5170253.32  120  582 3472705.13 9246552.43   166%   193  100s
   770   626 4687340.18  163  571 3472705.13 9246552.43   166%   169  114s
H  804   641                    3524996.0519 9246552.43   162%   165  114s
   818   661 4530836.21  176  374 3524996.05 9246552.43   162%   163  116s
   975   764 5614617.17    6  469 3524996.05 8094687.08   130%   162  120s
  1058   818 8094687.08   19  912 3524996.05 8094687.08   130%   164  140s
  1060   819 4445682.91   34  534 3524996.05 7510270.21   113%   164  149s
  1061   820 4553461.63   19  601 3524996.05 6987920.35  98.2%   164  153s
  1065   823 5570654.87   99  732 3524996.05 6480606.53  83.8%   163  158s
  1067   824 4191189.00   27  736 3524996.05 6469382.67  83.5%   163  160s
  1070   826 6392413.62   34  631 3524996.05 6392413.62  81.3%   163  165s
  1073   828 3815309.18   21  702 3524996.05 6245984.15  77.2%   162  170s
  1076   830 5875026.70   22  691 3524996.05 6242699.15  77.1%   162  189s
  1077   831 6242699.15   63  689 3524996.05 6242699.15  77.1%   162  191s
H 1077   789                    3524996.1780 6242699.15  77.1%   162  213s
  1079   792 6092108.50   16  632 3524996.18 6128880.85  73.9%   176  258s
  1081   794 6008923.39   17  558 3524996.18 6016006.23  70.7%   178  262s
  1084   797 5806732.10   18  583 3524996.18 6008829.51  70.5%   180  265s
  1096   805 4751612.12   20  528 3524996.18 5233563.45  48.5%   188  270s
  1154   808 4522019.67   21  527 3524996.18 5233223.58  48.5%   189  275s
  1214   804 4025912.79   25  411 3524996.18 5217674.87  48.0%   189  280s
  1351   805 3861658.35   22  353 3524996.18 4763989.86  35.1%   187  285s
* 1505   742              39    3635052.7664 4418847.97  21.6%   181  288s
  1534   735 3803206.56   30  446 3635052.77 4310625.92  18.6%   181  290s
  1783   716 3992537.85   30  477 3635052.77 4171064.53  14.7%   174  295s
  2059   626     cutoff   35      3635052.77 4076218.35  12.1%   169  300s
  2449   476 3739603.46   29  482 3635052.77 3965646.28  9.09%   162  306s
  2658   390     cutoff   25      3635052.77 3912763.05  7.64%   157  311s
H 2666   349                    3635052.9153 3912763.05  7.64%   157  311s
H 2668   309                    3661159.4825 3903368.59  6.62%   157  311s

Cutting planes:
  Gomory: 13
  Implied bound: 10
  Projected implied bound: 31
  Clique: 14
  MIR: 16
  Flow cover: 6
  Zero half: 37

Explored 2966 nodes (456182 simplex iterations) in 313.87 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: 3.66116e+06 3.63505e+06 3.63505e+06 ... -2.17397e+06
No other solutions better than 3.66116e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 3.661159482517e+06, best bound 3.661159482517e+06, gap 0.0000%
Took  320.3457729816437
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Optimize a model with 65413 rows, 61005 columns and 205613 nonzeros
Variable types: 58503 continuous, 2502 integer (2502 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+08]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 8e+02]
Presolve removed 65413 rows and 61005 columns
Presolve time: 0.10s
Presolve: All rows and columns removed

Explored 0 nodes (0 simplex iterations) in 0.14 seconds
Thread count was 1 (of 16 available processors)

Solution count 1: -7.17254e+06 

Optimal solution found (tolerance 1.00e-04)
Best objective -7.172544085298e+06, best bound -7.172544085298e+06, gap 0.0000%

### Instance
### LSFDRP_3_4_1_1_fd_52

### Graph info
### |S|: 24
### |S^E|: 0
### |S^C|: 6
### |V|: 106
### |A|: 101
### |A^i|: 101
### |A^f|: 0
### |M|: 575
### |E|: 28
#############################################################################################################################
###
### Results
###
### The objective of the solution is -7,172,544.0853 (-7172544.0853)
###
### Service 7 (ME3) is operated by 6 vessels from type 30 (PMax35)
###  |
###  |- Vessel  9 has the path [9, 384, 260, 400, 261, 147, 394, 68, 480, 441, -3]
###  |  9 (BEANR Out@66) - 384 (ESALG In@160/Out@179) - 260 (MAPTM In@207/Out@228) - 400 (MAPTM In@211/Out@230) - 261 (MAPTM 
###  |  In@238/Out@259) - 147 (ESALG In@284/Out@296) - 394 (GWOXB In@463/Out@542) - 68 (CMDLA In@796/Out@842) - 480 (TRMER In
###  |  @1083/Out@1127) - 441 (AEJEA In@1530/Out@1563) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand  10,  161.0 containers from 260 to  68
###  |   |- and carried Demand  11,  580.0 containers from 260 to 147
###  |   |- and carried Demand  59,  161.0 containers from 261 to  68
###  |   |- and carried Demand  60,  580.0 containers from 261 to 147
###  |   |- and carried Demand 257,  580.0 containers from 260 to 147
###  |   |- and carried Demand 271,  580.0 containers from 261 to 147
###  |   |- and carried Demand 294,  161.0 containers from 400 to  68
###  |   |- and carried Demand 295,  580.0 containers from 400 to 147
###  |
###  |- Vessel 11 has the path [13, 477, 437, -3]
###  |  13 (BEANR Out@402) - 477 (TRMER In@579/Out@623) - 437 (AEJEA In@858/Out@891) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 12 has the path [16, 478, 448, 449, 369, 439, -3]
###  |  16 (BEANR Out@570) - 478 (TRMER In@747/Out@791) - 448 (EGPSD In@867/Out@886) - 449 (EGPSD In@957/Out@973) - 369 (TRUS
###  |  K In@1018/Out@1028) - 439 (AEJEA In@1194/Out@1227) - -3 (DUMMY_END Ziel-Service 7)
###  |   |- and carried Demand 364,  467.0 containers from 448 to 439
###  |   |- and carried Demand 368,  467.0 containers from 449 to 439
###  |
###  |- Vessel 17 has the path [190, 476, 435, -3]
###  |  190 (GBFXT Out@302) - 476 (TRMER In@411/Out@455) - 435 (AEJEA In@690/Out@723) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 18 has the path [193, 479, 440, -3]
###  |  193 (GBFXT Out@470) - 479 (TRMER In@915/Out@959) - 440 (AEJEA In@1362/Out@1395) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |- Vessel 23 has the path [375, 438, -3]
###  |  375 (UYMVD Out@216) - 438 (AEJEA In@1026/Out@1059) - -3 (DUMMY_END Ziel-Service 7)
###  |
###  |
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  1 has the path [131, 386, -1]
###  |  131 (DEHAM Out@288) - 386 (ESALG In@496/Out@515) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel  2 has the path [132, 387, -1]
###  |  132 (DEHAM Out@456) - 387 (ESALG In@664/Out@683) - -1 (DUMMY_END Ziel-Service 24)
###  |
###  |- Vessel 20 has the path [164, 401, 263, 149, 388, -1]
###  |  164 (ESBCN Out@94) - 401 (MAPTM In@379/Out@398) - 263 (MAPTM In@406/Out@427) - 149 (ESALG In@452/Out@464) - 388 (ESAL
###  |  G In@832/Out@851) - -1 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  66,  484.0 containers from 263 to 149
###  |   |- and carried Demand  67,   18.0 containers from 263 to 149
###  |   |- and carried Demand 272,  214.0 containers from 263 to 388
###  |   |- and carried Demand 273,   10.0 containers from 263 to 388
###  |   |- and carried Demand 274,  484.0 containers from 263 to 149
###  |   |- and carried Demand 275,   18.0 containers from 263 to 149
###  |   |- and carried Demand 285,  214.0 containers from 401 to 388
###  |   |- and carried Demand 286,   10.0 containers from 401 to 388
###  |   |- and carried Demand 287,  484.0 containers from 401 to 149
###  |   |- and carried Demand 288,   18.0 containers from 401 to 149
###  |
###  |
### Service 81 (MECL1) is operated by 6 vessels from type 19 (PMax28)
###  |
###  |- Vessel  5 has the path [282, 483, 543, 215, 487, 547, 524, -4]
###  |  282 (NLRTM Out@92) - 483 (AEJEA In@563/Out@587) - 543 (PKBQM In@644/Out@674) - 215 (INNSA In@1144/Out@1176) - 487 (AE
###  |  JEA In@1235/Out@1259) - 547 (PKBQM In@1316/Out@1346) - 524 (INNSA In@1410/Out@1426) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 499,   31.0 containers from 547 to 524
###  |   |- and carried Demand 500,   37.0 containers from 487 to 524
###  |   |- and carried Demand 501,  211.0 containers from 487 to 524
###  |   |- and carried Demand 502,  176.0 containers from 487 to 547
###  |   |- and carried Demand 567,  146.0 containers from 543 to 215
###  |   |- and carried Demand 570,  704.0 containers from 483 to 524
###  |   |- and carried Demand 571,    8.0 containers from 483 to 524
###  |   |- and carried Demand 572,  156.0 containers from 483 to 543
###  |   |- and carried Demand 573,   45.0 containers from 483 to 543
###  |
###  |- Vessel  6 has the path [283, 180, 262, 148, 370, 484, 544, 521, -4]
###  |  283 (NLRTM Out@260) - 180 (FRLEH In@279/Out@295) - 262 (MAPTM In@375/Out@396) - 148 (ESALG In@421/Out@433) - 370 (TRZ
###  |  MK In@617/Out@631) - 484 (AEJEA In@731/Out@755) - 544 (PKBQM In@812/Out@842) - 521 (INNSA In@906/Out@922) - -4 (DUMMY
###  |  _END Ziel-Service 81)
###  |   |- and carried Demand  16,   74.0 containers from 283 to 148
###  |   |- and carried Demand  17,    5.0 containers from 283 to 180
###  |   |- and carried Demand  20,  484.0 containers from 262 to 148
###  |   |- and carried Demand  21,   18.0 containers from 262 to 148
###  |   |- and carried Demand 258,   74.0 containers from 283 to 148
###  |   |- and carried Demand 261,  484.0 containers from 262 to 148
###  |   |- and carried Demand 262,   18.0 containers from 262 to 148
###  |   |- and carried Demand 554,   44.0 containers from 544 to 521
###  |   |- and carried Demand 559,  133.0 containers from 484 to 544
###  |   |- and carried Demand 560,   61.0 containers from 484 to 544
###  |
###  |- Vessel  7 has the path [285, 182, 264, 150, 371, 485, 545, 349, 488, 548, 525, -4]
###  |  285 (NLRTM Out@428) - 182 (FRLEH In@447/Out@463) - 264 (MAPTM In@543/Out@564) - 150 (ESALG In@589/Out@601) - 371 (TRZ
###  |  MK In@778/Out@793) - 485 (AEJEA In@899/Out@923) - 545 (PKBQM In@980/Out@1010) - 349 (PKBQM In@1218/Out@1246) - 488 (A
###  |  EJEA In@1403/Out@1427) - 548 (PKBQM In@1484/Out@1514) - 525 (INNSA In@1578/Out@1594) - -4 (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand  31,   68.0 containers from 285 to 150
###  |   |- and carried Demand  32,    6.0 containers from 285 to 182
###  |   |- and carried Demand  35,   60.0 containers from 264 to 150
###  |   |- and carried Demand  36,   11.0 containers from 264 to 150
###  |   |- and carried Demand 263,   68.0 containers from 285 to 150
###  |   |- and carried Demand 266,   60.0 containers from 264 to 150
###  |   |- and carried Demand 267,   11.0 containers from 264 to 150
###  |   |- and carried Demand 475,   34.0 containers from 548 to 525
###  |   |- and carried Demand 476,   84.0 containers from 488 to 548
###  |   |- and carried Demand 477,   50.0 containers from 488 to 548
###  |   |- and carried Demand 535,  254.0 containers from 545 to 525
###  |   |- and carried Demand 536,    1.0 containers from 545 to 525
###  |   |- and carried Demand 537,  135.0 containers from 545 to 525
###  |   |- and carried Demand 540,   75.0 containers from 485 to 525
###  |   |- and carried Demand 541,  200.0 containers from 485 to 545
###  |   |- and carried Demand 542,   24.0 containers from 485 to 545
###  |   |- and carried Demand 543,   26.0 containers from 485 to 525
###  |   |- and carried Demand 544,  144.0 containers from 485 to 525
###  |   |- and carried Demand 545,    1.0 containers from 485 to 525
###  |
###  |- Vessel  8 has the path [287, 368, 522, -4]
###  |  287 (NLRTM Out@596) - 368 (TRUSK In@844/Out@854) - 522 (INNSA In@1074/Out@1090) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |- Vessel 15 has the path [179, 557, 573, 565, 374, 486, 546, 523, -4]
###  |  179 (FRLEH Out@158) - 557 (USCHS In@546/Out@556) - 573 (USORF In@581/Out@594) - 565 (USEWR In@618/Out@634) - 374 (UAI
###  |  LK In@957/Out@992) - 486 (AEJEA In@1067/Out@1091) - 546 (PKBQM In@1148/Out@1178) - 523 (INNSA In@1242/Out@1258) - -4 
###  |  (DUMMY_END Ziel-Service 81)
###  |   |- and carried Demand 522,   86.0 containers from 486 to 546
###  |   |- and carried Demand 523,  118.0 containers from 486 to 546
###  |   |- and carried Demand 527,  418.0 containers from 573 to 486
###  |   |- and carried Demand 528,  123.0 containers from 573 to 486
###  |   |- and carried Demand 529,   61.0 containers from 573 to 546
###  |   |- and carried Demand 530,   64.0 containers from 557 to 486
###  |   |- and carried Demand 531,   49.0 containers from 557 to 546
###  |   |- and carried Demand 532,   24.0 containers from 557 to 546
###  |
###  |- Vessel 16 has the path [181, 362, 520, -4]
###  |  181 (FRLEH Out@326) - 362 (TRAMB In@576/Out@603) - 520 (INNSA In@738/Out@754) - -4 (DUMMY_END Ziel-Service 81)
###  |
###  |
### Service 121 (SAE) is operated by 2 vessels from type 30 (PMax35)
###  |
###  |- Vessel 10 has the path [10, 425, 421, 411, -2]
###  |  10 (BEANR Out@234) - 425 (USORF In@873/Out@881) - 421 (USMIA In@935/Out@943) - 411 (HNPCR In@998/Out@1011) - -2 (DUMM
###  |  Y_END Ziel-Service 121)
###  |   |- and carried Demand 325,   10.0 containers from 421 to 411
###  |   |- and carried Demand 326,   75.0 containers from 425 to 411
###  |   |- and carried Demand 327,  114.0 containers from 425 to 411
###  |
###  |- Vessel 24 has the path [231, 106, 78, 210, 107, 75, 86, 270, 300, 412, -2]
###  |  231 (JPYOK Out@171) - 106 (CNXMN In@238/Out@260) - 78 (CNNSA In@287/Out@297) - 210 (HKHKG In@334/Out@350) - 107 (CNYT
###  |  N In@358/Out@376) - 75 (CNNGB In@430/Out@439) - 86 (CNSHA In@465/Out@483) - 270 (MXLZC In@873/Out@896) - 300 (PABLB I
###  |  n@982/Out@1030) - 412 (HNPCR In@1166/Out@1179) - -2 (DUMMY_END Ziel-Service 121)
###  |   |- and carried Demand  87,  541.0 containers from 231 to 300
###  |   |- and carried Demand 178,  785.0 containers from 106 to 210
###  |   |- and carried Demand 179,    1.0 containers from 106 to 210
###  |   |- and carried Demand 181,  121.0 containers from 210 to  75
###  |   |- and carried Demand 182,   75.0 containers from 210 to  86
###  |   |- and carried Demand 183,    1.0 containers from 210 to 107
###  |   |- and carried Demand 184,   17.0 containers from 210 to 270
###  |   |- and carried Demand 185,  339.0 containers from 107 to 270
###  |   |- and carried Demand 186,    6.0 containers from 107 to 300
###  |   |- and carried Demand 187,  166.0 containers from  75 to 270
###  |   |- and carried Demand 188,  773.0 containers from  75 to 300
###  |   |- and carried Demand 189,    1.0 containers from  75 to 300
###  |   |- and carried Demand 192,  298.0 containers from 270 to 300
###  |   |- and carried Demand 193,  199.0 containers from 270 to 300
###  |   |- and carried Demand 297,    6.0 containers from 107 to 300
###  |   |- and carried Demand 298,  773.0 containers from  75 to 300
###  |   |- and carried Demand 299,    1.0 containers from  75 to 300
###  |   |- and carried Demand 300,  298.0 containers from 270 to 300
###  |   |- and carried Demand 301,  199.0 containers from 270 to 300
###  |
###  |
### Service 177 (WCSA) is operated by 3 vessels from type 18 (PMax25)
###  |
###  |- Vessel  4 has the path [134, 365, 305, 0]
###  |  134 (DEHAM Out@792) - 365 (TRAMB In@1040/Out@1064) - 305 (PABLB In@1640/Out@1652) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |- Vessel 13 has the path [135, 385, 151, 361, 303, 0]
###  |  135 (DKAAR Out@220) - 385 (ESALG In@328/Out@347) - 151 (ESALG In@625/Out@670) - 361 (TRALI In@836/Out@854) - 303 (PAB
###  |  LB In@1472/Out@1484) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand 140,  552.0 containers from 151 to 303
###  |   |- and carried Demand 141,   13.0 containers from 151 to 303
###  |
###  |- Vessel 14 has the path [136, 360, 302, 0]
###  |  136 (DKAAR Out@388) - 360 (TRALI In@668/Out@686) - 302 (PABLB In@1304/Out@1316) - 0 (DUMMY_END Ziel-Service 177)
###  |
###  |
### Vessels not used in the solution: [3, 19, 21, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

real	5m30.742s
user	18m16.772s
sys	1m36.843s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
