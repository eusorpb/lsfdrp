	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_3_1_1_0_fr.p
Instance LSFDP: LSFDRP_3_1_1_0_fd_46.p

Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 57 rows, 75 columns and 222 nonzeros
Variable types: 0 continuous, 75 integer (75 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+00]
  Objective range  [9e+05, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Found heuristic solution: objective -2.09864e+07
Presolve removed 10 rows and 12 columns
Presolve time: 0.00s
Presolved: 47 rows, 63 columns, 163 nonzeros
Variable types: 0 continuous, 63 integer (63 binary)

Root relaxation: objective -0.000000e+00, 11 iterations, 0.00 seconds
Warning: Failed to open log file 'gurobi.log'

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0      -0.0000000   -0.00000  0.00%     -    0s

Explored 0 nodes (11 simplex iterations) in 0.01 seconds
Thread count was 4 (of 16 available processors)

Solution count 2: -0 -2.09864e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective -0.000000000000e+00, best bound -0.000000000000e+00, gap 0.0000%

### Objective value in 1k: -0.00
### Objective value: -0.0000
### Gap: 0.0
###
### Service: vessel typ and number of vessels
### 24(WAF7): 18, (PMax25), 3
### 177(WCSA): 18, (PMax25), 3
### ---
### Charter vessels chartered in:
### ---
### Own vessels chartered out:
### ---
### End of overview

### All variables with value greater zero:
rho[24,18] = 1
rho[177,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
eta[177,18,1] = 1
eta[177,18,2] = 1
eta[177,18,3] = 1
y[1,24] = 1
y[2,24] = 1
y[3,177] = 1
y[4,177] = 1
y[13,177] = 1
y[14,24] = 1

### All variables with value zero:
rho[24,19] = -0
rho[24,30] = -0
rho[177,19] = -0
rho[177,30] = -0
eta[24,18,4] = -0
eta[24,19,1] = 0
eta[24,19,2] = 0
eta[24,19,3] = 0
eta[24,19,4] = -0
eta[24,30,1] = 0
eta[24,30,2] = 0
eta[24,30,3] = 0
eta[24,30,4] = -0
eta[177,19,1] = 0
eta[177,19,2] = 0
eta[177,19,3] = 0
eta[177,30,1] = 0
eta[177,30,2] = 0
eta[177,30,3] = 0
y[1,177] = -0
y[2,177] = -0
y[3,24] = -0
y[4,24] = -0
y[5,24] = -0
y[5,177] = -0
y[6,24] = -0
y[6,177] = -0
y[7,24] = -0
y[7,177] = -0
y[8,24] = -0
y[8,177] = -0
y[9,24] = -0
y[9,177] = -0
y[10,24] = -0
y[10,177] = -0
y[11,24] = -0
y[11,177] = -0
y[12,24] = -0
y[12,177] = -0
y[13,24] = 0
y[14,177] = 0
y[15,24] = -0
y[15,177] = -0
y[16,24] = -0
y[16,177] = -0
y[17,24] = -0
y[17,177] = -0
y[18,24] = -0
y[18,177] = -0
y[19,24] = -0
y[19,177] = -0
y[20,24] = -0
y[20,177] = -0
y[21,24] = -0
y[21,177] = -0
y[22,24] = -0
y[22,177] = -0
y[23,24] = -0
y[23,177] = -0
y[24,24] = -0
y[24,177] = -0

### End

real	0m0.611s
user	0m0.291s
sys	0m0.084s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
