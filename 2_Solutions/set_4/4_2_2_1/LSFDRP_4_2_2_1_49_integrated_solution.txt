	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_2_2_1_fr.p
Instance LSFDP: LSFDRP_4_2_2_1_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_2_2_1_fd_49 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 152
*** |A|: 1246
*** |A^i|: 1246
*** |A^f|: 0
*** |M|: 71
*** |E|: 0

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 16732 rows, 114960 columns and 476060 nonzeros
Variable types: 91271 continuous, 23689 integer (23689 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 4e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Presolve removed 3412 rows and 8987 columns (presolve time = 35s) ...
Presolve removed 3412 rows and 10507 columns (presolve time = 36s) ...
Presolve removed 4094 rows and 22804 columns (presolve time = 68s) ...
Presolve removed 4666 rows and 29034 columns (presolve time = 91s) ...
Presolve removed 4666 rows and 29034 columns
Presolve time: 90.75s
Presolved: 12066 rows, 85926 columns, 302416 nonzeros
Variable types: 76338 continuous, 9588 integer (9565 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -1.0966703e+09   1.013043e+05   0.000000e+00     92s
    5417   -3.8254763e+06   0.000000e+00   0.000000e+00     92s

Root relaxation: objective -3.825476e+06, 5417 iterations, 0.44 seconds
Total elapsed time = 92.21s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 2112715.29    0   20          - 2112715.29      -     -   92s
H    0     0                    -2249751.871 2112715.29   194%     -   92s
H    0     0                    -564982.3726 2112715.29   474%     -   92s
H    0     0                    -100290.9690 2112715.29  2207%     -   92s
H    0     0                    489254.05578 2112715.29   332%     -   92s
     0     0 1839191.22    0   34 489254.056 1839191.22   276%     -   92s
     0     0 1839191.21    0   31 489254.056 1839191.21   276%     -   93s
H    0     0                    489254.08934 1839191.21   276%     -   93s
     0     0 1839191.21    0   31 489254.089 1839191.21   276%     -   93s
     0     0 1838089.06    0   34 489254.089 1838089.06   276%     -   93s
     0     0 1817023.16    0   32 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   27 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   27 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   31 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   37 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   27 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   30 489254.089 1817023.16   271%     -   93s
     0     0 1817023.16    0   30 489254.089 1817023.16   271%     -   93s
     0     2 1817023.16    0   30 489254.089 1817023.16   271%     -   93s
*    8     8               3    1764567.4704 1766609.88  0.12%   134   93s

Cutting planes:
  Implied bound: 1
  MIR: 2
  Flow cover: 1
  Zero half: 1

Explored 14 nodes (13206 simplex iterations) in 93.93 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: 1.76457e+06 489254 489254 ... -2.24975e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 1.764567426168e+06, best bound 1.764567470523e+06, gap 0.0000%

***
***  |- Calculation finished, took 93.94 (93.94) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4262 (1764567.4262) and is feasible with a Gap of 0.0
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [48, 60, 0]
###  |  48 (DEHAM Out@734.0) - 60 (ESALG In@942.0/Out@961.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [49, 62, 0]
###  |  49 (DEHAM Out@902.0) - 62 (ESALG In@1110.0/Out@1129.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [52, 57, 120, 121, 122, 59, 64, 0]
###  |  52 (DKAAR Out@666.0) - 57 (ESALG In@774.0/Out@793.0) - 120 (MAPTM In@821.0/Out@842.0) - 121 (MAPTM In@825.0/Out@844.0
###  |  ) - 122 (MAPTM In@852.0/Out@873.0) - 59 (ESALG In@898.0/Out@910.0) - 64 (ESALG In@1278.0/Out@1297.0) - 0 (DUMMY_END Z
###  |  iel-Service 24)
###  |   |- and carried Demand 11,  214.0 containers from 120 to  64
###  |   |- and carried Demand 12,   10.0 containers from 120 to  64
###  |   |- and carried Demand 13,  484.0 containers from 120 to  59
###  |   |- and carried Demand 14,   18.0 containers from 120 to  59
###  |   |- and carried Demand 28,  214.0 containers from 122 to  64
###  |   |- and carried Demand 29,   10.0 containers from 122 to  64
###  |   |- and carried Demand 30,  484.0 containers from 122 to  59
###  |   |- and carried Demand 31,   18.0 containers from 122 to  59
###  |   |- and carried Demand 61,  214.0 containers from 121 to  64
###  |   |- and carried Demand 62,   10.0 containers from 121 to  64
###  |   |- and carried Demand 63,  484.0 containers from 121 to  59
###  |   |- and carried Demand 64,   18.0 containers from 121 to  59
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[1,60,0] = 1
y[1,48,60] = 1
y[2,49,62] = 1
y[2,62,0] = 1
y[13,59,64] = 1
y[13,52,57] = 1
y[13,122,59] = 1
y[13,64,0] = 1
y[13,120,121] = 1
y[13,57,120] = 1
y[13,121,122] = 1
xD[11,59,64] = 214
xD[11,122,59] = 214
xD[11,120,121] = 214
xD[11,121,122] = 214
xD[12,59,64] = 10
xD[12,122,59] = 10
xD[12,120,121] = 10
xD[12,121,122] = 10
xD[13,122,59] = 484
xD[13,120,121] = 484
xD[13,121,122] = 484
xD[14,122,59] = 18
xD[14,120,121] = 18
xD[14,121,122] = 18
xD[28,59,64] = 214
xD[28,122,59] = 214
xD[29,59,64] = 10
xD[29,122,59] = 10
xD[30,122,59] = 484
xD[31,122,59] = 18
xD[61,59,64] = 214
xD[61,122,59] = 214
xD[61,121,122] = 214
xD[62,59,64] = 10
xD[62,122,59] = 10
xD[62,121,122] = 10
xD[63,122,59] = 484
xD[63,121,122] = 484
xD[64,122,59] = 18
xD[64,121,122] = 18
zE[57] = 774
zE[59] = 898
zE[60] = 942
zE[62] = 1110
zE[64] = 1278
zE[120] = 821
zE[121] = 825
zE[122] = 852
zX[48] = 734
zX[49] = 902
zX[52] = 666
zX[57] = 793
zX[59] = 910
zX[60] = 961
zX[62] = 1129
zX[64] = 1297
zX[120] = 842
zX[121] = 844
zX[122] = 873
phi[1] = 227
phi[2] = 227
phi[13] = 631

### All variables with value smaller zero:

### End

real	1m39.756s
user	1m46.818s
sys	0m2.053s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
