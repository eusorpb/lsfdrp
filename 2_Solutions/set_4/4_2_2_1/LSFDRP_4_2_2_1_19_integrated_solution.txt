	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_2_2_1_fr.p
Instance LSFDP: LSFDRP_4_2_2_1_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_2_2_1_fd_19 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 152
*** |A|: 1246
*** |A^i|: 1246
*** |A^f|: 0
*** |M|: 71
*** |E|: 0

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 16732 rows, 114960 columns and 476060 nonzeros
Variable types: 91271 continuous, 23689 integer (23689 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Presolve removed 3412 rows and 8987 columns (presolve time = 29s) ...
Presolve removed 3412 rows and 10507 columns (presolve time = 31s) ...
Presolve removed 4094 rows and 22804 columns (presolve time = 56s) ...
Presolve removed 4666 rows and 29034 columns (presolve time = 80s) ...
Presolve removed 4666 rows and 29034 columns
Presolve time: 80.04s
Presolved: 12066 rows, 85926 columns, 302416 nonzeros
Variable types: 76338 continuous, 9588 integer (9565 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -1.0966703e+09   1.013043e+05   0.000000e+00     81s
    5583   -4.1846258e+06   0.000000e+00   0.000000e+00     82s

Root relaxation: objective -4.184626e+06, 5583 iterations, 0.52 seconds
Total elapsed time = 81.73s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 3332395.29    0   20          - 3332395.29      -     -   82s
H    0     0                    -1030071.871 3332395.29   424%     -   82s
H    0     0                    654697.62478 3332395.29   409%     -   82s
H    0     0                    1119389.0310 3332395.29   198%     -   82s
H    0     0                    1119389.0467 3332395.29   198%     -   82s
H    0     0                    1119389.0483 3332395.29   198%     -   82s
     0     0 3162678.99    0   20 1119389.05 3162678.99   183%     -   82s
     0     0 3162678.99    0   20 1119389.05 3162678.99   183%     -   82s
H    0     0                    1708934.0865 3162678.99  85.1%     -   83s
     0     0 3162678.99    0   20 1708934.09 3162678.99  85.1%     -   83s
     0     0 3162278.14    0   28 1708934.09 3162278.14  85.0%     -   83s
     0     0 3162278.14    0   28 1708934.09 3162278.14  85.0%     -   83s
     0     0 3161371.96    0   22 1708934.09 3161371.96  85.0%     -   83s
     0     2 3161371.96    0   22 1708934.09 3161371.96  85.0%     -   83s
H   30    18                    2129950.9699 2703724.51  26.9%  97.8   84s
*   32    10               6    2129950.9857 2703724.51  26.9%  95.1   84s
   199    47 2271940.08   18   42 2129950.99 2524516.33  18.5%  60.6   85s

Cutting planes:
  MIR: 7

Explored 405 nodes (39120 simplex iterations) in 85.82 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: 2.12995e+06 2.12995e+06 1.70893e+06 ... -1.03007e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 2.129950924142e+06, best bound 2.129950985719e+06, gap 0.0000%

***
***  |- Calculation finished, took 85.82 (85.82) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 2,129,950.9241 (2129950.9241) and is feasible with a Gap of 0.0
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [48, 60, 0]
###  |  48 (DEHAM Out@734.0) - 60 (ESALG In@942.0/Out@961.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [49, 62, 0]
###  |  49 (DEHAM Out@902.0) - 62 (ESALG In@1110.0/Out@1129.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel 19 has the path [68, 54, 117, 118, 119, 57, 120, 121, 122, 59, 64, 0]
###  |  68 (ESBCN Out@540.0) - 54 (ESALG In@606.0/Out@625.0) - 117 (MAPTM In@653.0/Out@674.0) - 118 (MAPTM In@657.0/Out@676.0
###  |  ) - 119 (MAPTM In@684.0/Out@705.0) - 57 (ESALG In@774.0/Out@793.0) - 120 (MAPTM In@821.0/Out@842.0) - 121 (MAPTM In@8
###  |  25.0/Out@844.0) - 122 (MAPTM In@852.0/Out@873.0) - 59 (ESALG In@898.0/Out@910.0) - 64 (ESALG In@1278.0/Out@1297.0) - 
###  |  0 (DUMMY_END Ziel-Service 24)
###  |   |- and carried Demand  7,  580.0 containers from 117 to  57
###  |   |- and carried Demand 11,  214.0 containers from 120 to  64
###  |   |- and carried Demand 12,   10.0 containers from 120 to  64
###  |   |- and carried Demand 13,  484.0 containers from 120 to  59
###  |   |- and carried Demand 14,   18.0 containers from 120 to  59
###  |   |- and carried Demand 27,  580.0 containers from 119 to  57
###  |   |- and carried Demand 28,  214.0 containers from 122 to  64
###  |   |- and carried Demand 29,   10.0 containers from 122 to  64
###  |   |- and carried Demand 30,  484.0 containers from 122 to  59
###  |   |- and carried Demand 31,   18.0 containers from 122 to  59
###  |   |- and carried Demand 61,  214.0 containers from 121 to  64
###  |   |- and carried Demand 62,   10.0 containers from 121 to  64
###  |   |- and carried Demand 63,  484.0 containers from 121 to  59
###  |   |- and carried Demand 64,   18.0 containers from 121 to  59
###  |   |- and carried Demand 68,  580.0 containers from 118 to  57
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[1,60,0] = 1
y[1,48,60] = 1
y[2,49,62] = 1
y[2,62,0] = 1
y[19,59,64] = 1
y[19,68,54] = 1
y[19,122,59] = 1
y[19,64,0] = 1
y[19,120,121] = 1
y[19,57,120] = 1
y[19,118,119] = 1
y[19,54,117] = 1
y[19,119,57] = 1
y[19,117,118] = 1
y[19,121,122] = 1
xD[7,118,119] = 580
xD[7,119,57] = 580
xD[7,117,118] = 580
xD[11,59,64] = 214
xD[11,122,59] = 214
xD[11,120,121] = 214
xD[11,121,122] = 214
xD[12,59,64] = 10
xD[12,122,59] = 10
xD[12,120,121] = 10
xD[12,121,122] = 10
xD[13,122,59] = 484
xD[13,120,121] = 484
xD[13,121,122] = 484
xD[14,122,59] = 18
xD[14,120,121] = 18
xD[14,121,122] = 18
xD[27,119,57] = 580
xD[28,59,64] = 214
xD[28,122,59] = 214
xD[29,59,64] = 10
xD[29,122,59] = 10
xD[30,122,59] = 484
xD[31,122,59] = 18
xD[61,59,64] = 214
xD[61,122,59] = 214
xD[61,121,122] = 214
xD[62,59,64] = 10
xD[62,122,59] = 10
xD[62,121,122] = 10
xD[63,122,59] = 484
xD[63,121,122] = 484
xD[64,122,59] = 18
xD[64,121,122] = 18
xD[68,118,119] = 580
xD[68,119,57] = 580
zE[54] = 606
zE[57] = 774
zE[59] = 898
zE[60] = 942
zE[62] = 1110
zE[64] = 1278
zE[117] = 653
zE[118] = 657
zE[119] = 684
zE[120] = 821
zE[121] = 825
zE[122] = 852
zX[48] = 734
zX[49] = 902
zX[54] = 625
zX[57] = 793
zX[59] = 910
zX[60] = 961
zX[62] = 1129
zX[64] = 1297
zX[68] = 540
zX[117] = 674
zX[118] = 676
zX[119] = 705
zX[120] = 842
zX[121] = 844
zX[122] = 873
phi[1] = 227
phi[2] = 227
phi[19] = 757

### All variables with value smaller zero:

### End

real	1m32.797s
user	1m45.672s
sys	0m2.953s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
