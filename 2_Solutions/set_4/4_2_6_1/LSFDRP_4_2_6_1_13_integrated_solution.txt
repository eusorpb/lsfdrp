	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_2_6_1_fr.p
Instance LSFDP: LSFDRP_4_2_6_1_fd_13.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_2_6_1_fd_13 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 153
*** |A|: 1378
*** |A^i|: 1246
*** |A^f|: 132
*** |M|: 71
*** |E|: 0

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 24633 rows, 129614 columns and 540905 nonzeros
Variable types: 103417 continuous, 26197 integer (26197 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+01, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 1639 rows and 1794 columns (presolve time = 25s) ...
Presolve removed 1639 rows and 4075 columns (presolve time = 37s) ...
Presolve removed 4291 rows and 30792 columns (presolve time = 59s) ...
Presolve removed 4291 rows and 30792 columns (presolve time = 75s) ...
Presolve removed 4291 rows and 30792 columns (presolve time = 91s) ...
Presolve removed 7049 rows and 41843 columns
Presolve time: 92.75s
Presolved: 17584 rows, 87771 columns, 432859 nonzeros
Variable types: 73956 continuous, 13815 integer (13793 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -1.5234124e+09   1.389765e+05   0.000000e+00     94s
    5916   -4.8847796e+06   2.246192e+04   0.000000e+00     95s
    7425   -4.2425663e+06   0.000000e+00   0.000000e+00     96s

Root relaxation: objective -4.242566e+06, 7425 iterations, 1.17 seconds
Total elapsed time = 95.52s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 3623932.40    0  144          - 3623932.40      -     -   96s
H    0     0                    -4013608.483 3623932.40   190%     -   96s
H    0     0                    1604292.9199 3623932.40   126%     -   97s
     0     0 2860775.99    0  112 1604292.92 2860775.99  78.3%     -   98s
     0     0 2860775.99    0  116 1604292.92 2860775.99  78.3%     -   98s
     0     0 2860775.99    0  111 1604292.92 2860775.99  78.3%     -  100s
H    0     0                    1604292.9215 2860775.99  78.3%     -  100s
     0     0 2860775.99    0   88 1604292.92 2860775.99  78.3%     -  100s
     0     0 2743982.19    0  111 1604292.92 2743982.19  71.0%     -  100s
     0     0 2743612.54    0  112 1604292.92 2743612.54  71.0%     -  100s
H    0     0                    1666917.3852 2743612.54  64.6%     -  100s
     0     0 2743361.62    0  122 1666917.39 2743361.62  64.6%     -  100s
     0     0 2743361.04    0  122 1666917.39 2743361.04  64.6%     -  100s
     0     0 2679445.61    0  121 1666917.39 2679445.61  60.7%     -  100s
     0     0 2679445.61    0   97 1666917.39 2679445.61  60.7%     -  102s
     0     0 2679445.61    0  106 1666917.39 2679445.61  60.7%     -  102s
     0     0 2679203.23    0  118 1666917.39 2679203.23  60.7%     -  102s
     0     0 2668520.02    0  116 1666917.39 2668520.02  60.1%     -  102s
     0     0 2668520.02    0  116 1666917.39 2668520.02  60.1%     -  102s
     0     0 2663450.58    0  103 1666917.39 2663450.58  59.8%     -  102s
     0     0 2656852.61    0  107 1666917.39 2656852.61  59.4%     -  102s
     0     0 2655930.20    0  107 1666917.39 2655930.20  59.3%     -  102s
     0     0 2649867.63    0  121 1666917.39 2649867.63  59.0%     -  103s
     0     2 2649867.63    0  121 1666917.39 2649867.63  59.0%     -  103s
    28    16 2062380.23    8   22 1666917.39 2473447.53  48.4%   139  105s
H   31    16                    1666917.3856 2473447.53  48.4%   134  105s
H  300    23                    1764567.4334 1891904.72  7.22%  73.1  106s

Cutting planes:
  Implied bound: 1
  Clique: 5
  MIR: 6
  Flow cover: 15
  Zero half: 3
  Mod-K: 1

Explored 355 nodes (51386 simplex iterations) in 106.81 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: 1.76457e+06 1.66692e+06 1.66692e+06 ... -4.01361e+06
No other solutions better than 1.76457e+06

Optimal solution found (tolerance 1.00e-04)
Best objective 1.764567433447e+06, best bound 1.764567433447e+06, gap 0.0000%

***
***  |- Calculation finished, took 106.81 (106.81) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4334 (1764567.4334) and is feasible with a Gap of 0.0
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [48, 60, 0]
###  |  48 (DEHAM Out@734.0) - 60 (ESALG In@942.0/Out@961.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [49, 62, 0]
###  |  49 (DEHAM Out@902.0) - 62 (ESALG In@1110.0/Out@1129.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [52, 57, 120, 121, 122, 59, 64, 0]
###  |  52 (DKAAR Out@666.0) - 57 (ESALG In@774.0/Out@793.0) - 120 (MAPTM In@821.0/Out@842.0) - 121 (MAPTM In@825.0/Out@844.0
###  |  ) - 122 (MAPTM In@852.0/Out@873.0) - 59 (ESALG In@898.0/Out@910.0) - 64 (ESALG In@1278.0/Out@1297.0) - 0 (DUMMY_END Z
###  |  iel-Service 24)
###  |   |- and carried Demand 11,  214.0 containers from 120 to  64
###  |   |- and carried Demand 12,   10.0 containers from 120 to  64
###  |   |- and carried Demand 13,  484.0 containers from 120 to  59
###  |   |- and carried Demand 14,   18.0 containers from 120 to  59
###  |   |- and carried Demand 28,  214.0 containers from 122 to  64
###  |   |- and carried Demand 29,   10.0 containers from 122 to  64
###  |   |- and carried Demand 30,  484.0 containers from 122 to  59
###  |   |- and carried Demand 31,   18.0 containers from 122 to  59
###  |   |- and carried Demand 61,  214.0 containers from 121 to  64
###  |   |- and carried Demand 62,   10.0 containers from 121 to  64
###  |   |- and carried Demand 63,  484.0 containers from 121 to  59
###  |   |- and carried Demand 64,   18.0 containers from 121 to  59
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 23]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[1,48,60] = 1
y[1,60,0] = 1
y[2,49,62] = 1
y[2,62,0] = 1
y[13,64,0] = 1
y[13,120,121] = 1
y[13,59,64] = 1
y[13,52,57] = 1
y[13,122,59] = 1
y[13,57,120] = 1
y[13,121,122] = 1
xD[11,120,121] = 214
xD[11,59,64] = 214
xD[11,122,59] = 214
xD[11,121,122] = 214
xD[12,120,121] = 10
xD[12,59,64] = 10
xD[12,122,59] = 10
xD[12,121,122] = 10
xD[13,120,121] = 484
xD[13,122,59] = 484
xD[13,121,122] = 484
xD[14,120,121] = 18
xD[14,122,59] = 18
xD[14,121,122] = 18
xD[28,59,64] = 214
xD[28,122,59] = 214
xD[29,59,64] = 10
xD[29,122,59] = 10
xD[30,122,59] = 484
xD[31,122,59] = 18
xD[61,59,64] = 214
xD[61,122,59] = 214
xD[61,121,122] = 214
xD[62,59,64] = 10
xD[62,122,59] = 10
xD[62,121,122] = 10
xD[63,122,59] = 484
xD[63,121,122] = 484
xD[64,122,59] = 18
xD[64,121,122] = 18
zE[57] = 774
zE[59] = 898
zE[60] = 942
zE[62] = 1110
zE[64] = 1278
zE[120] = 821
zE[121] = 825
zE[122] = 852
zX[48] = 734
zX[49] = 902
zX[52] = 666
zX[57] = 793
zX[59] = 910
zX[60] = 961
zX[62] = 1129
zX[64] = 1297
zX[120] = 842
zX[121] = 844
zX[122] = 873
phi[1] = 227
phi[2] = 227
phi[13] = 631

### All variables with value smaller zero:

### End

real	1m54.996s
user	2m29.657s
sys	0m6.680s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
