	OpenBLAS 0.2.6 environment established
		compiled with gcc for max. 16 cores
	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_6_1_fr.p
Instance LSFDP: LSFDRP_4_1_6_1_fd_16.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_6_1_fd_16 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 339
*** |A|: 9468
*** |A^i|: 9051
*** |A^f|: 417
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 119158 rows, 2110520 columns and 7793596 nonzeros
Variable types: 1930616 continuous, 179904 integer (179904 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 9e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7100 rows and 10000 columns (presolve time = 5s) ...
Presolve removed 9373 rows and 11875 columns (presolve time = 10s) ...
Presolve removed 9375 rows and 139785 columns (presolve time = 670s) ...
Presolve removed 19350 rows and 139937 columns (presolve time = 670s) ...
Presolve removed 19384 rows and 278423 columns (presolve time = 675s) ...
Presolve removed 27324 rows and 283333 columns (presolve time = 680s) ...
Presolve removed 28205 rows and 302687 columns (presolve time = 688s) ...
Presolve removed 29117 rows and 303600 columns (presolve time = 1401s) ...
Presolve removed 29117 rows and 303600 columns
Presolve time: 1400.55s
Presolved: 90041 rows, 1806920 columns, 6490424 nonzeros
Variable types: 1753395 continuous, 53525 integer (53476 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.1147745e+03   2.000000e+00   1.365522e+07   1422s
   19456   -1.2252745e+06   0.000000e+00   4.191242e+08   1425s
Concurrent spin time: 0.21s

Solved with dual simplex

Root relaxation: objective -1.738256e+06, 17358 iterations, 8.59 seconds
Total elapsed time = 1429.23s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1738256.25    0   71          - 1738256.25      -     - 1430s
H    0     0                    -3325646.015 1738256.25   152%     - 1432s
H    0     0                    744385.27518 1738256.25   134%     - 1432s
H    0     0                    796419.68522 1738256.25   118%     - 1432s
     0     0 1668459.51    0   53 796419.685 1668459.51   109%     - 1433s
     0     0 1223322.10    0   87 796419.685 1223322.10  53.6%     - 1439s
H    0     0                    796419.68577 1223322.10  53.6%     - 1439s
     0     0 1087429.29    0   98 796419.686 1087429.29  36.5%     - 1439s
     0     0 1028561.22    0   54 796419.686 1028561.22  29.1%     - 1440s
H    0     0                    1005931.8363 1028561.22  2.25%     - 1440s
*    0     0               0    1008139.8478 1008139.85  0.00%     - 1440s

Cutting planes:
  Implied bound: 2
  MIR: 2
  Flow cover: 3

Explored 1 nodes (25236 simplex iterations) in 1440.63 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: 1.00814e+06 1.00593e+06 796420 ... -3.32565e+06
No other solutions better than 1.00814e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (8.6841e-06) exceeds tolerance
Warning: max bound violation (5.2713e-06) exceeds tolerance
Best objective 1.008139847831e+06, best bound 1.008139847831e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,440.66 (1440.66) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,008,139.8478 (1008139.8478) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel 15 has the path [151, 222, 128, 35, 295, 260, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 35 (CLLQN In@1072.0/Out@109
###  |  0.0) - 295 (PECLL In@1230.71/Out@1232.71) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 180,  540.0 containers from  35 to 260
###  |   |- and carried Demand 181,   69.0 containers from  35 to 260
###  |   |- and carried Demand 182,  138.0 containers from  35 to 260
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel 23 has the path [338, 86, 58, 181, 89, 55, 67, 232, 261, 0]
###  |  338 (JPYOK Out@785.42) - 86 (CNXMN In@910.0/Out@932.0) - 58 (CNNSA In@959.0/Out@969.0) - 181 (HKHKG In@1006.0/Out@102
###  |  2.0) - 89 (CNYTN In@1030.0/Out@1048.0) - 55 (CNNGB In@1102.0/Out@1111.0) - 67 (CNSHA In@1137.0/Out@1155.0) - 232 (MXL
###  |  ZC In@1545.0/Out@1568.0) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  78,   59.0 containers from  86 to 232
###  |   |- and carried Demand  79,  314.0 containers from  86 to 181
###  |   |- and carried Demand  80,    1.0 containers from  86 to 181
###  |   |- and carried Demand  81,  373.0 containers from  86 to 261
###  |   |- and carried Demand  82,  169.0 containers from 181 to  55
###  |   |- and carried Demand  83,    7.0 containers from 181 to  67
###  |   |- and carried Demand  85,    9.0 containers from 181 to 261
###  |   |- and carried Demand  86,   20.0 containers from  89 to 232
###  |   |- and carried Demand  89,  969.0 containers from  89 to 261
###  |   |- and carried Demand  90,   39.0 containers from  89 to 261
###  |   |- and carried Demand  91,  370.0 containers from  55 to 232
###  |   |- and carried Demand  92,  975.0 containers from  55 to 261
###  |   |- and carried Demand  93,    1.0 containers from  55 to 261
###  |   |- and carried Demand  96,  356.0 containers from 232 to 261
###  |   |- and carried Demand  97,  100.0 containers from 232 to 261
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[15,35,295] = 1
y[15,222,128] = 1
y[15,295,260] = 1
y[15,260,0] = 1
y[15,151,222] = 1
y[15,128,35] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
y[23,89,55] = 1
y[23,232,261] = 1
y[23,67,232] = 1
y[23,261,0] = 1
y[23,86,58] = 1
y[23,55,67] = 1
y[23,58,181] = 1
y[23,338,86] = 1
y[23,181,89] = 1
xD[50,222,128] = 580
xD[78,89,55] = 59
xD[78,67,232] = 59
xD[78,86,58] = 59
xD[78,55,67] = 59
xD[78,58,181] = 59
xD[78,181,89] = 59
xD[79,86,58] = 314
xD[79,58,181] = 314
xD[80,86,58] = 1
xD[80,58,181] = 1
xD[81,89,55] = 373
xD[81,232,261] = 373
xD[81,67,232] = 373
xD[81,86,58] = 373
xD[81,55,67] = 373
xD[81,58,181] = 373
xD[81,181,89] = 373
xD[82,89,55] = 169
xD[82,181,89] = 169
xD[83,89,55] = 7
xD[83,55,67] = 7
xD[83,181,89] = 7
xD[85,89,55] = 9
xD[85,232,261] = 9
xD[85,67,232] = 9
xD[85,55,67] = 9
xD[85,181,89] = 9
xD[86,89,55] = 20
xD[86,67,232] = 20
xD[86,55,67] = 20
xD[89,89,55] = 969
xD[89,232,261] = 969
xD[89,67,232] = 969
xD[89,55,67] = 969
xD[90,89,55] = 39
xD[90,232,261] = 39
xD[90,67,232] = 39
xD[90,55,67] = 39
xD[91,67,232] = 370
xD[91,55,67] = 370
xD[92,232,261] = 975
xD[92,67,232] = 975
xD[92,55,67] = 975
xD[93,232,261] = 1
xD[93,67,232] = 1
xD[93,55,67] = 1
xD[96,232,261] = 356
xD[97,232,261] = 100
xD[180,35,295] = 540
xD[180,295,260] = 540
xD[181,35,295] = 69
xD[181,295,260] = 69
xD[182,35,295] = 138
xD[182,295,260] = 138
zE[35] = 1072
zE[55] = 1102
zE[58] = 959
zE[67] = 1137
zE[86] = 910
zE[89] = 1030
zE[128] = 284
zE[181] = 1006
zE[222] = 238
zE[224] = 406
zE[232] = 1545
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1230.71
zX[35] = 1090
zX[55] = 1111
zX[58] = 969
zX[67] = 1155
zX[86] = 932
zX[89] = 1048
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[181] = 1022
zX[222] = 259
zX[224] = 427
zX[232] = 1568
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1232.71
zX[338] = 785.421
w[15,(295,260)] = 125.884
w[15,(35,295)] = 140.711
w[23,(338,86)] = 124.579
phi[15] = 1326
phi[16] = 990
phi[23] = 866.579

### All variables with value smaller zero:

### End

real	26m20.083s
user	94m9.081s
sys	2m21.898s
	OpenBLAS 0.2.6 environment removed
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
