	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_2_4_1_fr.p
Instance LSFDP: LSFDRP_4_2_4_1_fd_40.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_2_4_1_fd_40 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 152
*** |A|: 1246
*** |A^i|: 1246
*** |A^f|: 0
*** |M|: 71
*** |E|: 0

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 16732 rows, 114960 columns and 476330 nonzeros
Variable types: 91271 continuous, 23689 integer (23689 binary)
Coefficient statistics:
  Matrix range     [1e+00, 4e+03]
  Objective range  [2e+01, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 4e+00]
Presolve removed 3713 rows and 11737 columns (presolve time = 24s) ...
Presolve removed 3713 rows and 11737 columns (presolve time = 31s) ...
Presolve removed 4287 rows and 26446 columns (presolve time = 65s) ...
Presolve removed 4832 rows and 31617 columns (presolve time = 70s) ...
Presolve removed 4832 rows and 31617 columns
Presolve time: 70.39s
Presolved: 11900 rows, 83343 columns, 290850 nonzeros
Variable types: 74665 continuous, 8678 integer (8656 binary)
Warning: Failed to open log file 'gurobi.log'

Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -1.4061922e+09   1.284679e+05   0.000000e+00     71s
    4896   -3.9061784e+06   0.000000e+00   0.000000e+00     72s

Root relaxation: objective -3.906178e+06, 4896 iterations, 0.33 seconds
Total elapsed time = 71.62s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1917170.19    0   10          - 1917170.19      -     -   71s
H    0     0                    701430.78999 1917170.19   173%     -   71s
H    0     0                    866934.44640 1917170.19   121%     -   71s
H    0     0                    866934.46351 1917170.19   121%     -   71s
     0     0 1811190.68    0   11 866934.464 1811190.68   109%     -   72s
     0     0 1811190.68    0    6 866934.464 1811190.68   109%     -   72s
     0     0 1811190.68    0    9 866934.464 1811190.68   109%     -   72s
     0     0 1811190.68    0    9 866934.464 1811190.68   109%     -   72s
     0     0 1811190.68    0    9 866934.464 1811190.68   109%     -   72s
H    0     0                    866934.49142 1811190.68   109%     -   72s
     0     0 1792925.76    0   11 866934.491 1792925.76   107%     -   72s
H    0     0                    1432399.4486 1792925.76  25.2%     -   72s
H    0     0                    1764567.4914 1792925.76  1.61%     -   72s
     0     0     cutoff    0      1764567.49 1764567.49  0.00%     -   72s

Cutting planes:
  Implied bound: 1
  MIR: 5
  Flow cover: 5

Explored 1 nodes (6673 simplex iterations) in 72.22 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: 1.76457e+06 1.4324e+06 866934 ... 701431

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (8.8737e-06) exceeds tolerance
Warning: max bound violation (3.7587e-06) exceeds tolerance
Best objective 1.764567491400e+06, best bound 1.764567491400e+06, gap 0.0000%

***
***  |- Calculation finished, took 72.22 (72.22) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 1,764,567.4914 (1764567.4914) and is feasible with a Gap of 0.0
###
### Service 24 (WAF7) is operated by 3 vessels from type 18 (PMax25)
###  |- Vessel  1 has the path [48, 60, 0]
###  |  48 (DEHAM Out@734.0) - 60 (ESALG In@942.0/Out@961.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel  2 has the path [49, 62, 0]
###  |  49 (DEHAM Out@902.0) - 62 (ESALG In@1110.0/Out@1129.0) - 0 (DUMMY_END Ziel-Service 24)
###  |- Vessel 13 has the path [52, 57, 120, 121, 122, 59, 64, 0]
###  |  52 (DKAAR Out@666.0) - 57 (ESALG In@774.0/Out@793.0) - 120 (MAPTM In@821.0/Out@842.0) - 121 (MAPTM In@825.0/Out@844.0
###  |  ) - 122 (MAPTM In@852.0/Out@873.0) - 59 (ESALG In@898.0/Out@910.0) - 64 (ESALG In@1278.0/Out@1297.0) - 0 (DUMMY_END Z
###  |  iel-Service 24)
###  |   |- and carried Demand 11,  214.0 containers from 120 to  64
###  |   |- and carried Demand 12,   10.0 containers from 120 to  64
###  |   |- and carried Demand 13,  484.0 containers from 120 to  59
###  |   |- and carried Demand 14,   18.0 containers from 120 to  59
###  |   |- and carried Demand 28,  214.0 containers from 122 to  64
###  |   |- and carried Demand 29,   10.0 containers from 122 to  64
###  |   |- and carried Demand 30,  484.0 containers from 122 to  59
###  |   |- and carried Demand 31,   18.0 containers from 122 to  59
###  |   |- and carried Demand 61,  214.0 containers from 121 to  64
###  |   |- and carried Demand 62,   10.0 containers from 121 to  64
###  |   |- and carried Demand 63,  484.0 containers from 121 to  59
###  |   |- and carried Demand 64,   18.0 containers from 121 to  59
###  |
### Vessels not used in the solution: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 21]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[24,18] = 1
eta[24,18,1] = 1
eta[24,18,2] = 1
eta[24,18,3] = 1
y[1,60,0] = 1
y[1,48,60] = 1
y[2,49,62] = 1
y[2,62,0] = 1
y[13,59,64] = 1
y[13,52,57] = 1
y[13,122,59] = 1
y[13,64,0] = 1
y[13,120,121] = 1
y[13,57,120] = 1
y[13,121,122] = 1
xD[11,59,64] = 214
xD[11,122,59] = 214
xD[11,120,121] = 214
xD[11,121,122] = 214
xD[12,59,64] = 10
xD[12,122,59] = 10
xD[12,120,121] = 10
xD[12,121,122] = 10
xD[13,122,59] = 484
xD[13,120,121] = 484
xD[13,121,122] = 484
xD[14,122,59] = 18
xD[14,120,121] = 18
xD[14,121,122] = 18
xD[28,59,64] = 214
xD[28,122,59] = 214
xD[29,59,64] = 10
xD[29,122,59] = 10
xD[30,122,59] = 484
xD[31,122,59] = 18
xD[61,59,64] = 214
xD[61,122,59] = 214
xD[61,121,122] = 214
xD[62,59,64] = 10
xD[62,122,59] = 10
xD[62,121,122] = 10
xD[63,122,59] = 484
xD[63,121,122] = 484
xD[64,122,59] = 18
xD[64,121,122] = 18
zE[57] = 774
zE[59] = 898
zE[60] = 942
zE[62] = 1110
zE[64] = 1278
zE[120] = 821
zE[121] = 825
zE[122] = 852
zX[48] = 734
zX[49] = 902
zX[52] = 666
zX[57] = 793
zX[59] = 910
zX[60] = 961
zX[62] = 1129
zX[64] = 1297
zX[120] = 842
zX[121] = 844
zX[122] = 873
phi[1] = 227
phi[2] = 227
phi[13] = 631

### All variables with value smaller zero:

### End

real	1m18.945s
user	1m22.532s
sys	0m1.272s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
