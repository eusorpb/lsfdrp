	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_3_1_fr.p
Instance LSFDP: LSFDRP_4_1_3_1_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_3_1_fd_43 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520537 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7651 rows and 20000 columns (presolve time = 5s) ...
Presolve removed 11644 rows and 28801 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11646 rows and 28825 columns (presolve time = 16s) ...
Presolve removed 11646 rows and 142364 columns (presolve time = 634s) ...
Presolve removed 17593 rows and 142460 columns (presolve time = 636s) ...
Presolve removed 23463 rows and 162245 columns (presolve time = 640s) ...
Presolve removed 23671 rows and 176420 columns (presolve time = 645s) ...
Presolve removed 24396 rows and 194410 columns (presolve time = 653s) ...
Presolve removed 24396 rows and 194411 columns (presolve time = 1264s) ...
Presolve removed 24396 rows and 194411 columns
Presolve time: 1264.25s
Presolved: 77114 rows, 1845012 columns, 6674502 nonzeros
Variable types: 1799442 continuous, 45570 integer (45522 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.7456015e+02   8.000000e+00   4.611168e+08   1286s
Concurrent spin time: 0.09s

Solved with dual simplex

Root relaxation: objective 7.961612e+05, 6195 iterations, 1.85 seconds
Total elapsed time = 1287.05s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.16    0   59          - -796161.16      -     - 1287s
H    0     0                    -2783079.581 -796161.16  71.4%     - 1287s
H    0     0                    -2223054.312 -796161.16  64.2%     - 1289s
     0     0 -1183625.0    0   57 -2223054.3 -1183625.0  46.8%     - 1290s
H    0     0                    -2011697.283 -1183625.0  41.2%     - 1291s
     0     0 -1242643.6    0   62 -2011697.3 -1242643.6  38.2%     - 1292s
H    0     0                    -2011697.281 -1242643.6  38.2%     - 1299s
     0     0 -1378279.7    0   66 -2011697.3 -1378279.7  31.5%     - 1300s
     0     0 -1636650.4    0   54 -2011697.3 -1636650.4  18.6%     - 1300s
     0     0 -1681507.3    0   42 -2011697.3 -1681507.3  16.4%     - 1301s
     0     0 -1681507.3    0   44 -2011697.3 -1681507.3  16.4%     - 1301s
     0     0 -1681697.6    0   60 -2011697.3 -1681697.6  16.4%     - 1301s
     0     0 -1681697.6    0   51 -2011697.3 -1681697.6  16.4%     - 1304s
H    0     0                    -1760743.301 -1681697.6  4.49%     - 1304s
     0     0 -1691299.1    0   47 -1760743.3 -1691299.1  3.94%     - 1305s
H    0     0                    -1760743.300 -1691299.1  3.94%     - 1305s
     0     0 -1750337.0    0    6 -1760743.3 -1750337.0  0.59%     - 1305s
*    0     0               0    -1750336.976 -1750337.0  0.00%     - 1305s

Cutting planes:
  Implied bound: 2
  Clique: 3
  MIR: 1

Explored 1 nodes (17353 simplex iterations) in 1305.53 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -1.75034e+06 -1.76074e+06 -1.76074e+06 ... -2.78308e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (8.4518e-06) exceeds tolerance
Warning: max bound violation (3.4390e-06) exceeds tolerance
Best objective -1.750336975642e+06, best bound -1.750336975642e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,305.56 (1305.56) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9756 (-1750336.9756) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 20]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	24m9.336s
user	85m44.144s
sys	1m43.883s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
