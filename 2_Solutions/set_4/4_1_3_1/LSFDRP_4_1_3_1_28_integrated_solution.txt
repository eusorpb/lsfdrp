	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_3_1_fr.p
Instance LSFDP: LSFDRP_4_1_3_1_fd_28.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_3_1_fd_28 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520537 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7152 rows and 19501 columns (presolve time = 5s) ...
Presolve removed 11644 rows and 28801 columns (presolve time = 11s) ...
Presolve removed 11646 rows and 28825 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11646 rows and 142364 columns (presolve time = 657s) ...
Presolve removed 17593 rows and 142460 columns (presolve time = 660s) ...
Presolve removed 23469 rows and 162246 columns (presolve time = 665s) ...
Presolve removed 23805 rows and 176553 columns (presolve time = 671s) ...
Presolve removed 24396 rows and 194410 columns (presolve time = 676s) ...
Presolve removed 24396 rows and 194411 columns (presolve time = 1292s) ...
Presolve removed 24396 rows and 194411 columns
Presolve time: 1292.20s
Presolved: 77114 rows, 1845012 columns, 6674502 nonzeros
Variable types: 1799442 continuous, 45570 integer (45522 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    4.9711176e+02   8.000000e+00   4.609204e+08   1314s
Concurrent spin time: 0.02s

Solved with dual simplex

Root relaxation: objective 7.812895e+05, 5892 iterations, 1.80 seconds
Total elapsed time = 1314.71s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.16    0   65          - -796161.16      -     - 1315s
H    0     0                    -2617337.853 -796161.16  69.6%     - 1316s
H    0     0                    -1894536.394 -796161.16  58.0%     - 1316s
     0     0 -1087954.0    0  108 -1894536.4 -1087954.0  42.6%     - 1319s
     0     0 -1105013.1    0   57 -1894536.4 -1105013.1  41.7%     - 1328s
H    0     0                    -1855993.920 -1105013.1  40.5%     - 1328s
     0     0 -1468270.5    0   90 -1855993.9 -1468270.5  20.9%     - 1330s
     0     0 -1514579.0    0   70 -1855993.9 -1514579.0  18.4%     - 1330s
     0     0 -1514579.0    0   70 -1855993.9 -1514579.0  18.4%     - 1330s
     0     0 -1525797.8    0   67 -1855993.9 -1525797.8  17.8%     - 1331s
     0     0 -1525797.8    0   58 -1855993.9 -1525797.8  17.8%     - 1333s
H    0     0                    -1750336.975 -1525797.8  12.8%     - 1334s
     0     0 -1685358.7    0   45 -1750337.0 -1685358.7  3.71%     - 1334s
*    0     0               0    -1750336.962 -1750337.0  0.00%     - 1335s

Explored 1 nodes (19806 simplex iterations) in 1335.13 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -1.75034e+06 -1.75034e+06 -1.85599e+06 ... -2.61734e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (3.7600e-06) exceeds tolerance
Warning: max bound violation (3.7600e-06) exceeds tolerance
Best objective -1.750336962122e+06, best bound -1.750336962122e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,335.17 (1335.17) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9621 (-1750336.9621) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 20]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	24m42.985s
user	87m43.838s
sys	1m40.241s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
