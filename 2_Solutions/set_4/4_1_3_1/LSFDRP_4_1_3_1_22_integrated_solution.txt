	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_3_1_fr.p
Instance LSFDP: LSFDRP_4_1_3_1_fd_22.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_3_1_fd_22 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520537 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7152 rows and 19501 columns (presolve time = 5s) ...
Presolve removed 11644 rows and 28801 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11646 rows and 28825 columns (presolve time = 16s) ...
Presolve removed 11646 rows and 142364 columns (presolve time = 634s) ...
Presolve removed 17593 rows and 142460 columns (presolve time = 636s) ...
Presolve removed 23469 rows and 162246 columns (presolve time = 641s) ...
Presolve removed 23805 rows and 176589 columns (presolve time = 645s) ...
Presolve removed 24396 rows and 194411 columns (presolve time = 1259s) ...
Presolve removed 24396 rows and 194411 columns
Presolve time: 1258.57s
Presolved: 77114 rows, 1845012 columns, 6674502 nonzeros
Variable types: 1799442 continuous, 45570 integer (45522 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    6.7110838e+02   8.000000e+00   4.611578e+08   1280s
    8738    9.8572224e+05   0.000000e+00   6.986142e+08   1280s
Concurrent spin time: 0.11s

Solved with dual simplex

Root relaxation: objective 7.471659e+05, 5852 iterations, 1.69 seconds
Total elapsed time = 1280.62s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -789289.26    0   77          - -789289.26      -     - 1281s
H    0     0                    -3629881.941 -789289.26  78.3%     - 1281s
H    0     0                    -3511540.184 -789289.26  77.5%     - 1281s
H    0     0                    -1894536.394 -789289.26  58.3%     - 1282s
     0     0 -1181458.4    0   52 -1894536.4 -1181458.4  37.6%     - 1283s
     0     0 -1277965.4    0   64 -1894536.4 -1277965.4  32.5%     - 1291s
H    0     0                    -1894536.386 -1277965.4  32.5%     - 1291s
H    0     0                    -1855993.917 -1279216.2  31.1%     - 1291s
     0     0 -1460752.3    0   87 -1855993.9 -1460752.3  21.3%     - 1292s
     0     0 -1498457.1    0   65 -1855993.9 -1498457.1  19.3%     - 1292s
     0     0 -1525375.6    0   66 -1855993.9 -1525375.6  17.8%     - 1293s
     0     0 -1525375.6    0   60 -1855993.9 -1525375.6  17.8%     - 1295s
     0     0 -1643868.3    0   60 -1855993.9 -1643868.3  11.4%     - 1296s
H    0     0                    -1778711.419 -1643868.3  7.58%     - 1296s
H    0     0                    -1778711.418 -1643868.3  7.58%     - 1296s
     0     0 -1704615.8    0   51 -1778711.4 -1704615.8  4.17%     - 1296s
     0     0 -1704615.8    0   51 -1778711.4 -1704615.8  4.17%     - 1296s
     0     0 -1704615.8    0   53 -1778711.4 -1704615.8  4.17%     - 1296s
*    0     0               0    -1750336.955 -1750337.0  0.00%     - 1296s

Explored 1 nodes (17172 simplex iterations) in 1297.08 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: -1.75034e+06 -1.77871e+06 -1.77871e+06 ... -3.62988e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (3.3033e-06) exceeds tolerance
Warning: max bound violation (1.0946e-06) exceeds tolerance
Best objective -1.750336954847e+06, best bound -1.750336954847e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,297.11 (1297.11) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9548 (-1750336.9548) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 20]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	24m12.132s
user	85m36.699s
sys	1m44.420s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
