	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_3_1_fr.p
Instance LSFDP: LSFDRP_4_1_3_1_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_3_1_fd_49 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520537 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7152 rows and 19501 columns (presolve time = 5s) ...
Presolve removed 11644 rows and 28801 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11646 rows and 28825 columns (presolve time = 17s) ...
Presolve removed 11646 rows and 142364 columns (presolve time = 662s) ...
Presolve removed 17593 rows and 142460 columns (presolve time = 665s) ...
Presolve removed 23464 rows and 162246 columns (presolve time = 670s) ...
Presolve removed 23671 rows and 176358 columns (presolve time = 675s) ...
Presolve removed 24396 rows and 194410 columns (presolve time = 683s) ...
Presolve removed 24396 rows and 194411 columns (presolve time = 1319s) ...
Presolve removed 24396 rows and 194411 columns
Presolve time: 1319.12s
Presolved: 77114 rows, 1845012 columns, 6674502 nonzeros
Variable types: 1799442 continuous, 45570 integer (45522 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.2368851e+02   8.000000e+00   4.605474e+08   1343s
Concurrent spin time: 0.05s

Solved with dual simplex

Root relaxation: objective 7.961612e+05, 5687 iterations, 1.72 seconds
Total elapsed time = 1343.93s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.16    0   63          - -796161.16      -     - 1344s
H    0     0                    -2617337.853 -796161.16  69.6%     - 1344s
H    0     0                    -1894536.394 -796161.16  58.0%     - 1344s
     0     0 -1183625.0    0   50 -1894536.4 -1183625.0  37.5%     - 1346s
     0     0 -1277965.4    0   64 -1894536.4 -1277965.4  32.5%     - 1354s
H    0     0                    -1855993.920 -1277965.4  31.1%     - 1354s
     0     0 -1460631.3    0   83 -1855993.9 -1460631.3  21.3%     - 1354s
H    0     0                    -1855993.913 -1460631.3  21.3%     - 1355s
     0     0 -1497511.8    0   60 -1855993.9 -1497511.8  19.3%     - 1355s
H    0     0                    -1846712.920 -1497511.8  18.9%     - 1355s
     0     0 -1497511.8    0   63 -1846712.9 -1497511.8  18.9%     - 1355s
     0     0 -1516597.0    0   77 -1846712.9 -1516597.0  17.9%     - 1355s
     0     0 -1516597.0    0   60 -1846712.9 -1516597.0  17.9%     - 1356s
     0     0 -1673546.3    0   60 -1846712.9 -1673546.3  9.38%     - 1356s
H    0     0                    -1770024.298 -1673546.3  5.45%     - 1357s
     0     0 -1673546.3    0   56 -1770024.3 -1673546.3  5.45%     - 1357s
     0     0 -1750337.0    0    7 -1770024.3 -1750337.0  1.11%     - 1357s
H    0     0                    -1750336.983 -1750337.0  0.00%     - 1357s

Explored 1 nodes (14668 simplex iterations) in 1357.58 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: -1.75034e+06 -1.77002e+06 -1.77002e+06 ... -2.61734e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max bound violation (1.4549e-06) exceeds tolerance
Best objective -1.750336982812e+06, best bound -1.750336958451e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,357.62 (1357.62) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9828 (-1750336.9828) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 20]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	25m12.708s
user	89m3.243s
sys	1m59.707s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
