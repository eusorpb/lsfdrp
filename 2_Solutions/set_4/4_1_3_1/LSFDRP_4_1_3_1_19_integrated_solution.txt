	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_3_1_fr.p
Instance LSFDP: LSFDRP_4_1_3_1_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_3_1_fd_19 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520537 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10384 rows and 28718 columns (presolve time = 5s) ...
Presolve removed 11644 rows and 28825 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11646 rows and 142364 columns (presolve time = 651s) ...
Presolve removed 17596 rows and 142463 columns (presolve time = 655s) ...
Presolve removed 23671 rows and 162448 columns (presolve time = 661s) ...
Presolve removed 24396 rows and 194410 columns (presolve time = 670s) ...
Presolve removed 24396 rows and 194411 columns (presolve time = 1293s) ...
Presolve removed 24396 rows and 194411 columns
Presolve time: 1293.42s
Presolved: 77114 rows, 1845012 columns, 6674502 nonzeros
Variable types: 1799442 continuous, 45570 integer (45522 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    7.9931642e+02   8.000000e+00   4.613327e+08   1314s
   10933    8.8968617e+05   0.000000e+00   8.849415e+09   1315s
Concurrent spin time: 0.06s

Solved with dual simplex

Root relaxation: objective 7.297322e+05, 5745 iterations, 1.84 seconds
Total elapsed time = 1315.41s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -785123.10    0   73          - -785123.10      -     - 1316s
H    0     0                    -3587227.470 -785123.10  78.1%     - 1316s
H    0     0                    -3511540.184 -785123.10  77.6%     - 1316s
H    0     0                    -1894536.394 -785123.10  58.6%     - 1317s
H    0     0                    -1894536.394 -785123.10  58.6%     - 1318s
     0     0 -1183625.0    0   56 -1894536.4 -1183625.0  37.5%     - 1319s
     0     0 -1183625.0    0   75 -1894536.4 -1183625.0  37.5%     - 1321s
     0     0 -1243793.4    0   70 -1894536.4 -1243793.4  34.3%     - 1323s
     0     0 -1249645.4    0   75 -1894536.4 -1249645.4  34.0%     - 1324s
     0     0 -1259437.8    0   83 -1894536.4 -1259437.8  33.5%     - 1324s
     0     0 -1259437.8    0   83 -1894536.4 -1259437.8  33.5%     - 1324s
     0     0 -1263726.2    0   87 -1894536.4 -1263726.2  33.3%     - 1326s
     0     2 -1263726.2    0   87 -1894536.4 -1263726.2  33.3%     - 1327s
     6     2 -1330751.2    3  128 -1894536.4 -1330751.2  29.8%   461 1332s
    28    18 -1432285.1    9   46 -1894536.4 -1343523.6  29.1%   384 1335s
   158    64 -1819094.2   41   46 -1894536.4 -1400402.8  26.1%   214 1340s
H  217    83                    -1760743.295 -1410390.5  19.9%   199 1340s
*  284    90              14    -1759617.973 -1494343.1  15.1%   200 1343s
   374   102 -1526523.5   12   27 -1759618.0 -1501866.1  14.6%   207 1345s
   737    94 -1729445.2   24   48 -1759618.0 -1601754.3  8.97%   195 1350s
H  857    64                    -1750336.980 -1653068.3  5.56%   198 1354s
   862    41 -1744699.8    9   51 -1750337.0 -1655383.8  5.42%   197 1355s

Cutting planes:
  Implied bound: 8
  Clique: 9
  MIR: 18
  Flow cover: 5
  Zero half: 1

Explored 993 nodes (207190 simplex iterations) in 1356.52 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -1.75034e+06 -1.75962e+06 -1.76074e+06 ... -3.58723e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336979658e+06, best bound -1.750336979656e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,356.55 (1356.55) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9797 (-1750336.9797) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 20]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	24m58.978s
user	89m4.179s
sys	1m56.271s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
