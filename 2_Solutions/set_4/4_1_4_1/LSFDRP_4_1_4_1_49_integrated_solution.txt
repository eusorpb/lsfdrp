	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_4_1_fr.p
Instance LSFDP: LSFDRP_4_1_4_1_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_4_1_fd_49 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520555 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10340 rows and 22353 columns (presolve time = 5s) ...
Presolve removed 11222 rows and 24892 columns (presolve time = 10s) ...
Presolve removed 11388 rows and 28015 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11388 rows and 145088 columns (presolve time = 635s) ...
Presolve removed 17324 rows and 145154 columns (presolve time = 640s) ...
Presolve removed 23584 rows and 176743 columns (presolve time = 645s) ...
Presolve removed 24309 rows and 194755 columns (presolve time = 653s) ...
Presolve removed 24309 rows and 194756 columns (presolve time = 1360s) ...
Presolve removed 24309 rows and 194756 columns
Presolve time: 1359.88s
Presolved: 77201 rows, 1844667 columns, 6671330 nonzeros
Variable types: 1798679 continuous, 45988 integer (45940 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.2368990e+02   8.000000e+00   4.696113e+08   1383s
   17763   -6.2668605e+05   0.000000e+00   1.756576e+08   1385s
Concurrent spin time: 0.00s

Solved with dual simplex

Root relaxation: objective -1.962873e+05, 9096 iterations, 3.19 seconds
Total elapsed time = 1385.47s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 196287.258    0   56          - 196287.258      -     - 1385s
H    0     0                    -1151939.863 196287.258   117%     - 1386s
H    0     0                    -640447.4890 196287.258   131%     - 1386s
     0     0 10868.0527    0   65 -640447.49 10868.0527   102%     - 1390s
     0     0 -205485.03    0   45 -640447.49 -205485.03  67.9%     - 1394s
H    0     0                    -612040.8389 -205485.03  66.4%     - 1395s
H    0     0                    -584471.9747 -259138.26  55.7%     - 1395s
H    0     0                    -584471.9716 -259138.26  55.7%     - 1395s
     0     0 -459633.49    0   45 -584471.97 -459633.49  21.4%     - 1395s
H    0     0                    -543732.3689 -459633.49  15.5%     - 1395s
H    0     0                    -498502.3521 -459633.49  7.80%     - 1396s
     0     0 -484160.84    0   18 -498502.35 -484160.84  2.88%     - 1396s
     0     0 -484160.84    0   18 -498502.35 -484160.84  2.88%     - 1396s
     0     0 -484160.84    0   18 -498502.35 -484160.84  2.88%     - 1396s
     0     0     cutoff    0      -498502.35 -498502.35  0.00%     - 1396s

Explored 1 nodes (14924 simplex iterations) in 1396.54 seconds
Thread count was 4 (of 16 available processors)

Solution count 7: -498502 -543732 -584472 ... -1.15194e+06
No other solutions better than -498502

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (6.8115e-06) exceeds tolerance
Warning: max bound violation (1.1771e-06) exceeds tolerance
Best objective -4.985023513344e+05, best bound -4.985023513344e+05, gap 0.0000%

***
***  |- Calculation finished, took 1,396.58 (1396.58) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -498,502.3513 (-498502.3513) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel 15 has the path [151, 222, 128, 35, 295, 260, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 35 (CLLQN In@1072.0/Out@109
###  |  0.0) - 295 (PECLL In@1230.71/Out@1232.71) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 180,  540.0 containers from  35 to 260
###  |   |- and carried Demand 181,   69.0 containers from  35 to 260
###  |   |- and carried Demand 182,  138.0 containers from  35 to 260
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel 21 has the path [195, 86, 58, 181, 89, 55, 67, 232, 261, 0]
###  |  195 (JPYOK Out@843.0) - 86 (CNXMN In@910.0/Out@932.0) - 58 (CNNSA In@959.0/Out@969.0) - 181 (HKHKG In@1006.0/Out@1022
###  |  .0) - 89 (CNYTN In@1030.0/Out@1048.0) - 55 (CNNGB In@1102.0/Out@1111.0) - 67 (CNSHA In@1137.0/Out@1155.0) - 232 (MXLZ
###  |  C In@1545.0/Out@1568.0) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  78,   59.0 containers from  86 to 232
###  |   |- and carried Demand  79,  314.0 containers from  86 to 181
###  |   |- and carried Demand  80,    1.0 containers from  86 to 181
###  |   |- and carried Demand  81,  373.0 containers from  86 to 261
###  |   |- and carried Demand  82,  169.0 containers from 181 to  55
###  |   |- and carried Demand  83,    7.0 containers from 181 to  67
###  |   |- and carried Demand  85,    9.0 containers from 181 to 261
###  |   |- and carried Demand  86,   20.0 containers from  89 to 232
###  |   |- and carried Demand  89,  969.0 containers from  89 to 261
###  |   |- and carried Demand  90,   39.0 containers from  89 to 261
###  |   |- and carried Demand  91,  370.0 containers from  55 to 232
###  |   |- and carried Demand  92,  975.0 containers from  55 to 261
###  |   |- and carried Demand  93,    1.0 containers from  55 to 261
###  |   |- and carried Demand  96,  356.0 containers from 232 to 261
###  |   |- and carried Demand  97,  100.0 containers from 232 to 261
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[15,35,295] = 1
y[15,222,128] = 1
y[15,295,260] = 1
y[15,260,0] = 1
y[15,151,222] = 1
y[15,128,35] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
y[21,89,55] = 1
y[21,232,261] = 1
y[21,195,86] = 1
y[21,67,232] = 1
y[21,261,0] = 1
y[21,86,58] = 1
y[21,55,67] = 1
y[21,58,181] = 1
y[21,181,89] = 1
xD[50,222,128] = 580
xD[78,89,55] = 59
xD[78,67,232] = 59
xD[78,86,58] = 59
xD[78,55,67] = 59
xD[78,58,181] = 59
xD[78,181,89] = 59
xD[79,86,58] = 314
xD[79,58,181] = 314
xD[80,86,58] = 1
xD[80,58,181] = 1
xD[81,89,55] = 373
xD[81,232,261] = 373
xD[81,67,232] = 373
xD[81,86,58] = 373
xD[81,55,67] = 373
xD[81,58,181] = 373
xD[81,181,89] = 373
xD[82,89,55] = 169
xD[82,181,89] = 169
xD[83,89,55] = 7
xD[83,55,67] = 7
xD[83,181,89] = 7
xD[85,89,55] = 9
xD[85,232,261] = 9
xD[85,67,232] = 9
xD[85,55,67] = 9
xD[85,181,89] = 9
xD[86,89,55] = 20
xD[86,67,232] = 20
xD[86,55,67] = 20
xD[89,89,55] = 969
xD[89,232,261] = 969
xD[89,67,232] = 969
xD[89,55,67] = 969
xD[90,89,55] = 39
xD[90,232,261] = 39
xD[90,67,232] = 39
xD[90,55,67] = 39
xD[91,67,232] = 370
xD[91,55,67] = 370
xD[92,232,261] = 975
xD[92,67,232] = 975
xD[92,55,67] = 975
xD[93,232,261] = 1
xD[93,67,232] = 1
xD[93,55,67] = 1
xD[96,232,261] = 356
xD[97,232,261] = 100
xD[180,35,295] = 540
xD[180,295,260] = 540
xD[181,35,295] = 69
xD[181,295,260] = 69
xD[182,35,295] = 138
xD[182,295,260] = 138
zE[35] = 1072
zE[55] = 1102
zE[58] = 959
zE[67] = 1137
zE[86] = 910
zE[89] = 1030
zE[128] = 284
zE[181] = 1006
zE[222] = 238
zE[224] = 406
zE[232] = 1545
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1230.71
zX[35] = 1090
zX[55] = 1111
zX[58] = 969
zX[67] = 1155
zX[86] = 932
zX[89] = 1048
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[181] = 1022
zX[195] = 843
zX[222] = 259
zX[224] = 427
zX[232] = 1568
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1232.71
w[15,(295,260)] = 125.884
w[15,(35,295)] = 140.711
phi[15] = 1326
phi[16] = 990
phi[21] = 809

### All variables with value smaller zero:

### End

real	25m52.402s
user	90m47.401s
sys	2m52.390s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
