	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_4_1_fr.p
Instance LSFDP: LSFDRP_4_1_4_1_fd_22.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_4_1_fd_22 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520555 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10340 rows and 22353 columns (presolve time = 5s) ...
Presolve removed 11222 rows and 24892 columns (presolve time = 11s) ...
Presolve removed 11388 rows and 28015 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11388 rows and 145088 columns (presolve time = 675s) ...
Presolve removed 17324 rows and 162492 columns (presolve time = 680s) ...
Presolve removed 23718 rows and 177000 columns (presolve time = 685s) ...
Presolve removed 24309 rows and 194756 columns (presolve time = 1373s) ...
Presolve removed 24309 rows and 194756 columns
Presolve time: 1373.19s
Presolved: 77201 rows, 1844667 columns, 6671330 nonzeros
Variable types: 1798679 continuous, 45988 integer (45940 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    6.7110968e+02   8.000000e+00   4.722231e+08   1396s
Concurrent spin time: 0.24s

Solved with dual simplex

Root relaxation: objective -1.339359e+06, 10950 iterations, 3.50 seconds
Total elapsed time = 1398.43s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 1339359.23    0   63          - 1339359.23      -     - 1398s
H    0     0                    -65947.46031 1339359.23  2131%     - 1399s
H    0     0                    549373.53993 1339359.23   144%     - 1399s
     0     0 1153640.23    0   54 549373.540 1153640.23   110%     - 1403s
     0     0 1024699.24    0   50 549373.540 1024699.24  86.5%     - 1409s
     0     0 805307.276    0   74 549373.540 805307.276  46.6%     - 1410s
     0     0 787915.725    0   62 549373.540 787915.725  43.4%     - 1410s
     0     0 787915.725    0   62 549373.540 787915.725  43.4%     - 1410s
     0     0 777170.509    0   64 549373.540 777170.509  41.5%     - 1411s
     0     0 772923.926    0   75 549373.540 772923.926  40.7%     - 1411s
     0     0 772456.947    0   76 549373.540 772456.947  40.6%     - 1411s
     0     0 747399.486    0   51 549373.540 747399.486  36.0%     - 1412s
     0     0 746914.265    0   56 549373.540 746914.265  36.0%     - 1412s
     0     0 746914.265    0   56 549373.540 746914.265  36.0%     - 1413s
H    0     0                    644569.63124 746914.265  15.9%     - 1413s
*    0     0               0    644569.65131 644569.651  0.00%     - 1414s

Explored 1 nodes (18985 simplex iterations) in 1414.98 seconds
Thread count was 4 (of 16 available processors)

Solution count 4: 644570 644570 549374 -65947.5 
No other solutions better than 644570

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (9.5828e-06) exceeds tolerance
Warning: max bound violation (6.6115e-06) exceeds tolerance
Best objective 6.445696573246e+05, best bound 6.445696573246e+05, gap 0.0000%

***
***  |- Calculation finished, took 1,415.01 (1415.01) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is 644,569.6573 (644569.6573) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel 15 has the path [151, 222, 128, 35, 295, 260, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 35 (CLLQN In@1072.0/Out@109
###  |  0.0) - 295 (PECLL In@1230.71/Out@1346.12) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 180,  540.0 containers from  35 to 260
###  |   |- and carried Demand 181,   69.0 containers from  35 to 260
###  |   |- and carried Demand 182,  138.0 containers from  35 to 260
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |- Vessel 21 has the path [195, 86, 58, 181, 89, 55, 67, 232, 261, 0]
###  |  195 (JPYOK Out@843.0) - 86 (CNXMN In@910.0/Out@932.0) - 58 (CNNSA In@959.0/Out@969.0) - 181 (HKHKG In@1006.0/Out@1022
###  |  .0) - 89 (CNYTN In@1030.0/Out@1048.0) - 55 (CNNGB In@1102.0/Out@1111.0) - 67 (CNSHA In@1137.0/Out@1155.0) - 232 (MXLZ
###  |  C In@1545.0/Out@1568.0) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  78,   59.0 containers from  86 to 232
###  |   |- and carried Demand  79,  314.0 containers from  86 to 181
###  |   |- and carried Demand  80,    1.0 containers from  86 to 181
###  |   |- and carried Demand  81,  373.0 containers from  86 to 261
###  |   |- and carried Demand  82,  169.0 containers from 181 to  55
###  |   |- and carried Demand  83,    7.0 containers from 181 to  67
###  |   |- and carried Demand  85,    9.0 containers from 181 to 261
###  |   |- and carried Demand  86,   20.0 containers from  89 to 232
###  |   |- and carried Demand  89,  969.0 containers from  89 to 261
###  |   |- and carried Demand  90,   39.0 containers from  89 to 261
###  |   |- and carried Demand  91,  370.0 containers from  55 to 232
###  |   |- and carried Demand  92,  975.0 containers from  55 to 261
###  |   |- and carried Demand  93,    1.0 containers from  55 to 261
###  |   |- and carried Demand  96,  356.0 containers from 232 to 261
###  |   |- and carried Demand  97,  100.0 containers from 232 to 261
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[15,35,295] = 1
y[15,222,128] = 1
y[15,295,260] = 1
y[15,260,0] = 1
y[15,151,222] = 1
y[15,128,35] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
y[21,89,55] = 1
y[21,232,261] = 1
y[21,195,86] = 1
y[21,67,232] = 1
y[21,261,0] = 1
y[21,86,58] = 1
y[21,55,67] = 1
y[21,58,181] = 1
y[21,181,89] = 1
xD[50,222,128] = 580
xD[78,89,55] = 59
xD[78,67,232] = 59
xD[78,86,58] = 59
xD[78,55,67] = 59
xD[78,58,181] = 59
xD[78,181,89] = 59
xD[79,86,58] = 314
xD[79,58,181] = 314
xD[80,86,58] = 1
xD[80,58,181] = 1
xD[81,89,55] = 373
xD[81,232,261] = 373
xD[81,67,232] = 373
xD[81,86,58] = 373
xD[81,55,67] = 373
xD[81,58,181] = 373
xD[81,181,89] = 373
xD[82,89,55] = 169
xD[82,181,89] = 169
xD[83,89,55] = 7
xD[83,55,67] = 7
xD[83,181,89] = 7
xD[85,89,55] = 9
xD[85,232,261] = 9
xD[85,67,232] = 9
xD[85,55,67] = 9
xD[85,181,89] = 9
xD[86,89,55] = 20
xD[86,67,232] = 20
xD[86,55,67] = 20
xD[89,89,55] = 969
xD[89,232,261] = 969
xD[89,67,232] = 969
xD[89,55,67] = 969
xD[90,89,55] = 39
xD[90,232,261] = 39
xD[90,67,232] = 39
xD[90,55,67] = 39
xD[91,67,232] = 370
xD[91,55,67] = 370
xD[92,232,261] = 975
xD[92,67,232] = 975
xD[92,55,67] = 975
xD[93,232,261] = 1
xD[93,67,232] = 1
xD[93,55,67] = 1
xD[96,232,261] = 356
xD[97,232,261] = 100
xD[180,35,295] = 540
xD[180,295,260] = 540
xD[181,35,295] = 69
xD[181,295,260] = 69
xD[182,35,295] = 138
xD[182,295,260] = 138
zE[35] = 1072
zE[55] = 1102
zE[58] = 959
zE[67] = 1137
zE[86] = 910
zE[89] = 1030
zE[128] = 284
zE[181] = 1006
zE[222] = 238
zE[224] = 406
zE[232] = 1545
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1230.71
zX[35] = 1090
zX[55] = 1111
zX[58] = 969
zX[67] = 1155
zX[86] = 932
zX[89] = 1048
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[181] = 1022
zX[195] = 843
zX[222] = 259
zX[224] = 427
zX[232] = 1568
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1346.12
w[15,(295,260)] = 125.884
w[15,(35,295)] = 140.711
phi[15] = 1326
phi[16] = 990
phi[21] = 809

### All variables with value smaller zero:

### End

real	25m48.734s
user	92m18.164s
sys	2m21.767s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
