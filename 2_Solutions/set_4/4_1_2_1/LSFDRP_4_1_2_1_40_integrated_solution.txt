	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_2_1_fr.p
Instance LSFDP: LSFDRP_4_1_2_1_fd_40.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_2_1_fd_40 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520303 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10263 rows and 21669 columns (presolve time = 5s) ...
Presolve removed 11170 rows and 25785 columns (presolve time = 10s) ...
Presolve removed 11419 rows and 28908 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11419 rows and 145030 columns (presolve time = 685s) ...
Presolve removed 17337 rows and 145084 columns (presolve time = 685s) ...
Presolve removed 23357 rows and 164028 columns (presolve time = 690s) ...
Presolve removed 24290 rows and 196191 columns (presolve time = 699s) ...
Presolve removed 24290 rows and 196192 columns (presolve time = 1311s) ...
Presolve removed 24290 rows and 196192 columns
Presolve time: 1311.02s
Presolved: 77220 rows, 1843231 columns, 6677893 nonzeros
Variable types: 1797205 continuous, 46026 integer (45978 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.0571474e+02   8.000000e+00   5.209173e+08   1332s
   22769    6.2711592e+05   0.000000e+00   2.263264e+07   1335s
Concurrent spin time: 0.03s

Solved with dual simplex

Root relaxation: objective 7.961612e+05, 9489 iterations, 3.73 seconds
Total elapsed time = 1335.30s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.16    0   59          - -796161.16      -     - 1335s
H    0     0                    -2384469.326 -796161.16  66.6%     - 1336s
H    0     0                    -2223054.314 -796161.16  64.2%     - 1337s
H    0     0                    -2223054.311 -796161.16  64.2%     - 1338s
     0     0 -1173611.4    0  100 -2223054.3 -1173611.4  47.2%     - 1341s
     0     0 -1242643.6    0   67 -2223054.3 -1242643.6  44.1%     - 1344s
     0     0 -1242643.6    0   61 -2223054.3 -1242643.6  44.1%     - 1348s
     0     0 -1276740.4    0   67 -2223054.3 -1276740.4  42.6%     - 1350s
     0     0 -1277340.1    0   72 -2223054.3 -1277340.1  42.5%     - 1350s
     0     0 -1277489.5    0   79 -2223054.3 -1277489.5  42.5%     - 1350s
     0     0 -1277489.5    0   81 -2223054.3 -1277489.5  42.5%     - 1351s
     0     0 -1280832.2    0   82 -2223054.3 -1280832.2  42.4%     - 1352s
     0     0 -1283683.7    0   86 -2223054.3 -1283683.7  42.3%     - 1352s
     0     0 -1283683.7    0   92 -2223054.3 -1283683.7  42.3%     - 1355s
     0     2 -1283683.7    0   91 -2223054.3 -1283683.7  42.3%     - 1356s
     6     7 -1409612.0    3   64 -2223054.3 -1340549.6  39.7%   495 1361s
*   20    14               6    -2071808.817 -1434435.5  30.8%   400 1363s
*   34    18              11    -2060866.598 -1434435.5  30.4%   302 1364s
*   35    18              11    -1944079.597 -1434435.5  26.2%   305 1364s
    43    25 -1502794.5   11   28 -1944079.6 -1437384.2  26.1%   311 1365s
   156    72 -1882037.5   42   25 -1944079.6 -1437384.2  26.1%   227 1370s
*  167    69              10    -1836306.597 -1437384.2  21.7%   216 1370s
*  284    79              18    -1834302.506 -1507394.5  17.8%   186 1372s
*  316    94              17    -1823941.417 -1509741.5  17.2%   181 1373s
H  317    90                    -1804847.977 -1509741.5  16.4%   180 1373s
H  367   107                    -1804847.976 -1509741.5  16.4%   173 1374s
   401   110     cutoff   21      -1804848.0 -1517041.0  15.9%   178 1375s
H  513   127                    -1804847.976 -1548622.4  14.2%   174 1377s
H  561   137                    -1804847.968 -1555925.2  13.8%   168 1378s
H  565   128                    -1759617.975 -1575533.0  10.5%   169 1378s
H  627   141                    -1750336.977 -1580766.6  9.69%   164 1379s
   645   135 infeasible   25      -1750337.0 -1582474.1  9.59%   161 1380s
H  749   126                    -1750336.972 -1600012.6  8.59%   165 1383s
   821   116     cutoff   16      -1750337.0 -1609560.6  8.04%   161 1385s

Cutting planes:
  Learned: 1
  Cover: 1
  Implied bound: 8
  MIR: 15
  Flow cover: 3
  Zero half: 6

Explored 1095 nodes (194224 simplex iterations) in 1389.80 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75034e+06 -1.75962e+06 ... -1.83631e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (1.3936e-06) exceeds tolerance
Best objective -1.750336971583e+06, best bound -1.750336971583e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,389.84 (1389.84) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9716 (-1750336.9716) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	25m26.516s
user	90m41.507s
sys	2m27.004s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
