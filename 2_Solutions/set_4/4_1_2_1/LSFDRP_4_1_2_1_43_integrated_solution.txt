	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_2_1_fr.p
Instance LSFDP: LSFDRP_4_1_2_1_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_2_1_fd_43 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520303 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10038 rows and 20000 columns (presolve time = 5s) ...
Presolve removed 11170 rows and 25761 columns (presolve time = 10s) ...
Presolve removed 11386 rows and 28875 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11419 rows and 28908 columns (presolve time = 21s) ...
Presolve removed 11419 rows and 145030 columns (presolve time = 627s) ...
Presolve removed 17352 rows and 145094 columns (presolve time = 630s) ...
Presolve removed 23565 rows and 164231 columns (presolve time = 635s) ...
Presolve removed 24290 rows and 196191 columns (presolve time = 643s) ...
Presolve removed 24290 rows and 196192 columns (presolve time = 1272s) ...
Presolve removed 24290 rows and 196192 columns
Presolve time: 1272.22s
Presolved: 77220 rows, 1843231 columns, 6677893 nonzeros
Variable types: 1797205 continuous, 46026 integer (45978 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.7455926e+02   8.000000e+00   5.213698e+08   1293s
   14623    1.0109889e+06   0.000000e+00   4.068757e+08   1295s
Concurrent spin time: 0.23s

Solved with dual simplex

Root relaxation: objective 7.961611e+05, 10831 iterations, 3.51 seconds
Total elapsed time = 1296.02s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.13    0   63          - -796161.13      -     - 1296s
H    0     0                    -2599567.466 -796161.13  69.4%     - 1296s
H    0     0                    -2223054.311 -796161.13  64.2%     - 1298s
H    0     0                    -2223054.293 -796161.13  64.2%     - 1299s
     0     0 -1183625.0    0   58 -2223054.3 -1183625.0  46.8%     - 1301s
H    0     0                    -2200958.882 -1183625.0  46.2%     - 1302s
     0     0 -1241854.9    0   63 -2200958.9 -1241854.9  43.6%     - 1303s
     0     0 -1277965.3    0   63 -2200958.9 -1277965.3  41.9%     - 1315s
     0     0 -1461433.1    0   89 -2200958.9 -1461433.1  33.6%     - 1317s
     0     0 -1502102.2    0   65 -2200958.9 -1502102.2  31.8%     - 1317s
     0     0 -1513250.8    0   77 -2200958.9 -1513250.8  31.2%     - 1317s
     0     0 -1513250.8    0   81 -2200958.9 -1513250.8  31.2%     - 1317s
     0     0 -1528339.7    0   82 -2200958.9 -1528339.7  30.6%     - 1318s
     0     0 -1529142.3    0   82 -2200958.9 -1529142.3  30.5%     - 1318s
H    0     0                    -1845587.590 -1529142.3  17.1%     - 1318s
     0     0 -1529492.1    0   86 -1845587.6 -1529492.1  17.1%     - 1318s
     0     0 -1529492.1    0   86 -1845587.6 -1529492.1  17.1%     - 1318s
     0     0 -1530414.4    0   90 -1845587.6 -1530414.4  17.1%     - 1319s
     0     0 -1530414.4    0   60 -1845587.6 -1530414.4  17.1%     - 1322s
     0     0 -1530414.4    0   90 -1845587.6 -1530414.4  17.1%     - 1323s
     0     0 -1538304.0    0   80 -1845587.6 -1538304.0  16.6%     - 1323s
     0     0 -1540733.6    0   80 -1845587.6 -1540733.6  16.5%     - 1323s
     0     0 -1540733.6    0   80 -1845587.6 -1540733.6  16.5%     - 1323s
     0     0 -1556868.2    0   82 -1845587.6 -1556868.2  15.6%     - 1323s
     0     0 -1564824.4    0   86 -1845587.6 -1564824.4  15.2%     - 1323s
     0     0 -1565100.7    0   84 -1845587.6 -1565100.7  15.2%     - 1323s
     0     0 -1565100.7    0   82 -1845587.6 -1565100.7  15.2%     - 1323s
H    0     0                    -1800806.844 -1565100.7  13.1%     - 1324s
     0     0 -1568504.3    0   78 -1800806.8 -1568504.3  12.9%     - 1324s
     0     0 -1590817.4    0   76 -1800806.8 -1590817.4  11.7%     - 1325s
     0     0 -1590817.4    0   76 -1800806.8 -1590817.4  11.7%     - 1325s
H    0     0                    -1778711.379 -1590817.4  10.6%     - 1325s
     0     0 -1645468.5    0   26 -1778711.4 -1645468.5  7.49%     - 1327s
H    0     0                    -1760743.283 -1645468.5  6.55%     - 1327s
     0     0 -1750336.9    0    7 -1760743.3 -1750336.9  0.59%     - 1327s
*    0     0               0    -1750336.949 -1750336.9  0.00%     - 1327s

Explored 1 nodes (31644 simplex iterations) in 1327.37 seconds
Thread count was 4 (of 16 available processors)

Solution count 9: -1.75034e+06 -1.76074e+06 -1.77871e+06 ... -2.59957e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (3.1183e-06) exceeds tolerance
Warning: max bound violation (3.0017e-06) exceeds tolerance
Best objective -1.750336947599e+06, best bound -1.750336947599e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,327.4 (1327.4) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9476 (-1750336.9476) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	24m13.613s
user	86m37.609s
sys	1m59.292s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
