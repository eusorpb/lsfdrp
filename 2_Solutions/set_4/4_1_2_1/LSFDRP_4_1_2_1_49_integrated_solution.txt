	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_2_1_fr.p
Instance LSFDP: LSFDRP_4_1_2_1_fd_49.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_2_1_fd_49 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520303 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 3e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7031 rows and 16993 columns (presolve time = 5s) ...
Presolve removed 11170 rows and 25761 columns (presolve time = 10s) ...
Presolve removed 11386 rows and 28875 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11419 rows and 28908 columns (presolve time = 22s) ...
Presolve removed 11419 rows and 145030 columns (presolve time = 672s) ...
Presolve removed 17352 rows and 145094 columns (presolve time = 675s) ...
Presolve removed 23565 rows and 164231 columns (presolve time = 680s) ...
Presolve removed 24290 rows and 196191 columns (presolve time = 689s) ...
Presolve removed 24290 rows and 196192 columns (presolve time = 1401s) ...
Presolve removed 24290 rows and 196192 columns
Presolve time: 1400.65s
Presolved: 77220 rows, 1843231 columns, 6677893 nonzeros
Variable types: 1797205 continuous, 46026 integer (45978 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.2368981e+02   8.000000e+00   5.212924e+08   1425s
     953    1.6868911e+07   0.000000e+00   1.745875e+10   1425s
Concurrent spin time: 0.00s

Solved with dual simplex

Root relaxation: objective 7.961611e+05, 10001 iterations, 3.40 seconds
Total elapsed time = 1427.82s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.13    0   63          - -796161.13      -     - 1428s
H    0     0                    -2599567.466 -796161.13  69.4%     - 1428s
H    0     0                    -2223054.315 -796161.13  64.2%     - 1429s
H    0     0                    -2223054.311 -796161.13  64.2%     - 1430s
     0     0 -1183625.0    0   54 -2223054.3 -1183625.0  46.8%     - 1432s
H    0     0                    -2200958.882 -1183625.0  46.2%     - 1432s
     0     0 -1209465.0    0   82 -2200958.9 -1209465.0  45.0%     - 1434s
     0     0 -1217450.9    0   57 -2200958.9 -1217450.9  44.7%     - 1446s
     0     0 -1378388.7    0   72 -2200958.9 -1378388.7  37.4%     - 1447s
     0     0 -1511299.8    0   65 -2200958.9 -1511299.8  31.3%     - 1448s
     0     0 -1524592.5    0   66 -2200958.9 -1524592.5  30.7%     - 1448s
H    0     0                    -1917593.824 -1524592.5  20.5%     - 1449s
H    0     0                    -1818239.880 -1524592.5  16.2%     - 1449s
     0     0 -1524725.1    0   74 -1818239.9 -1524725.1  16.1%     - 1449s
     0     0 -1532840.4    0   76 -1818239.9 -1532840.4  15.7%     - 1449s
     0     0 -1532840.4    0   58 -1818239.9 -1532840.4  15.7%     - 1452s
     0     0 -1532840.4    0   71 -1818239.9 -1532840.4  15.7%     - 1453s
H    0     0                    -1770024.289 -1532840.4  13.4%     - 1453s
H    0     0                    -1759617.969 -1532840.4  12.9%     - 1453s
     0     0 -1545026.7    0   65 -1759618.0 -1545026.7  12.2%     - 1453s
     0     0 -1546845.2    0   63 -1759618.0 -1546845.2  12.1%     - 1453s
     0     0 -1546845.2    0   64 -1759618.0 -1546845.2  12.1%     - 1453s
     0     0 -1554796.1    0   73 -1759618.0 -1554796.1  11.6%     - 1453s
     0     0 -1556277.1    0   62 -1759618.0 -1556277.1  11.6%     - 1453s
H    0     0                    -1750336.967 -1556277.1  11.1%     - 1453s
     0     0 -1556277.1    0   62 -1750337.0 -1556277.1  11.1%     - 1453s
     0     0 -1571087.8    0   65 -1750337.0 -1571087.8  10.2%     - 1454s
     0     0 -1571087.8    0   65 -1750337.0 -1571087.8  10.2%     - 1454s
     0     0 -1607018.0    0   53 -1750337.0 -1607018.0  8.19%     - 1455s
     0     0 -1750337.0    0    7 -1750337.0 -1750337.0  0.00%     - 1455s

Cutting planes:
  Implied bound: 6
  Clique: 4
  MIR: 2
  Flow cover: 8
  Zero half: 1

Explored 1 nodes (28317 simplex iterations) in 1458.18 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75962e+06 -1.77002e+06 ... -2.59957e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986698e+06, best bound -1.750336955142e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,458.21 (1458.21) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9867 (-1750336.9867) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	26m53.649s
user	94m11.150s
sys	3m27.899s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
