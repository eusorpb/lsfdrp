	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_2_1_fr.p
Instance LSFDP: LSFDRP_4_1_2_1_fd_16.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_2_1_fd_16 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520303 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 9e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10263 rows and 21669 columns (presolve time = 5s) ...
Presolve removed 11170 rows and 25785 columns (presolve time = 10s) ...
Presolve removed 11419 rows and 28908 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11419 rows and 145030 columns (presolve time = 676s) ...
Presolve removed 17355 rows and 162992 columns (presolve time = 680s) ...
Presolve removed 23699 rows and 178460 columns (presolve time = 685s) ...
Presolve removed 24290 rows and 196192 columns (presolve time = 1354s) ...
Presolve removed 24290 rows and 196192 columns
Presolve time: 1353.57s
Presolved: 77220 rows, 1843231 columns, 6677893 nonzeros
Variable types: 1797205 continuous, 46026 integer (45978 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    9.7560377e+02   8.000000e+00   5.224372e+08   1377s
Concurrent spin time: 0.20s

Solved with dual simplex

Root relaxation: objective 7.109469e+05, 8133 iterations, 2.60 seconds
Total elapsed time = 1378.41s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -710946.88    0   94          - -710946.88      -     - 1379s
H    0     0                    -3564173.526 -710946.88  80.1%     - 1379s
     0     0 -1239394.6    0   71 -3564173.5 -1239394.6  65.2%     - 1384s
H    0     0                    -2950411.323 -1239394.6  58.0%     - 1385s
     0     0 -1239671.3    0   66 -2950411.3 -1239671.3  58.0%     - 1385s
     0     0 -1240008.8    0   66 -2950411.3 -1240008.8  58.0%     - 1385s
     0     0 -1251134.6    0   73 -2950411.3 -1251134.6  57.6%     - 1387s
H    0     0                    -2730593.901 -1251134.6  54.2%     - 1387s
     0     0 -1253310.1    0   77 -2730593.9 -1253310.1  54.1%     - 1388s
     0     0 -1254703.4    0   80 -2730593.9 -1254703.4  54.1%     - 1389s
     0     0 -1254703.4    0   76 -2730593.9 -1254703.4  54.1%     - 1394s
H    0     0                    -2423198.577 -1254703.4  48.2%     - 1395s
H    0     0                    -2423198.576 -1254703.4  48.2%     - 1396s
H    0     0                    -2423198.574 -1254703.4  48.2%     - 1396s
     0     0 -1270032.2    0   73 -2423198.6 -1270032.2  47.6%     - 1398s
     0     0 -1270032.2    0   66 -2423198.6 -1270032.2  47.6%     - 1402s
H    0     0                    -2423198.573 -1270032.2  47.6%     - 1403s
     0     0 -1320668.2    0   59 -2423198.6 -1320668.2  45.5%     - 1404s
     0     0 -1320668.2    0   59 -2423198.6 -1320668.2  45.5%     - 1404s
     0     0 -1321793.0    0   75 -2423198.6 -1321793.0  45.5%     - 1405s
     0     0 -1321793.0    0   77 -2423198.6 -1321793.0  45.5%     - 1405s
     0     0 -1322377.5    0   69 -2423198.6 -1322377.5  45.4%     - 1408s
     0     2 -1322377.5    0   69 -2423198.6 -1322377.5  45.4%     - 1410s
     6     8 -1348616.4    3   74 -2423198.6 -1348616.4  44.3%   894 1415s
    39    24 -1506820.9   10   62 -2423198.6 -1348616.4  44.3%   472 1420s
H   54    36                    -2411292.330 -1349245.6  44.0%   457 1422s
*   77    43              10    -2099537.809 -1349245.6  35.7%   437 1423s
*   79    42              20    -2009919.503 -1349245.6  32.9%   426 1423s
   110    47 -1624673.9   10   56 -2009919.5 -1357767.3  32.4%   401 1425s
*  150    56              18    -1981333.371 -1369048.6  30.9%   378 1427s
*  178    61              24    -1972052.351 -1369048.6  30.6%   357 1428s
H  206    61                    -1750336.980 -1383287.9  21.0%   350 1430s
H  262    75                    -1750336.948 -1453270.7  17.0%   336 1432s
   322    77     cutoff   14      -1750336.9 -1502859.0  14.1%   315 1435s
   530    80 -1730386.4   11   60 -1750336.9 -1534868.6  12.3%   291 1440s
*  561    83              13    -1750336.947 -1534868.6  12.3%   286 1440s
   794     4 infeasible   14      -1750336.9 -1660201.4  5.15%   275 1445s

Cutting planes:
  Implied bound: 10
  Clique: 32
  MIR: 15
  Flow cover: 3
  Zero half: 5

Explored 853 nodes (264404 simplex iterations) in 1446.10 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75034e+06 -1.75034e+06 ... -2.4232e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (7.6135e-06) exceeds tolerance
Warning: max bound violation (5.9025e-06) exceeds tolerance
Best objective -1.750336946002e+06, best bound -1.750336946002e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,446.13 (1446.13) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.946 (-1750336.946) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	26m18.750s
user	94m27.189s
sys	2m19.935s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
