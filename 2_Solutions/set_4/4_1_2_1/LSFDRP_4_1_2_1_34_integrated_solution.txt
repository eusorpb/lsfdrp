	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_2_1_fr.p
Instance LSFDP: LSFDRP_4_1_2_1_fd_34.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_2_1_fd_34 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520303 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7031 rows and 16993 columns (presolve time = 5s) ...
Presolve removed 11170 rows and 25761 columns (presolve time = 10s) ...
Presolve removed 11386 rows and 28875 columns (presolve time = 15s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11419 rows and 28908 columns (presolve time = 22s) ...
Presolve removed 11419 rows and 145030 columns (presolve time = 656s) ...
Presolve removed 17355 rows and 145097 columns (presolve time = 660s) ...
Presolve removed 23696 rows and 178333 columns (presolve time = 665s) ...
Presolve removed 24290 rows and 196191 columns (presolve time = 672s) ...
Presolve removed 24290 rows and 196192 columns (presolve time = 1356s) ...
Presolve removed 24290 rows and 196192 columns
Presolve time: 1355.74s
Presolved: 77220 rows, 1843231 columns, 6677893 nonzeros
Variable types: 1797205 continuous, 46026 integer (45978 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.8452497e+02   8.000000e+00   5.210373e+08   1381s
Concurrent spin time: 0.72s

Solved with dual simplex

Root relaxation: objective 7.961612e+05, 9745 iterations, 4.05 seconds
Total elapsed time = 1383.86s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -796161.16    0   59          - -796161.16      -     - 1384s
H    0     0                    -2229555.061 -796161.16  64.3%     - 1384s
H    0     0                    -2223054.308 -796161.16  64.2%     - 1386s
     0     0 -1134189.7    0  103 -2223054.3 -1134189.7  49.0%     - 1389s
H    0     0                    -2154353.318 -1134189.7  47.4%     - 1391s
     0     0 -1240779.7    0   63 -2154353.3 -1240779.7  42.4%     - 1393s
     0     0 -1240779.7    0   81 -2154353.3 -1240779.7  42.4%     - 1397s
     0     0 -1304359.4    0   70 -2154353.3 -1304359.4  39.5%     - 1399s
     0     0 -1304782.1    0   73 -2154353.3 -1304782.1  39.4%     - 1400s
     0     0 -1307379.3    0   66 -2154353.3 -1307379.3  39.3%     - 1401s
     0     0 -1307410.1    0   68 -2154353.3 -1307410.1  39.3%     - 1401s
     0     0 -1307487.3    0   68 -2154353.3 -1307487.3  39.3%     - 1401s
     0     0 -1307704.8    0   68 -2154353.3 -1307704.8  39.3%     - 1403s
     0     2 -1307704.8    0   68 -2154353.3 -1307704.8  39.3%     - 1404s
     1     5 -1308022.3    1   67 -2154353.3 -1308022.3  39.3%  85.0 1407s
     7     8 -1431689.7    3   56 -2154353.3 -1345300.4  37.6%   680 1410s
*   58    28               9    -1961676.656 -1434474.8  26.9%   297 1414s
    59    28 -1649062.5   14   52 -1961676.7 -1434474.8  26.9%   298 1415s
*  211    78              12    -1836306.598 -1465972.4  20.2%   167 1419s
*  259    59              12    -1760743.296 -1477755.3  16.1%   160 1419s
   263    62 -1484170.2    8   63 -1760743.3 -1477755.3  16.1%   162 1420s
*  308    77              11    -1750336.977 -1477755.3  15.6%   173 1422s
H  405    72                    -1750336.976 -1539367.2  12.1%   170 1424s
   425    70     cutoff    8      -1750337.0 -1554677.5  11.2%   170 1425s

Cutting planes:
  Cover: 1
  Implied bound: 5
  Clique: 25
  MIR: 12
  Flow cover: 3
  Zero half: 1

Explored 713 nodes (131020 simplex iterations) in 1428.86 seconds
Thread count was 4 (of 16 available processors)

Solution count 8: -1.75034e+06 -1.75034e+06 -1.76074e+06 ... -2.22956e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (4.5327e-06) exceeds tolerance
Warning: max bound violation (4.5327e-06) exceeds tolerance
Best objective -1.750336976257e+06, best bound -1.750336976257e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,428.89 (1428.89) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9763 (-1750336.9763) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	26m23.190s
user	93m23.236s
sys	2m21.194s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
