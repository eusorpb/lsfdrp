	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_2_1_fr.p
Instance LSFDP: LSFDRP_4_1_2_1_fd_31.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_2_1_fd_31 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520303 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10038 rows and 20000 columns (presolve time = 6s) ...
Presolve removed 11170 rows and 25761 columns (presolve time = 10s) ...
Presolve removed 11386 rows and 28875 columns (presolve time = 16s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11419 rows and 28908 columns (presolve time = 23s) ...
Presolve removed 11419 rows and 145030 columns (presolve time = 649s) ...
Presolve removed 17352 rows and 145094 columns (presolve time = 651s) ...
Presolve removed 17355 rows and 162992 columns (presolve time = 655s) ...
Presolve removed 23565 rows and 178203 columns (presolve time = 660s) ...
Presolve removed 24290 rows and 196191 columns (presolve time = 668s) ...
Presolve removed 24290 rows and 196192 columns (presolve time = 1316s) ...
Presolve removed 24290 rows and 196192 columns
Presolve time: 1316.41s
Presolved: 77220 rows, 1843231 columns, 6677893 nonzeros
Variable types: 1797205 continuous, 46026 integer (45978 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    4.3537029e+02   8.000000e+00   5.211147e+08   1340s
    5769    1.7186649e+06   0.000000e+00   3.854981e+08   1340s
Concurrent spin time: 0.15s

Solved with dual simplex

Root relaxation: objective 7.920661e+05, 7894 iterations, 3.04 seconds
Total elapsed time = 1341.98s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -792066.08    0   75          - -792066.08      -     - 1342s
H    0     0                    -3738244.792 -792066.08  78.8%     - 1342s
H    0     0                    -3040095.621 -792066.08  73.9%     - 1343s
H    0     0                    -2223054.315 -792066.08  64.4%     - 1344s
H    0     0                    -2223054.309 -792066.08  64.4%     - 1345s
     0     0 -1183625.0    0   54 -2223054.3 -1183625.0  46.8%     - 1347s
     0     0 -1240779.8    0   63 -2223054.3 -1240779.8  44.2%     - 1349s
     0     0 -1240779.8    0   72 -2223054.3 -1240779.8  44.2%     - 1353s
     0     0 -1283211.7    0   74 -2223054.3 -1283211.7  42.3%     - 1355s
     0     0 -1286782.4    0   76 -2223054.3 -1286782.4  42.1%     - 1356s
     0     0 -1286782.4    0   77 -2223054.3 -1286782.4  42.1%     - 1356s
     0     0 -1286782.4    0   82 -2223054.3 -1286782.4  42.1%     - 1356s
     0     0 -1289344.9    0   82 -2223054.3 -1289344.9  42.0%     - 1359s
     0     2 -1289344.9    0   82 -2223054.3 -1289344.9  42.0%     - 1359s
     1     4 -1311576.1    1   77 -2223054.3 -1311576.1  41.0%   649 1361s
    10    11 -1579170.2    4   80 -2223054.3 -1338984.1  39.8%   510 1365s
H   15    15                    -1975987.478 -1338984.1  32.2%   506 1366s
*   24    19               7    -1934798.597 -1415927.7  26.8%   505 1367s
    46    33 -1604521.9   13   16 -1934798.6 -1415927.7  26.8%   377 1370s
*  147    80              11    -1876404.978 -1419371.9  24.4%   229 1373s
   196    91 -1550950.3   14   44 -1876405.0 -1419371.9  24.4%   234 1375s
H  259    98                    -1759617.977 -1419371.9  19.3%   215 1376s
*  260    98              13    -1759617.976 -1419371.9  19.3%   214 1376s
   466   123 -1621870.7   18   45 -1759618.0 -1550754.2  11.9%   185 1380s
   857   149 infeasible   11      -1759618.0 -1599161.0  9.12%   153 1385s
H  895   152                    -1759617.975 -1599161.0  9.12%   150 1385s
H 1033   142                    -1750336.975 -1613543.7  7.82%   146 1387s
H 1105   110                    -1750336.969 -1631619.4  6.78%   145 1388s
H 1106   110                    -1750336.968 -1631619.4  6.78%   145 1388s
  1125    81 -1693977.3    9   44 -1750337.0 -1641143.4  6.24%   145 1390s

Cutting planes:
  Learned: 1
  Cover: 2
  Implied bound: 4
  Clique: 26
  MIR: 12
  Flow cover: 2
  Zero half: 5

Explored 1399 nodes (216141 simplex iterations) in 1397.77 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75034e+06 -1.75034e+06 ... -2.22305e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986698e+06, best bound -1.750336967281e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,397.81 (1397.81) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9867 (-1750336.9867) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 19]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	25m50.722s
user	91m12.120s
sys	2m16.947s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
