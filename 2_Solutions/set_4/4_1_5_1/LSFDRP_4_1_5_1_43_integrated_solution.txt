	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_5_1_fr.p
Instance LSFDP: LSFDRP_4_1_5_1_fd_43.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_5_1_fd_43 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520681 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10421 rows and 28357 columns (presolve time = 5s) ...
Presolve removed 11661 rows and 28464 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11663 rows and 142851 columns (presolve time = 638s) ...
Presolve removed 17661 rows and 142958 columns (presolve time = 641s) ...
Presolve removed 17665 rows and 162186 columns (presolve time = 645s) ...
Presolve removed 23828 rows and 163903 columns (presolve time = 651s) ...
Presolve removed 24553 rows and 195859 columns (presolve time = 658s) ...
Presolve removed 24740 rows and 197485 columns (presolve time = 1290s) ...
Presolve removed 24740 rows and 197485 columns
Presolve time: 1289.82s
Presolved: 76770 rows, 1841938 columns, 6639105 nonzeros
Variable types: 1798903 continuous, 43035 integer (42987 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    2.7456015e+02   8.000000e+00   3.707849e+08   1310s
    1233    1.2649885e+07   0.000000e+00   5.100428e+08   1310s
Concurrent spin time: 0.01s

Solved with dual simplex

Root relaxation: objective 8.318597e+05, 5528 iterations, 1.57 seconds
Total elapsed time = 1311.15s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -831859.73    0   60          - -831859.73      -     - 1311s
H    0     0                    -5784858.832 -831859.73  85.6%     - 1311s
H    0     0                    -2628232.821 -831859.73  68.3%     - 1312s
H    0     0                    -2423198.581 -831859.73  65.7%     - 1312s
     0     0 -1133700.6    0   87 -2423198.6 -1133700.6  53.2%     - 1314s
H    0     0                    -2384226.676 -1133700.6  52.4%     - 1315s
     0     0 -1242495.2    0   68 -2384226.7 -1242495.2  47.9%     - 1316s
     0     0 -1242495.2    0   73 -2384226.7 -1242495.2  47.9%     - 1320s
     0     0 -1246358.9    0   72 -2384226.7 -1246358.9  47.7%     - 1323s
     0     0 -1249875.1    0   71 -2384226.7 -1249875.1  47.6%     - 1323s
H    0     0                    -2257758.842 -1249875.1  44.6%     - 1324s
     0     0 -1250019.4    0   79 -2257758.8 -1250019.4  44.6%     - 1324s
H    0     0                    -2019486.199 -1250019.4  38.1%     - 1325s
     0     0 -1258257.2    0   79 -2019486.2 -1258257.2  37.7%     - 1326s
H    0     0                    -2019486.197 -1258257.2  37.7%     - 1326s
     0     0 -1258257.2    0   74 -2019486.2 -1258257.2  37.7%     - 1328s
     0     0 -1310779.3    0   66 -2019486.2 -1310779.3  35.1%     - 1330s
     0     0 -1313199.4    0   81 -2019486.2 -1313199.4  35.0%     - 1330s
     0     0 -1313216.8    0   81 -2019486.2 -1313216.8  35.0%     - 1330s
     0     0 -1323233.8    0   84 -2019486.2 -1323233.8  34.5%     - 1331s
     0     0 -1323308.4    0   84 -2019486.2 -1323308.4  34.5%     - 1331s
     0     0 -1323308.4    0   75 -2019486.2 -1323308.4  34.5%     - 1333s
     0     2 -1323308.4    0   75 -2019486.2 -1323308.4  34.5%     - 1334s
     1     3 -1341710.0    1   77 -2019486.2 -1341710.0  33.6%   328 1335s
*   27    14               7    -2010016.423 -1470308.0  26.9%   383 1339s
H   32    17                    -1894928.513 -1470308.0  22.4%   368 1339s
H   43    11                    -1847226.241 -1470308.0  20.4%   332 1339s
*   44    11              16    -1846712.923 -1470308.0  20.4%   329 1339s
    46     8 -1532239.6   11   58 -1846712.9 -1489169.1  19.4%   336 1340s
*  252    77              11    -1750336.982 -1529687.1  12.6%   131 1344s
   268    76 -1588563.7   13   57 -1750337.0 -1546866.0  11.6%   132 1345s

Cutting planes:
  Learned: 1
  Implied bound: 3
  Clique: 26
  MIR: 11
  Flow cover: 7

Explored 581 nodes (96404 simplex iterations) in 1349.32 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.84671e+06 -1.84723e+06 ... -2.4232e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (7.2344e-06) exceeds tolerance
Warning: max bound violation (6.2748e-06) exceeds tolerance
Best objective -1.750336981544e+06, best bound -1.750336981544e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,349.36 (1349.36) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9815 (-1750336.9815) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	25m4.308s
user	88m20.077s
sys	2m20.264s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
