	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_5_1_fr.p
Instance LSFDP: LSFDRP_4_1_5_1_fd_40.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_5_1_fd_40 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520681 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10421 rows and 28357 columns (presolve time = 5s) ...
Presolve removed 11661 rows and 28464 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11663 rows and 142851 columns (presolve time = 661s) ...
Presolve removed 17664 rows and 142961 columns (presolve time = 666s) ...
Presolve removed 23626 rows and 163701 columns (presolve time = 670s) ...
Presolve removed 23962 rows and 178132 columns (presolve time = 675s) ...
Presolve removed 24740 rows and 197485 columns (presolve time = 1290s) ...
Presolve removed 24740 rows and 197485 columns
Presolve time: 1290.24s
Presolved: 76770 rows, 1841938 columns, 6639105 nonzeros
Variable types: 1798903 continuous, 43035 integer (42987 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    3.0571768e+02   8.000000e+00   3.708265e+08   1310s
    2611    4.7536666e+02   4.887786e-01   5.452836e+08   1310s
    3892    1.4476026e+07   0.000000e+00   1.248681e+09   1310s
Concurrent spin time: 0.12s

Solved with dual simplex

Root relaxation: objective 8.318597e+05, 6517 iterations, 2.21 seconds
Total elapsed time = 1311.41s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -831859.73    0   62          - -831859.73      -     - 1311s
H    0     0                    -5657850.832 -831859.73  85.3%     - 1312s
H    0     0                    -2423198.584 -831859.73  65.7%     - 1313s
H    0     0                    -2195542.205 -831859.73  62.1%     - 1315s
H    0     0                    -2011697.285 -845585.68  58.0%     - 1315s
     0     0 -1154107.5    0   77 -2011697.3 -1154107.5  42.6%     - 1316s
H    0     0                    -2011697.284 -1154107.5  42.6%     - 1317s
     0     0 -1155802.3    0   82 -2011697.3 -1155802.3  42.5%     - 1317s
     0     0 -1245078.6    0   77 -2011697.3 -1245078.6  38.1%     - 1319s
     0     0 -1245078.6    0   73 -2011697.3 -1245078.6  38.1%     - 1321s
     0     0 -1247005.1    0   78 -2011697.3 -1247005.1  38.0%     - 1323s
     0     0 -1254650.3    0   81 -2011697.3 -1254650.3  37.6%     - 1324s
H    0     0                    -1821789.861 -1254650.3  31.1%     - 1324s
     0     0 -1255026.2    0   77 -1821789.9 -1255026.2  31.1%     - 1324s
     0     0 -1255222.2    0   82 -1821789.9 -1255222.2  31.1%     - 1324s
     0     0 -1258324.3    0   84 -1821789.9 -1258324.3  30.9%     - 1324s
     0     0 -1258324.3    0   85 -1821789.9 -1258324.3  30.9%     - 1325s
     0     0 -1258854.8    0   83 -1821789.9 -1258854.8  30.9%     - 1326s
     0     0 -1258854.8    0   59 -1821789.9 -1258854.8  30.9%     - 1332s
     0     0 -1652855.6    0   59 -1821789.9 -1652855.6  9.27%     - 1333s
H    0     0                    -1821789.857 -1652855.6  9.27%     - 1333s
H    0     0                    -1821789.855 -1652855.6  9.27%     - 1333s
     0     0 -1683760.2    0   45 -1821789.9 -1683760.2  7.58%     - 1333s
H    0     0                    -1770024.301 -1683760.2  4.87%     - 1333s
     0     0 -1683760.2    0   52 -1770024.3 -1683760.2  4.87%     - 1333s
*    0     0               0    -1750336.970 -1750337.0  0.00%     - 1334s

Explored 1 nodes (22154 simplex iterations) in 1334.58 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.77002e+06 -1.77002e+06 ... -2.19554e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (7.5746e-06) exceeds tolerance
Warning: max bound violation (7.0832e-06) exceeds tolerance
Best objective -1.750336969853e+06, best bound -1.750336969853e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,334.61 (1334.61) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9699 (-1750336.9699) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	24m23.685s
user	87m6.339s
sys	2m12.768s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
