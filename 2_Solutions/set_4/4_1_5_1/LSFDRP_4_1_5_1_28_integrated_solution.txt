	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_5_1_fr.p
Instance LSFDP: LSFDRP_4_1_5_1_fd_28.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_5_1_fd_28 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520681 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 7189 rows and 19140 columns (presolve time = 5s) ...
Presolve removed 11661 rows and 28440 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11663 rows and 28464 columns (presolve time = 16s) ...
Presolve removed 11663 rows and 142851 columns (presolve time = 705s) ...
Presolve removed 17634 rows and 142938 columns (presolve time = 705s) ...
Presolve removed 17664 rows and 142961 columns (presolve time = 711s) ...
Presolve removed 23626 rows and 163701 columns (presolve time = 715s) ...
Presolve removed 24553 rows and 195859 columns (presolve time = 724s) ...
Presolve removed 24740 rows and 197485 columns (presolve time = 1375s) ...
Presolve removed 24740 rows and 197485 columns
Presolve time: 1375.49s
Presolved: 76770 rows, 1841938 columns, 6639105 nonzeros
Variable types: 1798903 continuous, 43035 integer (42987 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    4.9711396e+02   8.000000e+00   3.705821e+08   1395s
    2712    1.1328271e+07   0.000000e+00   4.359162e+08   1395s
Concurrent spin time: 0.00s

Solved with dual simplex

Root relaxation: objective 8.019725e+05, 5585 iterations, 1.62 seconds
Total elapsed time = 1396.14s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -831859.73    0   60          - -831859.73      -     - 1397s
H    0     0                    -5149818.830 -831859.73  83.8%     - 1397s
     0     0 -1155514.0    0   75 -5149818.8 -1155514.0  77.6%     - 1402s
H    0     0                    -2579411.532 -1155514.0  55.2%     - 1403s
     0     0 -1243392.4    0   77 -2579411.5 -1243392.4  51.8%     - 1405s
     0     0 -1243698.4    0   73 -2579411.5 -1243698.4  51.8%     - 1405s
     0     0 -1244071.6    0   73 -2579411.5 -1244071.6  51.8%     - 1405s
     0     0 -1245214.1    0   71 -2579411.5 -1245214.1  51.7%     - 1407s
     0     0 -1245214.1    0   66 -2579411.5 -1245214.1  51.7%     - 1411s
     0     0 -1246576.8    0   81 -2579411.5 -1246576.8  51.7%     - 1413s
H    0     0                    -2421038.592 -1246576.8  48.5%     - 1414s
     0     0 -1249794.7    0   78 -2421038.6 -1249794.7  48.4%     - 1415s
     0     0 -1249794.7    0   78 -2421038.6 -1249794.7  48.4%     - 1415s
     0     0 -1263982.7    0   85 -2421038.6 -1263982.7  47.8%     - 1416s
     0     0 -1263982.7    0   85 -2421038.6 -1263982.7  47.8%     - 1416s
     0     0 -1265290.9    0   79 -2421038.6 -1265290.9  47.7%     - 1419s
     0     0 -1265290.9    0   76 -2421038.6 -1265290.9  47.7%     - 1424s
     0     0 -1316666.4    0   80 -2421038.6 -1316666.4  45.6%     - 1427s
     0     0 -1322471.3    0   79 -2421038.6 -1322471.3  45.4%     - 1428s
     0     0 -1331462.5    0  101 -2421038.6 -1331462.5  45.0%     - 1428s
     0     0 -1332786.3    0   86 -2421038.6 -1332786.3  44.9%     - 1428s
     0     0 -1332786.3    0   86 -2421038.6 -1332786.3  44.9%     - 1428s
     0     0 -1333223.8    0   84 -2421038.6 -1333223.8  44.9%     - 1429s
     0     0 -1334674.9    0   83 -2421038.6 -1334674.9  44.9%     - 1429s
     0     0 -1334716.4    0   83 -2421038.6 -1334716.4  44.9%     - 1429s
     0     0 -1334716.4    0   85 -2421038.6 -1334716.4  44.9%     - 1432s
     0     2 -1334716.4    0   85 -2421038.6 -1334716.4  44.9%     - 1434s
     1     3 -1348832.5    1   84 -2421038.6 -1348832.5  44.3%   304 1435s
     9    10 -1504184.2    4   81 -2421038.6 -1410726.7  41.7%   705 1440s
    35    27 -1488842.0    8  114 -2421038.6 -1466457.9  39.4%   322 1445s
*   56    40              18    -2314954.796 -1466457.9  36.7%   256 1446s
*   76    53               8    -2309441.783 -1466918.9  36.5%   248 1447s
*   89    49              10    -2121215.223 -1466918.9  30.8%   228 1448s
    91    41 -1612021.8   15  117 -2121215.2 -1466918.9  30.8%   226 1450s
H   92    39                    -2075985.224 -1466918.9  29.3%   223 1450s
H  103    32                    -1935247.863 -1466918.9  24.2%   214 1450s
*  105    28              12    -1894058.983 -1466918.9  22.6%   210 1450s
*  126    32               9    -1848828.983 -1514443.5  18.1%   272 1452s
*  137    28              16    -1770024.303 -1514443.5  14.4%   262 1452s
*  176    25              16    -1760743.303 -1549508.1  12.0%   279 1454s
   179    27     cutoff   12      -1760743.3 -1549508.1  12.0%   279 1455s
*  204    21              19    -1759617.983 -1599407.8  9.10%   268 1456s
*  205    21              19    -1750336.983 -1599407.8  8.62%   267 1456s

Cutting planes:
  Implied bound: 5
  Clique: 27
  MIR: 17
  Flow cover: 3
  Zero half: 3

Explored 295 nodes (96157 simplex iterations) in 1460.41 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75962e+06 -1.76074e+06 ... -2.30944e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986698e+06, best bound -1.750336983208e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,460.44 (1460.44) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9867 (-1750336.9867) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	26m44.562s
user	93m56.649s
sys	3m53.298s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
