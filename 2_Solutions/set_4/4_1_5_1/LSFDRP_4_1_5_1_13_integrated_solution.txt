	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_5_1_fr.p
Instance LSFDP: LSFDRP_4_1_5_1_fd_13.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_5_1_fd_13 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520681 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 7e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 8049 rows and 20000 columns (presolve time = 5s) ...
Presolve removed 11661 rows and 28464 columns (presolve time = 11s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11663 rows and 142851 columns (presolve time = 657s) ...
Presolve removed 17664 rows and 142961 columns (presolve time = 661s) ...
Presolve removed 23828 rows and 163903 columns (presolve time = 666s) ...
Presolve removed 24553 rows and 195859 columns (presolve time = 673s) ...
Presolve removed 24740 rows and 197485 columns (presolve time = 1287s) ...
Presolve removed 24740 rows and 197485 columns
Presolve time: 1286.67s
Presolved: 76770 rows, 1841938 columns, 6639105 nonzeros
Variable types: 1798903 continuous, 43035 integer (42987 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    1.2332535e+03   8.000000e+00   3.715653e+08   1308s
Concurrent spin time: 0.00s

Solved with dual simplex

Root relaxation: objective 7.003016e+05, 5330 iterations, 1.66 seconds
Total elapsed time = 1308.69s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -700301.57    0   91          - -700301.57      -     - 1309s
H    0     0                    -6446553.388 -700301.57  89.1%     - 1309s
     0     0 -1216091.8    0   71 -6446553.4 -1216091.8  81.1%     - 1312s
H    0     0                    -3009991.323 -1216091.8  59.6%     - 1313s
     0     0 -1218839.2    0   73 -3009991.3 -1218839.2  59.5%     - 1314s
     0     0 -1219180.8    0   73 -3009991.3 -1219180.8  59.5%     - 1314s
     0     0 -1245214.1    0   78 -3009991.3 -1245214.1  58.6%     - 1315s
     0     0 -1245214.1    0   78 -3009991.3 -1245214.1  58.6%     - 1315s
     0     0 -1245214.1    0   82 -3009991.3 -1245214.1  58.6%     - 1318s
H    0     0                    -2771390.250 -1245214.1  55.1%     - 1319s
H    0     0                    -2269856.222 -1245214.1  45.1%     - 1320s
     0     0 -1245214.1    0   76 -2269856.2 -1245214.1  45.1%     - 1323s
     0     0 -1245214.1    0   74 -2269856.2 -1245214.1  45.1%     - 1325s
H    0     0                    -2268284.323 -1245214.1  45.1%     - 1326s
     0     0 -1248138.9    0   70 -2268284.3 -1248138.9  45.0%     - 1326s
     0     0 -1248238.5    0   69 -2268284.3 -1248238.5  45.0%     - 1326s
H    0     0                    -2268284.322 -1248238.5  45.0%     - 1326s
     0     0 -1255883.0    0   90 -2268284.3 -1255883.0  44.6%     - 1327s
     0     0 -1256431.9    0   74 -2268284.3 -1256431.9  44.6%     - 1327s
H    0     0                    -2154353.324 -1256431.9  41.7%     - 1328s
H    0     0                    -1962374.605 -1256431.9  36.0%     - 1328s
     0     0 -1260361.9    0   75 -1962374.6 -1260361.9  35.8%     - 1328s
     0     0 -1260361.9    0   81 -1962374.6 -1260361.9  35.8%     - 1331s
     0     0 -1323012.6    0   79 -1962374.6 -1323012.6  32.6%     - 1332s
     0     0 -1333215.9    0   82 -1962374.6 -1333215.9  32.1%     - 1332s
     0     0 -1341936.3    0  133 -1962374.6 -1341936.3  31.6%     - 1333s
     0     0 -1342764.0    0   87 -1962374.6 -1342764.0  31.6%     - 1333s
     0     0 -1342801.2    0   87 -1962374.6 -1342801.2  31.6%     - 1333s
     0     0 -1343004.9    0  147 -1962374.6 -1343004.9  31.6%     - 1334s
     0     0 -1355924.2    0  135 -1962374.6 -1355924.2  30.9%     - 1337s
     0     2 -1355924.2    0  135 -1962374.6 -1355924.2  30.9%     - 1339s
     1     5 -1371639.4    1   83 -1962374.6 -1371639.4  30.1%   988 1341s
H   33    31                    -1962374.599 -1460430.5  25.6%   289 1344s
    50    45 -1578263.5   13   33 -1962374.6 -1460430.5  25.6%   233 1345s
*   87    64              19    -1817703.893 -1460430.5  19.7%   161 1346s
   264   149 -1636208.7   64   41 -1817703.9 -1499375.2  17.5%   140 1350s
H  300   165                    -1798610.453 -1499375.2  16.6%   129 1350s
*  342   179              10    -1759617.977 -1509512.8  14.2%   128 1351s
H  496   223                    -1750336.984 -1518502.5  13.2%   122 1353s
*  498   223              16    -1750336.978 -1518502.5  13.2%   122 1353s
   542   222 -1608469.0    7   55 -1750337.0 -1529034.0  12.6%   125 1355s
H  566   224                    -1750336.978 -1533516.7  12.4%   132 1355s
H  654   228                    -1750336.978 -1541313.4  11.9%   137 1357s
H  743   237                    -1750336.977 -1549782.9  11.5%   136 1359s
   798   241 -1617604.1   25   92 -1750337.0 -1552875.8  11.3%   136 1360s
H  972   255                    -1750336.976 -1569729.5  10.3%   137 1364s
   976   238 -1685028.6   13   56 -1750337.0 -1572318.2  10.2%   138 1365s
  1411   186 -1680874.9   20   32 -1750337.0 -1609862.6  8.03%   139 1370s
  1686    39     cutoff   16      -1750337.0 -1660609.1  5.13%   140 1375s

Cutting planes:
  Cover: 1
  Implied bound: 5
  Clique: 14
  MIR: 19
  Flow cover: 11
  Zero half: 2

Explored 1831 nodes (274240 simplex iterations) in 1376.28 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75034e+06 -1.75034e+06 ... -1.96237e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (3.4981e-06) exceeds tolerance
Warning: max bound violation (3.4981e-06) exceeds tolerance
Best objective -1.750336976459e+06, best bound -1.750336976459e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,376.31 (1376.31) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9765 (-1750336.9765) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	25m8.769s
user	90m2.807s
sys	2m14.287s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
