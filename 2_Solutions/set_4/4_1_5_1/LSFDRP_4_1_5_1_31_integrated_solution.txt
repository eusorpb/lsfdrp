	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_5_1_fr.p
Instance LSFDP: LSFDRP_4_1_5_1_fd_31.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_5_1_fd_31 with Gurobi
***
*** Graph info
*** |S|: 19
*** |S^E|: 0
*** |S^C|: 1
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 101510 rows, 2039423 columns and 7520681 nonzeros
Variable types: 1865124 continuous, 174299 integer (174299 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10421 rows and 28357 columns (presolve time = 5s) ...
Presolve removed 11661 rows and 28464 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 11663 rows and 142851 columns (presolve time = 699s) ...
Presolve removed 17661 rows and 142958 columns (presolve time = 702s) ...
Presolve removed 17665 rows and 162186 columns (presolve time = 705s) ...
Presolve removed 23828 rows and 163903 columns (presolve time = 711s) ...
Presolve removed 24553 rows and 195859 columns (presolve time = 719s) ...
Presolve removed 24740 rows and 197485 columns (presolve time = 1405s) ...
Presolve removed 24740 rows and 197485 columns
Presolve time: 1404.56s
Presolved: 76770 rows, 1841938 columns, 6639105 nonzeros
Variable types: 1798903 continuous, 43035 integer (42987 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    4.3537322e+02   8.000000e+00   3.709996e+08   1427s
Concurrent spin time: 0.00s

Solved with dual simplex

Root relaxation: objective 8.190075e+05, 5076 iterations, 1.23 seconds
Total elapsed time = 1427.55s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -819007.53    0   74          - -819007.53      -     - 1428s
H    0     0                    -1.10648e+07 -819007.53  92.6%     - 1428s
     0     0 -1183625.0    0   55 -1.106e+07 -1183625.0  89.3%     - 1432s
H    0     0                    -2805887.714 -1183625.0  57.8%     - 1432s
     0     0 -1241266.5    0   70 -2805887.7 -1241266.5  55.8%     - 1435s
H    0     0                    -2732140.931 -1241266.5  54.6%     - 1436s
     0     0 -1243698.4    0   72 -2732140.9 -1243698.4  54.5%     - 1436s
     0     0 -1244071.6    0   75 -2732140.9 -1244071.6  54.5%     - 1437s
     0     0 -1244071.6    0   76 -2732140.9 -1244071.6  54.5%     - 1437s
     0     0 -1247559.9    0   80 -2732140.9 -1247559.9  54.3%     - 1440s
H    0     0                    -2730593.902 -1247559.9  54.3%     - 1442s
H    0     0                    -2411292.342 -1247559.9  48.3%     - 1443s
     0     0 -1247559.9    0   67 -2411292.3 -1247559.9  48.3%     - 1447s
     0     0 -1247559.9    0   74 -2411292.3 -1247559.9  48.3%     - 1449s
     0     0 -1254918.9    0   77 -2411292.3 -1254918.9  48.0%     - 1450s
     0     0 -1255101.7    0   79 -2411292.3 -1255101.7  47.9%     - 1450s
H    0     0                    -2117101.955 -1255101.7  40.7%     - 1451s
     0     0 -1263955.4    0   87 -2117102.0 -1263955.4  40.3%     - 1451s
     0     0 -1263955.4    0   86 -2117102.0 -1263955.4  40.3%     - 1451s
     0     0 -1264721.8    0   85 -2117102.0 -1264721.8  40.3%     - 1452s
H    0     0                    -2117101.954 -1264721.8  40.3%     - 1452s
     0     0 -1264721.8    0   86 -2117102.0 -1264721.8  40.3%     - 1454s
     0     0 -1324933.0    0   80 -2117102.0 -1324933.0  37.4%     - 1456s
     0     0 -1329591.0    0   82 -2117102.0 -1329591.0  37.2%     - 1457s
     0     0 -1329591.0    0   82 -2117102.0 -1329591.0  37.2%     - 1457s
     0     0 -1352113.2    0   78 -2117102.0 -1352113.2  36.1%     - 1458s
H    0     0                    -2081472.303 -1352113.2  35.0%     - 1459s
H    0     0                    -1995502.681 -1352113.2  32.2%     - 1459s
     0     0 -1377967.8    0   81 -1995502.7 -1377967.8  30.9%     - 1459s
     0     0 -1390101.6    0   77 -1995502.7 -1390101.6  30.3%     - 1459s
     0     0 -1390690.0    0   76 -1995502.7 -1390690.0  30.3%     - 1459s
     0     0 -1396770.3    0   81 -1995502.7 -1396770.3  30.0%     - 1461s
     0     0 -1396770.3    0   75 -1995502.7 -1396770.3  30.0%     - 1464s
     0     0 -1396770.3    0   80 -1995502.7 -1396770.3  30.0%     - 1465s
     0     0 -1396770.3    0  126 -1995502.7 -1396770.3  30.0%     - 1466s
     0     0 -1396770.3    0   82 -1995502.7 -1396770.3  30.0%     - 1466s
     0     0 -1405711.9    0   79 -1995502.7 -1405711.9  29.6%     - 1467s
     0     0 -1406441.7    0   65 -1995502.7 -1406441.7  29.5%     - 1467s
     0     0 -1407620.3    0   85 -1995502.7 -1407620.3  29.5%     - 1467s
     0     0 -1408380.2    0   73 -1995502.7 -1408380.2  29.4%     - 1467s
     0     0 -1408380.2    0   73 -1995502.7 -1408380.2  29.4%     - 1467s
     0     0 -1409053.4    0   78 -1995502.7 -1409053.4  29.4%     - 1469s
     0     2 -1409053.4    0   78 -1995502.7 -1409053.4  29.4%     - 1470s
    19    15 -1581996.5    6   52 -1995502.7 -1519509.9  23.9%   437 1475s
*   89    39              18    -1821789.861 -1537806.5  15.6%   306 1479s
   110    31 -1784075.8   23   27 -1821789.9 -1537806.5  15.6%   284 1480s
*  145    35              17    -1804847.982 -1565123.9  13.3%   237 1481s
*  230    27              14    -1759617.981 -1653217.1  6.05%   189 1482s
*  246    15              14    -1750336.981 -1656139.0  5.38%   183 1483s

Cutting planes:
  Cover: 1
  Implied bound: 2
  Clique: 21
  MIR: 19
  Flow cover: 6
  Zero half: 1

Explored 307 nodes (80947 simplex iterations) in 1483.42 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75962e+06 -1.80485e+06 ... -2.73059e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (4.0602e-06) exceeds tolerance
Warning: max bound violation (3.2652e-06) exceeds tolerance
Best objective -1.750336980507e+06, best bound -1.750336980507e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,483.45 (1483.45) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9805 (-1750336.9805) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18, 22]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	27m8.730s
user	95m26.385s
sys	3m58.302s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
