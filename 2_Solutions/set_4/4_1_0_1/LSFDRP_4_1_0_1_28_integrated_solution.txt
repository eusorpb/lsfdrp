	OpenBLAS 0.2.6 environment established
		compiled with gcc for max. 16 cores
	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_0_1_fr.p
Instance LSFDP: LSFDRP_4_1_0_1_fd_28.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_0_1_fd_28 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter NumericFocus to 2
   Prev: 0  Min: 0  Max: 3  Default: 0
Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 100812 rows, 2030127 columns and 7450262 nonzeros
Variable types: 1865001 continuous, 165126 integer (165126 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 10332 rows and 24728 columns (presolve time = 5s) ...
Presolve removed 11458 rows and 28633 columns (presolve time = 10s) ...
Presolve removed 11707 rows and 31756 columns (presolve time = 15s) ...
Presolve removed 11707 rows and 138838 columns (presolve time = 591s) ...
Presolve removed 17318 rows and 138899 columns (presolve time = 595s) ...
Presolve removed 23366 rows and 172654 columns (presolve time = 600s) ...
Presolve removed 24537 rows and 199482 columns (presolve time = 608s) ...
Presolve removed 24537 rows and 199483 columns (presolve time = 1192s) ...
Presolve removed 24537 rows and 199483 columns
Presolve time: 1191.97s
Presolved: 76275 rows, 1830644 columns, 6522954 nonzeros
Variable types: 1787650 continuous, 42994 integer (42948 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    4.9711157e+02   8.000000e+00   3.720029e+08   1213s
Concurrent spin time: 0.24s

Solved with dual simplex

Root relaxation: objective 8.019724e+05, 6204 iterations, 1.89 seconds
Total elapsed time = 1214.17s

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -831859.69    0   61          - -831859.69      -     - 1215s
H    0     0                    -3126866.714 -831859.69  73.4%     - 1215s
H    0     0                    -2145948.219 -831859.69  61.2%     - 1216s
     0     0 -1153927.7    0   69 -2145948.2 -1153927.7  46.2%     - 1219s
     0     0 -1153927.7    0   75 -2145948.2 -1153927.7  46.2%     - 1222s
H    0     0                    -1939766.394 -1153927.7  40.5%     - 1223s
     0     0 -1243414.7    0   71 -1939766.4 -1243414.7  35.9%     - 1224s
     0     0 -1249286.8    0   75 -1939766.4 -1249286.8  35.6%     - 1225s
     0     0 -1258824.1    0   81 -1939766.4 -1258824.1  35.1%     - 1226s
     0     0 -1258824.1    0   79 -1939766.4 -1258824.1  35.1%     - 1228s
     0     0 -1307147.7    0   64 -1939766.4 -1307147.7  32.6%     - 1230s
     0     0 -1310248.2    0   78 -1939766.4 -1310248.2  32.5%     - 1230s
     0     0 -1310941.6    0   78 -1939766.4 -1310941.6  32.4%     - 1230s
     0     0 -1311113.6    0   82 -1939766.4 -1311113.6  32.4%     - 1231s
     0     0 -1317961.7    0   84 -1939766.4 -1317961.7  32.1%     - 1231s
     0     0 -1317961.7    0   84 -1939766.4 -1317961.7  32.1%     - 1232s
     0     0 -1319764.0    0   86 -1939766.4 -1319764.0  32.0%     - 1233s
     0     2 -1319764.0    0   86 -1939766.4 -1319764.0  32.0%     - 1234s
     3     2 -1351140.8    2   91 -1939766.4 -1351140.8  30.3%   338 1235s
*   27    13              10    -1858109.970 -1371616.3  26.2%   271 1238s
    72    31 -1538925.3   16   43 -1858110.0 -1434021.2  22.8%   212 1240s
*  118    53              13    -1841374.442 -1434021.2  22.1%   196 1241s
H  144    56                    -1822281.001 -1434021.2  21.3%   186 1243s
   218    86 -1695864.3   40   63 -1822281.0 -1481811.0  18.7%   167 1245s
*  385   149              12    -1767769.972 -1515329.8  14.3%   158 1248s
   466   144 -1642563.5   20   52 -1767770.0 -1566984.0  11.4%   158 1250s
H  810   101                    -1760743.298 -1630248.9  7.41%   159 1254s
H  818   103                    -1750336.980 -1630248.9  6.86%   159 1255s
H  847    78                    -1750336.972 -1634480.7  6.62%   161 1255s
H  864    80                    -1750336.931 -1634480.7  6.62%   160 1255s

Cutting planes:
  Learned: 1
  Cover: 4
  Implied bound: 11
  Clique: 35
  MIR: 15
  Flow cover: 4
  Zero half: 2

Explored 1040 nodes (184433 simplex iterations) in 1257.81 seconds
Thread count was 4 (of 16 available processors)

Solution count 10: -1.75034e+06 -1.75034e+06 -1.75034e+06 ... -2.14595e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (7.7745e-06) exceeds tolerance
Warning: max bound violation (7.7473e-06) exceeds tolerance
Best objective -1.750336930374e+06, best bound -1.750336930374e+06, gap 0.0000%

***
***  |- Calculation finished, took 1,257.86 (1257.86) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9304 (-1750336.9304) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	23m16.932s
user	82m47.330s
sys	1m23.382s
	OpenBLAS 0.2.6 environment removed
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
