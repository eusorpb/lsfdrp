	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_0_1_fr.p
Instance LSFDP: LSFDRP_4_1_0_1_fd_19.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_0_1_fd_19 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 100812 rows, 2030127 columns and 7450262 nonzeros
Variable types: 1865001 continuous, 165126 integer (165126 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 1e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 52189 rows and 1559825 columns (presolve time = 5s) ...
Presolve removed 63365 rows and 1693559 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 63760 rows and 1693953 columns (presolve time = 16s) ...
Presolve removed 63760 rows and 1801036 columns (presolve time = 155s) ...
Presolve removed 90177 rows and 1975734 columns (presolve time = 160s) ...
Presolve removed 90177 rows and 1975734 columns (presolve time = 165s) ...
Presolve removed 90177 rows and 1975734 columns
Presolve time: 165.23s
Presolved: 10635 rows, 54393 columns, 413871 nonzeros
Variable types: 13055 continuous, 41338 integer (41292 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -0.0000000e+00   3.000000e+00   1.037497e+07    167s
Concurrent spin time: 0.31s

Solved with dual simplex

Root relaxation: objective -4.149631e+05, 5041 iterations, 1.74 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -414963.07    0   63          - -414963.07      -     -  168s
H    0     0                    -8149170.204 -414963.07  94.9%     -  169s
H    0     0                    -1894536.398 -414963.07  78.1%     -  170s
     0     0 -1151653.1    0   77 -1894536.4 -1151653.1  39.2%     -  171s
     0     0 -1151653.1    0   73 -1894536.4 -1151653.1  39.2%     -  173s
     0     0 -1287658.0    0   80 -1894536.4 -1287658.0  32.0%     -  174s
     0     0 -1354768.5    0  108 -1894536.4 -1354768.5  28.5%     -  175s
     0     0 -1356819.8    0  103 -1894536.4 -1356819.8  28.4%     -  175s
     0     0 -1356912.2    0  108 -1894536.4 -1356912.2  28.4%     -  175s
     0     0 -1356912.2    0  111 -1894536.4 -1356912.2  28.4%     -  175s
     0     0 -1356925.1    0  115 -1894536.4 -1356925.1  28.4%     -  175s
     0     0 -1356925.1    0   97 -1894536.4 -1356925.1  28.4%     -  177s
     0     0 -1361684.3    0   74 -1894536.4 -1361684.3  28.1%     -  177s
     0     0 -1391819.3    0  110 -1894536.4 -1391819.3  26.5%     -  178s
     0     0 -1394255.2    0  104 -1894536.4 -1394255.2  26.4%     -  178s
     0     0 -1394255.2    0  106 -1894536.4 -1394255.2  26.4%     -  178s
     0     0 -1394857.4    0  100 -1894536.4 -1394857.4  26.4%     -  178s
     0     0 -1419511.7    0   73 -1894536.4 -1419511.7  25.1%     -  179s
     0     0 -1588111.4    0   74 -1894536.4 -1588111.4  16.2%     -  180s
     0     0 -1630673.4    0   73 -1894536.4 -1630673.4  13.9%     -  180s
H    0     0                    -1791525.867 -1630673.4  8.98%     -  180s
     0     0 -1631561.0    0   75 -1791525.9 -1631561.0  8.93%     -  180s
H    0     0                    -1778711.426 -1631561.0  8.27%     -  180s
     0     0 -1631561.0    0   77 -1778711.4 -1631561.0  8.27%     -  180s
     0     0 -1750337.0    0   14 -1778711.4 -1750337.0  1.60%     -  181s
H    0     0                    -1750336.987 -1750337.0  0.00%     -  181s

Explored 1 nodes (21939 simplex iterations) in 182.11 seconds
Thread count was 4 (of 16 available processors)

Solution count 5: -1.75034e+06 -1.77871e+06 -1.79153e+06 ... -8.14917e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986691e+06, best bound -1.750336986691e+06, gap 0.0000%

***
***  |- Calculation finished, took 182.15 (182.15) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9867 (-1750336.9867) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 260, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 260 (PABLB In@1472.0/Out@14
###  |  84.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 259, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 259 (PABLB In@1304.0/Out@1316.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,260] = 1
y[8,260,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,224,259] = 1
y[16,259,0] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 888
phi[15] = 1494
phi[16] = 990

### All variables with value smaller zero:

### End

real	5m19.043s
user	12m51.630s
sys	0m21.440s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
