	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_0_1_fr.p
Instance LSFDP: LSFDRP_4_1_0_1_fd_34.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_0_1_fd_34 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 100812 rows, 2030127 columns and 7450262 nonzeros
Variable types: 1865001 continuous, 165126 integer (165126 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 52189 rows and 1519394 columns (presolve time = 5s) ...
Presolve removed 63372 rows and 1693559 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 63760 rows and 1801036 columns (presolve time = 139s) ...
Presolve removed 88492 rows and 1969949 columns (presolve time = 140s) ...
Presolve removed 90177 rows and 1975734 columns (presolve time = 147s) ...
Presolve removed 90177 rows and 1975734 columns
Presolve time: 147.27s
Presolved: 10635 rows, 54393 columns, 413871 nonzeros
Variable types: 13055 continuous, 41338 integer (41292 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -0.0000000e+00   3.000000e+00   9.729438e+06    149s
   14306   -1.8754500e+05   0.000000e+00   3.994711e+07    150s
Concurrent spin time: 0.05s

Solved with dual simplex

Root relaxation: objective -4.149631e+05, 5485 iterations, 1.68 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -414963.07    0   64          - -414963.07      -     -  150s
H    0     0                    -1.35790e+07 -414963.07  96.9%     -  150s
     0     0 -1151653.1    0   79 -1.358e+07 -1151653.1  91.5%     -  153s
H    0     0                    -2373359.008 -1151653.1  51.5%     -  153s
     0     0 -1154349.3    0   76 -2373359.0 -1154349.3  51.4%     -  154s
     0     0 -1255331.6    0   96 -2373359.0 -1255331.6  47.1%     -  155s
     0     0 -1255331.6    0   92 -2373359.0 -1255331.6  47.1%     -  157s
H    0     0                    -1939766.398 -1255331.6  35.3%     -  158s
     0     0 -1312079.9    0   69 -1939766.4 -1312079.9  32.4%     -  159s
     0     0 -1312079.9    0   86 -1939766.4 -1312079.9  32.4%     -  160s
     0     0 -1381916.1    0   89 -1939766.4 -1381916.1  28.8%     -  161s
     0     0 -1389226.8    0   82 -1939766.4 -1389226.8  28.4%     -  161s
     0     0 -1389226.8    0   82 -1939766.4 -1389226.8  28.4%     -  161s
     0     0 -1413158.4    0   92 -1939766.4 -1413158.4  27.1%     -  161s
     0     0 -1413237.0    0   93 -1939766.4 -1413237.0  27.1%     -  162s
     0     0 -1413237.0    0   94 -1939766.4 -1413237.0  27.1%     -  162s
     0     0 -1416454.4    0   94 -1939766.4 -1416454.4  27.0%     -  162s
     0     0 -1420045.4    0  104 -1939766.4 -1420045.4  26.8%     -  162s
     0     0 -1420045.4    0  104 -1939766.4 -1420045.4  26.8%     -  162s
     0     0 -1431295.5    0  104 -1939766.4 -1431295.5  26.2%     -  162s
     0     0 -1433936.2    0   97 -1939766.4 -1433936.2  26.1%     -  162s
     0     0 -1434919.2    0  106 -1939766.4 -1434919.2  26.0%     -  162s
     0     0 -1435016.0    0  109 -1939766.4 -1435016.0  26.0%     -  162s
     0     0 -1452280.6    0  119 -1939766.4 -1452280.6  25.1%     -  163s
     0     0 -1452280.6    0   73 -1939766.4 -1452280.6  25.1%     -  164s
     0     0 -1452280.6    0   62 -1939766.4 -1452280.6  25.1%     -  164s
     0     0 -1490084.3    0   82 -1939766.4 -1490084.3  23.2%     -  165s
     0     0 -1493682.6    0   72 -1939766.4 -1493682.6  23.0%     -  165s
     0     0 -1494030.6    0   72 -1939766.4 -1494030.6  23.0%     -  165s
     0     0 -1494354.1    0   74 -1939766.4 -1494354.1  23.0%     -  165s
H    0     0                    -1916446.667 -1494354.1  22.0%     -  165s
     0     0 -1502620.2    0   99 -1916446.7 -1502620.2  21.6%     -  165s
H    0     0                    -1769430.427 -1502620.2  15.1%     -  165s
     0     0 -1506556.0    0  106 -1769430.4 -1506556.0  14.9%     -  165s
     0     0 -1506913.1    0  111 -1769430.4 -1506913.1  14.8%     -  165s
     0     0 -1507742.1    0   79 -1769430.4 -1507742.1  14.8%     -  165s
     0     0 -1704710.5    0   46 -1769430.4 -1704710.5  3.66%     -  167s
H    0     0                    -1750336.987 -1704710.5  2.61%     -  167s
     0     0     cutoff    0      -1750337.0 -1750337.0  0.00%     -  167s

Explored 1 nodes (28203 simplex iterations) in 167.67 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -1.75034e+06 -1.76943e+06 -1.91645e+06 ... -1.3579e+07

Optimal solution found (tolerance 1.00e-04)
Best objective -1.750336986698e+06, best bound -1.750336986698e+06, gap 0.0000%

***
***  |- Calculation finished, took 167.7 (167.7) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9867 (-1750336.9867) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	5m22.098s
user	12m18.220s
sys	0m17.423s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
