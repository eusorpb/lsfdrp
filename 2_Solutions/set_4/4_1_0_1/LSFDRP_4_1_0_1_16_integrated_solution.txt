	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_0_1_fr.p
Instance LSFDP: LSFDRP_4_1_0_1_fd_16.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_0_1_fd_16 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 100812 rows, 2030127 columns and 7450262 nonzeros
Variable types: 1865001 continuous, 165126 integer (165126 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 9e+06]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 57950 rows and 1670679 columns (presolve time = 5s) ...
Presolve removed 63372 rows and 1693567 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 63760 rows and 1801036 columns (presolve time = 160s) ...
Presolve removed 90177 rows and 1975734 columns (presolve time = 170s) ...
Presolve removed 90177 rows and 1975734 columns
Presolve time: 169.61s
Presolved: 10635 rows, 54393 columns, 413871 nonzeros
Variable types: 13055 continuous, 41338 integer (41292 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -0.0000000e+00   3.000000e+00   1.064846e+07    171s
Concurrent spin time: 0.20s

Solved with dual simplex

Root relaxation: objective -4.149631e+05, 5034 iterations, 1.66 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -414963.07    0   63          - -414963.07      -     -  172s
H    0     0                    -7063209.771 -414963.07  94.1%     -  173s
H    0     0                    -1867922.427 -414963.07  77.8%     -  173s
     0     0 -1151653.1    0   79 -1867922.4 -1151653.1  38.3%     -  175s
     0     0 -1151653.1    0   86 -1867922.4 -1151653.1  38.3%     -  177s
     0     0 -1292761.8    0   66 -1867922.4 -1292761.8  30.8%     -  178s
     0     0 -1317345.4    0   67 -1867922.4 -1317345.4  29.5%     -  178s
     0     0 -1317610.8    0   67 -1867922.4 -1317610.8  29.5%     -  178s
     0     0 -1358114.0    0   90 -1867922.4 -1358114.0  27.3%     -  179s
     0     0 -1396512.1    0   76 -1867922.4 -1396512.1  25.2%     -  179s
     0     0 -1396512.1    0   75 -1867922.4 -1396512.1  25.2%     -  179s
     0     0 -1404338.1    0   85 -1867922.4 -1404338.1  24.8%     -  179s
     0     0 -1404338.1    0   88 -1867922.4 -1404338.1  24.8%     -  181s
     0     0 -1404338.1    0  127 -1867922.4 -1404338.1  24.8%     -  181s
     0     0 -1443193.0    0  115 -1867922.4 -1443193.0  22.7%     -  181s
     0     0 -1444079.3    0  126 -1867922.4 -1444079.3  22.7%     -  181s
     0     0 -1470135.7    0   92 -1867922.4 -1470135.7  21.3%     -  182s
     0     0 -1485471.1    0  143 -1867922.4 -1485471.1  20.5%     -  182s
     0     0 -1490572.2    0  141 -1867922.4 -1490572.2  20.2%     -  182s
     0     0 -1490856.7    0  135 -1867922.4 -1490856.7  20.2%     -  182s
     0     0 -1506510.9    0  115 -1867922.4 -1506510.9  19.3%     -  182s
     0     0 -1507806.7    0   98 -1867922.4 -1507806.7  19.3%     -  183s
H    0     0                    -1867922.426 -1507806.7  19.3%     -  183s
H    0     0                    -1830518.337 -1507806.7  17.6%     -  184s
     0     0 -1624930.6    0   83 -1830518.3 -1624930.6  11.2%     -  184s
H    0     0                    -1760743.306 -1624930.6  7.71%     -  184s
H    0     0                    -1750336.987 -1624930.6  7.16%     -  184s
     0     0 -1630777.9    0   75 -1750337.0 -1630777.9  6.83%     -  184s
     0     0 -1646670.6    0   68 -1750337.0 -1646670.6  5.92%     -  184s
     0     0     cutoff    0      -1750337.0 -1750337.0  0.00%     -  185s

Explored 1 nodes (24069 simplex iterations) in 185.17 seconds
Thread count was 4 (of 16 available processors)

Solution count 6: -1.75034e+06 -1.76074e+06 -1.83052e+06 ... -7.06321e+06
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (3.1628e-06) exceeds tolerance
Warning: max bound violation (1.2131e-06) exceeds tolerance
Best objective -1.750336986546e+06, best bound -1.750336986546e+06, gap 0.0000%

***
***  |- Calculation finished, took 185.2 (185.2) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9865 (-1750336.9865) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1398.71/Out@1400.71) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1398.71
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1400.71
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	5m18.350s
user	13m8.183s
sys	0m20.005s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
