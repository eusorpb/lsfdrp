	Freetype 2.4.12 environment established
		library to render fonts
	Python 3.6.0 environment established
	Gurobi 8.1.0 environment established
Instance LSFRP: LSFDRP_4_1_0_1_fr.p
Instance LSFDP: LSFDRP_4_1_0_1_fd_40.p

*****************************************************************************************************************************
*** Solve LSFDRP_4_1_0_1_fd_40 with Gurobi
***
*** Graph info
*** |S|: 18
*** |S^E|: 0
*** |S^C|: 0
*** |V|: 338
*** |A|: 9173
*** |A^i|: 9051
*** |A^f|: 122
*** |M|: 201
*** |E|: 27

Changed value of parameter TimeLimit to 86400.0
   Prev: 1e+100  Min: 0.0  Max: 1e+100  Default: 1e+100
Changed value of parameter Threads to 4
   Prev: 0  Min: 0  Max: 1024  Default: 0
Optimize a model with 100812 rows, 2030127 columns and 7450262 nonzeros
Variable types: 1865001 continuous, 165126 integer (165126 binary)
Coefficient statistics:
  Matrix range     [1e+00, 5e+03]
  Objective range  [2e+00, 2e+07]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 5e+03]
Presolve removed 55735 rows and 1616168 columns (presolve time = 5s) ...
Presolve removed 63372 rows and 1693559 columns (presolve time = 10s) ...
Warning: Failed to open log file 'gurobi.log'
Presolve removed 63760 rows and 1801036 columns (presolve time = 144s) ...
Presolve removed 88661 rows and 1970566 columns (presolve time = 145s) ...
Presolve removed 90177 rows and 1975734 columns (presolve time = 152s) ...
Presolve removed 90177 rows and 1975734 columns
Presolve time: 152.29s
Presolved: 10635 rows, 54393 columns, 413871 nonzeros
Variable types: 13055 continuous, 41338 integer (41292 binary)

Deterministic concurrent LP optimizer: primal and dual simplex
Showing first log only...


Root simplex log...

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0   -0.0000000e+00   3.000000e+00   9.607187e+06    154s
   11455   -5.1530110e+05   0.000000e+00   1.283871e+08    155s
Concurrent spin time: 0.01s

Solved with dual simplex

Root relaxation: objective -4.149631e+05, 4943 iterations, 1.64 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0 -414963.07    0   64          - -414963.07      -     -  155s
H    0     0                    -1.57509e+07 -414963.07  97.4%     -  155s
H    0     0                    -1894536.398 -414963.07  78.1%     -  156s
     0     0 -1151653.1    0   81 -1894536.4 -1151653.1  39.2%     -  158s
     0     0 -1416265.7    0   63 -1894536.4 -1416265.7  25.2%     -  163s
     0     0 -1696043.0    0   63 -1894536.4 -1696043.0  10.5%     -  164s
*    0     0               0    -1750336.970 -1750337.0  0.00%     -  165s

Explored 1 nodes (10662 simplex iterations) in 165.79 seconds
Thread count was 4 (of 16 available processors)

Solution count 3: -1.75034e+06 -1.89454e+06 -1.57509e+07 
No other solutions better than -1.75034e+06

Optimal solution found (tolerance 1.00e-04)
Warning: max constraint violation (5.7259e-06) exceeds tolerance
Warning: max bound violation (7.0116e-06) exceeds tolerance
Best objective -1.750336968287e+06, best bound -1.750336968287e+06, gap 0.0000%

***
***  |- Calculation finished, took 165.82 (165.82) sec.
***  |-----------------------------------------------------------------------------------------------------------------------
***
*****************************************************************************************************************************
#############################################################################################################################
###
### Results
###
### The objective of the solution is -1,750,336.9683 (-1750336.9683) and is feasible with a Gap of 0.0
###
### Service 177 (WCSA) is operated by 3 vessels from type 19 (PMax28)
###  |- Vessel  8 has the path [247, 155, 226, 259, 0]
###  |  247 (NLRTM Out@596.0) - 155 (FRLEH In@615.0/Out@631.0) - 226 (MAPTM In@711.0/Out@732.0) - 259 (PABLB In@1304.0/Out@13
###  |  16.0) - 0 (DUMMY_END Ziel-Service 177)
###  |- Vessel 15 has the path [151, 222, 128, 36, 295, 261, 0]
###  |  151 (FRLEH Out@158.0) - 222 (MAPTM In@238.0/Out@259.0) - 128 (ESALG In@284.0/Out@296.0) - 36 (CLLQN In@1240.0/Out@125
###  |  8.0) - 295 (PECLL In@1512.12/Out@1514.12) - 261 (PABLB In@1640.0/Out@1652.0) - 0 (DUMMY_END Ziel-Service 177)
###  |   |- and carried Demand  50,  580.0 containers from 222 to 128
###  |   |- and carried Demand 156,  194.0 containers from  36 to 261
###  |   |- and carried Demand 157,   20.0 containers from  36 to 261
###  |   |- and carried Demand 159,    5.0 containers from  36 to 261
###  |- Vessel 16 has the path [153, 224, 260, 0]
###  |  153 (FRLEH Out@326.0) - 224 (MAPTM In@406.0/Out@427.0) - 260 (PABLB In@1472.0/Out@1484.0) - 0 (DUMMY_END Ziel-Service
###  |   177)
###  |
### Vessels not used in the solution: [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 17, 18]
###  |-----------------------------------------------------------------------------------------------------------------------
###
#############################################################################################################################

### Detialed variable overview

### All variables with value greater zero:
rho[177,19] = 1
eta[177,19,1] = 1
eta[177,19,2] = 1
eta[177,19,3] = 1
y[8,155,226] = 1
y[8,226,259] = 1
y[8,259,0] = 1
y[8,247,155] = 1
y[15,222,128] = 1
y[15,295,261] = 1
y[15,151,222] = 1
y[15,261,0] = 1
y[15,36,295] = 1
y[15,128,36] = 1
y[16,153,224] = 1
y[16,260,0] = 1
y[16,224,260] = 1
xD[50,222,128] = 580
xD[156,295,261] = 194
xD[156,36,295] = 194
xD[157,295,261] = 20
xD[157,36,295] = 20
xD[159,295,261] = 5
xD[159,36,295] = 5
zE[36] = 1240
zE[128] = 284
zE[155] = 615
zE[222] = 238
zE[224] = 406
zE[226] = 711
zE[259] = 1304
zE[260] = 1472
zE[261] = 1640
zE[295] = 1512.12
zX[36] = 1258
zX[128] = 296
zX[151] = 158
zX[153] = 326
zX[155] = 631
zX[222] = 259
zX[224] = 427
zX[226] = 732
zX[247] = 596
zX[259] = 1316
zX[260] = 1484
zX[261] = 1652
zX[295] = 1514.12
w[15,(36,295)] = 140.711
w[15,(295,261)] = 125.884
phi[8] = 720
phi[15] = 1494
phi[16] = 1158

### All variables with value smaller zero:

### End

real	5m9.934s
user	11m56.904s
sys	0m21.177s
	Freetype 2.4.12 environment removed
	Python 3.6.0 environment removed
	Gurobi 8.1.0 environment removed
