### Additional Information to the goal services
# Basic costs on route 121: -228250151.26853302
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 82826436.57442367
# (121, 18, 2): 71945322.97442368
# (121, 19, 1): 61744834.4645268
# (121, 19, 2): 48749684.415712506
# (121, 30, 1): 14518496.953269988
# (121, 30, 2): 0.0
# -------------------------------------------
