### Additional Information to the goal services
# Basic costs on route 121: -262487673.95881295
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 95250402.0605872
# (121, 18, 2): 82737121.42058721
# (121, 19, 1): 71006559.63420582
# (121, 19, 2): 56062137.07806933
# (121, 30, 1): 16696271.496260494
# (121, 30, 2): 0.0
# -------------------------------------------
