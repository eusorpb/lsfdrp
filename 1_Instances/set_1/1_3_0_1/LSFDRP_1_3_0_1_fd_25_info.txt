### Additional Information to the goal services
# Basic costs on route 121: -142656344.54283315
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 51766522.85901481
# (121, 18, 2): 44965826.859014824
# (121, 19, 1): 38590521.54032928
# (121, 19, 2): 30468552.759820312
# (121, 30, 1): 9074060.595793754
# (121, 30, 2): 0.0
# -------------------------------------------
