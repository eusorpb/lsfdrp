### Additional Information to the goal services
# Basic costs on route 121: -296725196.649093
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 107674367.54675084
# (121, 18, 2): 93528919.86675084
# (121, 19, 1): 80268284.80388492
# (121, 19, 2): 63374589.74042627
# (121, 30, 1): 18874046.03925103
# (121, 30, 2): 0.0
# -------------------------------------------
