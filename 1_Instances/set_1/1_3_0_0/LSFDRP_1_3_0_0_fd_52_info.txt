### Additional Information to the goal services
# Basic costs on route 121: 19640961.789663397
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 15949471.951264836
# (121, 18, 2): 1804024.2712648362
# (121, 19, 1): 18348161.50904032
# (121, 19, 2): 1454466.4455816932
# (121, 30, 1): 18874046.039251007
# (121, 30, 2): 0.0
# -------------------------------------------
