### Additional Information to the goal services
# Basic costs on route 121: 18507829.378721282
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 15029310.107922632
# (121, 18, 2): 1699945.9479226284
# (121, 19, 1): 17289613.729672603
# (121, 19, 2): 1370554.9198750556
# (121, 30, 1): 17785158.767755747
# (121, 30, 2): 0.0
# -------------------------------------------
