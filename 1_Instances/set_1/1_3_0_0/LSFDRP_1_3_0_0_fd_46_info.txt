### Additional Information to the goal services
# Basic costs on route 121: 17374696.967779152
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 14109148.264580444
# (121, 18, 2): 1595867.624580443
# (121, 19, 1): 16231065.95030491
# (121, 19, 2): 1286643.3941684254
# (121, 30, 1): 16696271.496260509
# (121, 30, 2): 0.0
# -------------------------------------------
