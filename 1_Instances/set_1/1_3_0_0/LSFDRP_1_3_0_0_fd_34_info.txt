### Additional Information to the goal services
# Basic costs on route 121: 12842167.324010685
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 10428500.891211625
# (121, 18, 2): 1179554.3312116247
# (121, 19, 1): 11996874.832834054
# (121, 19, 2): 950997.2913418729
# (121, 30, 1): 12340722.410279501
# (121, 30, 2): 0.0
# -------------------------------------------
