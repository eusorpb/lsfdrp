### Additional Information to the goal services
# Basic costs on route 121: 10575902.502126448
# Direct costs (not staggered):
# For goal service 121:
# n_min: 1
# n_max: 2
# lowest costs of the goal service 121 with configuration (121, 30, 2) at 0.0
# (121, 18, 1): 8588177.204527214
# (121, 18, 2): 971397.6845272146
# (121, 19, 1): 9879779.274098635
# (121, 19, 2): 783174.2399286032
# (121, 30, 1): 10162947.867289
# (121, 30, 2): 0.0
# -------------------------------------------
