### Additional Information to the goal services
# Basic costs on route 24: 9049153.889154103
# Direct costs (not staggered):
# For goal service 24:
# n_min: 2
# n_max: 4
# lowest costs of the goal service 24 with configuration (24, 18, 3) at 0.0
# (24, 18, 1): 8993160.517257724
# (24, 18, 2): 4951657.791435869
# (24, 18, 3): 0.0
# (24, 18, 4): 372410.64000000246
# (24, 19, 1): 13889247.374869008
# (24, 19, 2): 8087507.56700686
# (24, 19, 3): 4231100.325081481
# (24, 19, 4): 4640729.68508148
# (24, 30, 1): 13606992.706361603
# (24, 30, 2): 8653245.526150223
# (24, 30, 3): 3999059.0108637977
# (24, 30, 4): 4445939.010863801
# -------------------------------------------
