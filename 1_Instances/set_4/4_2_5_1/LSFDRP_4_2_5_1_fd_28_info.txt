### Additional Information to the goal services
# Basic costs on route 24: 13335595.205069203
# Direct costs (not staggered):
# For goal service 24:
# n_min: 2
# n_max: 4
# lowest costs of the goal service 24 with configuration (24, 18, 3) at 0.0
# (24, 18, 1): 13253078.657011382
# (24, 18, 2): 7297179.9031686485
# (24, 18, 3): 0.0
# (24, 18, 4): 548815.6800000034
# (24, 19, 1): 20468364.552438553
# (24, 19, 2): 11918432.204010122
# (24, 19, 3): 6235305.742225349
# (24, 19, 4): 6838970.062225349
# (24, 30, 1): 20052410.30411184
# (24, 30, 2): 12752151.301695064
# (24, 30, 3): 5893350.121272966
# (24, 30, 4): 6551910.121272966
# -------------------------------------------
