﻿# Function: Show information to LSFDRP instance
# Author: Daniel Wetzel
#
# Input: fr and fd file

# Used modules
import argparse
import os
import pickle


def show_instance(fr, fd):  

    # Data types:
    # Sets: frRoutes, frK, Ships, ShipsE, ShipsC, A, A_wSink, A_inflex, A_flex, V, V_wSink, V_inflex, V_flex, Q, Routes, K
    # Dictionaries: cHotel, M, RPO, u_rf, u_dc, vStart, p, tau, delta, cMV, cPort, cSail, cVarSail, rDemand, rEqp, amount, V_ePlus, V_eMinus, V_ePlusMinus, tMV, tE, tX, tP, d_min, d_max, unlocode, cCharter, rCharter, n_min, n_max, cRoute, K_notOnR

    # Read instance
    frRoutes, frK, Ships, ShipsE, ShipsC, cHotel, A, A_wSink, A_inflex, A_flex, V, V_wSink, V_inflex, V_flex, Q, M, RPO, u_rf, u_dc, vStart, p, tau, delta, cMV, cPort, cSail, cVarSail, rDemand, rEqp, amount, V_ePlus, V_eMinus, V_ePlusMinus, tMV, tE, tX, tP, d_min, d_max, unlocode = pickle.load( open( fr, "rb" ) )
    
    Routes, K, cCharter, rCharter, n_min, n_max, cRoute, K_notOnR, filename= pickle.load( open( fd, "rb" ) )

    if frRoutes != Routes or frK != K:
        print("Data for routes or fessel classes does not match!")
        print("From fr:\n {0}\n {1}".format(frRoutes, frK))
        print("From fd:\n {0}\n {1}".format(Routes, K))
    else:

        print("\n### Instance")
        print("###", filename)
        
        print("\n### Graph info")
        lenEqp = len(V_ePlusMinus['dc']) + len(V_ePlusMinus['rf'])
        print(f"### |S|: {len(Ships)}, {Ships}")
        print(f"### |S^E|: {len(ShipsE)}, {ShipsE}")
        print(f"### |S^C|: {len(ShipsC)}, {ShipsC}")
        print("### |V|: %s" % len(V))
        print("### |A|: %s" % len(A))
        print("### |A^i|: %s" % len(A_inflex))
        print("### |A^f|: %s" % len(A_flex))
        print("### |M|: %s" % len(M))
        print("### |E|: %s" % lenEqp)


if __name__ == "__main__":
    args = None
    parser = argparse.ArgumentParser(description='LSFDRP used in the publication to LSFDRP')
    parser.add_argument('-fr', '--fleetrepositioning', type=str, help='Path to the lsfrp-instance', required=True)
    parser.add_argument('-fd', '--fleetdeployment', type=str, help='Path to one lsfdp-configuration for the given lsfrp-instnace', required=True)
    args = parser.parse_args()

    if not os.path.exists(args.fleetrepositioning):
        print("Instance LSFRP not found: {0}".format(args.fleetrepositioning), file=sys.stderr)
        sys.exit(1)
    if not os.path.exists(args.fleetdeployment):
        print("Instance LSFDP not found: {0}".format(args.fleetdeployment), file=sys.stderr)
        sys.exit(1)
        
    # Note: error handling omitted; add your own if desired.
    instance = args.fleetrepositioning
    data = args.fleetdeployment
    print("Instance LSFRP: {0}".format(instance))
    print("Instance LSFDP: {0}\n".format(data))
    show_instance(instance, data)
