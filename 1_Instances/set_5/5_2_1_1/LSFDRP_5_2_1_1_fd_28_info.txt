### Additional Information to the goal services
# Basic costs on route 43: 48524306.58660035
# Direct costs (not staggered):
# For goal service 43:
# n_min: 5
# n_max: 8
# lowest costs of the goal service 43 with configuration (43, 18, 8) at 0.0
# (43, 18, 1): 26932166.615928814
# (43, 18, 2): 27480982.29592879
# (43, 18, 3): 28029797.975928828
# (43, 18, 4): 28578613.655928805
# (43, 18, 5): 22850338.560000017
# (43, 18, 6): 15233559.040000014
# (43, 18, 7): 7616779.520000003
# (43, 18, 8): 0.0
# -------------------------------------------
