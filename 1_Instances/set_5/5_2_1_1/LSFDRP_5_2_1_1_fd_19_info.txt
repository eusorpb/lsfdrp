### Additional Information to the goal services
# Basic costs on route 43: 32927208.040907387
# Direct costs (not staggered):
# For goal service 43:
# n_min: 5
# n_max: 8
# lowest costs of the goal service 43 with configuration (43, 18, 8) at 0.0
# (43, 18, 1): 18275398.775094543
# (43, 18, 2): 18647809.415094543
# (43, 18, 3): 19020220.05509453
# (43, 18, 4): 19392630.695094544
# (43, 18, 5): 15505586.879999999
# (43, 18, 6): 10337057.919999983
# (43, 18, 7): 5168528.95999999
# (43, 18, 8): 0.0
# -------------------------------------------
