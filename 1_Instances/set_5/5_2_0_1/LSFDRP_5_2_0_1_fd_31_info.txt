### Additional Information to the goal services
# Basic costs on route 43: 53723339.43516468
# Direct costs (not staggered):
# For goal service 43:
# n_min: 5
# n_max: 8
# lowest costs of the goal service 43 with configuration (43, 18, 8) at 0.0
# (43, 18, 1): 29817755.896206878
# (43, 18, 2): 30425373.256206878
# (43, 18, 3): 31032990.616206877
# (43, 18, 4): 31640607.976206876
# (43, 18, 5): 25298589.119999982
# (43, 18, 6): 16865726.080000006
# (43, 18, 7): 8432863.039999977
# (43, 18, 8): 0.0
# -------------------------------------------
