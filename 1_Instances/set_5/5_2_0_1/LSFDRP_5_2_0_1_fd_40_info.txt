### Additional Information to the goal services
# Basic costs on route 43: 69320437.98085764
# Direct costs (not staggered):
# For goal service 43:
# n_min: 5
# n_max: 8
# lowest costs of the goal service 43 with configuration (43, 18, 8) at 0.0
# (43, 18, 1): 38474523.737041146
# (43, 18, 2): 39258546.13704114
# (43, 18, 3): 40042568.53704113
# (43, 18, 4): 40826590.93704113
# (43, 18, 5): 32643340.800000012
# (43, 18, 6): 21762227.200000003
# (43, 18, 7): 10881113.600000009
# (43, 18, 8): 0.0
# -------------------------------------------
