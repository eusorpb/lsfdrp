### Additional Information to the goal services
# Basic costs on route 43: 90116569.37511492
# Direct costs (not staggered):
# For goal service 43:
# n_min: 5
# n_max: 8
# lowest costs of the goal service 43 with configuration (43, 18, 8) at 0.0
# (43, 18, 1): 50016880.85815349
# (43, 18, 2): 51036109.9781535
# (43, 18, 3): 52055339.0981535
# (43, 18, 4): 53074568.21815348
# (43, 18, 5): 42436343.04000001
# (43, 18, 6): 28290895.359999985
# (43, 18, 7): 14145447.680000037
# (43, 18, 8): 0.0
# -------------------------------------------
