### Additional Information to the goal services
# Basic costs on route 42: -306520364.51649344
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 36793560.29455459
# (42, 18, 2): 37636384.374554574
# (42, 18, 3): 38479208.45455456
# (42, 18, 4): 35091591.360000014
# (42, 18, 5): 23394394.24000001
# (42, 18, 6): 11697197.120000005
# (42, 18, 7): 0.0
# -------------------------------------------
