### Additional Information to the goal services
# Basic costs on route 42: -114054089.12241615
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 13690627.086345896
# (42, 18, 2): 14004236.046345904
# (42, 18, 3): 14317845.006345883
# (42, 18, 4): 13057336.320000008
# (42, 18, 5): 8704890.88000001
# (42, 18, 6): 4352445.439999998
# (42, 18, 7): 0.0
# -------------------------------------------
