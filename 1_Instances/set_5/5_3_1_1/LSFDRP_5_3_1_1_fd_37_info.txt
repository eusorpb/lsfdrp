### Additional Information to the goal services
# Basic costs on route 42: -263750081.0955873
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 31659575.137174845
# (42, 18, 2): 32384795.857174814
# (42, 18, 3): 33110016.577174783
# (42, 18, 4): 30195090.23999995
# (42, 18, 5): 20130060.159999967
# (42, 18, 6): 10065030.079999954
# (42, 18, 7): 0.0
# -------------------------------------------
