### Additional Information to the goal services
# Basic costs on route 42: -199594655.96422824
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 23958597.401105314
# (42, 18, 2): 24507413.08110529
# (42, 18, 3): 25056228.76110527
# (42, 18, 4): 22850338.560000002
# (42, 18, 5): 15233559.039999992
# (42, 18, 6): 7616779.519999981
# (42, 18, 7): 0.0
# -------------------------------------------
