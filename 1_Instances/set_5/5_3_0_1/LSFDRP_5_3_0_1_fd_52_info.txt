### Additional Information to the goal services
# Basic costs on route 42: -370675789.64785254
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 44494538.03062415
# (42, 18, 2): 45513767.150624156
# (42, 18, 3): 46532996.27062416
# (42, 18, 4): 42436343.04000002
# (42, 18, 5): 28290895.360000014
# (42, 18, 6): 14145447.680000007
# (42, 18, 7): 0.0
# -------------------------------------------
