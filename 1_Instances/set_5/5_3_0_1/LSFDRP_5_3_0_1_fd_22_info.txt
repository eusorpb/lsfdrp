### Additional Information to the goal services
# Basic costs on route 42: -156824372.5433222
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 18824612.243725598
# (42, 18, 2): 19255824.56372559
# (42, 18, 3): 19687036.883725613
# (42, 18, 4): 17953837.439999998
# (42, 18, 5): 11969224.960000008
# (42, 18, 6): 5984612.480000019
# (42, 18, 7): 0.0
# -------------------------------------------
