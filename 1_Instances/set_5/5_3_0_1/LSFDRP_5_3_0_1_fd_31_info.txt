### Additional Information to the goal services
# Basic costs on route 42: -220979797.6746813
# Direct costs (not staggered):
# For goal service 42:
# n_min: 4
# n_max: 7
# lowest costs of the goal service 42 with configuration (42, 18, 7) at 0.0
# (42, 18, 1): 26525589.979795158
# (42, 18, 2): 27133207.339795142
# (42, 18, 3): 27740824.699795127
# (42, 18, 4): 25298589.120000005
# (42, 18, 5): 16865726.079999983
# (42, 18, 6): 8432863.039999992
# (42, 18, 7): 0.0
# -------------------------------------------
