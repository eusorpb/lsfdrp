import copy
import pickle
import time

# Helper
def In(node, A):
    nodes = []
    for i, j in A:
        if j == node:
            nodes.append(i)
    return nodes


class Instance(object):
    """
    Holds the data in the form needed for hte heuristic
    If the instance is not feasible, the data in the fleet deployment and fleet repositioning file do not contain the same structure
    """

    def __init__(self, fr, fd):
        self.feasible = True

        # Load instance, for type information see show_instance_information.py in folder 1_Instances
        self.Routes, self.K, self.Ships, self.ShipsE, self.ShipsC, self.cHotel, self.A, self.A_wSink, self.A_inflex, self.A_flex, self.V, self.V_wSink,\
            self.V_inflex, self.V_flex, self.Q, self.M, self.RPO, self.u_rf, self.u_dc, self.vStart, self.p, self.tau, self.delta, self.cMV, self.cPort,\
            self.cSail, self.cVarSail, self.rDemand, self.rEqp, self.amount, self.V_ePlus, self.V_eMinus, self.V_ePlusMinus, self.tMV, self.tE, self.tX,\
            self.tP, self.d_min, self.d_max, self.unlocode = pickle.load(open(fr, "rb"))

        self.fdRoutes, self.fdK, self.cCharter, self.rCharter, self.n_min, self.n_max, self.cRoute, self.K_notOnR, self.filename = pickle.load(open(fd, "rb"))

        self.V_wSink = list(self.V_wSink)
        self.Routes = list(self.Routes)
        self.fdRoutes = list(self.fdRoutes)
        self.Ships = list(self.Ships)
        self.V = list(self.V)

        self.possible_configurations = []
        self.penalty_values = None

        # Check if files are compatible
        if self.fdRoutes != self.Routes or self.fdK != self.K:
            print("Data for routes or vessel classes does not match!")
            print("From fr:\n {0}\n {1}".format(self.Routes, self.K))
            print("From fd:\n {0}\n {1}".format(self.fdRoutes, self.fdK))
            
            self.feasible = False
        
        if self.feasible:
            print("***  |- Create instance object for heuristic")

            self.heu_V = set()
            self.heu_V_to_V = {}
            self.V_to_heu_V = {}
            self.heu_V_wSink = set()
            self.heu_unlocode = {}
            self.heu_tE = {}
            self.heu_tX = {}           
            self.heu_cPort = {}
            self.heu_cMV = {}            

            self.heu_next_nodes = {}
            self.heu_prev_nodes = {}
            self.heu_next_nodes_wSink = {}  # next_nodes without sink
            self.heu_prev_nodes_wSink = {}  # prev_nodes without sink
            self.heu_cSail = {}
            self.heu_multi_ports = []
            
            self.heu_M = {}
            self.heu_M_to_M = {}
            
            self.heu_rDemand = {}
            self.heu_amount = {}
            self.heu_multi_demands = []

            self.heu_sinks = set(self.tau.values())
            
            max_node = 0

            # Copy values of inflexible nodes
            for ii in self.V_inflex:
                if ii > max_node:
                    max_node = ii
                self.heu_V.add(ii)
                self.heu_V_to_V[ii] = ii
                self.heu_next_nodes[ii] = set()
                self.heu_prev_nodes[ii] = set()
                self.heu_unlocode[ii] = self.unlocode[ii]
                self.heu_tE[ii] = self.tE[ii]
                self.heu_tX[ii] = self.tX[ii]                
                self.heu_cMV[ii] = self.cMV[ii]

                for ss in self.Ships:
                    self.heu_cPort[ss, ii] = self.cPort[ss, ii]

            # Same for sinks
            for ii in self.tau.values():
                self.heu_V.add(ii)
                self.heu_V_to_V[ii] = ii
                self.heu_next_nodes[ii] = set()
                self.heu_prev_nodes[ii] = set()
                self.heu_unlocode[ii] = self.unlocode[ii]
                self.heu_tE[ii] = self.tE[ii]
                self.heu_tX[ii] = self.tX[ii] 
            
            minDurationAflex = {(ii, jj): None for ii, jj in self.A_flex}
            maxDurationAflex = {(ii, jj): None for ii, jj in self.A_flex}

            # Set arcs and filter flexible arcs
            for (ii, jj) in self.A:
                if (ii, jj) in self.A_flex:
                    for ss in self.Ships:
                        if minDurationAflex[ii, jj] and minDurationAflex[ii, jj] < self.d_min[ii, jj, ss]:
                            minDurationAflex[ii, jj] = self.d_min[ii, jj, ss]
                        elif not minDurationAflex[ii, jj]:
                            minDurationAflex[ii, jj] = self.d_min[ii, jj, ss]
                            
                        if maxDurationAflex[ii, jj] and maxDurationAflex[ii, jj] > self.d_max[ii, jj, ss]:
                            maxDurationAflex[ii, jj] = self.d_max[ii, jj, ss]
                        elif not maxDurationAflex[ii, jj]:
                            maxDurationAflex[ii, jj] = self.d_max[ii, jj, ss]
                            
                else:
                    self.heu_next_nodes[ii].add(jj)
                    self.heu_prev_nodes[jj].add(ii)

                    for ss in self.Ships:
                        self.heu_cSail[ss, ii, jj] = self.cSail[ss, ii, jj]

            time_start = time.time()

            if self.V_flex:
                multiPorts = {ii: set() for ii in self.V_flex}
                print("***  |   |- Convert flexible visitations into inflexible visitations,\t", end="")

            # Convert inflexible nodes and arcs to inflexible (node copy for every possible connection (multiPort)
            for ii, jj in self.A_flex:
                if ii not in self.V_flex and jj in self.V_flex:
                    for jj2, kk in self.A_flex:
                        if jj == jj2:
                            if self.tX[ii] + minDurationAflex[ii, jj] + self.tP[jj] + minDurationAflex[jj, kk] <= self.tE[kk]:
                                max_node += 1
                                self.heu_V.add(max_node)
                                self.heu_V_to_V[max_node] = jj
                                multiPorts[jj].add(max_node)
                                
                                self.heu_unlocode[max_node] = self.unlocode[jj]
                                self.heu_tE[max_node] = self.tX[ii] + minDurationAflex[ii, jj]
                                self.heu_tX[max_node] = self.tE[kk] - minDurationAflex[jj, kk]
                                self.heu_cMV[max_node] = self.cMV[jj]
                                
                                self.heu_next_nodes[ii].add(max_node)
                                self.heu_prev_nodes[max_node] = {ii}                               
                                self.heu_next_nodes[max_node] = {kk}
                                self.heu_prev_nodes[kk].add(max_node)
                                
                                for ss in self.Ships:
                                    self.heu_cPort[ss, max_node] = self.cPort[ss, jj]
                                    self.heu_cSail[ss, ii, max_node] = self.cSail[ss, ii, jj] + minDurationAflex[ii, jj] * self.cVarSail[ss, ii, jj]
                                    self.heu_cSail[ss, max_node, kk] = self.cSail[ss, ii, jj] + minDurationAflex[jj, kk] * self.cVarSail[ss, jj, kk]

                                if self.tX[ii] + maxDurationAflex[ii, jj] + self.tP[jj] + maxDurationAflex[jj, kk] <= self.tE[kk]:
                                    max_node += 1
                                    self.heu_V.add(max_node)
                                    self.heu_V_to_V[max_node] = jj
                                    multiPorts[jj].add(max_node)
                                    
                                    self.heu_unlocode[max_node] = self.unlocode[jj]
                                    self.heu_tE[max_node] = self.tX[ii] + maxDurationAflex[ii, jj]
                                    self.heu_tX[max_node] = self.tE[kk] - maxDurationAflex[jj, kk]
                                    self.heu_cMV[max_node] = self.cMV[jj]
                                    
                                    self.heu_next_nodes[ii].add(max_node)
                                    self.heu_prev_nodes[max_node] = {ii}                                
                                    self.heu_next_nodes[max_node] = {kk}
                                    self.heu_prev_nodes[kk].add(max_node)
                                    
                                    for ss in self.Ships:
                                        self.heu_cPort[ss, max_node] = self.cPort[ss, jj]
                                        self.heu_cSail[ss, ii, max_node] = self.cSail[ss, ii, jj] + maxDurationAflex[ii, jj] * self.cVarSail[ss, ii, jj]
                                        self.heu_cSail[ss, max_node, kk] = self.cSail[ss, ii, jj] + maxDurationAflex[jj, kk] * self.cVarSail[ss, jj, kk]

            if self.V_flex:                    
                for ports in multiPorts.values():
                    self.heu_multi_ports.append(frozenset(ports))
            
                print(f"took {round(time.time()-time_start,6):>8} sec.")
            
            time_start = time.time()
            print("***  |   |- Calculate demand structure for node flow model,\t\t", end="")
            self.heu_next_nodes_wSink = copy.deepcopy(self.heu_next_nodes)        
            self.heu_prev_nodes_wSink = copy.deepcopy(self.heu_prev_nodes)
            self.heu_V_wSink = copy.copy(self.heu_V)

            for ii in self.tau.values():
                del self.heu_next_nodes_wSink[ii]
                del self.heu_prev_nodes_wSink[ii]
                self.heu_V_wSink.remove(ii)

            copy_next_nodes = copy.deepcopy(self.heu_next_nodes)
            copy_prev_nodes = copy.deepcopy(self.heu_prev_nodes) 
            demand_origins = {node: set() for node in self.heu_V}
            demand_destinations = {node: set() for node in self.heu_V}            

            self.heu_M_Vis = {node: set() for node in self.heu_V}
            self.heu_M_Vis_rf = {node: set() for node in self.heu_V}
            
            self.demand_origins = {node: set() for node in self.V}

            # Create demand copy for every destination (multiDemand)
            for demand_id, (origin, destinations, ctype) in self.M.items():
                self.demand_origins[origin].add(demand_id)
                
                multiDemand = []
                for destination in destinations:
                    new_demand_id = len(self.heu_M)+1
                    self.heu_M[new_demand_id] = (origin, destination, ctype)
                    self.heu_M_to_M[new_demand_id] = demand_id
                    self.heu_rDemand[new_demand_id] = self.rDemand[demand_id]
                    self.heu_amount[new_demand_id] = self.amount[demand_id]
                    
                    demand_origins[origin].add(new_demand_id)
                    demand_destinations[destination].add(new_demand_id)
                    
                    if len(destinations) > 1:
                        multiDemand.append(new_demand_id)
                if multiDemand:
                    new_multi_demand = tuple(multiDemand)
                    self.heu_multi_demands.append(new_multi_demand)
                    self.heu_amount[new_multi_demand] = self.amount[demand_id]

            # Idea, push through the graph from the start (front) and sink (back) and label visited nodes with demands that can flow through.
            # If demands are available at a node from the front and back, demand can flow through node

            from_start = {node: set() for node in self.heu_V}
            from_end = {node: set() for node in self.heu_V}
            from_start["start"] = set()
            from_start["end"] = set()
            from_end["start"] = set()
            from_end["end"] = set()

            copy_next_nodes["start"] = copy.copy(self.heu_V)
            copy_prev_nodes["end"] = copy.copy(self.heu_V)
            copy_prev_nodes["start"] = set()
            copy_next_nodes["end"] = set()
            demand_origins["start"] = set()
            demand_destinations["start"] = set()
            demand_origins["end"] = set()
            demand_destinations["end"] = set()

            # Front
            visited = set()
            queue = ["start"]
            while len(queue) > 0:
                current_node = queue.pop(0)
                visited.add(current_node)
                
                if demand_origins[current_node]:
                    from_start[current_node] |= demand_origins[current_node]

                for next_node in copy_next_nodes[current_node]:
                    for demand in from_start[current_node]:
                        if demand not in demand_destinations[current_node] and self.heu_tE[next_node] <= self.heu_tE[self.heu_M[demand][1]]:
                            from_start[next_node].add(demand)
                    for node in copy_prev_nodes[next_node]:
                        if node not in visited:
                            break
                    else:
                        queue.append(next_node)

            # Back
            visited = set()
            queue = ["end"]
            while len(queue) > 0:
                current_node = queue.pop(0)
                visited.add(current_node)

                if demand_destinations[current_node]:
                    from_end[current_node] |= demand_destinations[current_node]

                for prev_node in copy_prev_nodes[current_node]:
                    for demand in from_end[current_node]:
                        if demand not in demand_origins[current_node] and self.heu_tE[prev_node] >= self.heu_tE[self.heu_M[demand][0]]:
                            from_end[prev_node].add(demand)
                    for node in copy_next_nodes[prev_node]:
                        if node not in visited:
                            break
                    else:
                        queue.append(prev_node)
                        
            for port, demands in demand_destinations.items():
                for demand in demands:
                    from_end[port].remove(demand)

            # Check if demands can flow through node
            del from_start["start"]
            
            for node, demands in from_start.items():
                for demand in demands:
                    if demand in from_end[node]:
                        self.heu_M_Vis[node].add(demand)
                        if self.heu_M[demand][2] == "rf":
                            self.heu_M_Vis_rf[node].add(demand)

            print(f"took {round(time.time()-time_start,6):>8} sec.")

            print("***  |   |- Create additional vessel information,\t\t\t\t", end="")
            time_start = time.time()
            
            self.vessels_by_class = {vessel_class: [] for vessel_class in self.K}
            self.vessel_class = {}
            
            for (vessel, vessel_class), pp in self.p.items():
                if pp == 1:
                    self.vessels_by_class[vessel_class].append(vessel)
                    self.vessel_class[vessel] = vessel_class  

            self.phase_in_ordered = {route: [] for route in self.fdRoutes}
            
            for route in self.fdRoutes:
                for node in In(self.tau[route], self.A):
                    if not self.phase_in_ordered[route]:
                        self.phase_in_ordered[route].append(node)
                    else:
                        for pos, port in enumerate(self.phase_in_ordered[route]):
                            if self.tX[node] < self.tX[port]:
                                self.phase_in_ordered[route].insert(pos, node)
                                break
                            elif pos == len(self.phase_in_ordered[route])-1:
                                self.phase_in_ordered[route].append(node)
                                break

            print(f"took {round(time.time()-time_start,6):>8} sec.")
        print("***  |")

    def return_phaseIn_order(self, sink):
        """
        Returns phase-in order of service
        """

        phaseIn_order = []
        for node in self.heu_prev_nodes[sink]:

            if phaseIn_order:
                for pos, port in enumerate(phaseIn_order):
                    if self.heu_tX[port] > self.heu_tX[node]:
                        phaseIn_order.insert(pos, node)
                        break
                else:
                    phaseIn_order.append(node)
            else:
                phaseIn_order.append(node)

        return phaseIn_order

    def create_ranking(self):
        """
        Creates ranking for fdp allocations
        """

        print("***  |   |- Create ranking for FDP solutions,\t\t\t\t", end="")
        time_start = time.time()

        # Create possible allocations
        services = list(sorted(self.Routes))
        vessel_types = list(self.K)
        amount_vessels = {vessel_type: len(self.vessels_by_class[vessel_type]) for vessel_type in vessel_types}
        vessel_type_allocations = {}

        for service in services:
            vessel_type_allocations[service] = {}

            for vessel_type in vessel_types:
                vessel_type_allocations[service][vessel_type] = []

                for nn in range(self.n_min[service], self.n_max[service] + 1):
                    if nn <= amount_vessels[vessel_type]:
                        vessel_type_allocations[service][vessel_type].append(nn)

        possible_vessel_type_allocations = create_possible_vessel_type_allocations(services, vessel_type_allocations, amount_vessels, -1,
                                                                                   {vessel_type: 0 for vessel_type in vessel_types}, [], [])

        # Rank generated allocations
        costs_vessel_type_allocations = {}
        ranking_vessel_type_allocations = []

        for pos, vessel_type_allocation in enumerate(possible_vessel_type_allocations):
            costs = 0
            for pos2, service in enumerate(services):
                for nn in range(1, vessel_type_allocation[pos2][1] + 1):
                    costs += self.cRoute[(service, vessel_type_allocation[pos2][0], nn)]

            costs_vessel_type_allocations[pos] = costs

            if ranking_vessel_type_allocations:
                for pos2, index in enumerate(ranking_vessel_type_allocations):
                    if costs < costs_vessel_type_allocations[index]:
                        ranking_vessel_type_allocations.insert(pos2, pos)
                        break
                else:
                    ranking_vessel_type_allocations.append(pos)
            else:
                ranking_vessel_type_allocations.append(pos)

        print(f"took {round(time.time()-time_start,6):>8} sec.")

        return ranking_vessel_type_allocations, possible_vessel_type_allocations


def create_possible_vessel_type_allocations(services, vessel_type_allocations, amount_vessels, index, current_amount_vessels, current_allocation, possible_allocations):
    """
    Creates all possible vessel type allocations  (recursive function)
    """

    index += 1

    service = services[index]

    # Idea: Create possible assignments for first service, then recursive for the next until all services are covered

    for vessel_type, nns in vessel_type_allocations[service].items():
        for nn in nns:
            if current_amount_vessels[vessel_type] + nn <= amount_vessels[vessel_type]:
                new_amount_vessels = copy.deepcopy(current_amount_vessels)
                new_amount_vessels[vessel_type] += nn
                new_allocation = copy.deepcopy(current_allocation)
                new_allocation.append((vessel_type, nn))

                # Check if we covered all services, else go to next service
                if index == len(services) - 1:
                    possible_allocations.append(new_allocation)

                else:
                    possible_allocations = create_possible_vessel_type_allocations(services, vessel_type_allocations,
                                                                                   amount_vessels, index,
                                                                                   new_amount_vessels, new_allocation,
                                                                                   possible_allocations)

    return possible_allocations
