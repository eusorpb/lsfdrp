from gurobipy import *


def calculate_vessels_path(instance, heu_subgraph, calc_type, vessels, first_goals, amount=None, second_goals=None, prohibited_ports=None):
    """
    Creates vessel paths based on the calculation type and inputs:
    0=Solve path for all vessels
    1=Solve path for #amount vessels
    2=Recalculate path of all vessels (Requires second_goals and prohibited_ports)
    """

    mm = Model('Calculate vessels path')

    start_ports = set()

    for ss in vessels:
        start_ports.add(instance.vStart[ss])

    phi = mm.addVars(vessels, name="phi")
    xD = mm.addVars(heu_subgraph.M, vessels, name="xD")
    xE = {}
    yy = {}

    for ii in heu_subgraph.V:
        for jj in heu_subgraph.next_nodes[ii]:
            for ss in vessels:
                yy[ss, ii, jj] = mm.addVar(vtype='B', name=f"y[{ss},{ii},{jj}]")
            for qq in instance.Q:
                xE[qq, ii, jj] = mm.addVar(name=f"xE[{qq},{ii},{jj}]")

    mm.update()

    #
    # Set objective
    #

    mm.setObjective(
            # less shipping cost
            - quicksum(instance.cHotel[ss] * phi[ss] for ss in vessels)
            - quicksum(quicksum(quicksum(instance.heu_cSail[ss, ii, jj] * yy[ss, ii, jj] for jj in heu_subgraph.next_nodes[ii])
                                for ii in heu_subgraph.V_wSink)
                       for ss in vessels)
            # less port fee
            - quicksum(quicksum(quicksum(instance.heu_cPort[ss, jj] * yy[ss, ii, jj] for ii in heu_subgraph.prev_nodes[jj])
                                for jj in heu_subgraph.V_wSink)
                       for ss in vessels)
            # plus revenue due to demand during repositioning
            + quicksum(quicksum((instance.heu_rDemand[demand_id] - instance.heu_cMV[oo] - instance.heu_cMV[dd]) * xD[demand_id, ss] for demand_id, (oo, dd, qq) in heu_subgraph.M.items())
                       for ss in vessels)
            , GRB.MAXIMIZE)

    #
    # Add constraints
    #

    if calc_type in [0, 2]:
        # Vessel must reach their goal services
        mm.addConstrs(quicksum(yy[ss, ii, first_goals[pos]] for ii in heu_subgraph.prev_nodes[first_goals[pos]]) == 1
                      for pos, ss in enumerate(vessels))

        # Vessels must leave their start port
        mm.addConstrs(quicksum(yy[ss, instance.vStart[ss], jj] for jj in heu_subgraph.next_nodes[instance.vStart[ss]])
                      == 1
                      for ss in vessels)

    if calc_type in [0, 1]:
        # Phase-In slots must be used in ascending order
        mm.addConstrs(quicksum(yy[ss, ii, first_goals[0]] for ss in vessels) <= quicksum(yy[ss, jj, first_goals[0]] for ss in vessels)
                      for ii in heu_subgraph.prev_nodes[first_goals[0]]
                      for jj in heu_subgraph.prev_nodes[first_goals[0]]
                      if ii != jj and instance.delta[ii, jj] == 1)

    if calc_type == 1:
        # Vessel must reach their goal services
        mm.addConstr(quicksum(quicksum(yy[ss, ii, first_goals[pos]] for ii in heu_subgraph.prev_nodes[first_goals[pos]])
                               for pos, ss in enumerate(vessels)) == amount)

        # a Vessel must leave the start port if it reaches the goal service
        mm.addConstrs(quicksum(yy[ss, instance.vStart[ss], jj] for jj in heu_subgraph.next_nodes[instance.vStart[ss]])
                      == quicksum(yy[ss, ii, first_goals[pos]] for ii in heu_subgraph.prev_nodes[first_goals[pos]])
                      for pos, ss in enumerate(vessels))

    if calc_type == 2:
        # Vessels must reach their second goal
        mm.addConstrs(quicksum(yy[ss, ii, second_goals[pos]] for ii in heu_subgraph.prev_nodes[second_goals[pos]]) == 1
                      for pos, ss in enumerate(vessels))

    if calc_type == 2:
        # Vessels cannot visit prohibited nodes
        if prohibited_ports:
            mm.addConstrs(quicksum(yy[ss, ii, jj] for ii in heu_subgraph.prev_nodes[jj]) == 0
                          for ss in vessels for jj in prohibited_ports)

    # Defines the time used by a vessel for the repositioning operation
    mm.addConstrs(phi[ss] >= quicksum(instance.heu_tX[ii] * yy[ss, ii, first_goals[pos]] for ii in heu_subgraph.prev_nodes[first_goals[pos]])
                  - instance.heu_tX[instance.vStart[ss]]
                  for pos, ss in enumerate(vessels))

    # Only one vessel can visit a port visitation
    mm.addConstrs(quicksum(quicksum(yy[ss, ii, jj] for ii in heu_subgraph.prev_nodes[jj]) for ss in vessels) <= 1
                  for jj in heu_subgraph.V_wSink)

    # Only one vessel can leave a port visitation
    mm.addConstrs(quicksum(quicksum(yy[ss, ii, jj] for jj in heu_subgraph.next_nodes[ii]) for ss in vessels) <= 1
                  for ii in heu_subgraph.V_wSink)

    # Vessels can only stop in a graph sink
    mm.addConstrs(quicksum(yy[ss, ii, jj] for ii in heu_subgraph.prev_nodes[jj])
                  - quicksum(yy[ss, jj, ii] for ii in heu_subgraph.next_nodes[jj]) == 0
                  for jj in heu_subgraph.V_wSink - start_ports for ss in vessels)

    # Demand can only be loaded on a vessel if the origin is visited
    mm.addConstrs(xD[demand_id, ss] <= instance.heu_amount[demand_id] * quicksum(yy[ss, oo, jj]
                                                                                 for jj in heu_subgraph.next_nodes[oo])
                  for ss in vessels for demand_id, (oo, dd, qq) in heu_subgraph.M.items())

    # Demand can only be loaded on a vessel if the destination is visited
    mm.addConstrs(xD[demand_id, ss] <= instance.heu_amount[demand_id] * quicksum(yy[ss, ii, dd]
                                                                                 for ii in heu_subgraph.prev_nodes[dd])
                  for ss in vessels for demand_id, (oo, dd, qq) in heu_subgraph.M.items())

    # The overall amount of containers cannot surpass the vessels capacity
    mm.addConstrs(quicksum(xD[demand_id, ss] for demand_id in heu_subgraph.M_Vis[jj])
                  + quicksum(xE[qq, ii, jj] for qq in instance.Q for ii in heu_subgraph.prev_nodes[jj])
                  <= instance.u_dc[ss]
                  for ss in vessels for jj in heu_subgraph.V_wSink)

    # The amount of reefer container cannot surpass the capacity of the vessel
    mm.addConstrs(quicksum(xD[demand_id, ss] for demand_id in heu_subgraph.M_Vis_rf[ii]) <= instance.u_rf[ss]
                  for ss in vessels for ii in heu_subgraph.V_wSink)

    for ii in heu_subgraph.V_wSink:
        for jj in heu_subgraph.next_nodes[ii]:
            mm.addConstr(quicksum(xE[qq, ii, jj] for qq in instance.Q)
                  <= quicksum(instance.u_dc[ss] * yy[ss, ii, jj] for ss in vessels))

    # The amount of single-demands cannot exceed the amount of the original demand
    mm.addConstrs(quicksum(xD[demand_id, ss] for demand_id in multi_demands) <= heu_subgraph.amount[multi_demands]
                 for ss in vessels for multi_demands in heu_subgraph.multi_demands)

    # Vessels representing the same flexible visitation can in sum only be visited once
    mm.addConstrs(quicksum(quicksum(yy[ss, ii, jj] for ii in heu_subgraph.prev_nodes[jj])
                           for jj in multi_ports for ss in vessels) <= 1
                  for multi_ports in heu_subgraph.multi_ports)

    # Optimize with four threads and without output
    mm.setParam("OutputFlag", 0)
    mm.setParam('Threads', 4)
    mm.optimize()

    # Parse solution
    if mm.status == 2:
        vessels_arcs = {ss: [] for ss in vessels}
        vessel_obj_new = {ss: 0 for ss in vessels}

        for var in mm.getVars():
            if var.x > 0.1:
                if "xE" in var.VarName:
                    print("EQUIPMENT")
                elif "y" in var.VarName:
                    ss, ii, jj = [int(x) for x in var.VarName[2:var.VarName.find("]")].split(",")]
                    vessels_arcs[ss].append((ii, jj))
                elif "phi" in var.VarName:
                    ss = int(var.VarName[4:var.VarName.find("]")])
                    vessel_obj_new[ss] -= instance.cHotel[ss] * var.x
                elif "xD" in var.VarName:
                    demand_id, ss = [int(x) for x in var.VarName[3:var.VarName.find("]")].split(",")]
                    (oo, dd, qq) = instance.heu_M[demand_id]
                    vessel_obj_new[ss] += (instance.heu_rDemand[demand_id] - instance.heu_cMV[oo] - instance.heu_cMV[dd]) * var.x

        vessels_path = []
        vessels_objective = []

        for pos, ss in enumerate(vessels):
            if vessels_arcs[ss]:
                path = [instance.vStart[ss]]
                while path[-1] != first_goals[pos]:
                    for ii, jj in vessels_arcs[ss]:
                        if ii == path[-1]:
                            path.append(jj)
            else:
                path = None
            vessels_path.append(path)
            if path:
                for pos2, port in enumerate(path):
                    if port != path[-1]:
                        pass
                        vessel_obj_new[ss] -= instance.heu_cSail[ss, port, path[pos2+1]]
                        if port != path[0]:
                            vessel_obj_new[ss] -= instance.heu_cPort[ss, port]
                            pass
                vessels_objective.append(vessel_obj_new[ss])
            else:
                vessels_objective.append(None)

        return True, round(mm.objVal, 4), vessels_path, vessels_objective
    else:
        return False, None, None, None


def calculate_assignment(instance, services, vessel_to_phaseIn, possible_phaseIn_to_vessel, possible_vessel_to_phaseIn):
    """
    Finds the best assignment of vessels to phase in slots
    """
    mm = Model('Vessel assignment')
    xx = {}

    for phaseIn, vessels in possible_vessel_to_phaseIn.items():
        for vessel in vessels:
            xx[phaseIn, vessel] = mm.addVar(vtype='B', name=f"x[{phaseIn},{vessel}]")

    mm.update()
    mm.setObjective(quicksum(quicksum(vessel_to_phaseIn[phaseIn, vessel] * xx[phaseIn, vessel] for vessel in vessels)
                             for phaseIn, vessels in possible_vessel_to_phaseIn.items())
                    , GRB.MAXIMIZE)

    # Phase-in slots must be satisfied
    mm.addConstrs(quicksum(xx[phaseIn, vessel] for vessel in possible_vessel_to_phaseIn[phaseIn]) == 1
                  for phaseIn in possible_vessel_to_phaseIn)

    # A vessel can only satisfy at most one phase-in slot
    mm.addConstrs(quicksum(xx[phaseIn, vessel] for phaseIn in possible_phaseIn_to_vessel[vessel]) <= 1
                  for vessel in possible_phaseIn_to_vessel)

    # Optimize with four threads and without output
    mm.setParam("OutputFlag", 0)
    mm.setParam('Threads', 4)
    mm.optimize()

    # Return solution
    service_vessels = {ss: [] for ss in services}

    if mm.status == 2:
        for var in mm.getVars():
            if var.x > 0.1:
                if "x" in var.VarName:
                    phaseIn, vessel = [int(x) for x in var.VarName[2:var.VarName.find("]")].split(",")]

                    for service, sink in instance.tau.items():
                        if phaseIn in instance.heu_prev_nodes[sink]:
                            service_vessels[service].append(vessel)

        return tuple([tuple(sorted(service_vessels[service])) for service in services])

