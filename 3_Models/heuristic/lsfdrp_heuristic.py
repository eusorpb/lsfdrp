import argparse
from gurobipy import *
import math
import os.path
import sys
import time

import heuristic_instance
import heuristic_matheuristic
import lsfdrp_instance
import lsfdrp_models


if __name__ == "__main__":
    args = None
    parser = argparse.ArgumentParser(description='LSFDRP Heuristic')
    parser.add_argument('-fr', '--fleetrepositioning', type=str, help='Path to the lsfrp-instance to be solved', required=True)
    parser.add_argument('-fd', '--fleetdeployment', type=str, help='Path to the lsfdp-instance to be solved', required=True)
    args = parser.parse_args()

    if not os.path.exists(args.fleetrepositioning):
        print("Instance LSFRP not found: {0}".format(args.fleetrepositioning), file=sys.stderr)
        sys.exit(1)
    if not os.path.exists(args.fleetdeployment):
        print("Instance LSFDP not found: {0}".format(args.fleetdeployment), file=sys.stderr)
        sys.exit(1)
        
    # Note: error handling omitted; add your own if desired.
    fr = args.fleetrepositioning
    fd = args.fleetdeployment
    print("Instance LSFRP: {0}".format(fr))
    print("Instance LSFDP: {0}\n".format(fd))
    
    # Create Gurobi Academic output
    m = Model()
    print()

    # Parameters used for solving the LSFDRP problem
    overall_timelimit = 600
    best_solution = None

    print("*"*125)
    print(f"*** Solve {fd} with matheuristics.\n***  |")

    """
    Heuristic Idea
    1. FDP Ranking
    2. Loop until time limit is up
    2.a. Select n FDP solutions; if first iteration, the top n. In subsequent iterations, select n * p (p in [0,1]) from the best FDP solutions using the
         objective function values we've found in our local search; select n * (1-p) FDP solutions from FDP solutions we have not solved yet
    2.b. For each of the n best FDP solutions:
    2.b.1. If a vessel type is assigned to only one goal service, solve a subproblem for it that uses exactly the amount of assigned vessels
    2.b.2 otherwise:
    2.b.2.i randomly generate an assignment of vessels to goal service slots based on the FDP solution.
    2.b.2.ii Perform a iterative improvement in which we can add/remove/swap vessels between slots, solving the necessary subproblems at each step of the
             iterative improvement
    2.b.3 Save the best solution found for this FDP solution
    """

    # Create instance object
    instance = heuristic_instance.Instance(fr, fd)
    overall_time_start = time.time()
    # mh = heuristic_matheuristic.Matheuristic(instance, overall_time_start) Info: replaced by loop with six runs

    solutions = {}

    for ii in range(1, 7):
        print(f"***  |- Start solving instance with matheuristics, run {ii}")
        overall_time_start = time.time()
        time_start = time.time()
        mh = heuristic_matheuristic.Matheuristic(instance, overall_time_start)

        nn = math.ceil(3 + math.log(len(instance.Routes)))
        best_solution, time_out = mh.start(instance, nn, overall_timelimit)

        if best_solution:
            solutions[ii] = best_solution

            if time_out:
                print(f"***  |   |- Calculation finished, timelimit reached \t\t\ttook {round(time.time() - time_start,2):>8} sec.")
            else:
                print(f"***  |   |- Calculation finished,\t\t\t\t\ttook {round(time.time() - time_start, 2):>8} sec.")
            print("***  |")
        else:
            print("ERROR")

    print("***  |{0}".format("-" * 119))
    print("***\n" + "*" * 125)

    avg_objective = 0
    avg_counter = 0
    best_solution = None

    for ii in range(1, 7):
        if ii in solutions:
            print("###\n###")
            print("#"*125)
            print(f"### Solution run {ii}")

            # Transfer and solve solution with original model to have an additional feasibility check and print solution
            lf_instance = lsfdrp_instance.Instance(fr, fd)
            lf_instance.adapt_to_solution(instance, solutions[ii])
            final_solution = lsfdrp_models.solve_arc_flow(lf_instance, output_flag=True)
            final_solution.print_solution(lf_instance)

            avg_counter += 1
            avg_objective += final_solution.objective

            if best_solution:
                if best_solution.objective < final_solution.objective:
                    best_solution = final_solution
            else:
                best_solution = final_solution
        else:
            print(f"***  |- Timelimit reached, could not find a feasible solution (took {round(time.time() - time_start, 2)}) sec.)")

    print("+" * 125 + "\n+++")
    print(f"+++ The average objective is {round(avg_objective/avg_counter,4):,} ({round(avg_objective/avg_counter,4)})")
    print("+++\n" + "+" * 125)

    best_solution.export(instance)
