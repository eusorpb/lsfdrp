import pickle


class Solution(object):
    """
    Solution object to the original mathmodel
    """
    def __init__(self, instance):

        self.feasible = False
        self.objective = 0

        self.service_info = {rr: (None, 0) for rr in instance.services}  # Key: service, value: tuple(vessel type, vessel amount)
        self.service_vessels = {rr: [] for rr in instance.services}

        self.vessel_path = {}
        self.vessel_demand = {ss: [] for ss in instance.vessels}
        self.vessels_not_on_service = None

        self.port_enter = {}
        self.port_exit = {}

    def print_solution(self, instance):
        """
        Output solution
        """

        fvessel = len(str(len(instance.vessels)))
        fdemand = len(str(len(instance.demands)))
        fport = len(str(len(instance.nodes)))
        fcontainer = len(str(max(instance.amount_demand.values()))) + 2

        serviceNames = {177: "WCSA", 24: "WAF7", 121: "SAE", 7: "ME3", 81: "MECL1", 42: "SAMBA", 43: "AMEX", 32: "SAECS CORE", 3: "SAFARI 1", 100: "unknown"}
        vesselClasses = {18: "PMax25", 19: "PMax28", 30: "PMax35"}

        print("#" * 125)
        print("###\n### Results\n###")
        print(f"### The objective of the solution is {round(self.objective, 4):,} ({round(self.objective, 4)})")
        print("###")
        for service, (vessel_type, vessel_amount) in sorted(self.service_info.items()):
            print(f"### Service {service} ({serviceNames[service]}) is operated by {vessel_amount} vessels from type {vessel_type} ({vesselClasses[vessel_type]})")
            print("###  |")
            for vessel in sorted(self.service_vessels[service]):
                print(f"###  |- Vessel {vessel:{fvessel}} has the path {self.vessel_path[vessel]}")
                text = ""
                for pos, port in enumerate(self.vessel_path[vessel]):
                    if port == self.vessel_path[vessel][0]:
                        text += f"{port} ({instance.unlocode[port]} Out@{self.port_exit[port]}) - "
                    elif port != self.vessel_path[vessel][-1]:
                        text += f"{port} ({instance.unlocode[port]} In@{self.port_enter[port]}/Out@{self.port_exit[port]}) - "
                    else:
                        text += f"{port} ({instance.unlocode[port]})"
                if len(text) > 117:
                    texte = [text[ii:ii + 117] for ii in range(0, len(text), 117)]
                else:
                    texte = [text]
                for text in texte:
                    print("###  |  " + text)

                for (demand, origin, destination, amount) in sorted(self.vessel_demand[vessel]):
                    print(f"###  |   |- and carried Demand {demand:{fdemand}}, {amount:{fcontainer}.1f} containers from {origin:{fport}} to {destination:{fport}}")
                print("###  |")
            print("###  |")
        print(f"### Vessels not used in the solution: {sorted(self.vessels_not_on_service)}")
        print("###  |{0}".format("-" * 119))
        print("###\n{0}".format("#" * 125))

    def export(self, instance):
        """
        Export solution to file (needed for HPP)
        """
        service_class = {(rr, kk): 0 for rr in instance.Routes for kk in instance.K}
        service_amount = {rr: 0 for rr in instance.Routes}

        for service_id, (vessel_type, vessel_amount) in self.service_info.items():
            service_class[(service_id, vessel_type)] = 1
            service_amount[service_id] = vessel_amount

        par_y = {}

        for vessel, path in self.vessel_path.items():
            if path:
                for pos, port in enumerate(path):
                    if pos < len(path) - 1:
                        par_y[vessel, port, path[pos + 1]] = 1

        fr_pickle_stream = [service_class, service_amount, par_y, self.service_vessels]
        pickle.dump(fr_pickle_stream, open("{0}_heuristic_solution.p".format(instance.filename), "wb"))
