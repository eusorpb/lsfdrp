import pickle


class Instance:
    """
    Class containing the information regarding the instance to be solved. If the instance is not feasible,
    the data in the fleet deployment and fleet repositioning file do not contain the same structure
    """

    def __init__(self, fr, fd):
        self.feasible = True

        self.services, self.vessel_types, self.vessels, self.vessels_owned, self.vessels_chartered, self.costs_hotel,\
            self.arcs, self.arcs_without_sinks, self.arcs_inflexible, self.arcs_flexible, self.nodes,\
            self.nodes_without_sinks, self.nodes_inflexible, self.nodes_flexible, self.container_types, self.demands,\
            self.RPO, self.vessel_capacity_reefer, self.vessel_capacity, self.vessel_start, self.vessel_type,\
            self.sinks, self.delta, self.costs_movement, self.costs_port, self.costs_sailing,\
            self.costs_variable_sailing, self.revenue_demand, self.revenue_equipment, self.amount_demand, self.nodes_ePlus,\
            self.nodes_eMinus, self.nodes_ePlusMinus, self.time_movement, self.time_enter, self.time_exit, self.time_port, self.duration_min,\
            self.duration_max, self.unlocode = pickle.load(open(fr, 'rb'))

        self.fd_services, self.fd_vessel_types, self.costs_charter, self.revenue_charter, self.vessel_amount_min,\
            self.vessel_amount_max, self.costs_service, self.K_notOnR, self.filename = pickle.load(open(fd, 'rb'))

        if self.services == self.fd_services and self.vessel_types == self.fd_vessel_types:
            self.feasible = True
        else:
            self.feasible = False

        self.big_m = 0
        for node in self.sinks.values():
            if self.time_enter[node] > self.big_m:
                self.big_m = self.time_enter[node]

        self.m_orig = {node: {} for node in self.nodes}
        self.m_dest = {node: {} for node in self.nodes}
        for node in self.m_orig:
            for demand_id, (oo, dds, qq) in self.demands.items():
                if node == oo:
                    self.m_orig[node][demand_id] = (oo, dds, qq)
                if node in dds:
                    self.m_dest[node][demand_id] = (oo, dds, qq)

        self.sink_to_service = {}
        for service, sink in self.sinks.items():
            self.sink_to_service[sink] = service

    def __str__(self):
        name, week = self.filename.split("LSFDRP_")[1].split(".p")[0].split("_fd_")
        return f"This object contains the information regarding the instance {name} for a duration of {week} weeks.\n"

    def adapt_to_solution(self, heu_instance, solution):
        """
        Stores nodes and arcs used in heuristic solution to reduce the graph of the instance.
        """

        temp_nodes = set()
        temp_nodes_inflex = set()
        temp_nodes_flex = set()
        temp_nodes_without_sinks = set()

        temp_arcs = []
        temp_arcs_inflex = []
        temp_arcs_flex = []
        temp_arcs_without_sinks = []

        for ss, path in solution.vessel_path.items():
            for pos, node in enumerate(path):
                cur_node = heu_instance.heu_V_to_V[node]
                temp_nodes.add(cur_node)

                if cur_node != path[-1]:

                    if cur_node in self.nodes_flexible:
                        temp_nodes_flex.add(cur_node)
                    else:
                        temp_nodes_inflex.add(cur_node)

                    next_node = heu_instance.heu_V_to_V[path[pos + 1]]
                    temp_nodes_without_sinks.add(cur_node)
                    temp_arcs.append((cur_node, next_node))

                    if (cur_node, next_node) in self.arcs_flexible:
                        temp_arcs_flex.append((cur_node, next_node))
                    else:
                        temp_arcs_inflex.append((cur_node, next_node))

                    if next_node != path[-1]:
                        temp_arcs_without_sinks.append((cur_node, next_node))

        self.nodes = temp_nodes
        self.nodes_inflexible = temp_nodes_inflex
        self.nodes_flexible = temp_nodes_flex
        self.nodes_without_sinks = temp_nodes_without_sinks

        self.arcs = temp_arcs
        self.arcs_inflexible = temp_arcs_inflex
        self.arcs_flexible = temp_arcs_flex
        self.arcs_without_sinks = temp_arcs_without_sinks
