class Solution(object):
    """
    Contains a heuristic solution for a specific LSFDRP instance
    """

    def __init__(self, instance):
        self.instance = instance.filename
        
        self.objective = 0
        self.feasible = False
        
        self.penalty = 0
        self.penalties = []

        self.service_info = {service: (None, None) for service in instance.Routes}  # Key: service, value: tuple(vessel type, vessel amount)
        self.service_objective = {service: None for service in instance.Routes}
        self.service_vessels = {service: None for service in instance.Routes}
        
        self.vessel_path = {}
        self.vessel_objective = {}
        
        self.vessel_conflicts = None

        self.service_vessels_lh1 = {}
        self.vessel_path_lh1 = {}
        self.vessel_path_lh2 = {}
        self.vessel_objective_lh1 = {}
        self.vessel_objective_lh2 = {}

    def add_vessels_to_service(self, instance, service, vessels, vessels_path, vessels_objective):
        """
        Adds given vessels to solution
        """

        used_vessels = []
        used_vessels_path = []
        used_vessels_objective = []

        for pos, path in enumerate(vessels_path):
            if path:
                used_vessels.append(vessels[pos])
                used_vessels_path.append(path)
                used_vessels_objective.append(vessels_objective[pos])

        self.service_info[service] = (instance.vessel_class[used_vessels[0]], len(used_vessels))
        self.service_vessels[service] = used_vessels

        for pos, vessel in enumerate(used_vessels):
            self.vessel_path[vessel] = used_vessels_path[pos]
            self.vessel_objective[vessel] = used_vessels_objective[pos]

        self.service_objective[service] = self.calc_service_objective(instance, service)

    def update_vessels(self, vessels, vessels_path, vessels_objective):
        """
        Update given vessel in the solution
        """

        for pos, vessel in enumerate(vessels):
            self.vessel_path[vessel] = vessels_path[pos]
            self.vessel_objective[vessel] = vessels_objective[pos]

    def calc_service_objective(self, instance, service):
        """
        Calc objective of a service based on routing and charter costs
        """

        objective = 0
        
        for ii in range(1, self.service_info[service][1]+1):
            objective -= instance.cRoute[service, self.service_info[service][0], ii]
            
        for vessel in self.service_vessels[service]:
            if vessel in instance.ShipsC:
                objective -= instance.cCharter[vessel, service]
                
        return objective    
    
    def calc_instance_objective(self):
        """
        Calc objective of the solution based on the objective of the services and vessels
        """

        self.objective = 0

        for objective in self.service_objective.values():
            self.objective += objective
            
        for objective in self.vessel_objective.values():
            self.objective += objective        

    def check_feasibility(self, instance):
        """
        Function to check if a solution is feasible and to calculate the penalty if not
        """
        
        if self.instance == instance.filename:
            # Check for service conflict
            service_conflict = False

            for service, (vessel_type, amount) in self.service_info.items():
                if amount:
                    if not instance.n_min[service] <= amount <= instance.n_max[service]:
                        service_conflict = True
                        break
                else:
                    service_conflict = True
                    break

            if not service_conflict:

                # Check for vessel conflict
                vessel_ports = {vessel: set() for vessels in self.service_vessels.values() for vessel in vessels}
                visited_ports = set()
                vessel_conflicts = set()

                # Build vessel path based on original representation
                converted_vessels_path = {}
                for service, vessels in self.service_vessels.items():
                    for vessel in vessels:
                        converted_vessels_path[vessel] = []
                        for port in self.vessel_path[vessel]:
                            converted_vessels_path[vessel].append(instance.heu_V_to_V[port])

                # Check if nodes are visited by multiple vessels
                for service, vessels in self.service_vessels.items():
                    for vessel in vessels:
                        for port in converted_vessels_path[vessel][:-1]:
                            if port in visited_ports:
                                vessel_conflicts.add(vessel)

                                for vessel2 in vessel_ports:
                                    if vessel != vessel2:
                                        if port in vessel_ports[vessel2]:
                                            vessel_conflicts.add(vessel2)
                                            break
                            else:
                                visited_ports.add(port)
                            vessel_ports[vessel].add(port)

                if vessel_conflicts:
                    self.vessel_conflicts = vessel_conflicts
                    self.feasible = False
                else:
                    self.vessel_conflicts = None
                    self.feasible = True
            else:
                self.vessel_conflicts = None
                self.feasible = False

        else:
            print("Error: Wrong instance")
