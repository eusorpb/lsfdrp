import math
import random
import time

import heuristic_models
import heuristic_solution
import heuristic_subgraph


class Matheuristic:
    """
    Matheuristic to the LSFDRP
    """

    def __init__(self, instance, overall_time_start):
        self.overall_time_start = overall_time_start
        self.overall_time_limit = None
        self.time_limit_reached = False
        self.counter_solved_models = 1

        self.services = sorted(instance.Routes[:])
        self.service_pos = {ss: pos for pos, ss in enumerate(self.services)}

        self.fdp_ranking, self.fdp_allocations = instance.create_ranking()

        self.random_starts = set()
        self.solved_fdp_vessels = {}
        self.solved_fdp_allocs_random = {}
        self.solved_fdp_allocs_greedy = {}
        self.counter_solved_models = 1
        self.fdp_frp_objective = {}
        self.best_solution = None

        # Storage used for
        self.solved_service_vessels = {}            # unique start
        self.solved_service_vessels_solution = {}   # unique start
        self.solved_greedy_starts = {}              # greedy start
        self.solved_assignment_starts = {}          # assignment start
        self.vessel_to_phaseIn = {}                 # assignment start

        self.solved_fdp_allocs = {}

        self.test_solutions = {}
        self.best_test = None

        self.objective_time = []

    def export_objective_time(self, filename):
        """
        Export time and objective for later analysis
        """
        with open(f"{filename}_objective_time.txt", 'a') as myfile:
            myfile.write("Start\n")
            for (time_stamp, objective) in self.objective_time:
                myfile.write(f"{time_stamp};{objective}\n")
            myfile.write("End\n")

    def start(self, instance, nn, time_limit):
        """
        Starts the matheuristic
        """

        self.overall_time_limit = time_limit
        counter_solved_models = 1

        while not self.time_limit_reached and self.fdp_ranking:

            # Next rankings to solve depend on if we already found a solution
            if self.best_solution:
                fdp_ranks = choose_rankings(self.fdp_ranking, nn, self.fdp_frp_objective)
            else:
                fdp_ranks = choose_rankings(self.fdp_ranking, nn, self.fdp_frp_objective, first=True)

            # Solve chosen ranks
            for fdp_rank in fdp_ranks:

                if self.time_check():
                    break

                fdp_allocation = self.fdp_allocations[fdp_rank]

                start_options = [1, 2, 3]

                # Check if multiple services uses the same vessel type (remove option 1)
                used_vessel_type = set()
                for alloc in fdp_allocation:
                    if alloc[0] in used_vessel_type:
                        start_options = [2, 3]
                        break
                    used_vessel_type.add(alloc[0])

                # With only one service we do not have to consider option 2 and 3
                if len(self.services) == 1:
                    start_options = [1]
                    self.fdp_ranking.remove(fdp_rank)

                for ii in start_options:
                    if self.time_check():
                        break

                    # Create start solution
                    if ii == 1:
                        solution = self.create_unique_start(instance, fdp_allocation)
                    elif ii == 2:
                        solution = self.create_greedy_start(instance, fdp_allocation)
                    elif ii == 3:
                        solution = self.create_random_start(instance, fdp_allocation)
                    else:
                        solution = None

                    # Perform local search if multiple services
                    if solution and len(self.services) > 1:
                        solution = self.perform_ls(instance, solution)

                    # Update the best solution and fdp ranks
                    if solution and solution.feasible:
                        if fdp_rank in self.fdp_frp_objective:
                            if self.fdp_frp_objective[fdp_rank] < solution.objective:
                                self.fdp_frp_objective[fdp_rank] = solution.objective
                        else:
                            self.fdp_frp_objective[fdp_rank] = solution.objective

                        if self.best_solution:
                            if self.best_solution.objective < solution.objective:
                                self.best_solution = solution
                                self.objective_time.append((time.time() - self.overall_time_start, self.best_solution.objective))
                        else:
                            self.best_solution = solution
                            self.objective_time.append((time.time() - self.overall_time_start, self.best_solution.objective))

                    counter_solved_models += 1

        self.export_objective_time(instance.filename)

        return self.best_solution, self.time_limit_reached

    def create_unique_start(self, instance, fdp_allocation):
        """
        Creates a stat solution considering all vessels possible on a service.
        """

        if tuple(fdp_allocation) in self.solved_service_vessels_solution:
            solution = self.solved_service_vessels_solution[tuple(fdp_allocation)]

        else:
            solution = heuristic_solution.Solution(instance)

            for pos, alloc in enumerate(fdp_allocation):

                # Collect vessels
                service = self.services[pos]
                vessel_type = alloc[0]
                vessels = tuple(sorted(instance.vessels_by_class[vessel_type]))
                goals = [instance.tau[service]] * len(vessels)
                amount = alloc[1]

                # Check time limit
                if self.time_check():
                    break

                # Create or load solution
                if (service, vessel_type, amount) not in self.solved_service_vessels:
                    # Create subgraphs for vessels
                    heu_subgraph = heuristic_subgraph.Subgraph()
                    heu_subgraph.create(instance, vessels, [service])

                    if heu_subgraph.feasible:
                        feasible_solution, overall_objective, vessels_path, vessels_objective = \
                            heuristic_models.calculate_vessels_path(instance, heu_subgraph, 1, vessels, goals, amount=amount)

                        if feasible_solution:
                            self.solved_service_vessels[(service, vessel_type, amount)] = (vessels_path, vessels_objective)
                            solution.add_vessels_to_service(instance, service, vessels, vessels_path, vessels_objective)
                        else:
                            self.solved_service_vessels[(service, vessel_type, amount)] = None
                    else:
                        self.solved_service_vessels[(service, vessel_type, amount)] = None
                else:
                    if self.solved_service_vessels[(service, vessel_type, amount)]:
                        solution.add_vessels_to_service(instance, service, vessels,
                                                        self.solved_service_vessels[(service, vessel_type, amount)][0],
                                                        self.solved_service_vessels[(service, vessel_type, amount)][1])

                self.counter_solved_models += 1
            solution.check_feasibility(instance)

            # Check and resolve possible conflicts
            if solution.vessel_conflicts:
                solution = self.solve_conflict(instance, solution)

            # Note, it is still possible to not find a feasible solution
            if solution and solution.feasible:
                solution.calc_instance_objective()
                self.solved_service_vessels_solution[tuple(fdp_allocation)] = solution
            else:
                solution = None
                self.solved_service_vessels_solution[tuple(fdp_allocation)] = solution

        return solution

    def create_greedy_start(self, instance, fdp_allocation):
        """
        Create a greedy start solution
        """

        # Use existing start allocation, else create allocation
        if tuple(fdp_allocation) in self.solved_assignment_starts:
            start_alloc = self.solved_assignment_starts[tuple(fdp_allocation)]

        else:
            service_phaseIn = []
            possible_service_vessels = []
            possible_vessel_to_phaseIn = {}
            possible_phaseIn_to_vessel = {}

            # Identify vessels that could serve service and the phase-in slots
            for pos, alloc in enumerate(fdp_allocation):
                possible_service_vessels.append(instance.vessels_by_class[alloc[0]])
                service_phaseIn.append(instance.return_phaseIn_order(instance.tau[self.services[pos]])[:alloc[1]])

            for pos, vessels in enumerate(possible_service_vessels):
                service = self.services[pos]
                first_goal = [instance.tau[service]]

                for vessel in vessels:
                    # Create subgraph for vessel
                    if vessel not in possible_phaseIn_to_vessel:
                        possible_phaseIn_to_vessel[vessel] = set()

                    heu_subgraph = heuristic_subgraph.Subgraph()
                    heu_subgraph.create(instance, [vessel], [service])

                    # Route vessel to each phase-in slot if possible and store the objective
                    for phaseIn in service_phaseIn[pos]:
                        if phaseIn not in possible_vessel_to_phaseIn:
                            possible_vessel_to_phaseIn[phaseIn] = set()

                        if phaseIn in heu_subgraph.V:
                            if (phaseIn, vessel) not in self.vessel_to_phaseIn:
                                feasible_solution, overall_objective, vessels_path, vessels_objective = \
                                    heuristic_models.calculate_vessels_path(instance, heu_subgraph, 2, [vessel], first_goal, second_goals=[phaseIn])

                                if feasible_solution:
                                    self.vessel_to_phaseIn[(phaseIn, vessel)] = vessels_objective[0]
                                else:
                                    self.vessel_to_phaseIn[(phaseIn, vessel)] = None
                        else:
                            self.vessel_to_phaseIn[(phaseIn, vessel)] = None

                        if self.vessel_to_phaseIn[(phaseIn, vessel)]:
                            possible_phaseIn_to_vessel[vessel].add(phaseIn)
                            possible_vessel_to_phaseIn[phaseIn].add(vessel)

            # Create assignment based on the objective of all possible vessel paths
            start_alloc = heuristic_models.calculate_assignment(instance, self.services, self.vessel_to_phaseIn,
                                                                     possible_phaseIn_to_vessel,
                                                                     possible_vessel_to_phaseIn)

            self.solved_assignment_starts[tuple(fdp_allocation)] = start_alloc

        # Load or create solution based on allocation
        if start_alloc in self.solved_fdp_allocs:
            solution = self.solved_fdp_allocs[start_alloc]
        else:
            solution = self.solve_start_alloc(instance, start_alloc)
            self.solved_fdp_allocs[start_alloc] = solution

        return solution

    def create_random_start(self, instance, fdp_allocation):
        """
        Create a random start solution
        """

        # First feasible start allocation is used for solution
        while True:
            start_alloc = create_random_alloc(instance, fdp_allocation)

            if start_alloc in self.solved_fdp_allocs:
                solution = self.solved_fdp_allocs[start_alloc]
            else:
                solution = self.solve_start_alloc(instance, start_alloc)
                self.solved_fdp_allocs[start_alloc] = solution

            if solution or self.time_check():
                break

        return solution

    def solve_start_alloc(self, instance, start_alloc):
        """
        Generates a feasible solution (if possible) for given start allocation
        """

        solution = heuristic_solution.Solution(instance)

        for pos, vessels in enumerate(start_alloc):
            service = self.services[pos]
            goals = [instance.tau[service]] * len(vessels)

            # Stop if time limit is reached
            if time.time() - self.overall_time_start \
                    + 2 * (time.time() - self.overall_time_start) / self.counter_solved_models \
                    > self.overall_time_limit:
                self.time_limit_reached = True
                break

            if (service, vessels) not in self.solved_fdp_vessels:
                # Create paths for vessels
                heu_subgraph = heuristic_subgraph.Subgraph()
                heu_subgraph.create(instance, vessels, [service])

                if heu_subgraph.feasible:
                    feasible_solution, overall_objective, vessels_path, vessels_objective = \
                        heuristic_models.calculate_vessels_path(instance, heu_subgraph, 0, vessels, goals)

                    if feasible_solution:
                        self.solved_fdp_vessels[(service, vessels)] = (vessels_path, vessels_objective)
                        solution.add_vessels_to_service(instance, service, vessels, vessels_path, vessels_objective)
                    else:
                        self.solved_fdp_vessels[(service, vessels)] = None
                else:
                    self.solved_fdp_vessels[(service, vessels)] = None
            else:
                # Use already existing paths
                if self.solved_fdp_vessels[(service, vessels)]:
                    solution.add_vessels_to_service(instance, service, vessels,
                                                    self.solved_fdp_vessels[(service, vessels)][0],
                                                    self.solved_fdp_vessels[(service, vessels)][1])

            self.counter_solved_models += 1

        solution.check_feasibility(instance)

        # Check and resolve possible conflicts
        if solution.vessel_conflicts:
            solution = self.solve_conflict(instance, solution)

        # Note, it is still possible to not find a feasible solution
        if solution and solution.feasible:
            solution.calc_instance_objective()
            self.solved_fdp_allocs_random[start_alloc] = solution
        else:
            solution = None
            self.solved_fdp_allocs_random[start_alloc] = solution

        return solution

    def solve_conflict(self, instance, solution):
        """
        Tries to remove conflicts (vessels visiting the same nodes) from a solution
        """

        vessel_conflicts = list(solution.vessel_conflicts)

        first_goals = []
        second_goals = []

        for vessel in vessel_conflicts:
            first_goals.append(solution.vessel_path[vessel][-1])
            second_goals.append(solution.vessel_path[vessel][-2])

        # List of nodes already visited by other vessels
        prohibited_ports = []
        for service, vessels in solution.service_vessels.items():
            for vessel in vessels:
                if vessel not in vessel_conflicts:
                    prohibited_ports.extend(solution.vessel_path[vessel][:-1])

        heu_subgraph = heuristic_subgraph.Subgraph()

        # Find services that have a conflict
        service_conflicts = set()
        for service, vessels in solution.service_vessels.items():
            for vessel in vessels:
                if vessel in vessel_conflicts:
                    service_conflicts.add(service)

        # Create needed subgraph and update nodes that are already visited by other vessels
        heu_subgraph.create(instance, vessel_conflicts, list(service_conflicts))
        prohibited_ports = [port for port in prohibited_ports if port in heu_subgraph.V]

        # Solve subproblem
        feasible_solution, overall_objective, vessels_path, vessels_objective = \
            heuristic_models.calculate_vessels_path(instance, heu_subgraph, 2, vessel_conflicts,
                                                         first_goals, second_goals=second_goals,
                                                         prohibited_ports=prohibited_ports)

        self.counter_solved_models += 1

        # Check if conflict could be solved
        if feasible_solution:
            solution.update_vessels(vessel_conflicts, vessels_path, vessels_objective)
            solution.calc_instance_objective()
            solution.check_feasibility(instance)

            if solution.feasible:
                return solution
        else:
            return None

    def perform_ls(self, instance, solution):
        """
        Performs a local search on a given solution. Strategy hill climbing by swap picking first improvement
        """

        current_solution = solution
        improvement = 0.01 * abs(solution.objective)

        while improvement > 0.001 * abs(current_solution.objective):
            improvement = 0

            # Determine used vessels
            current_vessels = []
            for service in self.services:
                current_vessels.append(current_solution.service_vessels[service])

            # Create possible neighbors on used vessels
            neighbors = create_neighborhood(instance, current_vessels)

            while neighbors:
                if self.time_check():
                    break

                neighbor = random.choice(neighbors)

                # Create or load neighbor solution
                if neighbor not in self.solved_fdp_allocs:
                    new_solution = heuristic_solution.Solution(instance)

                    for pos, vessels in enumerate(neighbor):

                        if self.time_check():
                            break

                        service = self.services[pos]
                        goals = [instance.tau[service]] * len(vessels)

                        # Solve or load vessel service combination
                        if (service, vessels) not in self.solved_fdp_vessels:
                            heu_subgraph = heuristic_subgraph.Subgraph()
                            heu_subgraph.create(instance, vessels, [service])

                            if heu_subgraph.feasible:
                                feasible_solution, overall_objective, vessels_path, vessels_objective = \
                                    heuristic_models.calculate_vessels_path(instance, heu_subgraph, 0, vessels, goals)

                                if feasible_solution:
                                    self.solved_fdp_vessels[(service, vessels)] = (vessels_path, vessels_objective)
                                    new_solution.add_vessels_to_service(instance, service, vessels, vessels_path,
                                                                        vessels_objective)
                                else:
                                    self.solved_fdp_vessels[(service, vessels)] = None
                            else:
                                self.solved_fdp_vessels[(service, vessels)] = None
                        else:
                            if self.solved_fdp_vessels[(service, vessels)]:
                                new_solution.add_vessels_to_service(instance, service, vessels,
                                                                    self.solved_fdp_vessels[(service, vessels)][0],
                                                                    self.solved_fdp_vessels[(service, vessels)][1])

                        self.counter_solved_models += 1

                    new_solution.check_feasibility(instance)

                else:
                    new_solution = self.solved_fdp_allocs[neighbor]

                # Check for conflicts
                if new_solution and new_solution.vessel_conflicts:
                    new_solution = self.solve_conflict(instance, new_solution)

                # Update neighbor
                if neighbor not in self.solved_fdp_allocs:
                    self.solved_fdp_allocs[neighbor] = new_solution

                # Check for improvement
                if new_solution and new_solution.feasible:
                    new_solution.calc_instance_objective()
                    if current_solution.objective < new_solution.objective:
                        improvement = new_solution.objective - current_solution.objective
                        current_solution = new_solution
                        break

                neighbors.remove(neighbor)

        return current_solution

    def time_check(self):
        """
        Check if the overall time limit would be reached with next execution
        """

        if time.time() - self.overall_time_start \
                + 3 * (time.time() - self.overall_time_start) / self.counter_solved_models \
                > self.overall_time_limit:
            self.time_limit_reached = True

        return self.time_limit_reached


def create_random_alloc(instance, fdp_allocation):
    """
    Creates a random allocation, does not have to be feasible
    """
    needed_vessel_types = set()
    vessels = {}
    vessel_alloc = []

    # Collect possible vessels
    for pos, alloc in enumerate(fdp_allocation):
        needed_vessel_types.add(alloc[0])
        vessel_alloc.append([])

    for vessel_type in needed_vessel_types:
        vessels[vessel_type] = list(instance.vessels_by_class[vessel_type])

    possible_alloc_pos = list(range(0, len(fdp_allocation)))

    # Pick vessels until all allocations are satisfied
    while possible_alloc_pos:
        pos = random.choice(possible_alloc_pos)

        vessel = random.choice(vessels[fdp_allocation[pos][0]])
        vessel_alloc[pos].append(vessel)
        vessels[fdp_allocation[pos][0]].remove(vessel)

        if len(vessel_alloc[pos]) == fdp_allocation[pos][1]:
            possible_alloc_pos.remove(pos)

    vessel_alloc = tuple([tuple(sorted(vessels)) for vessels in vessel_alloc])

    return vessel_alloc


def create_neighborhood(instance, group_one_vessels):
    """
    Create neighbors based on vessel swaps
    """
    neighbors = set()

    for group in group_one_vessels:
        for vessel in group:
            vessels_exluded_from_swap = group
            vessel_to_swap = vessel

            # Swap only possible with vessel from same type
            vessels_ready_for_swap = list(instance.vessels_by_class[instance.vessel_class[vessel_to_swap]])
            for vessel2 in vessels_exluded_from_swap:
                vessels_ready_for_swap.remove(vessel2)

            # Perform swap
            for swap_vessel in vessels_ready_for_swap:
                new_vessel_alloc = []

                for vessels in group_one_vessels:
                    new_vessels = vessels[:]

                    if swap_vessel in new_vessels:
                        new_vessels.remove(swap_vessel)
                        new_vessels.append(vessel_to_swap)
                    elif vessel_to_swap in new_vessels:
                        new_vessels.remove(vessel_to_swap)
                        new_vessels.append(swap_vessel)

                    new_vessel_alloc.append(tuple(sorted(new_vessels)))
                neighbors.add(tuple(new_vessel_alloc))

    return list(neighbors)


def choose_rankings(ranking, nn, fdp_frp_objective, first=False):
    """
    Returns fdp ranking
    """

    if first:
        return ranking[:nn]
    else:
        amount_best = math.ceil(nn * random.random())

        not_solved = []
        solved = []

        for rank in ranking:
            if rank in fdp_frp_objective:
                if solved:
                    for pos, rank2 in enumerate(solved):
                        if fdp_frp_objective[rank2] < fdp_frp_objective[rank]:
                            solved.insert(pos, rank)
                            break
                    else:
                        solved.append(rank)
                else:
                    solved.append(rank)
            else:
                not_solved.append(rank)

        new_ranking = solved[:amount_best]
        new_ranking.extend(not_solved[:nn - len(new_ranking)])

        return new_ranking
