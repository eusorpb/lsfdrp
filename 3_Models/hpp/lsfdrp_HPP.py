﻿import argparse
import copy
from gurobipy import *
import pickle
import time

import heuristic_instance
import heuristic_solution
import heuristic_subgraph
import lsfdrp_instance
import lsfdrp_models


def solve_lsfdrp(fr, fd):

    instance = heuristic_instance.Instance(fr, fd)
    start_time = time.time()
    service_class, service_amount, par_y, service_vessels = pickle.load(open("{0}_heuristic_solution.p".format(instance.filename), "rb"))

    service_subgraph = {}
    vessel_to_service = {}
    V = set()
    start_ports = set()
    V_vessels = {}
    A_vessels = {}
    M_vessels = {demand_id: set() for demand_id in instance.heu_M}

    # Create subgraph for each service and assigned vessels
    for service, vessels in sorted(service_vessels.items()):
        for vessel in vessels:
            vessel_to_service[vessel] = service

        service_subgraph[service] = heuristic_subgraph.Subgraph()
        service_subgraph[service].create(instance, vessels, [service])

        for vessel in vessels:
            for demand_id in service_subgraph[service].M:
                M_vessels[demand_id].add(vessel)

        for node in service_subgraph[service].V:
            V.add(node)
            if node not in V_vessels:
                V_vessels[node] = set()
            for vessel in vessels:
                V_vessels[node].add(vessel)
                
                for node2 in service_subgraph[service].next_nodes[node]:
                    if (node, node2) not in A_vessels:
                        A_vessels[(node, node2)] = set()
                    A_vessels[(node, node2)].add(vessel)

    # Create additional set
    V_wSink = copy.copy(V)

    for service, vessels in service_vessels.items():
        V_wSink.remove(instance.tau[service])
        for vessel in vessels:
            start_ports.add(instance.vStart[vessel])

    mm = Model("HPP")

    phi = {}
    xD = {}
    xE = {}
    yy = {}

    for service, vessels in service_vessels.items():
        for vessel in vessels:
            phi[vessel] = mm.addVar(name=f"phi[{vessel}]")

            for ii in service_subgraph[service].V:
                for jj in service_subgraph[service].next_nodes[ii]:
                    yy[vessel, ii, jj] = mm.addVar(vtype='B', name=f"y[{vessel},{ii},{jj}]")
                    for qq in instance.Q:
                        if (qq, ii, jj) not in xE:
                            xE[qq, ii, jj] = mm.addVar(name=f"xE[{qq},{ii},{jj}]")
                            
            for demand_id in service_subgraph[service].M:
                xD[demand_id, vessel] = mm.addVar(name=f"xD[{demand_id},{vessel}]")

    mm.update()

    #
    # Set objective
    #

    mm.setObjective(
            # less shipping cost
            - quicksum(quicksum(instance.cHotel[ss] * phi[ss] for ss in vessels) for service, vessels in service_vessels.items())
            - quicksum(quicksum(quicksum(quicksum(instance.heu_cSail[ss, ii, jj] * yy[ss, ii, jj] for jj in service_subgraph[service].next_nodes[ii]) for ii in
                    service_subgraph[service].V_wSink) for ss in vessels) for service, vessels in service_vessels.items())
            # less port fee
            - quicksum(quicksum(quicksum(quicksum(instance.heu_cPort[ss, jj] * yy[ss, ii, jj] for ii in service_subgraph[service].prev_nodes[jj]) for jj in
                    service_subgraph[service].V_wSink) for ss in vessels) for service, vessels in service_vessels.items())
            # plus revenue due to demand during repositioning
            + quicksum(quicksum(quicksum((instance.heu_rDemand[demand_id] - instance.heu_cMV[oo] - instance.heu_cMV[dd])
                    * xD[demand_id, ss] for demand_id, (oo, dd, qq) in service_subgraph[service].M.items()) for ss in vessels) for service, vessels in service_vessels.items())
        , GRB.MAXIMIZE)

    #
    # Constraints
    #

    # Vessel must reach their goal services
    for service, vessels in service_vessels.items():
        for ss in vessels:
            mm.addConstr(quicksum(yy[ss, ii, instance.tau[service]] for ii in service_subgraph[service].prev_nodes[instance.tau[service]]) == 1)

    # Vessels must leave their start port
    for service, vessels in service_vessels.items():
        for ss in vessels:
            mm.addConstr(quicksum(yy[ss, instance.vStart[ss], jj] for jj in service_subgraph[service].next_nodes[instance.vStart[ss]]) == 1)

    # Phase-In slots must be used in ascending order
    for service, vessels in service_vessels.items():
        for ii in service_subgraph[service].prev_nodes[instance.tau[service]]:
            for jj in (service_subgraph[service].prev_nodes[instance.tau[service]] - {ii}):
                if instance.delta[ii, jj] == 1:
                    mm.addConstr(quicksum(yy[ss, ii, instance.tau[service]] for ss in vessels) <=
                                quicksum(yy[ss, jj, instance.tau[service]] for ss in vessels))

    # Defines the time used by a vessel for the repositioning operation
    for service, vessels in service_vessels.items():
        for ss in vessels:
            mm.addConstr(phi[ss] >=
                          quicksum(instance.heu_tX[ii] * yy[ss, ii, instance.tau[service]] for ii in service_subgraph[service].prev_nodes[instance.tau[service]])
                          - instance.heu_tX[instance.vStart[ss]])

    # Only one vessel can visit a port visitation
    for jj in V_wSink:
        mm.addConstr(quicksum(quicksum(yy[ss, ii, jj] for ii in service_subgraph[vessel_to_service[ss]].prev_nodes[jj]) for ss in V_vessels[jj]) <= 1)

    # Only one vessel can leave a port visitation
    for ii in V_wSink:
        mm.addConstr(quicksum(quicksum(yy[ss, ii, jj] for jj in service_subgraph[vessel_to_service[ss]].next_nodes[ii]) for ss in V_vessels[ii]) <= 1)

    # Vessels can only stop in a graph sink
    for jj in V_wSink - start_ports:
        for ss in V_vessels[jj]:
            mm.addConstr(quicksum(yy[ss, ii, jj] for ii in service_subgraph[vessel_to_service[ss]].prev_nodes[jj])
                         - quicksum(yy[ss, jj, kk] for kk in service_subgraph[vessel_to_service[ss]].next_nodes[jj]) == 0)

    # Demand can only be loaded on a vessel if the origin is visited
    for demand_id, (oo, dd, qq) in instance.heu_M.items():
        for ss in M_vessels[demand_id]:
            mm.addConstr(xD[demand_id, ss] <= instance.heu_amount[demand_id] * quicksum(yy[ss, oo, jj] for jj in service_subgraph[vessel_to_service[ss]].next_nodes[oo]))

    # Demand can only be loaded on a vessel if the destination is visited
    for demand_id, (oo, dd, qq) in instance.heu_M.items():
        for ss in M_vessels[demand_id]:
            mm.addConstr(xD[demand_id, ss] <= instance.heu_amount[demand_id] * quicksum(yy[ss, ii, dd] for ii in service_subgraph[vessel_to_service[ss]].prev_nodes[dd]))

    # The overall amount of containers cannot surpass the vessels capacity
    for service, vessels in service_vessels.items():
        for ss in vessels:
            for jj in service_subgraph[service].V_wSink:
                mm.addConstr(quicksum(xD[demand_id, ss] for demand_id in service_subgraph[service].M_Vis[jj]) + quicksum(xE[qq, ii, jj] for qq in instance.Q for ii in service_subgraph[service].prev_nodes[jj])
                             <= instance.u_dc[ss])

    # The amount of reefer container cannot surpass the capacity of the vessel
    for service, vessels in service_vessels.items():
        for ss in vessels:
            for ii in service_subgraph[service].V_wSink:
                mm.addConstr(quicksum(xD[demand_id, ss] for demand_id in service_subgraph[service].M_Vis_rf[ii]) <= instance.u_rf[ss])

    # Equipment can only flow if a vessel is sailing on the arc
    for (ii, jj), vessels in A_vessels.items():
        mm.addConstr(quicksum(xE[qq, ii, jj] for qq in instance.Q) <= quicksum(instance.u_dc[ss] * yy[ss, ii, jj] for ss in vessels))

    # The amount of single-demands cannot exceed the amount of the original demand
    for service, vessels in service_vessels.items():
        for ss in vessels:
            for multi_demands in service_subgraph[service].multi_demands:
                mm.addConstr(quicksum(xD[demand_id, ss] for demand_id in multi_demands) <= service_subgraph[service].amount[multi_demands])

    # Vessels representing the same flexible visitation can in sum only be visited once
    for service, vessels in service_vessels.items():
        for multi_ports in service_subgraph[service].multi_ports:
            mm.addConstr(quicksum(quicksum(quicksum(yy[ss, ii, jj] for ii in service_subgraph[vessel_to_service[ss]].prev_nodes[jj]) for ss in V_vessels[jj]) for jj in multi_ports) <= 1)

    # Optimize with four threads
    mm.setParam('Threads', 4)
    mm.optimize()
    print("Took ", time.time()-start_time)

    # Parse solution
    if mm.status == 2:

        vessels_arcs = {}

        for var in mm.getVars():
            if var.x > 0.1:
                if "y" in var.VarName:
                    ss, ii, jj = [int(x) for x in var.VarName[2:var.VarName.find("]")].split(",")]

                    if ss not in vessels_arcs:
                        vessels_arcs[ss] = []
                    vessels_arcs[ss].append((ii, jj))

        all_vessels_path = {}

        for service, vessels in sorted(service_vessels.items()):
            for ss in vessels:
                if vessels_arcs[ss]:
                    path = [instance.vStart[ss]]
                    while path[-1] != instance.tau[service]:
                        for ii, jj in vessels_arcs[ss]:
                            if ii == path[-1]:
                                path.append(jj)
                else:
                    path = None

                all_vessels_path[ss] = path

        solution = heuristic_solution.Solution(instance)

        for service, vessels in service_vessels.items():
            vessels_path = []
            vessels_obj = []
            for ss in vessels:
                vessels_path.append(all_vessels_path[ss])
                vessels_obj.append(0)
            solution.add_vessels_to_service(instance, service, vessels, vessels_path, vessels_obj)

        # Transfer and solve solution with original model to have an additional feasibility check and print solution
        lf_instance = lsfdrp_instance.Instance(fr, fd)
        lf_instance.adapt_to_solution(instance, solution)
        lsfdrp_models.solve_arc_flow(lf_instance)


if __name__ == "__main__":
    args = None
    parser = argparse.ArgumentParser(description='LSFDRP HPP')
    parser.add_argument('-fr', '--fleetrepositioning', type=str, help='Path to the lsfrp-instance to be solved', required=True)
    parser.add_argument('-fd', '--fleetdeployment', type=str, help='Path to the lsfdp-instance to be solved', required=True)
    args = parser.parse_args()

    if not os.path.exists(args.fleetrepositioning):
        print("Instance LSFRP not found: {0}".format(args.fleetrepositioning), file=sys.stderr)
        sys.exit(1)
    if not os.path.exists(args.fleetdeployment):
        print("Instance LSFDP not found: {0}".format(args.fleetdeployment), file=sys.stderr)
        sys.exit(1)
        
    # Note: error handling omitted; add your own if desired.
    fr = args.fleetrepositioning
    fd = args.fleetdeployment

    # Create Gurobi Academic output
    m = Model()
    print()

    print("Instance LSFRP: {0}".format(fr))
    print("Instance LSFDP: {0}\n".format(fd))

    print(lsfdrp_instance.Instance(fr, fd))

    solve_lsfdrp(fr, fd)
