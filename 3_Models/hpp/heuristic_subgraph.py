import copy
import time


class Subgraph(object):
    """
    Contains a subgraph for a specific set of vessels and services
    """

    def __init__(self):

        self.V = set()
        self.V_wSink = set()
        self.next_nodes = {}
        self.prev_nodes = {}
        self.M = {}
        self.M_Vis = {}
        self.M_Vis_rf = {}
        self.amount = {}
        self.multi_demands = []
        self.multi_ports = []
        self.feasible = True

    def create(self, instance, vessels, services):
        """
        Creates a subgraph for the given vessels and services
        """

        start = time.time()

        copy_next_nodes = copy.deepcopy(instance.heu_next_nodes)
        copy_prev_nodes = copy.deepcopy(instance.heu_prev_nodes)

        node_counter = {node: 0 for node in instance.heu_V}
        copy_next_nodes["start"] = [instance.vStart[ss] for ss in vessels]
        copy_prev_nodes["end"] = [instance.tau[service] for service in services]

        # Idea, push through the graph from the vessel starts (front) and graph sink (back) and label visited nodes. Nodes with two labels belong to
        # the subgraph

        # Front
        visited = set()
        queue = ["start"]
        while len(queue) > 0:
            current_node = queue.pop(0)
            visited.add(current_node)

            for next_node in copy_next_nodes[current_node]:
                if next_node not in visited:
                    queue.append(next_node)
                    visited.add(next_node)
                    node_counter[next_node] += 1

        # Back
        visited = set()
        queue = ["end"]
        while len(queue) > 0:
            current_node = queue.pop(0)
            visited.add(current_node)

            for prev_node in copy_prev_nodes[current_node]:
                if prev_node not in visited:
                    queue.append(prev_node)
                    visited.add(prev_node)
                    node_counter[prev_node] += 1

        # Check which nodes belong to subgraph
        for node, value in node_counter.items():
            if value == 2:
                self.V_wSink.add(node)
                self.next_nodes[node] = set()
                self.prev_nodes[node] = set()

        self.V = copy.copy(self.V_wSink)
        for service in services:
            self.V_wSink.remove(instance.tau[service])

        # Check which arcs belong to subgraph
        for ii in self.V:
            for jj in instance.heu_next_nodes[ii]:
                if jj in self.V:
                    self.next_nodes[ii].add(jj)
                    self.prev_nodes[jj].add(ii)

        # Check which demands belong to subgraph
        for demand_id, (oo, dd, qq) in instance.heu_M.items():
            if oo in self.V and dd in self.V:
                self.M[demand_id] = (oo, dd, qq)

        self.M_Vis = {node: set() for node in self.V}
        self.M_Vis_rf = {node: set() for node in self.V}

        for node, demand_ids in instance.heu_M_Vis.items():
            if node in self.V:
                for demand_id in demand_ids:
                    if demand_id in self.M:
                        self.M_Vis[node].add(demand_id)

        for node, demand_ids in instance.heu_M_Vis_rf.items():
            if node in self.V:
                for demand_id in demand_ids:
                    if demand_id in self.M:
                        self.M_Vis_rf[node].add(demand_id)

        for demand_id, amount in instance.heu_amount.items():
            if demand_id in self.M:
                self.amount[demand_id] = amount

        # Consider multi demands
        for multi_demands in instance.heu_multi_demands:

            new_multi_demands = []
            for demand_id in multi_demands:
                if demand_id in self.M:
                    new_multi_demands.append(demand_id)
            if new_multi_demands:
                new_multi_demands = tuple(new_multi_demands)
                self.multi_demands.append(new_multi_demands)
                self.amount[new_multi_demands] = instance.heu_amount[multi_demands]

        # Consider multi ports
        for multi_ports in instance.heu_multi_ports:
            if multi_ports:
                new_multi_ports = []

                for port in multi_ports:
                    if port in self.V:
                        new_multi_ports.append(port)
                if new_multi_ports:
                    self.multi_ports.append(frozenset(new_multi_ports))

        # Check if service end could be reached
        for service in services:
            for node in instance.heu_prev_nodes[instance.tau[service]]:
                if node not in self.V:
                    self.feasible = False
