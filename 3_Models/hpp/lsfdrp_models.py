from gurobipy import *

import lsfdrp_solution


def solve_arc_flow(instance, output_flag=False, threads=4, time_limit=86400):

    # Create optimization model
    #
    mm = Model('fleetRedeployment')

    # Create variables
    #
    eta = {}
    rho = {}
    phi = {}

    for rr in instance.services:
        for kk in instance.vessel_types:
            rho[rr, kk] = mm.addVar(vtype='B', name=f"rho[{rr},{kk}]")

    for rr in instance.services:
        for kk in instance.vessel_types:
            for ii in range(1, instance.vessel_amount_max[rr]+1):
                eta[rr, kk, ii] = mm.addVar(vtype='B', name=f"eta[{rr},{kk},{ii}]")

    for ss in instance.vessels:
        phi[ss] = mm.addVar(name=f"phi[{ss}]")

    xx_demand = {}
    xx_equipment = {}
    yy = {}

    for demand_id in instance.demands:
        for ii, jj in instance.arcs:
            xx_demand[demand_id, ii, jj] = mm.addVar(name=f"xx_demand[{demand_id},{ii},{jj}]")

    for qq in instance.container_types:
        for ii, jj in instance.arcs:
            xx_equipment[qq, ii, jj] = mm.addVar(name=f"xx_equipment[{qq},{ii},{jj}]")

    for ss in instance.vessels:
        for ii, jj in instance.arcs:
            yy[ss, ii, jj] = mm.addVar(vtype='B', name=f"yy[{ss},{ii},{jj}]")

    ww = {}
    zz_enter = {}
    zz_exit = {}

    for ss in instance.vessels:
        for ii, jj in instance.arcs_flexible:
            ww[ss, ii, jj] = mm.addVar(name=f"ww[{ss},{ii},{jj}]")

    for ii in instance.nodes:
        if ii not in instance.sinks.values():
            zz_enter[ii] = mm.addVar(name=f"zz_enter[{ii}]")

    for ii in instance.nodes:
        if ii not in instance.sinks.values():
            zz_exit[ii] = mm.addVar(name=f"zz_exit[{ii}]")

    mm.update()

    #
    # Objective
    #
    
    mm.setObjective(
        # minus shipping costs
        - quicksum(instance.costs_hotel[ss] * phi[ss]
                   + quicksum(instance.costs_sailing[ss, ii, jj] * yy[ss, ii, jj] for ii, jj in instance.arcs_without_sinks)
                   + quicksum(instance.costs_variable_sailing[ss, ii, jj] * ww[ss, ii, jj] for ii, jj in instance.arcs_flexible)
                   for ss in instance.vessels)

        # minus port fees
        - quicksum(quicksum(quicksum(instance.costs_port[ss, jj] * yy[ss, ii, jj] for ss in instance.vessels)
                            for (ii, jj) in instance.arcs if jj == jj2)
                   for jj2 in instance.nodes_without_sinks)

        # plus revenue due to demand during repositioning
        + quicksum(quicksum(quicksum((instance.revenue_demand[demand_id] - instance.costs_movement[oo] - instance.costs_movement[jj]) * xx_demand[demand_id, ii, jj] for (ii, jj) in instance.arcs if jj == jj2)
                            for jj2 in dd)
                   for demand_id, (oo, dd, qq) in instance.demands.items())

        # plus revenue due to equipment during repositioning
        + quicksum(quicksum(quicksum((instance.revenue_equipment[qq] - instance.costs_movement[ii]) * xx_equipment[qq, ii, jj] for (ii, jj) in instance.arcs if ii == ii2)
                            for ii2 in instance.nodes_ePlus[qq])
                   - quicksum(quicksum(instance.costs_movement[ii] * xx_equipment[qq, ii, jj] for (ii, jj) in instance.arcs if jj == jj2)
                              for jj2 in instance.nodes_eMinus[qq])
                   for qq in instance.container_types)

        # minus costs for target routes
        - quicksum(quicksum(quicksum(instance.costs_service[rr, kk, ii] * eta[rr, kk, ii] for ii in range(1, instance.vessel_amount_max[rr]+1))
                            for kk in instance.vessel_types)
                   for rr in instance.services)

        # minus costs for chartering at target routes
        - quicksum(quicksum(quicksum(instance.costs_charter[ss, rr] * yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr])
                            for rr in instance.services)
                   for ss in instance.vessels_chartered)

        # plus charter revenue
        + quicksum(instance.revenue_charter[ss] * (1 - quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr])
                                                                for rr in instance.services))
                   for ss in instance.vessels_owned)
        
        , GRB.MAXIMIZE)

    #
    # Constraints
    #
    
    # 4.8 controls the amount of vessels reaching the target routes
    for rr in instance.services:
        mm.addConstr(quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr]) for ss in instance.vessels)
                     == quicksum(quicksum(eta[rr, kk, ii] for ii in range(1, instance.vessel_amount_max[rr]+1)) for kk in instance.vessel_types))

    # 4.9 sets type of vessel for each route
    for rr in instance.services:
        mm.addConstr(quicksum(rho[rr, kk] for kk in instance.vessel_types) == 1)

    # 4.10 controls type restriction for each route
    for rr in instance.services:
        for kk in instance.K_notOnR[rr]:
            mm.addConstr(rho[rr, kk] == 0)

    # 4.11 upperbound and lowerbound for number of vessels for each route
    for rr in instance.services:
        mm.addConstr(quicksum(quicksum(eta[rr, kk, ii] for ii in range(1, instance.vessel_amount_max[rr]+1))
                              for kk in instance.vessel_types)
                     <= instance.vessel_amount_max[rr])

    for rr in instance.services:
        mm.addConstr(quicksum(quicksum(eta[rr, kk, ii] for ii in range(1, instance.vessel_amount_max[rr]+1))
                              for kk in instance.vessel_types)
                     >= instance.vessel_amount_min[rr])

    # 4.12  Exclude vessels from service that are not allowed
    for rr in instance.services:
        for kk in instance.vessel_types:
            mm.addConstr(quicksum(eta[rr, kk, ii] for ii in range(1, instance.vessel_amount_max[rr] + 1))
                         <= instance.vessel_amount_max[rr] * rho[rr, kk])

    # 4.13 if vessel i is used on route, then vessel i-1,too
    for rr in instance.services:
        for kk in instance.vessel_types:
            for ii in range(2, instance.vessel_amount_max[rr]+1):
                mm.addConstr(eta[rr, kk, ii-1] >= eta[rr, kk, ii])

    # 4.14 controls, that if the vessel is used on the route, the type of the vessel is the same as the type used on the route
    for ss in instance.vessels:
        for rr in instance.services:
            for kk in instance.vessel_types:
                mm.addConstr(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr]) + instance.vessel_type[ss, kk] - rho[rr, kk] <= 1)

    # 4.15 same as 4.14
    for ss in instance.vessels:
        for rr in instance.services:
            for kk in instance.vessel_types:
                mm.addConstr(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr]) - instance.vessel_type[ss, kk] + rho[rr, kk] <= 1)

    # 4.16 controls, that a vessel can can only be part of one target route
    for ss in instance.vessels:
        mm.addConstr(quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr]) for rr in instance.services) <= 1)
        
    # 4.17 controls that only one vessel can visit a visitation at the same time
    for jj2 in instance.nodes_without_sinks:
        mm.addConstr(quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == jj2) for ss in instance.vessels) <= 1)        
        
    # 4.18 flow balance vessels (only end at graph sink)
    for jj2 in instance.nodes_without_sinks:
        if jj2 not in instance.vessel_start.values() and jj2 not in instance.sinks.values():
            for ss in instance.vessels:
                mm.addConstr(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == jj2) - quicksum(yy[ss, jj, kk] for (jj, kk) in instance.arcs if jj == jj2) == 0)       
        
    # 4.19 defines the time used by a vessel for the repositioning operation
    for ss in instance.vessels:
        mm.addConstr(quicksum(quicksum(instance.time_exit[ii] * yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr])
                              for rr in instance.services) - instance.time_exit[instance.vessel_start[ss]]
                     <= phi[ss])

    # 4.20 controls that vessels leave their starting point, if used on a route
    for ss in instance.vessels:
        mm.addConstr(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if ii == instance.vessel_start[ss])
                     == quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == instance.sinks[rr])
                                 for rr in instance.services))

    # 4.21 controls that phase-in slots are taken in time ascending order
    for rr in instance.services:
        for (ii, jj) in instance.arcs:
            for (ii2, jj2) in instance.arcs:
                if jj == instance.sinks[rr] and jj2 == instance.sinks[rr] and ii != ii2:
                    if instance.delta[ii, ii2] == 1:
                        mm.addConstr(quicksum(yy[ss, ii, jj] for ss in instance.vessels)
                                     <= quicksum(yy[ss, ii2, jj2] for ss in instance.vessels))

    # 4.22 controls the refrigerated capacity of a vessel for the time of repositioning
    for (ii, jj) in instance.arcs_without_sinks:
        mm.addConstr(quicksum(xx_demand[demand_id, ii, jj] for demand_id, (oo, dd, qq) in instance.demands.items() if qq == "rf")
                     <= quicksum(instance.vessel_capacity_reefer[ss] * yy[ss, ii, jj] for ss in instance.vessels))

    # 4.23 controls the overall capacity of a vessel for the time of repositioning
    for (ii, jj) in instance.arcs_without_sinks:
        mm.addConstr(quicksum(xx_demand[demand_id, ii, jj] for demand_id in instance.demands)
                     + quicksum(xx_equipment[qq, ii, jj] for qq in instance.container_types)
                     <= quicksum(instance.vessel_capacity[ss] * yy[ss, ii, jj] for ss in instance.vessels))

    # 4.24 controls the amount of demand leaving the origin
    for demand_id, (oo, dd, qq) in instance.demands.items():
        mm.addConstr(quicksum(xx_demand[demand_id, ii, jj] for (ii, jj) in instance.arcs if ii == oo)
                     <= instance.amount_demand[demand_id] * quicksum(quicksum(yy[ss, oo, jj] for (ii, jj) in instance.arcs if ii == oo)
                                                                     for ss in instance.vessels))

    # 4.24 controls that demand doesn't leave their destination (not needed in the model because it is clear, but here needed)
    for demand_id, (oo, dd, qq) in instance.demands.items():
        for dd2 in dd:
            mm.addConstr(quicksum(xx_demand[demand_id, ii, jj] for (ii, jj) in instance.arcs if ii == dd2) == 0)

    # 4.25 guaranteed, that demand arrives at the destination (flow balance)
    for demand_id, (oo, dd, qq) in instance.demands.items():
        for jj2 in instance.nodes - ({oo} | dd):
            mm.addConstr(quicksum(xx_demand[demand_id, ii, jj] for (ii, jj) in instance.arcs if jj == jj2)
                         - quicksum(xx_demand[demand_id, jj, kk] for (jj, kk) in instance.arcs if jj == jj2)
                         == 0)

    # 4.26 guaranteed, that equipment arrives at the destination (flow balance)
    for qq in instance.container_types:
        for jj2 in instance.nodes_without_sinks - instance.nodes_ePlusMinus[qq]:
            mm.addConstr(quicksum(xx_equipment[qq, ii, jj] for (ii, jj) in instance.arcs if jj == jj2)
                         - quicksum(xx_equipment[qq, jj, kk] for (jj, kk) in instance.arcs if jj == jj2)
                         == 0)

    # 4.27 sets duration for flexible arc, if used
    for (ii, jj) in instance.arcs_flexible:
        for ss in instance.vessels:
            mm.addConstr(instance.duration_max[ii, jj, ss] * yy[ss, ii, jj] >= ww[ss, ii, jj])

    for (ii, jj) in instance.arcs_flexible:
        for ss in instance.vessels:
            mm.addConstr(instance.duration_min[ii, jj, ss] * yy[ss, ii, jj] <= ww[ss, ii, jj])

    # 4.28 sets the enter time of a inflexible visitation, if visited by a vessel
    for jj2 in instance.nodes_inflexible:
        mm.addConstr(instance.time_enter[jj2] * quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == jj2)
                                                         for ss in instance.vessels)
                     == zz_enter[jj2])

    # 4.29 sets the exit time of a inflexible visitation, if visited by a vessel
    for ii2 in instance.nodes_inflexible:
        mm.addConstr(instance.time_exit[ii2] * quicksum(quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if ii == ii2)
                                                        for ss in instance.vessels)
                     == zz_exit[ii2])

    # 4.30 sets the possible enter and exit time of a flexible visitation
    for (ii, jj) in instance.arcs_flexible:
        for ss in instance.vessels:
            mm.addConstr(zz_exit[ii] + ww[ss, ii, jj] <= zz_enter[jj] + instance.big_m * (1 - yy[ss, ii, jj]))

    # 4.31 controlls the time used for a flexible visitation
    for node in instance.nodes_flexible:
        for ss in instance.vessels:
            mm.addConstr(quicksum(quicksum(instance.time_movement[ss, ii] * xx_demand[demand_id, ii, jj] for (ii, jj) in instance.arcs if ii == oo)
                                  for demand_id, (oo, dds, qq) in instance.m_orig[node].items())
                         + quicksum(quicksum(quicksum(instance.time_movement[ss, jj] * xx_demand[demand_id, ii, jj] for (ii, jj) in instance.arcs if jj == dd)
                                             for dd in dds)
                                    for demand_id, (oo, dds, qq) in instance.m_dest[node].items())
                         + quicksum(quicksum(quicksum(instance.time_movement[ss, ii] * xx_equipment[qq, ii, jj] for (ii, jj) in instance.arcs if ii == ii2)
                                             for ii2 in {node} & instance.nodes_ePlus[qq])
                                    + quicksum(quicksum(instance.time_movement[ss, jj] * xx_equipment[qq, ii, jj] for (ii, jj) in instance.arcs if jj == jj2)
                                               for jj2 in {node} & instance.nodes_eMinus[qq])
                                    for qq in instance.container_types)
                         - zz_exit[node] + zz_enter[node]
                         + instance.time_port[node] * quicksum(yy[ss, ii, jj] for (ii, jj) in instance.arcs if jj == node)
                         <= 0)

    # 4.32 correct start time for flexible nodes
    for ss in instance.vessels:
        if instance.vessel_start[ss] in instance.nodes_flexible:
            mm.addConstr(quicksum(yy[ss, ii, jj] * instance.time_enter[jj] for (ii, jj) in instance.arcs_flexible if ii == instance.vessel_start[ss])
                         - quicksum(ww[ss, ii, jj] for (ii, jj) in instance.arcs_flexible if ii == instance.vessel_start[ss])
                         >= zz_exit[instance.vessel_start[ss]])

    if output_flag:
        mm.setParam("OutputFlag", 0)
    mm.setParam('Threads', threads)
    mm.setParam('TimeLimit', time_limit)
    mm.optimize()

    if not output_flag:
        print("\n### Instance")
        print("###", instance.filename)

        print("\n### Graph info")
        lenEqp = len(instance.nodes_ePlusMinus['dc']) + len(instance.nodes_ePlusMinus['rf'])
        print("### |S|: %s" % len(instance.vessels))
        print("### |S^E|: %s" % len(instance.vessels_owned))
        print("### |S^C|: %s" % len(instance.vessels_chartered))
        print("### |V|: %s" % len(instance.nodes))
        print("### |A|: %s" % len(instance.arcs))
        print("### |A^i|: %s" % len(instance.arcs_inflexible))
        print("### |A^f|: %s" % len(instance.arcs_flexible))
        print("### |M|: %s" % len(instance.demands))
        print("### |E|: %s" % lenEqp)

    solution = lsfdrp_solution.Solution(instance)

    if mm.status == 3 or mm.status == 4:
        print("### Objective: infeasible")
    else:
        # Parse solution into solution object
        paths = {}
        demands = []

        solution.feasible = True
        for var in mm.getVars():
            if var.x > 0.1:
                if "eta" in var.VarName:
                    rr, vtype, nn = [int(xx) for xx in var.VarName[4:var.VarName.find("]")].split(",")]

                    if nn > solution.service_info[rr][1]:
                        solution.service_info[rr] = (vtype, nn)
                elif "xx_demand" in var.VarName:
                    demand_id, ii, jj = [int(xx) for xx in var.VarName[10:var.VarName.find("]")].split(",")]

                    if jj in instance.demands[demand_id][1]:
                        demands.append((demand_id, instance.demands[demand_id][0], jj, var.x))
                elif "xx_equipment" in var.VarName:
                    print("Error\nError\nError")
                elif "yy" in var.VarName:
                    ss, ii, jj = [int(xx) for xx in var.VarName[3:var.VarName.find("]")].split(",")]

                    if ss not in paths:
                        paths[ss] = set()

                    paths[ss].add((ii, jj))
                elif "zz_enter" in var.VarName:
                    port = int(var.VarName[9:var.VarName.find("]")])
                    solution.port_enter[port] = int(round(var.x, 0))
                elif "zz_exit" in var.VarName:
                    port = int(var.VarName[8:var.VarName.find("]")])
                    solution.port_exit[port] = int(round(var.x, 0))

        solution.objective = mm.objVal
        solution.vessels_not_on_service = set(instance.vessels)
        for ss in paths:
            temp_path = [instance.vessel_start[ss]]

            for nn in range(1, len(paths[ss])+1):
                for (ii, jj) in paths[ss]:
                    if ii == temp_path[-1]:
                        temp_path.append(jj)

            solution.vessel_path[ss] = temp_path
            solution.vessels_not_on_service.remove(ss)

            solution.service_vessels[instance.sink_to_service[temp_path[-1]]].append(ss)

        for (demand_id, ii, jj, value) in demands:
            for ss, path in solution.vessel_path.items():
                if ii in path:
                    solution.vessel_demand[ss].append((demand_id, ii, jj, value))

    if output_flag:
        return solution
    else:
        solution.print_solution(instance)

