﻿import argparse
from gurobipy import *
import pickle

#
# Helpers
#

def createMOrig(i, M):
    MOrig = {}
    for id, (o, d, q) in M.items():
        if i == o:
            MOrig[id] = (o, d, q)
    return MOrig


def createMDest(i, M):
    MOrig = {}
    for id, (o, d, q) in M.items():
        if i in d:
            MOrig[id] = (o, d, q)
    return MOrig


def createVwSinkwStart(V_wSink, Ships, vStart):
    V_wSinkwStart = V_wSink.copy()
    for s in Ships:
        if vStart[s] in V_wSinkwStart:
            V_wSinkwStart.remove(vStart[s])
    return V_wSinkwStart


def In(node, A):
    nodes = set()
    for i, j in A:
        if j == node:
            nodes.add(i)
    return nodes


def Out(node, A):
    nodes = set()
    for i, j in A:
        if i == node:
            nodes.add(j)
    return nodes


def getBigM(tau, tE):
    time = 0
    for element in tau.items():
        if tE[element[1]] > time:
            time = tE[element[1]]
    return time



def solve_lsfdrp(fr, fd):
    """
    Solve the LSFDRP problem with the linear model
    """

    # Load data
    Routes, K, Ships, ShipsE, ShipsC, cHotel, A, A_wSink, A_inflex, A_flex, V, V_wSink, V_inflex, V_flex, Q, M, RPO, u_rf, u_dc, vStart, p, \
        tau, delta, cMV, cPort, cSail, cVarSail, rDemand, rEqp, amount, V_ePlus, V_eMinus, V_ePlusMinus, tMV, tE, tX, tP, d_min, \
        d_max, unlocode = pickle.load(open(fr, "rb"))
    
    fdRoutes, fdK, cCharter, rCharter, n_min, n_max, cRoute, K_notOnR, filename = pickle.load(open(fd, "rb"))
  
    if fdRoutes != Routes or fdK != K:
        print("Data for routes or fessel classes does not match!")
        print("From fr:\n {0}\n {1}".format(Routes, K))
        print("From fd:\n {0}\n {1}".format(fdRoutes, fdK))
    else:

        #
        # Create optimization model
        #

        m = Model('fleetRedeployment')
        
        #
        # Create variables
        #
        
        # indicates, if type k of vessel will be used on route r
        rho = {}
        for r in Routes:
            for k in K:
                rho[r, k] = m.addVar(vtype='B', name="rho[%s,%s]" % (r, k))
        
        # indicates, if vessel i of type k will be used on route r
        eta = {}
        for r in Routes:
            for k in K:
                for i in range(1, n_max[r] + 1):
                    eta[r, k, i] = m.addVar(vtype='B',
                                            name="eta[%s,%s,%s]" % (r, k, i))
        
        # indicates, if vessel s sails on arc (i,j)
        y = {}
        for s in Ships:
            for i, j in A:
                y[s, i, j] = m.addVar(vtype='B', name="y[%s,%s,%s]" % (s, i, j))
        
        # amount of flow of demand triplet (o,d,q) on arc (i,j)
        xD = {}
        for id, (o,d,q) in M.items():
            for i, j in A:
                xD[id, i, j] = m.addVar(lb=0, name="xD[%s,%s,%s]" % (id, i, j))
          
        # amount of equipment q on arc (i,j)
        xE = {}
        for q in Q:
            for i, j in A_wSink:
                xE[q, i, j] = m.addVar(lb=0, name="xE[%s,%s,%s]" % (q, i, j))
        
        # Enter time of a vessel at visitation i
        zE = {}
        for i in V_wSink:
            zE[i] = m.addVar(lb=0, name="zE[%s]" % i)
        
        # Exit time of a vessel at visitation i
        zX = {}
        for i in V_wSink:
            zX[i] = m.addVar(lb=0, name="zX[%s]" % i)
        
        # Duration that vessel s sails on Arc (i,j)
        w = {}
        for s in Ships:
            for i, j in A_flex:
                w[s, i, j] = m.addVar(lb=0, name="w[%s,(%s,%s)]" % (s, i, j))
        
        # Duration that vessel s needs for the repositioning
        phi = {}
        for s in Ships:
            phi[s] = m.addVar(lb=0, name="phi[%s]" % s)
        
        #
        # Integrate variables
        #

        m.update()
        
        #
        # Set objective
        #

        m.setObjective(
            # Repositioning
            # less shipping cost
            - quicksum(cHotel[s] * phi[s] + quicksum(cSail[s, i, j] * y[s, i, j] for i, j in A_wSink) + quicksum(cVarSail[s, i, j] * w[s, i, j] for i, j in A_flex) for s in Ships)
            
            # less port fee
            - quicksum(quicksum(quicksum(cPort[s, j] * y[s, i, j] for s in Ships) for i in In(j, A)) for j in V_wSink)
            
            # plus revenue due to demand during repositioning
            + quicksum(quicksum(quicksum((rDemand[id] - cMV[o] - cMV[j]) * xD[id, i, j] for i in In(j, A)) for j in d) for id, (o, d, q) in M.items())
            
            # plus revenue due to equipment during repositioning
            + quicksum(quicksum(quicksum((rEqp[q] - cMV[i]) * xE[q, i, j] for j in Out(i, A)) for i in V_ePlus[q])
                       - quicksum(quicksum(cMV[i] * xE[q, j, i] for j in In(i, A)) for i in V_eMinus[q]) for q in Q)
                     
            # Goal service operation
            # less cost for target routes
            - quicksum(quicksum(quicksum(cRoute[r, k, i] * eta[r, k, i] for i in range(1, n_max[r] + 1)) for k in K) for r in Routes)
            
            # minus cost for chartering at target routes
            - quicksum(quicksum(quicksum(cCharter[s, r] * y[s, i, tau[r]] for i in In(tau[r], A)) for r in Routes) for s in ShipsC)
            
            # plus charter revenue
            - quicksum(rCharter[s] * (1 - quicksum(quicksum(y[s, i, tau[r]] for i in In(tau[r], A)) for r in Routes)) for s in ShipsE)
            
            , GRB.MAXIMIZE)

        #
        # Add constraints
        #

        # 4.8 controls the amount of vessels reaching the target routes
        for r in Routes:
            m.addConstr(quicksum(quicksum(y[s, i, tau[r]] for s in Ships) for i in In(tau[r], A))
                        == quicksum(quicksum(eta[r, k, i] for i in range(1, n_max[r] + 1)) for k in K))

        # 4.9 sets type of vessel for each route
        for r in Routes:
            m.addConstr(quicksum(rho[r, k] for k in K) == 1)
        
        # 4.10 controls type restriction for each route
        for r in Routes:
            for k in K_notOnR[r]:
                m.addConstr(rho[r, k] == 0)
        
        # 4.11 upperbound and lowerbound for number of vessels for each route
        for r in Routes:
            m.addConstr(quicksum(quicksum(eta[r, k, i] for i in range(1, n_max[r] + 1)) for k in K) <= n_max[r])
        
        for r in Routes:
            m.addConstr(quicksum(quicksum(eta[r, k, i] for i in range(1, n_max[r] + 1)) for k in K) >= n_min[r])
        
        # 4.12  Exclude vessels from service that are not allowed
        for r in Routes:
            for k in K:
                m.addConstr(quicksum(eta[r, k, i] for i in range(1, n_max[r] + 1)) <= n_max[r] * rho[r, k])
                
        # 4.13 if vessel i is used on route, then vessel i-1,too
        for r in Routes:
            for k in K:
                for i in range(2, n_max[r] + 1):
                    m.addConstr(eta[r, k, i - 1] >= eta[r, k, i])
        
        # 4.14 controls, that if the vessel is used on the route, the type of the vessel is the same as the type used on the route
        for s in Ships:
            for r in Routes:
                for k in K:
                    m.addConstr(quicksum(y[s, i, tau[r]] for i in In(tau[r], A)) + p[s, k] - rho[r, k] <= 1)
        
        # 4.15 same as 4.14
        for s in Ships:
            for r in Routes:
                for k in K:
                    m.addConstr(quicksum(y[s, i, tau[r]] for i in In(tau[r], A)) - p[s, k] + rho[r, k] <= 1)
        
        # 4.16 controls, that a vessel can can only be part of one target route
        for s in Ships:
            m.addConstr(quicksum(quicksum(y[s, i, tau[r]] for i in In(tau[r], A)) for r in Routes) <= 1)

        # 4.17 controls that only one vessel can visit a visitation at the same time
        for j in V_wSink:
            m.addConstr(quicksum(quicksum(y[s, i, j] for i in In(j, A)) for s in Ships) <= 1)

        # 4.18 flow balance vessels (only end at graph sink)
        for j in createVwSinkwStart(V_wSink, Ships, vStart):
            for s in Ships:
                m.addConstr(quicksum(y[s, i, j] for i in In(j, A)) - quicksum(y[s, j, i] for i in Out(j, A)) == 0)

        # 4.19 defines the time used by a vessel for the repositioning operation
        for s in Ships:
            m.addConstr(phi[s] >= quicksum(quicksum(tX[i] * y[s, i, tau[r]] for i in In(tau[r], A)) for r in Routes) - zX[vStart[s]])

        # 4.20 controls that vessels leave their starting point, if used on a route
        for s in Ships:
            m.addConstr(quicksum(y[s, vStart[s], j] for j in Out(vStart[s], A)) ==
                        quicksum(quicksum(y[s, i, tau[r]] for i in In(tau[r], A)) for r in Routes))
        
        # 4.21 controls that phase-in slots are taken in time ascending order
        for r in Routes:
            for i in In(tau[r], A):
                for j in (In(tau[r], A) - {i}):
                    if delta[i, j] == 1:
                        m.addConstr(quicksum(y[s, i, tau[r]] for s in Ships) <= quicksum(y[s, j, tau[r]] for s in Ships))

        # 4.22 controls the refrigerated capacity of a vessel for the time of repositioning
        for i, j in A_wSink:
            m.addConstr(quicksum(xD[id, i, j] for id, (o, d, q) in M.items() if q == 'rf') <= quicksum(u_rf[s] * y[s, i, j] for s in Ships))
        
        # 4.23 controls the overall capacity of a vessel for the time of repositioning
        for i, j in A_wSink:
            m.addConstr(quicksum(xD[id, i, j] for id in M) + quicksum(xE[q, i, j] for q in Q) <= quicksum(u_dc[s] * y[s, i, j] for s in Ships))
        
        # 4.24 controls the amount of demand leaving the origin
        for id, (o, d, q) in M.items():
            m.addConstr(quicksum(xD[id, o, j] for j in Out(o, A)) <= (amount[id] * quicksum(quicksum(y[s, o, j] for s in Ships) for j in Out(o, A))))
        
        # 4.24 controls that demand doesn't leave their destination (not needed in the model because it is clear, but here needed)
        for id, (o, d, q) in M.items():
            for d2 in d:
                m.addConstr(quicksum(xD[id, d2, j] for j in Out(d2, A)) == 0)

        # 4.25 guaranteed, that demand arrives at the destination (flow balance)
        for id, (o, d, q) in M.items():
            for j2 in V - ({o} | d):
                m.addConstr(quicksum(xD[id, i, j2] for i in In(j2, A)) - quicksum(xD[id, j2, j] for j in Out(j2, A)) == 0)

        # 4.26 guaranteed, that equipment arrives at the destination (flow balance)
        for q in Q:
            for j2 in V_wSink - V_ePlusMinus[q]:
                m.addConstr(quicksum(xE[q, i, j] for i, j in A_wSink if j == j2) - quicksum(xE[q, i, j] for i, j in A_wSink if i == j2) == 0)
        
        # 4.27 sets duration for flexible arc, if used
        for i, j in A_flex:
            for s in Ships:
                m.addConstr(w[s, i, j] <= d_max[i, j, s] * y[s, i, j])
        
        for i, j in A_flex:
            for s in Ships:
                m.addConstr(w[s, i, j] >= d_min[i, j, s] * y[s, i, j])
        
        # 4.28 sets the enter time of an inflexible visitation, if visited by a vessel
        for i in V_inflex:
            m.addConstr(zE[i] == tE[i] * (quicksum(quicksum(y[s, j, i] for j in In(i, A)) for s in Ships)))
        
        # 4.29 sets the exit time of an inflexible visitation, if visited by a vessel
        for i in V_inflex:
            m.addConstr(zX[i] == tX[i] * (quicksum(quicksum(y[s, i, j] for j in Out(i, A)) for s in Ships)))
        
        # 4.30 sets the possible enter and exit time of a flexible visitation
        for i, j in A_flex:
            for ss in Ships:
                m.addConstr(zX[i] + w[ss, i, j] <= zE[j] + getBigM(tau, tE) * (1 - y[ss, i, j]), "Constraint30_{0}_{1}".format(i,j,ss))
        
        # 4.31 controls the time used for a flexible visitation
        for i in V_flex:
            for s in Ships:
                m.addConstr(quicksum(quicksum(tMV[s, o] * xD[id, o, j] for j in Out(o, A))
                                     for id, (o, d, q) in createMOrig(i, M).items()) +
                            quicksum(quicksum(quicksum(tMV[s, j] * xD[id, i, j] for i in In(j, A)) for j in d)
                                     for id, (o, d, q) in createMDest(i, M).items()) +
                            quicksum(quicksum(quicksum(tMV[s, i] * xE[q, i2, j] for j in Out(i2, A))
                                              for i2 in {i} & V_ePlus[q]) +
                                     quicksum(quicksum(tMV[s, i] * xE[q, j, i2] for j in In(i2, A))
                                              for i2 in {i} & V_eMinus[q]) for q in Q) -
                            zX[i] + zE[i] + tP[i] * quicksum(y[s, j, i] for j in In(i, A))
                            <= 0)
                
        """
        # Needed if charter vessels should not be used (Used for scenario additional vessels multiple)
        for ss in ShipsC:
            m.addConstr(quicksum(y[ss, vStart[ss], j] for j in Out(vStart[ss])) == 0)
        """

        # 4.32 correct start time for flexible nodes
        for s in Ships:
            if vStart[s] in V_flex:
                m.addConstr(quicksum(y[s, i, j] * tE[j] for i, j in A_flex if i == vStart[s]) 
                            - quicksum(w[s, i, j] for i, j in A_flex if i == vStart[s]) >= zX[vStart[s]])

        print("*"*125)
        print(f"*** Solve {filename} with Gurobi\n***")
        print("*** Graph info")
        lenEqp = len(V_ePlusMinus['dc']) + len(V_ePlusMinus['rf'])
        print("*** |S|: %s" % len(Ships))
        print("*** |S^E|: %s" % len(ShipsE))
        print("*** |S^C|: %s" % len(ShipsC))
        print("*** |V|: %s" % len(V))
        print("*** |A|: %s" % len(A))
        print("*** |A^i|: %s" % len(A_inflex))
        print("*** |A^f|: %s" % len(A_flex))
        print("*** |M|: %s" % len(M))
        print("*** |E|: %s" % lenEqp)
        print()                  

        #
        # Run optimization and print results
        #

        # m.setParam("NumericFocus", 2)
        # m.setParam("Presolve", 0)
        # m.setParam('MIPGap', 2.00)
        m.setParam('TimeLimit', 86400)
        m.setParam('Threads', 4)
        m.optimize()

        #
        # Preparations for Output
        #

        serviceNames = {177: "WCSA", 24: "WAF7", 121: "SAE", 7: "ME3", 81: "MECL1", 42: "SAMBA", 43: "AMEX", 32: "SAECS CORE", 3: "SAFARI 1", 100: "unknown"}
        vesselClasses = {18: "PMax25", 19: "PMax28", 30: "PMax35"}
        
        rhos = {}
        etas = {}
        vessels_arcs = {ss: [] for ss in Ships}
        Ship = {}
        ShipsP = {}

        for s in Ships:
            Ship[s] = list()
            ShipsP[s] = [] 

        demands = []
        port_enter = {}
        port_exit = {}
        VT = set()
        for r in Routes:
            VT.add(tau[r])
           
        #
        # Parse solution
        #
   
        if m.status == 3 or m.status == 4:
            print("### Objective: infeasible")
        else:
            for var in m.getVars():
                if var.x > 0.1:
                    if var.varName.find("r") == 0:
                        rhos[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] = int(var.varName[var.varName.find(",")+1:var.varName.find("]")])
                    if var.varName.find("e") == 0:
                        if (int(var.varName[var.varName.find("[")+1:var.varName.find(",")])) in etas:
                            etas[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] = etas[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] + 1
                        else:
                            etas[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] = 1            
                    if var.varName.find("y") == 0:
                        ss, ii, jj = [int(x) for x in var.VarName[2:var.VarName.find("]")].split(",")]
                        vessels_arcs[ss].append((ii,jj))
                    if "xD" in var.VarName:
                        demand_id, ii, jj = [int(value) for value in var.VarName[3:var.VarName.find("]")].split(",")]

                        if jj in M[demand_id][1]:
                            demands.append((demand_id, M[demand_id][0], jj, var.x))
                            
                    if "xE" in var.VarName:
                        print("EQUIPMENT")
                    if "zE" in var.VarName:
                        port_enter[int(var.VarName[3:var.VarName.find("]")])] = round(var.x,2)
                    if "zX" in var.VarName:
                        port_exit[int(var.VarName[3:var.VarName.find("]")])] = round(var.x,2)

            service_vessels = {rr: [] for rr in Routes}
            vessel_path = {ss: [] for ss in Ships}
            
            service_info = {rr: (rhos[rr], etas[rr]) for rr in Routes}
            vessel_demand = {ss: [] for ss in Ships}
            vessels_not_on_service = list(Ships)

            for ss in Ships:
                current_node = vStart[ss]
                vessel_path[ss].append(current_node)
                
                for nn in range(1, len(vessels_arcs[ss])+1):
                    for (ii, jj) in vessels_arcs[ss]:
                        
                        if ii == current_node:
                            current_node = jj
                            vessel_path[ss].append(current_node)

                for rr in Routes:
                    if tau[rr] == current_node:
                        service_vessels[rr].append(ss)

            for vessels in service_vessels.values():
                for vessel in vessels:
                    vessels_not_on_service.remove(vessel)
            
            for demand, origin, destination, d_amount in demands:
                for vessels in service_vessels.values():
                    for vessel in vessels:
                        if origin in vessel_path[vessel]:
                            vessel_demand[vessel].append((demand, origin, destination, d_amount))

            #
            # Output solution
            #

            print("\n***")
            if m.status == 9:
                print(f"***  |- Calculation finished, timelimit reached {round(m.Runtime, 2):,} ({round(m.Runtime, 2)}) sec.))")
            else:
                print(f"***  |- Calculation finished, took {round(m.Runtime, 2):,} ({round(m.Runtime, 2)}) sec.")
            print("***  |{0}".format("-"*119))
            print("***\n" + "*"*125)

            fvessel = len(str(len(Ships)))
            fdemand = len(str(len(M)))
            fport = len(str(len(V)))
            fcontainer = len(str(max(amount.values()))) + 2

            print("#"*125)
            print("###\n### Results\n###")
            print(f"### The objective of the solution is {round(m.objVal,4):,} ({round(m.objVal,4)}) and is feasible with a Gap of {round(m.MIPGap, 4)}\n###")

            for service, (vessel_type, vessel_amount) in sorted(service_info.items()):
                print(f"### Service {service} ({serviceNames[service]}) is operated by {vessel_amount} vessels from type {vessel_type} ({vesselClasses[vessel_type]})")
                
                for vessel in sorted(service_vessels[service]):
                    print(f"###  |- Vessel {vessel:{fvessel}} has the path {vessel_path[vessel]}")
                    text = ""
                    for pos, port in enumerate(vessel_path[vessel]):
                        if port == vessel_path[vessel][0]:
                            text += f"{port} ({unlocode[port]} Out@{port_exit[port]}) - "
                        elif port != vessel_path[vessel][-1]:
                            text += f"{port} ({unlocode[port]} In@{port_enter[port]}/Out@{port_exit[port]}) - "
                        else:
                            text += f"{port} ({unlocode[port]})"
                    if len(text) > 117:
                        texte = [text[ii:ii+117] for ii in range(0, len(text), 117)]
                    else:
                        texte = [text]
                    for text in texte:
                        print("###  |  " + text)

                    for demand, origin, destination, amount in sorted(vessel_demand[vessel]):
                        print(f"###  |   |- and carried Demand {demand:{fdemand}}, {amount:{fcontainer}.1f} containers from {origin:{fport}} to {destination:{fport}}")
                print("###  |")
            print(f"### Vessels not used in the solution: {sorted(vessels_not_on_service)}")
            print("###  |{0}".format("-"*119))
            print("###\n{0}".format("#"*125))

            print("\n### Detailed variable overview")
            print()
            print("### All variables with value greater zero:")
            for v in m.getVars():
                if v.x > 0.1:
                    print('%s = %g' % (v.varName, v.x)) 
            print()
            print("### All variables with value smaller zero:")
            for v in m.getVars():
                if v.x < -0.1:
                    print('%s = %g' % (v.varName, v.x)) 
            print()
            print("### End")


if __name__ == "__main__":
    args = None
    parser = argparse.ArgumentParser(description='OR B WS16/17 Homework 1.')
    parser.add_argument('-fr', '--fleetrepositioning', type=str, help='Path to the lsfrp-instance to be solved', required=True)
    parser.add_argument('-fd', '--fleetdeployment', type=str, help='Path to the lsfdp-instance to be solved', required=True)
    args = parser.parse_args()

    if not os.path.exists(args.fleetrepositioning):
        print("Instance LSFRP not found: {0}".format(args.fleetrepositioning), file=sys.stderr)
        sys.exit(1)
    if not os.path.exists(args.fleetdeployment):
        print("Instance LSFDP not found: {0}".format(args.fleetdeployment), file=sys.stderr)
        sys.exit(1)
        
    # Note: error handling omitted; add your own if desired.
    instance = args.fleetrepositioning
    data = args.fleetdeployment
    print("Instance LSFRP: {0}".format(instance))
    print("Instance LSFDP: {0}\n".format(data))
    solve_lsfdrp(instance, data)
