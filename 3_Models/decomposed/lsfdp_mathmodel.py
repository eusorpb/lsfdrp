﻿import argparse
from gurobipy import *
import pickle


def solve_lsfdp(fr, fd):
    """
    Method to solve the liner shipping fleet deployment problem
    """
    # Read instance
    frRoutes, frK, Ships, ShipsE, ShipsC, cHotel, A, A_wSink, A_inflex, A_flex, V, V_wSink, V_inflex, V_flex, Q, M, RPO, u_rf, u_dc, vStart, p, tau,\
        delta, cMV, cPort, cSail, cVarSail, rDemand, rEqp, amount, V_ePlus, V_eMinus, V_ePlusMinus, tMV, tE, tX, tP, d_min, d_max, unlocode = pickle.load( open( fr, "rb" ) )
    
    Routes, K, cCharter, rCharter, n_min, n_max, cRoute, K_notOnR, filename= pickle.load( open( fd, "rb" ) )

    # Used for the output
    services = {177: "WCSA", 24: "WAF7", 121: "SAE", 7: "ME3", 81: "MECL1", 42: "SAMBA", 43: "AMEX"}       

    if frRoutes != Routes or frK != K:
        print("Data for routes or fessel classes does not match!")
        print("From fr:\n {0}\n {1}".format(frRoutes, frK))
        print("From fd:\n {0}\n {1}".format(Routes, K))
    else:
        #
        # Create optimization model
        #
        m = Model('fleetDeployment')
        
        #
        # Create variables
        #
        # indicates, if type kk of vessel will be used on route rr
        rho = {}
        for rr in Routes:
            for kk in K:
                rho[rr, kk] = m.addVar(vtype='B', name="rho[%s,%s]" % (rr, kk))
        
        # indicates, if vessel ii of type kk will be used on route rr
        eta = {}
        for rr in Routes:
            for kk in K:
                for ii in range(1, n_max[rr] + 1):
                    eta[rr, kk, ii] = m.addVar(vtype='B',
                                            name="eta[%s,%s,%s]" % (rr, kk, ii))
        
        # indicates if vessel ss will be used on route rr
        y = {}
        for ss in Ships:
            for rr in Routes:
                y[ss, rr] = m.addVar(vtype='B', name="y[%s,%s]" % (ss, rr))
        
        #
        # Integrate variables
        #

        m.update()

        # The order of the model is the order from the fleet deployment model in the appendix

        #
        # Set objective
        #

        m.setObjective(
            # cost for target routes
            - quicksum(quicksum(quicksum(cRoute[rr, kk, ii] * eta[rr, kk, ii] for ii in
                                        range(1, n_max[rr] + 1)) for kk in K) for rr in Routes)
            # cost for chartering at target routes 
            - quicksum(quicksum(cCharter[ss, rr] * y[ss, rr] for rr in Routes) for ss in ShipsC)
            # revenue for chartering out
            + quicksum(rCharter[ss] * (1 - (quicksum(y[ss, rr] for rr in Routes))) for ss in ShipsE),
            GRB.MAXIMIZE)

        #
        # Add constraints
        #

        # Sets type of vessel for each route
        for rr in Routes:
            m.addConstr(quicksum(rho[rr, kk] for kk in K) == 1)
        
        # Forbids specific vessel types on route 
        for rr in Routes:
            for kk in K_notOnR[rr]:
                m.addConstr(rho[rr, kk] == 0)

        # Upperbound and lowerbound for number of vessels for each route
        for rr in Routes:
            m.addConstr(quicksum(quicksum(eta[rr, kk, ii] for ii in range(1, n_max[rr] + 1)) for kk in K) <=
                        n_max[rr])
        for rr in Routes:
            m.addConstr(quicksum(quicksum(eta[rr, kk, ii] for ii in range(1, n_max[rr] + 1)) for kk in K) >=
                        n_min[rr])
        
        # Restricts the number of vessels from a vessel type
        for rr in Routes:
            for kk in K:
                m.addConstr(quicksum(eta[rr, kk, ii] for ii in range(1, n_max[rr] + 1)) <=
                            n_max[rr] * rho[rr, kk])
                
        # If vessel ii is used on route, then vessel ii-1,too
        for rr in Routes:
            for kk in K:
                for ii in range(2, n_max[rr] + 1):
                    m.addConstr(eta[rr, kk, ii - 1] >= eta[rr, kk, ii])
        
        # A vessel can only operate on one route at most
        for ss in Ships:
            m.addConstr(quicksum(y[ss, rr] for rr in Routes) <= 1)
        
        # The amount of vessels used on a service from a vessel class must be available 
        for rr in Routes:
            for kk in K:
                m.addConstr(quicksum(y[ss, rr] * p[ss, kk] for ss in Ships) == quicksum(eta[rr, kk, ii] for ii in range(1, n_max[rr] + 1)))
        
        #
        # Run optimization
        #
        m.setParam('Threads', 4)
        m.optimize()

        #
        #  Preparations for output and print results
        #
        t_rhos = {}
        t_etas = {}
        t_ShipsUsed = []
        t_ShipsOnRoute = {}
            
        if m.status == 3 or m.status == 4:
            print("\n### Objective: infeasible")
        else:
            for var in m.getVars():
                if var.x > 0.1:
                    if var.varName.find("r") == 0:
                        t_rhos[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] = int(var.varName[var.varName.find(",")+1:var.varName.find("]")])
                    if var.varName.find("e") == 0:
                        if (int(var.varName[var.varName.find("[")+1:var.varName.find(",")])) in t_etas:
                            t_etas[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] = t_etas[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] + 1
                        else:
                            t_etas[(int(var.varName[var.varName.find("[")+1:var.varName.find(",")]))] = 1
                    if var.varName.find("y") == 0:
                        t_vessel = int(var.varName[var.varName.find("[")+1:var.varName.find(",")])
                        t_ShipsUsed.append(t_vessel)
                        t_ShipsOnRoute[t_vessel] = [(int(var.varName[var.varName.find(",")+1:var.varName.find("]")]))]
            
            print("\n### Objective value in 1k: %.2f" % (m.objVal / 1000))
            print("### Objective value: %.4f" % (m.objVal))
            print("### Gap: %s" % (m.MIPGap))
            print("###")
            print("### Service: vessel typ and number of vessels")
            
            for rr in Routes:
                info = "### " + str(rr) +"("+str(services[rr]) + "): "
                if t_rhos[rr] == 18:
                    info += str(t_rhos[rr]) + ", (PMax25), " + str(t_etas[rr])
                elif t_rhos[rr] == 19:
                    info += str(t_rhos[rr]) + ", (PMax28), " + str(t_etas[rr])
                elif t_rhos[rr] == 30:
                    info += str(t_rhos[rr]) + ", (PMax35), " + str(t_etas[rr])
                else:
                    info += "ERROR, " + str(t_etas[rr])
                print(info)
            print("### ---")

            print("### Charter vessels chartered in:")
            for ss in ShipsC:
                if ss in t_ShipsUsed:
                    print("### {0} on service {1}".format(ss,t_ShipsOnRoute[ss][0]))
            print("### ---")

            print("### Own vessels chartered out:")
            for ss in ShipsE:
                if ss not in t_ShipsUsed:
                    print("### {0}".format(ss))
            print("### ---")
            
            print("### End of overview")

            print("")
            print("### All variables with value greater zero:")
            for v in m.getVars():
                if v.x > 0.1:
                    print('%s = %g' % (v.varName, v.x))  
                    
            print("")
            print("### All variables with value zero:")
            for v in m.getVars():
                if v.x <= 0.1:
                    print('%s = %g' % (v.varName, v.x))
                    
            print("")
            print("### End")


if __name__ == "__main__":
    args = None
    parser = argparse.ArgumentParser(description='LSFDP used in the publication to LSFDRP')
    parser.add_argument('-fr', '--fleetrepositioning', type=str, help='Path to the lsfrp-instance', required=True)
    parser.add_argument('-fd', '--fleetdeployment', type=str, help='Path to one lsfdp-configuration for the given lsfrp-instnace', required=True)
    args = parser.parse_args()

    if not os.path.exists(args.fleetrepositioning):
        print("Instance LSFRP not found: {0}".format(args.fleetrepositioning), file=sys.stderr)
        sys.exit(1)
    if not os.path.exists(args.fleetdeployment):
        print("Instance LSFDP not found: {0}".format(args.fleetdeployment), file=sys.stderr)
        sys.exit(1)
        
    # Note: error handling omitted; add your own if desired.
    instance = args.fleetrepositioning
    data = args.fleetdeployment
    print("Instance LSFRP: {0}".format(instance))
    print("Instance LSFDP: {0}\n".format(data))
    solve_lsfdp(instance, data)
